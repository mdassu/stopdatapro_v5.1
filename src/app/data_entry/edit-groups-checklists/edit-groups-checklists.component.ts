import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";

import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { Router, ActivatedRoute } from "@angular/router";

import { EditgroupchecklistService } from "src/app/_services/editgroupchecklist.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

@Component({
  selector: "app-edit-groups-checklists",
  templateUrl: "./edit-groups-checklists.component.html",
  styleUrls: ["./edit-groups-checklists.component.css"],
  providers: [MessageService, CustomValidatorsService],
})
export class EditGroupsChecklistsComponent implements OnInit {
  // Varialbe for fields name
  AddObservatinGeneral: FormGroup;
  submitted: boolean;
  userId: any;
  userFirstName: string;
  userLastName: string;
  grpList: any;
  finalGroups: any = [];
  tempGroups: any = [];
  observationDateType: any = [
    { name: "Date Range", id: 0 },
    { name: "Update Till Date", id: 1 },
  ];

  showDate: boolean = false;
  initialGeneralFormData: any;
  curDate = new Date();
  isLoaded: boolean;
  dateFormatType: any;
  dateFormatTypeNew: any;
  dateFormat: any;
  locale: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private route: Router,
    private activetedRoute: ActivatedRoute,
    private editgroupchecklistservice: EditgroupchecklistService,
    private translate: TranslateService,
    private auth: AuthenticationService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLDATAENTRY",
        url: "./assets/help/edit-groups-on-checklists-edit.md",
      },
      {
        label: "LBLOBSCHKLISTGROUPS",
        routerLink: ["/edit-groups-on-checklists"],
      },
    ]);
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    this.isLoaded = true;
    this.userId = this.activetedRoute.snapshot.paramMap.get("userid");
    this.getGroupChecklistData();

    this.dateFormatType = this.auth.UserInfo["dateFormat"];
    if (this.dateFormatType == "MM/dd/yyyy") {
      this.dateFormatTypeNew = "LBLMDY";
    } else if (this.dateFormatType == "dd/MM/yyyy") {
      this.dateFormatTypeNew = "LBLDMY";
    } else if (this.dateFormatType == "yyyy/MM/dd") {
      this.dateFormatTypeNew = "LBLYMD";
    }
    this.dateFormat = this.auth.UserInfo["dateFormat"].toLowerCase();
    this.dateFormat = this.dateFormat.replace("yyyy", "yy");

    // Add Observatin General
    this.AddObservatinGeneral = this.fb.group({
      obsDatetype: new FormControl(0),
      obsDateFrom: new FormControl(null, Validators.required),
      obsDateTo: new FormControl(null, Validators.required),
    });
  }

  get GeneralSubForm() {
    return this.AddObservatinGeneral.controls;
  }

  getGroupChecklistData() {
    var parameters = {
      userId: this.userId,
    };
    this.editgroupchecklistservice
      .getGroupChecklistData(parameters)
      .subscribe((res) => {
        if (res["status"] == true) {
          var fn = this;
          this.userFirstName = res["observerName"]["FIRSTNAME"];
          this.userLastName = res["observerName"]["LASTNAME"];
          var groupListing = res["group"];
          var groupOption = res["groupData"];
          this.grpList = [{ label: "All", value: "" }];
          this.finalGroups = [];

          var i = 1;
          groupListing.forEach(async function (groupLS) {
            var fArr = [];
            fArr["data"] = await groupOption.filter(function (groupVal) {
              return groupVal.GROUPTYPEID == groupLS.GROUPTYPEID;
            });
            var selectedGroup = await fArr["data"].filter(function (selectVal) {
              return selectVal.FLAG == 1;
            });
            fn.translate.get("LBLSELECT").subscribe((res: string) => {
              const groupsSelect = JSON.parse(JSON.stringify(fArr["data"]));
              groupsSelect.splice(0, 0, {
                GROUPID: 0,
                GROUPNAME: res,
              });
              fArr["data"] = groupsSelect;
            });
            fArr["groupName"] = groupLS.GROUPTYPENAME;
            fArr["fieldname"] = groupLS.GROUPTYPENAME.toLowerCase();

            if (selectedGroup.length > 0) {
              fn.AddObservatinGeneral.addControl(
                groupLS.GROUPTYPENAME.toLowerCase(),
                new FormControl(selectedGroup[0]["GROUPID"])
              );
            } else {
              fn.AddObservatinGeneral.addControl(
                groupLS.GROUPTYPENAME.toLowerCase(),
                new FormControl(null)
              );
            }
            fn.tempGroups.push(groupLS.GROUPTYPENAME.toLowerCase());
            fn.finalGroups.push(fArr);
            if (groupListing.length == i) {
              fn.initialGeneralFormData = fn.AddObservatinGeneral.getRawValue();
            }
            i++;
          });
        }
        this.isLoaded = false;
      });
  }

  chnageObsDate(dateType) {
    if (dateType == 1) {
      this.showDate = true;
      this.AddObservatinGeneral.get("obsDateFrom").clearValidators();
      this.AddObservatinGeneral.get("obsDateTo").clearValidators();
    } else {
      this.showDate = false;
      this.AddObservatinGeneral.controls["obsDateFrom"].setValidators([
        Validators.required,
      ]);
      this.AddObservatinGeneral.controls["obsDateTo"].setValidators([
        Validators.required,
      ]);
    }
    this.AddObservatinGeneral.get("obsDateFrom").updateValueAndValidity();
    this.AddObservatinGeneral.get("obsDateTo").updateValueAndValidity();
  }

  onSubmit(value: string) {
    this.submitted = true;
    if (this.AddObservatinGeneral.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      if (
        this.convertDate(this.AddObservatinGeneral.value["obsDateFrom"]) >
        this.convertDate(this.AddObservatinGeneral.value["obsDateTo"])
      ) {
        this.translate
          .get("ALTOBSERVATIONGROUPDATE")
          .subscribe((obsRes: string) => {
            this.messageService.add({
              severity: "error",
              detail: obsRes,
            });
          });
      } else {
        this.isLoaded = true;
        if (this.AddObservatinGeneral.value["obsDatetype"] == 0) {
          if (this.AddObservatinGeneral.value["obsDateFrom"] != null) {
            this.AddObservatinGeneral.value["obsDateFrom"] = this.convertDate(
              this.AddObservatinGeneral.value["obsDateFrom"]
            );
          } else {
            this.AddObservatinGeneral.value["obsDateFrom"] = "";
          }
          if (this.AddObservatinGeneral.value["obsDateTo"] != null) {
            this.AddObservatinGeneral.value["obsDateTo"] = this.convertDate(
              this.AddObservatinGeneral.value["obsDateTo"]
            );
          } else {
            this.AddObservatinGeneral.value["obsDateTo"] = "";
          }
        } else {
          this.AddObservatinGeneral.value["obsDateFrom"] = "";
          this.AddObservatinGeneral.value["obsDateTo"] = "";
        }

        var groupIds = [];
        var fn = this;
        this.tempGroups.forEach(function (group) {
          if (fn.AddObservatinGeneral.value[group] != null) {
            groupIds.push(fn.AddObservatinGeneral.value[group]);
          }
          delete fn.AddObservatinGeneral.value[group];
        });
        this.AddObservatinGeneral.value["groupIds"] = groupIds.join();
        this.AddObservatinGeneral.value["userId"] = this.userId;

        this.editgroupchecklistservice
          .SaveGrouponChecklistData(this.AddObservatinGeneral.value)
          .subscribe((res) => {
            if (res["status"] == true) {
              this.submitted = false;
              this.AddObservatinGeneral.markAsPristine();
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
              this.isLoaded = false;
            } else {
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "error",
                  detail: res,
                });
              });
              this.isLoaded = false;
            }
          });
      }
    }
  }

  convertDate(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join("/");
  }

  resetObsForm() {
    if (this.initialGeneralFormData["obsDatetype"] == 1) {
      this.showDate = true;
      this.AddObservatinGeneral.controls["obsDateFrom"].setValue(new Date());
      this.AddObservatinGeneral.controls["obsDateTo"].setValue(new Date());
    } else {
      this.showDate = false;
    }
    this.AddObservatinGeneral.patchValue(this.initialGeneralFormData);
  }
}
