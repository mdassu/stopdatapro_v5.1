import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { Router, RoutesRecognized } from "@angular/router";
import { InjuryStatisticsService } from "src/app/_services/injury-statistics.service";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
import { AuthenticationService } from "../../_services/authentication.service";
import { filter, pairwise } from "rxjs/operators";
declare var $: any;

@Component({
  selector: "app-injury-statistics-listing",
  templateUrl: "./injury-statistics-listing.component.html",
  styleUrls: ["./injury-statistics-listing.component.css"],
  providers: [ConfirmationService, MessageService],
})
export class InjuryStatisticsListingComponent implements OnInit {
  allInjury: any;
  injuryListing: any;
  injuryCols: any[];
  injuryCount: number;
  selectedfilterInjury: string = "YEAR";
  searchBy: any = [];
  searchText: any;
  selectedInjury: any = [];
  filterSites: any = [];
  injurySites: any = [];
  isLoaded: boolean;
  confirmClass: any;
  appName: any;
  parameters: any;
  previousUrl: any;
  userId: any;
  roleId: any;

  // Breadcrumb Service
  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private auth: AuthenticationService,
    private route: Router,
    private injurystatisticsservice: InjuryStatisticsService,
    private translate: TranslateService,
    private env: EnvService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLDATAENTRY",
        url: "./assets/help/injury-statistics-listing.md",
      },
      {
        label: "LBLINJURYSTATSLISTING",
        routerLink: ["/injury-statistics-listing"],
      },
    ]);
  }

  ngOnInit() {
    this.userId = this.auth.UserInfo["userId"];
    this.roleId = this.auth.UserInfo["roleId"];
    this.route.events
      .pipe(
        filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((e: any) => {
        var url = e[0].urlAfterRedirects; // previous url
        localStorage.setItem("previousUrl-" + this.userId, url);
      });
    // Table Header
    this.injuryCols = [
      { field: "YEAR", header: "LBLYEAR" },
      { field: "SITE NAME", header: "LBLSITENAME" },
      { field: "No of injuries", header: "LBLNOOFINJURY" },
      { field: "STATUS", header: "LBLSTATUS" },
    ];

    // Injury count
    this.injuryCount = this.injuryCols.length;

    // Get Injury
    this.checkFilter();

    // Search List
    this.injuryCols.map((item, key) => {
      this.translate.get(item.header).subscribe((text: string) => {
        if (key < 3) {
          this.searchBy.push({
            field: item.field,
            header: text,
          });
        }
      });
      this.selectedfilterInjury = "YEAR";
    });
    this.appName = this.env.appName;
  }

  checkFilter() {
    this.parameters = {};
    this.previousUrl = localStorage.getItem("previousUrl-" + this.userId);
    if (this.previousUrl) {
      if (
        this.previousUrl.search("/add-injury-statistics") != -1 ||
        this.previousUrl.search("/edit-injury-statistics/") != -1
      ) {
        if (localStorage.getItem("filterInfo")) {
          this.toggle();
          // this.animationState = "in";
          this.parameters = JSON.parse(localStorage.getItem("filterInfo"));
          var keys = Object.keys(this.parameters);
          keys.map((param) => {
            if (this.parameters[param]) {
              if (param == "siteId") {
                this.injurySites = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
            }
          });
          localStorage.removeItem("previousUrl-" + this.userId);
        }
      } else {
        localStorage.removeItem("filterInfo");
        this.injurySites = [];
        this.injurySites.push(this.auth.UserInfo["defaultSiteId"]);
      }
    } else {
      localStorage.removeItem("filterInfo");
      this.injurySites = [];
      this.injurySites.push(this.auth.UserInfo["defaultSiteId"]);
    }

    this.getInjuryStatistics(this.parameters);
  }

  // Get Injury Statistics
  getInjuryStatistics(parameters) {
    this.injurystatisticsservice.getInjuryList(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.allInjury = res["generalData"];
        this.injuryListing = res["generalData"];
        // this.filterSites = res["filterSites"];
        this.filterSites = [];
        var siteList = res["filterSites"];
        siteList.map((data) => {
          this.filterSites.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });

        if (parameters["clearFilter"] != 0) {
          this.injurySites = [res["defaultSiteId"]];
        }
      } else {
        this.allInjury = [];
        this.injuryListing = [];
        this.filterSites = [];
        var siteList = res["filterSites"];
        siteList.map((data) => {
          this.filterSites.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });
        if (parameters["clearFilter"] != 0) {
          this.injurySites = [res["defaultSiteId"]];
        }
      }
      if (this.injurySites) {
        this.filterSites = this.auth.rearrangeSelects(
          this.filterSites,
          this.injurySites
        );
      }
      this.isLoaded = true;
    });
  }

  // searching data
  searching() {
    var field = this.selectedfilterInjury;
    var stext = "";
    if (field == "YEAR" || field == "No of injuries") {
      if (this.searchText) {
        stext = this.searchText;
      }
    } else {
      if (this.searchText != undefined) {
        stext = this.searchText.toLowerCase();
      } else {
        stext = this.searchText;
      }
    }
    var fn = this;
    if (stext != "") {
      var newList = this.allInjury.filter((item) => {
        if (field == "YEAR" || field == "No of injuries") {
          return item[field].toString().indexOf(stext) >= 0;
        } else {
          return item[field].toLowerCase().indexOf(stext) >= 0;
        }
      });
      this.injuryListing = newList;
    } else {
      this.injuryListing = this.allInjury;
    }
  }

  filtering() {
    var siteId = "";
    if (this.injurySites) {
      siteId = this.injurySites.join();
    }
    var parameters = {
      siteId: siteId,
      clearFilter: 0,
    };
    localStorage.removeItem("filterInfo");
    localStorage.setItem("filterInfo", JSON.stringify(parameters));
    this.getInjuryStatistics(parameters);
  }

  clearFilter() {
    this.injurySites = "";
    var parameters = {
      clearFilter: 0,
    };
    this.searchText = "";
    this.getInjuryStatistics(parameters);
  }

  deleteRecord() {
    this.confirmClass = "warning-msg";
    if (this.selectedInjury == "") {
      this.messageService.add({
        severity: "error",
        detail: "Please select atleast one injury.",
      });
    } else {
      var msg = "";
      this.translate.get("ALTDELCONFIRMINJURYSTAT").subscribe((confirmMsg) => {
        msg = confirmMsg;
      });
      this.confirmationService.confirm({
        message: msg,
        accept: () => {
          var injuryIds = [];
          $.each(this.selectedInjury, function (key, InjuryId) {
            injuryIds.push(InjuryId["INJURYID"]);
          });
          var injuryId = injuryIds.join();
          this.injurystatisticsservice
            .deleteInjury({ injuryId: injuryId })
            .subscribe((res) => {
              this.selectedInjury = [];
              this.getInjuryStatistics({});
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedInjury = [];
          this.getInjuryStatistics({});
          this.searchText = "";
        },
      });
    }
  }

  changeStatus(id) {
    this.confirmClass = "info-msg";
    if (this.selectedInjury == "" && id == "") {
      this.messageService.add({
        severity: "error",
        detail: "Please select atleast one injury.",
      });
    } else {
      var msg = "";
      this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((confirmMsg) => {
        msg = confirmMsg;
      });
      this.confirmationService.confirm({
        message: msg,
        accept: () => {
          var injuryIds = [];
          var injuryId;
          if (id == "") {
            $.each(this.selectedInjury, function (key, InjuryId) {
              injuryIds.push(InjuryId["INJURYID"]);
            });
            injuryId = injuryIds.join();
          } else {
            injuryId = id;
          }
          this.injurystatisticsservice
            .updateInjuryStatus({ injuryId: injuryId })
            .subscribe((res) => {
              this.selectedInjury = [];
              this.getInjuryStatistics({});
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedInjury = [];
          this.getInjuryStatistics({});
          this.searchText = "";
        },
      });
    }
  }

  toggle() {
    $("#slideDwon").slideToggle();
  }

  onSelectAll() {
    const selected = this.filterSites.map((item) => item.SITEID);
    this.injurySites = selected;
  }

  onClearAll() {
    this.injurySites = [];
  }
}
