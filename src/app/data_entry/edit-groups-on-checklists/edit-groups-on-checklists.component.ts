import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder
} from "@angular/forms";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { Router, RoutesRecognized } from "@angular/router";
import { EditgroupchecklistService } from "src/app/_services/editgroupchecklist.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "../../_services/authentication.service";
import { filter, pairwise } from "rxjs/operators";

declare var $: any;

@Component({
  selector: "app-edit-groups-on-checklists",
  templateUrl: "./edit-groups-on-checklists.component.html",
  styleUrls: ["./edit-groups-on-checklists.component.css"],
  providers: [ConfirmationService, MessageService]
})
export class EditGroupsOnChecklistsComponent implements OnInit {
  //  Variable Declaration
  show_advance_search: boolean = false;
  groupCheckCols: any[];
  groupCheckCount: number;
  searchBy: any = [];
  allUsers: any[];
  userListing: any[];
  selectedUser: any = [];
  selectedField: string = "USERNAME";
  searchText: any;
  filterRole: any = [];
  filterSites: any = [];
  filterStatus: any = [];
  filterUserLevel: any = [];
  userSites: any = [];
  userLevel: any = [];
  userRole: any = [];
  userStatus: any = [];
  isLoaded: boolean;
  iconType: string = "ui-icon-add";
  parameters: any;
  previousUrl: any;
  userId: any;
  // Breadcrumb Service
  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    // private authService: AuthService,
    private route: Router,
    private editgroupchecklistservice: EditgroupchecklistService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLDATAENTRY",
        url: "./assets/help/edit-groups-on-checklists-listing.md"
      },
      {
        label: "LBLOBSCHKLISTGROUPS",
        routerLink: ["/edit-groups-on-checklists"]
      }
    ]);
  }

  ngOnInit() {
    this.userId = this.auth.UserInfo["userId"];
    this.route.events
      .pipe(
        filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((e: any) => {
        var url = e[0].urlAfterRedirects; // previous url
        localStorage.setItem("previousUrl-" + this.userId, url);
      });
    this.checkFilter();

    //Table Header
    this.groupCheckCols = [
      { field: "USERNAME", header: "LBLUSERNAME" },
      { field: "LASTNAME", header: "LBLLASTNAME" },
      { field: "FIRSTNAME", header: "LBLFIRSTNAME" },
      { field: "EMAIL", header: "LBLEMAILID" },
      { field: "STATUS", header: "LBLSTATUS" }
    ];

    //  user count
    this.groupCheckCount = this.groupCheckCols.length;

    // Search List
    this.groupCheckCols.map((item, key) => {
      this.translate.get(item.header).subscribe((text: string) => {
        if (key < 4) {
          this.searchBy.push({
            field: item.field,
            header: text
          });
        }
      });
      this.selectedField = "USERNAME";
    });
  }

  checkFilter() {
    this.parameters = {};
    this.previousUrl = localStorage.getItem("previousUrl-" + this.userId);
    if (this.previousUrl) {
      if (this.previousUrl.search("/edit-groups-checklists/") != -1) {
        if (localStorage.getItem("filterInfo")) {
          this.toggle();
          // this.animationState = "in";

          this.parameters = JSON.parse(localStorage.getItem("filterInfo"));
          var keys = Object.keys(this.parameters);
          keys.map(param => {
            if (this.parameters[param]) {
              if (param == "siteId") {
                this.userSites = this.parameters[param].split(",").map(Number);
              }
              if (param == "roleId") {
                this.userRole = this.parameters[param].split(",").map(Number);
              }
              if (param == "userType") {
                this.userLevel = this.parameters[param].split(",").map(Number);
              }
              if (param == "statusId") {
                this.userStatus = this.parameters[param].split(",").map(Number);
              }
            }
          });
          localStorage.removeItem("previousUrl-" + this.userId);
        }
      } else {
        localStorage.removeItem("filterInfo");
      }
    } else {
      localStorage.removeItem("filterInfo");
    }

    this.getGroupChecklist(this.parameters);
  }

  //Get Group Checklist
  getGroupChecklist(parameters) {
    this.editgroupchecklistservice
      .getGroupChecklist(parameters)
      .subscribe(res => {
        if (res["status"] == true) {
          this.allUsers = res["GeneralData"];
          this.userListing = res["GeneralData"];
          // this.filterSites = res["filterSites"];
          this.filterSites = [];
          var siteList = res["filterSites"];
          siteList.map(data => {
            this.filterSites.push({
              label: data["SITENAME"],
              value: data["SITEID"]
            });
          });

          // this.filterRole = res["filterRole"];
          this.filterRole = [];
          var roleList = res["filterRole"];
          roleList.map(data => {
            this.filterRole.push({
              label: data["ROLENAME"],
              value: data["ROLEID"]
            });
          });

          this.filterStatus = [];
          var filterStatusVr = res["filterStatus"];
          filterStatusVr.map((item, key) => {
            this.translate.get(item.VALUE).subscribe((text: string) => {
              this.filterStatus.push({
                value: item.VALUEID,
                label: text
              });
            });
          });

          this.filterUserLevel = [];
          var filterUserLevelVr = res["filterUserLevel"];
          filterUserLevelVr.map((item, key) => {
            this.translate.get(item.VALUE).subscribe((userLevel: string) => {
              this.filterUserLevel.push({
                value: item.VALUEID,
                label: userLevel
              });
            });
          });
        } else {
          this.allUsers = [];
          this.userListing = [];
        }
        if (this.userSites) {
          this.filterSites = this.auth.rearrangeSelects(
            this.filterSites,
            this.userSites
          );
        }
        if (this.userLevel) {
          this.filterUserLevel = this.auth.rearrangeSelects(
            this.filterUserLevel,
            this.userLevel
          );
        }
        if (this.userRole) {
          this.filterRole = this.auth.rearrangeSelects(
            this.filterRole,
            this.userRole
          );
        }
        if (this.userStatus) {
          this.filterStatus = this.auth.rearrangeSelects(
            this.filterStatus,
            this.userStatus
          );
        }
        this.isLoaded = true;
      });
  }

  filtering() {
    var siteId = "";
    var roleId = "";
    var userType = "";
    var statusId = "";
    if (this.userSites) {
      siteId = this.userSites.join();
    }
    if (this.userLevel) {
      userType = this.userLevel.join();
    }
    if (this.userRole) {
      roleId = this.userRole.join();
    }
    if (this.userStatus) {
      statusId = this.userStatus.join();
    }
    var parameters = {
      siteId: siteId,
      roleId: roleId,
      userType: userType,
      statusId: statusId
    };

    localStorage.removeItem("filterInfo");
    localStorage.setItem("filterInfo", JSON.stringify(parameters));
    this.getGroupChecklist(parameters);
  }

  clearFilter() {
    this.userSites = "";
    this.userLevel = "";
    this.userRole = "";
    this.userStatus = "";
    this.searchText = "";
    this.getGroupChecklist({});
  }

  // searching data
  searching() {
    var field = this.selectedField;
    var stext = this.searchText.toLowerCase();
    var fn = this;
    if (stext != "") {
      var newList = this.allUsers.filter(item => {
        return item[field].toLowerCase().indexOf(stext) >= 0;
      });
      this.userListing = newList;
      setTimeout(function () { }, 10);
    } else {
      setTimeout(function () { }, 10);
      this.userListing = this.allUsers;
    }
  }

  deleteRecord() {
    var msg = "";
    this.translate.get("ALTDELCONFIRMUSERS").subscribe(confirmMsg => {
      msg = confirmMsg;
    });
    this.confirmationService.confirm({
      message: msg,
      accept: () => { }
    });
  }

  changeStatus() {
    var msg = "";
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe(confirmMsg => {
      msg = confirmMsg;
    });
    this.confirmationService.confirm({
      message: msg,
      accept: () => { }
    });
  }
  toggle() {
    $("#slideDwon").slideToggle();
  }
  // Advance serach
  advance_search() {
    this.iconType =
      this.iconType === "ui-icon-add" ? "ui-icon-remove" : "ui-icon-add";
    this.show_advance_search = !this.show_advance_search;
  }

  onSelectAll() {
    const selected = this.filterSites.map(item => item.SITEID);
    this.userSites = selected;
  }

  onClearAll() {
    this.userSites = [];
  }

  onSelectAllUser() {
    const selectedUser = this.filterUserLevel.map(item => item.VALUEID);
    this.userLevel = selectedUser;
  }

  onClearAllUser() {
    this.userLevel = [];
  }

  onSelectAllRole() {
    const selectedRole = this.filterRole.map(item => item.ROLEID);
    this.userRole = selectedRole;
  }

  onClearAllRole() {
    this.userRole = [];
  }

  onSelectAllStatus() {
    const selectedStatus = this.filterStatus.map(item => item.VALUEID);
    this.userStatus = selectedStatus;
  }

  onClearAllStatus() {
    this.userStatus = [];
  }
}
