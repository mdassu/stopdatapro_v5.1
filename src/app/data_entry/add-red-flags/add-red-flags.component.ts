import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";

import { Router } from "@angular/router";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { RedFlagService } from "src/app/_services/red-flag.service";
import { CustomValidatorsService } from "../../_services/custom-validators.service";

import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "../../_services/authentication.service";

@Component({
  selector: "app-add-red-flags",
  templateUrl: "./add-red-flags.component.html",
  styleUrls: ["./add-red-flags.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class AddRedFlagsComponent implements OnInit {
  // Varialbe for fields name
  addRedFlag: FormGroup;
  submitted: boolean;

  siteListing: SelectItem[];
  obsListing: SelectItem[];
  areaListing: SelectItem[];
  subAreaListing: SelectItem[];
  checkListing: SelectItem[];
  catListing: SelectItem[];
  subCatListing: SelectItem[];
  metrics: SelectItem[];
  triggerList: any = [];
  trigger: any = [];
  status: SelectItem[];
  checkListSelectedId: any;

  initialGeneralFormData: any;
  subAreaOptionValue: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private redflagservice: RedFlagService,
    private router: Router,
    private cookieService: CookieService,
    private auth: AuthenticationService,
    private elementRef: ElementRef,
    private customValidatorsService: CustomValidatorsService,
    private translate: TranslateService
  ) {
    this.translate.get(["LBLDATAENTRY", "LBLREDFLAG"]).subscribe((label) => {
      this.breadcrumbService.setItems([
        {
          label: label["LBLDATAENTRY"],
          url: "./assets/help/red-flag-listing.md",
        },
        { label: label["LBLREDFLAG"], routerLink: ["/red-flags-listing"] },
      ]);
    });
  }

  ngOnInit() {
    // Add Observatin General
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.addRedFlag = this.fb.group({
      siteId: new FormControl(null, Validators.required),
      flagName: new FormControl(
        null,
        Validators.compose([
          Validators.required,
          Validators.maxLength(10),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),
      observeId: new FormControl(null, Validators.required),
      areaId: new FormControl(null, Validators.required),
      subareaId: new FormControl(null, Validators.required),
      checkListId: new FormControl(0, [Validators.required, Validators.min(1)]),
      mainCatID: new FormControl(null, Validators.required),
      subCatID: new FormControl(null, Validators.required),
      matric: new FormControl(1, Validators.required),
      trigger: new FormControl(1, Validators.required),
      limit: new FormControl(
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.maxLength(5),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),
      status: new FormControl(1, Validators.required),
    });

    this.getRedFlagData();

    this.metrics = [];
    this.metrics.push({ label: "Safe", value: 1 });
    this.metrics.push({ label: "Unsafe", value: 2 });

    this.triggerList.push({ label: "LBLGREATERNO", value: 1 });
    this.triggerList.push({ label: "LBLLESSERNO", value: 2 });
    this.triggerList.push({ label: "LBLGREATERPERCENTAGE", value: 3 });
    this.triggerList.push({ label: "LBLLESSERPERCENTAGE", value: 4 });

    this.triggerList.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.trigger.push({
          value: item.value,
          label: text,
        });
      });
    });

    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((resLabel) => {
      this.status = [];
      this.status.push({ label: resLabel["LBLACTIVE"], value: 1 });
      this.status.push({ label: resLabel["LBLINACTIVE"], value: 0 });
    });
  }

  getRedFlagData() {
    this.redflagservice.redFlagAddData().subscribe((res) => {
      if (res["status"] == true) {
        this.translate.get("LBLSELECT").subscribe((text: string) => {
          var siteSelect;
          siteSelect = {
            SITENAME: text,
            SITEID: -1,
          };
          this.siteListing = res["sites"];
          this.siteListing.unshift(siteSelect);
        });
        var obsListings;
        obsListings = {
          FULLNAME: "ALL",
          USERID: -1,
        };
        this.obsListing = res["observer"];
        this.obsListing.unshift(obsListings);

        var areaAll;
        areaAll = {
          AREANAME: "ALL",
          AREAID: -1,
        };
        this.areaListing = res["area"];
        this.areaListing.unshift(areaAll);

        var subAreaAll;
        subAreaAll = {
          SUBAREANAME: "ALL",
          SUBAREAID: -1,
        };
        this.subAreaListing = res["subarea"];
        this.subAreaListing.unshift(subAreaAll);

        this.translate.get("LBLSELECT").subscribe((text: string) => {
          var checklistSelect;
          checklistSelect = {
            SETUPNAME: text,
            CHECKLISTSETUPID: 0,
          };
          this.checkListing = res["checklist"];
          this.checkListing.unshift(checklistSelect);
        });

        var mainCatAll;
        mainCatAll = {
          MAINCATEGORYNAME: "ALL",
          MAINCATEGORYID: -1,
        };
        this.catListing = res["maincat"];
        this.catListing.unshift(mainCatAll);

        var subCatAll;
        subCatAll = {
          SUBCATEGORYNAME: "ALL",
          SUBCATEGORYID: -1,
        };
        this.subCatListing = res["subcat"];
        this.subCatListing.unshift(subCatAll);

        this.addRedFlag.controls["siteId"].setValue(res["defaultSiteId"]);
        this.addRedFlag.controls["observeId"].setValue(
          this.obsListing[0]["USERID"]
        );
        this.addRedFlag.controls["areaId"].setValue(
          this.areaListing[0]["AREAID"]
        );
        this.addRedFlag.controls["subareaId"].setValue(
          this.subAreaListing[0]["SUBAREAID"]
        );
        this.addRedFlag.controls["mainCatID"].setValue(
          this.catListing[0]["MAINCATEGORYID"]
        );
        this.addRedFlag.controls["subCatID"].setValue(
          this.subCatListing[0]["SUBCATEGORYID"]
        );
        this.initialGeneralFormData = this.addRedFlag.getRawValue();
      }
    });
  }

  getSiteData(siteId) {
    var parameters = {
      siteId: siteId,
    };
    this.redflagservice.getSiteData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var areaAll;
        areaAll = {
          AREANAME: "ALL",
          AREAID: -1,
        };
        this.areaListing = res["area"];
        this.areaListing.unshift(areaAll);

        var obsListings;
        obsListings = {
          FULLNAME: "ALL",
          USERID: -1,
        };
        this.obsListing = res["observer"];
        this.obsListing.unshift(obsListings);
        this.checkListing = res["checklist"];
        this.addRedFlag.controls["observeId"].setValue(-1);
        this.addRedFlag.controls["areaId"].setValue(-1);
        this.addRedFlag.controls["checkListId"].setValue(null);
        this.addRedFlag.controls["subareaId"].setValue(-1);
        this.addRedFlag.controls["mainCatID"].setValue(-1);
        this.addRedFlag.controls["subCatID"].setValue(-1);
      }
    });
  }

  getSubAreaData(areaId) {
    var parameters = {
      areaId: areaId,
    };
    this.redflagservice.getAreaData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var subAreaAll;
        subAreaAll = {
          SUBAREANAME: "ALL",
          SUBAREAID: -1,
        };
        this.subAreaListing = res["subArea"];
        this.subAreaListing.unshift(subAreaAll);
        this.addRedFlag.controls["subareaId"].setValue(
          this.subAreaListing[0]["SUBAREAID"]
        );
      } else {
        var subAreaAll;
        subAreaAll = {
          SUBAREANAME: "ALL",
          SUBAREAID: -1,
        };
        this.subAreaListing = [];
        this.subAreaListing.unshift(subAreaAll);
        this.addRedFlag.controls["subareaId"].setValue(
          this.subAreaListing[0]["SUBAREAID"]
        );
      }
    });
  }

  getCheckListData(checklistId) {
    var parameters = {
      checklistId: checklistId,
    };
    this.redflagservice.getCheckListData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var mainCatAll;
        mainCatAll = {
          MAINCATEGORYNAME: "ALL",
          MAINCATEGORYID: -1,
        };
        this.catListing = res["mainCat"];
        this.catListing.unshift(mainCatAll);

        var subCatAll;
        subCatAll = {
          SUBCATEGORYNAME: "ALL",
          SUBCATEGORYID: -1,
        };
        this.subCatListing = res["subCat"];
        this.subCatListing.unshift(subCatAll);

        this.checkListSelectedId = checklistId;
        this.addRedFlag.controls["mainCatID"].setValue(
          this.catListing[0]["MAINCATEGORYID"]
        );
        this.addRedFlag.controls["subCatID"].setValue(
          this.subCatListing[0]["SUBCATEGORYID"]
        );
      } else {
        var mainCatAll;
        mainCatAll = {
          MAINCATEGORYNAME: "ALL",
          MAINCATEGORYID: -1,
        };
        this.catListing = [];
        this.catListing.unshift(mainCatAll);

        var subCatAll;
        subCatAll = {
          SUBCATEGORYNAME: "ALL",
          SUBCATEGORYID: -1,
        };
        this.subCatListing = [];
        this.subCatListing.unshift(subCatAll);
        this.checkListSelectedId = checklistId;
      }
    });
  }

  getMainCatData(mainCatId) {
    var parameters = {
      mainCatId: mainCatId,
      checklistId: this.checkListSelectedId,
    };

    var tempCat = this.subCatListing.filter((cat) => {
      return cat["MAINCATEGORYID"] == mainCatId;
    });
    var subCatAll;
    subCatAll = {
      SUBCATEGORYNAME: "ALL",
      SUBCATEGORYID: -1,
    };
    this.subCatListing = tempCat;
    this.subCatListing.unshift(subCatAll);
    this.addRedFlag.controls["subCatID"].setValue(
      this.subCatListing[0]["SUBCATEGORYID"]
    );

    this.redflagservice.getMainCatData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var subCatAll;
        subCatAll = {
          SUBCATEGORYNAME: "ALL",
          SUBCATEGORYID: -1,
        };
        this.subCatListing = res["subCat"];
        this.subCatListing.unshift(subCatAll);
        this.addRedFlag.controls["subCatID"].setValue(
          this.subCatListing[0]["SUBCATEGORYID"]
        );
      } else {
        var subCatAll;
        subCatAll = {
          SUBCATEGORYNAME: "ALL",
          SUBCATEGORYID: -1,
        };
        this.subCatListing = [];
        this.subCatListing.unshift(subCatAll);
        this.addRedFlag.controls["subCatID"].setValue(
          this.subCatListing[0]["SUBCATEGORYID"]
        );
      }
    });
  }

  get redFlagForm() {
    return this.addRedFlag.controls;
  }

  onSubmit(value: string) {
    this.submitted = true;
    if (this.addRedFlag.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.redflagservice
        .insertRedFlag(this.addRedFlag.value)
        .subscribe((res) => {
          this.translate.get(res["message"]).subscribe((message) => {
            if (res["status"] == true) {
              // this.messageService.add({
              //   severity: "success",
              //   detail: message
              // });
              this.cookieService.set("add-new-red", message);
              this.router.navigate(["./edit-red-flags/" + res["id"]], {
                skipLocationChange: true,
              });
            } else {
              this.messageService.add({
                severity: "error",
                detail: message,
              });
            }
          });
        });
    }
  }

  resetRedFlagForm() {
    this.addRedFlag.patchValue(this.initialGeneralFormData);
  }
}
