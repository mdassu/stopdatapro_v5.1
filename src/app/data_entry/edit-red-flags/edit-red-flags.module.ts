import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { EditRedFlagsComponent } from "./edit-red-flags.component";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "../../_guards/auth.guard";

import { TranslateLoader, TranslateModule } from "@ngx-translate/core";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "assets/i18n/");
}

import { CardModule } from "primeng/card";
import { TableModule } from "primeng/table";
import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { DataViewModule } from "primeng/dataview";
import { DropdownModule } from "primeng/dropdown";
import { InputSwitchModule } from "primeng/inputswitch";
import { InputTextModule } from "primeng/inputtext";
import { InputTextareaModule } from "primeng/inputtextarea";
import { MessageModule } from "primeng/message";
import { MessagesModule } from "primeng/messages";
import { MultiSelectModule } from "primeng/multiselect";
import { OverlayPanelModule } from "primeng/overlaypanel";
import {
  AccordionModule,
  CalendarModule,
  DialogModule,
  FileUploadModule,
  GrowlModule,
  PanelModule,
  PickListModule,
  ScrollPanelModule,
  TabViewModule,
  TreeModule,
  TooltipModule,
  ProgressSpinnerModule
} from "primeng/primeng";
import { RadioButtonModule } from "primeng/radiobutton";
import { ToastModule } from "primeng/toast";
import { TreeTableModule } from "primeng/treetable";
import { ChartModule } from "primeng/chart";
import { HttpClient } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { NgSelectModule } from "@ng-select/ng-select";

let routes: Routes = [
  {
    path: "",
    component: EditRedFlagsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [EditRedFlagsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    TableModule,
    ButtonModule,
    CheckboxModule,
    ConfirmDialogModule,
    DataViewModule,
    DropdownModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    MessageModule,
    MessagesModule,
    MultiSelectModule,
    OverlayPanelModule,
    AccordionModule,
    CalendarModule,
    DialogModule,
    FileUploadModule,
    GrowlModule,
    PanelModule,
    PickListModule,
    ScrollPanelModule,
    TabViewModule,
    TreeModule,
    TooltipModule,
    ProgressSpinnerModule,
    RadioButtonModule,
    ToastModule,
    TreeTableModule,
    ChartModule,
    NgSelectModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forChild(routes)
  ]
})
export class EditRedFlagsModule {}
