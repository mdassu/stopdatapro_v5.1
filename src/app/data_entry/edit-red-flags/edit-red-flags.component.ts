import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";

import { Router, ActivatedRoute } from "@angular/router";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { RedFlagService } from "src/app/_services/red-flag.service";
import { CustomValidatorsService } from "../../_services/custom-validators.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "../../_services/authentication.service";

@Component({
  selector: "app-edit-red-flags",
  templateUrl: "./edit-red-flags.component.html",
  styleUrls: ["./edit-red-flags.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class EditRedFlagsComponent implements OnInit {
  // Varialbe for fields name
  addRedFlag: FormGroup;
  submitted: boolean;

  redFlagId: number;
  siteListing: string;
  obsListing: SelectItem[];
  areaListing: SelectItem[];
  subAreaListing: SelectItem[];
  checkListing: SelectItem[];
  catListing: SelectItem[];
  subCatListing: SelectItem[];
  metrics: SelectItem[];
  trigger: SelectItem[];
  status: SelectItem[];
  checkListSelectedId: any;
  initialGeneralFormData: any;
  isLoaded: boolean;
  subAreaOptionValue: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private redflagservice: RedFlagService,
    private router: Router,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private auth: AuthenticationService,
    private activetedRoute: ActivatedRoute,
    private customValidatorsService: CustomValidatorsService,
    private translate: TranslateService
  ) {
    this.translate.get(["LBLDATAENTRY", "LBLREDFLAG"]).subscribe((label) => {
      this.breadcrumbService.setItems([
        {
          label: label["LBLDATAENTRY"],
          url: "./assets/help/red-flag-listing.md",
        },
        { label: label["LBLREDFLAG"], routerLink: ["/red-flags-listing"] },
      ]);
    });

    if (this.cookieService.get("add-new-red")) {
      var fn = this;
      setTimeout(function () {
        fn.messageService.add({
          severity: "success",
          detail: fn.cookieService.get("add-new-red"),
        });
        fn.cookieService.delete("add-new-red");
      }, 1000);
    }
  }

  ngOnInit() {
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.isLoaded = true;
    this.redFlagId = parseInt(
      this.activetedRoute.snapshot.paramMap.get("redflagid")
    );
    this.addRedFlag = this.fb.group({
      checkListId: new FormControl(0, [Validators.required, Validators.min(1)]),
    });
    var data = {
      flagName: null,
      observeId: null,
      areaId: null,
      subareaId: null,
      checkListId: 0,
      mainCatID: null,
      subCatID: null,
      matric: 1,
      trigger: 1,
      limit: null,
      status: 1,
    };

    this.createFormBuilder(data);
    this.getRedFlagEditData();

    this.metrics = [];
    this.metrics.push({ label: "Safe", value: 1 });
    this.metrics.push({ label: "Unsafe", value: 2 });

    this.trigger = [];
    this.trigger.push({ label: "Greater #", value: 1 });
    this.trigger.push({ label: "Lesser #", value: 2 });
    this.trigger.push({ label: "Greater %", value: 3 });
    this.trigger.push({ label: "Lesser %", value: 4 });

    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((resLabel) => {
      this.status = [];
      this.status.push({ label: resLabel["LBLACTIVE"], value: 1 });
      this.status.push({ label: resLabel["LBLINACTIVE"], value: 0 });
    });
  }

  getRedFlagEditData() {
    var parameters = {
      redFlagId: this.redFlagId,
    };
    this.redflagservice.editRedFlagData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var redFlagData = res["generalData"][0];
        this.siteListing = redFlagData["SITENAME"];
        var obsListings;
        obsListings = {
          FULLNAME: "ALL",
          USERID: -1,
        };
        this.obsListing = res["observer"];
        this.obsListing.unshift(obsListings);

        var areaAll;
        areaAll = {
          AREANAME: "ALL",
          AREAID: -1,
        };
        this.areaListing = res["area"];
        this.areaListing.unshift(areaAll);

        var subAreaAll;
        subAreaAll = {
          SUBAREANAME: "ALL",
          SUBAREAID: -1,
        };
        this.subAreaListing = res["subarea"];
        this.subAreaListing.unshift(subAreaAll);

        this.translate.get("LBLSELECT").subscribe((text: string) => {
          var checklistSelect;
          checklistSelect = {
            SETUPNAME: text,
            CHECKLISTSETUPID: 0,
          };
          this.checkListing = res["checklist"];
          this.checkListing.unshift(checklistSelect);
        });

        var mainCatAll;
        mainCatAll = {
          MAINCATEGORYNAME: "ALL",
          MAINCATEGORYID: -1,
        };
        this.catListing = res["maincat"];
        this.catListing.unshift(mainCatAll);

        var subCatAll;
        subCatAll = {
          SUBCATEGORYNAME: "ALL",
          SUBCATEGORYID: -1,
        };
        this.subCatListing = res["subcat"];
        this.subCatListing.unshift(subCatAll);

        var data = {
          flagName: redFlagData["REDFLAGNAME"].toString(),
          observeId: redFlagData["OBSERVERID"],
          areaId: redFlagData["AREAID"],
          subareaId: redFlagData["SUBAREAID"],
          checkListId: parseInt(redFlagData["CHECKLISTSETUPID"]),
          mainCatID: redFlagData["MAINCATEGORYID"],
          subCatID: redFlagData["SUBCATEGORYID"],
          matric: redFlagData["METRIC"],
          trigger: redFlagData["TRIGGER"],
          limit: redFlagData["VALUE"].toString(),
          status: redFlagData["STATUS"],
        };

        this.createFormBuilder(data);
      }
      this.isLoaded = false;
    });
  }

  createFormBuilder(data) {
    this.addRedFlag = this.fb.group({
      flagName: new FormControl(
        data.flagName,
        Validators.compose([
          Validators.required,
          Validators.maxLength(10),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),
      observeId: new FormControl(data.observeId, Validators.required),
      areaId: new FormControl(data.areaId, Validators.required),
      subareaId: new FormControl(data.subareaId, Validators.required),
      checkListId: new FormControl(data.checkListId, [
        Validators.required,
        Validators.min(1),
      ]),
      mainCatID: new FormControl(data.mainCatID, Validators.required),
      subCatID: new FormControl(data.subCatID, Validators.required),
      matric: new FormControl(data.matric, Validators.required),
      trigger: new FormControl(data.trigger, Validators.required),
      limit: new FormControl(
        data.limit,
        Validators.compose([
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.maxLength(5),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),
      status: new FormControl(data.status, Validators.required),
    });
  }

  getSubAreaData(areaId) {
    var parameters = {
      areaId: areaId,
    };
    this.redflagservice.getAreaData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var subAreaAll;
        subAreaAll = {
          SUBAREANAME: "ALL",
          SUBAREAID: -1,
        };
        this.subAreaListing = res["subArea"];
        this.subAreaListing.unshift(subAreaAll);
        this.addRedFlag.controls["subareaId"].setValue(
          this.subAreaListing[0]["SUBAREAID"]
        );
      } else {
        this.subAreaListing = [];
        var subAreaAll;
        subAreaAll = {
          SUBAREANAME: "ALL",
          SUBAREAID: -1,
        };
        this.subAreaListing.push(subAreaAll);
        this.addRedFlag.controls["subareaId"].setValue(
          this.subAreaListing[0]["SUBAREAID"]
        );
      }
    });
  }

  getCheckListData(checklistId) {
    var parameters = {
      checklistId: checklistId,
    };
    this.redflagservice.getCheckListData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var mainCatAll;
        mainCatAll = {
          MAINCATEGORYNAME: "ALL",
          MAINCATEGORYID: -1,
        };
        this.catListing = res["mainCat"];
        this.catListing.unshift(mainCatAll);

        var subCatAll;
        subCatAll = {
          SUBCATEGORYNAME: "ALL",
          SUBCATEGORYID: -1,
        };
        this.subCatListing = res["subCat"];
        this.subCatListing.unshift(subCatAll);

        this.checkListSelectedId = checklistId;
        this.addRedFlag.controls["mainCatID"].setValue(
          this.catListing[0]["MAINCATEGORYID"]
        );
        this.addRedFlag.controls["subCatID"].setValue(
          this.subCatListing[0]["SUBCATEGORYID"]
        );
      }
    });
  }

  getMainCatData(mainCatId) {
    var parameters = {
      mainCatId: mainCatId,
      checklistId: this.checkListSelectedId,
    };
    this.redflagservice.getMainCatData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var subCatAll;
        subCatAll = {
          SUBCATEGORYNAME: "ALL",
          SUBCATEGORYID: -1,
        };
        this.subCatListing = res["subCat"];
        this.subCatListing.unshift(subCatAll);
        this.addRedFlag.controls["subCatID"].setValue(
          this.subCatListing[0]["SUBCATEGORYID"]
        );
      } else {
        this.subCatListing = [];

        var subCatAll;
        subCatAll = {
          SUBCATEGORYNAME: "ALL",
          SUBCATEGORYID: -1,
        };
        this.subCatListing.push(subCatAll);
        this.addRedFlag.controls["subCatID"].setValue(
          this.subCatListing[0]["SUBCATEGORYID"]
        );
      }
    });
  }

  get redFlagForm() {
    return this.addRedFlag.controls;
  }

  onSubmit(value: string) {
    this.submitted = true;
    if (this.addRedFlag.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.isLoaded = true;
      this.addRedFlag.value["redId"] = this.redFlagId;
      this.addRedFlag.value["limit"] = parseInt(this.addRedFlag.value["limit"]);
      this.redflagservice
        .updateRedFlag(this.addRedFlag.value)
        .subscribe((res) => {
          if (res["status"] == true) {
            this.submitted = false;
            // this.getInjuryAddData();
            this.addRedFlag.markAsPristine();
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.isLoaded = false;
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
            this.isLoaded = false;
          }
        });
    }
  }

  resetRedFlagForm() {
    this.getRedFlagEditData();
    // this.addRedFlag.patchValue(this.initialGeneralFormData);
  }
}
