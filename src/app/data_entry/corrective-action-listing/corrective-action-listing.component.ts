import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import { CloneVisitor } from "@angular/compiler/src/i18n/i18n_ast";
import { formatDate } from "@angular/common";
import {
  trigger,
  transition,
  style,
  animate,
  query,
  stagger,
} from "@angular/animations";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { Router, RoutesRecognized } from "@angular/router";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService, SortEvent } from "primeng/api";

import { CorractiveActionsService } from "../../_services/corractive-actions.service";
import { AuthenticationService } from "../../_services/authentication.service";
import { SlideUpDownAnimations } from "../../_animations/slide-up-down.animations";
import { isArray } from "util";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
import { ActivatedRoute } from "@angular/router";
import { filter, pairwise } from "rxjs/operators";
import * as moment from "moment";
declare var $: any;

@Component({
  selector: "app-corrective-action-listing",
  templateUrl: "./corrective-action-listing.component.html",
  styleUrls: ["./corrective-action-listing.component.css"],
  providers: [ConfirmationService, MessageService],
  animations: [SlideUpDownAnimations],
})
export class CorrectiveActionListingComponent implements OnInit {
  //  Variable Declaration
  colsCount: any;
  allCorrectiveList: any;
  correctiveDataList: any;
  correctiveDataCols: any[];
  selectedDelCorrective: any = [];
  searchBy: any = [];
  selectedField: any = "CARDID";
  searchText: any = "";
  isLoaded: boolean;
  defaultParameters: any = [];
  filterData: any[];
  optionalFilter: any[];
  siteFilter: any[];
  areaFilter: any[];
  subAreaFilter: any;
  shiftFilter: any[];
  public showAdvanceSarch: boolean = false;
  iconType: string = "ui-icon-add";
  selectedFilter: any = [];
  curDate = new Date();
  selectedSite: any = [];
  selectedArea: any = [];
  selectedSubArea: any = [];
  selectedShifts: any = [];
  actionDueDate: any;
  respondedDate: any;
  currDate: any = new Date();
  selectedOptionlFilter: any = [];
  animationState: any = "out";
  confirmClass: any;
  appName: any;
  roleId: any;
  dateFormat: any;
  dateFormatType: any;
  caType: any;
  parameters: any;
  previousUrl: any;
  subAreaOptionValue: any;
  actionDate: any;
  respondedDueDate: any;
  userId: any;
  yearList: any;
  actionFromDate: any;
  actionToDate: any;
  respondedFromDate: any;
  respondedToDate: any;
  viewActionFromDate: any;
  viewActionToDate: any;
  selecetedActionDateFilter: boolean;
  selecetedRespondedDateFilter: boolean;
  viewRespondedFromDate: any;
  viewRespondedToDate: any;
  locale: any;
  // Breadcrumb Service
  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private CorractiveActions: CorractiveActionsService,
    private auth: AuthenticationService,
    private route: Router,
    private env: EnvService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLDATAENTRY",
        url: "./assets/help/corrective-actions-listing.md",
      },
      { label: "LBLCORRECTIVEACTION" },
    ]);
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    this.userId = this.auth.UserInfo["userId"];
    // Table header
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.route.events
      .pipe(
        filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((e: any) => {
        var url = e[0].urlAfterRedirects; // previous url
        localStorage.setItem("previousUrl-" + this.userId, url);
      });
    this.caType = this.activatedRoute.snapshot.paramMap.get("caType");

    this.roleId = this.auth.UserInfo.roleId;
    this.correctiveDataCols = [
      { field: "CARDID", header: "LBLCHECKLISTNO" },
      { field: "CAID", header: "LBLCAID" },
      { field: "OBSERVATIONDATE", header: "LBLOBSDATE" },
      { field: "ACTIONOWNER", header: "LBLACTOWNER" },
      { field: "AREA", header: "LBLAREA" },
      { field: "PRIORITY", header: "LBLPRIORITY" },
      { field: "RESPONSEREQUIREDDATE", header: "LBLDUEDATELISTING" },
      { field: "CASTATUS", header: "LBLCASTATUS" },
    ];

    var previousYear = this.currDate.getFullYear() - 10;
    var nextYear = this.currDate.getFullYear() + 10;
    this.yearList = previousYear + ":" + nextYear;

    //  col of status
    this.colsCount = this.correctiveDataCols.length;
    this.correctiveDataCols.map((item, key) => {
      this.translate.get(item.header).subscribe((text: string) => {
        this.searchBy.push({ field: item.field, header: text });
      });
      this.selectedField = "CARDID";
    });
    if (this.caType == "") {
      this.caType = 0;
    }
    // this.defaultParameters = {
    //   // siteId: this.auth.UserInfo["defaultSiteId"],
    //   clearFilter: 0,
    //   caType: this.caType
    // };

    this.dateFormat = this.auth.UserInfo["dateFormat"].toLowerCase();
    this.dateFormatType = this.dateFormat.replace("mm", "MM");
    this.dateFormat = this.dateFormat.replace("yyyy", "yy");

    this.checkFilter();
    this.appName = this.env.appName;
  }

  checkFilter() {
    this.parameters = {
      // siteId: this.auth.UserInfo["defaultSiteId"],
      clearFilter: 0,
      caType: this.caType,
    };
    this.previousUrl = localStorage.getItem("previousUrl-" + this.userId);
    if (this.previousUrl) {
      if (
        this.previousUrl.search("/add-corrective-actions") != -1 ||
        this.previousUrl.search("/edit-corrective-action-listing/") != -1
      ) {
        if (localStorage.getItem("filterInfo")) {
          this.toggle();
          // this.animationState = "in";
          this.parameters = JSON.parse(localStorage.getItem("filterInfo"));
          var keys = Object.keys(this.parameters);
          keys.map((param) => {
            if (this.parameters[param]) {
              if (param == "filSiteId") {
                this.selectedSite = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "filAreaId") {
                this.selectedArea = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "filSubAreaId") {
                this.selectedSubArea = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "filShiftId") {
                this.selectedShifts = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "filRespPerson") {
                this.selectedOptionlFilter["filRespPerson"] = this.parameters[
                  param
                ]
                  .split(",")
                  .map(Number);
              }
              if (param == "filActionOwner") {
                this.selectedOptionlFilter["filActionOwner"] = this.parameters[
                  param
                ]
                  .split(",")
                  .map(Number);
              }
              if (param == "filResDateReqFrom") {
                this.actionFromDate = new Date(this.parameters[param]);
              }
              if (param == "filResDateReqTo") {
                this.actionToDate = new Date(this.parameters[param]);
              }
              if (param == "filRespondedFrom") {
                this.respondedFromDate = new Date(this.parameters[param]);
              }
              if (param == "filRespondedTo") {
                this.respondedToDate = new Date(this.parameters[param]);
              }
              this.actionDateSelected();
              this.respondedDateSelected();
              if (param == "filCardStatusId") {
                this.selectedOptionlFilter["filCardStatusId"] = this.parameters[
                  param
                ]
                  .split(",")
                  .map(Number);
              }
              if (param == "filCorractiveType") {
                this.selectedOptionlFilter[
                  "filCorractiveType"
                ] = this.parameters[param].split(",").map(Number);
              }
              if (param.search("group") != -1) {
                this.selectedFilter[param] = this.parameters[param]
                  .split(",")
                  .map(Number);
              }

              // this.selectedFilter[param] = this.parameters[param]
              //   .split(",")
              //   .map(Number);
            }
          });
          localStorage.removeItem("previousUrl-" + this.userId);
        } else {
          localStorage.removeItem("filterInfo");
          this.selectedSite = [];
          this.selectedSite.push(this.auth.UserInfo["defaultSiteId"]);
        }
      } else {
        localStorage.removeItem("filterInfo");
        this.selectedSite = [];
        this.selectedSite.push(this.auth.UserInfo["defaultSiteId"]);
      }
    } else {
      localStorage.removeItem("filterInfo");
      this.selectedSite = [];
      this.selectedSite.push(this.auth.UserInfo["defaultSiteId"]);
    }

    this.getCorrectiveActionList(this.parameters);
  }

  getCorrectiveActionList(defaultParameters) {
    this.CorractiveActions.getCorrectiveActionList(defaultParameters).subscribe(
      (res) => {
        if (res["status"] == true) {
          var filterData = res["filters"];
          var siteFilter = res["siteFilters"];
          var areaFilter = res["areaFilters"];
          var subAreaFilter = res["subAreafilters"];
          var shiftFilter = res["shiftfilters"];
          var optionalFilter = res["optionalFilters"];
          this.setFilters(
            siteFilter,
            areaFilter,
            subAreaFilter,
            shiftFilter,
            filterData
          );
          this.setOptionalFilters(optionalFilter);
          if (res["corractiveAction"].length > 0) {
            this.allCorrectiveList = res["corractiveAction"];
            this.correctiveDataList = res["corractiveAction"];
            this.correctiveDataList.map((item) => {
              if (item["PRIORITY"]) {
                this.translate.get(item["PRIORITY"]).subscribe((label) => {
                  item["PRIORITY"] = label;
                });
              }
            });
          } else {
            this.correctiveDataList = [];
          }
          this.isLoaded = true;
        } else {
          this.isLoaded = true;
        }
      }
    );
  }

  setFilters(sites, areas, subareas, shifts, filterdata) {
    sites["Data"] = JSON.parse(sites["Data"]);
    // this.siteFilter = sites;

    this.siteFilter = [];
    var siteListVr = sites["Data"];
    siteListVr.map((data) => {
      this.siteFilter.push({
        label: data["SITENAME"],
        value: data["SITEID"],
      });
    });
    areas["Data"] = JSON.parse(areas["Data"]);
    // this.areaFilter = areas;

    this.areaFilter = [];
    var areaListVr = areas["Data"];
    areaListVr.map((data) => {
      this.areaFilter.push({
        label: data["AREANAME"],
        value: data["AREAID"],
      });
    });
    if (this.subAreaOptionValue == 1) {
      subareas["Data"] = JSON.parse(subareas["Data"]);

      // this.subAreaFilter = subareas;

      this.subAreaFilter = [];
      var subAreaListVr = subareas["Data"];
      subAreaListVr.map((data) => {
        this.subAreaFilter.push({
          label: data["SUBAREANAME"],
          value: data["SUBAREAID"],
        });
      });
    } else {
      this.subAreaFilter = [];
    }

    shifts["Data"] = JSON.parse(shifts["Data"]);

    // this.shiftFilter = shifts;
    this.shiftFilter = [];
    var shiftListVr = shifts["Data"];
    shiftListVr.map((data) => {
      this.shiftFilter.push({
        label: data["SHIFTNAME"],
        value: data["SHIFTID"],
      });
    });

    filterdata.map((item, index) => {
      var newData = "";
      if (item["Data"]) {
        newData = JSON.parse(item["Data"]);
      }
      var tempData = [];
      if (newData.length > 0) {
        var keies = Object.keys(newData[0]);
        var value = keies[0];
        var label = keies[1];
        if (isArray(newData)) {
          newData.map((data) => {
            tempData.push({ label: data[label], value: data[value] });
          });
        } else {
        }
      }

      if (item["Name"] == "Areas") {
        item["field"] = "filAreaId";
      }
      if (item["Name"] == "Sub Areas") {
        item["field"] = "filSubAreaId";
      }
      if (item["Name"] == "Shifts") {
        item["field"] = "filShiftId";
      }
      if (
        item["Name"] != "Sites" &&
        item["Name"] != "Areas" &&
        item["Name"] != "Sub Areas" &&
        item["Name"] != "Shifts"
      ) {
        item["field"] = "group" + (index + 1);
      }
      item["Data"] = tempData;
    });
    this.filterData = filterdata;
    if (this.selectedFilter.group1) {
      this.auth.rearrangeSelect(this.filterData, this.selectedFilter.group1);
    }
    if (this.selectedFilter.group2) {
      this.auth.rearrangeSelect(this.filterData, this.selectedFilter.group2);
    }
    if (this.selectedFilter.group3) {
      this.auth.rearrangeSelect(this.filterData, this.selectedFilter.group3);
    }
    if (this.selectedFilter.group4) {
      this.auth.rearrangeSelect(this.filterData, this.selectedFilter.group4);
    }
    if (this.selectedFilter.group5) {
      this.auth.rearrangeSelect(this.filterData, this.selectedFilter.group5);
    }
    if (this.selectedFilter.group6) {
      this.auth.rearrangeSelect(this.filterData, this.selectedFilter.group6);
    }
    if (this.selectedFilter.group7) {
      this.auth.rearrangeSelect(this.filterData, this.selectedFilter.group7);
    }
    if (this.selectedFilter.group8) {
      this.auth.rearrangeSelect(this.filterData, this.selectedFilter.group8);
    }
    if (this.selectedFilter.group9) {
      this.auth.rearrangeSelect(this.filterData, this.selectedFilter.group9);
    }
    if (this.selectedFilter.group10) {
      this.auth.rearrangeSelect(this.filterData, this.selectedFilter.group10);
    }

    if (this.selectedSite) {
      this.siteFilter = this.auth.rearrangeSelects(
        this.siteFilter,
        this.selectedSite
      );
    }
    if (this.selectedArea) {
      this.areaFilter = this.auth.rearrangeSelects(
        this.areaFilter,
        this.selectedArea
      );
    }
    if (this.selectedSubArea) {
      this.subAreaFilter = this.auth.rearrangeSelects(
        this.subAreaFilter,
        this.selectedSubArea
      );
    }
    if (this.selectedShifts) {
      this.shiftFilter = this.auth.rearrangeSelects(
        this.shiftFilter,
        this.selectedShifts
      );
    }
    if (this.selectedShifts) {
      this.shiftFilter = this.auth.rearrangeSelects(
        this.shiftFilter,
        this.selectedShifts
      );
    }
  }

  // set optional filter data
  setOptionalFilters(optionalFilter) {
    optionalFilter.map((item) => {
      var newData = JSON.parse(item["Data"]);
      var tempData = [];
      if (newData.length > 0) {
        var keies = Object.keys(newData[0]);
        var value = keies[0];
        var label = keies[1];
      }
      newData.map((data) => {
        var newLabel;
        this.translate.get(data[label]).subscribe((res) => {
          newLabel = res;
        });
        tempData.push({ label: newLabel, value: data[value] });
      });
      if (item["Name"] == "LBLCARDSTATUS") {
        item["field"] = "filCardStatusId";
      }
      if (item["Name"] == "LBLRESPONSIBLEPERSON") {
        item["field"] = "filRespPerson";
      }
      if (item["Name"] == "LBLCORRACTIVEACTIONTYPE") {
        item["field"] = "filCorractiveType";
      }
      if (item["Name"] == "LBLACTOWNER") {
        item["field"] = "filActionOwner";
      }
      item["Data"] = tempData;
    });
    this.optionalFilter = optionalFilter;
    if (this.selectedOptionlFilter.filCardStatusId) {
      this.auth.rearrangeSelect(
        this.optionalFilter,
        this.selectedOptionlFilter.filCardStatusId
      );
    }
    if (this.selectedOptionlFilter.filRespPerson) {
      this.auth.rearrangeSelect(
        this.optionalFilter,
        this.selectedOptionlFilter.filRespPerson
      );
    }
    if (this.selectedOptionlFilter.filCorractiveType) {
      this.auth.rearrangeSelect(
        this.optionalFilter,
        this.selectedOptionlFilter.filCorractiveType
      );
    }
    if (this.selectedOptionlFilter.filActionOwner) {
      this.auth.rearrangeSelect(
        this.optionalFilter,
        this.selectedOptionlFilter.filActionOwner
      );
    }
  }

  clearFilter() {
    this.selectedFilter = [];
    this.selectedOptionlFilter = [];
    this.selectedArea = [];
    this.selectedSite = [];
    this.selectedShifts = [];
    this.selectedSubArea = [];
    this.searchText = "";
    this.actionFromDate = "";
    this.actionToDate = "";
    this.respondedFromDate = "";
    this.respondedToDate = "";
    this.viewActionFromDate = "";
    this.viewActionToDate = "";
    this.viewRespondedFromDate = "";
    this.viewRespondedToDate = "";
    this.selecetedActionDateFilter = false;
    this.selecetedRespondedDateFilter = false;
    this.getCorrectiveActionList({});
  }

  getAreaBySite() {
    var selectedSiteName = this.selectedSite.join();

    var params = {
      siteId: selectedSiteName,
    };
    this.CorractiveActions.getAreaFilter(params).subscribe((res) => {
      if (res["status"] == true) {
        // this.areaFilter["Data"] = res["area"];

        this.areaFilter = [];
        var areaListVr = res["area"];
        areaListVr.map((data) => {
          this.areaFilter.push({
            label: data["AREANAME"],
            value: data["AREAID"],
          });
        });
      } else {
        this.areaFilter = [];
      }
    });
  }

  changeSubArea() {
    var areaIds = {
      areaId: this.selectedArea.join(),
    };
    this.CorractiveActions.getSubAreas(areaIds).subscribe((res) => {
      // this.subAreaFilter["Data"] = res["Data"];
      this.subAreaFilter = [];
      var subAreaListVr = res["Data"];
      subAreaListVr.map((data) => {
        this.subAreaFilter.push({
          label: data["SUBAREANAME"],
          value: data["SUBAREAID"],
        });
      });
    });
  }

  actionDateSelected() {
    if (this.actionFromDate && this.actionToDate) {
      var startDate;
      var endDate;
      if (this.auth.UserInfo["dateFormat"].toLowerCase() == "dd/mm/yyyy") {
        if (
          this.actionFromDate &&
          this.actionFromDate.toString().search("/") != -1
        ) {
          var fDate = this.actionFromDate.split("/");
          startDate = new Date(fDate[1] + "/" + fDate[0] + "/" + fDate[2]);
        } else {
          startDate = this.actionFromDate;
        }

        if (
          this.actionToDate &&
          this.actionToDate.toString().search("/") != -1
        ) {
          var tDate = this.actionToDate.split("/");
          endDate = new Date(tDate[1] + "/" + tDate[0] + "/" + tDate[2]);
        } else {
          endDate = this.actionToDate;
        }
      } else {
        startDate = new Date(this.actionFromDate);
        endDate = new Date(this.actionToDate);
      }
    }
    if (!this.actionFromDate) {
      this.translate.get(["FMKPLS", "FMKFROM"]).subscribe((res: Object) => {
        this.messageService.add({
          severity: "info",
          detail: res["FMKPLS"] + " " + res["FMKFROM"],
        });
      });
    } else if (!this.actionToDate) {
      this.translate.get(["FMKPLS", "FMKTO"]).subscribe((res: Object) => {
        this.messageService.add({
          severity: "info",
          detail: res["FMKPLS"] + " " + res["FMKTO"],
        });
      });
    } else if (startDate > endDate) {
      this.translate.get(["FMKPRVD"]).subscribe((res: Object) => {
        this.messageService.add({
          severity: "info",
          detail: res["FMKPRVD"],
        });
      });
    } else {
      this.actionDate = false;
      this.selecetedActionDateFilter = true;
      var fromDate = this.actionFromDate;
      var toDate = this.actionToDate;
      if (this.auth.UserInfo["dateFormat"].toLowerCase() == "dd/mm/yyyy") {
        if (fromDate && fromDate.toString().search("/") != -1) {
          fromDate = this.actionFromDate.split("/");
          fromDate = fromDate[1] + "/" + fromDate[0] + "/" + fromDate[2];
        } else {
          fromDate = new Date(fromDate);
        }
        if (toDate && toDate.toString().search("/") != -1) {
          toDate = this.actionToDate.split("/");
          toDate = toDate[1] + "/" + toDate[0] + "/" + toDate[2];
        } else {
          toDate = new Date(toDate);
        }
      }
      this.viewActionFromDate = formatDate(
        fromDate,
        this.dateFormatType,
        this.translate.getDefaultLang()
      );
      this.viewActionToDate = formatDate(
        toDate,
        this.dateFormatType,
        this.translate.getDefaultLang()
      );
    }
  }

  respondedDateSelected() {
    if (this.respondedFromDate && this.respondedToDate) {
      var startDate;
      var endDate;
      if (this.auth.UserInfo["dateFormat"].toLowerCase() == "dd/mm/yyyy") {
        if (
          this.respondedFromDate &&
          this.respondedFromDate.toString().search("/") != -1
        ) {
          var fDate = this.respondedFromDate.split("/");
          startDate = new Date(fDate[1] + "/" + fDate[0] + "/" + fDate[2]);
        } else {
          startDate = this.respondedFromDate;
        }

        if (
          this.respondedToDate &&
          this.respondedToDate.toString().search("/") != -1
        ) {
          var tDate = this.respondedToDate.split("/");
          endDate = new Date(tDate[1] + "/" + tDate[0] + "/" + tDate[2]);
        } else {
          endDate = this.respondedToDate;
        }
      } else {
        startDate = new Date(this.respondedFromDate);
        endDate = new Date(this.respondedToDate);
      }
    }
    if (!this.respondedFromDate) {
      this.translate.get(["FMKPLS", "FMKFROM"]).subscribe((res: Object) => {
        this.messageService.add({
          severity: "info",
          detail: res["FMKPLS"] + " " + res["FMKFROM"],
        });
      });
    } else if (!this.respondedToDate) {
      this.translate.get(["FMKPLS", "FMKTO"]).subscribe((res: Object) => {
        this.messageService.add({
          severity: "info",
          detail: res["FMKPLS"] + " " + res["FMKTO"],
        });
      });
    } else if (startDate > endDate) {
      this.translate.get(["FMKPRVD"]).subscribe((res: Object) => {
        this.messageService.add({
          severity: "info",
          detail: res["FMKPRVD"],
        });
      });
    } else {
      this.respondedDueDate = false;
      this.selecetedRespondedDateFilter = true;
      var fromDate = this.respondedFromDate;
      var toDate = this.respondedToDate;
      if (this.auth.UserInfo["dateFormat"].toLowerCase() == "dd/mm/yyyy") {
        if (fromDate && fromDate.toString().search("/") != -1) {
          fromDate = this.respondedFromDate.split("/");
          fromDate = fromDate[1] + "/" + fromDate[0] + "/" + fromDate[2];
        }
        if (toDate && toDate.toString().search("/") != -1) {
          toDate = this.respondedToDate.split("/");
          toDate = toDate[1] + "/" + toDate[0] + "/" + toDate[2];
        }
      }

      this.viewRespondedFromDate = formatDate(
        fromDate,
        this.dateFormatType,
        this.translate.getDefaultLang()
      );
      this.viewRespondedToDate = formatDate(
        toDate,
        this.dateFormatType,
        this.translate.getDefaultLang()
      );
    }
  }

  clearActionDaterange() {
    if (this.viewActionFromDate) {
      this.actionFromDate = this.viewActionFromDate;
    } else {
      this.actionFromDate = "";
    }

    if (this.viewActionToDate) {
      this.actionToDate = this.viewActionToDate;
    } else {
      this.actionToDate = "";
    }
  }

  clearRespondedDaterange() {
    if (this.viewRespondedFromDate) {
      this.respondedFromDate = this.viewRespondedFromDate;
    } else {
      this.respondedFromDate = "";
    }

    if (this.viewRespondedToDate) {
      this.respondedToDate = this.viewRespondedToDate;
    } else {
      this.respondedToDate = "";
    }
  }

  cancelDialog() {
    this.actionDate = false;
    this.respondedDueDate = false;
  }

  // Filtering
  filtering() {
    var actionFromDate;
    var actionToDate;
    var respondedFromDate;
    var respondedToDate;
    if (this.actionFromDate && this.actionToDate) {
      actionFromDate = formatDate(
        new Date(this.actionFromDate),
        "MM/dd/yyyy",
        this.translate.getDefaultLang()
      );
      actionToDate = formatDate(
        new Date(this.actionToDate),
        "MM/dd/yyyy",
        this.translate.getDefaultLang()
      );
    } else {
      actionFromDate = "";
      actionToDate = "";
    }
    if (this.respondedFromDate && this.respondedToDate) {
      respondedFromDate = formatDate(
        new Date(this.respondedFromDate),
        "MM/dd/yyyy",
        this.translate.getDefaultLang()
      );
      respondedToDate = formatDate(
        new Date(this.respondedToDate),
        "MM/dd/yyyy",
        this.translate.getDefaultLang()
      );
    } else {
      respondedFromDate = "";
      respondedToDate = "";
    }
    var groupIds = [];
    for (var i = 1; i <= 10; i++) {
      if (this.selectedFilter["group" + i]) {
        groupIds.push(this.selectedFilter["group" + i].join());
      } else {
        groupIds.push("");
      }
    }
    var filRespPerson = "";
    var filActionOwner = "";
    var filCardStatusId = "";
    var filCorractiveType = "";
    if (this.selectedOptionlFilter["filRespPerson"]) {
      filRespPerson = this.selectedOptionlFilter["filRespPerson"].join();
    }
    if (this.selectedOptionlFilter["filActionOwner"]) {
      filActionOwner = this.selectedOptionlFilter["filActionOwner"].join();
    }
    if (this.selectedOptionlFilter["filCardStatusId"]) {
      filCardStatusId = this.selectedOptionlFilter["filCardStatusId"].join();
    }
    if (this.selectedOptionlFilter["filCorractiveType"]) {
      filCorractiveType = this.selectedOptionlFilter[
        "filCorractiveType"
      ].join();
    }
    var params = {
      filSiteId: this.selectedSite.join(),
      filAreaId: this.selectedArea.join(),
      filSubAreaId: this.selectedSubArea.join(),
      filShiftId: this.selectedShifts.join(),
      filRespPerson: filRespPerson,
      filActionOwner: filActionOwner,
      filResDateReqFrom: actionFromDate,
      filResDateReqTo: actionToDate,
      filRespondedFrom: respondedFromDate,
      filRespondedTo: respondedToDate,
      filCardStatusId: filCardStatusId,
      filCorractiveType: filCorractiveType,
      group1: groupIds[0],
      group2: groupIds[1],
      group3: groupIds[2],
      group4: groupIds[3],
      group5: groupIds[4],
      group6: groupIds[5],
      group7: groupIds[6],
      group8: groupIds[7],
      group9: groupIds[8],
      group10: groupIds[9],
    };
    if (
      (actionFromDate != "" && actionToDate == null) ||
      (respondedFromDate != "" && respondedToDate == null)
    ) {
      this.translate.get(["FMKPLS", "LBLMAILTO"]).subscribe((res: Object) => {
        this.messageService.add({
          severity: "info",
          detail: res["FMKPLS"] + res["LBLMAILTO"],
        });
      });
      return false;
    }

    this.isLoaded = false;
    localStorage.removeItem("filterInfo");
    localStorage.setItem("filterInfo", JSON.stringify(params));
    this.getCorrectiveActionList(params);
  }

  setSubAreaFilters(subAreaFilter) {
    subAreaFilter.map((item, index) => {
      var newData = "";
      if (item["Data"]) {
        newData = JSON.parse(item["Data"]);
      }
      var tempData = [];
      if (newData.length > 0) {
        var keies = Object.keys(newData[0]);
        var value = keies[0];
        var label = keies[1];
        if (isArray(newData)) {
          newData.map((data) => {
            tempData.push({ label: data[label], value: data[value] });
          });
        } else {
        }
      }
      if (item["Name"] == "Sites") {
        item["field"] = "filSiteId";
      }
      item["Data"] = tempData;
    });
    this.subAreaFilter = subAreaFilter;
  }

  // onTextSearch data
  onTextSearch() {
    var field = this.selectedField;
    var stext;
    var fn = this;
    this.isLoaded = false;
    if (this.searchText != "") {
      if (field != "CARDID" && field != "CAID") {
        stext = this.searchText.toLowerCase();
      } else {
        stext = this.searchText.toString();
      }
      var newList = this.allCorrectiveList.filter((item) => {
        if (field != "CARDID" && field != "CAID") {
          return item[field].toLowerCase().indexOf(stext) >= 0;
        } else {
          return item[field].toString().indexOf(stext) >= 0;
        }
      });

      this.correctiveDataList = newList;
      setTimeout(function () {
        fn.isLoaded = true;
      }, 10);
    } else {
      setTimeout(function () {
        fn.isLoaded = true;
      }, 10);
      this.correctiveDataList = this.allCorrectiveList;
    }
  }

  // Advance serach
  advanceSearch() {
    this.iconType =
      this.iconType === "ui-icon-add" ? "ui-icon-remove" : "ui-icon-add";
    this.showAdvanceSarch = !this.showAdvanceSarch;
  }

  deleteRecord() {
    this.confirmClass = "warning-msg";
    var msg = "";
    this.translate.get("ALTDELCONFIRMCAPA").subscribe((confirmMsg) => {
      msg = confirmMsg;
    });
    this.confirmationService.confirm({
      message: msg,
      accept: () => {
        this.CorractiveActions.deleteCorrectiveAction({
          corrActionId: Array.prototype.map
            .call(this.selectedDelCorrective, (s) => s.CARDID + "-" + s.CAID)
            .toString(),
        }).subscribe((res) => {
          // this.getCorrectiveActionList(this.defaultParameters);
          this.translate.get(res["message"]).subscribe((message) => {
            this.messageService.add({
              severity: "success",
              detail: message,
            });
          });
          this.searchText = "";
          this.selectedDelCorrective = [];
          this.getCorrectiveActionList(this.parameters);
        });
      },
      reject: () => {
        this.selectedDelCorrective = [];
        this.searchText = "";
        this.getCorrectiveActionList(this.parameters);
      },
    });
  }

  changeStatus(id) {
    this.confirmClass = "info-msg";
    var msg = "";
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((confirmMsg) => {
      msg = confirmMsg;
    });
    this.confirmationService.confirm({
      message: msg,
      accept: () => {
        this.CorractiveActions.updateCorrectiveAction({
          caId: id
            ? id
            : Array.prototype.map
              .call(this.selectedDelCorrective, (s) => s.CAID)
              .toString(),
        }).subscribe((res) => {
          this.selectedDelCorrective = [];
          this.getCorrectiveActionList(this.defaultParameters);
          this.searchText = "";
          this.translate.get(res["message"]).subscribe((message) => {
            this.messageService.add({
              severity: "success",
              detail: message,
            });
          });
        });
      },
      reject: () => {
        this.selectedDelCorrective = [];
        this.getCorrectiveActionList(this.defaultParameters);
        this.searchText = "";
      },
    });
  }

  toggle() {
    this.animationState = this.animationState === "out" ? "in" : "out";
    // this.selectedSite = [];
    // this.selectedSite.push(this.auth.UserInfo["defaultSiteId"]);
  }

  onRowSelect(data) {
    this.route.navigate(
      [
        "/edit-corrective-action-listing/" +
        data.CARDID +
        "/" +
        data.CAID +
        "/2",
      ],
      {
        skipLocationChange: true,
      }
    );
  }

  onSelectAll() {
    const selected = this.siteFilter["Data"].map((item) => item.SITEID);
    this.selectedSite = selected;
    this.getAreaBySite();
  }

  onClearAll() {
    this.selectedSite = [];
    this.getAreaBySite();
  }

  onSelectAllArea() {
    const selectedArea = this.areaFilter["Data"].map((item) => item.AREAID);
    this.selectedArea = selectedArea;
    this.changeSubArea();
  }

  onClearAllArea() {
    this.selectedArea = [];
    this.changeSubArea();
  }

  onSelectAllSubArea() {
    const selectedSubArea = this.subAreaFilter["Data"].map(
      (item) => item.AREAID
    );
    this.selectedSubArea = selectedSubArea;
  }

  onClearAllSubArea() {
    this.selectedSubArea = [];
  }

  onSelectAllShift() {
    const selectedShift = this.shiftFilter["Data"].map((item) => item.SHIFTID);
    this.selectedShifts = selectedShift;
  }

  onClearAllShift() {
    this.selectedShifts = [];
  }

  onSelectAllGrp(i) {
    const selected = this.filterData[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.filterData[i]["field"]] = selected;
  }

  onClearAllGrp(i) {
    this.selectedFilter[this.filterData[i]["field"]] = [];
  }

  onSelectAllAdv(i) {
    const selected = this.optionalFilter[i]["Data"].map((item) => item.value);
    this.selectedOptionlFilter[this.optionalFilter[i]["field"]] = selected;
  }

  onClearAllAdv(i) {
    this.selectedOptionlFilter[this.optionalFilter[i]["field"]] = [];
  }

  customSort(event: SortEvent) {
    event.data.sort((data1, data2) => {
      let value1 = data1[event.field];
      let value2 = data2[event.field];

      // if (this.isDateColumn(event.field)) {

      // }

      let result = null;

      if (value1 == null && value2 != null) result = -1;
      else if (value1 != null && value2 == null) result = 1;
      else if (value1 == null && value2 == null) result = 0;
      else if (typeof value1 === "string" && typeof value2 === "string") {
        const date1 = moment(
          value1,
          this.auth.UserInfo["dateFormat"].toUpperCase()
        );
        const date2 = moment(
          value2,
          this.auth.UserInfo["dateFormat"].toUpperCase()
        );

        let result: number = -1;
        if (moment(date2).isBefore(date1, "day")) {
          result = 1;
        }

        return result * event.order;
      }
      // result = value1.localeCompare(value2);
      else result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;

      return event.order * result;
    });
  }
}
