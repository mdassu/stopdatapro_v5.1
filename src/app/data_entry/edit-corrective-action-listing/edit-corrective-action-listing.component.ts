import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormArray,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { MessageService } from "primeng/api";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";
import { ActivatedRoute } from "@angular/router";
import { formatDate, DatePipe } from "@angular/common";
import { CorractiveActionsService } from "../../_services/corractive-actions.service";
import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";
import { CookieService } from "ngx-cookie-service";
import { AuthenticationService } from "../../_services/authentication.service";
import { isArray } from "util";
import { validateConfig } from "@angular/router/src/config";
import { GlobalDataService } from "src/app/_services/global-data.service";

@Component({
  selector: "app-edit-corrective-action-listing",
  templateUrl: "./edit-corrective-action-listing.component.html",
  styleUrls: ["./edit-corrective-action-listing.component.css"],
  providers: [MessageService, CustomValidatorsService],
})
export class EditCorrectiveActionListingComponent implements OnInit {
  marked = false;
  theCheckbox = false;
  // Varialbe for fields name
  myForm: FormGroup;

  AddGeneralDataForm: FormGroup;
  observationForm: FormGroup;
  AddCorrectiveActionsForm: FormGroup;
  submitted: boolean;

  display: boolean = false;
  display1: boolean = false;
  display2: boolean = false;
  site: any;
  Field3: any;
  date: any;
  Field1: any;
  Lenght: any;
  Checklistsetup: any;
  Peaoplecontacted: any;
  PeopleObserved: any;
  uploadedFiles: any[] = [];
  msgs: Message[];
  curDate = new Date();
  submitted1: boolean;
  Department: any;
  Division: any;
  Cat_assigned: any[];
  cardId: any;
  caId: any;
  caData: any;
  caCategories: any;
  actionOwner: any;
  resPersons: any;
  generalData: any;
  correctiveFormSubmitted: boolean;
  gerneralFormSubmitted: boolean;
  fieldData: any = [];
  fieldValueData: any = [];
  fieldList: any = [];
  areas: any;
  subAreas: any;
  shifts: any;
  observers: any;
  observerIds: any;
  mainCategories: any;
  subCategories: any;
  finalCategories: any = [];
  catData: any;
  CatDisplay: boolean;
  correctiveFormData: any;
  generalFormData: any;
  stopCardType: any;
  checkListSetupId: any;
  obsTabCount: any;
  dateFormat: any;
  dateFormatType: any;
  dateFormatTypeNew: any;
  correctiveLoading: boolean = false;
  generalLoading: boolean = false;
  subAreaOptionValue: any;
  obsName: any = "";
  areaName: any = "";
  shiftName: any = "";
  setupName: any = "";
  configData: any;
  uploadedImage: any;
  imageUrl: any;
  locale: any;

  ReactionsofPeople: any = [
    { getValue: "Tailgating" },
    { getValue: "Eating/Drinking" },
    { getValue: "Phone use" },
    { getValue: "Stopping Job" },
    { getValue: "Attaching grounds" },
    { getValue: "Performing lockouts" },
  ];

  priorities: any = [
    { label: "High", value: 1 },
    { label: "Medium", value: 2 },
    { label: "Low", value: 3 },
  ];

  caStatus: any = [
    { label: "Open", value: 1 },
    { label: "Completed", value: 2 },
    { label: "Alternative Action", value: 3 },
  ];

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private activatedRoute: ActivatedRoute,
    private corractiveService: CorractiveActionsService,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService,
    private cookieService: CookieService,
    private auth: AuthenticationService,
    private globalData: GlobalDataService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLDATAENTRY",
        url: "./assets/help/corrective-actions-edit(corrective_actions).md",
      },
      {
        label: "LBLCORRECTIVEACTION",
        routerLink: ["/corrective-action-list"],
      },
    ]);
    if (this.cookieService.get("new-corrective")) {
      var fn = this;
      setTimeout(function () {
        fn.messageService.add({
          severity: "success",
          detail: fn.cookieService.get("new-corrective"),
        });
        fn.cookieService.delete("new-corrective");
      }, 1000);
    }
    this.createForm();
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    // Add Observatin General
    this.correctiveLoading = true;
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.cardId = this.activatedRoute.snapshot.paramMap.get("cardId");
    this.caId = this.activatedRoute.snapshot.paramMap.get("caId");
    this.stopCardType = this.activatedRoute.snapshot.paramMap.get(
      "stopCardType"
    );
    this.checkListSetupId = this.activatedRoute.snapshot.paramMap.get(
      "checkListSetupId"
    );
    this.imageUrl = this.auth.UserInfo["uploadFileUrl"];
    var parameters = {
      cardId: this.cardId,
      caId: this.caId,
      stopCardType: this.stopCardType,
      checkListSetUpId: this.checkListSetupId,
    };

    this.getCorrectiveData(parameters);

    this.AddGeneralDataForm = this.fb.group({
      cardId: new FormControl(this.cardId),
      siteId: new FormControl(null),
      obsDate: new FormControl(null, Validators.required),
      observerId: new FormControl(null, Validators.required),
      areaId: new FormControl(0, [Validators.required, Validators.min(1)]),
      subAreaId: new FormControl(null),
      shiftId: new FormControl(0, [Validators.required, Validators.min(1)]),
      customFieldvalueId: new FormControl(null),
      safeComments: new FormControl("", [
        this.customValidatorsService.maxLengthValidator(1024),
      ]),
      unSafeComments: new FormControl("", [
        this.customValidatorsService.maxLengthValidator(1024),
      ]),
    });

    // Add Observatin Modal

    this.AddCorrectiveActionsForm = this.fb.group({
      assignCategory: new FormControl("", Validators.required),
      actionPlanned: new FormControl("", [
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.maxLengthValidator(1000),
      ]),
      actionPerformed: new FormControl(""),
      owner: new FormControl(0, [Validators.required, Validators.min(1)]),
      responseDate: new FormControl(""),
      responseRequireDate: new FormControl("", Validators.required),
      priority: new FormControl(1),
      cardStatus: new FormControl(1),
      otherRespPerson: new FormControl(""),
      otherEmailId: new FormControl(""),
      respPerson: new FormControl("", Validators.required),
      updateCorractiveActionData: new FormControl(""),
      corractionId: new FormControl(null),
      cardId: new FormControl(null),
      selectedRespPerson: new FormControl([]),
    });
    this.dateFormatType = this.auth.UserInfo["dateFormat"];
    this.dateFormatTypeNew = this.auth.UserInfo["dateFormat"];
    if (this.dateFormatTypeNew == "MM/dd/yyyy") {
      this.dateFormatTypeNew = "LBLMDY";
    } else if (this.dateFormatTypeNew == "dd/MM/yyyy") {
      this.dateFormatTypeNew = "LBLDMY";
    } else if (this.dateFormatTypeNew == "yyyy/MM/dd") {
      this.dateFormatTypeNew = "LBLYMD";
    }
    this.dateFormat = this.auth.UserInfo["dateFormat"].toLowerCase();
    this.dateFormat = this.dateFormat.replace("yyyy", "yy");
  }

  get GeneralForm() {
    return this.AddGeneralDataForm.controls;
  }

  // modals

  CreateNewObserved() {
    this.display = true;
  }
  AddCorrectiveActionsModal() {
    this.display1 = true;
  }
  CategoryComments() {
    this.display2 = true;
  }
  onUpload(event) {
    for (const file of event.files) {
      this.uploadedFiles.push(file);
    }

    this.msgs = [];
    this.msgs.push({ severity: "info", summary: "File Uploaded", detail: "" });
  }

  onEnter() {
    // this.addThing();
  }
  get things() {
    return this.myForm.get("things") as FormArray;
  }
  private createForm() {
    this.myForm = this.fb.group({
      things: this.fb.array([this.fb.control("")]),
    });
  }

  private addThing() {
    this.things.push(this.fb.control(""));
  }

  // Correctiv action tab start

  get CorrectiveSub() {
    return this.AddCorrectiveActionsForm.controls;
  }

  Responsibleperson() {
    this.marked = !this.marked;
    if (this.marked) {
      this.AddCorrectiveActionsForm.controls["otherRespPerson"].setValidators([
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.maxLengthValidator(256),
      ]);
      this.AddCorrectiveActionsForm.controls["otherEmailId"].setValidators([
        Validators.required,
        Validators.email,
        // Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        this.customValidatorsService.isEmailValid,
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.maxLengthValidator(256),
      ]);
      this.AddCorrectiveActionsForm.controls["respPerson"].clearValidators();
      this.AddCorrectiveActionsForm.controls[
        "respPerson"
      ].updateValueAndValidity();
    } else {
      this.AddCorrectiveActionsForm.controls[
        "otherRespPerson"
      ].clearValidators();
      this.AddCorrectiveActionsForm.controls[
        "otherRespPerson"
      ].updateValueAndValidity();
      this.AddCorrectiveActionsForm.controls["otherEmailId"].clearValidators();
      this.AddCorrectiveActionsForm.controls[
        "otherEmailId"
      ].updateValueAndValidity();
      this.AddCorrectiveActionsForm.controls["respPerson"].setValidators([
        Validators.required,
      ]);
      this.AddCorrectiveActionsForm.controls[
        "respPerson"
      ].updateValueAndValidity();
    }
  }

  getCorrectiveData(params) {
    this.corractiveService.getCorrectiveEditData(params).subscribe((res) => {
      if (res["status"] == true) {
        this.translate.get("LBLSELECT").subscribe((selectLabel) => {
          this.caData = res["caData"];
          // this.caCategories = res["caAvail"];
          this.caCategories = [];
          var catVr = res["caAvail"];
          catVr.map((data) => {
            this.caCategories.push({
              label: data["CATEGORYNAME"],
              value: data["CATEGORYID"],
            });
          });

          this.actionOwner = res["actionOwner"];
          this.actionOwner.splice(0, 0, { USERID: 0, FULLNAME: selectLabel });
          // this.resPersons = res["resPerson"];
          this.resPersons = [];
          var resPerson = res["resPerson"];
          resPerson.map((data) => {
            this.resPersons.push({
              label: data["FULLNAME"],
              value: data["USERID"],
            });
          });

          this.generalData = res["general"];
          this.setupName = res["setupName"];
          this.obsName = res["observerName"];
          this.areaName = res["areaName"];
          this.shiftName = res["shiftName"];
          this.configData = res["checkListConfig"];
          this.fieldData = res["fieldName"];
          this.fieldValueData = res["fieldValue"];
          // this.observers = res["observer"];
          this.observers = [];
          var observersList = res["observer"];
          observersList.map((data) => {
            this.observers.push({
              label: data["USERNAME"],
              value: data["USERID"],
            });
          });

          this.areas = res["area"];
          this.areas.splice(0, 0, { AREAID: 0, AREANAME: selectLabel });
          this.subAreas = res["subArea"];
          this.shifts = res["shifts"];
          this.shifts.splice(0, 0, { SHIFTID: 0, SHIFTNAME: selectLabel });
          this.observerIds = res["observerId"];
          this.mainCategories = res["observerMainCat"];
          this.subCategories = res["observerSubCat"];
        });

        this.obsTabCount = this.mainCategories.length;

        this.getGeneralData();
        if (this.mainCategories.length > 0) {
          this.getObservationData();
        }

        this.AddCorrectiveActionsForm.controls["cardId"].setValue(this.cardId);
        this.AddCorrectiveActionsForm.controls["corractionId"].setValue(
          this.caId
        );
        if (this.caData) {
          if (this.caId != 0) {
            this.AddCorrectiveActionsForm.controls["assignCategory"].setValue(
              res["caAssing"].split(",")
            );
            this.AddCorrectiveActionsForm.controls["actionPlanned"].setValue(
              this.caData["ACTIONPLANNED"]
            );
            this.AddCorrectiveActionsForm.controls["actionPerformed"].setValue(
              this.caData["ACTIONPERFORMED"]
            );
            this.AddCorrectiveActionsForm.controls["priority"].setValue(
              this.caData["PRIORITY"]
            );
            this.AddCorrectiveActionsForm.controls["cardStatus"].setValue(
              this.caData["CARDSTATUS"]
            );
            this.AddCorrectiveActionsForm.controls["owner"].setValue(
              this.caData["OWNER"]
            );
            if (this.caData["SELPERSON"]) {
              this.AddCorrectiveActionsForm.controls["respPerson"].setValue(
                this.caData["SELPERSON"].split(",")
              );
            } else {
              this.AddCorrectiveActionsForm.controls["respPerson"].setValue(
                null
              );
            }
            this.AddCorrectiveActionsForm.controls["responseDate"].setValue(
              this.caData["RESPONSEDATE"]
            );
            this.AddCorrectiveActionsForm.controls[
              "responseRequireDate"
            ].setValue(this.caData["RESPONSEREQUIREDDATE"]);

            this.AddCorrectiveActionsForm.controls["otherRespPerson"].setValue(
              this.caData["OTHERRESPONSIBLEPERSON"]
            );
            this.AddCorrectiveActionsForm.controls["otherEmailId"].setValue(
              this.caData["OTHEREMAILID"]
            );

            if (
              this.caData["OTHERRESPONSIBLEPERSON"] ||
              this.caData["OTHEREMAILID"]
            ) {
              this.AddCorrectiveActionsForm.controls[
                "selectedRespPerson"
              ].setValue(["otherRespPerson"]);
              this.AddCorrectiveActionsForm.controls[
                "respPerson"
              ].clearValidators();
              this.AddCorrectiveActionsForm.controls[
                "respPerson"
              ].updateValueAndValidity();
              this.marked = !this.marked;
            }

            if (this.caData["CARDSTATUS"] == 2) {
              this.AddCorrectiveActionsForm.controls[
                "responseDate"
              ].setValidators([Validators.required]);
              this.AddCorrectiveActionsForm.controls[
                "actionPerformed"
              ].setValidators([
                Validators.required,
                this.customValidatorsService.noWhitespaceValidator,
                this.customValidatorsService.maxLengthValidator(1000),
              ]);
              this.AddCorrectiveActionsForm.controls[
                "responseDate"
              ].updateValueAndValidity();
              this.AddCorrectiveActionsForm.controls[
                "actionPerformed"
              ].updateValueAndValidity();
            }
          } else {
            this.AddCorrectiveActionsForm.controls["owner"].setValue(
              this.auth.UserInfo.userId
            );
          }
        } else {
          this.AddCorrectiveActionsForm.controls["owner"].setValue(
            this.auth.UserInfo.userId
          );
        }

        this.correctiveFormData = this.AddCorrectiveActionsForm.getRawValue();
        this.correctiveLoading = false;
      }
    });
  }

  updateCorrectiveAction() {
    if (this.AddCorrectiveActionsForm.value["assignCategory"]) {
      this.caCategories = this.auth.rearrangeSelects(
        this.caCategories,
        this.AddCorrectiveActionsForm.value["assignCategory"]
      );
    }

    if (this.AddCorrectiveActionsForm.value["respPerson"]) {
      this.resPersons = this.auth.rearrangeSelects(
        this.resPersons,
        this.AddCorrectiveActionsForm.value["respPerson"]
      );
    }

    this.correctiveFormSubmitted = true;
    if (this.AddCorrectiveActionsForm.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.correctiveLoading = true;
      var params;
      params = JSON.parse(JSON.stringify(this.AddCorrectiveActionsForm.value));
      var actionDue = this.AddCorrectiveActionsForm.value[
        "responseRequireDate"
      ];
      var responseDateNew = this.AddCorrectiveActionsForm.value["responseDate"];
      // if (this.dateFormatType.toLowerCase() == "dd/mm/yyyy") {
      //   // if (
      //   //   params["responseRequireDate"] &&
      //   //   params["responseRequireDate"].search("/") != -1
      //   // ) {
      //   //   var resReqDate = params["responseRequireDate"].split("/");

      //   //   params["responseRequireDate"] =
      //   //     resReqDate[1] + "/" + resReqDate[0] + "/" + resReqDate[2];
      //   // }

      //   if (
      //     params["responseDate"] &&
      //     params["responseDate"].search("/") != -1
      //   ) {
      //     var resDate = params["responseDate"].split("/");
      //     params["responseDate"] =
      //       resDate[1] + "/" + resDate[0] + "/" + resDate[2];
      //   }
      // }

      if (this.dateFormatType.toLowerCase() == "dd/mm/yyyy") {
        if (
          this.AddCorrectiveActionsForm.value["responseRequireDate"]
            .toString()
            .search("/") != -1
        ) {
          var dateStart = this.AddCorrectiveActionsForm.value[
            "responseRequireDate"
          ].split("/");
          actionDue = new Date(
            dateStart[1] + "/" + dateStart[0] + "/" + dateStart[2]
          );
        }

        if (this.AddCorrectiveActionsForm.value["responseDate"] != null) {
          if (
            this.AddCorrectiveActionsForm.value["responseDate"]
              .toString()
              .search("/") != -1
          ) {
            var endStart = this.AddCorrectiveActionsForm.value[
              "responseDate"
            ].split("/");
            responseDateNew = new Date(
              endStart[1] + "/" + endStart[0] + "/" + endStart[2]
            );
          }
        } else {
          params["responseDate"] = "";
        }
      }
      params["responseRequireDate"] = formatDate(
        actionDue,
        "MM/dd/yyyy",
        this.translate.getDefaultLang()
      );

      if (
        this.AddCorrectiveActionsForm.value["responseDate"] != "" &&
        this.AddCorrectiveActionsForm.value["responseDate"] != null
      ) {
        params["responseDate"] = formatDate(
          responseDateNew,
          "MM/dd/yyyy",
          this.translate.getDefaultLang()
        );
      } else {
        params["responseDate"] = "";
      }
      if (
        this.AddCorrectiveActionsForm.controls["respPerson"].value &&
        this.AddCorrectiveActionsForm.controls["respPerson"].value.length != 0
      ) {
        params["respPerson"] = params["respPerson"].join();
      } else if (
        this.AddCorrectiveActionsForm.controls["respPerson"].value == null ||
        this.AddCorrectiveActionsForm.controls["respPerson"].value.length == 0
      ) {
        params["respPerson"] = null;
      }
      if (
        this.AddCorrectiveActionsForm.controls["assignCategory"].value &&
        this.AddCorrectiveActionsForm.controls["assignCategory"].value.length !=
          0
      ) {
        params["assignCategory"] = params["assignCategory"].join();
      }
      if (this.caId != 0) {
        var responseRequireDate = 0;
        var responseDate = 0;
        var respPerson = 0;
        var priority = 0;
        var otherEmailId = 0;
        var actionPlanned = 0;
        var actionPerformed = 0;
        var cardStatus = 0;
        var owner = 0;

        if (
          new Date(params["responseRequireDate"]).getTime() !=
          new Date(this.caData["RESPONSEREQUIREDDATE"]).getTime()
        ) {
          responseRequireDate = 1;
        }
        if (
          new Date(params["responseDate"]).getTime() !=
          new Date(this.caData["RESPONSEDATE"]).getTime()
        ) {
          responseDate = 1;
        }
        if (params["respPerson"] != this.caData["SELPERSON"]) {
          respPerson = 1;
        }
        if (params["priority"] != this.caData["PRIORITY"]) {
          priority = 1;
        }
        if (params["otherEmailId"] != this.caData["OTHEREMAILID"]) {
          otherEmailId = 1;
        }
        if (params["actionPlanned"] != this.caData["ACTIONPLANNED"]) {
          actionPlanned = 1;
        }
        if (params["actionPerformed"] != this.caData["ACTIONPERFORMED"]) {
          actionPerformed = 1;
        }
        if (params["cardStatus"] != this.caData["CARDSTATUS"]) {
          cardStatus = 1;
        }
        if (params["owner"] != this.caData["OWNER"]) {
          owner = 1;
        }

        var cheangeResponse = [
          {
            RESPPERSON: respPerson,
            RESPDATE: responseDate,
            RESPREQDATE: responseRequireDate,
            PRIORITY: priority,
            OTHEREMAILID: otherEmailId,
            PLANNED: actionPlanned,
            PERFORMED: actionPerformed,
            CASTATUS: cardStatus,
            ACTOWNER: owner,
            CATEGORYASSIGNED: 0,
          },
        ];

        params["updateCorractiveActionData"] = JSON.stringify(cheangeResponse);
      }
      this.corractiveService
        .updateCorrectiveActions(params)
        .subscribe((res) => {
          this.translate.get(res["message"]).subscribe((message) => {
            if (res["status"] == true) {
              this.AddCorrectiveActionsForm.markAsPristine();
              this.messageService.add({
                severity: "success",
                detail: message,
              });
              this.caId = res["corractionId"];
              this.AddCorrectiveActionsForm.controls["corractionId"].setValue(
                this.caId
              );
              if (isArray(params["respPerson"])) {
                // params["respPerson"] = params["respPerson"].split(",");
              }
              params["assignCategory"] = params["assignCategory"].split(",");
              this.correctiveFormData = this.AddCorrectiveActionsForm.getRawValue();
              this.correctiveLoading = false;
            } else {
              this.messageService.add({
                severity: "error",
                detail: message,
              });
              this.correctiveLoading = false;
            }
          });
        });
    }
  }

  resetCorrectiveForm() {
    this.AddCorrectiveActionsForm.patchValue(this.correctiveFormData);
  }
  // Correctiv action tab stop

  // General Form Data start

  getGeneralData() {
    this.AddGeneralDataForm.controls["siteId"].setValue(
      this.generalData.SITEID
    );
    var observerIds = this.observerIds.split(",").map(function (strVale) {
      return Number(strVale);
    });

    this.AddGeneralDataForm.controls["observerId"].setValue(observerIds);
    this.AddGeneralDataForm.controls["areaId"].setValue(
      this.generalData.AREAID
    );

    if (this.generalData.SUBAREAID) {
      this.AddGeneralDataForm.controls["subAreaId"].setValue(
        this.generalData.SUBAREAID
      );
    }
    this.AddGeneralDataForm.controls["shiftId"].setValue(
      this.generalData.SHIFTID
    );
    this.AddGeneralDataForm.controls["safeComments"].setValue(
      this.generalData.SAFECOMMENTS
    );
    this.AddGeneralDataForm.controls["unSafeComments"].setValue(
      this.generalData.UNSAFECOMMENTS
    );
    var obsDate = formatDate(
      this.generalData.OBSERVATIONDATE,
      this.dateFormatType,
      this.translate.getDefaultLang()
    );
    this.AddGeneralDataForm.controls["obsDate"].setValue(obsDate);

    var fields = this.fieldData;
    var fieldValue = this.fieldValueData;

    fields.map((field, key) => {
      var selectedFieldValue;
      var fieldval = fieldValue.filter((val) => {
        if (val.CUSTOMFIELDID === field.CUSTOMFIELDID) {
          if (val.FLAG == 1) {
            selectedFieldValue = val.CUSTOMFIELDVALUEID;
          }
          return val.CUSTOMFIELDID === field.CUSTOMFIELDID;
        }
      });

      if (field["MANDATORY"] == 1) {
        this.AddGeneralDataForm.addControl(
          "Field" + (key + 1),
          new FormControl(selectedFieldValue, [
            Validators.required,
            Validators.min(1),
          ])
        );
        selectedFieldValue = null;
      } else {
        this.AddGeneralDataForm.addControl(
          "Field" + (key + 1),
          new FormControl(selectedFieldValue)
        );
        selectedFieldValue = null;
      }

      this.translate.get("LBLSELECT").subscribe((selectLabel) => {
        fieldval.splice(0, 0, {
          CUSTOMFIELDVALUE: selectLabel,
          CUSTOMFIELDVALUEID: 0,
        });
        this.fieldList.push({
          fieldName: field["CUSTOMFIELDNAME"],
          mandatory: field["MANDATORY"],
          fieldValue: fieldval,
        });
      });
    });

    this.generalFormData = this.AddGeneralDataForm.getRawValue();
  }

  getSubAreaData(areaId) {
    var parameters = {
      areaId: areaId,
    };
    this.corractiveService.getSubAreaByAreaId(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.translate.get("LBLSELECT").subscribe((selectLabel) => {
          this.subAreas = res["Data"];
          this.subAreas.splice(0, 0, {
            SUBAREAID: 0,
            SUBAREANAME: selectLabel,
          });
        });
      } else {
        this.subAreas = [];
      }
    });
  }

  daysDiff(dateFrom, dateTo) {
    return Math.floor(
      (Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate()) -
        Date.UTC(
          dateFrom.getFullYear(),
          dateFrom.getMonth(),
          dateFrom.getDate()
        )) /
        (1000 * 60 * 60 * 24)
    );
  }

  updateGeneralFormData() {
    if (this.AddGeneralDataForm.value["observerId"]) {
      this.observers = this.auth.rearrangeSelects(
        this.observers,
        this.AddGeneralDataForm.value["observerId"]
      );
    }
    this.gerneralFormSubmitted = true;
    if (this.AddGeneralDataForm.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      var daysDiff = this.daysDiff(
        new Date(this.AddGeneralDataForm.value["obsDate"]),
        new Date()
      );
      if (
        daysDiff > this.globalData.GlobalData["PREVIOUSDAYLIMITONOBSDATE"] &&
        this.globalData.GlobalData["PREVIOUSDAYLIMITONOBSDATE"] != ""
      ) {
        this.submitted = false;
        this.translate
          .get(["ALTPREVIOUSDAYSLIMITONOBSDATE", "ALTPRIOROBSDATE"])
          .subscribe((message) => {
            this.messageService.add({
              severity: "error",
              detail:
                message["ALTPREVIOUSDAYSLIMITONOBSDATE"] +
                " " +
                this.globalData.GlobalData["PREVIOUSDAYLIMITONOBSDATE"] +
                " " +
                message["ALTPRIOROBSDATE"],
            });
          });
      } else {
        this.generalLoading = true;
        var finalField = [];
        this.fieldList.map((field, key) => {
          if (this.AddGeneralDataForm.value["Field" + (key + 1)]) {
            finalField.push(this.AddGeneralDataForm.value["Field" + (key + 1)]);
          }
          delete this.AddGeneralDataForm.value["Field" + (key + 1)];
        });
        if (finalField.length > 0) {
          this.AddGeneralDataForm.value[
            "customFieldvalueId"
          ] = finalField.join();
        } else {
          this.AddGeneralDataForm.value["customFieldvalueId"] = "";
        }

        var obsDate = this.AddGeneralDataForm.value["obsDate"];
        if (this.dateFormatType.toLowerCase() == "dd/mm/yyyy") {
          if (
            this.AddGeneralDataForm.value["obsDate"].toString().search("/") !=
            -1
          ) {
            var dateStart = this.AddGeneralDataForm.value["obsDate"].split("/");
            obsDate = new Date(
              dateStart[1] + "/" + dateStart[0] + "/" + dateStart[2]
            );
          }
        }
        this.AddGeneralDataForm.value["obsDate"] = formatDate(
          obsDate,
          "MM/dd/yyyy",
          this.translate.getDefaultLang()
        );

        this.AddGeneralDataForm.value[
          "observerId"
        ] = this.AddGeneralDataForm.value["observerId"].join();
        this.corractiveService
          .updateGeneralData(this.AddGeneralDataForm.value)
          .subscribe((res) => {
            this.translate.get(res["message"]).subscribe((message) => {
              if (res["status"] == true) {
                this.generalFormData = this.AddGeneralDataForm.getRawValue();
                this.AddGeneralDataForm.markAsPristine();
                this.messageService.add({
                  severity: "success",
                  detail: message,
                });
                this.AddGeneralDataForm.value[
                  "observerId"
                ] = this.AddGeneralDataForm.value["observerId"].split(",");
                this.generalLoading = false;
              } else {
                this.messageService.add({
                  severity: "error",
                  detail: message,
                });
                this.generalLoading = false;
              }
            });
          });
      }
    }
  }

  resetGeneralForm() {
    this.AddGeneralDataForm.patchValue(this.generalFormData);
  }

  // General Form Data stop

  // Observation Data start

  MainCategoryComments(mainCategoryId) {
    var catData = this.mainCategories.filter((item) => {
      return item.MAINCATEGORYID === mainCategoryId;
    });
    this.catData = catData[0];
    var params = {
      cardId: this.cardId,
      mainCatId: mainCategoryId,
      status: 4, // for obsApprobalPC
    };
    this.corractiveService.getCategoryComments(params).subscribe((res) => {
      if (res["status"] == true) {
        var data = res["details"];
        this.catData.SAFECOMMENTS = data.SAFECOMMENTS;
        this.catData.UNSAFECOMMENTS = data.UNSAFECOMMENTS;
        this.uploadedImage = data["IMAGEPATH"];
      }
    });
    this.CatDisplay = true;
  }

  SubCategoryComments(subCategory) {
    var params = {
      cardId: this.cardId,
      mainCatId: subCategory.MAINCATEGORYID,
      subCatId: subCategory.SUBCATEGORYID,
      status: 4, // for obsApprobalPC
    };
    this.corractiveService.getSubCategoryComments(params).subscribe((res) => {
      if (res["status"] == true) {
        var data = res["catComment"][0];
        this.uploadedImage = data["IMAGEPATH"];
      }
    });
    this.CatDisplay = true;
    this.catData = subCategory;
  }

  getObservationData() {
    this.mainCategories.map((mainCat) => {
      var subCatVal = this.subCategories.filter((subCat) => {
        return mainCat.MAINCATEGORYID === subCat.MAINCATEGORYID;
      });
      this.finalCategories.push({
        allSafeCheck: mainCat.ALLSAFECHECK,
        mainCategoryId: mainCat.MAINCATEGORYID,
        mainCategoryName: mainCat.MAINCATEGORYNAME,
        subCategoryValue: subCatVal,
        commentExists: mainCat.COMMENTEXISTS,
      });
    });
  }

  onSelectAll() {
    const selected = this.resPersons.map((item) => item.USERID);
    this.AddCorrectiveActionsForm.get("respPerson").patchValue(selected);
  }

  onClearAll() {
    this.AddCorrectiveActionsForm.get("respPerson").patchValue([]);
  }

  onSelectAllCat() {
    const selectedCat = this.caCategories.map((item) => item.CATEGORYID);
    this.AddCorrectiveActionsForm.get("assignCategory").patchValue(selectedCat);
  }

  onClearAllCat() {
    this.AddCorrectiveActionsForm.get("assignCategory").patchValue([]);
  }

  onSelectAllObs() {
    const selectedCat = this.observers.map((item) => item.USERID);
    this.AddGeneralDataForm.get("observerId").patchValue(selectedCat);
  }

  onClearAllObs() {
    this.AddGeneralDataForm.get("observerId").patchValue([]);
  }

  handleChange(data) {
    switch (data["index"]) {
      case 0:
        this.breadcrumbService.setItems([
          {
            label: "LBLDATAENTRY",
            url: "./assets/help/corrective-actions-edit(corrective_actions).md",
          },
          {
            label: "LBLCORRECTIVEACTION",
            routerLink: ["/corrective-action-list"],
          },
        ]);
        break;
      case 1:
        this.breadcrumbService.setItems([
          {
            label: "LBLDATAENTRY",
            url: "./assets/help/corrective-actions-edit(general).md",
          },
          {
            label: "LBLCORRECTIVEACTION",
            routerLink: ["/corrective-action-list"],
          },
        ]);
        break;
      case 2:
        this.breadcrumbService.setItems([
          {
            label: "LBLDATAENTRY",
            url: "./assets/help/corrective-actions-edit(observation).md",
          },
          {
            label: "LBLCORRECTIVEACTION",
            routerLink: ["/corrective-action-list"],
          },
        ]);
        break;
      default:
        break;
    }
  }

  changeCAStatus(event) {
    if (event["label"] == "Completed") {
      this.AddCorrectiveActionsForm.controls["responseDate"].setValidators([
        Validators.required,
      ]);
      this.AddCorrectiveActionsForm.controls["actionPerformed"].setValidators([
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.maxLengthValidator(1000),
      ]);
      this.AddCorrectiveActionsForm.controls[
        "responseDate"
      ].updateValueAndValidity();
      this.AddCorrectiveActionsForm.controls[
        "actionPerformed"
      ].updateValueAndValidity();
    } else {
      this.AddCorrectiveActionsForm.controls["responseDate"].clearValidators();
      this.AddCorrectiveActionsForm.controls[
        "responseDate"
      ].updateValueAndValidity();
      this.AddCorrectiveActionsForm.controls[
        "actionPerformed"
      ].clearValidators();
      this.AddCorrectiveActionsForm.controls[
        "actionPerformed"
      ].updateValueAndValidity();
    }
  }

  // Observation Data stop
}
