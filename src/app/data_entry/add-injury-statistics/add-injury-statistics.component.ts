import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";

import { Router } from "@angular/router";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { InjuryStatisticsService } from "src/app/_services/injury-statistics.service";
import { CustomValidatorsService } from "../../_services/custom-validators.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-add-injury-statistics",
  templateUrl: "./add-injury-statistics.component.html",
  styleUrls: ["./add-injury-statistics.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class AddInjuryStatisticsComponent implements OnInit {
  // Varialbe for fields name

  addInjuryStatistics: FormGroup;
  FlagName: any;
  submitted: boolean;

  yearListing: SelectItem[];
  siteListing: any = [];
  status: SelectItem[];
  initialGeneralFormData: any;
  isLoaded: boolean = true;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private injurystatisticsservice: InjuryStatisticsService,
    private router: Router,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private customValidatorsService: CustomValidatorsService,
    private translate: TranslateService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLDATAENTRY",
        url: "./assets/help/injury-statistics-listing.md",
      },
      {
        label: "LBLINJURYSTATSLISTING",
        routerLink: ["/injury-statistics-listing"],
      },
    ]);
  }

  ngOnInit() {
    // Add Observatin General
    this.addInjuryStatistics = this.fb.group({
      year: new FormControl("", Validators.compose([Validators.required])),
      workHour: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.maxLength(7),
          Validators.min(1),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),
      injuries: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.maxLength(4),
          Validators.min(1),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),

      siteIds: new FormControl("", Validators.compose([Validators.required])),
      status: new FormControl(1, Validators.required),
    });

    this.getInjuryAddData();

    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((resLabel) => {
      this.status = [];
      this.status.push({ label: resLabel["LBLACTIVE"], value: 1 });
      this.status.push({ label: resLabel["LBLINACTIVE"], value: 0 });
    });
  }

  /*
   * function name: getInjuryAddData
   * To get add injury data
   * @By Jitendra on 08 July 2019
   */
  getInjuryAddData() {
    this.injurystatisticsservice.getInjuryAddData().subscribe((res) => {
      if (res["status"] == true) {
        this.yearListing = res["year"];
        // this.siteListing = res["sites"];
        this.siteListing = [];
        var siteList = res["sites"];
        siteList.map((data) => {
          this.siteListing.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });

        this.addInjuryStatistics.controls["year"].setValue(
          new Date().getFullYear()
        );
        this.initialGeneralFormData = this.addInjuryStatistics.getRawValue();
        this.isLoaded = false;
      }
    });
  }

  get injuryForm() {
    return this.addInjuryStatistics.controls;
  }

  onSubmit(bType) {
    this.submitted = true;
    if (this.addInjuryStatistics.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.isLoaded = true;
      if (this.addInjuryStatistics.value["siteIds"] != null) {
        this.addInjuryStatistics.value[
          "siteIds"
        ] = this.addInjuryStatistics.value["siteIds"].join();
      }

      this.injurystatisticsservice
        .insertInjuryData(this.addInjuryStatistics.value)
        .subscribe((res) => {
          this.translate.get(res["message"]).subscribe((message) => {
            if (res["status"] == true) {
              // this.messageService.add({
              //   severity: "success",
              //   detail: message
              // });
              this.cookieService.set("add-new-injury", message);
              this.addInjuryStatistics.value["siteIds"] = null;
              this.router.navigate(["./edit-injury-statistics/" + res["id"]], {
                skipLocationChange: true,
              });
            } else {
              this.messageService.add({
                severity: "error",
                detail: message,
              });
            }
          });
        });
    }
  }

  resetInjuryForm() {
    this.addInjuryStatistics.patchValue(this.initialGeneralFormData);
  }

  onSelectAll() {
    const selected = this.siteListing.map((item) => item.SITEID);
    this.addInjuryStatistics.get("siteIds").patchValue(selected);
    this.addInjuryStatistics.get("siteIds").markAsDirty();
  }

  onClearAll() {
    this.addInjuryStatistics.get("siteIds").patchValue([]);
    this.addInjuryStatistics.get("siteIds").markAsDirty();
  }
}
