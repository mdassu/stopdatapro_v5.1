import { Component, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { EnvService } from "src/env.service";
import {
  TreeNode,
  SelectItem,
  LazyLoadEvent,
  InputTextModule,
} from "primeng/primeng";
import {
  Validators,
  FormArray,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { MessageService, ConfirmationService } from "primeng/api";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";
import { Router, ActivatedRoute } from "@angular/router";
import { FileUpload } from "primeng/fileupload";

import { AuthenticationService } from "../../_services/authentication.service";
import { ObservationChecklistService } from "../../_services/observation-checklist.service";
import { CustomValidatorsService } from "../../_services/custom-validators.service";
import { CookieService } from "ngx-cookie-service";
import { UploadImageService } from "../../_services/upload-image.service";

import { TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs";
import { ApprovalService } from "src/app/_services/approval.service";
import { formatDate } from "@angular/common";

@Component({
  selector: "app-edit-observation-list",
  templateUrl: "./edit-observation-list.component.html",
  styleUrls: ["./edit-observation-list.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class EditObservationListComponent implements OnInit {
  marked: boolean = false;
  theCheckbox = false;
  // Varialbe for fields name
  myForm: FormGroup;
  addComment: FormGroup;

  AddObservationGeneral: FormGroup;
  AddObservationModal: FormGroup;
  AddCorrectiveActions: FormGroup;
  observationForm: FormGroup;
  AddCatCommentsForm: FormGroup;
  submitted: boolean;
  submitted2: boolean;
  correctiveSubmitted: boolean;
  display: boolean = false;
  correctiveDisplay: boolean = false;
  CatDisplay: boolean = false;
  site: any;
  Field3: any;
  date: any;
  area: any;
  subarea: any;
  sifts: any;
  Field1: any;
  Lenght: any;
  Checklistsetup: any;
  Peaoplecontacted: any;
  PeopleObserved: any;
  Status: any;
  uploadedFiles: any[] = [];
  msgs: Message[];
  curDate = new Date();
  submitted1: boolean;
  Department: any;
  Division: any;
  checkError: boolean = false;
  appName: any;
  onSubmitTouched: boolean = false;
  cardId: any;
  cardReqId: any;
  checkListData: any;
  observersList: any = [];
  areaList: any;
  subAreaList: any;
  shiftsList: any;
  fieldList: any = [];
  groupList: any = [];
  userInfo: any;
  finalGroups: any = [];
  status: SelectItem[];
  generalFormData: any;
  mainCategories: any = [];
  subCategories: any = [];
  finalCategories: any = [];
  commentMainCategory: any;
  commentSubCategory: any;
  catData: any;
  correctiveData: any;
  actionOwner: any;
  responsiblePersons: any;
  selectedResPerson: any;
  correctivaData: any;
  observationFormData: any;
  isLoaded: boolean;
  correctivaFormData: any;
  successKey: any;
  defaultCorrectiveForm: any;
  tabSelected: boolean = false;
  roleId: any;
  dateFormatType: any;
  dateFormatTypeNew: any;
  dateFormat: any;
  configData: any;
  maxPeopleObserved: number;
  obsName: any;
  areaName: any;
  shiftName: any;
  fileToUpload: any = "";
  filestring: any;
  imageData: any;
  formData: any;
  imageUrl: any;
  imageName: any;
  uploadedImage: any = "";
  mainCatIdForImage: any;
  subCatIdForImage: any;
  confirmClass: any;
  approvalRequest: any;
  requestCols: any[];
  requestCount: number;
  showRequest: boolean = false;
  submittedRequest: boolean;
  observerFormData: any;
  approveStatus: any;
  generalLoading: boolean = false;
  showCorrectiveAction: boolean = true;
  activeIndex: any;
  multiple: boolean = false;
  selectedEx: boolean = false;
  createCA: any;
  subAreaOptionValue: any;
  areaManagerOptionValue: any;
  areaManager: any;
  index = 0;
  lastIndex = 0;
  addNewObsPermission: any;
  editObsPermission: any;
  commentLoading: boolean = false;
  locale: any;

  priority: any = [
    { label: "High", value: 1 },
    { label: "Medium", value: 2 },
    { label: "Low", value: 3 },
  ];

  correctiveStatus: any = [
    { label: "Open", value: 1 },
    { label: "Completed", value: 2 },
    { label: "Alternative Action", value: 3 },
  ];

  ReactionsofPeople: any = [
    { getValue: "Tailgating" },
    { getValue: "Eating/Drinking" },
    { getValue: "Phone use" },
    { getValue: "Stopping Job" },
    { getValue: "Attaching grounds" },
    { getValue: "Performing lockouts" },
  ];

  @ViewChild(FileUpload)
  fileUpload: FileUpload;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private customValidatorsService: CustomValidatorsService,
    private obcService: ObservationChecklistService,
    private auth: AuthenticationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private confirmationService: ConfirmationService,
    private cookieService: CookieService,
    private translate: TranslateService,
    private uploadImageService: UploadImageService,
    private approvalservice: ApprovalService,
    private env: EnvService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLDATAENTRY",
        url: "./assets/help/observation-checklist-edit.md",
      },
      {
        label: "LBLOBSCHECKLIST",
        routerLink: ["/observation-listing"],
      },
    ]);

    if (this.cookieService.get("new-checklist")) {
      var fn = this;
      this.tabSelected = true;
      setTimeout(function () {
        fn.messageService.add({
          severity: "success",
          detail: fn.cookieService.get("new-checklist"),
        });
        fn.cookieService.delete("new-checklist");
      }, 1000);
    }
    // this.createForm();
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    // Add Observatin General
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.areaManagerOptionValue = this.auth.UserInfo["areaManagerOptionValue"];
    this.isLoaded = true;
    this.userInfo = this.auth.UserInfo;
    var permissions = JSON.parse(
      localStorage.getItem("rolePermission-" + this.userInfo["userId"])
    );
    this.addNewObsPermission = permissions.filter((item) => {
      return item.PERMISSIONNAME == "LBLADDNEWOBS";
    });
    this.editObsPermission = permissions.filter((item) => {
      return item.PERMISSIONNAME == "LBLEDITOBSERVATION";
    });
    this.roleId = this.auth.UserInfo.roleId;
    this.dateFormatType = this.auth.UserInfo["dateFormat"];
    this.dateFormatTypeNew = this.auth.UserInfo["dateFormat"];
    if (this.dateFormatTypeNew == "MM/dd/yyyy") {
      this.dateFormatTypeNew = "LBLMDY";
    } else if (this.dateFormatTypeNew == "dd/MM/yyyy") {
      this.dateFormatTypeNew = "LBLDMY";
    } else if (this.dateFormatTypeNew == "yyyy/MM/dd") {
      this.dateFormatTypeNew = "LBLYMD";
    }
    this.dateFormat = this.auth.UserInfo["dateFormat"].toLowerCase();
    this.dateFormat = this.dateFormat.replace("yyyy", "yy");

    this.cardId = this.activatedRoute.snapshot.paramMap.get("cardId");
    this.cardReqId = this.activatedRoute.snapshot.paramMap.get("cardReqId");
    var roles = this.auth.UserInfo["assignRole"].split(",");
    if (
      Array.isArray(roles) &&
      roles.indexOf(this.auth.UserInfo["roleId"].toString()) != -1
    ) {
      this.showCorrectiveAction = false;
    }
    var data = {
      siteId: this.userInfo.defaultSiteId,
      customFieldValueId: null,
      checkListSetupId: null,
      observationDate: null,
      selObserverId: null,
      areaId: null,
      subAreaId: null,
      areaManagerId: null,
      shiftId: null,
      lengths: "",
      peopleContacted: "",
      peopleObserved: "",
      safeComments: "",
      unSafeComments: "",
      status: 1,
      maxPeopleObserved: "",
    };

    this.createObserverFormBuilder(data);

    // Add Observatin Modal

    this.AddObservationModal = this.fb.group({
      siteId: new FormControl(this.userInfo.defaultSiteId),
      firstName: new FormControl("", [
        Validators.required,
        this.customValidatorsService.maxLengthValidator(256),
      ]),
      lastName: new FormControl("", [
        Validators.required,
        this.customValidatorsService.maxLengthValidator(256),
      ]),
      email: ["", [Validators.email, Validators.maxLength(256)]],
      jobTitle: new FormControl(null),
    });

    this.AddCorrectiveActions = this.fb.group({
      mainCatId: new FormControl(""),
      subCatId: new FormControl(""),
      cardId: new FormControl(""),
      corractionId: new FormControl(""),
      checkListSetupId: new FormControl(""),
      actionPlanned: new FormControl("", [
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      actionPerformed: new FormControl(""),
      owner: new FormControl(null, Validators.required),
      priority: new FormControl(1),
      responseDate: new FormControl(""),
      cardStatus: new FormControl(1),
      respPerson: new FormControl("", Validators.required),
      responseRequireDate: new FormControl("", Validators.required),
      otherRespPerson: new FormControl(""),
      otherEmailId: new FormControl(""),
      selectedRespPerson: new FormControl([]),
      resPersonCheck: new FormControl(0),
    });

    this.defaultCorrectiveForm = this.AddCorrectiveActions.getRawValue();

    this.observationForm = this.fb.group({});

    this.AddCatCommentsForm = this.fb.group({
      mainCategoryId: new FormControl(null),
      subCategoryId: new FormControl(null),
      safeComments: new FormControl("", [
        this.customValidatorsService.maxLengthValidator(1024),
      ]),
      unSafeComments: new FormControl("", [
        this.customValidatorsService.maxLengthValidator(1024),
      ]),
    });

    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((resLabel) => {
      this.status = [];
      this.status.push({ label: resLabel["LBLACTIVE"], value: 1 });
      this.status.push({ label: resLabel["LBLINACTIVE"], value: 0 });
    });

    var parameters = {
      cardId: this.cardId,
      cardReqId: this.cardReqId,
    };
    this.getChecklistData(parameters);
    this.imageUrl = this.auth.UserInfo["uploadFileUrl"];

    this.AddCatCommentsForm = this.fb.group({
      mainCategoryId: new FormControl(null),
      subCategoryId: new FormControl(null),
      safeComments: new FormControl(""),
      unSafeComments: new FormControl(""),
    });

    this.addComment = this.fb.group({
      comments: new FormControl(
        "",
        Validators.compose([Validators.required, Validators.maxLength(1024)])
      ),
    });

    this.getUpdateRequest();
    this.appName = this.env.appName;
  }

  // General Form

  getChecklistData(parameters) {
    this.isLoaded = true;
    this.obcService.getEditFormData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.translate.get("LBLSELECT").subscribe((selectLabel) => {
          this.checkListData = res["cardData"];
          // this.observersList = res["observer"];
          this.observersList = [];
          var observersListVr = res["observer"];
          observersListVr.map((data) => {
            this.observersList.push({
              label: data["USERNAME"],
              value: data["USERID"],
            });
          });
          this.areaList = res["area"];
          this.areaList.splice(0, 0, { AREAID: 0, AREANAME: selectLabel });
          this.subAreaList = res["subArea"];
          if (this.subAreaList) {
            this.subAreaList.splice(0, 0, {
              SUBAREAID: 0,
              SUBAREANAME: selectLabel,
            });
          } else {
            this.subAreaList = [];
            this.subAreaList.splice(0, 0, {
              SUBAREAID: 0,
              SUBAREANAME: selectLabel,
            });
          }

          this.areaManager = res["users"];
          if (this.areaManager) {
            this.areaManager.splice(0, 0, {
              USERID: 0,
              FULLNAME: selectLabel,
            });
          } else {
            this.areaManager = [];
            this.areaManager.splice(0, 0, {
              USERID: 0,
              FULLNAME: selectLabel,
            });
          }

          if (
            this.editObsPermission.length > 0 &&
            this.checkListData["ISEDIT"] == 1
          ) {
            this.approveStatus = 1;
          } else {
            this.approveStatus = 0;
          }
          this.createCA = this.checkListData["CREATECA"];
          var previousUrl = localStorage.getItem(
            "editpreviousUrl-" + this.auth.UserInfo["userId"]
          );
          if (!previousUrl && this.editObsPermission.length == 0) {
            this.approveStatus = 1;
            if (this.roleId != 1 && this.roleId != 4) {
              this.createCA = 0;
            }
          }
          localStorage.removeItem(
            "editpreviousUrl-" + this.auth.UserInfo["userId"]
          );
          this.shiftsList = res["shifts"];
          this.shiftsList.splice(0, 0, { SHIFTID: 0, SHIFTNAME: selectLabel });
          this.mainCategories = res["mainCat"];
          this.subCategories = res["subCat"];
          this.configData = res["checkListConfig"];
          this.obsName = res["observerName"];
          this.areaName = res["areaName"];
          this.shiftName = res["shiftName"];
        });
        var observerIds;
        if (res["observerId"] != "") {
          observerIds = res["observerId"].split(",").map(function (strVale) {
            return Number(strVale);
          });
        } else {
          observerIds = "";
        }

        if (this.configData["MaxPeopleObservedLimit"] != "") {
          this.maxPeopleObserved = this.configData["MaxPeopleObservedLimit"];
        } else {
          this.maxPeopleObserved = 999;
        }
        if (this.configData["EXPANDCATEGORY"] == "0") {
          this.multiple = true;
          this.activeIndex = 0;
          this.selectedEx = false;
        } else {
          this.multiple = true;
          this.activeIndex = 0;
          this.selectedEx = true;
        }
        var data = {
          siteId: this.checkListData.SITEID,
          customFieldValueId: null,
          checkListSetupId: this.checkListData.CHECKLISTSETUPID,
          observationDate: this.checkListData.OBSERVATIONDATE,
          selObserverId: observerIds,
          areaId: this.checkListData.AREAID,
          subAreaId: this.checkListData.SUBAREAID,
          areaManagerId: this.checkListData.AREAMANAGER,
          shiftId: this.checkListData.SHIFTID,
          lengths: this.checkListData.LENGTH,
          peopleContacted: this.checkListData.PEOPLECONTACTED,
          peopleObserved: this.checkListData.PEOPLEOBSERVED,
          safeComments: this.checkListData.SAFECOMMENTS,
          unSafeComments: this.checkListData.UNSAFECOMMENTS,
          status: this.checkListData.STATUS,
          maxPeopleObserved: this.maxPeopleObserved,
        };

        this.createObserverFormBuilder(data);
        this.generalFormData = this.AddObservationGeneral.getRawValue();
        var fields = res["fieldName"];
        var fieldValue = res["fieldValue"];
        this.fieldList = [];
        fields.map((field, key) => {
          var selectedFieldValue = 0;
          var fieldval = fieldValue.filter((val) => {
            if (val.CUSTOMFIELDID === field.CUSTOMFIELDID) {
              if (val.FLAG == 1) {
                selectedFieldValue = val.CUSTOMFIELDVALUEID;
              }
              return val.CUSTOMFIELDID === field.CUSTOMFIELDID;
            }
          });

          if (field["MANDATORY"] == 1) {
            this.AddObservationGeneral.addControl(
              "Field" + (key + 1),
              new FormControl(selectedFieldValue, [
                Validators.required,
                Validators.min(1),
              ])
            );
            // selectedFieldValue = null;
          } else {
            this.AddObservationGeneral.addControl(
              "Field" + (key + 1),
              new FormControl(selectedFieldValue)
            );
            // selectedFieldValue = null;
          }

          this.translate.get("LBLSELECT").subscribe((selectLabel) => {
            fieldval.splice(0, 0, {
              CUSTOMFIELDVALUE: selectLabel,
              CUSTOMFIELDVALUEID: 0,
            });
            this.fieldList.push({
              fieldName: field["CUSTOMFIELDNAME"],
              mandatory: field["MANDATORY"],
              fieldValue: fieldval,
            });
          });
        });
        this.isLoaded = false;
        this.createObservationForm();
        this.createNewObserverAdd();
        this.getObservationData();
      }
    });
  }

  createObserverFormBuilder(data) {
    this.AddObservationGeneral = this.fb.group({
      siteId: new FormControl(data.siteId, Validators.required),
      customFieldValueId: new FormControl(data.customFieldValueId),
      observationDate: new FormControl(
        data.observationDate,
        Validators.required
      ),
      checkListSetupId: new FormControl(
        data.checkListSetupId,
        Validators.required
      ),
      selObserverId: new FormControl(data.selObserverId, Validators.required),
      areaId: new FormControl(data.areaId, [
        Validators.required,
        Validators.min(1),
      ]),
      subAreaId: new FormControl(data.subAreaId),
      areaManagerId: new FormControl(data.areaManagerId),
      shiftId: new FormControl(data.shiftId, [
        Validators.required,
        Validators.min(1),
      ]),
      length: new FormControl(data.lengths, [
        Validators.required,
        this.customValidatorsService.numericValidator(3),
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      peopleContacted: new FormControl(data.peopleContacted, [
        Validators.required,
        this.customValidatorsService.numericValidator(3),
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      peopleObserved: new FormControl(data.peopleObserved, [
        Validators.required,
        ,
        this.customValidatorsService.numericValidator(3),
        this.customValidatorsService.noWhitespaceValidator,
        Validators.max(this.maxPeopleObserved),
      ]),
      safeComments: new FormControl(data.safeComments, [
        this.customValidatorsService.maxLengthValidator(1024),
      ]),
      unSafeComments: new FormControl(data.unSafeComments, [
        this.customValidatorsService.maxLengthValidator(1024),
      ]),
      status: new FormControl(data.status),
    });
  }

  addNewObserver() {
    this.confirmClass = "warning-msg";
    this.submitted1 = true;
    if (this.AddObservationModal.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      var obsList = this.observersList;
      this.observersList = [];
      var groupIds = [];
      this.finalGroups.map((item, key) => {
        groupIds.push(this.AddObservationModal.value["Group" + (key + 1)]);
        delete this.AddObservationModal.value["Group" + (key + 1)];
      });
      this.AddObservationModal.value["groupId"] = groupIds.join();
      this.AddObservationModal.value[
        "siteId"
      ] = this.AddObservationGeneral.value["siteId"];
      this.obcService
        .addNewObserver(this.AddObservationModal.value)
        .subscribe((res) => {
          this.translate.get(res["message"]).subscribe((message) => {
            if (res["status"] == true) {
              var data = {
                value: parseInt(res["observerId"]),
                label: res["observer"],
              };
              obsList.unshift(data);
              this.observersList = obsList;
              this.messageService.add({
                severity: "success",
                detail: message,
              });
              this.submitted1 = false;
              this.display = false;
            } else {
              this.messageService.add({
                severity: "error",
                detail: message,
              });
            }
          });
        });
    }
  }

  // modals
  openPopup() {
    this.display = true;
    this.AddObservationModal.reset();
    this.AddObservationModal.controls["siteId"].setValue(
      this.AddObservationGeneral.value["siteId"]
    );
    this.observerFormData = this.AddObservationModal.getRawValue();
  }

  createNewObserverAdd() {
    if (this.finalGroups.length == 0) {
      this.AddObservationModal.value[
        "siteId"
      ] = this.AddObservationGeneral.value["siteId"];
      this.obcService.getObserverAddData().subscribe((res) => {
        if (res["status"] == true) {
          var groupListing = res["groupType"];
          var groupData = res["groupData"];
          this.finalGroups = [];
          groupListing.map((groupType, key) => {
            var tempGroupData = groupData.filter((data) => {
              return groupType.GROUPTYPEID === data.GROUPTYPEID;
            });
            if (groupType.MANDATORY == 0) {
              this.AddObservationModal.addControl(
                "Group" + (key + 1),
                new FormControl(0)
              );
            } else {
              this.AddObservationModal.addControl(
                "Group" + (key + 1),
                new FormControl(0, Validators.required)
              );
            }
            this.translate.get("LBLSELECT").subscribe((res: string) => {
              const groupsSelect = JSON.parse(JSON.stringify(tempGroupData));
              groupsSelect.splice(0, 0, {
                GROUPID: 0,
                GROUPNAME: res,
              });
              tempGroupData = groupsSelect;
            });
            this.finalGroups.push({
              groupTypeName: groupType.GROUPTYPENAME,
              groupData: tempGroupData,
            });
          });
        }
      });
    }
  }

  getSubArea() {
    var parameters = {
      siteId: this.AddObservationGeneral.value["siteId"],
      areaId: this.AddObservationGeneral.value["areaId"],
    };
    this.obcService.getSubAreaData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.translate.get("LBLSELECT").subscribe((selectLabel) => {
          this.subAreaList = res["subArea"];
          this.subAreaList.splice(0, 0, {
            SUBAREAID: 0,
            SUBAREANAME: selectLabel,
          });
        });

        this.translate.get("LBLSELECT").subscribe((selectLabel) => {
          this.areaManager = res["user"];
          this.areaManager.splice(0, 0, {
            USERID: 0,
            FULLNAME: selectLabel,
          });
        });

        this.AddObservationGeneral.controls["subAreaId"].setValue(0);
        this.AddObservationGeneral.controls["areaManagerId"].setValue(0);
      }
    });
  }

  updateGeneralForm() {
    if (this.AddObservationGeneral.value["selObserverId"]) {
      this.observersList = this.auth.rearrangeSelects(
        this.observersList,
        this.AddObservationGeneral.value["selObserverId"]
      );
    }

    this.confirmClass = "warning-msg";
    this.submitted = true;
    if (this.AddObservationGeneral.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.isLoaded = true;
      var finalField = [];
      this.fieldList.map((field, key) => {
        if (this.AddObservationGeneral.value["Field" + (key + 1)]) {
          finalField.push(
            this.AddObservationGeneral.value["Field" + (key + 1)]
          );
        }
        delete this.AddObservationGeneral.value["Field" + (key + 1)];
      });
      if (finalField.length > 0) {
        this.AddObservationGeneral.value[
          "customFieldValueId"
        ] = finalField.join();
      } else {
        this.AddObservationGeneral.value["customFieldValueId"] = "";
      }
      this.AddObservationGeneral.value[
        "selObserverId"
      ] = this.AddObservationGeneral.value["selObserverId"].join();
      var observationDate = this.AddObservationGeneral.value["observationDate"];
      if (this.dateFormatType.toLowerCase() == "dd/mm/yyyy") {
        if (
          this.AddObservationGeneral.value["observationDate"]
            .toString()
            .search("/") != -1
        ) {
          var dateStart = this.AddObservationGeneral.value[
            "observationDate"
          ].split("/");
          observationDate = new Date(
            dateStart[1] + "/" + dateStart[0] + "/" + dateStart[2]
          );
        }
      }
      this.AddObservationGeneral.value["observationDate"] = formatDate(
        observationDate,
        "MM/dd/yyyy",
        this.translate.getDefaultLang()
      );

      // let d = new Date(this.AddObservationGeneral.value["observationDate"]);
      // this.AddObservationGeneral.value["observationDate"] = `${d.getMonth() +
      //   1}/${d.getDate()}/${d.getFullYear()}`;

      this.AddObservationGeneral.value["cardId"] = this.cardId;
      this.obcService
        .editObservationForm(this.AddObservationGeneral.value)
        .subscribe((res) => {
          this.translate.get(res["message"]).subscribe((message) => {
            if (res["status"] == true) {
              this.messageService.add({
                severity: "success",
                detail: message,
              });
              this.AddObservationGeneral.value[
                "selObserverId"
              ] = this.AddObservationGeneral.value["selObserverId"].split(",");
              this.AddObservationGeneral.markAsPristine();
              this.isLoaded = false;
            } else {
              this.messageService.add({
                severity: "error",
                detail: message,
              });
              this.isLoaded = false;
            }
          });
        });
    }
  }

  // Observation Form

  getObservationData() {
    // console.time("observation");
    this.mainCategories.map((mainCat, key) => {
      // console.time("loop" + key);
      var subCatVal = this.subCategories.filter((subCat) => {
        if (subCat.UNSAFECOMMENTS == null) {
          subCat.UNSAFECOMMENTS = "";
        }

        // this.observationForm.addControl("allsafe", new FormControl(""));
        // this.observationForm.addControl(
        //   "mainCatId" + subCat.SUBCATEGORYID,
        //   new FormControl(subCat.MAINCATEGORYID)
        // );
        // this.observationForm.addControl(
        //   "subCatId" + subCat.SUBCATEGORYID,
        //   new FormControl(subCat.SUBCATEGORYID)
        // );
        // this.observationForm.addControl(
        //   "safe" + subCat.SUBCATEGORYID,
        //   new FormControl(
        //     subCat.SAFE,
        //     this.customValidatorsService.numericValidator(3)
        //   )
        // );
        // this.observationForm.addControl(
        //   "unsafe" + subCat.SUBCATEGORYID,
        //   new FormControl(
        //     subCat.UNSAFE,
        //     this.customValidatorsService.numericValidator(3)
        //   )
        // );

        return mainCat.MAINCATEGORYID === subCat.MAINCATEGORYID;
      });
      var selectedEx;
      if (this.configData["EXPANDCATEGORY"] == "0") {
        if (key == 0) {
          selectedEx = true;
        } else {
          selectedEx = false;
        }
      } else {
        selectedEx = true;
      }
      // console.timeEnd("loop" + key);
      this.finalCategories.push({
        allSafeCheck: mainCat.ALLSAFECHECK,
        mainCategoryId: mainCat.MAINCATEGORYID,
        mainCategoryName: mainCat.MAINCATEGORYNAME,
        subCategoryValue: subCatVal,
        commentExists: mainCat.COMMENTEXISTS,
        selectedEx: selectedEx,
      });
      this.observationFormData = this.observationForm.getRawValue();
    });
    // console.timeEnd("observation");
  }

  createObservationForm() {
    this.subCategories.map((subCat) => {
      this.observationForm.addControl(
        "allsafe" + subCat.MAINCATEGORYID,
        new FormControl("")
      );
      this.observationForm.addControl(
        "mainCatId" + subCat.SUBCATEGORYID,
        new FormControl(subCat.MAINCATEGORYID)
      );
      this.observationForm.addControl(
        "subCatId" + subCat.SUBCATEGORYID,
        new FormControl(subCat.SUBCATEGORYID)
      );
      this.observationForm.addControl(
        "safe" + subCat.SUBCATEGORYID,
        new FormControl(
          subCat.SAFE,
          this.customValidatorsService.numericValidator(3)
        )
      );
      this.observationForm.addControl(
        "unsafe" + subCat.SUBCATEGORYID,
        new FormControl(
          subCat.UNSAFE,
          this.customValidatorsService.numericValidator(3)
        )
      );
    });
  }

  get ModalSubForm() {
    return this.AddObservationModal.controls;
  }

  get CorrectiveSub() {
    return this.AddCorrectiveActions.controls;
  }

  get AddCatCmmtForm() {
    return this.AddCatCommentsForm.controls;
  }

  updateObservationForm(type) {
    this.confirmClass = "warning-msg";
    this.submitted2 = true;
    if (this.observationForm.invalid || this.checkError) {
      this.checkError = false;
    } else {
      this.isLoaded = true;
      var formValue = this.observationForm;
      var obcData = [];
      var safeCount = 0;
      var unSafeCount = 0;
      this.subCategories.map((item, key) => {
        if (formValue.value["safe" + item.SUBCATEGORYID] == "") {
          formValue.value["safe" + item.SUBCATEGORYID] = 0;
        }
        if (formValue.value["unsafe" + item.SUBCATEGORYID] == "") {
          formValue.value["unsafe" + item.SUBCATEGORYID] = 0;
        }
        safeCount = safeCount + formValue.value["safe" + item.SUBCATEGORYID];
        unSafeCount =
          unSafeCount + formValue.value["unsafe" + item.SUBCATEGORYID];

        obcData.push({
          MAINCATEGORYID: formValue.value["mainCatId" + item.SUBCATEGORYID],
          SUBCATEGORYID: formValue.value["subCatId" + item.SUBCATEGORYID],
          SAFE: formValue.value["safe" + item.SUBCATEGORYID],
          UNSAFE: formValue.value["unsafe" + item.SUBCATEGORYID],
          UNSAFECOMMENTS: item.UNSAFECOMMENTS,
        });
      });
      var checkVal = obcData.filter((item) => {
        return item.SAFE > 0 || item.UNSAFE > 0;
      });

      var parameters;
      if (type == "catcomments") {
        var checkFilterComment = this.subCategories.filter((item) => {
          return (
            item.SUBCATEGORYID == this.AddCatCommentsForm.value["subCategoryId"]
          );
        });
        if (checkFilterComment == "") {
          var checkFilterComment = this.mainCategories.filter((item) => {
            return (
              item.MAINCATEGORYID ==
              this.AddCatCommentsForm.value["mainCategoryId"]
            );
          });
        }
        if (
          this.AddCatCommentsForm.value["safeComments"] != "" ||
          this.AddCatCommentsForm.value["unSafeComments"] != "" ||
          this.imageName != ""
        ) {
          obcData.map((item) => {
            if (
              item.SUBCATEGORYID ==
              this.AddCatCommentsForm.value["subCategoryId"]
            ) {
              item.UNSAFECOMMENTS = this.AddCatCommentsForm.value[
                "unSafeComments"
              ];
            }
          });
          parameters = {
            cardId: this.cardId,
            checkListSetupId: this.checkListData.CHECKLISTSETUPID,
            isFromComments: 1,
            observations: JSON.stringify(obcData),
            mainCatId: this.AddCatCommentsForm.value["mainCategoryId"],
            subCatId: this.AddCatCommentsForm.value["subCategoryId"],
            safeComments: this.AddCatCommentsForm.value["safeComments"],
            unsafeComments: this.AddCatCommentsForm.value["unSafeComments"],
            uploadImage: this.imageName,
          };
        } else {
          this.isLoaded = false;
          this.translate
            .get("ALTSAFEUNSAFECOMMENTVALID")
            .subscribe((message) => {
              this.messageService.add({
                severity: "info",
                detail: message,
              });
            });
          return false;
        }
        // obcData.map((item) => {
        //   if (
        //     item.SUBCATEGORYID == this.AddCatCommentsForm.value["subCategoryId"]
        //   ) {
        //     item.UNSAFECOMMENTS = this.AddCatCommentsForm.value[
        //       "unSafeComments"
        //     ];
        //   }
        // });
        // parameters = {
        //   cardId: this.cardId,
        //   checkListSetupId: this.checkListData.CHECKLISTSETUPID,
        //   isFromComments: 1,
        //   observations: JSON.stringify(obcData),
        //   mainCatId: this.AddCatCommentsForm.value["mainCategoryId"],
        //   subCatId: this.AddCatCommentsForm.value["subCategoryId"],
        //   safeComments: this.AddCatCommentsForm.value["safeComments"],
        //   unsafeComments: this.AddCatCommentsForm.value["unSafeComments"],
        //   uploadImage: this.imageName,
        // };
      } else {
        if (
          (this.checkListData["ENABLESAFEUNSAFE"] == 0 &&
            checkVal.length == 0 &&
            type != "catcomments") ||
          (safeCount == 0 && unSafeCount == 0)
        ) {
          this.translate
            .get("ALTVALIDATESAFEUNSAFECOUNT")
            .subscribe((message) => {
              this.messageService.add({
                severity: "info",
                detail: message,
              });
            });
          this.isLoaded = false;
          return false;
        }
        parameters = {
          cardId: this.cardId,
          checkListSetupId: this.checkListData.CHECKLISTSETUPID,
          isFromComments: 0,
          observations: JSON.stringify(obcData),
        };
      }

      if (this.imageData != undefined) {
        this.uploadImageService.uploadImage(this.formData).subscribe((res) => {
          this.uploadImageService.changeImage(this.imageName);
        });
      }
      this.CatDisplay = false;
      this.obcService.addObservationData(parameters).subscribe((res) => {
        if (type == "corrective") {
          var msg = "";
          this.translate
            .get("ALTUNSAFECOMMENTSCONFIRM")
            .subscribe((confirmMsg) => {
              msg = confirmMsg;
            });
          this.confirmationService.confirm({
            message: msg,
            accept: () => {},
            reject: () => {
              this.router.navigate(
                ["/edit-corrective-action-listing/" + this.cardId + "/0/1"],
                {
                  skipLocationChange: true,
                }
              );
            },
          });
        } else if (type == "correctiveRedirect") {
          this.router.navigate(
            ["/edit-corrective-action-listing/" + this.cardId + "/0/1"],
            {
              skipLocationChange: true,
            }
          );
        } else {
          var changeMsg = obcData.filter((item) => {
            return item.UNSAFECOMMENTS == "" && item.UNSAFE > 0;
          });
          if (changeMsg.length > 0) {
            // if (type != "catcomments") {
            res["message"] = "ALTPROVIDECOMMENTS";
            // }
          } else {
          }
          this.translate.get(res["message"]).subscribe((message) => {
            if (res["status"] == true) {
              if (type == "catcomments") {
                // this.CatDisplay = false;
                this.messageService.add({
                  severity: "success",
                  detail: message,
                });
              } else if (type == "saveandnew") {
                if (changeMsg.length > 0) {
                  var msg = "";
                  this.translate
                    .get("ALTUNSAFECOMMENTSCONFIRM")
                    .subscribe((confirmMsg) => {
                      msg = confirmMsg;
                    });
                  this.confirmationService.confirm({
                    message: msg,
                    accept: () => {},
                    reject: () => {
                      this.router.navigate(["./add-observation-list"], {
                        skipLocationChange: true,
                      });
                    },
                  });
                } else {
                  this.cookieService.set("new-observer", message);
                  this.router.navigate(["./add-observation-list"], {
                    skipLocationChange: true,
                  });
                }
              } else {
                this.messageService.add({
                  severity: "success",
                  detail: message,
                });
              }
            } else {
              this.messageService.add({
                severity: "error",
                detail: message,
              });
            }
          });
        }

        this.fieldList = [];
        this.finalCategories = [];
        this.finalGroups = [];
        this.ngOnInit();
      });
    }
  }

  // routerLink="/edit-corrective-action-listing/{{ cardId }}/0/1"
  checkAddCorrective() {
    var formValue = this.observationForm;
    var obcData = [];
    this.subCategories.map((item) => {
      obcData.push({
        OBSCAEXISTS: item.OBSCAEXISTS,
        UNSAFECOMMENTS: item.UNSAFECOMMENTS,
        UNSAFE: formValue.value["unsafe" + item.SUBCATEGORYID],
      });
    });

    var notSave = obcData.filter((item) => {
      return (
        (item.UNSAFECOMMENTS == "" &&
          item.UNSAFE == 0 &&
          item.OBSCAEXISTS == 0) ||
        (item.UNSAFECOMMENTS == "" && item.UNSAFE == 0 && item.OBSCAEXISTS == 1)
      );
    });

    var onSaveWithConfirm = obcData.filter((item) => {
      return (
        (item.OBSCAEXISTS > 0 && item.UNSAFECOMMENTS == "") ||
        (item.OBSCAEXISTS == 0 && item.UNSAFECOMMENTS == "" && item.UNSAFE > 0)
      );
    });

    if (notSave.length == this.subCategories.length) {
      this.translate.get("ALTNOUNSAFETOCREATECAPA").subscribe((message) => {
        this.messageService.add({
          severity: "info",
          detail: message,
        });
      });

      return false;
    } else if (onSaveWithConfirm.length > 0) {
      this.updateObservationForm("corrective");
    } else {
      this.updateObservationForm("correctiveRedirect");
    }
  }

  get GeneralSubForm() {
    return this.AddObservationGeneral.controls;
  }

  // modals

  MainCategoryComments(mainCategoryId) {
    if (this.imageName != undefined) {
      this.fileUpload.clear();
    }
    this.mainCatIdForImage = mainCategoryId;
    this.subCatIdForImage = "";
    var catData = this.mainCategories.filter((item) => {
      return item.MAINCATEGORYID === mainCategoryId;
    });
    this.catData = catData[0];
    var status = this.cardReqId.split("-");
    var params = {
      cardId: this.cardId,
      mainCatId: mainCategoryId,
      status: status[1],
    };
    this.obcService.getCategoryComments(params).subscribe((res) => {
      if (res["status"] == true) {
        var data = res["catComment"][0];
        this.uploadedImage = data["IMAGEPATH"];
        this.imageName = data["IMAGEPATH"].substring(
          data["IMAGEPATH"].lastIndexOf("/") + 1
        );
        this.AddCatCommentsForm.removeControl("subCategoryId");
        this.AddCatCommentsForm.controls["mainCategoryId"].setValue(
          this.catData.MAINCATEGORYID
        );
        this.AddCatCommentsForm.controls["safeComments"].setValue(
          data.SAFECOMMENTS
        );
        this.AddCatCommentsForm.controls["unSafeComments"].setValue(
          data.UNSAFECOMMENTS
        );
      }
    });
    this.CatDisplay = true;
  }

  SubCategoryComments(subCategory) {
    if (this.imageName != undefined) {
      this.fileUpload.clear();
    }
    this.mainCatIdForImage = "";
    this.subCatIdForImage = "";
    this.CatDisplay = true;
    this.catData = subCategory;
    this.AddCatCommentsForm.controls["mainCategoryId"].setValue(
      this.catData.MAINCATEGORYID
    );
    this.AddCatCommentsForm.removeControl("subCategoryId");
    this.AddCatCommentsForm.addControl(
      "subCategoryId",
      new FormControl(this.catData.SUBCATEGORYID)
    );
    if (this.catData.SAFECOMMENTS == null) {
      this.AddCatCommentsForm.controls["safeComments"].setValue("");
    } else {
      this.AddCatCommentsForm.controls["safeComments"].setValue(
        this.catData.SAFECOMMENTS
      );
    }
    this.AddCatCommentsForm.controls["unSafeComments"].setValue(
      this.catData.UNSAFECOMMENTS
    );
    this.mainCatIdForImage = this.catData["MAINCATEGORYID"];
    this.subCatIdForImage = this.catData["SUBCATEGORYID"];
    var status = this.cardReqId.split("-");

    var params = {
      cardId: this.cardId,
      mainCatId: this.catData.MAINCATEGORYID,
      subCatId: this.catData.SUBCATEGORYID,
      status: status[1],
    };
    this.obcService.getCategoryComments(params).subscribe((res) => {
      if (res["status"] == true) {
        var data = res["catComment"][0];
        this.uploadedImage = data["IMAGEPATH"];
        this.imageName = data["IMAGEPATH"].substring(
          data["IMAGEPATH"].lastIndexOf("/") + 1
        );
      }
    });
  }

  cancelCommentsForm() {
    var msg = "";
    this.translate.get("ALTNOCHANGE").subscribe((confirmMsg) => {
      msg = confirmMsg;
    });
    this.confirmationService.confirm({
      message: msg,
      accept: () => {
        this.CatDisplay = false;
      },
    });
  }

  fileChangeEvent(event) {
    this.fileToUpload = this.fileUpload.files;
    var reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsBinaryString(this.fileToUpload[0]);
  }
  fileRemoveEvent(event) {
    this.filestring = "";

    this.imageName = "";
    this.imageData = {};
    this.formData.delete("hdndata");
  }
  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.filestring = btoa(binaryString);
    var filePath;
    if (this.subCatIdForImage == "") {
      if (!this.showCorrectiveAction) {
        filePath =
          this.auth.UserInfo["companyId"] +
          "/OL/" +
          this.cardId +
          "/" +
          this.mainCatIdForImage;
      } else {
        filePath =
          this.auth.UserInfo["companyId"] +
          "/" +
          this.cardId +
          "/" +
          this.mainCatIdForImage;
      }
    } else {
      if (!this.showCorrectiveAction) {
        filePath =
          this.auth.UserInfo["companyId"] +
          "/OL/" +
          this.cardId +
          "/" +
          this.mainCatIdForImage +
          "/" +
          this.subCatIdForImage;
      } else {
        filePath =
          this.auth.UserInfo["companyId"] +
          "/" +
          this.cardId +
          "/" +
          this.mainCatIdForImage +
          "/" +
          this.subCatIdForImage;
      }
    }
    this.imageName = this.fileToUpload[0]["name"];
    this.imageData = {
      action: "binarydata",
      filepath: filePath + "/",
      companyid: this.auth.UserInfo["companyId"],
      filename: this.fileToUpload[0]["name"],
      binaryimgdata: this.filestring,
    };
    this.formData = new FormData();
    this.formData.append("hdndata", JSON.stringify(this.imageData));
  }

  setTargetValue(allsafeval, mainCatId) {
    if (isNaN(allsafeval)) {
      this.checkError = true;
      this.translate.get("ALTSAFEUNSAFECOUNT").subscribe((message) => {
        this.messageService.add({
          severity: "info",
          detail: message,
        });
      });
      this.observationForm.controls["allsafe" + mainCatId].setValue("");
      return false;
    }

    this.onSubmitTouched = true;
    let val = Number(allsafeval);
    if (val > this.maxPeopleObserved) {
      this.checkError = true;
      this.translate
        .get("ALTMAXLIMITONSAFEUNSAFECOUNT")
        .subscribe((message) => {
          this.messageService.add({
            severity: "info",
            detail: message + " " + this.maxPeopleObserved,
          });
        });
      this.observationForm.controls["allsafe" + mainCatId].setValue("");
      return false;
    }
    // this.errorMessage = val > 100 ? true : false;
    val = val > 100 ? 100 : val;
    var subCats = this.subCategories.filter((item) => {
      return item.MAINCATEGORYID === mainCatId;
    });
    this.observationForm.controls["allsafe" + mainCatId].setValue(val);
    for (let subCat of subCats) {
      this.observationForm.controls["safe" + subCat.SUBCATEGORYID].setValue(
        val
      );
    }
  }

  Responsibleperson() {
    this.marked = !this.marked;
    if (this.marked) {
      this.AddCorrectiveActions.controls["otherRespPerson"].setValidators([
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.maxLengthValidator(256),
      ]);
      this.AddCorrectiveActions.controls["otherEmailId"].setValidators([
        Validators.required,
        Validators.email,
        Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.maxLengthValidator(256),
      ]);
      this.AddCorrectiveActions.controls["respPerson"].clearValidators();
      this.AddCorrectiveActions.controls["respPerson"].updateValueAndValidity();
    } else {
      this.AddCorrectiveActions.controls["otherRespPerson"].clearValidators();
      this.AddCorrectiveActions.controls[
        "otherRespPerson"
      ].updateValueAndValidity();
      this.AddCorrectiveActions.controls["otherEmailId"].clearValidators();
      this.AddCorrectiveActions.controls[
        "otherEmailId"
      ].updateValueAndValidity();
      this.AddCorrectiveActions.controls["respPerson"].setValidators([
        Validators.required,
      ]);
      this.AddCorrectiveActions.controls["respPerson"].updateValueAndValidity();
    }
  }

  AddCorrectiveActionsModal(catData) {
    this.marked = false;
    this.correctiveSubmitted = false;
    var params = {
      siteId: this.checkListData.SITEID,
      cardId: this.cardId,
      mainCatId: catData.MAINCATEGORYID,
      subCatId: catData.SUBCATEGORYID,
      corractionId: catData.CORRACTIONID,
    };
    this.obcService.getCorrectiveData(params).subscribe((res) => {
      this.correctiveData = res["catComment"];
      var addSelect;
      this.translate.get("LBLSELECT").subscribe((label) => {
        addSelect = {
          FULLNAME: label,
          USERID: "",
        };
      });
      this.actionOwner = res["user"];
      this.actionOwner.unshift(addSelect);
      //this.responsiblePersons = res["resPerson"];
      this.responsiblePersons = [];
      if (res["resPerson"] != "") {
        var catVr = res["resPerson"];
        catVr.map((data) => {
          this.responsiblePersons.push({
            label: data["FULLNAME"],
            value: data["USERID"],
          });
        });
      }

      this.correctivaData = res["correctivaData"];

      this.AddCorrectiveActions.controls["cardId"].setValue(this.cardId);
      this.AddCorrectiveActions.controls["mainCatId"].setValue(
        catData.MAINCATEGORYID
      );
      this.AddCorrectiveActions.controls["subCatId"].setValue(
        catData.SUBCATEGORYID
      );

      this.AddCorrectiveActions.controls["checkListSetupId"].setValue(
        this.checkListData["CHECKLISTSETUPID"]
      );

      this.AddCorrectiveActions.controls["corractionId"].setValue(
        catData.CORRACTIONID
      );

      if (this.correctivaData) {
        this.AddCorrectiveActions.controls["actionPlanned"].setValue(
          this.correctivaData.ACTIONPLANNED
        );
        this.AddCorrectiveActions.controls["actionPerformed"].setValue(
          this.correctivaData.ACTIONPERFORMED
        );
        this.AddCorrectiveActions.controls["owner"].setValue(
          this.correctivaData.OWNER
        );
        this.AddCorrectiveActions.controls["priority"].setValue(
          this.correctivaData.PRIORITY
        );
        this.AddCorrectiveActions.controls["responseDate"].setValue(
          this.correctivaData.RESPONSEDATE
        );
        this.AddCorrectiveActions.controls["cardStatus"].setValue(
          this.correctivaData.CARDSTATUS
        );
        if (this.correctivaData.SELPERSON != "") {
          this.AddCorrectiveActions.controls["respPerson"].setValue(
            this.correctivaData.SELPERSON.split(",")
          );
        }
        this.AddCorrectiveActions.controls["responseRequireDate"].setValue(
          this.correctivaData.RESPONSEREQUIREDDATE
        );
        this.AddCorrectiveActions.controls["otherRespPerson"].setValue(
          this.correctivaData.OTHERRESPONSIBLEPERSON
        );

        this.AddCorrectiveActions.controls["otherEmailId"].setValue(
          this.correctivaData.OTHEREMAILID
        );

        if (
          this.correctivaData.OTHERRESPONSIBLEPERSON ||
          this.correctivaData.OTHEREMAILID
        ) {
          this.AddCorrectiveActions.controls["selectedRespPerson"].setValue([
            "otherRespPerson",
          ]);
          this.AddCorrectiveActions.controls["respPerson"].clearValidators();
          this.AddCorrectiveActions.controls[
            "respPerson"
          ].updateValueAndValidity();
          this.marked = true;
        }

        if (this.correctivaData.CARDSTATUS == 2) {
          this.AddCorrectiveActions.controls["responseDate"].setValidators([
            Validators.required,
          ]);
          this.AddCorrectiveActions.controls["actionPerformed"].setValidators([
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
            this.customValidatorsService.maxLengthValidator(1000),
          ]);
          this.AddCorrectiveActions.controls[
            "responseDate"
          ].updateValueAndValidity();
          this.AddCorrectiveActions.controls[
            "actionPerformed"
          ].updateValueAndValidity();
        }
      } else {
        this.AddCorrectiveActions.patchValue(this.defaultCorrectiveForm);
        this.AddCorrectiveActions.controls["cardId"].setValue(this.cardId);
        this.AddCorrectiveActions.controls["mainCatId"].setValue(
          catData.MAINCATEGORYID
        );
        this.AddCorrectiveActions.controls["subCatId"].setValue(
          catData.SUBCATEGORYID
        );

        this.AddCorrectiveActions.controls["checkListSetupId"].setValue(
          this.checkListData["CHECKLISTSETUPID"]
        );

        this.AddCorrectiveActions.controls["corractionId"].setValue(
          catData.CORRACTIONID
        );

        this.AddCorrectiveActions.controls["owner"].setValue(
          this.auth.UserInfo["userId"]
        );
      }
      this.correctivaFormData = this.AddCorrectiveActions.getRawValue();
    });

    this.correctiveDisplay = true;
  }

  checkValue(val, ctrl) {
    if (isNaN(val)) {
      this.checkError = true;
      this.translate.get("ALTSAFEUNSAFECOUNT").subscribe((message) => {
        this.messageService.add({
          severity: "info",
          detail: message,
        });
      });
      this.observationForm.controls[ctrl].setValue("");
    } else if (parseInt(val) > this.maxPeopleObserved) {
      this.checkError = true;
      this.translate
        .get("ALTMAXLIMITONSAFEUNSAFECOUNT")
        .subscribe((message) => {
          this.messageService.add({
            severity: "info",
            detail: message + " " + this.maxPeopleObserved,
          });
        });
      this.observationForm.controls[ctrl].setValue("");
      // val = val > 100 ? 100 : val;
      // this.observationForm.controls[ctrl].setValue(val);
    } else {
      this.observationForm.controls[ctrl].setValue(val);
    }
  }

  updateCorrectiveAction(type) {
    this.correctiveSubmitted = true;
    if (this.AddCorrectiveActions.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      if (this.AddCorrectiveActions.value["respPerson"]) {
        this.AddCorrectiveActions.value[
          "respPerson"
        ] = this.AddCorrectiveActions.value["respPerson"].join();
      } else {
        this.AddCorrectiveActions.value["respPerson"] = "";
      }
      if (this.correctivaData == "") {
        this.correctiveDisplay = false;
        // insert Corrective data
        this.obcService
          .addCorrectiveAction(this.AddCorrectiveActions.value)
          .subscribe((res) => {
            this.AddCorrectiveActions.value[
              "respPerson"
            ] = this.AddCorrectiveActions.value["respPerson"].split(",");
            this.translate.get(res["message"]).subscribe((message) => {
              if (res["status"] == true) {
                this.subCategories.map((item) => {
                  if (
                    item.SUBCATEGORYID ==
                    this.AddCorrectiveActions.value["subCatId"]
                  ) {
                    item["OBSCAEXISTS"] = 1;
                  }
                });
                this.correctiveSubmitted = false;
                this.messageService.add({
                  severity: "success",
                  detail: message,
                });
              } else {
                this.correctiveDisplay = true;
                this.messageService.add({
                  severity: "error",
                  detail: message,
                });
              }
            });
            this.isLoaded = true;
            this.fieldList = [];
            this.finalCategories = [];
            this.finalGroups = [];
            this.ngOnInit();
          });
      } else {
        // update Corrective data

        var responseDateControl = this.AddCorrectiveActions.value[
          "responseDate"
        ];
        var responseRequireDateControl = this.AddCorrectiveActions.value[
          "responseRequireDate"
        ];
        if (this.dateFormatType.toLowerCase() == "dd/mm/yyyy") {
          if (
            this.AddCorrectiveActions.value["responseDate"] != "" &&
            this.AddCorrectiveActions.value["responseDate"] != null
          ) {
            if (
              this.AddCorrectiveActions.value["responseDate"]
                .toString()
                .search("/") != -1
            ) {
              var dateStart = this.AddCorrectiveActions.value[
                "responseDate"
              ].split("/");
              responseDateControl = new Date(
                dateStart[1] + "/" + dateStart[0] + "/" + dateStart[2]
              );
            }
          } else {
            this.AddCorrectiveActions.value["responseDate"] == "";
          }

          if (
            this.AddCorrectiveActions.value["responseRequireDate"]
              .toString()
              .search("/") != -1
          ) {
            var dateEnd = this.AddCorrectiveActions.value[
              "responseRequireDate"
            ].split("/");
            responseRequireDateControl = new Date(
              dateEnd[1] + "/" + dateEnd[0] + "/" + dateEnd[2]
            );
          }
        }
        if (
          this.AddCorrectiveActions.value["responseDate"] != "" &&
          this.AddCorrectiveActions.value["responseDate"] != null
        ) {
          this.AddCorrectiveActions.value["responseDate"] = formatDate(
            responseDateControl,
            "MM/dd/yyyy",
            this.translate.getDefaultLang()
          );
        }
        this.AddCorrectiveActions.value["responseRequireDate"] = formatDate(
          responseRequireDateControl,
          "MM/dd/yyyy",
          this.translate.getDefaultLang()
        );

        var formVal = this.AddCorrectiveActions.value;
        // check changes

        var responseRequireDate = 0;
        var responseDate = 0;
        var respPerson = 0;
        var priority = 0;
        var otherEmailId = 0;
        var actionPlanned = 0;
        var actionPerformed = 0;
        var cardStatus = 0;
        var owner = 0;

        if (
          new Date(formVal["responseRequireDate"]).getTime() !=
          new Date(this.correctivaData["RESPONSEREQUIREDDATE"]).getTime()
        ) {
          responseRequireDate = 1;
        }
        if (
          new Date(formVal["responseDate"]).getTime() !=
          new Date(this.correctivaData["RESPONSEDATE"]).getTime()
        ) {
          responseDate = 1;
        }
        if (formVal["respPerson"] != this.correctivaData["SELPERSON"]) {
          respPerson = 1;
        }
        if (formVal["priority"] != this.correctivaData["PRIORITY"]) {
          priority = 1;
        }
        if (formVal["otherEmailId"] != this.correctivaData["OTHEREMAILID"]) {
          otherEmailId = 1;
        }
        if (formVal["actionPlanned"] != this.correctivaData["ACTIONPLANNED"]) {
          actionPlanned = 1;
        }
        if (
          formVal["actionPerformed"] != this.correctivaData["ACTIONPERFORMED"]
        ) {
          actionPerformed = 1;
        }
        if (formVal["cardStatus"] != this.correctivaData["CARDSTATUS"]) {
          cardStatus = 1;
        }
        if (formVal["owner"] != this.correctivaData["OWNER"]) {
          owner = 1;
        }

        var cheangeResponse = [
          {
            RESPPERSON: respPerson,
            RESPDATE: responseDate,
            RESPREQDATE: responseRequireDate,
            PRIORITY: priority,
            OTHEREMAILID: otherEmailId,
            PLANNED: actionPlanned,
            PERFORMED: actionPerformed,
            CASTATUS: cardStatus,
            ACTOWNER: owner,
            CATEGORYASSIGNED: 0,
          },
        ];
        this.correctiveDisplay = false;
        formVal["updateCorractiveActionData"] = JSON.stringify(cheangeResponse);
        this.obcService.updateCorrectiveAction(formVal).subscribe((res) => {
          this.correctiveSubmitted = false;
          // this.correctiveDisplay = false;
          this.AddCorrectiveActions.value[
            "respPerson"
          ] = this.AddCorrectiveActions.value["respPerson"].split(",");
          this.translate.get(res["message"]).subscribe((message) => {
            if (res["status"] == true) {
              this.messageService.add({
                severity: "success",
                detail: message,
              });
            } else {
              this.messageService.add({
                severity: "error",
                detail: message,
              });
            }
          });
          this.isLoaded = true;
          this.fieldList = [];
          this.finalCategories = [];
          this.finalGroups = [];
          this.ngOnInit();
        });
      }
    }
  }

  resetGeneralForm() {
    this.AddObservationGeneral.patchValue(this.generalFormData);
    // var parameters = {
    //   cardId: this.cardId,
    //   cardReqId: this.cardReqId
    // };
    // this.getChecklistData(parameters);
  }

  resetCorrectiveActionForm() {
    this.correctiveDisplay = false;
    this.AddCorrectiveActions.patchValue(this.correctivaFormData);
  }

  resetObserverForm() {
    this.AddObservationModal.patchValue(this.observerFormData);
  }

  resetObservationForm() {
    this.observationForm.patchValue(this.observationFormData);
  }

  onSelectAll() {
    const selected = this.observersList.map((item) => item.USERID);
    this.AddObservationGeneral.get("selObserverId").patchValue(selected);
    this.AddObservationGeneral.get("selObserverId").markAsDirty();
  }

  onClearAll() {
    this.AddObservationGeneral.get("selObserverId").patchValue([]);
    this.AddObservationGeneral.get("selObserverId").markAsDirty();
  }

  onSelectAllRes() {
    const selected = this.responsiblePersons.map((item) => item.USERID);
    this.AddCorrectiveActions.get("respPerson").patchValue(selected);
    this.AddObservationGeneral.get("respPerson").markAsDirty();
  }

  onClearAllRes() {
    this.AddCorrectiveActions.get("respPerson").patchValue([]);
    this.AddObservationGeneral.get("respPerson").markAsDirty();
  }

  getUpdateRequest() {
    var parameters = {
      cardId: this.cardId,
    };
    this.approvalservice.getUpdateRequest(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.requestCols = [
          { field: "FIRSTNAME", header: "LBLENTEREDBY" },
          { field: "COMMENTS", header: "LBLCOMMENTS" },
          { field: "DATE", header: "LBLDATE" },
        ];
        this.requestCount = this.requestCols.length;
        this.approvalRequest = res["data"];
        this.showRequest = true;
      } else {
        this.showRequest = false;
      }
    });
  }

  onSubmitRequest(value: string) {
    var parameters = {
      cardId: this.cardId,
    };
    this.submittedRequest = true;
    if (this.addComment.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.commentLoading = true;
      this.addComment.value["cardId"] = this.cardId;
      this.addComment.value["requestType"] = "observerComment";
      this.approvalservice
        .updateRequest(this.addComment.value)
        .subscribe((res) => {
          if (res["status"] == true) {
            this.submittedRequest = false;
            this.getUpdateRequest();
            this.addComment.reset();
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.commentLoading = false;
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
            this.commentLoading = false;
          }
        });
    }
  }

  handleChange(data) {
    if (
      (this.AddObservationGeneral.dirty && this.lastIndex === 0) ||
      (this.observationForm.dirty && this.lastIndex === 1)
    ) {
      this.translate.get("LBLCONFIRMYESNO").subscribe((message) => {
        this.confirmationService.confirm({
          message: message,
          accept: () => {
            this.lastIndex = data.index;
            this.AddObservationGeneral.patchValue(this.generalFormData);
            this.observationForm.patchValue(this.observationFormData);
            this.setHelpFile(data.index);
          },
          reject: () => {
            this.index = this.lastIndex;
            this.setHelpFile(this.lastIndex);
          },
        });
      });
    } else {
      this.lastIndex = data.index;
      this.setHelpFile(data.index);
    }
  }

  setHelpFile(index) {
    if (index == 0) {
      this.breadcrumbService.setItems([
        {
          label: "LBLDATAENTRY",
          url: "./assets/help/observation-checklist-edit.md",
        },
        {
          label: "LBLOBSCHECKLIST",
          routerLink: ["/observation-listing"],
        },
      ]);
    } else {
      this.breadcrumbService.setItems([
        {
          label: "LBLDATAENTRY",
          url: "./assets/help/observation-checklist-enter-observations.md",
        },
        {
          label: "LBLOBSCHECKLIST",
          routerLink: ["/observation-listing"],
        },
      ]);
    }
  }

  changeCAStatus(event) {
    if (event["label"] == "Completed") {
      this.AddCorrectiveActions.controls["responseDate"].setValidators([
        Validators.required,
      ]);
      this.AddCorrectiveActions.controls["actionPerformed"].setValidators([
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.maxLengthValidator(1000),
      ]);
      this.AddCorrectiveActions.controls[
        "responseDate"
      ].updateValueAndValidity();
      this.AddCorrectiveActions.controls[
        "actionPerformed"
      ].updateValueAndValidity();
    } else {
      this.AddCorrectiveActions.controls["responseDate"].clearValidators();
      this.AddCorrectiveActions.controls[
        "responseDate"
      ].updateValueAndValidity();
      this.AddCorrectiveActions.controls["actionPerformed"].clearValidators();
      this.AddCorrectiveActions.controls[
        "actionPerformed"
      ].updateValueAndValidity();
    }
  }
}
