import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";

import { EnvService } from "src/env.service";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  FormArray,
} from "@angular/forms";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";
import { formatDate } from "@angular/common";
import { CookieService } from "ngx-cookie-service";
import { Router, ActivatedRoute } from "@angular/router";
import { ApprovalService } from "src/app/_services/approval.service";
import { AngularWaitBarrier } from "blocking-proxy/built/lib/angular_wait_barrier";
import { TranslateService } from "@ngx-translate/core";
import { Subject } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { GlobalDataService } from "src/app/_services/global-data.service";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

@Component({
  selector: "app-add-approval",
  templateUrl: "./add-approval.component.html",
  styleUrls: ["./add-approval.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class AddApprovalComponent implements OnInit {
  marked = false;
  // Varialbe for fields name
  myForm: FormGroup;
  addComment: FormGroup;

  AddObservatinGeneral: FormGroup;
  observationForm: FormGroup;
  AddCatCommentsForm: FormGroup;

  submitted: boolean;
  display: boolean = false;
  display1: boolean = false;
  display2: boolean = false;

  approvalId: any;
  approvalSites: string;
  approvalCheckList: string;
  approvalArea: any;
  approvalSubArea: any;
  approvalShift: any;
  approvalObserver: any = [];
  approvalStatus: SelectItem[];
  Status: SelectItem[];
  approvalCheckListId: any;
  mainCategories: Array<any>;
  subCategories: any = [];
  mainCategoryItems: any = [];
  fieldList: any = [];
  UNSAFE: any;
  SAFE: any;
  items: FormArray;
  approvalSitesId: any;
  initialGeneralFormData: any;
  curDate = new Date();
  finalCategories: any = [];
  approvalRequest: any;
  requestCols: any[];
  requestCount: number;
  submittedRequest: boolean;
  observationFormData: any;
  categoryDisplay: any;
  catData: any;
  showRequest: boolean = false;
  isLoaded: boolean;
  onHideDialogBox = true;
  index: number = 0;
  appName: any;
  maxObservationBound: number;
  customInput: Subject<any> = new Subject();
  confirmClass: any;
  checkVal: any;
  dateFormat: any;
  dateFormatType: any;
  dateFormatTypeNew: any;
  commentLoading: boolean = false;
  subAreaOptionValue: any;
  configData: any;
  catCommentimage: any;
  imageUrl: any;
  maxPeopleObserved: any;
  locale: any;

  ReactionsofPeople: any = [
    { getValue: "Tailgating" },
    { getValue: "Eating/Drinking" },
    { getValue: "Phone use" },
    { getValue: "Stopping Job" },
    { getValue: "Attaching grounds" },
    { getValue: "Performing lockouts" },
  ];

  constructor(
    private fb: FormBuilder,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private route: Router,
    private approvalservice: ApprovalService,
    private activetedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private translate: TranslateService,
    private env: EnvService,
    private auth: AuthenticationService,
    private globalData: GlobalDataService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLDATAENTRY", url: "./assets/help/approval-listing.md" },
      {
        label: "LBLAPPROVAL",
        routerLink: ["/approval-listing"],
      },
    ]);
    this.createForm();
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.imageUrl = this.auth.UserInfo["uploadFileUrl"];
    // this.maxObservationBound = JSON.parse(localStorage["currentUser"])[
    //   "editObservation"
    // ];
    this.maxPeopleObserved = this.auth.UserInfo["editObservation"];
    if (this.maxPeopleObserved != "") {
      this.maxObservationBound = this.auth.UserInfo["editObservation"];
    } else {
      this.maxObservationBound = 999;
    }

    this.isLoaded = true;
    // Add Observatin General
    this.approvalId = this.activetedRoute.snapshot.paramMap.get("approvalId");
    var data = {
      areaId: null,
      subAreaId: null,
      shiftId: null,
      obsDate: null,
      observerId: null,
      length: null,
      peopleContact: null,
      peopleObserver: "",
      safeComment: null,
      unsafeComment: null,
      status: 1,
    };
    this.createFormBuilder(data);
    this.getGeneralApprovalData();

    this.AddCatCommentsForm = this.fb.group({
      mainCategoryId: new FormControl(null),
      subCategoryId: new FormControl(null),
      safeComments: new FormControl(""),
      unSafeComments: new FormControl(""),
    });

    this.addComment = this.fb.group({
      comments: new FormControl(
        "",
        Validators.compose([Validators.required, Validators.maxLength(1024)])
      ),
    });

    this.observationForm = this.fb.group({});

    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((resLabel) => {
      this.Status = [];
      this.Status.push({ label: resLabel["LBLACTIVE"], value: 1 });
      this.Status.push({ label: resLabel["LBLINACTIVE"], value: 0 });
    });

    this.customInput
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe((value) => {
        this.translate
          .get(["ALTMAXLIMITONSAFEUNSAFECOUNT", "ALTSAFEUNSAFECOUNT"])
          .subscribe((translation) => {
            const control = `${value["controlName"]}${value["rowID"]}`;
            if (value["value"].length > 0) {
              if (!/^[0-9()]+$/.test(value["value"])) {
                this.messageService.add({
                  severity: "error",
                  detail: `${translation["ALTSAFEUNSAFECOUNT"]}`,
                });
                this.observationForm.controls[control].setValue("");
              } else {
                if (this.maxObservationBound) {
                  if (value["value"] > this.maxObservationBound) {
                    this.observationForm.controls[control].setValue(
                      this.maxObservationBound
                    );
                    this.messageService.add({
                      severity: "error",
                      detail: `${translation["ALTMAXLIMITONSAFEUNSAFECOUNT"]} ${this.maxObservationBound}`,
                    });
                  }
                }
              }
            }
          });
      });
    this.appName = this.env.appName;

    this.dateFormatType = this.auth.UserInfo["dateFormat"];
    if (this.dateFormatType == "MM/dd/yyyy") {
      this.dateFormatTypeNew = "LBLMDY";
    } else if (this.dateFormatType == "dd/MM/yyyy") {
      this.dateFormatTypeNew = "LBLDMY";
    } else if (this.dateFormatType == "yyyy/MM/dd") {
      this.dateFormatTypeNew = "LBLYMD";
    }
    this.dateFormat = this.auth.UserInfo["dateFormat"].toLowerCase();
    this.dateFormat = this.dateFormat.replace("yyyy", "yy");
  }

  getGeneralApprovalData() {
    this.confirmClass = "warning-msg";
    // var approvalCardId = this.approvalId.split("-");
    var parameters = {
      cardId: this.approvalId,
    };
    this.approvalservice.getApprovalGeneralData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var generalApproveData = res["approveData"];
        this.approvalSites = generalApproveData["SITENAME"];
        this.approvalCheckList = generalApproveData["SETUPNAME"];
        this.approvalCheckListId = generalApproveData["CHECKLISTSETUPID"];
        this.approvalSitesId = generalApproveData["SITEID"];
        this.approvalArea = res["area"];
        this.configData = res["checkListConfig"];

        // this.approvalSubArea = res["subArea"];
        this.approvalSubArea = res["subArea"];
        if (this.approvalSubArea.length > 0) {
          this.AddObservatinGeneral.controls["subAreaId"].setValue(
            this.approvalSubArea[0]["SUBAREAID"]
          );
        }

        this.approvalShift = res["shift"];

        this.translate.get("LBLSELECT").subscribe((translatedText: string) => {
          // this.approvalArea.unshift({
          //   AREANAME: translatedText,
          //   AREAID: 0
          // });
          this.approvalSubArea.unshift({
            SUBAREANAME: translatedText,
            SUBAREAID: 0,
          });
          // this.approvalShift.unshift({
          //   SHIFTNAME: translatedText,
          //   SHIFTID: 0
          // });
        });

        // this.approvalObserver = res["observerList"];
        this.approvalObserver = [];
        var obsVr = res["observerList"];
        obsVr.map((data) => {
          this.approvalObserver.push({
            label: data["NAME"],
            value: data["USERID"],
          });
        });
        //   this.approvalStatus = res['area'];

        var observerrAssignId;
        if (res["observerrAssignId"] != "") {
          observerrAssignId = res["observerrAssignId"].split(",");
        } else {
          observerrAssignId = [];
        }
        var newObsDate = formatDate(
          generalApproveData["OBSERVATIONDATE"],
          this.dateFormatType,
          this.translate.getDefaultLang()
        );
        var data = {
          areaId: generalApproveData["AREAID"],
          subAreaId: generalApproveData["SUBAREAID"],
          shiftId: generalApproveData["SHIFTID"],
          obsDate: newObsDate,
          observerId: observerrAssignId,
          length: generalApproveData["LENGTH"],
          peopleContact: generalApproveData["PEOPLECONTACTED"],
          peopleObserver: generalApproveData["PEOPLEOBSERVED"],
          safeComment: generalApproveData["SAFECOMMENTS"],
          unsafeComment: generalApproveData["UNSAFECOMMENTS"],
          status: generalApproveData["STATUS"],
        };

        this.createFormBuilder(data);

        var fields = res["customName"];
        var fieldValue = res["customValue"];
        fields.map((field, key) => {
          var fieldval = fieldValue.filter((val) => {
            return val.CUSTOMFIELDID === field.CUSTOMFIELDID;
          });

          var selectedField = fieldval.filter(function (selectVal) {
            return selectVal.FLAG == 1;
          });
          if (field["MANDATORY"] == 1) {
            if (selectedField.length > 0) {
              this.AddObservatinGeneral.addControl(
                "Field" + (key + 1),
                new FormControl(
                  selectedField[0]["CUSTOMFIELDVALUEID"],
                  Validators.required
                )
              );
            } else {
              this.AddObservatinGeneral.addControl(
                "Field" + (key + 1),
                new FormControl(null, Validators.required)
              );
            }
          } else {
            if (selectedField.length > 0) {
              this.AddObservatinGeneral.addControl(
                "Field" + (key + 1),
                new FormControl(selectedField[0]["CUSTOMFIELDVALUEID"])
              );
            } else {
              this.AddObservatinGeneral.addControl(
                "Field" + (key + 1),
                new FormControl(null)
              );
            }
          }

          this.translate
            .get("LBLSELECT")
            .subscribe((translatedText: string) => {
              fieldval.unshift({
                CUSTOMFIELDVALUE: translatedText,
                CUSTOMFIELDVALUEID: 0,
              });
            });

          this.fieldList.push({
            fieldName: field["CUSTOMFIELDNAME"],
            mandatory: field["MANDATORY"],
            fieldValue: fieldval,
          });
        });
        this.initialGeneralFormData = this.AddObservatinGeneral.getRawValue();
        this.isLoaded = false;

        this.getUpdateRequest();
        this.getObservationsData();
      }
    });
  }

  getSubAreaData(areaId) {
    this.confirmClass = "warning-msg";
    var parameters = {
      areaId: areaId,
    };
    this.approvalservice.getAreaFilterData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var subAreaAll;
        subAreaAll = {
          SUBAREANAME: "--Select Sub Area--",
          SUBAREAID: 0,
        };
        this.approvalSubArea = res["subArea"];
        this.approvalSubArea.unshift(subAreaAll);
        this.AddObservatinGeneral.controls["subAreaId"].setValue(
          this.approvalSubArea[0]["SUBAREAID"]
        );
      } else {
        this.approvalSubArea = [];
        var subAreaAll;
        subAreaAll = {
          SUBAREANAME: "--Select Sub Area--",
          SUBAREAID: 0,
        };
        this.approvalSubArea.push(subAreaAll);
        this.AddObservatinGeneral.controls["subAreaId"].setValue(
          this.approvalSubArea[0]["SUBAREAID"]
        );
      }
    });
  }

  getObservationsData() {
    this.confirmClass = "warning-msg";
    var parameters = {
      cardId: this.approvalId,
      checkListId: this.approvalCheckListId,
    };

    this.approvalservice.getObservationsData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.mainCategories = res["mainCat"];
        this.subCategories = res["subCat"];
        this.createObservationForm();
        this.mainCategories.map((mainCat) => {
          var subCatVal = this.subCategories.filter((subCat) => {
            // this.observationForm.addControl("allsafe", new FormControl(""));
            // this.observationForm.addControl(
            //   "mainCatId" + subCat["SUBCATEGORYID"],
            //   new FormControl(subCat["MAINCATEGORYID"])
            // );
            // this.observationForm.addControl(
            //   "subCatId" + subCat["SUBCATEGORYID"],
            //   new FormControl(subCat["SUBCATEGORYID"])
            // );
            // this.observationForm.addControl(
            //   "safe" + subCat["SUBCATEGORYID"],
            //   new FormControl(subCat["SAFE"])
            // );
            // this.observationForm.addControl(
            //   "unsafe" + subCat["SUBCATEGORYID"],
            //   new FormControl(subCat["UNSAFE"])
            // );
            return mainCat["MAINCATEGORYID"] === subCat["MAINCATEGORYID"];
          });
          this.finalCategories.push({
            allSafeCheck: mainCat.ALLSAFECHECK,
            mainCategoryId: mainCat.MAINCATEGORYID,
            mainCategoryName: mainCat.MAINCATEGORYNAME,
            subCategoryValue: subCatVal,
            commentExists: mainCat.COMMENTEXISTS,
          });
          this.observationFormData = this.observationForm.getRawValue();
        });
      }
    });
  }

  createObservationForm() {
    this.subCategories.map((subCat) => {
      this.observationForm.addControl("allsafe", new FormControl(""));
      this.observationForm.addControl(
        "mainCatId" + subCat["SUBCATEGORYID"],
        new FormControl(subCat["MAINCATEGORYID"])
      );
      this.observationForm.addControl(
        "subCatId" + subCat["SUBCATEGORYID"],
        new FormControl(subCat["SUBCATEGORYID"])
      );
      this.observationForm.addControl(
        "safe" + subCat["SUBCATEGORYID"],
        new FormControl(subCat["SAFE"])
      );
      this.observationForm.addControl(
        "unsafe" + subCat["SUBCATEGORYID"],
        new FormControl(subCat["UNSAFE"])
      );
    });
  }

  setTargetValue(allsafeval, mainCatId) {
    if (isNaN(allsafeval)) {
      this.translate.get("ALTSAFEUNSAFECOUNT").subscribe((message) => {
        this.messageService.add({
          severity: "info",
          detail: message,
        });
      });
      this.observationForm.controls["allsafe"].setValue("");
      return false;
    }
    this.confirmClass = "warning-msg";
    let val = Number(allsafeval);
    if (val > this.maxObservationBound) {
      this.translate
        .get("ALTMAXLIMITONSAFEUNSAFECOUNT")
        .subscribe((message) => {
          this.messageService.add({
            severity: "info",
            detail: message + " " + this.maxObservationBound,
          });
        });
      this.observationForm.controls["allsafe"].setValue("");
      return false;
    }
    // this.errorMessage = val > 100 ? true : false;
    val = val > 100 ? 100 : val;
    var subCats = this.subCategories.filter((item) => {
      return item["MAINCATEGORYID"] === mainCatId;
    });

    for (let subCat of subCats) {
      this.observationForm.controls["safe" + subCat["SUBCATEGORYID"]].setValue(
        val
      );
    }
  }

  MainCategoryComments(mainCategoryId) {
    this.catCommentimage = "";
    this.confirmClass = "warning-msg";
    var catData = this.mainCategories.filter((item) => {
      return item.MAINCATEGORYID === mainCategoryId;
    });
    this.catData = catData[0];
    var params = {
      cardId: this.approvalId,
      mainCatId: mainCategoryId,
    };
    this.approvalservice.getCategoryComments(params).subscribe((res) => {
      if (res["status"] == true) {
        var data = res["details"];
        this.catCommentimage = res["details"]["IMAGEPATH"];
        // this.AddCatCommentsForm.removeControl("subCategoryId");
        this.AddCatCommentsForm.controls["mainCategoryId"].setValue(
          this.catData.MAINCATEGORYID
        );
        this.AddCatCommentsForm.controls["subCategoryId"].setValue(null);
        this.AddCatCommentsForm.controls["safeComments"].setValue(
          data.SAFECOMMENTS
        );
        this.AddCatCommentsForm.controls["unSafeComments"].setValue(
          data.UNSAFECOMMENTS
        );
      }
    });
    this.categoryDisplay = true;
  }

  SubCategoryComments(subCategory) {
    this.catCommentimage = "";
    this.confirmClass = "warning-msg";
    this.categoryDisplay = true;
    this.catData = subCategory;
    var params = {
      cardId: this.approvalId,
      mainCatId: this.catData.MAINCATEGORYID,
      subCatId: this.catData.SUBCATEGORYID,
    };

    this.approvalservice.getCategoryComments(params).subscribe((res) => {
      if (res["status"] == true) {
        var data = res["details"];
        this.catCommentimage = res["details"]["IMAGEPATH"];
        this.catData["MAINCATEGORYNAME"] = res["details"]["MAINCATEGORYNAME"];
        this.AddCatCommentsForm.controls["mainCategoryId"].setValue(
          this.catData.MAINCATEGORYID
        );
        this.AddCatCommentsForm.controls["subCategoryId"].setValue(
          this.catData.SUBCATEGORYID
        );
        this.AddCatCommentsForm.controls["safeComments"].setValue(
          data.SAFECOMMENTS
        );
        this.AddCatCommentsForm.controls["unSafeComments"].setValue(
          data.UNSAFECOMMENTS
        );
      }
    });
  }

  updateObservationForm(type) {
    this.confirmClass = "warning-msg";
    if (
      this.AddCatCommentsForm.controls["safeComments"].value.length > 0 ||
      this.AddCatCommentsForm.controls["unSafeComments"].value.length > 0
    ) {
      this.updateObservationFormCall(type, false);
    } else {
      if (
        this.AddCatCommentsForm.controls["safeComments"].dirty ||
        this.AddCatCommentsForm.controls["unSafeComments"].dirty
      ) {
        this.updateObservationFormCall(type, true);
      } else {
        this.translate
          .get("ALTSAFEUNSAFECOMMENTVALID")
          .subscribe((res: string) => {
            this.messageService.add({
              severity: "info",
              detail: res,
            });
          });
      }
    }
  }

  updateObservationFormCall(type, resetValue) {
    this.confirmClass = "warning-msg";
    if (this.observationForm.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      var formValue = this.observationForm;
      var obcData = [];
      this.subCategories.map((item) => {
        obcData.push({
          MAINCATEGORYID: formValue.value["mainCatId" + item["SUBCATEGORYID"]],
          SUBCATEGORYID: formValue.value["subCatId" + item["SUBCATEGORYID"]],
          SAFE: formValue.value["safe" + item["SUBCATEGORYID"]],
          UNSAFE: formValue.value["unsafe" + item["SUBCATEGORYID"]],
        });
      });

      var parameters;
      if (type == "catcomments") {
        parameters = {
          cardId: this.approvalId,
          checkListSetupId: this.approvalCheckListId,
          isFromComments: 1,
          observations: JSON.stringify(obcData),
          mainCatId: this.AddCatCommentsForm.value["mainCategoryId"],
          subCatId: this.AddCatCommentsForm.value["subCategoryId"],
          safeComments: this.AddCatCommentsForm.value["safeComments"],
          unsafeComments: this.AddCatCommentsForm.value["unSafeComments"],
        };
      } else {
        parameters = {
          cardId: this.approvalId,
          checkListSetupId: this.approvalCheckListId,
          isFromComments: 0,
          observations: JSON.stringify(obcData),
        };
      }

      this.approvalservice.addObservationData(parameters).subscribe((res) => {
        this.confirmClass = "warning-msg";
        this.finalCategories = [];
        this.getObservationsData();
        if (resetValue) {
          this.AddCatCommentsForm.reset();
        }
        if (res["status"] == true) {
          this.observationForm.markAsPristine();
          if (type == "catcomments") {
            this.categoryDisplay = false;
            this.subCategories.map((item) => {
              item["SAFECOMMENTS"] = this.AddCatCommentsForm.value[
                "safeComments"
              ];
              item["UNSAFECOMMENTS"] = this.AddCatCommentsForm.value[
                "unSafeComments"
              ];
            });
          }
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "success",
              detail: res,
            });
          });
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
        }
        this.onHideDialogBox = false;
      });
    }
  }

  onObservationFormClose() {
    this.confirmClass = "warning-msg";
    if (this.onHideDialogBox) {
      this.categoryDisplay = false;
      this.translate.get("ALTNOCHANGE").subscribe((res) => {
        this.confirmationService.confirm({
          message: res,
          accept: () => {},
          reject: () => {
            this.categoryDisplay = true;
          },
        });
      });
    } else {
      this.onHideDialogBox = true;
    }
  }

  getUpdateRequest() {
    this.checkVal = 1;
    this.confirmClass = "warning-msg";
    var parameters = {
      cardId: this.approvalId,
    };
    this.approvalservice.getUpdateRequest(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.requestCols = [
          { field: "FIRSTNAME", header: "LBLENTEREDBY" },
          { field: "COMMENTS", header: "LBLCOMMENTS" },
          { field: "DATE", header: "LBLDATE" },
        ];

        this.requestCount = this.requestCols.length;
        this.approvalRequest = res["data"];
        this.showRequest = true;
      } else {
        this.showRequest = false;
      }
    });
  }
  // createItem(): FormGroup {
  //   return this.fb.group({
  //     MAINCATEGORYID: '',
  //     SUBCATEGORYID: '',
  //     SAFE: '',
  //     UNSAFE: ''
  //   });
  // }

  createFormBuilder(data) {
    this.confirmClass = "warning-msg";
    this.AddObservatinGeneral = this.fb.group({
      areaId: new FormControl(data.areaId),
      subAreaId: new FormControl(data.subAreaId),
      shiftId: new FormControl(data.shiftId),
      obsDate: new FormControl(data.obsDate, Validators.required),
      observerId: new FormControl(data.observerId, Validators.required),
      length: new FormControl(
        data.length,
        Validators.compose([
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.maxLength(3),
          //   this.noWhitespaceValidator
        ])
      ),

      peopleContact: new FormControl(
        data.peopleContact,
        Validators.compose([
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.maxLength(3),
          //   this.noWhitespaceValidator
        ])
      ),

      peopleObserver: new FormControl(
        data.peopleObserver,
        Validators.compose([
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.max(this.maxObservationBound),
          //   this.noWhitespaceValidator
        ])
      ),
      safeComment: new FormControl(data.safeComment),
      unsafeComment: new FormControl(data.unsafeComment),
      status: new FormControl(data.status, Validators.required),
    });
  }

  get observatinGeneralForm() {
    return this.AddObservatinGeneral.controls;
  }

  get observatinForm() {
    return this.observatinForm.controls;
  }

  monthDiff(dateFrom, dateTo) {
    return Math.floor(
      (Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate()) -
        Date.UTC(
          dateFrom.getFullYear(),
          dateFrom.getMonth(),
          dateFrom.getDate()
        )) /
        (1000 * 60 * 60 * 24)
    );
  }

  onSubmit(value: string) {
    if (this.AddObservatinGeneral.value["observerId"]) {
      this.approvalObserver = this.auth.rearrangeSelects(
        this.approvalObserver,
        this.AddObservatinGeneral.value["observerId"]
      );
    }

    this.confirmClass = "warning-msg";
    // var approvalCardId = this.approvalId.split("-");
    var parameters = {
      cardId: this.approvalId,
    };
    this.submitted = true;
    if (this.AddObservatinGeneral.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      var monthDiff = this.monthDiff(
        this.AddObservatinGeneral.value["obsDate"],
        new Date()
      );
      if (monthDiff > this.globalData.GlobalData["PREVIOUSDAYLIMITONOBSDATE"]) {
        this.submitted = false;
        this.translate
          .get(["ALTPREVIOUSDAYSLIMITONOBSDATE", "ALTPRIOROBSDATE"])
          .subscribe((message) => {
            this.messageService.add({
              severity: "error",
              detail:
                message["ALTPREVIOUSDAYSLIMITONOBSDATE"] +
                " " +
                this.globalData.GlobalData["PREVIOUSDAYLIMITONOBSDATE"] +
                " " +
                message["ALTPRIOROBSDATE"],
            });
          });
      } else {
        var finalField = [];
        this.fieldList.map((field, key) => {
          if (this.AddObservatinGeneral.value["Field" + (key + 1)]) {
            finalField.push(
              this.AddObservatinGeneral.value["Field" + (key + 1)]
            );
          }
          delete this.AddObservatinGeneral.value["Field" + (key + 1)];
        });
        if (finalField.length > 0) {
          this.AddObservatinGeneral.value[
            "customFieldvalueId"
          ] = finalField.join();
        } else {
          this.AddObservatinGeneral.value["customFieldValueId"] = "";
        }
        if (this.AddObservatinGeneral.value["observerId"] != null) {
          this.AddObservatinGeneral.value[
            "observerId"
          ] = this.AddObservatinGeneral.value["observerId"].join();
        }
        this.AddObservatinGeneral.value["cardId"] = this.approvalId;
        this.AddObservatinGeneral.value[
          "checkListId"
        ] = this.approvalCheckListId;
        this.AddObservatinGeneral.value["siteId"] = this.approvalSitesId;

        var obsDate = this.AddObservatinGeneral.value["obsDate"];
        if (this.dateFormatType.toLowerCase() == "dd/mm/yyyy") {
          if (
            this.AddObservatinGeneral.value["obsDate"].toString().search("/") !=
            -1
          ) {
            var dateStart = this.AddObservatinGeneral.value["obsDate"].split(
              "/"
            );
            obsDate = new Date(
              dateStart[1] + "/" + dateStart[0] + "/" + dateStart[2]
            );
          }
        }
        this.AddObservatinGeneral.value["obsDate"] = formatDate(
          obsDate,
          "MM/dd/yyyy",
          this.translate.getDefaultLang()
        );

        this.approvalservice
          .insertObservationsGnData(this.AddObservatinGeneral.value)
          .subscribe((res) => {
            if (res["status"] == true) {
              this.submitted = false;
              this.AddObservatinGeneral.markAsPristine();
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            } else {
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "error",
                  detail: res,
                });
              });
            }
          });
      }
    }
  }

  onSubmitRequest(value: string) {
    this.confirmClass = "warning-msg";

    var parameters = {
      cardId: this.approvalId,
    };
    this.submittedRequest = true;
    if (this.addComment.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.commentLoading = true;
      if (this.checkVal == 1) {
        this.checkVal = 2;
        this.addComment.value["cardId"] = this.approvalId;
        this.addComment.value["requestType"] = "approvalComment";
        this.approvalservice
          .updateRequest(this.addComment.value)
          .subscribe((res) => {
            if (res["status"] == true) {
              this.submittedRequest = false;

              this.getUpdateRequest();
              this.addComment.reset();
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
              this.commentLoading = false;
            } else {
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "error",
                  detail: res,
                });
              });
              this.commentLoading = false;
            }
          });
      }
    }
  }

  convertDate(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join("/");
  }

  changeApprovalStatus() {
    this.confirmClass = "warning-msg";

    var params = {
      cardId: this.approvalId,
    };
    this.translate.get("ALERTAPPROVESELDATA").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          this.approvalservice.changeApprovalStatus(params).subscribe((res) => {
            this.translate.get(res["message"]).subscribe((message) => {
              if (res["status"] == true) {
                this.cookieService.set("change-approval", message);
                this.route.navigate(["./approval-listing"], {
                  skipLocationChange: true,
                });
              } else {
                this.messageService.add({
                  severity: "error",
                  detail: message,
                });
              }
            });
          });
        },
        reject: () => {},
      });
    });
  }

  CategoryComments() {
    this.display2 = true;
  }

  onEnter() {
    // this.addThing();
  }
  get things() {
    return this.myForm.get("things") as FormArray;
  }

  private createForm() {
    this.myForm = this.fb.group({
      things: this.fb.array([this.fb.control("")]),
    });
  }

  private addThing() {
    this.things.push(this.fb.control(""));
  }

  Responsibleperson(e) {
    this.marked = e.target.checked;
  }

  resetApprovalForm() {
    this.AddObservatinGeneral.patchValue(this.initialGeneralFormData);
  }

  onSelectAll() {
    const selected = this.approvalObserver.map((item) => item.USERID);
    this.AddObservatinGeneral.get("observerId").patchValue(selected);
    this.AddObservatinGeneral.get("observerId").markAsDirty();
  }

  onClearAll() {
    this.AddObservatinGeneral.get("observerId").patchValue([]);
    this.AddObservatinGeneral.get("observerId").markAsDirty();
  }

  onShowRequest() {
    this.showRequest = true;
    setTimeout(() => {
      this.index = 2;
    }, 1000);
  }

  observationInput(value, controlName, rowID) {
    // const obj = { value, controlName, rowID };
    // this.customInput.next(obj);
    if (isNaN(value)) {
      // this.checkError = true;
      this.translate.get("ALTSAFEUNSAFECOUNT").subscribe((message) => {
        this.messageService.add({
          severity: "info",
          detail: message,
        });
      });
      this.observationForm.controls[controlName + rowID].setValue("");
    } else if (parseInt(value) > this.maxPeopleObserved) {
      // this.checkError = true;
      this.translate
        .get("ALTMAXLIMITONSAFEUNSAFECOUNT")
        .subscribe((message) => {
          this.messageService.add({
            severity: "info",
            detail: message + " " + this.maxPeopleObserved,
          });
        });
      this.observationForm.controls[controlName + rowID].setValue("");
      // val = val > 100 ? 100 : val;
      // this.observationForm.controls[ctrl].setValue(val);
    } else {
      this.observationForm.controls[controlName + rowID].setValue(value);
    }
  }
}
