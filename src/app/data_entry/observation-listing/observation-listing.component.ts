import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";

import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import { CloneVisitor } from "@angular/compiler/src/i18n/i18n_ast";
import { TranslateService } from "@ngx-translate/core";
import { formatDate } from "@angular/common";
import { EnvService } from "src/env.service";
import {
  trigger,
  transition,
  style,
  animate,
  query,
  stagger,
} from "@angular/animations";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { Router, RoutesRecognized } from "@angular/router";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService, SortEvent } from "primeng/api";

import { ObservationChecklistService } from "../../_services/observation-checklist.service";
import { AuthenticationService } from "../../_services/authentication.service";
import { SlideUpDownAnimations } from "../../_animations/slide-up-down.animations";
import { from } from "rxjs";
import { filter, pairwise } from "rxjs/operators";

import * as moment from "moment";
import { GlobalDataService } from "src/app/_services/global-data.service";
declare var $: any;

@Component({
  selector: "app-observation-listing",
  templateUrl: "./observation-listing.component.html",
  styleUrls: ["./observation-listing.component.css"],
  providers: [ConfirmationService, MessageService],
  animations: [SlideUpDownAnimations],
})
export class ObservationListingComponent implements OnInit {
  //  Variable Declaration
  public show_advance_search: boolean = false;
  iconType: string = "ui-icon-add";
  colsCount: any;
  allObserverDataList: any;
  observerDataList: any;
  observerDataCols: any[];
  observerSearchCols: any = [];
  selectedDelChecklist: any = [];
  siteList: any = [];
  areaList: any = [];
  subAreaList: any = [];
  shiftList: any = [];
  checkList: any = [];
  observerList: any = [];
  status: any;
  selectedSiteName: any = [];
  selectedAreaName: any = [];
  selectedSubAreaName: any = [];
  selectedShiftName: any = [];
  selectedObserver: any = [];
  selectedChecklist: any = [];
  selectedObservationDate: any;
  selectedStatus: any = [];
  selectedDisplayIncomplete: any = [];
  selectedUnsafeChecklist: any = [];
  selectedUnsafeChecklistCA: any = [];
  animationState = "out";
  displayIncomplete: any;
  unSafeChecklist: any;
  unSafeChecklistCA: any;
  selectedField: any;
  searchText: any = "";
  isLoaded: boolean;
  defaultParameters: any = {};

  successKey: any;
  Months: any = [];
  obsDateCheck: any = "FTDATE";
  oldObsDateCheck: any = "FTDATE";
  selectMonth: any = 1;
  includeToday: any = 0;
  currDate: any = new Date();
  roleId: any;
  yearList: any;
  approvalStatus: any = [];
  selectedApproval: any = 0;
  dateFormat: any;
  fromDate: any;
  toDate: any;
  oldFromDate: any;
  oldToDate: any;
  appName: any;
  observationDate: boolean = false;
  toDateBound: any = new Date();
  selecetedDateFilter: boolean = false;
  confirmClass: any;
  dateFormatType: any;
  viewFromDate: any;
  viewToDate: any;
  parameters: any;
  previousUrl: any;
  subAreaOptionValue: any;
  userId: any;
  approvalPermission: any;
  locale: any;

  // Breadcrumb Service
  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private obcService: ObservationChecklistService,
    private auth: AuthenticationService,
    private route: Router,
    private env: EnvService,
    private translate: TranslateService,
    private globalData: GlobalDataService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLDATAENTRY",
        url: "./assets/help/observation-checklist-listing.md",
      },
      {
        label: "LBLOBSERVATIONCHECKLIST",
        routerLink: ["/observation-listing"],
      },
    ]);
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    this.approvalPermission = this.globalData.GlobalData["OBSAPPROBEPC"];
    this.userId = this.auth.UserInfo["userId"];
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.route.events
      .pipe(
        filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((e: any) => {
        var url = e[0].urlAfterRedirects; // previous url
        localStorage.setItem("previousUrl-" + this.userId, url);
      });
    // Table header

    if (this.subAreaOptionValue == 0) {
      this.observerDataCols = [
        { field: "CARDID", header: "LBLCHECKLISTNO" },
        { field: "OBSERVATION DATE", header: "LBLOBSDATE" },
        { field: "OBSERVER", header: "LBLOBSERVER" },
        { field: "AREAS", header: "LBLAREAS" },
        { field: "STATUS", header: "LBLSTATUS" },
      ];
    } else {
      this.observerDataCols = [
        { field: "CARDID", header: "LBLCHECKLISTNO" },
        { field: "OBSERVATION DATE", header: "LBLOBSDATE" },
        { field: "OBSERVER", header: "LBLOBSERVER" },
        { field: "AREAS", header: "LBLAREAS" },
        { field: "SUBAREANAME", header: "LBLSUBAREAS" },
        { field: "STATUS", header: "LBLSTATUS" },
      ];
    }
    var cDate = this.currDate;
    var cMonth = cDate.getMonth() + 1;
    var cYear = cDate.getFullYear();

    this.approvalStatus = [
      { label: "ALL", value: 0 },
      { label: "Update Request", value: "LBLUPDATEREQUESTED" },
      { label: "Re-Submitted", value: "LBLRESUBMITTED" },
      { label: "Waiting Approval", value: "LBLWAITINGAPPROVAL" },
      { label: "Approved", value: "LBLAPPROVED" },
    ];

    this.observerDataCols.map((item, key) => {
      this.translate.get(item.header).subscribe((text: string) => {
        if (key != 5) {
          this.observerSearchCols.push({
            field: item.field,
            header: text,
          });
        }
      });
      this.selectedField = "CARDID";
    });

    for (var i = 1; i <= 30; i++) {
      this.Months.push({ label: i, value: i });
    }

    //Year Rang
    var previous_year = this.currDate.getFullYear() - 10;
    var next_year = this.currDate.getFullYear() + 10;
    this.yearList = previous_year + ":" + next_year;

    //  col of status
    this.colsCount = this.observerDataCols.length;

    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((resLabel) => {
      this.status = [];
      this.status.push({ label: resLabel["LBLACTIVE"], value: 1 });
      this.status.push({ label: resLabel["LBLINACTIVE"], value: 0 });
    });

    this.displayIncomplete = [{ label: "Yes", value: 1 }];

    this.unSafeChecklist = [{ label: "Yes", value: 1 }];

    this.unSafeChecklistCA = [{ label: "Yes", value: 1 }];

    this.roleId = this.auth.UserInfo["roleId"];

    this.dateFormat = this.auth.UserInfo["dateFormat"].toLowerCase();
    this.dateFormatType = this.dateFormat.replace("mm", "MM");
    this.dateFormat = this.dateFormat.replace("yyyy", "yy");

    this.checkFilter();
    this.appName = this.env.appName;
  }

  setToDateBound() {
    this.toDate = "";
    this.toDateBound = this.fromDate;
  }

  checkFilter() {
    this.parameters = {};
    this.previousUrl = localStorage.getItem("previousUrl-" + this.userId);
    if (this.previousUrl) {
      if (
        this.previousUrl.search("/add-observation-list") != -1 ||
        this.previousUrl.search("/edit-observation-list/") != -1
      ) {
        if (localStorage.getItem("filterInfo")) {
          this.toggleFilterShow();
          // this.animationState = "in";
          this.parameters = JSON.parse(localStorage.getItem("filterInfo"));
          var keys = Object.keys(this.parameters);
          keys.map((param) => {
            if (this.parameters[param]) {
              if (param == "siteId") {
                this.selectedSiteName = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "areaId") {
                this.selectedAreaName = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "subAreaId") {
                this.selectedSubAreaName = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "shiftId") {
                this.selectedShiftName = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "observerId") {
                this.selectedObserver = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "checkListId") {
                this.selectedChecklist = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "statusId") {
                this.selectedStatus = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "displayIncomplete") {
                this.selectedDisplayIncomplete = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "unsafeChecklist") {
                this.selectedUnsafeChecklist = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "unsafeChecklistCA") {
                this.selectedUnsafeChecklistCA = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "filterDateType") {
                this.obsDateCheck = this.parameters[param];
                this.oldObsDateCheck = this.parameters[param];
              }
              if (param == "fromDate") {
                this.fromDate = this.parameters[param];
                if (this.dateFormatType.toLowerCase() == "dd/mm/yyyy") {
                  if (
                    this.fromDate &&
                    this.fromDate.toString().search("/") != -1
                  ) {
                    this.fromDate = this.fromDate.split("/");
                    this.fromDate =
                      this.fromDate[1] +
                      "/" +
                      this.fromDate[0] +
                      "/" +
                      this.fromDate[2];
                  }
                } else if (this.dateFormatType.toLowerCase() == "yyyy/mm/dd") {
                  if (
                    this.fromDate &&
                    this.fromDate.toString().search("/") != -1
                  ) {
                    this.fromDate = this.fromDate.split("/");
                    this.fromDate =
                      this.fromDate[2] +
                      "/" +
                      this.fromDate[0] +
                      "/" +
                      this.fromDate[1];
                  }
                }
                this.viewFromDate = this.fromDate;
              }
              if (param == "toDate") {
                this.toDate = this.parameters[param];
                if (this.dateFormatType.toLowerCase() == "dd/mm/yyyy") {
                  if (this.toDate && this.toDate.toString().search("/") != -1) {
                    this.toDate = this.toDate.split("/");
                    this.toDate =
                      this.toDate[1] +
                      "/" +
                      this.toDate[0] +
                      "/" +
                      this.toDate[2];
                  }
                } else if (this.dateFormatType.toLowerCase() == "yyyy/mm/dd") {
                  if (this.toDate && this.toDate.toString().search("/") != -1) {
                    this.toDate = this.toDate.split("/");
                    this.toDate =
                      this.toDate[2] +
                      "/" +
                      this.toDate[0] +
                      "/" +
                      this.toDate[1];
                  }
                }
                this.viewToDate = this.toDate;
              }

              // this.selectedFilter[param] = this.parameters[param]
              //   .split(",")
              //   .map(Number);
            }
          });
          localStorage.removeItem("previousUrl-" + this.userId);
        } else {
          localStorage.removeItem("filterInfo");
          this.selectedSiteName = [];
          this.selectedSiteName.push(this.auth.UserInfo["defaultSiteId"]);
          this.fromDate = "";
          this.toDate = "";
        }
      } else {
        localStorage.removeItem("filterInfo");
        this.selectedSiteName = [];
        this.selectedSiteName.push(this.auth.UserInfo["defaultSiteId"]);
        this.fromDate = "";
        this.toDate = "";
      }
    } else {
      localStorage.removeItem("filterInfo");
      this.selectedSiteName = [];
      this.selectedSiteName.push(this.auth.UserInfo["defaultSiteId"]);
      this.fromDate = "";
      this.toDate = "";
    }

    this.getObservationList(this.parameters);
  }

  getObservationList(defaultParameters) {
    this.obcService.getObservationList(defaultParameters).subscribe((res) => {
      if (res["status"] == true) {
        // this.siteList = res["sites"];
        // this.siteListing = res["available"];
        this.siteList = [];
        var siteListVr = res["sites"];
        siteListVr.map((data) => {
          this.siteList.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });

        // this.areaList = res["area"];
        // this.siteListing = res["available"];
        this.areaList = [];
        var areaListVr = res["area"];
        areaListVr.map((data) => {
          this.areaList.push({
            label: data["AREANAME"],
            value: data["AREAID"],
          });
        });

        // this.subAreaList = res["subArea"];
        this.subAreaList = [];
        var subAreaListVr = res["subArea"];
        subAreaListVr.map((data) => {
          this.subAreaList.push({
            label: data["SUBAREANAME"],
            value: data["SUBAREAID"],
          });
        });

        // this.shiftList = res["shifts"];
        this.shiftList = [];
        var shiftListVr = res["shifts"];
        shiftListVr.map((data) => {
          this.shiftList.push({
            label: data["SHIFTNAME"],
            value: data["SHIFTID"],
          });
        });

        // this.checkList = res["checkList"];
        this.checkList = [];
        var checkListVr = res["checkList"];
        checkListVr.map((data) => {
          this.checkList.push({
            label: data["SETUPNAME"],
            value: data["CHECKLISTSETUPID"],
          });
        });

        this.observerList = [];
        var observerListVr = res["observer"];
        observerListVr.map((data) => {
          this.observerList.push({
            label: data["FULLNAME"],
            value: data["USERID"],
          });
        });

        var defaultFilter = res["defaultFilter"][0]["OBSLISTINGXMONTHSVAL"];
        var defaultFilterArr = defaultFilter.split("-");

        if (!this.fromDate && !this.toDate) {
          this.fromDate = formatDate(
            new Date(defaultFilterArr[0]),
            this.dateFormatType,
            this.translate.getDefaultLang()
          );
          this.toDate = formatDate(
            new Date(defaultFilterArr[1]),
            this.dateFormatType,
            this.translate.getDefaultLang()
          );
          this.oldFromDate = formatDate(
            new Date(defaultFilterArr[0]),
            this.dateFormatType,
            this.translate.getDefaultLang()
          );
          this.oldToDate = formatDate(
            new Date(defaultFilterArr[1]),
            this.dateFormatType,
            this.translate.getDefaultLang()
          );
          this.viewFromDate = this.fromDate;
          this.viewToDate = this.toDate;
        }

        this.selecetedDateFilter = true;
        if (res["list"].length > 0) {
          this.allObserverDataList = res["list"];
          this.observerDataList = res["list"];
        } else {
          this.observerDataList = [];
        }
      }
      // if (this.previousUrl) {
      //   if (
      //     this.previousUrl.search("/add-observation-list") == -1 &&
      //     this.previousUrl.search("/edit-observation-list/") == -1
      //   ) {
      //     this.selectedSiteName = [];
      //     this.selectedSiteName.push(this.auth.UserInfo["defaultSiteId"]);
      //   }
      // } else {
      //   this.selectedSiteName = [];
      //   this.selectedSiteName.push(this.auth.UserInfo["defaultSiteId"]);
      // }

      if (this.selectedSiteName) {
        this.siteList = this.auth.rearrangeSelects(
          this.siteList,
          this.selectedSiteName
        );
      }
      if (this.selectedAreaName) {
        this.areaList = this.auth.rearrangeSelects(
          this.areaList,
          this.selectedAreaName
        );
      }
      if (this.selectedSubAreaName) {
        this.subAreaList = this.auth.rearrangeSelects(
          this.subAreaList,
          this.selectedSubAreaName
        );
      }
      if (this.selectedShiftName) {
        this.shiftList = this.auth.rearrangeSelects(
          this.shiftList,
          this.selectedShiftName
        );
      }
      if (this.selectedObserver) {
        this.observerList = this.auth.rearrangeSelects(
          this.observerList,
          this.selectedObserver
        );
      }
      if (this.selectedChecklist) {
        this.checkList = this.auth.rearrangeSelects(
          this.checkList,
          this.selectedChecklist
        );
      }
      if (this.selectedStatus) {
        this.status = this.auth.rearrangeSelects(
          this.status,
          this.selectedStatus
        );
      }
      if (this.selectedDisplayIncomplete) {
        this.displayIncomplete = this.auth.rearrangeSelects(
          this.displayIncomplete,
          this.selectedDisplayIncomplete
        );
      }
      if (this.selectedUnsafeChecklist) {
        this.unSafeChecklist = this.auth.rearrangeSelects(
          this.unSafeChecklist,
          this.selectedUnsafeChecklist
        );
      }
      if (this.selectedUnsafeChecklistCA) {
        this.unSafeChecklistCA = this.auth.rearrangeSelects(
          this.unSafeChecklistCA,
          this.selectedUnsafeChecklistCA
        );
      }
      this.isLoaded = true;
    });
  }

  // Filtering
  filterData() {
    if (this.selectedSiteName == "") {
      this.translate.get(["ALTOBSCHECKLISTFILTER"]).subscribe((res: Object) => {
        this.messageService.add({
          severity: "info",
          detail: res["ALTOBSCHECKLISTFILTER"],
        });
      });
      return false;
    }

    if (this.selectedSiteName) {
      var selectedSiteName = this.selectedSiteName.join();
    }
    if (this.selectedAreaName) {
      var selectedAreaName = this.selectedAreaName.join();
    }
    if (this.selectedSubAreaName) {
      var selectedSubAreaName = this.selectedSubAreaName.join();
    }
    if (this.selectedShiftName) {
      var selectedShiftName = this.selectedShiftName.join();
    }

    if (this.selectedObserver) {
      var selectedObserver = this.selectedObserver.join();
    }

    var selectedChecklist = this.selectedChecklist.join();
    var selectedStatus = this.selectedStatus.join();
    var selectedDisplayIncomplete = this.selectedDisplayIncomplete.join();
    var selectedUnsafeChecklist = this.selectedUnsafeChecklist.join();
    var selectedUnsafeChecklistCA = this.selectedUnsafeChecklistCA.join();

    var cDate = this.currDate;
    var cMonth = cDate.getMonth() + 1;
    var cYear = cDate.getFullYear();
    var formatedDate = formatDate(
      cDate,
      "MM/dd/yyyy",
      this.translate.getDefaultLang()
    );
    var fromDate;
    var toDate;
    if (this.obsDateCheck == "YTD") {
      fromDate = "01/01/" + cYear;
      toDate = formatedDate;
    } else if (this.obsDateCheck == "CMY") {
      fromDate = cMonth + "/01/" + cYear;
      toDate = formatedDate;
    } else if (this.obsDateCheck == "PRVMONTH") {
      fromDate = this.selectMonth;

      if (this.includeToday[0] == "1") {
        toDate = 1;
      } else {
        toDate = 0;
      }
    } else if (this.obsDateCheck == "FTDATE") {
      var newFromDate = this.fromDate;
      var newToDate = this.toDate;
      if (this.dateFormatType.toLowerCase() == "dd/mm/yyyy") {
        if (newFromDate && newFromDate.toString().search("/") != -1) {
          newFromDate = this.fromDate.split("/");
          newFromDate =
            newFromDate[1] + "/" + newFromDate[0] + "/" + newFromDate[2];
        }
        if (newToDate && newToDate.toString().search("/") != -1) {
          newToDate = this.toDate.split("/");
          newToDate = newToDate[1] + "/" + newToDate[0] + "/" + newToDate[2];
        }
      }
      fromDate = formatDate(
        newFromDate,
        "MM/dd/yyyy",
        this.translate.getDefaultLang()
      );

      toDate = formatDate(
        newToDate,
        "MM/dd/yyyy",
        this.translate.getDefaultLang()
      );

      if (this.toDate == null) {
        this.translate.get(["FMKPLS", "LBLMAILTO"]).subscribe((res: Object) => {
          this.messageService.add({
            severity: "info",
            detail: res["FMKPLS"] + res["LBLMAILTO"],
          });
        });
        return false;
      }
    }

    this.defaultParameters = {
      siteId: selectedSiteName,
      areaId: selectedAreaName,
      subAreaId: selectedSubAreaName,
      shiftId: selectedShiftName,
      observerId: selectedObserver,
      fromDate: fromDate,
      toDate: toDate,
      checkListId: selectedChecklist,
      statusId: selectedStatus,
      displayIncomplete: selectedDisplayIncomplete,
      unsafeChecklist: selectedUnsafeChecklist,
      unsafeChecklistCA: selectedUnsafeChecklistCA,
      filterDateType: this.obsDateCheck,
    };

    this.isLoaded = false;
    this.observerDataList = [];
    localStorage.removeItem("filterInfo");
    localStorage.setItem("filterInfo", JSON.stringify(this.defaultParameters));
    this.getObservationList(this.defaultParameters);
  }

  serchByApproval() {
    var fieldStatus = "STATUS";
    var field = this.selectedField;
    var status;
    var stext;
    var fn = this;
    this.isLoaded = false;
    if (this.selectedApproval != "") {
      status = this.selectedApproval;
      if (field != "CARDID") {
        stext = this.searchText.toLowerCase();
      } else {
        stext = this.searchText.toString();
      }
      var newList = this.allObserverDataList.filter((item) => {
        if (field != "CARDID") {
          if (status != 0) {
            return (
              item[field].toLowerCase().indexOf(stext) >= 0 &&
              item[fieldStatus].toString().indexOf(status) >= 0
            );
          } else {
            return item[field].toLowerCase().indexOf(stext) >= 0;
          }
        } else {
          if (status != 0) {
            return (
              item[field].toString().indexOf(stext) >= 0 &&
              item[fieldStatus].toString().indexOf(status) >= 0
            );
          } else {
            return item[field].toString().indexOf(stext) >= 0;
          }
        }
      });
      this.observerDataList = newList;
      setTimeout(function () {
        fn.isLoaded = true;
      }, 10);
    } else {
      this.observerDataList = this.allObserverDataList;
      setTimeout(function () {
        fn.isLoaded = true;
      }, 10);
    }
  }

  getAreaBySite() {
    var selectedSiteName = this.selectedSiteName.join();

    var params = {
      siteId: selectedSiteName,
    };
    this.obcService.getAreaFilter(params).subscribe((res) => {
      if (res["status"] == true) {
        // this.areaList = res["area"];
        this.areaList = [];
        var areaListVr = res["area"];
        areaListVr.map((data) => {
          this.areaList.push({
            label: data["AREANAME"],
            value: data["AREAID"],
          });
        });

        this.shiftList = [];
        var shiftListVr = res["shift"];
        shiftListVr.map((data) => {
          this.shiftList.push({
            label: data["SHIFTNAME"],
            value: data["SHIFTID"],
          });
        });

        this.observerList = [];
        var observerListVr = res["observer"];
        observerListVr.map((data) => {
          this.observerList.push({
            label: data["FULLNAME"],
            value: data["USERID"],
          });
        });
        // this.shiftList = res["shift"];
        // this.observerList = res["observer"];
        this.selectedAreaName = "";
        this.selectedShiftName = "";
        this.selectedObserver = "";
      } else {
        this.areaList = [];
        this.shiftList = [];
        this.observerList = [];
      }
    });
  }

  getSubAreaByArea() {
    var selectedAreaName = this.selectedAreaName.join();

    var params = {
      areaId: selectedAreaName,
    };
    this.obcService.getSubAreaFilter(params).subscribe((res) => {
      if (res["status"] == true) {
        var finalSubArea = [];
        if (res["subArea"].length > 0) {
          res["subArea"].map((item) => {
            finalSubArea.push({
              label: item["SUBAREANAME"],
              value: item["SUBAREAID"],
            });
          });
        }
        this.subAreaList = finalSubArea;
      } else {
        this.subAreaList = [];
      }
    });
  }

  // onTextSearch data
  onTextSearch() {
    var fieldStatus = "STATUS";
    var status = this.selectedApproval;
    var field = this.selectedField;
    var stext;
    var fn = this;
    // this.isLoaded = false;
    if (this.searchText != "") {
      if (field != "CARDID") {
        stext = this.searchText.toLowerCase();
      } else {
        stext = this.searchText.toString();
      }
      var newList = this.allObserverDataList.filter((item) => {
        if (field != "CARDID") {
          if (status != 0) {
            return (
              item[field].toLowerCase().indexOf(stext) >= 0 &&
              item[fieldStatus].toString().indexOf(status) >= 0
            );
          } else {
            return item[field].toLowerCase().indexOf(stext) >= 0;
          }
        } else {
          if (status != 0) {
            return (
              item[field].toString().indexOf(stext) >= 0 &&
              item[fieldStatus].toString().indexOf(status) >= 0
            );
          } else {
            return item[field].toString().indexOf(stext) >= 0;
          }
        }
      });
      this.observerDataList = newList;
    } else {
      this.observerDataList = this.allObserverDataList;
      setTimeout(function () {
        fn.isLoaded = true;
      }, 10);
    }
  }

  onRowSelect(data) {
    localStorage.setItem(
      "editpreviousUrl-" + this.userId,
      "observation-listing"
    );
    this.route.navigate(
      ["/edit-observation-list/" + data.CARDID + "/" + data.CARDREQID],
      {
        skipLocationChange: true,
      }
    );
  }

  onRowSelectNew(data) {
    this.route.navigate(
      ["/edit-observation-list-new/" + data.CARDID + "/" + data.CARDREQID],
      {
        skipLocationChange: true,
      }
    );
  }

  // Advance serach
  advance_search() {
    this.iconType =
      this.iconType === "ui-icon-add" ? "ui-icon-remove" : "ui-icon-add";
    this.show_advance_search = !this.show_advance_search;
  }

  deleteRecord() {
    this.confirmClass = "warning-msg";
    var msg = "";
    this.translate.get("ALTDELCONFIRM").subscribe((confirmMsg) => {
      msg = confirmMsg;
    });
    this.confirmationService.confirm({
      message: msg,
      accept: () => {
        var checklistIds = [];
        this.selectedDelChecklist.map((checkList) => {
          checklistIds.push(checkList["CARDREQID"]);
        });
        var cardId = checklistIds.join();
        this.obcService.deleteChecklist({ cardId: cardId }).subscribe((res) => {
          this.translate.get(res["message"]).subscribe((message) => {
            if (res["status"] == true) {
              this.getObservationList(this.defaultParameters);
              this.messageService.add({
                severity: "success",
                detail: message,
              });
            } else {
              this.messageService.add({
                severity: "error",
                detail: message,
              });
            }
          });
        });
        this.selectedDelChecklist = [];
        this.searchText = "";
        this.getObservationList(this.defaultParameters);
      },
      reject: () => {
        this.selectedDelChecklist = [];
        this.searchText = "";
        this.getObservationList(this.defaultParameters);
      },
    });
  }

  toggleFilterShow() {
    this.animationState = this.animationState === "out" ? "in" : "out";
    // this.selectedSiteName = [];
    // this.selectedSiteName.push(this.auth.UserInfo["defaultSiteId"]);
  }

  onSelectAll() {
    const selected = this.siteList.map((item) => item.SITEID);
    this.selectedSiteName = selected;
    this.getAreaBySite();
  }

  onClearAll() {
    this.selectedSiteName = [];
    this.getAreaBySite();
  }

  onSelectAllArea() {
    const selectedArea = this.areaList.map((item) => item.AREAID);
    this.selectedAreaName = selectedArea;
    this.getSubAreaByArea();
  }

  onClearAllArea() {
    this.selectedAreaName = [];
    this.getSubAreaByArea();
  }

  onSelectAllSubArea() {
    const selectedSubArea = this.subAreaList.map((item) => item.SUBAREAID);
    this.selectedSubAreaName = selectedSubArea;
  }

  onClearAllSubArea() {
    this.selectedSubAreaName = [];
  }

  onSelectAllShift() {
    const selectedShift = this.shiftList.map((item) => item.SHIFTID);
    this.selectedShiftName = selectedShift;
  }

  onClearAllShift() {
    this.selectedShiftName = [];
  }

  onSelectAllObs() {
    const selectedObs = this.observerList.map((item) => item.USERID);
    this.selectedObserver = selectedObs;
  }

  onClearAllObs() {
    this.selectedObserver = [];
  }

  onSelectAllCheck() {
    const selectedCheck = this.checkList.map((item) => item.CHECKLISTSETUPID);
    this.selectedChecklist = selectedCheck;
  }

  onClearAllCheck() {
    this.selectedChecklist = [];
  }

  onSelectAllStatus() {
    const selectedStatus = this.status.map((item) => item.value);
    this.selectedStatus = selectedStatus;
  }

  onClearAllStatus() {
    this.selectedStatus = [];
  }

  onSelectAllObservations() {
    const selectedObs = this.displayIncomplete.map((item) => item.value);
    this.selectedDisplayIncomplete = selectedObs;
  }

  onClearAllObservations() {
    this.selectedDisplayIncomplete = [];
  }

  onSelectAllUnsafe() {
    const selectedUnsafe = this.unSafeChecklist.map((item) => item.value);
    this.selectedUnsafeChecklist = selectedUnsafe;
  }

  onClearAllUnsafe() {
    this.selectedUnsafeChecklist = [];
  }

  onSelectAllWithoutCAPA() {
    const selectedCAPA = this.unSafeChecklistCA.map((item) => item.value);
    this.selectedUnsafeChecklistCA = selectedCAPA;
  }

  onClearAllWithoutCAPA() {
    this.selectedUnsafeChecklistCA = [];
  }
  clearFilter() {
    this.obsDateCheck = "FTDATE";
    this.searchText = "";
    this.selectedSiteName = [];
    this.selectedAreaName = [];
    this.selectedSubAreaName = [];
    this.selectedShiftName = [];
    this.selectedObserver = [];
    this.selectedChecklist = [];
    this.selectedDisplayIncomplete = [];
    this.selectedStatus = [];
    this.selectedUnsafeChecklist = [];
    this.selectedUnsafeChecklistCA = [];
    this.selectedSiteName.push(this.auth.UserInfo["defaultSiteId"]);
    this.getObservationList({});
  }

  obsDateOpen() {
    if (
      this.obsDateCheck == "YTD" ||
      this.obsDateCheck == "CMY" ||
      this.obsDateCheck == "PRVMONTH"
    ) {
      this.fromDate = "";
      this.toDate = "";
    }
    this.observationDate = true;
    this.selecetedDateFilter = false;
  }

  dateSelected() {
    if (this.obsDateCheck == "FTDATE") {
      if (this.fromDate && this.toDate) {
        var startDate;
        var endDate;
        if (this.dateFormatType.toLowerCase() == "dd/mm/yyyy") {
          if (this.fromDate && this.fromDate.toString().search("/") != -1) {
            var fDate = this.fromDate.split("/");
            startDate = new Date(fDate[1] + "/" + fDate[0] + "/" + fDate[2]);
          } else {
            startDate = this.fromDate;
          }

          if (this.toDate && this.toDate.toString().search("/") != -1) {
            var tDate = this.toDate.split("/");
            endDate = new Date(tDate[1] + "/" + tDate[0] + "/" + tDate[2]);
          } else {
            endDate = this.toDate;
          }
        } else {
          startDate = new Date(this.fromDate);
          endDate = new Date(this.toDate);
        }
      }
      if (this.obsDateCheck == "FTDATE" && !this.fromDate) {
        this.translate.get(["FMKPLS", "FMKFROM"]).subscribe((res: Object) => {
          this.messageService.add({
            severity: "info",
            detail: res["FMKPLS"] + " " + res["FMKFROM"],
          });
        });
        // this.fromDate = this.oldFromDate;
        // this.toDate = this.oldToDate;
      } else if (this.obsDateCheck == "FTDATE" && !this.toDate) {
        this.translate.get(["FMKPLS", "FMKTO"]).subscribe((res: Object) => {
          this.messageService.add({
            severity: "info",
            detail: res["FMKPLS"] + " " + res["FMKTO"],
          });
        });
        // this.fromDate = this.oldFromDate;
        // this.toDate = this.oldToDate;
      } else if (this.obsDateCheck == "FTDATE" && startDate > endDate) {
        this.translate.get(["FMKPRVD"]).subscribe((res: Object) => {
          this.messageService.add({
            severity: "info",
            detail: res["FMKPRVD"],
          });
        });
        // this.fromDate = this.oldFromDate;
        // this.toDate = this.oldToDate;
      } else {
        this.observationDate = false;
        this.selecetedDateFilter = true;
        var fromDate = this.fromDate;
        var toDate = this.toDate;
        if (this.dateFormatType.toLowerCase() == "dd/mm/yyyy") {
          if (fromDate && fromDate.toString().search("/") != -1) {
            fromDate = this.fromDate.split("/");
            fromDate = fromDate[1] + "/" + fromDate[0] + "/" + fromDate[2];
          }
          if (toDate && toDate.toString().search("/") != -1) {
            toDate = this.toDate.split("/");
            toDate = toDate[1] + "/" + toDate[0] + "/" + toDate[2];
          }
        }
        this.viewFromDate = formatDate(
          fromDate,
          this.dateFormatType,
          this.translate.getDefaultLang()
        );
        this.viewToDate = formatDate(
          toDate,
          this.dateFormatType,
          this.translate.getDefaultLang()
        );
      }
    } else {
      if (
        this.obsDateCheck == "YTD" ||
        this.obsDateCheck == "CMY" ||
        this.obsDateCheck == "PRVMONTH"
      ) {
        this.fromDate = "";
        this.toDate = "";
      } else {
        this.viewFromDate = formatDate(
          this.fromDate,
          this.dateFormatType,
          this.translate.getDefaultLang()
        );
        this.viewToDate = formatDate(
          this.toDate,
          this.dateFormatType,
          this.translate.getDefaultLang()
        );
      }
      this.observationDate = false;
      this.selecetedDateFilter = true;
    }
  }

  clearDaterange() {
    this.obsDateCheck = this.oldObsDateCheck;
    this.fromDate = this.oldFromDate;
    this.toDate = this.oldToDate;
  }

  cancelDialog() {
    this.obsDateCheck = this.oldObsDateCheck;
    this.fromDate = this.oldFromDate;
    this.toDate = this.oldToDate;
    this.observationDate = false;
    this.selecetedDateFilter = true;
  }

  convertDate(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join("/");
  }

  customSort(event: SortEvent) {
    event.data.sort((data1, data2) => {
      let value1 = data1[event.field];
      let value2 = data2[event.field];

      // if (this.isDateColumn(event.field)) {

      // }

      let result = null;

      if (value1 == null && value2 != null) result = -1;
      else if (value1 != null && value2 == null) result = 1;
      else if (value1 == null && value2 == null) result = 0;
      else if (typeof value1 === "string" && typeof value2 === "string") {
        const date1 = moment(
          value1,
          this.auth.UserInfo["dateFormat"].toUpperCase()
        );
        const date2 = moment(
          value2,
          this.auth.UserInfo["dateFormat"].toUpperCase()
        );

        let result: number = -1;
        if (moment(date2).isBefore(date1, "day")) {
          result = 1;
        }

        return result * event.order;
      }
      // result = value1.localeCompare(value2);
      else result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;

      return event.order * result;
    });
  }
}
