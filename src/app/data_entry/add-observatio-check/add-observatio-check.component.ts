import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { MessageService } from "primeng/api";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";

import { AuthenticationService } from "../../_services/authentication.service";

import { ObservationChecklistService } from "../../_services/observation-checklist.service";
import { CustomValidatorsService } from "../../_services/custom-validators.service";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-add-observatio-check",
  templateUrl: "./add-observatio-check.component.html",
  styleUrls: ["./add-observatio-check.component.css"],
  providers: [MessageService, CustomValidatorsService],
})
export class AddObservatioCheckComponent implements OnInit {
  // Varialbe for fields name
  AddObservatinGeneral: FormGroup;
  AddObservatinModal: FormGroup;
  AddCorrectiveActions: FormGroup;
  submitted: boolean;
  submitted1: boolean;

  display: boolean = false;
  display1: boolean = false;
  display2: boolean = false;
  site: any;
  Field3: any;
  date: any;
  area: any;
  subarea: any;
  sifts: any;
  Field1: any;
  Lenght: any;
  Checklistsetup: any;
  Peaoplecontacted: any;
  PeopleObserved: any;
  uploadedFiles: any[] = [];
  msgs: Message[];
  Department: any;
  Division: any;
  curDate = new Date();

  // Variables
  siteList: any;
  checkList: any;
  observersList: any = [];
  areaList: any;
  subAreaList: any = [];
  shiftsList: any;
  fieldList: any = [];
  groupList: any = [];
  userInfo: any;
  finalGroups: any = [];
  status: SelectItem[];
  generalFormData: any;
  observerFormData: any;
  configData: any;
  roleId: any;
  dateFormatType: any;
  dateFormatTypeNew: any;
  dateFormat: any;
  maxPeopleObserved: number;
  subAreaOptionValue: any;
  areaManagerOptionValue: any;
  areaManager: any;
  addNewObsPermission: any;
  locale: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private obcService: ObservationChecklistService,
    private auth: AuthenticationService,
    private customValidatorsService: CustomValidatorsService,
    private cookieService: CookieService,
    private router: Router,
    private translate: TranslateService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLDATAENTRY",
        url: "./assets/help/observation-checklist-add.md",
      },
      {
        label: "LBLOBSCHECKLIST",
        routerLink: ["/observation-listing"],
      },
    ]);
    if (this.cookieService.get("new-observer")) {
      var fn = this;
      setTimeout(function () {
        fn.messageService.add({
          severity: "success",
          detail: fn.cookieService.get("new-observer"),
        });
        fn.cookieService.delete("new-observer");
      }, 1000);
    }
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    // Add Observatin GeneralgroupList
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.areaManagerOptionValue = this.auth.UserInfo["areaManagerOptionValue"];
    this.userInfo = this.auth.UserInfo;
    var permissions = JSON.parse(
      localStorage.getItem("rolePermission-" + this.userInfo["userId"])
    );
    this.addNewObsPermission = permissions.filter((item) => {
      return item.PERMISSIONNAME == "LBLADDNEWOBS";
    });
    this.roleId = this.auth.UserInfo.roleId;
    this.dateFormatType = this.auth.UserInfo["dateFormat"];
    if (this.dateFormatType == "MM/dd/yyyy") {
      this.dateFormatTypeNew = "LBLMDY";
    } else if (this.dateFormatType == "dd/MM/yyyy") {
      this.dateFormatTypeNew = "LBLDMY";
    } else if (this.dateFormatType == "yyyy/MM/dd") {
      this.dateFormatTypeNew = "LBLYMD";
    }
    this.dateFormat = this.auth.UserInfo["dateFormat"].toLowerCase();
    this.dateFormat = this.dateFormat.replace("yyyy", "yy");
    var data = {
      siteId: this.userInfo.defaultSiteId,
      customFieldValueId: null,
      checkListSetupId: 0,
      observationDate: null,
      selObserverId: null,
      areaId: 0,
      subAreaId: 0,
      areaManagerId: 0,
      shiftId: 0,
      lengths: "",
      peopleContacted: "",
      peopleObserved: "",
      safeComments: "",
      unSafeComments: "",
      status: 1,
      maxPeopleObserved: "",
    };
    this.createObserverFormBuilder(data);

    // Add Observatin Modal

    this.AddObservatinModal = this.fb.group({
      siteId: new FormControl(this.userInfo.defaultSiteId),
      firstName: new FormControl("", [
        Validators.required,
        this.customValidatorsService.maxLengthValidator(256),
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      lastName: new FormControl("", [
        Validators.required,
        this.customValidatorsService.maxLengthValidator(256),
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      email: ["", [Validators.email, Validators.maxLength(256)]],
      jobTitle: new FormControl(null),
    });

    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((resLabel) => {
      this.status = [];
      this.status.push({ label: resLabel["LBLACTIVE"], value: 1 });
      this.status.push({ label: resLabel["LBLINACTIVE"], value: 0 });
    });

    // get new Form data
    this.getNewFormData({});
  }

  get GeneralSubForm() {
    return this.AddObservatinGeneral.controls;
  }

  getNewFormData(params) {
    this.obcService.getObsFormData(params).subscribe((res) => {
      if (res["status"] == true) {
        var checkListVal;
        this.translate.get("LBLSELECT").subscribe((selectLabel) => {
          this.siteList = res["sites"];
          this.siteList.splice(0, 0, { SITEID: 0, SITENAME: selectLabel });
          this.checkList = res["checkList"];
          if (this.checkList.length == 1) {
            checkListVal = this.checkList[0]["CHECKLISTSETUPID"];
          } else {
            checkListVal = 0;
          }
          this.checkList.splice(0, 0, {
            CHECKLISTSETUPID: 0,
            SETUPNAME: selectLabel,
          });

          // this.observersList = res["observer"];
          this.observersList = [];
          var observersListVr = res["observer"];
          observersListVr.map((data) => {
            this.observersList.push({
              label: data["USERNAME"],
              value: data["USERID"],
            });
          });

          this.areaList = res["area"];
          this.areaList.splice(0, 0, { AREAID: 0, AREANAME: selectLabel });
          this.subAreaList = res["subArea"];
          if (this.subAreaList) {
            this.subAreaList.splice(0, 0, {
              SUBAREAID: 0,
              SUBAREANAME: selectLabel,
            });
          } else {
            this.subAreaList = [];
            this.subAreaList.splice(0, 0, {
              SUBAREAID: 0,
              SUBAREANAME: selectLabel,
            });
          }

          this.areaManager = [];
          this.areaManager.splice(0, 0, {
            USERID: 0,
            FULLNAME: selectLabel,
          });

          this.shiftsList = res["shifts"];
          this.shiftsList.splice(0, 0, { SHIFTID: 0, SHIFTNAME: selectLabel });
        });

        this.configData = res["checkListConfig"];
        var siteid = this.userInfo.defaultSiteId;
        if (Object.keys(params).length > 0) {
          siteid = params["siteId"];
        }

        var obsId;
        if (this.roleId == 5) {
          obsId = [this.userInfo.userId];
        } else {
          obsId = null;
        }
        if (this.configData["MaxPeopleObservedLimit"] != "") {
          this.maxPeopleObserved = this.configData["MaxPeopleObservedLimit"];
        } else {
          this.maxPeopleObserved = 999;
        }
        var data = {
          siteId: siteid,
          customFieldValueId: null,
          checkListSetupId: checkListVal,
          observationDate: null,
          selObserverId: obsId,
          areaId: 0,
          subAreaId: 0,
          areaManagerId: 0,
          shiftId: 0,
          lengths: "",
          peopleContacted: "",
          peopleObserved: "",
          safeComments: "",
          unSafeComments: "",
          status: 1,
          maxPeopleObserved: this.maxPeopleObserved,
        };
        this.createObserverFormBuilder(data);
        this.generalFormData = this.AddObservatinGeneral.getRawValue();
        var fields = res["fieldName"];
        var fieldValue = res["fieldValue"];
        fields.map((field, key) => {
          var fieldval = fieldValue.filter((val) => {
            return val.CUSTOMFIELDID === field.CUSTOMFIELDID;
          });

          if (field["MANDATORY"] == 1) {
            this.AddObservatinGeneral.addControl(
              "Field" + (key + 1),
              new FormControl(0, [Validators.required, Validators.min(1)])
            );
          } else {
            this.AddObservatinGeneral.addControl(
              "Field" + (key + 1),
              new FormControl(0)
            );
          }
          this.translate.get("LBLSELECT").subscribe((selectLabel) => {
            fieldval.splice(0, 0, {
              CUSTOMFIELDVALUE: selectLabel,
              CUSTOMFIELDVALUEID: 0,
            });
            this.fieldList.push({
              fieldName: field["CUSTOMFIELDNAME"],
              mandatory: field["MANDATORY"],
              fieldValue: fieldval,
            });
          });
        });
      }
      this.createNewObserverAdd();
    });
  }

  createObserverFormBuilder(data) {
    this.AddObservatinGeneral = this.fb.group({
      siteId: new FormControl(data.siteId, [
        Validators.required,
        Validators.min(1),
      ]),
      customFieldValueId: new FormControl(data.customFieldValueId),
      observationDate: new FormControl(
        data.observationDate,
        Validators.required
      ),
      checkListSetupId: new FormControl(data.checkListSetupId, [
        Validators.required,
        Validators.min(1),
      ]),
      selObserverId: new FormControl(data.selObserverId, Validators.required),
      areaId: new FormControl(data.areaId, [
        Validators.required,
        Validators.min(1),
      ]),
      subAreaId: new FormControl(data.subAreaId),
      areaManagerId: new FormControl(data.areaManagerId),
      shiftId: new FormControl(data.shiftId, [
        Validators.required,
        Validators.min(1),
      ]),
      length: new FormControl(data.lengths, [
        Validators.required,
        this.customValidatorsService.numericValidator(3),
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      peopleContacted: new FormControl(data.peopleContacted, [
        Validators.required,
        this.customValidatorsService.numericValidator(3),
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      peopleObserved: new FormControl(data.peopleObserved, [
        Validators.required,
        ,
        this.customValidatorsService.numericValidator(3),
        Validators.max(this.maxPeopleObserved),
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      safeComments: new FormControl(data.safeComments, [
        this.customValidatorsService.maxLengthValidator(1024),
      ]),
      unSafeComments: new FormControl(data.unSafeComments, [
        this.customValidatorsService.maxLengthValidator(1024),
      ]),
      status: new FormControl(data.status),
    });
  }

  getSubArea() {
    var parameters = {
      siteId: this.AddObservatinGeneral.value["siteId"],
      areaId: this.AddObservatinGeneral.value["areaId"],
    };
    this.obcService.getSubAreaData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.translate.get("LBLSELECT").subscribe((selectLabel) => {
          this.subAreaList = res["subArea"];
          this.subAreaList.splice(0, 0, {
            SUBAREAID: 0,
            SUBAREANAME: selectLabel,
          });
        });
        this.translate.get("LBLSELECT").subscribe((selectLabel) => {
          this.areaManager = res["user"];
          this.areaManager.splice(0, 0, {
            USERID: 0,
            FULLNAME: selectLabel,
          });
        });

        this.AddObservatinGeneral.controls["subAreaId"].setValue(null);
        this.AddObservatinGeneral.controls["areaManagerId"].setValue(0);
      }
    });
  }

  getDataBySite(siteid) {
    this.fieldList = [];
    var params = {
      siteId: siteid,
    };

    this.getNewFormData(params);
  }

  checklistSubmit() {
    this.submitted = true;
    if (this.AddObservatinGeneral.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      if (this.configData["PreviousDaysLimitOnObservationDate"] != "") {
        var lastDate = new Date(
          this.curDate.getTime() -
            this.configData["PreviousDaysLimitOnObservationDate"] *
              24 *
              60 *
              60 *
              1000
        );
        if (lastDate >= this.AddObservatinGeneral.value["observationDate"]) {
          var message1;
          this.translate
            .get("ALTPREVIOUSDAYSLIMITONOBSDATE")
            .subscribe((msg1) => {
              message1 = msg1;
            });
          var message2;
          this.translate.get("ALTPRIOROBSDATE").subscribe((msg2) => {
            message2 = msg2;
          });
          this.messageService.add({
            severity: "info",
            detail:
              message1 +
              " " +
              this.configData["PreviousDaysLimitOnObservationDate"] +
              " " +
              message2,
          });
          return false;
        }
      }
      var finalField = [];
      this.fieldList.map((field, key) => {
        if (this.AddObservatinGeneral.value["Field" + (key + 1)]) {
          finalField.push(this.AddObservatinGeneral.value["Field" + (key + 1)]);
        }
        delete this.AddObservatinGeneral.value["Field" + (key + 1)];
      });
      if (finalField.length > 0) {
        this.AddObservatinGeneral.value[
          "customFieldValueId"
        ] = finalField.join();
      } else {
        this.AddObservatinGeneral.value["customFieldValueId"] = "";
      }
      this.AddObservatinGeneral.value[
        "selObserverId"
      ] = this.AddObservatinGeneral.value["selObserverId"].join();

      let d = new Date(this.AddObservatinGeneral.value["observationDate"]);
      this.AddObservatinGeneral.value["observationDate"] = `${
        d.getMonth() + 1
      }/${d.getDate()}/${d.getFullYear()}`;

      this.obcService
        .addObservationForm(this.AddObservatinGeneral.value)
        .subscribe((res) => {
          this.translate.get(res["message"]).subscribe((message) => {
            if (res["status"] == true) {
              // this.messageService.add({
              //   severity: "success",
              //   detail: message
              // });
              this.AddObservatinGeneral.value[
                "selObserverId"
              ] = this.AddObservatinGeneral.value["selObserverId"].split(",");
              this.cookieService.set("new-checklist", message);
              this.router.navigate(
                [
                  "./edit-observation-list/" +
                    parseInt(res["cardId"]) +
                    "/" +
                    res["cardReqId"],
                ],
                {
                  skipLocationChange: true,
                }
              );
            } else {
              this.messageService.add({
                severity: "error",
                detail: message,
              });
            }
          });
        });
    }
  }

  get ModalSubForm() {
    return this.AddObservatinModal.controls;
  }

  addNewObserver() {
    this.submitted1 = true;
    if (this.AddObservatinModal.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      var obsList = this.observersList;
      this.observersList = [];
      var groupIds = [];
      this.finalGroups.map((item, key) => {
        groupIds.push(this.AddObservatinModal.value["Group" + (key + 1)]);
        delete this.AddObservatinModal.value["Group" + (key + 1)];
      });
      this.AddObservatinModal.value["groupId"] = groupIds.join();
      this.obcService
        .addNewObserver(this.AddObservatinModal.value)
        .subscribe((res) => {
          this.translate.get(res["message"]).subscribe((message) => {
            if (res["status"] == true) {
              var data = {
                value: parseInt(res["observerId"]),
                label: res["observer"],
              };
              obsList.unshift(data);
              this.observersList = obsList;

              this.messageService.add({
                severity: "success",
                detail: message,
              });
              this.submitted1 = false;
              this.AddObservatinModal.patchValue(this.observerFormData);
              this.display = false;
            } else {
              this.messageService.add({
                severity: "error",
                detail: message,
              });
            }
          });
        });
    }
  }

  // modals
  openPopup() {
    this.display = true;
    this.submitted1 = false;
    this.AddObservatinModal.reset();
    this.AddObservatinModal.controls["siteId"].setValue(
      this.AddObservatinGeneral.value["siteId"]
    );
    this.observerFormData = this.AddObservatinModal.getRawValue();
  }

  createNewObserverAdd() {
    if (this.finalGroups.length == 0) {
      this.AddObservatinModal.value["siteId"] = this.AddObservatinGeneral.value[
        "siteId"
      ];
      this.obcService.getObserverAddData().subscribe((res) => {
        if (res["status"] == true) {
          var groupListing = res["groupType"];
          var groupData = res["groupData"];
          this.finalGroups = [];
          groupListing.map((groupType, key) => {
            var tempGroupData = groupData.filter((data) => {
              return groupType.GROUPTYPEID === data.GROUPTYPEID;
            });
            if (groupType.MANDATORY == 0) {
              this.AddObservatinModal.addControl(
                "Group" + (key + 1),
                new FormControl(0)
              );
            } else {
              this.AddObservatinModal.addControl(
                "Group" + (key + 1),
                new FormControl(0, Validators.required)
              );
            }
            this.translate.get("LBLSELECT").subscribe((res: string) => {
              const groupsSelect = JSON.parse(JSON.stringify(tempGroupData));
              groupsSelect.splice(0, 0, {
                GROUPID: 0,
                GROUPNAME: res,
              });
              tempGroupData = groupsSelect;
            });
            this.finalGroups.push({
              groupTypeName: groupType.GROUPTYPENAME,
              groupData: tempGroupData,
            });
          });
        }
      });
    }
  }
  AddCorrectiveActionsModal() {
    this.display1 = true;
  }
  CategoryComments() {
    this.display2 = true;
  }
  onUpload(event) {
    for (const file of event.files) {
      this.uploadedFiles.push(file);
    }

    this.msgs = [];
    this.msgs.push({ severity: "info", summary: "File Uploaded", detail: "" });
  }

  resetGeneralForm() {
    this.subAreaList = [];
    this.AddObservatinGeneral.patchValue(this.generalFormData);
  }

  resetObservationForm() {
    this.submitted1 = false;
    this.AddObservatinModal.patchValue(this.observerFormData);
  }

  onSelectAll() {
    const selected = this.observersList.map((item) => item.USERID);
    this.AddObservatinGeneral.get("selObserverId").patchValue(selected);
    this.AddObservatinGeneral.get("selObserverId").markAsDirty();
  }

  onClearAll() {
    this.AddObservatinGeneral.get("selObserverId").patchValue([]);
    this.AddObservatinGeneral.get("selObserverId").markAsDirty();
  }
}
