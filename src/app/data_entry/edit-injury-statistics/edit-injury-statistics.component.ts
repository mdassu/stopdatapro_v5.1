import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";

import { Router, ActivatedRoute } from "@angular/router";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { InjuryStatisticsService } from "src/app/_services/injury-statistics.service";
import { CustomValidatorsService } from "../../_services/custom-validators.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "../../_services/authentication.service";

@Component({
  selector: "app-edit-injury-statistics",
  templateUrl: "./edit-injury-statistics.component.html",
  styleUrls: ["./edit-injury-statistics.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class EditInjuryStatisticsComponent implements OnInit {
  // Varialbe for fields name
  AddObservatinGeneral: FormGroup;
  Limit: any;
  ChecklistSetup: any;
  FlagName: any;
  submitted: boolean;

  addInjuryStatistics: FormGroup;
  injuryId: any;

  yearListing: SelectItem[];
  siteListing: any = [];
  status: SelectItem[];
  initialGeneralFormData: any;
  isLoaded: boolean;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private injurystatisticsservice: InjuryStatisticsService,
    private router: Router,
    private cookieService: CookieService,
    private activetedRoute: ActivatedRoute,
    private elementRef: ElementRef,
    private customValidatorsService: CustomValidatorsService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLDATAENTRY",
        url: "./assets/help/injury-statistics-listing.md",
      },
      {
        label: "LBLINJURYSTATSLISTING",
        routerLink: ["/injury-statistics-listing"],
      },
    ]);

    if (this.cookieService.get("add-new-injury")) {
      var fn = this;
      setTimeout(function () {
        fn.messageService.add({
          severity: "success",
          detail: fn.cookieService.get("add-new-injury"),
        });
        fn.cookieService.delete("add-new-injury");
      }, 1000);
    }
  }

  ngOnInit() {
    this.isLoaded = true;
    this.injuryId = this.activetedRoute.snapshot.paramMap.get("injuryid");

    var data = {
      year: "",
      workHour: "",
      injuries: "",
      siteIds: "",
      status: 1,
    };

    this.createFormBuilder(data);
    this.getInjuryAddData();

    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((resLabel) => {
      this.status = [];
      this.status.push({ label: resLabel["LBLACTIVE"], value: 1 });
      this.status.push({ label: resLabel["LBLINACTIVE"], value: 0 });
    });
    // Add Observatin General

    this.AddObservatinGeneral = this.fb.group({
      FlagName: new FormControl(null, Validators.required),
      ChecklistSetup: new FormControl(null, Validators.required),
      Limit: new FormControl(null, Validators.required),
    });
  }

  /*
   * function name: getInjuryAddData
   * To get add injury data by injury id
   * @By Jitendra on 09 July 2019
   */
  getInjuryAddData() {
    var parameters = {
      injuryId: this.injuryId,
    };
    this.injurystatisticsservice.getInjuryList(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.yearListing = res["year"];
        // this.siteListing = res["avail"];
        this.siteListing = [];
        var siteList = res["avail"];
        siteList.map((data) => {
          this.siteListing.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });

        var injuryData = res["generalData"];
        var assignSite;
        if (res["assign"] != "") {
          assignSite = res["assign"].split(",");
        } else {
          assignSite = "";
        }
        var data = {
          year: injuryData.INJURYYEAR,
          workHour: injuryData.WORKEDHOURS.toString(),
          injuries: injuryData.INJURIES.toString(),
          siteIds: assignSite,
          status: injuryData.STATUS,
        };
        this.createFormBuilder(data);
        this.initialGeneralFormData = this.addInjuryStatistics.getRawValue();
      }
      this.isLoaded = false;
    });
  }

  /*
   * function name: createFormBuilder
   * Create form builder
   * @By Jitendra on 26 Jun 2019
   */
  createFormBuilder(data) {
    this.addInjuryStatistics = this.fb.group({
      year: new FormControl(
        data.year,
        Validators.compose([Validators.required])
      ),
      workHour: new FormControl(
        data.workHour,
        Validators.compose([
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.maxLength(7),
          Validators.min(1),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),
      injuries: new FormControl(
        data.injuries,
        Validators.compose([
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.maxLength(4),
          Validators.min(1),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),

      siteIds: new FormControl(
        data.siteIds,
        Validators.compose([Validators.required])
      ),
      status: new FormControl(data.status, Validators.required),
    });
  }

  get injuryForm() {
    return this.addInjuryStatistics.controls;
  }

  onSubmit(bType) {
    if (this.addInjuryStatistics.value["siteIds"]) {
      this.siteListing = this.auth.rearrangeSelects(
        this.siteListing,
        this.addInjuryStatistics.value["siteIds"]
      );
    }

    this.submitted = true;
    if (this.addInjuryStatistics.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.isLoaded = true;
      if (this.addInjuryStatistics.value["siteIds"] != null) {
        this.addInjuryStatistics.value[
          "siteIds"
        ] = this.addInjuryStatistics.value["siteIds"].join();
      }
      this.addInjuryStatistics.value["injuryId"] = this.injuryId;
      this.injurystatisticsservice
        .updateInjuryData(this.addInjuryStatistics.value)
        .subscribe((res) => {
          if (res["status"] == true) {
            this.submitted = false;
            this.addInjuryStatistics.reset();
            this.getInjuryAddData();
            this.isLoaded = false;
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
            this.isLoaded = false;
          }
        });
    }
  }

  resetInjuryForm() {
    this.addInjuryStatistics.patchValue(this.initialGeneralFormData);
  }

  onSelectAll() {
    const selected = this.siteListing.map((item) => item.SITEID);
    this.addInjuryStatistics.get("siteIds").patchValue(selected);
    this.addInjuryStatistics.get("siteIds").markAsDirty();
  }

  onClearAll() {
    this.addInjuryStatistics.get("siteIds").patchValue([]);
    this.addInjuryStatistics.get("siteIds").markAsDirty();
  }
}
