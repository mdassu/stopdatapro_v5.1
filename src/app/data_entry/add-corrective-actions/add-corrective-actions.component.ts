import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  FormArray,
} from "@angular/forms";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { Router, ActivatedRoute } from "@angular/router";
import { CorractiveActionsService } from "../../_services/corractive-actions.service";
import { AngularWaitBarrier } from "blocking-proxy/built/lib/angular_wait_barrier";

import { TranslateService } from "@ngx-translate/core";
import { core } from "@angular/compiler";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { GlobalDataService } from "src/app/_services/global-data.service";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

@Component({
  selector: "app-add-corrective-actions",
  templateUrl: "./add-corrective-actions.component.html",
  styleUrls: ["./add-corrective-actions.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class AddCorrectiveActionsComponent implements OnInit {
  // Varialbe for fields name
  addCorrectiveAction: FormGroup;
  submitted: boolean;
  site: any;
  Field3: any;
  date: any;
  area: any;
  subarea: any;
  sifts: any;
  msgs: Message[];

  siteListing: any = [];
  obsListing: any = [];
  areaListing: any = [];
  subAreaListing: any = [];
  shiftListing: any = [];
  fieldList: any = [];
  initialGeneralFormData: any;
  currDate: any = new Date();
  dateFormatType: any;
  dateFormatTypeNew: any;
  dateFormat: any;
  subAreaOptionValue: any;
  locale: any;
  constructor(
    private fb: FormBuilder,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private route: Router,
    private corractiveActions: CorractiveActionsService,
    private activetedRoute: ActivatedRoute,
    private translate: TranslateService,
    private auth: AuthenticationService,
    private cookieService: CookieService,
    private globalData: GlobalDataService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLDATAENTRY",
        url: "./assets/help/corrective-actions-edit(general).md",
      },
      {
        label: "LBLCORRECTIVEACTION",
        routerLink: ["/corrective-action-list"],
      },
    ]);
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.dateFormatType = this.auth.UserInfo["dateFormat"];
    if (this.dateFormatType == "MM/dd/yyyy") {
      this.dateFormatTypeNew = "LBLMDY";
    } else if (this.dateFormatType == "dd/MM/yyyy") {
      this.dateFormatTypeNew = "LBLDMY";
    } else if (this.dateFormatType == "yyyy/MM/dd") {
      this.dateFormatTypeNew = "LBLYMD";
    }
    this.dateFormat = this.auth.UserInfo["dateFormat"].toLowerCase();
    this.dateFormat = this.dateFormat.replace("yyyy", "yy");
    // Add Observatin General
    this.addCorrectiveAction = this.fb.group({
      siteId: new FormControl(0, [Validators.required, Validators.min(1)]),
      observerId: new FormControl(null, Validators.required),
      obsDate: new FormControl(null, Validators.required),
      areaId: new FormControl(0, [Validators.required, Validators.min(1)]),
      subAreaId: new FormControl(null),
      shiftId: new FormControl(0, [Validators.required, Validators.min(1)]),
    });
    this.getCorrectiveActionDataForAdd();
  }

  getCorrectiveActionDataForAdd() {
    this.corractiveActions.getCorrectiveActionForAdd().subscribe((res) => {
      if (res["status"] == true) {
        this.translate.get("LBLSELECT").subscribe((selectLabel) => {
          this.siteListing = res["sites"];
          this.siteListing.splice(0, 0, { SITEID: 0, SITENAME: selectLabel });
          this.areaListing = res["area"];
          this.areaListing.splice(0, 0, { AREAID: 0, AREANAME: selectLabel });
          this.subAreaListing = [];
          this.shiftListing = res["shifts"];
          this.shiftListing.splice(0, 0, {
            SHIFTID: 0,
            SHIFTNAME: selectLabel,
          });

          // this.obsListing = res["observer"];
          this.obsListing = [];
          var observersListVr = res["observer"];
          observersListVr.map((data) => {
            this.obsListing.push({
              label: data["USERNAME"],
              value: data["USERID"],
            });
          });
        });
        var fields = res["fieldName"];
        var fieldValue = res["fieldValue"];
        this.addCorrectiveAction.controls["siteId"].setValue(
          res["defaultSiteId"]
        );

        fields.map((field, key) => {
          var fieldval = fieldValue.filter((val) => {
            return val.CUSTOMFIELDID === field.CUSTOMFIELDID;
          });

          if (field["MANDATORY"] == 1) {
            this.addCorrectiveAction.addControl(
              "Field" + (key + 1),
              new FormControl(0, [Validators.required, Validators.min(1)])
            );
          } else {
            this.addCorrectiveAction.addControl(
              "Field" + (key + 1),
              new FormControl(0)
            );
          }

          this.translate.get("LBLSELECT").subscribe((selectLabel) => {
            fieldval.splice(0, 0, {
              CUSTOMFIELDVALUE: selectLabel,
              CUSTOMFIELDVALUEID: 0,
            });
            this.fieldList.push({
              fieldName: field["CUSTOMFIELDNAME"],
              mandatory: field["MANDATORY"],
              fieldValue: fieldval,
            });
          });
        });
        this.initialGeneralFormData = this.addCorrectiveAction.getRawValue();
      }
    });
  }

  getSiteData(siteId) {
    var parameters = {
      siteId: siteId,
    };
    this.corractiveActions.getSiteData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.areaListing = res["area"];
        this.subAreaListing = [];
        this.shiftListing = res["shifts"];
        // this.obsListing = res["observer"];
        this.obsListing = [];
        var observersListVr = res["observer"];
        observersListVr.map((data) => {
          this.obsListing.push({
            label: data["USERNAME"],
            value: data["USERID"],
          });
        });
      } else {
        this.areaListing = [];
        this.subAreaListing = [];
        this.shiftListing = [];
        this.obsListing = [];
      }
    });
  }

  getSubAreaData(areaId) {
    var parameters = {
      areaId: areaId,
    };
    this.corractiveActions.getSubAreaByAreaId(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.translate.get("LBLSELECT").subscribe((selectLabel) => {
          this.subAreaListing = res["Data"];
          this.subAreaListing.splice(0, 0, {
            SUBAREAID: 0,
            SUBAREANAME: selectLabel,
          });
        });
      } else {
        this.subAreaListing = [];
      }
    });
  }

  get GeneralSubForm() {
    return this.addCorrectiveAction.controls;
  }

  daysDiff(dateFrom, dateTo) {
    return Math.floor(
      (Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate()) -
        Date.UTC(
          dateFrom.getFullYear(),
          dateFrom.getMonth(),
          dateFrom.getDate()
        )) /
        (1000 * 60 * 60 * 24)
    );
  }

  onSubmit(value: string) {
    this.submitted = true;
    if (this.addCorrectiveAction.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      var daysDiff = this.daysDiff(
        this.addCorrectiveAction.value["obsDate"],
        new Date()
      );
      if (
        daysDiff > this.globalData.GlobalData["PREVIOUSDAYLIMITONOBSDATE"] &&
        this.globalData.GlobalData["PREVIOUSDAYLIMITONOBSDATE"] != ""
      ) {
        this.submitted = false;
        this.translate
          .get(["ALTPREVIOUSDAYSLIMITONOBSDATE", "ALTPRIOROBSDATE"])
          .subscribe((message) => {
            this.messageService.add({
              severity: "error",
              detail:
                message["ALTPREVIOUSDAYSLIMITONOBSDATE"] +
                " " +
                this.globalData.GlobalData["PREVIOUSDAYLIMITONOBSDATE"] +
                " " +
                message["ALTPRIOROBSDATE"],
            });
          });
      } else {
        var finalField = [];
        this.fieldList.map((field, key) => {
          if (this.addCorrectiveAction.value["Field" + (key + 1)]) {
            finalField.push(
              this.addCorrectiveAction.value["Field" + (key + 1)]
            );
          }
          delete this.addCorrectiveAction.value["Field" + (key + 1)];
        });
        if (finalField.length > 0) {
          this.addCorrectiveAction.value[
            "customFieldvalueId"
          ] = finalField.join();
        } else {
          this.addCorrectiveAction.value["customFieldValueId"] = "";
        }
        if (this.addCorrectiveAction.value["observerId"] != null) {
          this.addCorrectiveAction.value[
            "observerId"
          ] = this.addCorrectiveAction.value["observerId"].join();
        }
        this.addCorrectiveAction.value["obsDate"] = this.convertDate(
          this.addCorrectiveAction.value["obsDate"]
        );
        this.corractiveActions
          .insertCorrectiveAction(this.addCorrectiveAction.value)
          .subscribe((res) => {
            this.translate.get(res["message"]).subscribe((message) => {
              if (res["status"] == true) {
                this.submitted = false;
                // this.getInjuryAddData();
                this.cookieService.set("new-corrective", message);

                this.route.navigate(
                  [
                    "/edit-corrective-action-listing/" +
                      res["corrActionId"] +
                      "/0/2",
                  ],
                  {
                    skipLocationChange: true,
                  }
                );
              } else {
                this.messageService.add({
                  severity: "error",
                  detail: message,
                });
              }
            });
          });
      }
    }
  }

  convertDate(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join("/");
  }

  resetCAForm() {
    this.addCorrectiveAction.patchValue(this.initialGeneralFormData);
  }

  onSelectAll() {
    const selected = this.obsListing.map((item) => item.USERID);
    this.addCorrectiveAction.get("observerId").patchValue(selected);
    this.addCorrectiveAction.get("observerId").markAsDirty();
  }

  onClearAll() {
    this.addCorrectiveAction.get("observerId").patchValue([]);
    this.addCorrectiveAction.get("observerId").markAsDirty();
  }
}
