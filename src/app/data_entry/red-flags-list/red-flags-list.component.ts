import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { Router, RoutesRecognized } from "@angular/router";
import { RedFlagService } from "src/app/_services/red-flag.service";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
import { AuthenticationService } from "../../_services/authentication.service";
import { filter, pairwise } from "rxjs/operators";
declare var $: any;

@Component({
  selector: "app-red-flags-list",
  templateUrl: "./red-flags-list.component.html",
  styleUrls: ["./red-flags-list.component.css"],
  providers: [ConfirmationService, MessageService],
})
export class RedFlagsListComponent implements OnInit {
  //  Variable Declaration
  public show_advance_search: boolean = false;
  iconType: string = "ui-icon-add";
  allRedFlag: any;
  redFlagListing: any;
  redFlagCols: any[];
  redFlagCount: number;
  filterSites: any = [];
  filterArea: any = [];
  filterSubArea: any = [];
  filterStatus: any = [];

  searchBy: any = [];
  selectedfilterRedFlag: string = "RED FLAG NAME";
  searchText: any;
  selectedRedFlag: any = [];
  redFlagSites: any = [];
  redFlagAreas: any = [];
  redFlagSubAreas: any = [];
  redFlagStatus: any = [];
  isLoaded: boolean;
  confirmClass: any;
  appName: any;
  parameters: any;
  previousUrl: any;
  // Breadcrumb Service
  subAreaOptionValue: any;
  userId: any;
  roleId: any;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private route: Router,
    private redflagservice: RedFlagService,
    private translate: TranslateService,
    private env: EnvService,
    private auth: AuthenticationService
  ) {
    this.translate.get(["LBLDATAENTRY", "LBLREDFLAG"]).subscribe((label) => {
      this.breadcrumbService.setItems([
        {
          label: label["LBLDATAENTRY"],
          url: "./assets/help/red-flag-listing.md",
        },
        { label: label["LBLREDFLAG"], routerLink: ["/red-flags-listing"] },
      ]);
    });
  }

  ngOnInit() {
    this.userId = this.auth.UserInfo["userId"];
    this.roleId = this.auth.UserInfo["roleId"];
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.route.events
      .pipe(
        filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((e: any) => {
        var url = e[0].urlAfterRedirects; // previous url
        localStorage.setItem("previousUrl-" + this.userId, url);
      });
    // Table Header
    if (this.subAreaOptionValue == 0) {
      this.redFlagCols = [
        { field: "RED FLAG NAME", header: "LBLREDFLAGNAME" },
        { field: "SITE NAME", header: "LBLSITENAME" },
        { field: "OBSERVER", header: "LBLOBSERVER" },
        { field: "AREAS", header: "LBLAREA" },
        { field: "STATUS", header: "LBLSTATUS" },
      ];
    } else {
      this.redFlagCols = [
        { field: "RED FLAG NAME", header: "LBLREDFLAGNAME" },
        { field: "SITE NAME", header: "LBLSITENAME" },
        { field: "OBSERVER", header: "LBLOBSERVER" },
        { field: "AREAS", header: "LBLAREA" },
        { field: "SUBAREAS", header: "LBLSUBAREAS" },
        { field: "STATUS", header: "LBLSTATUS" },
      ];
    }

    // Injury count
    this.redFlagCount = this.redFlagCols.length;

    this.checkFilter();

    this.redFlagCols.map((item, key) => {
      this.translate.get(item.header).subscribe((text: string) => {
        if (key < 5) {
          this.searchBy.push({
            field: item.field,
            header: text,
          });
        }
      });
      this.selectedfilterRedFlag = "RED FLAG NAME";
    });
    this.appName = this.env.appName;
  }

  // Advance serach
  advance_search() {
    this.iconType =
      this.iconType === "ui-icon-add" ? "ui-icon-remove" : "ui-icon-add";
    this.show_advance_search = !this.show_advance_search;
  }

  checkFilter() {
    this.parameters = {};
    this.previousUrl = localStorage.getItem("previousUrl-" + this.userId);
    if (this.previousUrl) {
      if (
        this.previousUrl.search("/add-red-flags") != -1 ||
        this.previousUrl.search("/edit-red-flags/") != -1
      ) {
        if (localStorage.getItem("filterInfo")) {
          this.toggle();
          // this.animationState = "in";
          this.parameters = JSON.parse(localStorage.getItem("filterInfo"));
          var keys = Object.keys(this.parameters);
          keys.map((param) => {
            if (this.parameters[param]) {
              if (param == "siteId") {
                this.redFlagSites = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "areaId") {
                this.redFlagAreas = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "subAreaId") {
                this.redFlagSubAreas = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "statusId") {
                this.redFlagStatus = this.parameters[param]
                  .split(",")
                  .map(Number);
              }

              // this.selectedFilter[param] = this.parameters[param]
              //   .split(",")
              //   .map(Number);
            }
          });
          localStorage.removeItem("previousUrl-" + this.userId);
        }
      } else {
        localStorage.removeItem("filterInfo");
        this.redFlagSites = [];
        this.redFlagSites.push(this.auth.UserInfo["defaultSiteId"]);
      }
    } else {
      localStorage.removeItem("filterInfo");
      this.redFlagSites = [];
      this.redFlagSites.push(this.auth.UserInfo["defaultSiteId"]);
    }
    this.getRedFlag(this.parameters);
  }

  // Get Red Flag List
  getRedFlag(parameters) {
    this.redflagservice.getRedFlagList(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.allRedFlag = res["generalData"];
        this.redFlagListing = res["generalData"];
        this.redFlagListing.map((item) => {
          if (item["AREAS"] == "LBLALL") {
            this.translate.get(item.AREAS).subscribe((text: string) => {
              item["AREAS"] = text;
            });
          }
          if (item["SUBAREAS"] == "LBLALL") {
            this.translate.get(item.SUBAREAS).subscribe((subArea: string) => {
              item["SUBAREAS"] = subArea;
            });
          }
          if (item["OBSERVER"] == "LBLALL") {
            this.translate.get(item.OBSERVER).subscribe((observer: string) => {
              item["OBSERVER"] = observer;
            });
          }
        });

        // if (parameters["clearFilter"] != 0) {
        //   this.redFlagSites = [res["defaultSiteId"]];
        // }
      } else {
        this.allRedFlag = [];
        this.redFlagListing = [];
      }

      // this.filterSites = res["filterSites"];
      this.filterSites = [];
      var siteList = res["filterSites"];
      siteList.map((data) => {
        this.filterSites.push({
          label: data["SITENAME"],
          value: data["SITEID"],
        });
      });
      // this.filterArea = res["filterArea"];
      this.filterArea = [];
      var areaList = res["filterArea"];
      areaList.map((data) => {
        this.filterArea.push({
          label: data["AREANAME"],
          value: data["AREAID"],
        });
      });

      // this.filterSubArea = res["filterSubArea"];
      this.filterSubArea = [];
      var subareaList = res["filterSubArea"];
      subareaList.map((data) => {
        this.filterSubArea.push({
          label: data["SUBAREANAME"],
          value: data["SUBAREAID"],
        });
      });

      var filterStatus = res["filterStatus"];
      this.filterStatus = [];
      filterStatus.map((item, key) => {
        this.translate.get(item.VALUE).subscribe((text: string) => {
          this.filterStatus.push({
            value: item.VALUEID,
            label: text,
          });
        });
      });

      if (this.redFlagSites) {
        this.filterSites = this.auth.rearrangeSelects(
          this.filterSites,
          this.redFlagSites
        );
      }
      if (this.redFlagAreas) {
        this.filterArea = this.auth.rearrangeSelects(
          this.filterArea,
          this.redFlagAreas
        );
      }

      if (this.redFlagSubAreas) {
        this.filterSubArea = this.auth.rearrangeSelects(
          this.filterSubArea,
          this.redFlagSubAreas
        );
      }

      if (this.redFlagStatus) {
        this.filterStatus = this.auth.rearrangeSelects(
          this.filterStatus,
          this.redFlagStatus
        );
      }
      this.isLoaded = true;
    });
  }

  // searching data
  searching() {
    var field = this.selectedfilterRedFlag;
    var stext = this.searchText.toLowerCase();
    var fn = this;

    if (stext != "") {
      var newList = this.allRedFlag.filter((item) => {
        if (item[field] != null) {
          return item[field].toLowerCase().indexOf(stext) >= 0;
        } else {
          return;
        }
      });
      this.redFlagListing = newList;
    } else {
      this.redFlagListing = this.allRedFlag;
    }
  }

  getArea() {
    var siteID = "";
    if (this.redFlagSites) {
      siteID = this.redFlagSites.join();
    }
    var parameters = {
      siteId: siteID,
    };
    this.redflagservice.getAreaBySiteId(parameters).subscribe((res) => {
      if (res["status"] == true) {
        // this.filterArea = res["filterArea"];
        this.filterArea = [];
        var areaList = res["area"];
        areaList.map((data) => {
          this.filterArea.push({
            label: data["AREANAME"],
            value: data["AREAID"],
          });
        });
        this.redFlagAreas = "";
      } else {
        this.filterArea = [];
        this.filterSubArea = [];
        this.redFlagAreas = "";
      }
    });
  }

  getSubArea() {
    var areaID = "";
    if (this.redFlagAreas) {
      areaID = this.redFlagAreas.join();
    } else {
      areaID = "";
    }
    var parameters = {
      areaId: areaID,
    };
    this.redflagservice.getSubAreaByAreaId(parameters).subscribe((res) => {
      if (res["status"] == true) {
        // this.filterSubArea = res["filterSubArea"];
        this.filterSubArea = [];
        var subareaList = res["subArea"];

        subareaList.map((data) => {
          this.filterSubArea.push({
            label: data["SUBAREANAME"],
            value: data["SUBAREAID"],
          });
        });
        this.redFlagSubAreas = "";
      } else {
        this.filterSubArea = [];
        this.redFlagSubAreas = "";
      }
    });
  }

  filtering() {
    this.searchText = "";
    var siteId = "";
    if (this.redFlagSites) {
      siteId = this.redFlagSites.join();
    }

    var areaId = "";
    if (this.redFlagAreas) {
      areaId = this.redFlagAreas.join();
    }

    var subAreaId = "";
    if (this.redFlagSubAreas) {
      subAreaId = this.redFlagSubAreas.join();
    }

    var statusId = "";
    if (this.redFlagStatus) {
      statusId = this.redFlagStatus.join();
    }
    var parameters = {
      siteId: siteId,
      areaId: areaId,
      subAreaId: subAreaId,
      statusId: statusId,
      clearFilter: 0,
    };

    localStorage.removeItem("filterInfo");
    localStorage.setItem("filterInfo", JSON.stringify(parameters));
    this.getRedFlag(parameters);
  }

  clearFilter() {
    this.redFlagSites = "";
    this.redFlagAreas = "";
    this.redFlagSubAreas = "";
    this.redFlagStatus = "";
    var parameters = {
      clearFilter: 0,
    };
    this.searchText = "";
    this.getRedFlag(parameters);
  }

  deleteRecord() {
    this.confirmClass = "warning-msg";
    var msg = "";
    this.translate.get("ALTDELCONFIRM").subscribe((confirmMsg) => {
      msg = confirmMsg;
    });
    if (this.selectedRedFlag == "") {
      this.messageService.add({
        severity: "error",
        detail: "Please select atleast one red flag.",
      });
    } else {
      this.confirmationService.confirm({
        message: msg,
        accept: () => {
          this.redflagservice
            .deleteRedFlag({
              redflagId: Array.prototype.map
                .call(this.selectedRedFlag, (s) => s.REDFLAGID)
                .toString(),
            })
            .subscribe((res) => {
              this.getRedFlag({});
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
          this.searchText = "";
          this.getRedFlag({});
          this.selectedRedFlag = [];
        },
        reject: () => {
          this.selectedRedFlag = [];
          this.searchText = "";
          this.getRedFlag({});
        },
      });
    }
  }

  changeStatus(id) {
    this.confirmClass = "info-msg";
    var msg = "";
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((confirmMsg) => {
      msg = confirmMsg;
    });
    if (this.selectedRedFlag == "" && id == "") {
      this.messageService.add({
        severity: "error",
        detail: "Please select atleast one red flag.",
      });
    } else {
      this.confirmationService.confirm({
        message: msg,
        accept: () => {
          this.redflagservice
            .updateRedFlagStatus({
              redflagId: id
                ? id
                : Array.prototype.map
                    .call(this.selectedRedFlag, (s) => s.REDFLAGID)
                    .toString(),
            })
            .subscribe((res) => {
              this.selectedRedFlag = [];
              this.getRedFlag({});
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
          this.searchText = "";
          this.getRedFlag({});
          this.selectedRedFlag = [];
        },
        reject: () => {
          this.selectedRedFlag = [];
          this.searchText = "";
          this.getRedFlag({});
        },
      });
    }
  }

  toggle() {
    $("#slideDwon").slideToggle();
  }

  onSelectAll() {
    const selected = this.filterSites.map((item) => item.SITEID);
    this.redFlagSites = selected;
    this.getArea();
  }

  onClearAll() {
    this.redFlagSites = [];
  }

  onSelectAllArea() {
    const selectedArea = this.filterArea.map((item) => item.AREAID);
    this.redFlagAreas = selectedArea;
    this.getSubArea();
  }

  onClearAllArea() {
    this.redFlagAreas = [];
  }

  onSelectAllSubArea() {
    const selectedSubArea = this.filterSubArea.map((item) => item.AREAID);
    this.redFlagSubAreas = selectedSubArea;
    this.getSubArea();
  }

  onClearAllSubArea() {
    this.redFlagSubAreas = [];
  }

  onSelectAllStatus() {
    const selectedStatus = this.filterStatus.map((item) => item.VALUEID);
    this.redFlagStatus = selectedStatus;
  }

  onClearAllStatus() {
    this.redFlagStatus = [];
  }
}
