import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService, SortEvent } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { Router, RoutesRecognized } from "@angular/router";
import { ApprovalService } from "src/app/_services/approval.service";
import { SlideUpDownAnimations } from "../../_animations/slide-up-down.animations";
import { EnvService } from "src/env.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { filter, pairwise } from "rxjs/operators";
import { formatDate } from "@angular/common";
import * as moment from "moment";

// declare var $: any;

@Component({
  selector: "app-approval-listing",
  templateUrl: "./approval-listing.component.html",
  styleUrls: ["./approval-listing.component.css"],
  providers: [ConfirmationService, MessageService],
  animations: [SlideUpDownAnimations],
})
export class ApprovalListingComponent implements OnInit {
  //  Variable Declaration
  filterForm: FormGroup;
  public showAdvanceSearch: boolean = false;

  allApproval: any;
  approvalListing: any = [];
  approvalCols: any[];
  approvalCount: number;
  filterSites: any = [];
  filterArea: any = [];
  filterSubArea: any = [];
  filterShift: any = [];
  filterObserver: any = [];
  filterStatus: any = [];

  searchText: any;
  selectedApproval: any = [];
  approvalSites: any = [];
  approvalAreas: any = [];
  approvalSubAreas: any = [];
  approvalShift: any = [];
  approvalOBS: any = [];
  approvalOBSDateFrom: any = [];
  approvalOBSDateTo: any = [];
  approvalStatus: any = [];
  curDate = new Date();
  iconType: string = "ui-icon-add";
  searchBy: any = [];

  selectedfilterOBSDate: string = "OBSERVATION DATE";
  animationState = "out";
  isLoaded: boolean;
  fiterToDate: boolean;
  fiterFromDate: boolean;
  fiterFromDateLessThan: boolean;
  appName: any;
  dateFormatType: any;
  dateFormat: any;
  parameters: any;
  previousUrl: any;
  subAreaOptionValue: any;
  userId: any;
  approvalObsDate: boolean = false;
  currDate: any = new Date();
  yearList: any;
  viewApprovalOBSDateFrom: any;
  viewApprovalOBSDateTo: any;
  selecetedApprovalDateFilter: boolean = false;
  locale: any;
  // Breadcrumb Service
  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private route: Router,
    private approvalservice: ApprovalService,
    private cookieService: CookieService,
    private translate: TranslateService,
    private auth: AuthenticationService,
    private env: EnvService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLDATAENTRY", url: "./assets/help/approval-listing.md" },
      {
        label: "LBLAPPROVAL",
        routerLink: ["/approval-listing"],
      },
    ]);

    if (this.cookieService.get("change-approval")) {
      var fn = this;
      setTimeout(function () {
        fn.messageService.add({
          severity: "success",
          detail: fn.cookieService.get("change-approval"),
        });
        fn.cookieService.delete("change-approval");
      }, 1000);
    }

    this.approvalOBSDateFrom = "";
    this.approvalOBSDateTo = "";
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    this.userId = this.auth.UserInfo["userId"];
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.route.events
      .pipe(
        filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((e: any) => {
        var url = e[0].urlAfterRedirects; // previous url
        localStorage.setItem("previousUrl-" + this.userId, url);
      });
    // Table Header

    var previousYear = this.currDate.getFullYear() - 10;
    var nextYear = this.currDate.getFullYear() + 10;
    this.yearList = previousYear + ":" + nextYear;

    if (this.subAreaOptionValue == 0) {
      this.approvalCols = [
        { field: "OBSERVATION DATE", header: "LBLOBSDATE" },
        { field: "OBSERVER", header: "LBLOBSERVER" },
        { field: "AREAS", header: "LBLAREA" },
        { field: "STATUS", header: "LBLAPPROVALSTATUS" },
      ];
    } else {
      this.approvalCols = [
        { field: "OBSERVATION DATE", header: "LBLOBSDATE" },
        { field: "OBSERVER", header: "LBLOBSERVER" },
        { field: "AREAS", header: "LBLAREA" },
        { field: "SUBAREAS", header: "LBLSUBAREAS" },
        { field: "STATUS", header: "LBLAPPROVALSTATUS" },
      ];
    }
    // this.dateFormatType = this.auth.UserInfo["dateFormat"];
    this.dateFormatType = this.auth.UserInfo["dateFormat"].toLowerCase();
    this.dateFormat = this.dateFormatType.replace("yyyy", "yy");

    // Approval count
    this.approvalCount = this.approvalCols.length;

    this.approvalSites.push(this.auth.UserInfo["defaultSiteId"]);

    this.checkFilter();

    this.approvalCols.map((item, key) => {
      this.translate.get(item.header).subscribe((text: string) => {
        if (key != 4) {
          this.searchBy.push({ field: item.field, header: text });
        }
      });
      this.selectedfilterOBSDate = "OBSERVATION DATE";
    });
    this.appName = this.env.appName;
  }

  checkFilter() {
    this.parameters = { siteId: this.auth.UserInfo["defaultSiteId"] };
    this.previousUrl = localStorage.getItem("previousUrl-" + this.userId);
    if (this.previousUrl) {
      if (this.previousUrl.search("/edit-approval/") != -1) {
        if (localStorage.getItem("filterInfo")) {
          this.toggleFilterShow();
          // this.animationState = "in";
          this.parameters = JSON.parse(localStorage.getItem("filterInfo"));
          var keys = Object.keys(this.parameters);
          keys.map((param) => {
            if (this.parameters[param]) {
              if (param == "siteId") {
                this.approvalSites = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "areaId") {
                this.approvalAreas = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "subAreaId") {
                this.approvalSubAreas = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "shiftId") {
                this.approvalShift = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "observerId") {
                this.approvalOBS = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "statusId") {
                this.approvalStatus = this.parameters[param]
                  .split(",")
                  .map(Number);
              }

              if (param == "fromDate") {
                this.approvalOBSDateFrom = new Date(this.parameters[param]);
              }
              if (param == "toDate") {
                this.approvalOBSDateTo = new Date(this.parameters[param]);
              }

              this.approvalDateSelected();

              // this.selectedFilter[param] = this.parameters[param]
              //   .split(",")
              //   .map(Number);
            }
          });
          localStorage.removeItem("previousUrl-" + this.userId);
        }
      } else {
        localStorage.removeItem("filterInfo");
        this.approvalSites = [];
        this.approvalSites.push(this.auth.UserInfo["defaultSiteId"]);
      }
    } else {
      localStorage.removeItem("filterInfo");
      this.approvalSites = [];
      this.approvalSites.push(this.auth.UserInfo["defaultSiteId"]);
    }

    this.getApproval(this.parameters);
  }

  getApproval(parameters) {
    this.approvalservice.getApprovalList(parameters).subscribe((res) => {
      if (res["status"] == true) {
        // this.filterSites = res["filterSites"];
        this.filterSites = [];
        var siteList = res["filterSites"];
        siteList.map((data) => {
          this.filterSites.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });
        // this.filterArea = res["filterArea"];
        this.filterArea = [];
        var areaList = res["filterArea"];
        areaList.map((data) => {
          this.filterArea.push({
            label: data["AREANAME"],
            value: data["AREAID"],
          });
        });

        // this.filterSubArea = res["filterSubArea"];
        this.filterSubArea = [];
        var subareaList = res["filterSubArea"];
        subareaList.map((data) => {
          this.filterArea.push({
            label: data["SUBAREANAME"],
            value: data["SUBAREAID"],
          });
        });

        // this.filterShift = res["filterShift"];
        this.filterShift = [];
        var shiftList = res["filterShift"];
        shiftList.map((data) => {
          this.filterShift.push({
            label: data["SHIFTNAME"],
            value: data["SHIFTID"],
          });
        });

        // this.filterObserver = res["filterObserver"];
        this.filterObserver = [];
        var observerListVr = res["filterObserver"];
        observerListVr.map((data) => {
          this.filterObserver.push({
            label: data["FULLNAME"],
            value: data["USERID"],
          });
        });

        this.filterStatus = [];
        var filterStatus = res["filterStatus"];
        filterStatus.map((item, key) => {
          this.translate.get(item.VALUE).subscribe((text: string) => {
            this.filterStatus.push({
              value: item.VALUEID,
              label: text,
            });
          });
        });
        this.allApproval = res["generalData"];
        this.approvalListing = res["generalData"];
        this.approvalListing.map((item) => {
          if (item["OBSERVER"] == "LBLMULTIPLEOBSERVERS") {
            this.translate.get(item.OBSERVER).subscribe((observer: string) => {
              item["OBSERVER"] = observer;
            });
          }
        });
        // if (parameters["clearFilter"] != 0) {
        // 	this.approvalSites = [res["defaultSiteId"]];
        // }
      } else {
        // this.filterSites = res["filterSites"];
        this.filterSites = [];
        var siteList = res["filterSites"];
        siteList.map((data) => {
          this.filterSites.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });
        // this.filterArea = res["filterArea"];
        this.filterArea = [];
        var areaList = res["filterArea"];
        areaList.map((data) => {
          this.filterArea.push({
            label: data["AREANAME"],
            value: data["AREAID"],
          });
        });

        // this.filterSubArea = res["filterSubArea"];
        this.filterSubArea = [];
        var subareaList = res["filterSubArea"];
        subareaList.map((data) => {
          this.filterArea.push({
            label: data["SUBAREANAME"],
            value: data["SUBAREAID"],
          });
        });

        // this.filterShift = res["filterShift"];
        this.filterShift = [];
        var shiftList = res["filterShift"];
        shiftList.map((data) => {
          this.filterShift.push({
            label: data["SHIFTNAME"],
            value: data["SHIFTID"],
          });
        });

        // this.filterObserver = res["filterObserver"];
        this.filterObserver = [];
        var observerListVr = res["filterObserver"];
        observerListVr.map((data) => {
          this.filterObserver.push({
            label: data["FULLNAME"],
            value: data["USERID"],
          });
        });

        this.filterStatus = [];
        var filterStatus = res["filterStatus"];
        filterStatus.map((item, key) => {
          this.translate.get(item.VALUE).subscribe((text: string) => {
            this.filterStatus.push({
              value: item.VALUEID,
              label: text,
            });
          });
        });
        this.allApproval = [];
        this.approvalListing = [];
      }

      if (this.approvalSites) {
        this.filterSites = this.auth.rearrangeSelects(
          this.filterSites,
          this.approvalSites
        );
      }
      if (this.approvalAreas) {
        this.filterArea = this.auth.rearrangeSelects(
          this.filterArea,
          this.approvalAreas
        );
      }
      if (this.approvalSubAreas) {
        this.filterSubArea = this.auth.rearrangeSelects(
          this.filterSubArea,
          this.approvalSubAreas
        );
      }
      if (this.approvalShift) {
        this.filterShift = this.auth.rearrangeSelects(
          this.filterShift,
          this.approvalShift
        );
      }
      if (this.approvalOBS) {
        this.filterObserver = this.auth.rearrangeSelects(
          this.filterObserver,
          this.approvalOBS
        );
      }
      if (this.approvalStatus) {
        this.filterStatus = this.auth.rearrangeSelects(
          this.filterStatus,
          this.approvalStatus
        );
      }
      this.isLoaded = true;
    });
  }

  getSiteFilter() {
    var siteID = "";
    if (this.approvalSites) {
      siteID = this.approvalSites.join();
    }
    var parameters = {
      siteId: siteID,
    };
    this.approvalservice.getSiteFilterData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        // this.filterArea = res["area"];
        this.filterArea = [];
        var areaList = res["area"];
        areaList.map((data) => {
          this.filterArea.push({
            label: data["AREANAME"],
            value: data["AREAID"],
          });
        });

        // this.filterShift = res["shift"];
        this.filterShift = [];
        var shiftList = res["shift"];
        shiftList.map((data) => {
          this.filterShift.push({
            label: data["SHIFTNAME"],
            value: data["SHIFTID"],
          });
        });

        // this.filterObserver = res["observer"];
        this.filterObserver = [];
        var observerListVr = res["observer"];
        observerListVr.map((data) => {
          this.filterObserver.push({
            label: data["FULLNAME"],
            value: data["USERID"],
          });
        });

        this.approvalAreas = "";
        this.approvalOBS = "";
        this.approvalShift = "";
      } else {
        this.filterArea = [];
        this.filterObserver = [];
        this.filterShift = [];
        this.approvalAreas = "";
        this.approvalOBS = "";
        this.approvalShift = "";
      }
    });
  }

  getAreaFilter() {
    var areaID = "";
    if (this.approvalAreas) {
      areaID = this.approvalAreas.join();
    } else {
      areaID = "";
    }
    var parameters = {
      areaId: areaID,
    };
    this.approvalservice.getAreaFilterData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        // this.filterSubArea = res["subArea"];
        this.filterSubArea = [];
        var subareaList = res["subArea"];
        subareaList.map((data) => {
          this.filterArea.push({
            label: data["SUBAREANAME"],
            value: data["SUBAREAID"],
          });
        });

        this.approvalSubAreas = "";
      } else {
        this.filterSubArea = [];
        this.approvalSubAreas = "";
      }
    });
  }

  // searching data
  searching() {
    var field = this.selectedfilterOBSDate;
    var stext = this.searchText.toLowerCase();
    var fn = this;

    if (stext != "") {
      var newList = this.allApproval.filter((item) => {
        if (item[field] != null) {
          return item[field].toLowerCase().indexOf(stext) >= 0;
        } else {
          return;
        }
      });
      this.approvalListing = newList;
    } else {
      this.approvalListing = this.allApproval;
    }
  }

  filtering() {
    var siteId = "";
    if (this.approvalSites) {
      siteId = this.approvalSites.join();
    }

    var shiftId = "";
    if (this.approvalShift) {
      shiftId = this.approvalShift.join();
    }

    var areaId = "";
    if (this.approvalAreas) {
      areaId = this.approvalAreas.join();
    }

    var subAreaId = "";
    if (this.approvalSubAreas) {
      subAreaId = this.approvalSubAreas.join();
    }

    var observeId = "";
    if (this.approvalOBS) {
      observeId = this.approvalOBS.join();
    }

    var fromDate = "";
    if (this.approvalOBSDateFrom) {
      this.fiterFromDate = false;
      this.fiterToDate = true;
      fromDate = this.convertDate(this.approvalOBSDateFrom);
    }

    var toDate = "";
    if (this.approvalOBSDateTo) {
      if (!this.approvalOBSDateFrom) {
        this.fiterFromDate = true;
      }
      this.fiterToDate = false;
      toDate = this.convertDate(this.approvalOBSDateTo);
    }

    var statusId = "";
    if (this.approvalStatus) {
      statusId = this.approvalStatus.join();
    }

    if (
      (this.approvalOBSDateFrom && this.approvalOBSDateTo) ||
      (!this.approvalOBSDateFrom && !this.approvalOBSDateTo)
    ) {
      if (fromDate <= toDate || (!fromDate && !toDate)) {
        this.fiterFromDateLessThan = false;
        var parameters = {
          siteId: siteId,
          shiftId: shiftId,
          areaId: areaId,
          subAreaId: subAreaId,
          observeId: observeId,
          fromDate: fromDate,
          toDate: toDate,
          statusId: statusId,
        };
        localStorage.removeItem("filterInfo");
        localStorage.setItem("filterInfo", JSON.stringify(parameters));
        this.getApproval(parameters);
      } else {
        this.fiterFromDateLessThan = true;
      }
    } else {
    }
  }

  clearFilter() {
    this.approvalSites = "";
    this.approvalShift = "";
    this.approvalAreas = "";
    this.approvalSubAreas = "";
    this.approvalOBS = "";
    this.approvalOBSDateFrom = "";
    this.approvalOBSDateTo = "";
    this.approvalStatus = "";
    var parameters = {
      clearFilter: 0,
    };
    this.getApproval(parameters);
    this.fiterFromDate = false;
    this.fiterToDate = false;
    this.fiterFromDateLessThan = false;
    this.selecetedApprovalDateFilter = false;
  }

  convertDate(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join("/");
  }

  approvalDateSelected() {
    if (this.approvalOBSDateFrom && this.approvalOBSDateTo) {
      var startDate;
      var endDate;
      if (this.auth.UserInfo["dateFormat"].toLowerCase() == "dd/mm/yyyy") {
        if (
          this.approvalOBSDateFrom &&
          this.approvalOBSDateFrom.toString().search("/") != -1
        ) {
          var fDate = this.approvalOBSDateFrom.split("/");
          startDate = new Date(fDate[1] + "/" + fDate[0] + "/" + fDate[2]);
        } else {
          startDate = this.approvalOBSDateFrom;
        }

        if (
          this.approvalOBSDateTo &&
          this.approvalOBSDateTo.toString().search("/") != -1
        ) {
          var tDate = this.approvalOBSDateTo.split("/");
          endDate = new Date(tDate[1] + "/" + tDate[0] + "/" + tDate[2]);
        } else {
          endDate = this.approvalOBSDateTo;
        }
      } else {
        startDate = new Date(this.approvalOBSDateFrom);
        endDate = new Date(this.approvalOBSDateTo);
      }
    }
    if (!this.approvalOBSDateFrom) {
      this.translate.get(["FMKPLS", "FMKFROM"]).subscribe((res: Object) => {
        this.messageService.add({
          severity: "info",
          detail: res["FMKPLS"] + " " + res["FMKFROM"],
        });
      });
    } else if (!this.approvalOBSDateTo) {
      this.translate.get(["FMKPLS", "FMKTO"]).subscribe((res: Object) => {
        this.messageService.add({
          severity: "info",
          detail: res["FMKPLS"] + " " + res["FMKTO"],
        });
      });
    } else if (startDate > endDate) {
      this.translate.get(["FMKPRVD"]).subscribe((res: Object) => {
        this.messageService.add({
          severity: "info",
          detail: res["FMKPRVD"],
        });
      });
    } else {
      this.approvalObsDate = false;
      this.selecetedApprovalDateFilter = true;
      var fromDate = this.approvalOBSDateFrom;
      var toDate = this.approvalOBSDateTo;
      if (this.auth.UserInfo["dateFormat"].toLowerCase() == "dd/mm/yyyy") {
        if (fromDate && fromDate.toString().search("/") != -1) {
          fromDate = this.approvalOBSDateFrom.split("/");
          fromDate = fromDate[1] + "/" + fromDate[0] + "/" + fromDate[2];
        }
        if (toDate && toDate.toString().search("/") != -1) {
          toDate = this.approvalOBSDateTo.split("/");
          toDate = toDate[1] + "/" + toDate[0] + "/" + toDate[2];
        }
      }

      this.viewApprovalOBSDateFrom = formatDate(
        fromDate,
        this.auth.UserInfo["dateFormat"],
        this.translate.getDefaultLang()
      );
      this.viewApprovalOBSDateTo = formatDate(
        toDate,
        this.auth.UserInfo["dateFormat"],
        this.translate.getDefaultLang()
      );
    }
  }

  clearRespondedDaterange() {
    if (this.viewApprovalOBSDateFrom) {
      this.approvalOBSDateFrom = this.viewApprovalOBSDateFrom;
    } else {
      this.approvalOBSDateFrom = "";
    }

    if (this.viewApprovalOBSDateTo) {
      this.approvalOBSDateTo = this.viewApprovalOBSDateTo;
    } else {
      this.approvalOBSDateTo = "";
    }
  }

  cancelDialog() {
    this.approvalOBSDateFrom = "";
    this.approvalOBSDateTo = "";
    this.approvalObsDate = false;
  }

  deleteRecord() {
    var msg = "";
    this.translate.get("ALTDELCONFIRM").subscribe((confirmMsg) => {
      msg = confirmMsg;
    });
    if (this.selectedApproval == "") {
      this.messageService.add({
        severity: "error",
        detail: "Please select atleast one item.",
      });
    } else {
      this.confirmationService.confirm({
        message: msg,
        accept: () => {
          this.approvalservice
            .deleteApproval({
              cardId: Array.prototype.map
                .call(this.selectedApproval, (s) => s.CARDID)
                .toString(),
            })
            .subscribe((res) => {
              this.selectedApproval = [];
              this.getApproval({});
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedApproval = [];
          this.searchText = "";
          this.getApproval({});
        },
      });
    }
  }

  approve() {
    var msg = "";
    this.translate.get("ALERTAPPROVESELDATA").subscribe((confirmMsg) => {
      msg = confirmMsg;
    });
    if (this.selectedApproval == "") {
      this.messageService.add({
        severity: "error",
        detail: "Please select any one item for offline data(s) approval",
      });
    } else {
      this.confirmationService.confirm({
        message: msg,
        accept: () => {
          this.approvalservice
            .approveApproval({
              cardId: Array.prototype.map
                .call(this.selectedApproval, (s) => s.CARDID)
                .toString(),
            })
            .subscribe((res) => {
              this.getApproval({});
              this.searchText = "";
              this.selectedApproval = [];
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedApproval = [];
          this.getApproval({});
          this.searchText = "";
        },
      });
    }
  }

  approveAll() {
    let cardId = "";
    if (this.approvalListing.length > 0) {
      cardId = this.approvalListing.map((item) => item.CARDID).join(",");
      let msg = "";
      this.translate.get("ALERTAPPROVEALLSELDATA").subscribe((confirmMsg) => {
        msg = confirmMsg;
      });
      this.confirmationService.confirm({
        message: msg,
        accept: () => {
          this.approvalservice
            .approveApproval({
              cardId,
            })
            .subscribe((res) => {
              this.getApproval({});
              this.searchText = "";
              this.selectedApproval = [];
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedApproval = [];
          this.getApproval({});
          this.searchText = "";
        },
      });
    }
  }

  toggleFilterShow() {
    this.animationState = this.animationState === "out" ? "in" : "out";
  }

  // Advance serach
  advanceSearch() {
    this.iconType =
      this.iconType === "ui-icon-add" ? "ui-icon-remove" : "ui-icon-add";
    this.showAdvanceSearch = !this.showAdvanceSearch;
  }

  onSelectAll() {
    const selected = this.filterSites.map((item) => item.SITEID);
    this.approvalSites = selected;
    this.getSiteFilter();
  }

  onClearAll() {
    this.approvalSites = [];
    this.getSiteFilter();
  }

  onSelectAllArea() {
    const selectedArea = this.filterArea.map((item) => item.AREAID);
    this.approvalAreas = selectedArea;
    this.getAreaFilter();
  }

  onClearAllArea() {
    this.approvalAreas = [];
    this.getAreaFilter();
  }

  onSelectAllSubArea() {
    const selectedSubArea = this.filterSubArea.map((item) => item.AREAID);
    this.approvalSubAreas = selectedSubArea;
  }

  onClearAllSubArea() {
    this.approvalSubAreas = [];
  }

  onSelectAllShift() {
    const selectedShift = this.filterShift.map((item) => item.SHIFTID);
    this.approvalShift = selectedShift;
  }

  onClearAllShift() {
    this.approvalShift = [];
  }

  onSelectAllObs() {
    const selectedObs = this.filterObserver.map((item) => item.USERID);
    this.approvalOBS = selectedObs;
  }

  onClearAllObs() {
    this.approvalOBS = [];
  }

  onSelectAllStatus() {
    const selectedStatus = this.filterStatus.map((item) => item.VALUEID);
    this.approvalStatus = selectedStatus;
  }

  onClearAllStatus() {
    this.approvalStatus = [];
  }

  customSort(event: SortEvent) {
    event.data.sort((data1, data2) => {
      let value1 = data1[event.field];
      let value2 = data2[event.field];

      // if (this.isDateColumn(event.field)) {

      // }

      let result = null;

      if (value1 == null && value2 != null) result = -1;
      else if (value1 != null && value2 == null) result = 1;
      else if (value1 == null && value2 == null) result = 0;
      else if (typeof value1 === "string" && typeof value2 === "string") {
        const date1 = moment(
          value1,
          this.auth.UserInfo["dateFormat"].toUpperCase()
        );
        const date2 = moment(
          value2,
          this.auth.UserInfo["dateFormat"].toUpperCase()
        );

        let result: number = -1;
        if (moment(date2).isBefore(date1, "day")) {
          result = 1;
        }

        return result * event.order;
      }
      // result = value1.localeCompare(value2);
      else result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;

      return event.order * result;
    });
  }
}
