import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDefinedFieldsTranslationsComponent } from './user-defined-fields-translations.component';

describe('UserDefinedFieldsTranslationsComponent', () => {
  let component: UserDefinedFieldsTranslationsComponent;
  let fixture: ComponentFixture<UserDefinedFieldsTranslationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDefinedFieldsTranslationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDefinedFieldsTranslationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
