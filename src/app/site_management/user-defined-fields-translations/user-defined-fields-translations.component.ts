import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { MessageService } from "primeng/api";

@Component({
  selector: "app-user-defined-fields-translations",
  templateUrl: "./user-defined-fields-translations.component.html",
  styleUrls: ["./user-defined-fields-translations.component.css"]
})
export class UserDefinedFieldsTranslationsComponent implements OnInit {
  constructor(
    private breadcrumbService: BreadcrumbService,
    private messageService: MessageService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLSITEMGMT",
        url: "./assets/help/user-defined-fields-list-translate.md"
      },
      {
        label: "LBLUSERDEFINEDFILEDLISTING",
        routerLink: ["/user-defind-field"]
      }
    ]);
  }
  ngOnInit() { }
}
