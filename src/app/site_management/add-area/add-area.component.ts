import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { Router } from "@angular/router";

import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { AreaService } from "src/app/_services/area.service";

import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;

@Component({
  selector: "app-add-area",
  templateUrl: "./add-area.component.html",
  styleUrls: ["./add-area.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class AddAreaComponent implements OnInit {
  AddAreas: FormGroup;
  submitted: boolean;

  //  Declare name of all fields
  isLoading: boolean = true;
  status: SelectItem[];
  siteListing: any[];
  subAreaListing: any[];
  defaultSite: any;
  initialGeneralFormData: any;
  subAreaOptionValue: any;
  checkFormSubmit: boolean = true;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private areaservice: AreaService,
    private router: Router,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private auth: AuthenticationService,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/add-area.md" },
      { label: "LBLAREAS", routerLink: ["/area-listing"] },
    ]);
  }

  ngOnInit() {
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.AddAreas = this.fb.group({
      areaName: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.noWhitespaceValidator,
        ])
      ),
      siteIds: new FormControl(null),
      subareaIds: new FormControl(null),
      status: new FormControl(1, Validators.required),
    });
    this.getAreaData();

    this.status = [];
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: Object) => {
        this.status.push({ label: res["LBLACTIVE"], value: 1 });
        this.status.push({ label: res["LBLINACTIVE"], value: 0 });
      });
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  getAreaData() {
    this.areaservice.getAddAreaData().subscribe((res) => {
      if (res["status"] == true) {
        // this.siteListing = res["site"];
        this.siteListing = [];
        var siteList = res["site"];
        siteList.map((data) => {
          this.siteListing.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });
        // this.subAreaListing = res["subArea"];
        this.subAreaListing = [];
        var subAreaList = res["subArea"];
        subAreaList.map((data) => {
          this.subAreaListing.push({
            label: data["SUBAREANAME"],
            value: data["SUBAREAID"],
          });
        });
        if (res["defaultSite"] != "") {
          var defaultSite = res["defaultSite"].toString().split(",");
          this.AddAreas.controls["siteIds"].setValue(defaultSite);
        }
        this.initialGeneralFormData = this.AddAreas.getRawValue();
      } else {
        this.siteListing = [];
        this.subAreaListing = [];
      }
      this.isLoading = false;
    });
  }

  get uform() {
    return this.AddAreas.controls;
  }
  onSubmit(bType) {
    this.submitted = true;
    if (this.AddAreas.invalid) {
      this.customValidatorsService.scrollToError();
      let target;
      target = this.elementRef.nativeElement.querySelector(".ng-invalid");
      if (target) {
        $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
        target.focus();
      }
    } else {
      var siteIds = this.AddAreas.value["siteIds"];
      if (this.checkFormSubmit == true) {
        this.checkFormSubmit = false;
        if (this.AddAreas.value["siteIds"] != null) {
          this.AddAreas.value["siteIds"] = this.AddAreas.value[
            "siteIds"
          ].join();
        }
        if (this.AddAreas.value["subareaIds"] != null) {
          this.AddAreas.value["subareaIds"] = this.AddAreas.value[
            "subareaIds"
          ].join();
        }
        this.areaservice.insertArea(this.AddAreas.value).subscribe((res) => {
          if (res["status"] == true) {
            if (bType) {
              this.submitted = false;
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
              this.areaReset();
              this.AddAreas.markAsPristine();
            } else {
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
              setTimeout(() => {
                this.router.navigate(["./edit-area/" + res["id"]], {
                  skipLocationChange: true,
                });
              }, 1000);
            }
          } else {
            this.AddAreas.value["siteIds"] = siteIds;
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
          }
          this.checkFormSubmit = true;
        });
      }
    }
  }

  areaReset() {
    this.AddAreas.patchValue(this.initialGeneralFormData);
  }

  onSelectAll() {
    this.AddAreas.markAsDirty();
    const selectedSubArea = this.subAreaListing.map((item) => item.SUBAREAID);
    this.AddAreas.get("subareaIds").patchValue(selectedSubArea);
  }

  onClearAll() {
    this.AddAreas.markAsDirty();
    this.AddAreas.get("subareaIds").patchValue([]);
  }
  onSelectAllAssign() {
    const selected = this.siteListing.map((item) => item.SITEID);
    this.AddAreas.markAsDirty();
    this.AddAreas.get("siteIds").patchValue(selected);
  }

  onClearAllAssign() {
    this.AddAreas.markAsDirty();
    this.AddAreas.get("siteIds").patchValue([]);
  }
}
