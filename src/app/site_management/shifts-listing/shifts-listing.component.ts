import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";

import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import { CloneVisitor } from "@angular/compiler/src/i18n/i18n_ast";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { ShiftService } from "src/app/_services/shift.service";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
declare var $: any;

@Component({
  selector: "app-shifts-listing",
  templateUrl: "./shifts-listing.component.html",
  styleUrls: ["./shifts-listing.component.css"],
  providers: [ConfirmationService, MessageService],
})
export class ShiftsListingComponent implements OnInit {
  //  Variable Declaration
  filterForm: FormGroup;
  cities1: SelectItem[];
  cols: any[];
  cities2: SelectItem[];
  colsCount: any;
  cars1: any;
  userlevel: any;

  allShift: any;
  shiftListing: any;
  dataCols: any[];
  dataCount: any;
  selectedfilterShift: string = "SHIFTNAME";
  searchBy: SelectItem[];
  selectedData: any = [];
  appName: any;

  visibleTable: boolean = true;
  searchText: any;
  searchByShift: SelectItem[];
  selectedShift: number = 1;
  isLoaded: boolean;
  confirmClass: any;
  roleId: any;
  // Breadcrumb Service
  constructor(
    private breadcrumbService: BreadcrumbService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private shiftservice: ShiftService,
    private route: Router,
    private env: EnvService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/shifts.md" },
      { label: "LBLSHIFTLISTING", routerLink: ["/shifts-listing"] },
    ]);
  }

  ngOnInit() {
    this.roleId = this.auth.UserInfo["roleId"];
    //Get Area
    this.getShift();

    this.translate
      .get(["LBLASSIGNEDSHIFTS", "LBLALLSHIFTS", "LBLSHIFTNAME", "LBLSTATUS"])
      .subscribe((res: Object) => {
        // Table header
        this.dataCols = [
          { field: "SHIFTNAME", header: res["LBLSHIFTNAME"] },
          { field: "STATUS", header: res["LBLSTATUS"] },
        ];

        // col of status
        this.dataCount = this.dataCols.length;

        // Search List
        this.searchByShift = [];
        this.searchByShift.push({ label: res["LBLASSIGNEDSHIFTS"], value: 1 });
        this.searchByShift.push({ label: res["LBLALLSHIFTS"], value: 0 });

        // Search List
        this.searchBy = [];
        this.searchBy.push({ label: res["LBLSHIFTNAME"], value: "SHIFTNAME" });
      });
    this.appName = this.env.appName;
  }

  getShift() {
    this.shiftservice.getShiftData({}).subscribe((res) => {
      if (res["status"] == true) {
        this.allShift = res["Data"];
        this.shiftListing = res["Data"];
        this.visibleTable = true;
      } else {
        this.allShift = [];
        this.shiftListing = [];
        this.visibleTable = true;
      }
      this.isLoaded = true;
    });
  }

  // searching data
  searching() {
    var field = this.selectedfilterShift;
    var stext = this.searchText.toLowerCase();
    var fn = this;
    if (stext != "") {
      var newList = this.allShift.filter((item) => {
        return item[field].toLowerCase().indexOf(stext) >= 0;
      });
      this.shiftListing = newList;
      setTimeout(function () {}, 10);
    } else {
      setTimeout(function () {}, 10);
      this.shiftListing = this.allShift;
    }
  }

  deleteRecord() {
    this.confirmClass = "warning-msg";
    this.translate.get("ALTDELCONFIRMSHIFTS").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var shiftIds = [];
          $.each(this.selectedData, function (key, ShiftId) {
            shiftIds.push(ShiftId["SHIFTID"]);
          });
          var shiftId = shiftIds.join();
          this.shiftservice
            .deleteShift({ shiftId: shiftId })
            .subscribe((res) => {
              this.selectedData = [];
              this.getShift();
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedData = [];
          this.getShift();
          this.searchText = "";
        },
      });
    });
  }

  changeStatus(id) {
    this.confirmClass = "info-msg";
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var shiftIds = [];
          var shiftId;
          if (id == "") {
            $.each(this.selectedData, function (key, ShiftId) {
              shiftIds.push(ShiftId["SHIFTID"]);
            });
            shiftId = shiftIds.join();
          } else {
            shiftId = id;
          }

          this.shiftservice
            .updateShifttatus({ shiftId: shiftId })
            .subscribe((res) => {
              this.selectedData = [];
              this.getShift();
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedData = [];
          this.getShift();
          this.searchText = "";
        },
      });
    });
  }
}
