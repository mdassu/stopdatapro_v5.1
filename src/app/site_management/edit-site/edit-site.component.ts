import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { ColorPickerModule } from "primeng/colorpicker";
import { Router, ActivatedRoute } from "@angular/router";

import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { SitesService } from "src/app/_services/sites.service";

import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";
declare var $: any;
@Component({
  selector: "app-edit-site",
  templateUrl: "./edit-site.component.html",
  styleUrls: ["./edit-site.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class EditSiteComponent implements OnInit {
  addSite: FormGroup;
  isLoading: boolean = true;
  submitted: boolean;
  targetForm: FormGroup;
  targetSubmitted: boolean;
  selectedValues: string[] = [];
  countryListing: SelectItem[];
  status: SelectItem[];
  timeZoneListing: SelectItem[];
  siteData: any;
  siteId: any;
  siteNameDs: any;
  assignmentType: SelectItem[];
  assignmentListing: any = [];
  groupType: SelectItem[];
  selectedGroup: any;
  itemLable: any;
  itemValue: any;
  placeHolder: any;
  selectedType: any;
  selectedAvailable: any;
  showArea: boolean = true;
  showShift: boolean = true;
  showUser: boolean = true;
  showGroup: boolean = true;
  Target: SelectItem[];
  tTarget: boolean = true;
  wTarget: boolean = false;
  targetMonthly: any;
  targetWeekly: any;
  targetWeek: any;
  initialGeneralFormData: any;
  initialTargetData: any;
  initialSiteData: any;
  index = 0;
  lastIndex = 0;
  isAssignmentsChanged: boolean;
  assignmentsSaveBtn = true;
  appName: any;
  generalLoading: boolean = false;
  assignLoading: boolean = false;
  targetLoading: boolean = false;
  selectedAreaMng: any;
  areaManager: any;
  userAreaManager: any;
  selecteduserAreaManager: any;
  areaManagerSaveBtn = true;
  areaManagerOptionValue: any;

  months: any = [
    { monthname: "LBLPOPULATETARGET", name: "all" },
    { monthname: "FMKJANUARY", name: "jan" },
    { monthname: "FMKFEBRUARY", name: "feb" },
    { monthname: "FMKMARCH", name: "mar" },
    { monthname: "FMKAPRIL", name: "apr" },
    { monthname: "FMKMAAY", name: "may" },
    { monthname: "FMKJUNE", name: "jun" },
    { monthname: "FMKJULY", name: "jul" },
    { monthname: "FMKAUGUST", name: "aug" },
    { monthname: "FMKSEPTEMBER", name: "sep" },
    { monthname: "FMKOCTOBER", name: "oct" },
    { monthname: "FMKNOVEMBER", name: "nov" },
    { monthname: "FMKDECEMBER", name: "dec" },
  ];

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private sitesservice: SitesService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private activetedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private translate: TranslateService,
    private env: EnvService,
    private auth: AuthenticationService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/edit-site.md" },
      { label: "LBLBSITES", routerLink: ["/site-listing"] },
    ]);
  }

  ngOnInit() {
    this.siteId = this.activetedRoute.snapshot.paramMap.get("siteid");
    this.areaManagerOptionValue = this.auth.UserInfo["areaManagerOptionValue"];

    var data = {
      siteName: "",
      address: "",
      city: "",
      state: "",
      zipCode: "",
      comments: "",
      status: 1,
      employees: "",
      countryId: "",
      defaultTimeZone: "",
    };

    this.createFormBuilder(data);
    this.getSite();
    this.getSiteAssignData();

    this.translate
      .get([
        "LBLACTIVE",
        "LBLINACTIVE",
        "LBLAREAS",
        "LBLSHIFTS",
        "LBLUSERS",
        "LBLGROUPS",
        "LBLMONTHLY",
        "LBLWEEKLY",
      ])
      .subscribe((res: Object) => {
        this.status = [];
        this.status.push({ label: res["LBLACTIVE"], value: 1 });
        this.status.push({ label: res["LBLINACTIVE"], value: 0 });

        this.assignmentType = [];
        this.assignmentType.push({ label: res["LBLAREAS"], value: 1 });
        this.assignmentType.push({ label: res["LBLSHIFTS"], value: 2 });
        this.assignmentType.push({ label: res["LBLUSERS"], value: 3 });
        this.assignmentType.push({ label: res["LBLGROUPS"], value: 4 });

        this.Target = [];
        this.Target.push({ label: res["LBLMONTHLY"], value: 0 });
        this.Target.push({ label: res["LBLWEEKLY"], value: 1 });
      });
    this.targetForm = this.fb.group({
      siteId: new FormControl(null),
      targets: new FormControl(0),
      all: new FormControl(""),
      jan: new FormControl(null),
      feb: new FormControl(null),
      mar: new FormControl(null),
      apr: new FormControl(null),
      may: new FormControl(null),
      jun: new FormControl(null),
      jul: new FormControl(null),
      aug: new FormControl(null),
      sep: new FormControl(null),
      oct: new FormControl(null),
      nov: new FormControl(null),
      dec: new FormControl(null),
      applyToAllObser: new FormControl(0),
      weeklyTarget: new FormControl(null),
    });
    this.appName = this.env.appName;
  }

  getSite() {
    var parameters = {
      siteId: this.siteId,
    };
    this.sitesservice.getSiteData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.siteData = res["Data"];
        var siteData = res["Data"];
        this.countryListing = res["country"];

        this.translate.get("LBLSELECT").subscribe((text: string) => {
          var areaSelect;
          areaSelect = {
            AREANAME: text,
            AREAID: 0,
          };
          this.areaManager = res["areas"];
          this.areaManager.unshift(areaSelect);
        });
        this.selectedAreaMng = 0;
        this.userAreaManager = [];
        var userAreaManagerList = res["areaAvailable"];
        userAreaManagerList.map((data) => {
          this.userAreaManager.push({
            label: data["FULLNAME"],
            value: data["USERID"],
          });
        });
        this.timeZoneListing = res["timeZone"];
        this.timeZoneListing.map((i) => {
          this.translate.get(i["TIMEZONENAME"]).subscribe((label) => {
            i["TIMEZONENAME"] = label;
          });
        });
        this.targetWeek = res["targetWeekly"];
        this.siteNameDs = siteData["SITENAME"];
        this.addSite.controls["countryId"].setValue(
          this.countryListing[0]["COUNTRYID"]
        );
        this.addSite.controls["defaultTimeZone"].setValue(
          this.timeZoneListing[0]["TIMEZONEID"]
        );

        if (res["targetMonthly"] != "") {
          var targetMonthly = res["targetMonthly"];
          this.targetForm.controls["jan"].setValue(targetMonthly["JAN"]);
          this.targetForm.controls["feb"].setValue(targetMonthly["FEB"]);
          this.targetForm.controls["mar"].setValue(targetMonthly["MAR"]);
          this.targetForm.controls["apr"].setValue(targetMonthly["APR"]);
          this.targetForm.controls["may"].setValue(targetMonthly["MAY"]);
          this.targetForm.controls["jun"].setValue(targetMonthly["JUN"]);
          this.targetForm.controls["jul"].setValue(targetMonthly["JUL"]);
          this.targetForm.controls["aug"].setValue(targetMonthly["AUG"]);
          this.targetForm.controls["sep"].setValue(targetMonthly["SEP"]);
          this.targetForm.controls["oct"].setValue(targetMonthly["OCT"]);
          this.targetForm.controls["nov"].setValue(targetMonthly["NOV"]);
          this.targetForm.controls["dec"].setValue(targetMonthly["DEC"]);
        }

        if (res["targetWeekly"] != "") {
          var targetWeekly = res["targetWeekly"];
          this.targetForm.controls["weeklyTarget"].setValue(
            targetWeekly["WEEKTARGET"]
          );
        }

        var data = {
          siteName: siteData.SITENAME,
          address: siteData.ADDRESS,
          city: siteData.CITY,
          state: siteData.STATE,
          zipCode: siteData.ZIPCODE,
          comments: siteData.COMMENTS,
          status: siteData.STATUS,
          employees: siteData.EMPCOUNT,
          countryId: siteData.COUNTRYID,
          defaultTimeZone: siteData.TIMEZONEID,
        };
        this.createFormBuilder(data);
        this.initialGeneralFormData = this.addSite.getRawValue();
      }
      this.isLoading = false;
    });
  }

  getSiteAssignData() {
    var parameters = {
      siteId: this.siteId,
      assignType: 1,
    };
    this.sitesservice.getSiteAssignData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        // this.assignmentListing = res["available"];

        this.assignmentListing = [];
        var assignmentList = res["available"];
        assignmentList.map((data) => {
          this.assignmentListing.push({
            label: data["AREANAME"],
            value: data["AREAID"],
          });
        });

        this.showArea = false;
        this.placeHolder = "LBLAREA";
        this.selectedType = 1;
        if (res["assign"] != "") {
          this.selectedAvailable = res["assign"].split(",");
        } else {
          this.selectedAvailable = "";
        }
        this.showGroup = true;
      }
      this.isAssignmentsChanged = false;
    });
  }

  changeAssignment(assignType) {
    this.assignmentsSaveBtn = false;
    var parameters;
    if (assignType == 1) {
      parameters = {
        siteId: this.siteId,
        assignType: assignType,
      };
      this.showGroup = true;
    } else if (assignType == 2) {
      parameters = {
        siteId: this.siteId,
        assignType: assignType,
      };
      this.showGroup = true;
    } else if (assignType == 3) {
      parameters = {
        siteId: this.siteId,
        assignType: assignType,
      };
      this.showGroup = true;
    } else {
      parameters = {
        siteId: this.siteId,
        assignType: assignType,
        groupTypeId: 0,
      };
      this.showGroup = false;
    }
    this.sitesservice.getSiteAssignData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.assignmentListing = [];
        var sassignmentList = res["available"];
        sassignmentList.map((data) => {
          if (assignType == 1) {
            this.assignmentListing.push({
              label: data["AREANAME"],
              value: data["AREAID"],
            });
          } else if (assignType == 2) {
            this.assignmentListing.push({
              label: data["SHIFTNAME"],
              value: data["SHIFTID"],
            });
          } else if (assignType == 3) {
            this.assignmentListing.push({
              label: data["FULLNAME"],
              value: data["USERID"],
            });
          } else {
            this.assignmentListing.push({
              label: data["ENTITYVALUE"],
              value: data["GROUPID"],
            });
          }
        });
        if (res["assign"] != "") {
          this.selectedAvailable = res["assign"].split(",");
        } else {
          this.selectedAvailable = "";
        }

        if (assignType == 1) {
          this.placeHolder = "LBLAREA";
          this.itemLable = "AREANAME";
          this.itemValue = "AREAID";
        } else if (assignType == 2) {
          this.placeHolder = "LBLSHIFT";
          this.itemLable = "SHIFTNAME";
          this.itemValue = "SHIFTID";
        } else if (assignType == 3) {
          this.placeHolder = "LBLUSER";
          this.itemLable = "FULLNAME";
          this.itemValue = "USERID";
        } else {
          this.placeHolder = "LBLGROUP";
          this.itemLable = "ENTITYVALUE";
          this.itemValue = "GROUPID";
          if (res["group"] != "") {
            this.groupType = res["group"];
            this.selectedGroup = res["group"][0]["value"];
            this.changeGroup(res["group"][0]["value"]);
          } else {
            this.groupType = [];
            this.selectedGroup = [];
          }
        }
      }
    });
  }

  changeGroup(groupId) {
    this.assignmentsSaveBtn = false;
    var parameters;
    parameters = {
      siteId: this.siteId,
      assignType: 4,
      groupTypeId: groupId,
    };

    this.sitesservice.getSiteAssignData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.assignmentListing = [];
        var assignmentList = res["available"];
        assignmentList.map((data) => {
          this.assignmentListing.push({
            label: data["ENTITYVALUE"],
            value: data["GROUPID"],
          });
        });
        if (res["assign"] != "") {
          this.selectedAvailable = res["assign"].split(",");
        } else {
          this.selectedAvailable = "";
        }
        if (res["group"] != "") {
          this.groupType = res["group"];
        }
        this.placeHolder = "LBLGROUP";
        this.itemLable = "ENTITYVALUE";
        this.itemValue = "GROUPID";
      }
    });
  }

  get innerForm() {
    return this.addSite.controls;
  }
  onSubmit(bType) {
    this.submitted = true;
    if (this.addSite.invalid) {
      this.customValidatorsService.scrollToError();
      // let target;
      // target = this.elementRef.nativeElement.querySelector(".ng-invalid");
      // if (target) {
      //   $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
      //   target.focus();
      // }
    } else {
      this.generalLoading = true;
      this.addSite.value["siteId"] = this.siteId;
      this.sitesservice.updateSite(this.addSite.value).subscribe((res) => {
        if (res["status"] == true) {
          this.initialSiteData = this.addSite.getRawValue();
          if (bType) {
            this.cookieService.set("edit-site", "yes");
            this.router.navigate(["./add-site"], {
              skipLocationChange: true,
            });
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          } else {
            this.submitted = false;
            this.addSite.reset();
            this.getSite();
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          }
          this.generalLoading = false;
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
          this.generalLoading = false;
        }
      });
    }
  }

  createFormBuilder(data) {
    this.addSite = this.fb.group({
      siteName: new FormControl(
        data.siteName,
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.noWhitespaceValidator,
        ])
      ),
      address: new FormControl(
        data.address,
        Validators.compose([
          Validators.required,
          Validators.maxLength(1024),
          this.noWhitespaceValidator,
        ])
      ),
      city: new FormControl(
        data.city,
        Validators.compose([
          Validators.required,
          Validators.maxLength(128),
          this.noWhitespaceValidator,
        ])
      ),
      state: new FormControl(data.state),
      zipCode: new FormControl(data.zipCode),
      comments: new FormControl(data.comments),
      status: new FormControl(data.status, Validators.required),
      employees: new FormControl(data.employees),
      countryId: new FormControl(data.countryId, Validators.required),
      defaultTimeZone: new FormControl(
        data.defaultTimeZone,
        Validators.required
      ),
    });
    this.initialSiteData = this.addSite.getRawValue();
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  updateSiteAssign() {
    if (this.selectedAvailable) {
      this.assignmentListing = this.auth.rearrangeSelects(
        this.assignmentListing,
        this.selectedAvailable
      );
    }
    var parameters;
    var selectedAssign;
    this.assignLoading = true;
    if (this.selectedAvailable != "") {
      selectedAssign = this.selectedAvailable.join();
    } else {
      selectedAssign = "";
    }

    if (this.selectedType == 4) {
      parameters = {
        siteId: this.siteId,
        assignType: this.selectedType,
        groupTypeId: this.selectedGroup,
        assignIds: selectedAssign,
      };
    } else {
      parameters = {
        siteId: this.siteId,
        assignType: this.selectedType,
        assignIds: selectedAssign,
      };
    }
    this.sitesservice.updateSiteAssign(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "success",
            detail: res,
          });
        });
        this.assignmentsSaveBtn = true;
        this.assignLoading = false;
        this.getSite();
      } else {
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
        this.assignLoading = false;
      }
      this.isAssignmentsChanged = false;
    });
  }

  targetOpen() {
    if (this.targetForm.value["targets"] == 0) {
      this.tTarget = true;
      this.wTarget = false;
      this.targetForm.controls["weeklyTarget"].clearValidators();
      this.targetForm.controls["weeklyTarget"].updateValueAndValidity();
    } else {
      this.getSite();
      this.targetForm.controls["all"].setValue("");
      this.targetForm.controls["weeklyTarget"].setValidators([
        Validators.required,
      ]);
      if (this.targetWeek != "") {
        this.targetForm.controls["weeklyTarget"].setValue(
          this.targetWeek["WEEKTARGET"]
        );
      } else {
        this.targetForm.controls["weeklyTarget"].setValue(null);
      }
      this.tTarget = false;
      this.wTarget = true;
    }
    this.initialTargetData = this.targetForm.getRawValue();

    this.targetForm.get("targets").markAsPristine();
  }

  setTargetValue(allMonthVal, rowName) {
    if (
      isNaN(allMonthVal) == true ||
      allMonthVal > 99 ||
      allMonthVal < 0 ||
      allMonthVal.startsWith(" ")
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      setTimeout(() => {
        this.targetForm.controls[rowName].reset({ value: "", disabled: false });
      }, 2000);
      this.targetForm.controls[rowName].reset({ value: "", disabled: true });
      this.targetForm.patchValue(this.initialTargetData);
    } else {
      this.targetForm.controls["jan"].setValue(allMonthVal);
      this.targetForm.controls["feb"].setValue(allMonthVal);
      this.targetForm.controls["mar"].setValue(allMonthVal);
      this.targetForm.controls["apr"].setValue(allMonthVal);
      this.targetForm.controls["may"].setValue(allMonthVal);
      this.targetForm.controls["jun"].setValue(allMonthVal);
      this.targetForm.controls["jul"].setValue(allMonthVal);
      this.targetForm.controls["aug"].setValue(allMonthVal);
      this.targetForm.controls["sep"].setValue(allMonthVal);
      this.targetForm.controls["oct"].setValue(allMonthVal);
      this.targetForm.controls["nov"].setValue(allMonthVal);
      this.targetForm.controls["dec"].setValue(allMonthVal);
    }
  }

  setValue(monthVal, rowName) {
    if (
      isNaN(monthVal) == true ||
      monthVal > 99 ||
      monthVal < 0 ||
      monthVal.startsWith(" ")
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      setTimeout(() => {
        this.targetForm.controls[rowName].reset({ value: "", disabled: false });
      }, 2000);
      this.targetForm.controls[rowName].reset({ value: "", disabled: true });
    }
  }
  setWeeklyValue(monthVal) {
    if (
      isNaN(monthVal) == true ||
      monthVal > 99 ||
      monthVal < 0 ||
      monthVal.startsWith(" ")
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      setTimeout(() => {
        this.targetForm.controls["weeklyTarget"].reset({
          value: "",
          disabled: false,
        });
      }, 3200);
      this.targetForm.controls["weeklyTarget"].reset({
        value: "",
        disabled: true,
      });
    }
  }

  targetSubmit(value: string) {
    this.targetLoading = true;
    const jan = this.targetForm.value["jan"];
    const feb = this.targetForm.value["feb"];
    const mar = this.targetForm.value["mar"];
    const apr = this.targetForm.value["apr"];
    const may = this.targetForm.value["may"];
    const jun = this.targetForm.value["jun"];
    const jul = this.targetForm.value["jul"];
    const aug = this.targetForm.value["aug"];
    const sep = this.targetForm.value["sep"];
    const oct = this.targetForm.value["oct"];
    const nov = this.targetForm.value["nov"];
    const dec = this.targetForm.value["dec"];

    const checkAllNull =
      jan == null &&
      feb == null &&
      mar == null &&
      apr == null &&
      may == null &&
      jun == null &&
      jul == null &&
      aug == null &&
      sep == null &&
      oct == null &&
      nov == null &&
      dec == null;
    const checkAllNullOrEmpty =
      !jan &&
      !feb &&
      !mar &&
      !apr &&
      !may &&
      !jun &&
      !jul &&
      !aug &&
      !sep &&
      !oct &&
      !nov &&
      !dec;
    const weeklyMaxLength = Math.max(
      jan,
      feb,
      mar,
      apr,
      may,
      jun,
      jul,
      aug,
      sep,
      oct,
      nov,
      dec
    );
    if (
      this.targetForm.value["targets"] == 0 &&
      checkAllNullOrEmpty &&
      this.targetForm.value["targets"] != 1
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      this.targetLoading = false;
    } else if (
      weeklyMaxLength < 0 ||
      (weeklyMaxLength > 100 && !checkAllNull)
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      this.targetLoading = false;
    } else if (
      weeklyMaxLength < this.targetForm.value["weeklyTarget"] &&
      !checkAllNull
    ) {
      this.translate.get("ALTTARGETPERWEEK").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      setTimeout(() => {
        this.targetForm.controls["weeklyTarget"].reset({
          value: "",
          disabled: false,
        });
      }, 3200);
      this.targetForm.controls["weeklyTarget"].reset({
        value: "",
        disabled: true,
      });
      this.targetLoading = false;
    } else if (
      this.targetForm.value["weeklyTarget"] < 0 ||
      (this.targetForm.value["weeklyTarget"] > 100 && !checkAllNull)
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      this.targetLoading = false;
    } else {
      if (this.targetForm.invalid) {
        this.targetSubmitted = true;
        this.targetLoading = false;
      } else {
        this.targetSubmitted = false;
        var applyToAllObserValue;
        if (this.targetForm.value["applyToAllObser"][0] == undefined) {
          applyToAllObserValue = 0;
        } else {
          applyToAllObserValue = this.targetForm.value["applyToAllObser"][0];
        }
        if (applyToAllObserValue != 0) {
          this.translate
            .get("ALTTARGETFORALLOBSDEFAULT")
            .subscribe((res: string) => {
              this.confirmationService.confirm({
                message: res,
                key: "confirmDialogue",
                accept: () => {
                  if (this.targetForm.value["targets"] == 0) {
                    this.targetForm.value["siteId"] = this.siteId;
                    delete this.targetForm.value["all"];
                    delete this.targetForm.value["weeklyTarget"];
                    this.targetForm.value[
                      "applyToAllObser"
                    ] = this.targetForm.value["applyToAllObser"][0];
                  } else {
                    this.targetForm.value["siteId"] = this.siteId;
                    delete this.targetForm.value["all"];
                    delete this.targetForm.value["jan"];
                    delete this.targetForm.value["feb"];
                    delete this.targetForm.value["mar"];
                    delete this.targetForm.value["apr"];
                    delete this.targetForm.value["may"];
                    delete this.targetForm.value["jun"];
                    delete this.targetForm.value["jul"];
                    delete this.targetForm.value["aug"];
                    delete this.targetForm.value["sep"];
                    delete this.targetForm.value["oct"];
                    delete this.targetForm.value["nov"];
                    delete this.targetForm.value["dec"];
                    this.targetForm.value[
                      "applyToAllObser"
                    ] = this.targetForm.value["applyToAllObser"][0];
                  }
                  this.sitesservice
                    .updateTargetData(this.targetForm.value)
                    .subscribe((res) => {
                      if (res["status"] == true) {
                        this.targetForm.controls["applyToAllObser"].setValue(0);
                        this.translate
                          .get(res["message"])
                          .subscribe((res: string) => {
                            this.messageService.add({
                              severity: "success",
                              detail: res,
                            });
                          });
                        this.targetLoading = false;
                      } else {
                        this.translate
                          .get(res["message"])
                          .subscribe((res: string) => {
                            this.messageService.add({
                              severity: "error",
                              detail: res,
                            });
                          });
                        this.targetLoading = false;
                      }
                    });
                },
              });
            });
        } else {
          if (this.targetForm.value["targets"] == 0) {
            this.targetForm.value["siteId"] = this.siteId;
            delete this.targetForm.value["all"];
            delete this.targetForm.value["weeklyTarget"];
            this.targetForm.value["applyToAllObser"] = this.targetForm.value[
              "applyToAllObser"
            ][0];
          } else {
            this.targetForm.value["siteId"] = this.siteId;
            delete this.targetForm.value["all"];
            delete this.targetForm.value["jan"];
            delete this.targetForm.value["feb"];
            delete this.targetForm.value["mar"];
            delete this.targetForm.value["apr"];
            delete this.targetForm.value["may"];
            delete this.targetForm.value["jun"];
            delete this.targetForm.value["jul"];
            delete this.targetForm.value["aug"];
            delete this.targetForm.value["sep"];
            delete this.targetForm.value["oct"];
            delete this.targetForm.value["nov"];
            delete this.targetForm.value["dec"];
            this.targetForm.value["applyToAllObser"] = this.targetForm.value[
              "applyToAllObser"
            ][0];
          }
          this.sitesservice
            .updateTargetData(this.targetForm.value)
            .subscribe((res) => {
              if (res["status"] == true) {
                this.initialTargetData = this.targetForm.getRawValue();
                const formData = this.targetForm.getRawValue();
                this.targetForm.reset();
                this.targetForm.patchValue(formData);
                this.targetForm.controls["all"].setValue("");
                this.translate.get(res["message"]).subscribe((res: string) => {
                  this.messageService.add({
                    severity: "success",
                    detail: res,
                  });
                });
                this.targetLoading = false;
              } else {
                this.translate.get(res["message"]).subscribe((res: string) => {
                  this.messageService.add({
                    severity: "error",
                    detail: res,
                  });
                });
                this.targetLoading = false;
              }
            });
        }
      }
    }
  }

  siteReset() {
    this.addSite.patchValue(this.initialSiteData);
  }

  targetReset() {
    this.targetForm.patchValue(this.initialTargetData);
  }

  assignmentreset() {
    this.getSiteAssignData();
  }

  onAssignmentsSelectAll() {
    this.assignmentsSaveBtn = false;
    let selectedAvailable = [];
    this.assignmentListing.map((item) => {
      selectedAvailable.push(item[this.itemValue]);
    });
    this.selectedAvailable = selectedAvailable;
  }

  onAssignmentsClearAll() {
    this.assignmentsSaveBtn = false;
    this.selectedAvailable = [];
  }

  onChangeAvailable() {
    this.assignmentsSaveBtn = false;
    this.isAssignmentsChanged = true;
  }

  handleChange(e) {
    if (
      (this.addSite.dirty && this.lastIndex === 0) ||
      (this.isAssignmentsChanged && this.lastIndex === 1) ||
      (this.targetForm.dirty && this.lastIndex === 2)
    ) {
      this.translate.get("LBLCONFIRMYESNO").subscribe((text: string) => {
        this.confirmationService.confirm({
          message: text,
          header: this.appName,
          key: "tabChangeConfirmation",
          accept: () => {
            if (this.lastIndex === 0) {
              this.addSite.reset();
              this.addSite.patchValue(this.initialGeneralFormData);
            } else if (this.lastIndex === 1) {
              this.getSiteAssignData();
            } else {
              this.targetForm.reset();
              this.targetForm.patchValue(this.initialTargetData);
            }
            this.lastIndex = e.index;
            this.setHelpFile(e.index);
          },
          reject: () => {
            this.index = this.lastIndex;
            this.setHelpFile(this.lastIndex);
          },
        });
      });
    } else {
      this.lastIndex = e.index;
      this.setHelpFile(e.index);
    }
  }

  setHelpFile(index) {
    if (index == 0) {
      this.breadcrumbService.setItems([
        { label: "LBLSITEMGMT", url: "./assets/help/edit-site.md" },
        { label: "LBLBSITES", routerLink: ["/site-listing"] },
      ]);
    } else if (index == 1) {
      this.breadcrumbService.setItems([
        { label: "LBLSITEMGMT", url: "./assets/help/edit-site-assignments.md" },
        { label: "LBLBSITES", routerLink: ["/site-listing"] },
      ]);
    } else if (index == 2) {
      this.breadcrumbService.setItems([
        { label: "LBLSITEMGMT", url: "./assets/help/edit-site-targets.md" },
        { label: "LBLBSITES", routerLink: ["/site-listing"] },
      ]);
    }
  }

  getAreaManager(areaId) {
    this.areaManagerSaveBtn = false;
    var parameters = {
      siteId: this.siteId,
      areaId: areaId,
    };
    this.sitesservice.getAreaManagerData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.userAreaManager = [];
        var userAreaManagerList = res["userAssign"];
        userAreaManagerList.map((data) => {
          this.userAreaManager.push({
            label: data["FULLNAME"],
            value: data["USERID"],
          });
        });

        if (res["userAvailable"] != "") {
          this.selecteduserAreaManager = res["userAvailable"].split(",");
        } else {
          this.selecteduserAreaManager = "";
        }
      }
    });
  }

  updateAreaManager() {
    if (this.selecteduserAreaManager) {
      this.userAreaManager = this.auth.rearrangeSelects(
        this.userAreaManager,
        this.selecteduserAreaManager
      );
    }

    if (this.selectedAreaMng == 0) {
      this.translate.get("ALTPROVIDEAREA").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      return false;
    }
    var parameters;
    var selectedAssign;
    this.assignLoading = true;
    if (this.selecteduserAreaManager != "") {
      selectedAssign = this.selecteduserAreaManager.join();
    } else {
      selectedAssign = "";
    }

    parameters = {
      siteId: this.siteId,
      areaId: this.selectedAreaMng,
      userIds: selectedAssign,
    };

    this.sitesservice.updateAreaManager(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "success",
            detail: res,
          });
        });
        this.areaManagerSaveBtn = true;
        this.assignLoading = false;
      } else {
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
        this.assignLoading = false;
      }
    });
  }
  areaManagerreset() {
    this.getAreaManager(this.selectedAreaMng);
  }

  onChangeaArea() {
    this.areaManagerSaveBtn = false;
  }
}
