import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { EnvService } from "src/env.service";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { Router, ActivatedRoute } from "@angular/router";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { UserDefinedFieldService } from "src/app/_services/user-defined-field.service";
import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;

@Component({
  selector: "app-user-defind-fields",
  templateUrl: "./user-defind-fields.component.html",
  styleUrls: ["./user-defind-fields.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class UserDefindFieldsComponent implements OnInit {
  addValue: FormGroup;
  submitted: boolean;
  siteStatus: boolean = true;
  isLoading: boolean = true;
  //  Declare name of all fields
  inputvalue: any;
  fieldId: any;
  userDefineListing: any;
  visibleTable: boolean = true;
  dataCols: any[];
  dataCount: any;
  sitelisting: any[];
  selectedData: any = [];
  selectedUDF: any;
  selectedAllSite: any;
  selectedSites: any = [];
  fieldName: any;
  isLoaded: boolean;
  assignmentSaveBtn = true;
  initData: {};
  appName: any;
  parameters: {};
  assignLoading: boolean = false;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private userdefinedfieldservice: UserDefinedFieldService,
    private route: Router,
    private activetedRoute: ActivatedRoute,
    private env: EnvService,
    private translate: TranslateService,
    private auth: AuthenticationService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLSITEMGMT",
        url: "./assets/help/edit-user-defined-fields.md",
      },
      { label: "LBLUSERDEFINEDFEILDS", routerLink: ["/user-defind-field"] },
    ]);
  }

  ngOnInit() {
    this.fieldId = this.activetedRoute.snapshot.paramMap.get("userdefindid");
    this.getUserDefinedFields();
    this.translate.get("LBLFIELDVALUES").subscribe((res: string) => {
      // Table header
      this.dataCols = [{ field: "CUSTOMFIELDVALUE", header: res }];

      // col of status
      this.dataCount = this.dataCols.length;
    });

    this.addValue = this.fb.group({
      fieldValue: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(128),
          this.noWhitespaceValidator,
        ])
      ),
    });
    this.appName = this.env.appName;
  }

  getUserDefinedFields() {
    var parameters = {
      fieldId: this.fieldId,
    };
    this.userdefinedfieldservice
      .getUserDefinedData(parameters)
      .subscribe((res) => {
        if (res["status"] == true) {
          this.userDefineListing = res["Data"];
          this.getAssignUserDefined(res["Data"][0]["CUSTOMFIELDVALUEID"]);
          this.selectedUDF = res["Data"][0]["CUSTOMFIELDVALUEID"];
          this.fieldName = res["field"];
        } else {
          this.userDefineListing = [];
        }

        this.isLoaded = true;
        this.isLoading = false;
      });
  }

  changeSiteStatus(val) {
    this.assignmentSaveBtn = false;
    this.siteStatus = val;
  }

  selectedSiteChange() {
    this.assignmentSaveBtn = false;
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  onSubmit(value: string) {
    this.submitted = true;
    if (this.addValue.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.addValue.value["fieldId"] = this.fieldId;
      this.translate
        .get("ALTCUSTVALUEASIIGNCONFIRMALLSITES")
        .subscribe((res: string) => {
          this.confirmationService.confirm({
            message: res,
            accept: () => {
              this.addValue.value["allSites"] = 1;
              this.userdefinedfieldservice
                .insertUserDefinedData(this.addValue.value)
                .subscribe((res) => {
                  if (res["status"] == true) {
                    this.getUserDefinedFields();
                    this.addValue.reset();
                    this.translate
                      .get(res["message"])
                      .subscribe((res: string) => {
                        this.messageService.add({
                          severity: "success",
                          detail: res,
                        });
                      });
                  } else {
                    this.translate
                      .get(res["message"])
                      .subscribe((res: string) => {
                        this.messageService.add({
                          severity: "error",
                          detail: res,
                        });
                      });
                  }
                });
            },
            reject: () => {
              this.addValue.value["allSites"] = 0;
              this.userdefinedfieldservice
                .insertUserDefinedData(this.addValue.value)
                .subscribe((res) => {
                  if (res["status"] == true) {
                    this.getUserDefinedFields();
                    this.addValue.reset();
                    this.translate
                      .get(res["message"])
                      .subscribe((res: string) => {
                        this.messageService.add({
                          severity: "success",
                          detail: res,
                        });
                      });
                  } else {
                    this.translate
                      .get(res["message"])
                      .subscribe((res: string) => {
                        this.messageService.add({
                          severity: "error",
                          detail: res,
                        });
                      });
                  }
                });
            },
          });
        });
    }
  }

  deleteRecord() {
    this.translate
      .get("ALTDELCONFIRMCUSTOMFIELDVALUE")
      .subscribe((res: string) => {
        this.confirmationService.confirm({
          message: res,
          accept: () => {
            var fieldValIds = [];
            $.each(this.selectedData, function (key, fieldvalId) {
              fieldValIds.push(fieldvalId["CUSTOMFIELDVALUEID"]);
            });
            var fieldvalId = fieldValIds.join();

            var parameters = {
              fieldId: this.fieldId,
              customFieldValueId: fieldvalId,
            };

            this.userdefinedfieldservice
              .deleteUserDefunedField(parameters)
              .subscribe((res) => {
                this.selectedData = [];
                this.getUserDefinedFields();
                this.translate.get(res["message"]).subscribe((res: string) => {
                  this.messageService.add({
                    severity: "success",
                    detail: res,
                  });
                });
              });
          },
        });
      });
  }

  getAssignUserDefined(fieldValId) {
    var parameters = {
      fieldId: this.fieldId,
      customFieldValueId: fieldValId,
    };
    this.initData = {
      fieldId: this.fieldId,
      allSites: this.selectedAllSite,
    };
    this.userdefinedfieldservice
      .getAssignUserDefinedData(parameters)
      .subscribe((res) => {
        if (res["status"] == true) {
          if (res["valueForAllSite"] == 0) {
            this.siteStatus = false;
          } else {
            this.siteStatus = true;
          }
          this.sitelisting = [];
          var siteList = res["available"];
          siteList.map((data) => {
            this.sitelisting.push({
              label: data["SITENAME"],
              value: data["SITEID"],
            });
          });
          this.selectedAllSite = res["valueForAllSite"];
          if (res["assign"] != "") {
            this.selectedSites = res["assign"].split(",");
          } else {
            this.selectedSites = [];
          }
          this.initData["customFieldValueId"] =
            res["fieldValue"][0]["CUSTOMFIELDVALUEID"];
        } else {
          this.sitelisting = [];
        }
        this.initData["assignIds"] = this.selectedSites;
        this.initData["allSites"] = this.selectedAllSite;
      });
  }

  updateAssignField() {
    if (this.selectedSites) {
      this.sitelisting = this.auth.rearrangeSelects(
        this.sitelisting,
        this.selectedSites
      );
    }
    this.assignLoading = true;
    let selectedSites;
    if (this.selectedSites != "") {
      selectedSites = this.selectedSites.join();
    } else {
      selectedSites = "";
    }
    this.parameters = {
      fieldId: this.fieldId,
      customFieldValueId: this.selectedUDF,
      allSites: this.selectedAllSite,
      assignIds: selectedSites,
    };
    this.userdefinedfieldservice
      .updatUserDefunedData(this.parameters)
      .subscribe((res) => {
        if (res["status"] == true) {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "success",
              detail: res,
            });
            this.assignmentSaveBtn = true;
          });
          this.initData = {
            fieldId: this.fieldId,
            customFieldValueId: this.selectedUDF,
            allSites: this.selectedAllSite,
            assignIds: this.selectedSites,
          };
          this.assignLoading = false;
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
          this.assignLoading = false;
        }
      });
  }
  onSelectAll() {
    this.assignmentSaveBtn = false;
    const selected = this.sitelisting.map((item) => item.SITEID);
    this.selectedSites = selected;
  }

  onClearAll() {
    this.assignmentSaveBtn = false;
    this.selectedSites = [];
  }

  onReset() {
    this.selectedSites = this.initData["assignIds"];
    this.selectedAllSite = this.initData["allSites"];
    this.selectedUDF = this.initData["customFieldValueId"];
    if (this.initData["allSites"] == 1) {
      this.changeSiteStatus(true);
    } else {
      this.changeSiteStatus(false);
    }
  }

  onTabChange(data) {
    if (data.index == 0) {
      this.breadcrumbService.setItems([
        {
          label: "LBLSITEMGMT",
          url: "./assets/help/edit-user-defined-fields.md",
        },
        { label: "LBLUSERDEFINEDLISTING", routerLink: ["/user-defind-field"] },
      ]);
    } else {
      this.breadcrumbService.setItems([
        {
          label: "LBLSITEMGMT",
          url: "./assets/help/edit-user-defined-fields-assignments.md",
        },
        { label: "LBLUSERDEFINEDLISTING", routerLink: ["/user-defind-field"] },
      ]);
    }
  }
}
