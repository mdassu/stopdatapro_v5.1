import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";

import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import { CloneVisitor } from "@angular/compiler/src/i18n/i18n_ast";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";
import { EnvService } from "src/env.service";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { SubAreaService } from "src/app/_services/sub-area.service";
import { TranslateService } from "@ngx-translate/core";

declare var $: any;

@Component({
  selector: "app-sub-area-listing",
  templateUrl: "./sub-area-listing.component.html",
  styleUrls: ["./sub-area-listing.component.css"],
  providers: [ConfirmationService, MessageService],
})
export class SubAreaListingComponent implements OnInit {
  //  Variable Declaration
  filterForm: FormGroup;
  cities1: SelectItem[];
  cols: any[];
  cities2: SelectItem[];
  colsCount: any;
  cars1: any;
  userlevel: any;

  allSubArea: any;
  subAreaListing: any;
  dataCols: any[];
  dataCount: any;
  selectedfilterSubArea: string = "SUBAREANAME";
  searchBy: SelectItem[];
  selectedData: any = [];

  visibleTable: boolean = true;
  searchText: any;
  isLoaded: boolean;
  appName: any;
  roleId: any;
  // Breadcrumb Service
  constructor(
    private breadcrumbService: BreadcrumbService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private subareaservice: SubAreaService,
    private route: Router,
    private env: EnvService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/sub-area-listing.md" },
      { label: "LBLSUBAREASLISTING", routerLink: ["/subarea-listing"] },
    ]);
  }

  ngOnInit() {
    this.roleId = this.auth.UserInfo["roleId"];
    this.getSubArea();

    this.translate
      .get(["LBLSUBAREANAME", "LBLSTATUS"])
      .subscribe((res: Object) => {
        // Table header
        this.dataCols = [
          { field: "SUBAREANAME", header: res["LBLSUBAREANAME"] },
          { field: "STATUS", header: res["LBLSTATUS"] },
        ];

        // col of status
        this.dataCount = this.dataCols.length;

        // Search List
        this.searchBy = [];
        this.searchBy.push({
          label: res["LBLSUBAREANAME"],
          value: "SUBAREANAME",
        });
        this.cols = [
          { field: "firstname", header: res["LBLSUBAREANAME"] },
          { field: "status", header: res["LBLSTATUS"] },
        ];
      });

    //  col of status
    this.colsCount = this.cols.length;
    // User list
    this.cities1 = [];
    this.cities1.push({ label: "", value: "" });
    this.cities1.push({ label: "Demo", value: { id: 1, name: "Demo" } });
    this.cities1.push({ label: "Demo", value: { id: 1, name: "Demo" } });
    this.cities1.push({ label: "Demo", value: { id: 1, name: "Demo" } });
    this.appName = this.env.appName;
  }

  getSubArea() {
    this.subareaservice.getSubAreaData({}).subscribe((res) => {
      if (res["status"] == true) {
        this.allSubArea = res["Data"];
        this.subAreaListing = res["Data"];
        this.visibleTable = true;
      } else {
        this.allSubArea = [];
        this.subAreaListing = [];
        this.visibleTable = true;
      }
      this.isLoaded = true;
    });
  }

  // searching data
  searching() {
    var field = this.selectedfilterSubArea;
    var stext = this.searchText.toLowerCase();
    var fn = this;
    if (stext != "") {
      var newList = this.allSubArea.filter((item) => {
        return item[field].toLowerCase().indexOf(stext) >= 0;
      });
      this.subAreaListing = newList;
      setTimeout(function () {}, 10);
    } else {
      setTimeout(function () {}, 10);
      this.subAreaListing = this.allSubArea;
    }
  }

  // Delete Record
  deleteRecord() {
    this.translate.get("ALTDELCONFIRMSUBAREAS").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var subAreaIds = [];
          $.each(this.selectedData, function (key, SubAreaId) {
            subAreaIds.push(SubAreaId["SUBAREAID"]);
          });
          var subAreaId = subAreaIds.join();
          this.subareaservice
            .deleteSubArea({ subAreaId: subAreaId })
            .subscribe((res) => {
              this.selectedData = [];
              this.getSubArea();
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedData = [];
          this.getSubArea();
          this.searchText = "";
        },
      });
    });
  }

  // Change Status
  changeStatus(id) {
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var subAreaIds = [];
          var subAreaId;
          if (id == "") {
            $.each(this.selectedData, function (key, SubAreaId) {
              subAreaIds.push(SubAreaId["SUBAREAID"]);
            });
            subAreaId = subAreaIds.join();
          } else {
            subAreaId = id;
          }

          this.subareaservice
            .updateSubAreaStatus({ subAreaId: subAreaId })
            .subscribe((res) => {
              this.selectedData = [];
              this.getSubArea();
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedData = [];
          this.getSubArea();
          this.searchText = "";
        },
      });
    });
  }
}
