import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { UserDefinedFieldService } from "src/app/_services/user-defined-field.service";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
declare var $: any;

@Component({
  selector: "app-user-defined-field-listing",
  templateUrl: "./user-defined-field-listing.component.html",
  styleUrls: ["./user-defined-field-listing.component.css"],
  providers: [ConfirmationService, MessageService],
})
export class UserDefinedFieldListingComponent implements OnInit {
  userDefineListing: any = [];
  alluserDefine: any;
  dataCols: any[] = [];
  dataCount: any;
  visibleTable: boolean = true;

  selectedfilterUserDefind: string = "CUSTOMFIELDNAME";
  searchBy: SelectItem[];
  selectedData: any = [];
  searchText: any;
  isLoaded: boolean;
  appName: any;
  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private userdefinedfieldservice: UserDefinedFieldService,
    private route: Router,
    private env: EnvService,
    private translate: TranslateService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/user-defined-fields.md" },
      {
        label: "LBLUSERDEFINEDFEILDS",
        routerLink: ["/user-defind-field"],
      },
    ]);
  }

  ngOnInit() {
    this.getUserDefinedFields();

    this.translate.get("LBLFIELDNAME").subscribe((res: string) => {
      // Table header
      this.dataCols = [{ field: "CUSTOMFIELDNAME", header: res }];

      // col of status
      this.dataCount = this.dataCols.length;

      // Search List
      this.searchBy = [];
      this.searchBy.push({ label: res, value: "CUSTOMFIELDNAME" });
    });
    this.appName = this.env.appName;
  }

  getUserDefinedFields() {
    this.userdefinedfieldservice.getUserDefinedData({}).subscribe((res) => {
      if (res["status"] == true) {
        this.alluserDefine = res["Data"];
        this.userDefineListing = res["Data"];
      } else {
        this.alluserDefine = [];
        this.userDefineListing = [];
      }
      this.isLoaded = true;
    });
  }

  // searching data
  searching() {
    var field = this.selectedfilterUserDefind;
    var stext = this.searchText.toLowerCase();
    var fn = this;
    if (stext != "") {
      var newList = this.alluserDefine.filter((item) => {
        return item[field].toLowerCase().indexOf(stext) >= 0;
      });
      this.userDefineListing = newList;
      setTimeout(function () {}, 10);
    } else {
      setTimeout(function () {}, 10);
      this.userDefineListing = this.alluserDefine;
    }
  }
}
