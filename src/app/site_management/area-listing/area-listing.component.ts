import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import { CloneVisitor } from "@angular/compiler/src/i18n/i18n_ast";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { AreaService } from "src/app/_services/area.service";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
declare var $: any;

@Component({
  selector: "app-area-listing",
  templateUrl: "./area-listing.component.html",
  styleUrls: ["./area-listing.component.css"],
  providers: [ConfirmationService, MessageService],
})
export class AreaListingComponent implements OnInit {
  //  Variable Declaration
  filterForm: FormGroup;
  cities1: SelectItem[];
  cols: any[];
  cities2: SelectItem[];
  colsCount: any;
  cars1: any;
  userlevel: any;

  allArea: any;
  areaListing: any;
  dataCols: any[];
  dataCount: any;
  selectedfilterArea: string = "AREANAME";
  searchBy: SelectItem[];
  selectedData: any = [];

  visibleTable: boolean = true;
  searchText: any;
  searchByArea: SelectItem[];
  selectedArea: number = 1;
  isLoaded: boolean;
  appName: any;
  confirmClass: any;
  roleId: any;
  // Breadcrumb Service
  constructor(
    private breadcrumbService: BreadcrumbService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private areaservice: AreaService,
    private route: Router,
    private env: EnvService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/areas.md" },
      { label: "LBLAREALISTING", routerLink: ["/area-listing"] },
    ]);
  }

  ngOnInit() {
    this.roleId = this.auth.UserInfo["roleId"];
    //Get Area
    this.getArea();

    this.translate
      .get(["LBLAREANAME", "LBLSTATUS", "LBLASSIGNEDAREAS", "LBLALLAREAS"])
      .subscribe((res: Object) => {
        // Table header
        this.dataCols = [
          { field: "AREANAME", header: res["LBLAREANAME"] },
          { field: "STATUS", header: res["LBLSTATUS"] },
        ];

        // col of status
        this.dataCount = this.dataCols.length;

        // Search List
        this.searchByArea = [];
        this.searchByArea.push({ label: res["LBLASSIGNEDAREAS"], value: 1 });
        this.searchByArea.push({ label: res["LBLALLAREAS"], value: 0 });

        // Search List
        this.searchBy = [];
        this.searchBy.push({ label: res["LBLAREANAME"], value: "AREANAME" });
      });
    this.appName = this.env.appName;
  }

  getArea() {
    this.areaservice.getAreaData({}).subscribe((res) => {
      if (res["status"] == true) {
        this.allArea = res["Data"];
        this.areaListing = res["Data"];
        this.visibleTable = false;
      } else {
        this.allArea = [];
        this.areaListing = [];
        this.visibleTable = false;
      }
      this.isLoaded = true;
    });
  }

  // searching data
  searching() {
    var field = this.selectedfilterArea;
    var stext = this.searchText.toLowerCase();
    var fn = this;
    if (stext != "") {
      var newList = this.allArea.filter((item) => {
        return item[field].toLowerCase().indexOf(stext) >= 0;
      });
      this.areaListing = newList;
      setTimeout(function () {}, 10);
    } else {
      setTimeout(function () {}, 10);
      this.areaListing = this.allArea;
    }
  }

  deleteRecord() {
    this.confirmClass = "warning-msg";
    this.translate.get("ALTDELCONFIRMAREAS").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var areaIds = [];
          $.each(this.selectedData, function (key, AreaId) {
            areaIds.push(AreaId["AREAID"]);
          });
          var areaId = areaIds.join();
          this.areaservice.deleteArea({ areaId: areaId }).subscribe((res) => {
            this.selectedData = [];
            this.getArea();
            this.searchText = "";
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          });
        },
        reject: () => {
          this.selectedData = [];
          this.searchText = "";
          this.getArea();
        },
      });
    });
  }

  changeStatus(id) {
    this.confirmClass = "info-msg";
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var areaIds = [];
          var areaId;
          if (id == "") {
            $.each(this.selectedData, function (key, AreaId) {
              areaIds.push(AreaId["AREAID"]);
            });
            areaId = areaIds.join();
          } else {
            areaId = id;
          }

          this.areaservice
            .updateAreaStatus({ areaId: areaId })
            .subscribe((res) => {
              this.selectedData = [];
              this.getArea();
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedData = [];
          this.getArea();
          this.searchText = "";
        },
      });
    });
  }
}
