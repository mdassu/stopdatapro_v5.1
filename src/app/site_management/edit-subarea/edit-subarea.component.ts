import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { ColorPickerModule } from "primeng/colorpicker";
import { Router, ActivatedRoute } from "@angular/router";

import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { SubAreaService } from "src/app/_services/sub-area.service";

import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;

@Component({
  selector: "app-edit-subarea",
  templateUrl: "./edit-subarea.component.html",
  styleUrls: ["./edit-subarea.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class EditSubareaComponent implements OnInit {
  addSubArea: FormGroup;
  submitted: boolean;
  isLoading: boolean = true;
  subAreaId: any;
  status: SelectItem[];
  areaListing: any[];
  subAreaLoading: boolean = false;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private subareaservice: SubAreaService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private activetedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/edit-sub-area.md" },
      { label: "LBLSUBAREAS", routerLink: ["/subarea-listing"] },
    ]);
  }

  ngOnInit() {
    this.subAreaId = this.activetedRoute.snapshot.paramMap.get("subareaid");
    var data = {
      subAreaName: "",
      areaIds: null,
      status: 1,
    };
    this.createFormBuilder(data);
    this.getAddSubArea();

    this.status = [];
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: Object) => {
        this.status.push({ label: res["LBLACTIVE"], value: 1 });
        this.status.push({ label: res["LBLINACTIVE"], value: 0 });
      });
  }

  getAddSubArea() {
    var parameters = {
      subAreaId: this.subAreaId,
    };
    this.subareaservice.getSubAreaData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.areaListing = [];
        var areaList = res["available"];
        areaList.map((data) => {
          this.areaListing.push({
            label: data["AREANAME"],
            value: data["AREAID"],
          });
        });
        var subAreaData = res["Data"];
        var assignArea;
        if (res["assign"] != "") {
          assignArea = res["assign"].split(",");
        } else {
          assignArea = "";
        }

        var data = {
          subAreaName: subAreaData.SUBAREANAME,
          areaIds: assignArea,
          status: subAreaData.STATUS,
        };
        this.createFormBuilder(data);
      } else {
        this.areaListing = [];
      }
      this.isLoading = false;
    });
  }

  createFormBuilder(data) {
    this.addSubArea = this.fb.group({
      subAreaName: new FormControl(
        data.subAreaName,
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.noWhitespaceValidator,
        ])
      ),
      areaIds: new FormControl(data.areaIds),
      status: new FormControl(data.status, Validators.required),
    });
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  get uform() {
    return this.addSubArea.controls;
  }
  onSubmit(bType) {
    this.submitted = true;
    if (this.addSubArea.invalid) {
      this.customValidatorsService.scrollToError();
      // let target;
      // target = this.elementRef.nativeElement.querySelector(".ng-invalid");
      // if (target) {
      //   $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
      //   target.focus();
      // }
    } else {
      this.subAreaLoading = true;
      this.addSubArea.value["subAreaId"] = this.subAreaId;
      if (
        this.addSubArea.value["areaIds"] != null &&
        this.addSubArea.value["areaIds"] != ""
      ) {
        this.addSubArea.value["areaIds"] = this.addSubArea.value[
          "areaIds"
        ].join();
      } else {
        this.addSubArea.value["areaIds"] = "";
      }
      this.subareaservice
        .updateSubArea(this.addSubArea.value)
        .subscribe((res) => {
          if (res["status"] == true) {
            if (bType) {
              this.cookieService.set("edit-subarea", "yes");
              this.router.navigate(["./add-sub-area"], {
                skipLocationChange: true,
              });
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            } else {
              this.submitted = false;
              this.addSubArea.reset();
              this.getAddSubArea();
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            }
            this.subAreaLoading = false;
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
            this.subAreaLoading = true;
          }
        });
    }
  }

  subAreaReset() {
    this.getAddSubArea();
  }
  onSelectAll() {
    this.addSubArea.markAsDirty();
    const selected = this.areaListing.map((item) => item.AREAID);
    this.addSubArea.get("areaIds").patchValue(selected);
  }

  onClearAll() {
    this.addSubArea.markAsDirty();
    this.addSubArea.get("areaIds").patchValue([]);
  }
}
