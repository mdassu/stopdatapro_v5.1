import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { Router } from "@angular/router";

import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { SubAreaService } from "src/app/_services/sub-area.service";

import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;

@Component({
  templateUrl: "./add-subarea.component.html",
  styleUrls: ["./add-subarea.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class AddSubareaComponent implements OnInit {
  addSubArea: FormGroup;
  submitted: boolean;
  initialGeneralFormData: any;
  isLoading: boolean = true;
  status: SelectItem[];
  areaListing: any[];

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private subareaservice: SubAreaService,
    private router: Router,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/add-sub-area.md" },
      { label: "LBLSUBAREAS", routerLink: ["/subarea-listing"] },
    ]);
  }

  ngOnInit() {
    this.addSubArea = this.fb.group({
      subAreaName: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.noWhitespaceValidator,
        ])
      ),
      areaIds: new FormControl(null),
      status: new FormControl(1, Validators.required),
    });

    this.getAddSubAreaData();
    this.status = [];
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: Object) => {
        this.status.push({ label: res["LBLACTIVE"], value: 1 });
        this.status.push({ label: res["LBLINACTIVE"], value: 0 });
      });
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  getAddSubAreaData() {
    this.subareaservice.getAddSubAreaData().subscribe((res) => {
      if (res["status"] == true) {
        // this.areaListing = res["available"];
        this.areaListing = [];
        var areaList = res["available"];
        areaList.map((data) => {
          this.areaListing.push({
            label: data["AREANAME"],
            value: data["AREAID"],
          });
        });
      } else {
        this.areaListing = [];
      }
      this.initialGeneralFormData = this.addSubArea.getRawValue();
      this.isLoading = false;
    });
  }

  get uform() {
    return this.addSubArea.controls;
  }
  onSubmit(bType) {
    this.submitted = true;
    if (this.addSubArea.invalid) {
      this.customValidatorsService.scrollToError();
      // let target;
      // target = this.elementRef.nativeElement.querySelector(".ng-invalid");
      // if (target) {
      //   $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
      //   target.focus();
      // }
    } else {
      if (this.addSubArea.value["areaIds"] != null) {
        this.addSubArea.value["areaIds"] = this.addSubArea.value[
          "areaIds"
        ].join();
      }
      this.subareaservice
        .insertSubArea(this.addSubArea.value)
        .subscribe((res) => {
          if (res["status"] == true) {
            if (bType) {
              this.submitted = false;
              this.addSubArea.reset();
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
              this.onReset();
              this.addSubArea.markAsPristine();
            } else {
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
              setTimeout(() => {
                this.router.navigate(["./edit-subarea/" + res["id"]], {
                  skipLocationChange: true,
                });
              }, 1000);
            }
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
          }
        });
    }
  }
  onSelectAll() {
    this.addSubArea.markAsDirty();
    const selected = this.areaListing.map((item) => item.AREAID);
    this.addSubArea.get("areaIds").patchValue(selected);
  }

  onClearAll() {
    this.addSubArea.markAsDirty();
    this.addSubArea.get("areaIds").patchValue([]);
  }
  onReset() {
    this.addSubArea.patchValue(this.initialGeneralFormData);
  }
}
