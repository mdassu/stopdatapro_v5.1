import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { ColorPickerModule } from "primeng/colorpicker";
import { Router, ActivatedRoute } from "@angular/router";

import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { ShiftService } from "src/app/_services/shift.service";

import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;

@Component({
  selector: "app-edit-shift",
  templateUrl: "./edit-shift.component.html",
  styleUrls: ["./edit-shift.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class EditShiftComponent implements OnInit {
  AddShift: FormGroup;
  submitted: boolean;
  shiftId: any;
  isLoading: boolean = true;
  siteListing: any[];
  status: SelectItem[];
  initialGeneralFormData: any;
  shiftLoading: boolean = false;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private shiftservice: ShiftService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private activetedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/edit-shifts.md" },
      { label: "LBLSHIFT", routerLink: ["/shifts-listing"] },
    ]);
  }

  ngOnInit() {
    this.shiftId = this.activetedRoute.snapshot.paramMap.get("shiftid");

    var data = {
      shiftName: "",
      siteIds: null,
      status: 1,
    };
    this.createFormBuilder(data);
    this.getShift();

    this.status = [];
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: Object) => {
        this.status.push({ label: res["LBLACTIVE"], value: 1 });
        this.status.push({ label: res["LBLINACTIVE"], value: 0 });
      });
  }

  getShift() {
    var parameters = {
      shiftId: this.shiftId,
    };
    this.shiftservice.getShiftData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        // this.siteListing = res["available"];
        this.siteListing = [];
        var siteList = res["available"];
        siteList.map((data) => {
          this.siteListing.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });
        var shiftData = res["Data"];
        var assignShift;
        if (res["assign"] != "") {
          assignShift = res["assign"].split(",");
        } else {
          assignShift = "";
        }
        var data = {
          shiftName: shiftData.SHIFTNAME,
          siteIds: assignShift,
          status: shiftData.STATUS,
        };
        this.createFormBuilder(data);
        this.initialGeneralFormData = this.AddShift.getRawValue();
      } else {
        this.siteListing = [];
      }
      this.isLoading = false;
    });
  }

  createFormBuilder(data) {
    this.AddShift = this.fb.group({
      shiftName: new FormControl(
        data.shiftName,
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.noWhitespaceValidator,
        ])
      ),
      siteIds: new FormControl(data.siteIds),
      status: new FormControl(data.status, Validators.required),
    });
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  get uform() {
    return this.AddShift.controls;
  }
  onSubmit(bType) {
    this.submitted = true;
    if (this.AddShift.invalid) {
      this.customValidatorsService.scrollToError();
      // let target;
      // target = this.elementRef.nativeElement.querySelector(".ng-invalid");
      // if (target) {
      //   $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
      //   target.focus();
      // }
    } else {
      this.shiftLoading = true;
      this.AddShift.value["shiftId"] = this.shiftId;
      if (
        this.AddShift.value["siteIds"] != null &&
        this.AddShift.value["siteIds"] != ""
      ) {
        this.AddShift.value["siteIds"] = this.AddShift.value["siteIds"].join();
      } else {
        this.AddShift.value["siteIds"] = "";
      }

      this.shiftservice.updateShift(this.AddShift.value).subscribe((res) => {
        if (res["status"] == true) {
          if (bType) {
            this.cookieService.set("edit-shift", "yes");
            this.router.navigate(["./add-shift"], {
              skipLocationChange: true,
            });
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          } else {
            this.submitted = false;
            this.AddShift.reset();
            this.getShift();
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          }
          this.shiftLoading = false;
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
          this.shiftLoading = false;
        }
      });
    }
  }

  shiftReset() {
    this.AddShift.patchValue(this.initialGeneralFormData);
  }
  onSelectAll() {
    this.AddShift.markAsDirty();
    const selected = this.siteListing.map((item) => item.SITEID);
    this.AddShift.get("siteIds").patchValue(selected);
  }

  onClearAll() {
    this.AddShift.markAsDirty();
    this.AddShift.get("siteIds").patchValue([]);
  }
}
