import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { Router } from "@angular/router";

import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { ShiftService } from "src/app/_services/shift.service";

import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;
@Component({
  selector: "app-add-shift",
  templateUrl: "./add-shift.component.html",
  styleUrls: ["./add-shift.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class AddShiftComponent implements OnInit {
  AddShift: FormGroup;
  submitted: boolean;
  siteListing: any[];
  isLoading: boolean = true;
  //  Declare name of all fields
  status: SelectItem[];
  initialGeneralFormData: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private shiftservice: ShiftService,
    private router: Router,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/add-shifts.md" },
      { label: "LBLSHIFT", routerLink: ["/shifts-listing"] },
    ]);
  }

  ngOnInit() {
    this.AddShift = this.fb.group({
      shiftName: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.noWhitespaceValidator,
        ])
      ),
      siteIds: new FormControl(null),
      status: new FormControl(1, Validators.required),
    });

    this.getAddShiftData();

    this.status = [];
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: Object) => {
        this.status.push({ label: res["LBLACTIVE"], value: 1 });
        this.status.push({ label: res["LBLINACTIVE"], value: 0 });
      });
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  getAddShiftData() {
    this.shiftservice.getAddShiftData().subscribe((res) => {
      if (res["status"] == true) {
        // this.siteListing = res["available"];
        this.siteListing = [];
        var siteList = res["available"];
        siteList.map((data) => {
          this.siteListing.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });
        if (res["defaultSite"] != "") {
          var defaultSite = res["defaultSite"].toString().split(",");
          this.AddShift.controls["siteIds"].setValue(defaultSite);
        }
      } else {
        this.siteListing = [];
      }
      this.initialGeneralFormData = this.AddShift.getRawValue();
      this.isLoading = false;
    });
  }

  get uform() {
    return this.AddShift.controls;
  }
  onSubmit(bType) {
    this.submitted = true;
    if (this.AddShift.invalid) {
      this.customValidatorsService.scrollToError();
      let target;
      target = this.elementRef.nativeElement.querySelector(".ng-invalid");
      if (target) {
        $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
        target.focus();
      }
    } else {
      if (this.AddShift.value["siteIds"] != null) {
        this.AddShift.value["siteIds"] = this.AddShift.value["siteIds"].join();
      }
      this.shiftservice.insertShift(this.AddShift.value).subscribe((res) => {
        if (res["status"] == true) {
          if (bType) {
            this.submitted = false;
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.resetShiftForm();
            this.AddShift.markAsPristine();
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            setTimeout(() => {
              this.router.navigate(["./edit-shift/" + res["id"]], {
                skipLocationChange: true,
              });
            }, 1000);
            // this.cookieService.set("add-new-shift", "yes");
            // this.router.navigate(["./edit-shift/" + res["id"]], {
            //   skipLocationChange: true
            // });
          }
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
        }
      });
    }
  }

  resetShiftForm() {
    this.AddShift.patchValue(this.initialGeneralFormData);
  }
  onSelectAll() {
    this.AddShift.markAsDirty();
    const selected = this.siteListing.map((item) => item.SITEID);
    this.AddShift.get("siteIds").patchValue(selected);
  }

  onClearAll() {
    this.AddShift.markAsDirty();
    this.AddShift.get("siteIds").patchValue([]);
  }
}
