import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";

import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import { CloneVisitor } from "@angular/compiler/src/i18n/i18n_ast";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { AuthenticationService } from "src/app/_services/authentication.service";
import { SitesService } from "src/app/_services/sites.service";
import { TranslateService } from "@ngx-translate/core";

import tinymce from "tinymce/tinymce";

// A theme is also required
import "tinymce/themes/modern/theme";

// Any plugins you want to use has to be imported
import "tinymce/plugins/paste";
import "tinymce/plugins/link";
import "tinymce/plugins/table";
import { EnvService } from "src/env.service";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;

@Component({
  selector: "app-sites-listing",
  templateUrl: "./sites-listing.component.html",
  styleUrls: ["./sites-listing.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class SitesListingComponent implements OnInit {
  //  Variable Declaration
  filterForm: FormGroup;
  sendEmailForm: FormGroup;
  cities1: SelectItem[];
  cols: any[];
  cities2: SelectItem[];
  colsCount: any;
  cars1: any;
  userlevel: any;
  appName: any;
  allSites: any;
  siteListing: any;
  dataCols: any[] = [];
  dataCount: any;
  selectedfilterSite: string = "SITENAME";
  searchBy: SelectItem[];
  selectedData: any = [];

  visibleTable: boolean = true;
  searchText: any;
  isLoaded: boolean;
  display: boolean;
  menuList = [];
  Keyword = "keywords";
  keywords: any;
  selectedList = "";
  sendEmailsubmitted: boolean;
  confirmClass: any;
  roleId: any;
  // Breadcrumb Service
  constructor(
    private breadcrumbService: BreadcrumbService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private sitesservice: SitesService,
    private route: Router,
    private fb: FormBuilder,
    private env: EnvService,
    private translate: TranslateService,
    private auth: AuthenticationService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/site-listing.md" },
      { label: "LBLSITELISTING" },
    ]);

    this.translate.get("LBLKEYWORDS").subscribe((res) => {
      this.Keyword = res;
    });
  }

  ngOnInit() {
    //Get Sites
    this.roleId = this.auth.UserInfo["roleId"];
    this.getSites({});

    this.translate
      .get(["LBLSITENAME", "LBLCOUNTRY", "LBLCITY", "LBLSTATUS"])
      .subscribe((res: Object) => {
        //Header
        this.dataCols = [
          { field: "SITENAME", header: res["LBLSITENAME"] },
          { field: "COUNTRY", header: res["LBLCOUNTRY"] },
          { field: "CITY", header: res["LBLCITY"] },
          { field: "STATUS", header: res["LBLSTATUS"] },
        ];

        // col of status
        this.dataCount = this.dataCols.length;

        // Search List
        this.searchBy = [];
        this.searchBy.push({ label: res["LBLSITENAME"], value: "SITENAME" });
        this.searchBy.push({ label: res["LBLCOUNTRY"], value: "COUNTRY" });
        this.searchBy.push({ label: res["LBLCITY"], value: "CITY" });
      });

    this.sendEmailForm = this.fb.group({
      subject: new FormControl("", [Validators.required]),
      body: new FormControl("", [Validators.required]),
    });

    this.sitesservice.getKeyWord({}).subscribe((res) => {
      this.keywords = JSON.parse(res["Data"][0]["KEYWORD"]);

      this.keywords.map((item) => {
        this.translate.get(item.label).subscribe((res) => {
          item["label"] = res;
          let tempItem = {};
          tempItem["text"] = item["label"];
          tempItem[
            "value"
          ] = `&nbsp;<span id=${item["keyWordId"]} class="mceNonEditable" contenteditable="false"><strong>[${item["label"]}]</strong></span>&nbsp;`;
          this.menuList.push(tempItem);
        });
      });
    });
    this.appName = this.env.appName;
  }

  getSites(parameters) {
    this.sitesservice.getSiteData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.allSites = res["Data"];
        this.siteListing = res["Data"];
      } else {
        this.allSites = [];
        this.siteListing = [];
      }
      this.isLoaded = true;
    });
  }

  // searching data
  searching() {
    var field = this.selectedfilterSite;
    var stext = this.searchText.toLowerCase();
    var fn = this;
    if (stext != "") {
      var newList = this.allSites.filter((item) => {
        return item[field].toLowerCase().indexOf(stext) >= 0;
      });
      this.siteListing = newList;
      setTimeout(function () {}, 10);
    } else {
      setTimeout(function () {}, 10);
      this.siteListing = this.allSites;
    }
  }

  // Delete Record
  deleteRecord() {
    this.confirmClass = "warning-msg";
    this.translate.get("ALTDELCONFIRMSITES").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var siteIds = [];
          $.each(this.selectedData, function (key, SiteId) {
            siteIds.push(SiteId["SITEID"]);
          });
          var siteId = siteIds.join();
          this.sitesservice.deleteSites({ siteId: siteId }).subscribe((res) => {
            this.selectedData = [];
            this.getSites({});
            this.searchText = "";
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          });
        },
        reject: () => {
          this.selectedData = [];
          this.getSites({});
          this.searchText = "";
        },
      });
    });
  }

  // Change Status
  changeStatus(id) {
    this.confirmClass = "info-msg";
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var siteIds = [];
          var siteId;
          if (id == "") {
            $.each(this.selectedData, function (key, SiteId) {
              siteIds.push(SiteId["SITEID"]);
            });
            siteId = siteIds.join();
          } else {
            siteId = id;
          }
          this.sitesservice
            .updateSiteStatus({ siteId: siteId })
            .subscribe((res) => {
              this.selectedData = [];
              this.getSites({});
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedData = [];
          this.getSites({});
          this.searchText = "";
        },
      });
    });
  }

  get sendMailform() {
    return this.sendEmailForm.controls;
  }

  sendEmail() {
    this.selectedList = this.selectedData
      .map((item) => item.SITENAME)
      .join("  ,");

    this.translate.get("ALTSENDMAIL").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          this.display = true;
          setTimeout(() => {
            var that = this;
            tinymce.remove();
            var languageTinymce = "";
            let currentUser = this.auth.UserInfo;
            if (currentUser.languageCode == "da-dk") {
              var languageTinymce = "da";
            } else if (currentUser.languageCode == "de-de") {
              var languageTinymce = "de";
            } else if (currentUser.languageCode == "el") {
              var languageTinymce = "el";
            } else if (currentUser.languageCode == "en-us") {
              var languageTinymce = "en";
            } else if (currentUser.languageCode == "es-es") {
              var languageTinymce = "es";
            } else if (currentUser.languageCode == "fr-fr") {
              var languageTinymce = "fr-FR";
            } else if (currentUser.languageCode == "hu") {
              var languageTinymce = "hu_HU";
            } else if (currentUser.languageCode == "it-it") {
              var languageTinymce = "it";
            } else if (currentUser.languageCode == "ja-jp") {
              var languageTinymce = "ja";
            } else if (currentUser.languageCode == "ko") {
              var languageTinymce = "ko_KR";
            } else if (currentUser.languageCode == "nl-nl") {
              var languageTinymce = "nl";
            } else if (currentUser.languageCode == "pl-pl") {
              var languageTinymce = "pl";
            } else if (currentUser.languageCode == "pt-pt") {
              var languageTinymce = "pt_PT";
            } else if (currentUser.languageCode == "ru-ru") {
              var languageTinymce = "ru";
            } else if (currentUser.languageCode == "th-th") {
              var languageTinymce = "th_TH";
            } else if (currentUser.languageCode == "zh-cn") {
              var languageTinymce = "zh_CN";
            }
            tinymce.init({
              selector: "#tinymceEditor",
              base_url: "/tinymce",
              language: languageTinymce,
              height: "500",
              suffix: ".min",
              skin_url: "tinymce/skins/lightgray",
              plugins: "table",
              toolbar: "table fontselect fontsizeselect mybutton",
              // setup: function (editor) {
              //   editor.addButton("mybutton", {
              //     type: "listbox",
              //     text: that.Keyword,
              //     icon: false,
              //     onselect: function (e) {
              //       editor.insertContent(this.value());
              //     },
              //     values: this.menuList,
              //   });
              //   editor.on("change", function (e) {
              //     that.sendEmailForm.patchValue({
              //       body: editor.getContent(),
              //     });
              //     that.sendEmailForm.get("body").markAsDirty();
              //   });
              // },
            });
          }, 1000);
        },
        reject: () => {
          this.selectedData = [];
        },
      });
    });
  }

  onSendMail() {
    this.sendEmailsubmitted = true;
    if (this.sendEmailForm.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      const body = { ...this.sendEmailForm.value };
      body["siteId"] = this.selectedData
        .map((item) => item.SITEID)
        .map(Number)
        .join();
      this.sitesservice.sendMailForSite(body).subscribe((res) => {
        if (res["status"]) {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.sendEmailForm.reset();
            var editorCache = tinymce.get("tinymceEditor"); //get from cache
            if (editorCache) {
              editorCache.setContent("");
              editorCache.destroy();
            }
            this.messageService.add({
              severity: "success",
              detail: res,
            });
          });
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.sendEmailForm.reset();
            var editorCache = tinymce.get("tinymceEditor"); //get from cache
            if (editorCache) {
              editorCache.setContent("");
              editorCache.destroy();
            }
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
        }
        this.display = false;
        this.sendEmailsubmitted = false;
      });
    }
  }

  onDialogClose() {
    this.sendEmailsubmitted = false;
    this.sendEmailForm.reset();
    var editorCache = tinymce.get("tinymceEditor"); //get from cache
    if (editorCache) {
      editorCache.setContent("");
      editorCache.destroy();
    }
  }
}
