import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { ColorPickerModule } from "primeng/colorpicker";
import { Router } from "@angular/router";

import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { SitesService } from "src/app/_services/sites.service";

import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;
@Component({
  selector: "app-add-site",
  templateUrl: "./add-site.component.html",
  styleUrls: ["./add-site.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class AddSiteComponent implements OnInit {
  addSite: FormGroup;
  submitted: boolean;
  isLoading: boolean = true;
  selectedValues: string[] = [];
  countryListing: SelectItem[];
  status: SelectItem[];
  timeZoneListing: SelectItem[];

  initialGeneralFormData: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private sitesservice: SitesService,
    private router: Router,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/add-site.md" },
      { label: "LBLBSITES", routerLink: ["/site-listing"] },
    ]);
  }

  ngOnInit() {
    this.addSite = this.fb.group({
      siteName: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.noWhitespaceValidator,
        ])
      ),
      address: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(1024),
          this.noWhitespaceValidator,
        ])
      ),
      city: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(128),
          this.noWhitespaceValidator,
        ])
      ),
      state: new FormControl(""),
      zipCode: new FormControl(""),
      comments: new FormControl(""),
      status: new FormControl(1, Validators.required),
      employees: new FormControl(""),
      countryId: new FormControl("", Validators.required),
      defaultTimeZone: new FormControl("", Validators.required),
    });
    this.getAddSite();

    this.status = [];
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: Object) => {
        this.status.push({ label: res["LBLACTIVE"], value: 1 });
        this.status.push({ label: res["LBLINACTIVE"], value: 0 });
      });
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  /*
   * function name: getAddSite
   * To get add site data
   * @By Jitendra on 26 Jun 2019
   */
  getAddSite() {
    this.sitesservice.getAddSiteData().subscribe((res) => {
      if (res["status"] == true) {
        this.countryListing = res["countries"];
        this.timeZoneListing = res["timeZone"];
        this.timeZoneListing.map((i) => {
          this.translate.get(i["TIMEZONENAME"]).subscribe((label) => {
            i["TIMEZONENAME"] = label;
          });
        });
        this.addSite.controls["countryId"].setValue(
          this.countryListing[0]["COUNTRYID"]
        );
        this.addSite.controls["defaultTimeZone"].setValue(
          parseInt(res["globaltimeZone"])
        );
        this.initialGeneralFormData = this.addSite.getRawValue();
      }
      this.isLoading = false;
    });
  }

  get innerForm() {
    return this.addSite.controls;
  }

  onSubmit(bType) {
    this.submitted = true;
    if (this.addSite.invalid) {
      this.customValidatorsService.scrollToError();
      let target;
      target = this.elementRef.nativeElement.querySelector(".ng-invalid");
      if (target) {
        $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
        target.focus();
      }
    } else {
      this.sitesservice.insertSite(this.addSite.value).subscribe((res) => {
        if (res["status"] == true) {
          if (bType) {
            this.submitted = false;
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.resetSiteForm();
            this.addSite.markAsPristine();
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            setTimeout(() => {
              this.router.navigate(["./edit-site/" + res["id"]], {
                skipLocationChange: true,
              });
            }, 1000);
          }
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
        }
      });
    }
  }

  resetSiteForm() {
    this.addSite.patchValue(this.initialGeneralFormData);
  }
}
