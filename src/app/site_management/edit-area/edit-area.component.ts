import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { ColorPickerModule } from "primeng/colorpicker";
import { Router, ActivatedRoute } from "@angular/router";

import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { AreaService } from "src/app/_services/area.service";

import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;

@Component({
  selector: "app-edit-area",
  templateUrl: "./edit-area.component.html",
  styleUrls: ["./edit-area.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class EditAreaComponent implements OnInit {
  AddAreas: FormGroup;
  isLoading: boolean = true;
  submitted: boolean;
  areaId: any;
  siteListing: any[];
  subAreaListing: any[];
  status: SelectItem[];
  initialGeneralFormData: any;
  areaLoading: boolean = false;
  subAreaOptionValue: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private areaservice: AreaService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private activetedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private auth: AuthenticationService,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSITEMGMT", url: "./assets/help/edit-area.md" },
      { label: "LBLAREA", routerLink: ["/area-listing"] },
    ]);
  }

  ngOnInit() {
    this.subAreaOptionValue = this.auth.UserInfo["subAreaOptionValue"];
    this.areaId = this.activetedRoute.snapshot.paramMap.get("areaid");

    var data = {
      areaName: "",
      siteIds: null,
      subareaIds: null,
      status: 1,
    };

    this.createFormBuilder(data);
    this.getArea();

    this.status = [];
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: Object) => {
        this.status.push({ label: res["LBLACTIVE"], value: 1 });
        this.status.push({ label: res["LBLINACTIVE"], value: 0 });
      });
  }

  getArea() {
    var parameters = {
      areaId: this.areaId,
    };
    this.areaservice.getAreaData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var areaData = res["Data"];
        var siteAssign;
        var subAreaAssign;
        if (res["siteAssign"] != "") {
          siteAssign = res["siteAssign"].split(",");
        } else {
          siteAssign = "";
        }
        if (res["subAreaAssign"] != "") {
          subAreaAssign = res["subAreaAssign"].split(",");
        } else {
          subAreaAssign = "";
        }
        // this.siteListing = res["siteAvailable"];
        this.siteListing = [];
        var siteList = res["siteAvailable"];
        siteList.map((data) => {
          this.siteListing.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });
        // this.subAreaListing = res["subAreaAvailable"];
        this.subAreaListing = [];
        var subAreaList = res["subAreaAvailable"];
        subAreaList.map((data) => {
          this.subAreaListing.push({
            label: data["SUBAREANAME"],
            value: data["SUBAREAID"],
          });
        });

        var data = {
          areaName: areaData.AREANAME,
          siteIds: siteAssign,
          subareaIds: subAreaAssign,
          status: areaData.STATUS,
        };
        this.createFormBuilder(data);
        this.initialGeneralFormData = this.AddAreas.getRawValue();
      } else {
        this.siteListing = [];
        this.subAreaListing = [];
      }
      this.isLoading = false;
    });
  }

  createFormBuilder(data) {
    this.AddAreas = this.fb.group({
      areaName: new FormControl(
        data.areaName,
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.noWhitespaceValidator,
        ])
      ),
      siteIds: new FormControl(data.siteIds),
      subareaIds: new FormControl(data.subareaIds),
      status: new FormControl(data.status, Validators.required),
    });
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  get uform() {
    return this.AddAreas.controls;
  }
  onSubmit(bType) {
    this.submitted = true;
    if (this.AddAreas.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.areaLoading = true;
      this.AddAreas.value["areaId"] = this.areaId;
      if (
        this.AddAreas.value["siteIds"] != null &&
        this.AddAreas.value["siteIds"] != ""
      ) {
        this.AddAreas.value["siteIds"] = this.AddAreas.value["siteIds"].join();
      } else {
        this.AddAreas.value["siteIds"] = "";
      }
      if (
        this.AddAreas.value["subareaIds"] != null &&
        this.AddAreas.value["subareaIds"] != ""
      ) {
        this.AddAreas.value["subareaIds"] = this.AddAreas.value[
          "subareaIds"
        ].join();
      } else {
        this.AddAreas.value["subareaIds"] = "";
      }

      this.areaservice.updateArea(this.AddAreas.value).subscribe((res) => {
        if (res["status"] == true) {
          if (bType) {
            this.cookieService.set("edit-area", "yes");
            this.router.navigate(["./add-area"], {
              skipLocationChange: true,
            });
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          } else {
            this.submitted = false;
            this.AddAreas.reset();
            this.getArea();
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          }
          this.areaLoading = false;
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
          this.areaLoading = false;
        }
      });
    }
  }

  areaReset() {
    this.AddAreas.patchValue(this.initialGeneralFormData);
  }
  onSelectAll() {
    this.AddAreas.markAsDirty();
    const selectedSubArea = this.subAreaListing.map((item) => item.SUBAREAID);
    this.AddAreas.get("subareaIds").patchValue(selectedSubArea);
  }

  onClearAll() {
    this.AddAreas.markAsDirty();
    this.AddAreas.get("subareaIds").patchValue([]);
  }
  onSelectAllAssign() {
    this.AddAreas.markAsDirty();
    const selected = this.siteListing.map((item) => item.SITEID);
    this.AddAreas.get("siteIds").patchValue(selected);
  }

  onClearAllAssign() {
    this.AddAreas.markAsDirty();
    this.AddAreas.get("siteIds").patchValue([]);
  }
}
