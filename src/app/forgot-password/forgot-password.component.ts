import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { ForgetPasswordService } from "../_services/forget-password.service";
import { MessageService } from "primeng/api";
import { TranslateService } from "@ngx-translate/core";
import { GlobalDataService } from "../_services/global-data.service";
import { CustomValidatorsService } from "../_services/custom-validators.service";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  providers: [CustomValidatorsService],
})
export class ForgotPasswordComponent implements OnInit {
  forgetPasswordForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private forgetPasswordService: ForgetPasswordService,
    private messageService: MessageService,
    private translate: TranslateService,
    private globalService: GlobalDataService,
    private customValidatorsService: CustomValidatorsService
  ) {}
  ngOnInit() {
    this.globalService.getGlobalData({}).subscribe((res) => {
      this.translate.use(res["globalData"]["GLOBALLANGUAGE"]);
    });
    this.buildForm();
  }
  buildForm() {
    this.forgetPasswordForm = this.fb.group({
      userName: new FormControl("", [
        Validators.required,
        Validators.maxLength(64),
      ]),
      email: new FormControl("", [
        Validators.required,
        Validators.maxLength(256),
        Validators.pattern(
          "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$"
        ),
      ]),
    });
  }
  onSubmit(value) {
    var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    if (this.forgetPasswordForm.invalid) {
      this.customValidatorsService.scrollToError();
      if (value["userName"] === "" || value["userName"].length > 64) {
        this.translate.get("ALTADDUSERNAME").subscribe((res) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      } else if (
        value["email"] === "" ||
        value["email"].length > 256 ||
        value["email"].match(pattern) == null
      ) {
        this.translate.get("ALTADDEMAIL").subscribe((res) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      }
    } else {
      this.forgetPasswordService.forgetPassword(value).subscribe((res) => {
        if (res["status"]) {
          this.translate.get(res["message"]).subscribe((res) => {
            this.messageService.add({
              severity: "success",
              detail: res,
            });
          });
        } else {
          this.translate.get(res["message"]).subscribe((res) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
        }
      });
    }
  }

  login() {
    this.router.navigate(["./login"], { skipLocationChange: true });
  }
}
