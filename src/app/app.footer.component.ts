import { Component, OnInit } from "@angular/core";
import { EnvService } from "src/env.service";

@Component({
  selector: "app-footer",
  templateUrl: "./app.footer.component.html"
})
export class AppFooterComponent implements OnInit {
  display: boolean;
  version: any;
  constructor(private env: EnvService) {}

  ngOnInit() {
    this.version = this.env.version;
  }
}
