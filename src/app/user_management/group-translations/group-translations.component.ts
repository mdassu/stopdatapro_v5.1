import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { MessageService } from "primeng/api";

@Component({
  selector: "app-group-translatations",
  templateUrl: "./group-translations.component.html",
  styleUrls: ["./group-translations.component.css"]
})
export class GroupTranslationsComponent implements OnInit {
  constructor(
    private breadcrumbService: BreadcrumbService,
    private messageService: MessageService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLUSERMGMT", url: "./assets/help/group-translate.md" },
      { label: "LBLGROUPLISTING", routerLink: ["/group-listing"] }
    ]);
  }

  ngOnInit() {}
}
