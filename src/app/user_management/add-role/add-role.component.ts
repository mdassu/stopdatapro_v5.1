import { Component, OnInit, ElementRef } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { UserService } from "../../_services/user.service";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;
@Component({
  selector: "app-add-role",
  templateUrl: "./add-role.component.html",
  styleUrls: ["./add-role.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class AddRoleComponent implements OnInit {
  addRoleForm: FormGroup;
  Status: any;
  copyForms: any;
  submitted: boolean = false;
  initialGeneralFormData: any;
  isLoading: boolean = true;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private activatedRoute: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private cookieService: CookieService,
    private userservice: UserService,
    private route: Router,
    private elementRef: ElementRef,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLUSERMGMT", url: "./assets/help/add-role.md" },
      { label: "LBLROLES", routerLink: ["/role-listing"] },
    ]);
  }

  ngOnInit() {
    this.addRoleForm = this.fb.group({
      roleName: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(128),
          this.noWhitespaceValidator,
        ])
      ),
      description: new FormControl("", [Validators.maxLength(256)]),
      copyFrom: new FormControl(-1),
      status: new FormControl(1, Validators.required),
    });

    this.initialGeneralFormData = this.addRoleForm.getRawValue();

    this.Status = [];
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: Object) => {
        this.Status.push({ label: res["LBLACTIVE"], value: 1 });
        this.Status.push({ label: res["LBLINACTIVE"], value: 0 });
      });
    this.copyForms = [];
    this.copyForms.push({ label: "None", value: -1 });
    this.copyForms.push({ label: "Data Entry Operator", value: 4 });
    this.copyForms.push({ label: "Observer Only", value: 5 });
    this.copyForms.push({ label: "Report Administrator", value: 3 });
    this.copyForms.push({ label: "Site Administrator", value: 2 });
    this.isLoading = false;
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  get uform() {
    return this.addRoleForm.controls;
  }

  addNewRole() {
    this.submitted = true;
    if (this.addRoleForm.invalid) {
      this.customValidatorsService.scrollToError();
      // let target;

      // target = this.elementRef.nativeElement.querySelector(".ng-invalid");

      // if (target) {
      //   $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
      //   target.focus();
      // }
    } else {
      this.userservice.addRole(this.addRoleForm.value).subscribe((res) => {
        if (res["status"] == true) {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "success",
              detail: res,
            });
          });
          setTimeout(() => {
            this.route.navigate(["./edit-role/" + res["id"]], {
              skipLocationChange: true,
            });
          }, 1000);
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
        }
      });
    }
  }

  resetRoleForm() {
    this.addRoleForm.patchValue(this.initialGeneralFormData);
  }
}
