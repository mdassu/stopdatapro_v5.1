import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { SelectItem } from "primeng/primeng";
import { FormGroup } from "@angular/forms";
import { ConfirmationService, MessageService } from "primeng/api";
import { UserService } from "../../_services/user.service";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
declare var $: any;

@Component({
  selector: "app-role-listing",
  templateUrl: "./role-listing.component.html",
  styleUrls: ["./role-listing.component.css"],
  providers: [ConfirmationService, MessageService],
})
export class RoleListingComponent implements OnInit {
  //  Variable Declaration
  filterForm: FormGroup;
  cities1: SelectItem[];
  cols: any[];
  cities2: SelectItem[];
  colsCount: any;
  cars1: any;
  userlevel: any;

  roleListing: any;
  defaultRole: any;
  dataCols: any[];
  dataCount: any;
  selectedData: any = [];
  tableCols: any[];
  selectedTableCol: string = "ROLENAME";
  searchText: any;
  allRoles: any[];
  visibleTable: boolean = true;
  isLoaded: boolean;
  confirmClass: any;
  appName: any;
  roleId: any;
  // Breadcrumb Service
  constructor(
    private breadcrumbService: BreadcrumbService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private userservice: UserService,
    private env: EnvService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLUSERMGMT", url: "./assets/help/role-listing.md" },
      { label: "LBLROLES" },
    ]);
  }

  ngOnInit() {
    // get Roles
    this.roleId = this.auth.UserInfo["roleId"];
    this.getRoles();
    this.translate
      .get(["LBLROLENAME", "LBLSTATUS"])
      .subscribe((res: Object) => {
        this.dataCols = [
          { field: "ROLENAME", header: res["LBLROLENAME"] },
          { field: "STATUS", header: res["LBLSTATUS"] },
        ];

        //  col of status
        this.dataCount = this.dataCols.length;

        this.tableCols = [];
        this.tableCols.push({ label: res["LBLROLENAME"], value: "ROLENAME" });

        this.cities1 = [];
        this.cities1.push({ label: res["LBLROLENAME"], value: null });
        this.cities1.push({ label: "Demo", value: { id: 1, name: "Demo" } });
        this.cities2 = this.cities1.slice(1, 6);
      });
    this.appName = this.env.appName;
  }

  getRoles() {
    this.userservice.getRole().subscribe((res) => {
      if (res["status"] == true) {
        this.allRoles = res["Data"];
        this.roleListing = res["Data"];
      }
      this.isLoaded = true;
    });
  }

  onTextSearch() {
    var field = this.selectedTableCol;
    var stext = this.searchText.toLowerCase();
    var fn = this;
    this.visibleTable = false;

    if (stext != "") {
      var newList = this.allRoles.filter((item) => {
        return item[field].toLowerCase().indexOf(stext) >= 0;
      });
      setTimeout(function () {
        fn.visibleTable = true;
      }, 10);
      this.roleListing = newList;
    } else {
      setTimeout(function () {
        fn.visibleTable = true;
      }, 10);
      this.roleListing = this.allRoles;
    }
  }

  deleteRecord() {
    this.confirmClass = "warning-msg";
    var defaultRole = [1, 2, 3, 4, 5];
    var roleIsDefault = true;
    this.selectedData.map((RoleId) => {
      var aexistRole = defaultRole.indexOf(RoleId["ROLEID"]);
      if (aexistRole == -1) {
        roleIsDefault = false;
      }
    });
    if (roleIsDefault == true) {
      this.translate.get("ALTDELDEFROLETYPE").subscribe((res: string) => {
        this.messageService.add({
          severity: "info",
          detail: res,
        });
      });
      return false;
    }
    this.translate.get("ALTDELCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var roleIds = [];
          this.selectedData.map((RoleId) => {
            roleIds.push(RoleId["ROLEID"]);
          });
          var roleId = roleIds.join();
          this.userservice.deleteRole({ roleId: roleId }).subscribe((res) => {
            this.selectedData = [];
            this.getRoles();
            this.searchText = "";
            if (res["status"] == true) {
              if (res["defaultRole"] == 1) {
                this.translate.get(res["message"]).subscribe((res: string) => {
                  this.messageService.add({
                    severity: "success",
                    detail: res,
                  });
                });
              } else {
                this.translate.get(res["message"]).subscribe((res: string) => {
                  this.messageService.add({
                    severity: "success",
                    detail: res,
                  });
                });
              }
            } else {
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            }
          });
        },
        reject: () => {
          this.selectedData = [];
          this.searchText = "";
        },
      });
    });
  }

  changeStatus(id) {
    this.confirmClass = "info-msg";
    var defaultRole = [1, 2, 3, 4, 5];
    var roleIsDefault = true;
    this.selectedData.map((RoleId) => {
      var aexistRole = defaultRole.indexOf(RoleId["ROLEID"]);
      if (aexistRole == -1) {
        roleIsDefault = false;
      }
    });
    if (id != "") {
      var aexistRole = defaultRole.indexOf(id);
      if (aexistRole == -1) {
        roleIsDefault = false;
      }
    }
    if (roleIsDefault == true) {
      this.translate.get("ALTCHGSTATUSDEFROLE").subscribe((res: string) => {
        this.messageService.add({
          severity: "info",
          detail: res,
        });
      });
      return false;
    }
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var roleIds = [];
          var roleId;
          if (id == "") {
            this.selectedData.map((RoleId) => {
              roleIds.push(RoleId["ROLEID"]);
            });
            roleId = roleIds.join();
          } else {
            roleId = id;
          }
          this.userservice
            .updateRoleStatus({ roleId: roleId })
            .subscribe((res) => {
              this.selectedData = [];
              this.getRoles();
              this.searchText = "";
              if (res["status"] == true) {
                if (res["defaultRole"] == 1) {
                  this.translate
                    .get(res["message"])
                    .subscribe((res: string) => {
                      this.messageService.add({
                        severity: "success",
                        detail: res,
                      });
                    });
                } else {
                  this.translate
                    .get(res["message"])
                    .subscribe((res: string) => {
                      this.messageService.add({
                        severity: "success",
                        detail: res,
                      });
                    });
                }
              } else {
                this.translate.get(res["message"]).subscribe((res: string) => {
                  this.messageService.add({
                    severity: "success",
                    detail: res,
                  });
                });
              }
            });
        },
        reject: () => {
          this.selectedData = [];
          this.searchText = "";
        },
      });
    });
  }
}
