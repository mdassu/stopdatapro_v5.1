import { Component, OnInit, ElementRef } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { SelectItem } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { SlideUpDownAnimations } from "../../_animations/slide-up-down.animations";
import { ConfirmationService, MessageService } from "primeng/api";
import { UserService } from "../../_services/user.service";
import { months } from "src/app/_models/months";
import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;

@Component({
  selector: "app-targets",
  templateUrl: "./targets.component.html",
  styleUrls: ["./targets.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
  animations: [SlideUpDownAnimations],
})
export class TargetsComponent implements OnInit {
  months: any = [
    { monthname: "LBLPOPULATETARGET", name: "allmonth" },
    { monthname: "FMKJANUARY", name: "jan" },
    { monthname: "FMKFEBRUARY", name: "feb" },
    { monthname: "FMKMARCH", name: "mar" },
    { monthname: "FMKAPRIL", name: "apr" },
    { monthname: "FMKMAAY", name: "may" },
    { monthname: "FMKJUNE", name: "jun" },
    { monthname: "FMKJULY", name: "jul" },
    { monthname: "FMKAUGUST", name: "aug" },
    { monthname: "FMKSEPTEMBER", name: "sep" },
    { monthname: "FMKOCTOBER", name: "oct" },
    { monthname: "FMKNOVEMBER", name: "nov" },
    { monthname: "FMKDECEMBER", name: "dec" },
  ];

  cols: any[];

  targetForm: FormGroup;
  filterForm: FormGroup;
  submitted: boolean;

  //  Declare name of all fields
  sitename: any;
  sites: SelectItem[];
  selectedCity1: any = "";
  cities1: SelectItem[];
  weekly: SelectItem[];
  targetListing: any;
  siteListing: SelectItem[];
  userListing: any = [];
  filterData: any;
  optionalFilter: any;
  finalGroups: any = [];
  wTarget: boolean = false;
  tTarget: boolean = true;
  selectedFilter: any = [];
  selectedSiteId: any;
  errorMessage = false;
  populateAll: number;
  initalTargetFormData: any;
  iconType = "ui-icon-add";
  weeklyMaxLength: any;
  selectedPeople: any = [];
  public show_advance_search: boolean = false;
  public show_dialog: boolean = false;
  animationState = "out";
  isLoading: boolean = true;
  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private userservice: UserService,
    private elementRef: ElementRef,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLUSERMGMT", url: "./assets/help/targets.md" },
      { label: "LBLTARGETS", routerLink: ["/targets"] },
    ]);
  }

  get tform() {
    return this.targetForm.controls;
  }

  ngOnInit() {
    this.targetForm = this.fb.group({
      siteId: new FormControl(null, Validators.required),
      targets: new FormControl(0, Validators.required),
      userIds: new FormControl(null, Validators.required),
      allmonth: new FormControl(null),
      jan: new FormControl(null),
      feb: new FormControl(null),
      mar: new FormControl(null),
      apr: new FormControl(null),
      may: new FormControl(null),
      jun: new FormControl(null),
      jul: new FormControl(null),
      aug: new FormControl(null),
      sep: new FormControl(null),
      oct: new FormControl(null),
      nov: new FormControl(null),
      dec: new FormControl(null),
      weeklyTarget: new FormControl(null),
    });

    this.filterForm = this.fb.group({
      siteId: new FormControl(null),
    });

    var fn = this;
    this.translate.get(["LBLMONTHLY", "LBLWEEKLY"]).subscribe((res: Object) => {
      this.weekly = [];
      this.weekly.push({ label: res["LBLMONTHLY"], value: 0 });
      this.weekly.push({ label: res["LBLWEEKLY"], value: 1 });
    });

    // get target data
    this.getTargetData();
  }

  getTargetData() {
    this.userservice.getTargetData({}).subscribe((res) => {
      if (res["status"] == true) {
        this.selectedSiteId = parseInt(res["defaultSiteId"]);
        this.targetForm.controls["siteId"].setValue(this.selectedSiteId);
        this.targetListing = res["Data"];
        this.siteListing = res["site"];
        this.userListing = [];
        var userList = res["users"];
        userList.map((data) => {
          this.userListing.push({
            label: data["FULLNAME"],
            value: data["USERID"],
          });
        });

        var filterData = res["filterData"];
        var optionalFilter = res["filterOptional"];
        //set filter data
        this.setFilters(filterData);
        this.setOptionalFilters(optionalFilter);
      }
      this.initalTargetFormData = this.targetForm.getRawValue();
      this.isLoading = false;
    });
  }

  toggleFilterShow() {
    this.animationState = this.animationState === "out" ? "in" : "out";
  }

  // set filter data
  setFilters(filterdata) {
    filterdata.map((item, index) => {
      var newData = JSON.parse(item["Data"]);

      var tempData = [];

      if (newData.length > 0) {
        var keies = Object.keys(newData[0]);
        var value = keies[0];
        var label = keies[1];
        newData.map((data) => {
          tempData.push({ label: data[label], value: data[value] });
        });
      }
      if (item["Name"] == "LBLBSITES") {
        item["field"] = "siteId";
      }
      if (
        item["Name"] != "LBLBSITES" &&
        item["Name"] != "User Level" &&
        item["Name"] != "Observer Status" &&
        item["Name"] != "Roles"
      ) {
        item["field"] = "group" + (index + 1);
      }
      item["Data"] = tempData;
    });
    this.filterData = filterdata;
    if (this.selectedFilter.group1) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group1);
    }
    if (this.selectedFilter.group2) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group2);
    }
    if (this.selectedFilter.group3) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group3);
    }
    if (this.selectedFilter.group4) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group4);
    }
    if (this.selectedFilter.group5) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group5);
    }
    if (this.selectedFilter.group6) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group6);
    }
    if (this.selectedFilter.group7) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group7);
    }
    if (this.selectedFilter.group8) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group8);
    }
    if (this.selectedFilter.group9) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group9);
    }
    if (this.selectedFilter.group10) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group10);
    }
  }

  // set optional filter data
  setOptionalFilters(optionalfilter) {
    optionalfilter.map((item) => {
      var newData = JSON.parse(item["Data"]);
      var tempData = [];

      if (newData.length > 0) {
        var keies = Object.keys(newData[0]);
        var value = keies[0];
        var label = keies[1];
      }

      newData.map((data) => {
        this.translate.get(data[label]).subscribe((label) => {
          tempData.push({ label: label, value: data[value] });
        });
      });
      if (item["Name"] == "LBLUSERLEVEL") {
        item["field"] = "userType";
      }
      if (item["Name"] == "LBLOBSERVERSTATUS") {
        item["field"] = "statusId";
      }
      if (item["Name"] == "LBLROLES") {
        item["field"] = "roleId";
      }
      item["Data"] = tempData;
    });
    this.optionalFilter = optionalfilter;
    if (this.selectedFilter.roleId) {
      this.rearrangeSelect(this.optionalFilter, this.selectedFilter.roleId);
    }
    if (this.selectedFilter.userType) {
      this.rearrangeSelect(this.optionalFilter, this.selectedFilter.userType);
    }
    if (this.selectedFilter.statusId) {
      this.rearrangeSelect(this.optionalFilter, this.selectedFilter.statusId);
    }
  }

  getSiteUsers(params) {
    this.targetForm.controls["userIds"].setValue(null);
    this.targetForm.controls["siteId"].setValue(this.selectedSiteId);
    var parameters;
    if (params) {
      parameters = params;
    } else {
      parameters = {
        siteId: this.selectedSiteId,
      };
    }
    this.userservice.getTargetData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.userListing = [];
        var userList = res["users"];
        userList.map((data) => {
          this.userListing.push({
            label: data["FULLNAME"],
            value: data["USERID"],
          });
        });
      }
    });
  }

  setTargetValue(allMonthVal, rowName) {
    if (
      isNaN(allMonthVal) == true ||
      allMonthVal > 99 ||
      allMonthVal < 0 ||
      allMonthVal.startsWith(" ")
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      setTimeout(() => {
        this.targetForm.controls[rowName].reset({ value: "", disabled: false });
      }, 2000);
      this.targetForm.controls[rowName].reset({ value: "", disabled: true });
      this.targetForm.controls["jan"].setValue("");
      this.targetForm.controls["feb"].setValue("");
      this.targetForm.controls["mar"].setValue("");
      this.targetForm.controls["apr"].setValue("");
      this.targetForm.controls["may"].setValue("");
      this.targetForm.controls["jun"].setValue("");
      this.targetForm.controls["jul"].setValue("");
      this.targetForm.controls["aug"].setValue("");
      this.targetForm.controls["sep"].setValue("");
      this.targetForm.controls["oct"].setValue("");
      this.targetForm.controls["nov"].setValue("");
      this.targetForm.controls["dec"].setValue("");
    } else {
      this.targetForm.controls["jan"].setValue(allMonthVal);
      this.targetForm.controls["feb"].setValue(allMonthVal);
      this.targetForm.controls["mar"].setValue(allMonthVal);
      this.targetForm.controls["apr"].setValue(allMonthVal);
      this.targetForm.controls["may"].setValue(allMonthVal);
      this.targetForm.controls["jun"].setValue(allMonthVal);
      this.targetForm.controls["jul"].setValue(allMonthVal);
      this.targetForm.controls["aug"].setValue(allMonthVal);
      this.targetForm.controls["sep"].setValue(allMonthVal);
      this.targetForm.controls["oct"].setValue(allMonthVal);
      this.targetForm.controls["nov"].setValue(allMonthVal);
      this.targetForm.controls["dec"].setValue(allMonthVal);
    }
  }

  setValue(monthVal, rowName) {
    if (
      isNaN(monthVal) == true ||
      monthVal > 99 ||
      monthVal < 0 ||
      monthVal.startsWith(" ")
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      setTimeout(() => {
        this.targetForm.controls[rowName].reset({ value: "", disabled: false });
      }, 2000);
      this.targetForm.controls[rowName].reset({ value: "", disabled: true });
    }
  }
  setWeeklyValue(monthVal) {
    if (
      isNaN(monthVal) == true ||
      monthVal > 99 ||
      monthVal < 0 ||
      monthVal.startsWith(" ")
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      setTimeout(() => {
        this.targetForm.controls["weeklyTarget"].reset({
          value: "",
          disabled: false,
        });
      }, 2000);
      this.targetForm.controls["weeklyTarget"].reset({
        value: "",
        disabled: true,
      });
    }
  }
  addTargetForm(value: string): void {
    if (this.targetForm.value["userIds"]) {
      this.rearrangeSelects(this.userListing, this.targetForm.value["userIds"]);
    }
    this.submitted = true;

    if (this.targetForm.invalid) {
      this.customValidatorsService.scrollToError();
      // let target;

      // target = this.elementRef.nativeElement.querySelector(".ng-invalid");

      // if (target) {
      //   $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
      //   target.focus();
      // }
    } else {
      const jan = this.targetForm.value["jan"];
      const feb = this.targetForm.value["feb"];
      const mar = this.targetForm.value["mar"];
      const apr = this.targetForm.value["apr"];
      const may = this.targetForm.value["may"];
      const jun = this.targetForm.value["jun"];
      const jul = this.targetForm.value["jul"];
      const aug = this.targetForm.value["aug"];
      const sep = this.targetForm.value["sep"];
      const oct = this.targetForm.value["oct"];
      const nov = this.targetForm.value["nov"];
      const dec = this.targetForm.value["dec"];

      delete this.targetForm.value["allmonth"];
      const checkAllNull =
        jan == null &&
        feb == null &&
        mar == null &&
        apr == null &&
        may == null &&
        jun == null &&
        jul == null &&
        aug == null &&
        sep == null &&
        oct == null &&
        nov == null &&
        dec == null;
      const checkAllNullOrEmpty =
        !jan &&
        !feb &&
        !mar &&
        !apr &&
        !may &&
        !jun &&
        !jul &&
        !aug &&
        !sep &&
        !oct &&
        !nov &&
        !dec;
      this.weeklyMaxLength = Math.max(
        jan,
        feb,
        mar,
        apr,
        may,
        jun,
        jul,
        aug,
        sep,
        oct,
        nov,
        dec
      );

      if (
        this.targetForm.value["targets"] == 0 &&
        checkAllNullOrEmpty &&
        this.targetForm.value["targets"] != 1
      ) {
        this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      } else if (
        (this.weeklyMaxLength < 0 || this.weeklyMaxLength > 100) &&
        !checkAllNull
      ) {
        this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      } else if (
        this.weeklyMaxLength < this.targetForm.value["weeklyTarget"] &&
        !checkAllNull
      ) {
        this.translate.get("ALTTARGETPERWEEK").subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
        setTimeout(() => {
          this.targetForm.controls["weeklyTarget"].reset({
            value: "",
            disabled: false,
          });
        }, 2000);
        this.targetForm.controls["weeklyTarget"].reset({
          value: "",
          disabled: true,
        });
      } else if (
        (this.targetForm.value["weeklyTarget"] < 0 ||
          this.targetForm.value["weeklyTarget"] > 100) &&
        !checkAllNull
      ) {
        this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      } else {
        this.targetForm.value["userIds"] = this.targetForm.value[
          "userIds"
        ].join();
        this.userservice
          .updateTargetData(this.targetForm.value)
          .subscribe((res) => {
            if (res["status"] == true) {
              this.targetForm.value["userIds"] = this.targetForm.value[
                "userIds"
              ].split(",");
              this.initalTargetFormData = this.targetForm.value;
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            } else {
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "error",
                  detail: res,
                });
              });
            }
            const formData = this.targetForm.getRawValue();
            this.targetForm.reset();
            this.targetForm.patchValue(formData);
          });
      }
    }
  }

  filtering() {
    var selectedFilter = this.selectedFilter;
    var siteId = this.selectedSiteId;
    var roleId = "";
    var userType = "";
    var statusId = "";
    var group1 = "";
    var group2 = "";
    var group3 = "";
    var group4 = "";
    var group5 = "";
    var group6 = "";
    var group7 = "";
    var group8 = "";
    var group9 = "";
    var group10 = "";

    if (selectedFilter.roleId) {
      roleId = selectedFilter.roleId.join();
    }
    if (selectedFilter.userType) {
      userType = selectedFilter.userType.join();
    }
    if (selectedFilter.statusId) {
      statusId = selectedFilter.statusId.join();
    }
    if (selectedFilter.group1) {
      group1 = selectedFilter.group1.join();
    }

    if (selectedFilter.group2) {
      group2 = selectedFilter.group2.join();
    }
    if (selectedFilter.group3) {
      group3 = selectedFilter.group3.join();
    }

    if (selectedFilter.group4) {
      group4 = selectedFilter.group4.join();
    }
    if (selectedFilter.group5) {
      group5 = selectedFilter.group5.join();
    }

    if (selectedFilter.group6) {
      group6 = selectedFilter.group6.join();
    }
    if (selectedFilter.group6) {
      group6 = selectedFilter.group6.join();
    }

    if (selectedFilter.group7) {
      group7 = selectedFilter.group7.join();
    }
    if (selectedFilter.group8) {
      group8 = selectedFilter.group8.join();
    }
    if (selectedFilter.group9) {
      group9 = selectedFilter.group9.join();
    }
    if (selectedFilter.group10) {
      group10 = selectedFilter.group10.join();
    }

    var parameters = {
      siteId: siteId,
      roleId: roleId,
      userType: userType,
      statusId: statusId,
      group1: group1,
      group2: group2,
      group3: group3,
      group4: group4,
      group5: group5,
      group6: group6,
      group7: group7,
      group8: group8,
      group9: group9,
      group10: group10,
    };
    //this.getUsers(parameters);
    this.getSiteUsers(parameters);
    this.getTargetData();
  }

  clearFilter() {
    this.selectedFilter = [];
    this.getSiteUsers({ siteId: this.selectedSiteId });
  }
  // Advance serach
  advance_search() {
    this.iconType =
      this.iconType === "ui-icon-add" ? "ui-icon-remove" : "ui-icon-add";
    this.show_advance_search = !this.show_advance_search;
  }
  onSelectAll(i) {
    const selected = this.filterData[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.filterData[i]["field"]] = selected;
  }

  onClearAll(i) {
    this.selectedFilter[this.filterData[i]["field"]] = [];
  }

  onSelectAllAdv(i) {
    const selected = this.optionalFilter[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.optionalFilter[i]["field"]] = selected;
  }

  onClearAllAdv(i) {
    this.selectedFilter[this.optionalFilter[i]["field"]] = [];
  }

  onUsersSelectAll() {
    const selected = this.userListing.map((item) => item.USERID);
    this.targetForm.get("userIds").patchValue(selected);
    this.targetForm.get("userIds").markAsDirty();
  }

  onUsersClearAll() {
    this.targetForm.get("userIds").patchValue([]);
    this.targetForm.get("userIds").markAsDirty();
  }

  changeTarget() {
    this.submitted = false;
    if (this.targetForm.value["targets"] == 0) {
      this.wTarget = false;
      this.tTarget = true;
      this.targetForm.get("weeklyTarget").clearValidators();
      this.targetForm.get("weeklyTarget").updateValueAndValidity();
    } else {
      this.targetForm.get("weeklyTarget").setValidators([Validators.required]);
      this.wTarget = true;
      this.tTarget = false;
    }
  }

  resetTargetForm() {
    this.targetForm.patchValue(this.initalTargetFormData);
    this.changeTarget();
  }

  rearrangeSelect(dataArray, selectedIDs) {
    dataArray.map((records) => {
      var tempArr = [];
      var tempDataArr;
      tempDataArr = records.Data;
      selectedIDs.map((id) => {
        records.Data.map((data, key) => {
          if (data.value == id) {
            tempArr.push(data);
            tempDataArr.splice(key, 1);
          }
        });
      });
      var temp = tempArr.concat(tempDataArr);
      records.Data = temp;
    });
  }

  rearrangeSelects(dataArray, selectedIDs) {
    var tempArr = [];
    var tempDataArr;
    tempDataArr = dataArray;
    selectedIDs.map((id) => {
      dataArray.map((data, key) => {
        if (data.value == id) {
          tempArr.push(data);
          tempDataArr.splice(key, 1);
        }
      });
    });
    var temp = tempArr.concat(tempDataArr);
    this.userListing = temp;
    // });
  }
}
