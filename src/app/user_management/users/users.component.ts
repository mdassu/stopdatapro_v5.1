import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { SelectItem } from "primeng/primeng";
import { SlideUpDownAnimations } from "../../_animations/slide-up-down.animations";
import { FormGroup } from "@angular/forms";
import { ConfirmationService, MessageService } from "primeng/api";
import { Router, RoutesRecognized } from "@angular/router";
import { UserService } from "../../_services/user.service";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
import { filter, pairwise } from "rxjs/operators";
import { AuthenticationService } from "src/app/_services/authentication.service";

declare var $: any;

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"],
  providers: [ConfirmationService, MessageService],
  animations: [SlideUpDownAnimations],
})
export class UsersComponent implements OnInit {
  //  Variable Declaration
  filterForm: FormGroup;
  userForm: FormGroup;
  public show_dialog: boolean = false;
  public show_advance_search: boolean = false;
  public button_name: any = "Show Login Form!";
  searchBy: SelectItem[];
  allUsers: any[];
  userCols: any[];
  userCount: any;
  userlevel: any;
  userListing: any[];
  totalUsers: number;
  loading: boolean;
  selectedUser: any = [];
  searchText: any;
  selectedField: string = "USERNAME";
  visibleTable: boolean = true;
  filterData: any[];
  optionalFilter: any[];
  selectedFilter: any = [];
  isLoading: boolean = true;
  isLoaded: boolean;
  animationState = "out";
  iconType = "ui-icon-add";
  totalRecords = 0;
  confirmClass: any;
  appName: any;
  filterSite: any[];
  previousUrl: string;
  parameters: any = {};
  userId: any;
  roleId: any;
  // Breadcrumb Service
  constructor(
    private breadcrumbService: BreadcrumbService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private userservice: UserService,
    private route: Router,
    private translate: TranslateService,
    private env: EnvService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLUSERMGMT", url: "./assets/help/user-listing.md" },
      { label: "LBLUSERS", routerLink: ["/users"] },
    ]);
  }

  ngOnInit() {
    // this.selectedFilter["siteId"] = [134, 146];
    this.userId = this.auth.UserInfo["userId"];
    this.roleId = this.auth.UserInfo["roleId"];
    this.route.events
      .pipe(
        filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((e: any) => {
        var url = e[0].urlAfterRedirects; // previous url
        localStorage.setItem("previousUrl-" + this.userId, url);
      });
    // this.previousUrl = url;

    this.checkFilter();
    //Get User

    this.translate
      .get([
        "LBLUSERNAME",
        "LBLLASTNAME",
        "LBLFIRSTNAME",
        "LBLEMAILID",
        "LBLSTATUS",
      ])
      .subscribe((res: Object) => {
        //Table Header
        this.userCols = [
          { field: "USERNAME", header: res["LBLUSERNAME"] },
          { field: "LASTNAME", header: res["LBLLASTNAME"] },
          { field: "FIRSTNAME", header: res["LBLFIRSTNAME"] },
          { field: "EMAIL", header: res["LBLEMAILID"] },
          { field: "STATUS", header: res["LBLSTATUS"] },
        ];

        //  user count
        this.userCount = this.userCols.length;

        // Search List
        this.searchBy = [];
        this.searchBy.push({ label: res["LBLUSERNAME"], value: "USERNAME" });
        this.searchBy.push({ label: res["LBLLASTNAME"], value: "LASTNAME" });
        this.searchBy.push({ label: res["LBLFIRSTNAME"], value: "FIRSTNAME" });
        this.searchBy.push({ label: res["LBLEMAILID"], value: "EMAIL" });
      });
    this.appName = this.env.appName;
  }

  checkDefaultSite() {
    var siteFilter = this.filterSite.filter((item) => {
      return item.REPORTPARAM == "SITEID";
    });
    if (siteFilter && siteFilter[0]["Data"].length == 0) {
      this.translate.get("LBLUSERALERT").subscribe((msg) => {
        this.messageService.add({
          severity: "error",
          detail: msg,
        });
      });
    } else {
      this.route.navigate(["./add-users"], { skipLocationChange: true });
    }
  }

  checkFilter() {
    this.parameters = {};
    this.previousUrl = localStorage.getItem("previousUrl-" + this.userId);
    if (this.previousUrl) {
      if (
        this.previousUrl.search("/add-users") != -1 ||
        this.previousUrl.search("/edit-user/") != -1
      ) {
        if (localStorage.getItem("filterInfo")) {
          this.toggleFilterShow();
          // this.animationState = "in";
          this.parameters = JSON.parse(localStorage.getItem("filterInfo"));
          var keys = Object.keys(this.parameters);
          keys.map((param) => {
            if (this.parameters[param]) {
              this.selectedFilter[param] = this.parameters[param]
                .split(",")
                .map(Number);
            }
          });
          localStorage.removeItem("previousUrl-" + this.userId);
        }
      } else {
        localStorage.removeItem("filterInfo");
      }
    }

    this.getUsers(this.parameters);
  }

  // get all users
  getUsers(parameters) {
    this.userservice.getUsers(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.allUsers = res["Data"];
        this.userListing = res["Data"];
        var filterData = res["filterData"];
        var optionalFilter = res["optionalFilter"];
        var filterSiteData = res["siteFilter"];
        //set filter data
        this.siteFilters(filterSiteData);
        this.setFilters(filterData);
        this.setOptionalFilters(optionalFilter);
        this.visibleTable = true;
        this.isLoading = false;
      } else {
        this.allUsers = [];
        this.userListing = [];
        this.visibleTable = true;
        this.isLoading = false;
      }

      this.isLoaded = true;
    });
  }
  toggleFilterShow() {
    this.animationState = this.animationState === "out" ? "in" : "out";
  }

  // set filter data
  siteFilters(filterSite) {
    filterSite.map((item, index) => {
      var newData = JSON.parse(item["Data"]);

      var tempData = [];

      if (newData.length > 0) {
        var keies = Object.keys(newData[0]);
        var value = keies[0];
        var label = keies[1];
        newData.map((data) => {
          tempData.push({ label: data[label], value: data[value] });
        });
      }
      if (item["Name"] == "LBLBSITES") {
        item["field"] = "siteId";
      }
      item["Data"] = tempData;
    });
    this.filterSite = filterSite;
    // if (this.selectedFilter.siteId) {
    //   this.rearrangeSelect(this.filterSite, this.selectedFilter.siteId);
    // }
  }

  // set filter data
  setFilters(filterdata) {
    filterdata.map((item, index) => {
      var newData = JSON.parse(item["Data"]);
      var tempData = [];

      if (newData.length > 0) {
        var keies = Object.keys(newData[0]);
        var value = keies[0];
        var label = keies[1];
        newData.map((data) => {
          tempData.push({ label: data[label], value: data[value] });
        });
      }
      if (item["Name"] == "LBLBSITES") {
        item["field"] = "siteId";
      }
      if (
        item["Name"] != "LBLBSITES" &&
        item["Name"] != "User Level" &&
        item["Name"] != "Observer Status" &&
        item["Name"] != "Roles"
      ) {
        item["field"] = "group" + (index + 1);
      }
      item["Data"] = tempData;
    });
    this.filterData = filterdata;
    if (this.selectedFilter.group1) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group1);
    }
    if (this.selectedFilter.group2) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group2);
    }
    if (this.selectedFilter.group3) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group3);
    }
    if (this.selectedFilter.group4) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group4);
    }
    if (this.selectedFilter.group5) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group5);
    }
    if (this.selectedFilter.group6) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group6);
    }
    if (this.selectedFilter.group7) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group7);
    }
    if (this.selectedFilter.group8) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group8);
    }
    if (this.selectedFilter.group9) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group9);
    }
    if (this.selectedFilter.group10) {
      this.rearrangeSelect(this.filterData, this.selectedFilter.group10);
    }
  }

  // set optional filter data
  setOptionalFilters(optionalFilter) {
    optionalFilter.map((item) => {
      var newData = JSON.parse(item["Data"]);
      var tempData = [];

      if (newData.length > 0) {
        var keies = Object.keys(newData[0]);
        var value = keies[0];
        var label = keies[1];
      }

      newData.map((data) => {
        this.translate.get(data[label]).subscribe((label) => {
          tempData.push({ label: label, value: data[value] });
        });
      });
      if (item["Name"] == "LBLUSERLEVEL") {
        item["field"] = "userType";
      }
      if (item["Name"] == "LBLOBSERVERSTATUS") {
        item["field"] = "statusId";
      }
      if (item["Name"] == "LBLROLES") {
        item["field"] = "roleId";
      }
      item["Data"] = tempData;
    });
    this.optionalFilter = optionalFilter;
    if (this.selectedFilter.roleId) {
      this.rearrangeSelect(this.optionalFilter, this.selectedFilter.roleId);
    }
    if (this.selectedFilter.userType) {
      this.rearrangeSelect(this.optionalFilter, this.selectedFilter.userType);
    }
    if (this.selectedFilter.statusId) {
      this.rearrangeSelect(this.optionalFilter, this.selectedFilter.statusId);
    }
  }
  // Advance serach
  advance_search() {
    this.iconType =
      this.iconType === "ui-icon-add" ? "ui-icon-remove" : "ui-icon-add";
    this.show_advance_search = !this.show_advance_search;
  }

  // onTextSearch data
  onTextSearch() {
    var field = this.selectedField;
    var stext = this.searchText.toLowerCase();
    var fn = this;
    this.visibleTable = false;
    if (stext != "") {
      var newList = this.allUsers.filter((item) => {
        return item[field].toLowerCase().indexOf(stext) >= 0;
      });
      this.userListing = newList;
      setTimeout(function () {
        fn.visibleTable = true;
      }, 10);
    } else {
      setTimeout(function () {
        fn.visibleTable = true;
      }, 10);
      this.userListing = this.allUsers;
    }
  }

  filtering() {
    this.visibleTable = false;
    var selectedFilter = this.selectedFilter;
    var siteId = "";
    var roleId = "";
    var userType = "";
    var statusId = "";
    var group1 = "";
    var group2 = "";
    var group3 = "";
    var group4 = "";
    var group5 = "";
    var group6 = "";
    var group7 = "";
    var group8 = "";
    var group9 = "";
    var group10 = "";

    if (selectedFilter.siteId) {
      siteId = selectedFilter.siteId.join();
      // this.rearrangeSelect(this.filterSite, selectedFilter.siteId)
    }
    if (selectedFilter.roleId) {
      roleId = selectedFilter.roleId.join();
      //this.rearrangeSelect(this.optionalFilter, selectedFilter.roleId)
    }
    if (selectedFilter.userType) {
      userType = selectedFilter.userType.join();
    }
    if (selectedFilter.statusId) {
      statusId = selectedFilter.statusId.join();
    }
    if (selectedFilter.group1) {
      group1 = selectedFilter.group1.join();
    }

    if (selectedFilter.group2) {
      group2 = selectedFilter.group2.join();
    }
    if (selectedFilter.group3) {
      group3 = selectedFilter.group3.join();
    }

    if (selectedFilter.group4) {
      group4 = selectedFilter.group4.join();
    }
    if (selectedFilter.group5) {
      group5 = selectedFilter.group5.join();
    }

    if (selectedFilter.group6) {
      group6 = selectedFilter.group6.join();
    }
    if (selectedFilter.group6) {
      group6 = selectedFilter.group6.join();
    }

    if (selectedFilter.group7) {
      group7 = selectedFilter.group7.join();
    }
    if (selectedFilter.group8) {
      group8 = selectedFilter.group8.join();
    }
    if (selectedFilter.group9) {
      group9 = selectedFilter.group9.join();
    }
    if (selectedFilter.group10) {
      group10 = selectedFilter.group10.join();
    }

    var parameters = {
      siteId: siteId,
      roleId: roleId,
      userType: userType,
      statusId: statusId,
      group1: group1,
      group2: group2,
      group3: group3,
      group4: group4,
      group5: group5,
      group6: group6,
      group7: group7,
      group8: group8,
      group9: group9,
      group10: group10,
    };
    localStorage.removeItem("filterInfo");
    localStorage.setItem("filterInfo", JSON.stringify(parameters));
    this.getUsers(parameters);
  }

  rearrangeSelect(dataArray, selectedIDs) {
    dataArray.map((records) => {
      var tempArr = [];
      var tempDataArr;
      tempDataArr = records.Data;
      selectedIDs.map((id) => {
        records.Data.map((data, key) => {
          if (data.value == id) {
            tempArr.push(data);
            tempDataArr.splice(key, 1);
          }
        });
      });
      var temp = tempArr.concat(tempDataArr);
      records.Data = temp;
    });
  }

  clearFilter() {
    this.visibleTable = false;
    this.selectedFilter = [];
    this.getUsers({});
    this.searchText = "";
  }

  //Delete Record
  deleteRecord() {
    this.confirmClass = "warning-msg";
    this.translate.get("ALTDELCONFIRMUSERS").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var userIds = [];
          $.each(this.selectedUser, function (key, UserId) {
            userIds.push(UserId["USERID"]);
          });
          var userid = userIds.join();
          this.userservice.deleteUsers({ userId: userid }).subscribe((res) => {
            this.getUsers({});
            this.loading = false;
            this.selectedUser.length = 0;
            this.searchText = "";
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          });
        },
        reject: () => {
          this.selectedUser = [];
          this.searchText = "";
        },
      });
    });
  }

  //Change Status
  changeStatus() {
    this.confirmClass = "info-msg";
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          const body = {};
          body["userId"] = this.selectedUser.map((x) => x["USERID"]).join();
          this.userservice.updateUserStatus(body).subscribe((res) => {
            this.selectedUser.length = 0;
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.getUsers({});
            this.searchText = "";
          });
        },
        reject: () => {
          this.selectedUser = [];
          this.searchText = "";
        },
      });
    });
  }

  // changing status using row button
  changeStatusBtn(id) {
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((res: string) => {
      event.stopPropagation();
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          const body = {};
          body["userId"] = id;
          this.userservice.updateUserStatus(body).subscribe((res) => {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          });
          this.getUsers({});
        },
        reject: () => {
          this.selectedUser = [];
        },
      });
    });
  }

  routeToEdit(userid) {
    this.route.navigate(["./edit-user/" + userid], {
      skipLocationChange: true,
    });
  }
  onSelectAllSite(i) {
    const selected = this.filterSite[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.filterSite[i]["field"]] = selected;
  }

  onClearAllSite(i) {
    this.selectedFilter[this.filterSite[i]["field"]] = [];
  }

  onSelectAll(i) {
    const selected = this.filterData[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.filterData[i]["field"]] = selected;
  }

  onClearAll(i) {
    this.selectedFilter[this.filterData[i]["field"]] = [];
  }
  onSelectAllAdv(i) {
    const selected = this.optionalFilter[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.optionalFilter[i]["field"]] = selected;
  }

  onClearAllAdv(i) {
    this.selectedFilter[this.optionalFilter[i]["field"]] = [];
  }
}
