import { Component, OnInit, ElementRef } from "@angular/core";
import { Router } from "@angular/router";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  ValidatorFn,
  AbstractControl,
} from "@angular/forms";
import { MustMatch } from "../../_helpers/must-match.validator";

import { ConfirmationService, MessageService } from "primeng/api";
import { SelectItem } from "primeng/primeng";

import { CookieService } from "ngx-cookie-service";
import { BreadcrumbService } from "../../breadcrumb.service";
import { UserService } from "../../_services/user.service";
import { CustomValidatorsService } from "../../_services/custom-validators.service";
import { TranslateService } from "@ngx-translate/core";
import { SlideUpDownAnimations } from "src/app/_animations/slide-up-down.animations";
import { GlobalDataService } from "src/app/_services/global-data.service";
import { AuthenticationService } from "src/app/_services/authentication.service";

declare var $: any;

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
  animations: [SlideUpDownAnimations],
})
export class AddUserComponent implements OnInit {
  months: any = [
    { monthname: "Populate for all month" },
    { monthname: "January" },
    { monthname: "February" },
    { monthname: "March" },
    { monthname: "April" },
    { monthname: "May" },
    { monthname: "June" },
    { monthname: "July" },
    { monthname: "August" },
    { monthname: "September" },
    { monthname: "October" },
    { monthname: "November" },
    { monthname: "December" },
  ];

  //  Declare name of all fields
  userform: FormGroup;
  siteGroup: FormGroup;
  targetForm: FormGroup;
  submitted: boolean;
  weekly: SelectItem[];
  siteName: SelectItem[];
  YesNo: SelectItem[];
  status: SelectItem[];
  userType: any;
  Language: any;
  siteListing: SelectItem[];
  roleListing: Array<any>;
  groupListing: any;
  groupOption: any;
  finalGroups: any = [];
  grpList: any;
  TimeZone: SelectItem[];
  DateFormat: SelectItem[];
  tempGroups: any = [];
  defaultSite = [];
  userValue: any = 0;
  defaultTimeZone: any;
  initialGeneralFormData: any;
  userNameLength: any;
  passLength: any;
  passAlphaLength: any;
  passNumLength: any;
  passSpecialLength: any;
  patterns: any;
  passMessage: any;
  userNameMaxLength: number = 128;
  allSiteTimeZone: any;
  passwordComplexity: number = 0;
  isLoading: boolean = true;
  generalLoading: boolean = true;
  userLenError: string = "";
  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private userservice: UserService,
    private router: Router,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private customValidatorsService: CustomValidatorsService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLUSERMGMT", url: "./assets/help/user-listing-add.md" },
      { label: "LBLUSERS", routerLink: ["/users"] },
    ]);
  }

  ngOnInit() {
    if (this.cookieService.get("edit-user")) {
      var fn = this;
      fn.cookieService.delete("edit-user");
      this.translate.get("ALTUSERUPDATED").subscribe((res: string) => {
        fn.messageService.add({
          severity: "success",
          detail: res,
        });
      });
    }
    this.siteGroup = this.fb.group({
      siteIds: "",
    });
    this.userform = this.fb.group(
      {
        userName: new FormControl("", [
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
          this.customValidatorsService.notStartsWithSpace,
          this.customValidatorsService.maxLengthValidator(
            this.userNameMaxLength
          ),
        ]),
        firstName: new FormControl("", [
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
        ]),
        lastName: new FormControl("", [
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
        ]),
        emailId: [
          "",
          [
            Validators.required,
            this.customValidatorsService.noSpaceValidator,
            // Validators.pattern(
            //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
            // ),
            this.customValidatorsService.isEmailValid,
            Validators.maxLength(256),
          ],
        ],
        userPassword: [
          "",
          [
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
          ],
        ],
        confirmPassword: ["", [Validators.required]],
        status: new FormControl(1),
        jobTitle: new FormControl(""),
        userType: new FormControl(0),
        defaultRole: new FormControl(0, [Validators.min(1)]),
        userOffline: new FormControl(0),
        userOfflineApprove: new FormControl(0),
        siteIds: new FormControl(""),
        userDefaultSite: new FormControl(null, Validators.required),
        languageId: new FormControl(null),
        userDateFormat: new FormControl(null),
        userTimeZone: new FormControl(null),
      },
      {
        validator: MustMatch("userPassword", "confirmPassword"),
      }
    );
    this.getAddUserData();
    this.translate
      .get([
        "LBLSELECTONLY",
        "LBLWEEKLY",
        "LBLMONTHLY",
        "LBLYES",
        "LBLNO",
        "LBLACTIVE",
        "LBLINACTIVE",
        "LBLUSER",
        "LBLOBSERVER",
        "LBLUSROBS",
        "LBLDMY",
        "LBLMDY",
        "LBLYMD",
        "LBLSTATUS",
      ])
      .subscribe((res: Object) => {
        this.weekly = [];
        this.weekly.push({ label: res["LBLSELECTONLY"], value: null });
        this.weekly.push({
          label: res["LBLWEEKLY"],
          value: { id: 1, name: "Weekly" },
        });
        this.weekly.push({
          label: res["LBLMONTHLY"],
          value: { id: 2, name: "Monthly" },
        });

        this.YesNo = [];
        this.YesNo.push({ label: res["LBLYES"], value: 1 });
        this.YesNo.push({ label: res["LBLNO"], value: 0 });

        this.status = [];
        this.status.push({ label: res["LBLACTIVE"], value: 1 });
        this.status.push({ label: res["LBLINACTIVE"], value: 0 });

        this.userType = [];
        this.userType.push({ label: res["LBLUSER"], value: 0 });
        this.userType.push({ label: res["LBLOBSERVER"], value: 1 });
        this.userType.push({ label: res["LBLUSROBS"], value: 2 });

        this.DateFormat = [];
        this.DateFormat.push({ label: res["LBLDMY"], value: 103 });
        this.DateFormat.push({ label: res["LBLMDY"], value: 101 });
        this.DateFormat.push({ label: res["LBLYMD"], value: 111 });

        this.TimeZone = [];
        this.TimeZone.push({ label: res["LBLSTATUS"], value: "" });
        this.TimeZone.push({ label: res["LBLYES"], value: 25 });
        this.TimeZone.push({ label: res["LBLNO"], value: 27 });
      });
  }
  get uform() {
    return this.userform.controls;
  }

  onEmailChange(event) {
    if (this.userValue == 1) {
      if (event.target.value !== "") {
        this.userform.get("emailId").setValidators([
          this.customValidatorsService.noSpaceValidator,
          // Validators.pattern(
          //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
          // ),
          this.customValidatorsService.isEmailValid,
          Validators.maxLength(256),
        ]);
        this.userform.get("emailId").updateValueAndValidity();
      } else {
        this.userform.get("emailId").clearValidators();
        this.userform.get("emailId").updateValueAndValidity();
      }
    }
  }

  getAddUserData() {
    this.defaultSite = [];
    this.userservice.addUser().subscribe((res) => {
      if (res["status"] == true) {
        var fn = this;
        this.roleListing = res["role"];
        this.Language = res["language"];
        this.TimeZone = res["timeZone"];
        this.allSiteTimeZone = res["allSiteTimezone"];

        this.siteListing = [];
        var siteList = res["userSiteAvail"];
        siteList.map((data) => {
          this.siteListing.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });
        this.defaultSite.push(res["site"]);
        var groupListing = res["groupMandatory"];
        var groupOption = res["userGroup"];
        if (this.userType !== "Observer") {
          this.userform.controls["defaultRole"].setValidators([
            Validators.required,
            Validators.min(1),
          ]);
        }

        this.translate.get("LBLSELECT").subscribe((res: string) => {
          const rolesSelect = JSON.parse(JSON.stringify(this.roleListing));
          rolesSelect.splice(0, 0, {
            ROLEID: 0,
            ROLENAME: res,
          });
          this.roleListing = rolesSelect;
        });

        this.grpList = [{ label: "All", value: "" }];
        this.tempGroups = [];
        this.finalGroups = [];
        groupListing.forEach(function (groupLS) {
          var fArr = [];
          fArr["data"] = groupOption.filter(function (groupVal) {
            return groupVal.GROUPTYPEID == groupLS.GROUPTYPEID;
          });
          fn.translate.get("LBLSELECT").subscribe((res: string) => {
            const groupsSelect = JSON.parse(JSON.stringify(fArr["data"]));
            groupsSelect.splice(0, 0, {
              GROUPID: 0,
              GROUPNAME: res,
            });
            fArr["data"] = groupsSelect;
          });
          fArr["groupName"] = groupLS.GROUPTYPENAME;
          fArr["mandatory"] = groupLS.MANDATORY;
          fArr["fieldname"] = groupLS.GROUPTYPENAME.toLowerCase();

          if (groupLS.MANDATORY == 0) {
            fn.userform.addControl(
              groupLS.GROUPTYPENAME.toLowerCase(),
              new FormControl(0)
            );
          } else {
            fn.userform.addControl(
              groupLS.GROUPTYPENAME.toLowerCase(),
              new FormControl(0, [Validators.required, Validators.min(1)])
            );
          }
          fn.tempGroups.push(groupLS.GROUPTYPENAME.toLowerCase());
          fn.finalGroups.push(fArr);
        });

        var siteIds =
          typeof res["site"] == "string"
            ? 0
            : this.defaultSite[0]["SITEID"].toString().split(",");
        var defaultValue = res["validDetails"];
        this.userNameLength = defaultValue[0].USERNAMELEN;
        this.passLength = defaultValue[0].PWDLEN;
        this.passAlphaLength = defaultValue[0].PWDALPHA;
        this.passNumLength = defaultValue[0].PWDNUMERIC;
        this.passSpecialLength = defaultValue[0].PWDSPECCHAR;
        this.passwordComplexity = defaultValue[0].PWDCOMPLEXITY;
        this.defaultTimeZone = this.translate.get(
          this.TimeZone["TIMEZONENAME"]
        )["value"];
        this.userform.controls["userDefaultSite"].setValue(
          this.defaultSite[0]["SITEID"]
        );
        this.userform.controls["languageId"].setValue(
          defaultValue[0]["LANGUAGEID"]
        );
        this.userform.controls["siteIds"].setValue(siteIds);
        this.userform.controls["userDateFormat"].setValue(
          parseInt(defaultValue[0]["DATEFORMAT"])
        );
        this.userform.controls["userTimeZone"].setValue(
          this.TimeZone["TIMEZONEID"]
        );

        this.userNameLength = res["validDetails"][0]["USERNAMELEN"];
        this.translate.get("ALTLETTERREQ").subscribe((errMsg) => {
          this.userLenError = errMsg.replace("#", this.userNameLength);
        });

        this.getSites();

        if (defaultValue[0].PWDCOMPLEXITY == 0) {
          this.userform.controls["userPassword"].setValidators([
            // 1. Password Field is Required
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
            // 3. Has a minimum length of characters
            Validators.minLength(defaultValue[0].PWDLEN),
          ]);
        } else if (defaultValue[0].PWDCOMPLEXITY == 1) {
          this.passMessage = "Password must contain only numeric characters.";
          this.userform.controls["userPassword"].setValidators([
            // 1. Password Field is Required
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
            // 2. check whether the entered password has a number
            Validators.pattern("^[0-9]*$"),
            // 3. Has a minimum length of numeric
            Validators.minLength(defaultValue[0].PWDLEN),
          ]);
        } else if (defaultValue[0].PWDCOMPLEXITY == 2) {
          this.passMessage =
            "Password must contain atleast " +
            this.passAlphaLength +
            " alphabets " +
            this.passNumLength +
            " numeric characters.";
          this.userform.controls["userPassword"].setValidators([
            // 1. Password Field is Required
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
            // 2. check whether the entered password has alphanumeric
            Validators.pattern(
              "(?=(.*\\d){" +
                this.passNumLength +
                "})(?=(.*[A-Za-z]){" +
                this.passAlphaLength +
                "})[a-zA-Z0-9]+$"
            ),
            // 3. Has a minimum length of characters
            Validators.minLength(defaultValue[0].PWDLEN),
          ]);
        } else if (defaultValue[0].PWDCOMPLEXITY == 3) {
          this.passMessage =
            "Password must contain atleast " +
            this.passAlphaLength +
            " alphabets " +
            this.passNumLength +
            " numeric and " +
            this.passSpecialLength +
            " special characters.";
          this.userform.controls["userPassword"].setValidators([
            // 1. Password Field is Required
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
            // 2. check whether the entered password has a alphanumeric+special characters
            Validators.pattern(
              "(?=(.*\\d){" +
                this.passNumLength +
                "})(?=(.*[A-Za-z]){" +
                this.passAlphaLength +
                "})(?=(.*[!@#$%^&+*?]){" +
                this.passSpecialLength +
                "})(?!.*[\\s])^.*"
            ),
            // 3. Has a minimum length of characters
            Validators.minLength(defaultValue[0].PWDLEN),
          ]);
        }
      }
      this.initialGeneralFormData = this.userform.getRawValue();
      this.generalLoading = false;
      this.isLoading = false;
    });
  }

  getSites() {
    var siteids = this.userform.value["siteIds"];
    var fn = this;
    this.defaultSite = [];
    if (siteids.length > 0) {
      siteids.forEach(function (id) {
        let site;
        site = fn.siteListing.filter(function (site) {
          return site["value"] == id;
        });
        fn.defaultSite.push(site[0]);
      });
      this.userform.patchValue({
        userDefaultSite: this.defaultSite[0]["value"],
      });
    } else {
      fn.defaultSite = [];
      this.userform.controls["userDefaultSite"].setValue(null);
    }
    this.getTimeZone();
  }

  get emailId() {
    return this.userform.get("emailId") as FormControl;
  }

  checkUser() {
    this.userValue = this.userform.value["userType"];

    if (this.userValue == 1) {
      this.userform.get("defaultRole").clearValidators();
      this.userform.get("defaultRole").updateValueAndValidity();
      this.userform.get("emailId").clearValidators();
      this.userform.get("emailId").updateValueAndValidity();
      this.userform.get("userPassword").clearValidators();
      this.userform.get("userPassword").reset();
      this.userform.get("userPassword").updateValueAndValidity();
      this.userform.get("confirmPassword").clearValidators();
      this.userform.get("confirmPassword").reset();
      this.userform.get("confirmPassword").updateValueAndValidity();

      if (this.userform.value["emailId"] !== "") {
        this.userform.get("emailId").setValidators([
          Validators.email,
          this.customValidatorsService.noSpaceValidator,
          // Validators.pattern(
          //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
          // ),
          this.customValidatorsService.isEmailValid,
          Validators.maxLength(256),
        ]);
        this.userform.get("emailId").updateValueAndValidity();
      }
    } else if (this.userValue != 1) {
      this.userform.controls["emailId"].setValidators([
        Validators.required,
        this.customValidatorsService.noSpaceValidator,
        // Validators.pattern(
        //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
        // ),
        this.customValidatorsService.isEmailValid,
        Validators.maxLength(256),
      ]);
      this.userform.get("emailId").updateValueAndValidity();
      this.userform
        .get("defaultRole")
        .setValidators([Validators.required, Validators.min(1)]);
      this.userform.get("defaultRole").updateValueAndValidity();
      this.userform
        .get("userPassword")
        .setValidators([
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
        ]);
      this.userform.get("userPassword").updateValueAndValidity();
      this.userform.get("confirmPassword").setValidators([Validators.required]);
      this.userform.get("confirmPassword").updateValueAndValidity();
    }
  }

  onSubmit(bType) {
    this.submitted = true;
    if (this.userform.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.generalLoading = true;
      var groupIds = [];
      var fn = this;
      this.tempGroups.forEach(function (group) {
        if (fn.userform.value[group] != null) {
          groupIds.push(fn.userform.value[group]);
        }
        delete fn.userform.value[group];
      });
      this.userform.value["groupIds"] = groupIds.join();
      this.userform.value["siteIds"] = this.userform.value["siteIds"].join();
      delete this.userform.value["confirmPassword"];
      this.userform.value["firstName"] = this.userform.value[
        "firstName"
      ].replace(/\s/g, "");
      this.userform.value["lastName"] = this.userform.value["lastName"].replace(
        /\s/g,
        ""
      );
      this.userservice.insertUser(this.userform.value).subscribe((res) => {
        if (res["status"] == true) {
          if (bType) {
            this.submitted = false;
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            setTimeout(() => {
              this.ngOnInit();
            }, 2000);
          } else {
            this.cookieService.set("add-new-user", "yes");
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            setTimeout(() => {
              this.router.navigate(["./edit-user/" + res["id"]], {
                skipLocationChange: true,
              });
            }, 2000);
          }
        } else {
          this.generalLoading = false;
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
        }
      });
    }
  }

  onReset() {
    this.userform.patchValue(this.initialGeneralFormData);
  }
  onSelectAll() {
    var selected = this.siteListing.map((item) => item["SITEID"]);
    this.userform.get("siteIds").patchValue(selected);
    this.userform.get("siteIds").markAsDirty();
    this.userform.get("userDefaultSite").patchValue(selected[0]);
    this.userform.get("userDefaultSite").markAsDirty();
    this.defaultSite = this.siteListing;
  }

  onClearAll() {
    this.userform.get("siteIds").patchValue([]);
    this.userform.get("siteIds").markAsDirty();
    this.userform.get("userDefaultSite").patchValue(null);
    this.userform.get("userDefaultSite").markAsDirty();
    this.defaultSite = [];
  }
  getTimeZone() {
    const defaultSite = this.userform.get("userDefaultSite").value;
    const timeZone = this.allSiteTimeZone.filter((item, key) => {
      if (this.auth.UserInfo["defaultSiteId"] == 1 && key == 0) {
        return item;
      } else {
        if (item["SITEID"] === defaultSite) {
          return item;
        }
      }
    });
    this.defaultTimeZone = this.translate.get(timeZone[0]["TIMEZONENAME"])[
      "value"
    ];
    this.userform.patchValue({
      userTimeZone: +timeZone[0]["TIMEZONEID"],
    });
  }
}
