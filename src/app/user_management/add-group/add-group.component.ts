import { Component, ElementRef, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { BreadcrumbService } from "../../breadcrumb.service";
import { UserService } from "../../_services/user.service";
import { CustomValidatorsService } from "../../_services/custom-validators.service";
import { TranslateService } from "@ngx-translate/core";
declare var $: any;
@Component({
  selector: "app-add-group",
  templateUrl: "./add-group.component.html",
  styleUrls: ["./add-group.component.css"],
  providers: [MessageService, CustomValidatorsService],
})
export class AddGroupComponent implements OnInit {
  addGroupForm: FormGroup;
  GroupType: any;
  groupTypeId: any = 0;
  submitted: boolean;
  Status: any;
  initialGeneralFormData: FormGroup;
  groupNameMaxLength: number = 128;
  isLoading: boolean = true;

  constructor(
    private readonly breadcrumbService: BreadcrumbService,
    private readonly fb: FormBuilder,
    private readonly messageService: MessageService,
    private readonly cookieService: CookieService,
    private readonly userservice: UserService,
    private readonly router: Router,
    private readonly elementRef: ElementRef,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLUSERMGMT", url: "./assets/help/add-group.md" },
      { label: "LBLGROUPS", routerLink: ["/group-listing"] },
    ]);
  }

  ngOnInit() {
    this.addGroupForm = this.fb.group({
      groupName: new FormControl("", [
        Validators.required,
        this.customValidatorsService.noSpecialCharValidator,
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.notStartsWithSpace,
        this.customValidatorsService.maxLengthValidator(
          this.groupNameMaxLength
        ),
      ]),
      groupTypeId: new FormControl({ value: 0, disabled: true }),
      status: new FormControl(1, Validators.required),
    });

    this.userservice.getGroupType().subscribe((res) => {
      this.GroupType = res["Data"];

      if (this.GroupType != null) {
        this.groupTypeId = this.GroupType[0]["value"];
        if (this.cookieService.get("grouptypeid")) {
          this.groupTypeId = this.cookieService.get("grouptypeid");
        }
        this.addGroupForm.controls["groupTypeId"].setValue(
          parseInt(this.groupTypeId)
        );
      }
      this.initialGeneralFormData = this.addGroupForm.getRawValue();
      this.isLoading = false;
    });

    this.Status = [];
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: Object) => {
        this.Status.push({ label: res["LBLACTIVE"], value: 1 });
        this.Status.push({ label: res["LBLINACTIVE"], value: 0 });
      });
  }

  get groupForm() {
    return this.addGroupForm.controls;
  }

  submitGroup() {
    this.submitted = true;
    if (this.addGroupForm.invalid) {
      this.customValidatorsService.scrollToError();
      // let target;
      // target = this.elementRef.nativeElement.querySelector(".ng-invalid");
      // if (target) {
      //   $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
      //   target.focus();
      // }
    } else {
      this.addGroupForm.value["groupTypeId"] = this.groupTypeId;
      this.userservice.addGroup(this.addGroupForm.value).subscribe((res) => {
        if (res["status"] === true) {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "success",
              detail: res,
            });
          });
          setTimeout(() => {
            // this.cookieService.delete("grouptypeid");
            this.cookieService.set("group-new", "yes");
            this.router.navigate(["./edit-group/" + res["id"]], {
              skipLocationChange: true,
            });
          }, 1000);
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
        }
      });
    }
  }
  onSubmit(bType) {
    this.submitted = true;
    if (this.addGroupForm.invalid) {
      this.customValidatorsService.scrollToError();
      // let target;
      // target = this.elementRef.nativeElement.querySelector(".ng-invalid");
      // if (target) {
      //   $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
      //   target.focus();
      // }
    } else {
      this.addGroupForm.value["groupTypeId"] = this.groupTypeId;
      this.userservice.addGroup(this.addGroupForm.value).subscribe((res) => {
        if (res["status"] === true) {
          if (bType) {
            this.submitted = false;
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.onReset();
            this.addGroupForm.markAsPristine();
          } else {
            this.cookieService.set("group-new", "yes");
            this.router.navigate(["./add-group/"], {
              skipLocationChange: true,
            });
          }
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
        }
      });
    }
  }
  onReset() {
    this.addGroupForm.patchValue(this.initialGeneralFormData);
  }
}
