import { Component, OnInit, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { MustMatch } from "../../_helpers/must-match.validator";

import { ConfirmationService, MessageService } from "primeng/api";
import { SelectItem, Message } from "primeng/primeng";

import { CookieService } from "ngx-cookie-service";
import { BreadcrumbService } from "../../breadcrumb.service";
import { UserService } from "../../_services/user.service";
import { months } from "src/app/_models/months";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
declare var $: any;
@Component({
  selector: "app-edit-user",
  templateUrl: "./edit-user.component.html",
  styleUrls: ["./edit-user.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class EditUserComponent implements OnInit {
  months: any = [
    { monthname: "LBLPOPULATETARGET", name: "allmonth" },
    { monthname: "FMKJANUARY", name: "jan" },
    { monthname: "FMKFEBRUARY", name: "feb" },
    { monthname: "FMKMARCH", name: "mar" },
    { monthname: "FMKAPRIL", name: "apr" },
    { monthname: "FMKMAAY", name: "may" },
    { monthname: "FMKJUNE", name: "jun" },
    { monthname: "FMKJULY", name: "jul" },
    { monthname: "FMKAUGUST", name: "aug" },
    { monthname: "FMKSEPTEMBER", name: "sep" },
    { monthname: "FMKOCTOBER", name: "oct" },
    { monthname: "FMKNOVEMBER", name: "nov" },
    { monthname: "FMKDECEMBER", name: "dec" },
  ];

  //  Declare name of all fields
  userform: FormGroup;
  targetForm: FormGroup;
  submitted: boolean;
  targetSubmitted: boolean;
  Target: SelectItem[];
  siteName: SelectItem[];
  YesNo: SelectItem[];
  status: SelectItem[];
  userType: any;
  Language: any;
  siteListing: any[] = [];
  roleListing: any[];
  groupListing: any;
  groupOption: any;
  finalGroups: any = [];
  grpList: any;
  TimeZone: SelectItem[];
  DateFormat: SelectItem[];
  tempGroups: any = [];
  defaultSite: SelectItem[] = [];
  userValue: any = 0;
  userId: any;
  targetPanelStatus: boolean = false;
  tTarget: boolean = true;
  wTarget: boolean = false;
  defaultTimeZone: any;
  userNameLength: any;
  initialGeneralFormData: any;
  errorMessage = false;
  populateAll: number;
  passLength: any;
  passAlphaLength: any;
  passNumLength: any;
  passSpecialLength: any;
  patterns: any;
  passMessage: any;
  maxOfMonthlyTargets: any;
  targetdefaultSite: any = [];
  userNameMaxLength: number = 128;
  allSiteTimeZone: any;
  initialTargetFormData: any;
  index = 0;
  lastIndex = 0;
  msgs: Message[] = [];
  passwordComplexity: number = 0;
  isLoading: boolean = true;
  onSubmitTouched: boolean = false;
  weeeklyTargetsSubmitted: boolean;
  appName: any;
  generalLoading: boolean = false;
  targetLoading: boolean = false;
  userLenError: string = "ALTLETTERREQ";

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private userservice: UserService,
    private router: Router,
    private activetedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private customValidatorsService: CustomValidatorsService,
    private env: EnvService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.userId = this.activetedRoute.snapshot.paramMap.get("userid");
    this.breadcrumbService.setItems([
      { label: "LBLUSERMGMT", url: "./assets/help/user-listing-add.md" },
      { label: "LBLUSERS", routerLink: ["/users"] },
    ]);
    var data = {
      username: "",
      firstname: "",
      lastname: "",
      emailid: "",
      userpassword: "",
      confirmpassword: "",
      status: 1,
      jodtitle: "",
      usertype: 0,
      defaultrole: -1,
      useroffline: 0,
      userofflineapprove: 0,
      siteids: "",
      defualtsiteid: null,
      languageid: null,
      userdateformat: 103,
      usertimezone: null,
    };

    this.createFormBuilder(data);
    this.getAddUserData();
    this.translate
      .get([
        "LBLYES",
        "LBLNO",
        "LBLACTIVE",
        "LBLINACTIVE",
        "LBLUSER",
        "LBLOBSERVER",
        "LBLUSROBS",
        "LBLDMY",
        "LBLMDY",
        "LBLYMD",
        "LBLSTATUS",
        "LBLMONTHLY",
        "LBLWEEKLY",
      ])
      .subscribe((res: Object) => {
        this.YesNo = [];
        this.YesNo.push({ label: res["LBLYES"], value: 1 });
        this.YesNo.push({ label: res["LBLNO"], value: 0 });

        this.status = [];
        this.status.push({ label: res["LBLACTIVE"], value: 1 });
        this.status.push({ label: res["LBLINACTIVE"], value: 0 });

        this.userType = [];
        this.userType.push({ label: res["LBLUSER"], value: 0 });
        this.userType.push({ label: res["LBLOBSERVER"], value: 1 });
        this.userType.push({ label: res["LBLUSROBS"], value: 2 });

        this.DateFormat = [];
        this.DateFormat.push({ label: res["LBLDMY"], value: 103 });
        this.DateFormat.push({ label: res["LBLMDY"], value: 101 });
        this.DateFormat.push({ label: res["LBLYMD"], value: 111 });

        this.TimeZone = [];
        this.TimeZone.push({ label: res["LBLSTATUS"], value: "" });
        this.TimeZone.push({ label: res["LBLYES"], value: 25 });
        this.TimeZone.push({ label: res["LBLNO"], value: 27 });

        this.Target = [];
        this.Target.push({ label: res["LBLMONTHLY"], value: 0 });
        this.Target.push({ label: res["LBLWEEKLY"], value: 1 });
      });

    this.targetForm = this.fb.group({
      siteid: new FormControl(null),
      targets: new FormControl(0),
      allmonth: new FormControl(""),
      jan: new FormControl(""),
      feb: new FormControl(""),
      mar: new FormControl(""),
      apr: new FormControl(""),
      may: new FormControl(""),
      jun: new FormControl(""),
      jul: new FormControl(""),
      aug: new FormControl(""),
      sep: new FormControl(""),
      oct: new FormControl(""),
      nov: new FormControl(""),
      dec: new FormControl(""),
      targetAllSite: new FormControl(false),
      weeklyTarget: new FormControl(""),
    });
    this.appName = this.env.appName;
  }

  onEmailChange(event) {
    if (this.userValue == 1) {
      if (event.target.value !== "") {
        this.userform.get("emailId").setValidators([
          Validators.email,
          this.customValidatorsService.noSpaceValidator,
          // Validators.pattern(
          //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
          // ),
          this.customValidatorsService.isEmailValid,
          Validators.maxLength(256),
        ]);
        this.userform.get("emailId").updateValueAndValidity();
      } else {
        this.userform.get("emailId").clearValidators();
        this.userform.get("emailId").updateValueAndValidity();
      }
    }
  }

  get uform() {
    return this.userform.controls;
  }
  get tform() {
    return this.targetForm.controls;
  }

  getAddUserData() {
    var parameters = {
      userId: this.userId,
    };

    this.userservice.getUsers(parameters).subscribe((res) => {
      var fn = this;
      this.roleListing = res["role"];
      this.translate.get("LBLSELECT").subscribe((res: string) => {
        const rolesSelect = JSON.parse(JSON.stringify(this.roleListing));
        rolesSelect.splice(0, 0, {
          ROLEID: 0,
          ROLENAME: res,
        });
        this.roleListing = rolesSelect;
      });
      this.Language = res["language"];
      this.TimeZone = res["timeZone"];
      this.defaultTimeZone = this.translate.get(this.TimeZone["TIMEZONENAME"])[
        "value"
      ];
      this.allSiteTimeZone = res["allSiteTimezone"];
      this.siteListing = [];
      var siteList = res["userSiteAvail"];
      siteList.map((data) => {
        this.siteListing.push({
          label: data["SITENAME"],
          value: data["SITEID"],
        });
      });

      this.defaultSite = res["site"];

      var userData = res["userInfo"][0];

      if (userData["USERTYPEID"] == 0) {
        this.targetPanelStatus = false;
      } else {
        this.targetPanelStatus = true;
      }

      var assignSiteIds;
      if (userData["userSiteAssign"] != "") {
        assignSiteIds = userData["userSiteAssign"].split(",");
      } else {
        assignSiteIds = [];
      }

      this.targetForm.controls["siteid"].setValue(userData.DEFAULTSITEID);
      if (userData.STATUS == 4) {
        this.translate.get("LBLLOCK").subscribe((res) => {
          this.status.push({ label: res, value: 4 });
        });
      }
      var data = {
        username: userData.USERNAME,
        firstname: userData.FIRSTNAME,
        lastname: userData.LASTNAME,
        emailid: userData.EMAIL,
        userpassword: userData.PASSWORD,
        confirmpassword: userData.PASSWORD,
        status: userData.STATUS,
        jobtitle: userData.JOBTITLE,
        usertype: userData.USERTYPEID,
        defaultrole: userData.DEFAULTROLEID,
        useroffline: userData.OFFLINEACCESS,
        userofflineapprove: userData.APPROVEOFFLINE,
        siteids: assignSiteIds,
        defualtsiteid: userData.DEFAULTSITEID,
        languageid: userData.LANGUAGEID,
        userdateformat: userData.DATEFORMAT,
        usertimezone: userData.TIMEZONE,
      };

      this.createFormBuilder(data);
      this.getSites(userData.DEFAULTSITEID);

      var groupListing = res["groupMandatory"];

      var groupOption = res["userGroup"];

      this.grpList = [{ label: "All", value: "" }];

      this.finalGroups = [];

      this.userNameLength = res["validDetails"][0]["USERNAMELEN"];
      this.translate.get("ALTLETTERREQ").subscribe((errMsg) => {
        this.userLenError = errMsg.replace("#", this.userNameLength);
      });
      groupListing.forEach(function (groupLS) {
        var fArr = [];
        fArr["data"] = groupOption.filter(function (groupVal) {
          return groupVal.GROUPTYPEID == groupLS.GROUPTYPEID;
        });

        var selectedGroup = fArr["data"].filter(function (selectVal) {
          return selectVal.FLAG == 1;
        });
        fn.translate.get("LBLSELECT").subscribe((res: string) => {
          const groupsSelect = JSON.parse(JSON.stringify(fArr["data"]));
          groupsSelect.splice(0, 0, {
            GROUPID: 0,
            GROUPNAME: res,
          });
          fArr["data"] = groupsSelect;
        });

        fArr["groupName"] = groupLS.GROUPTYPENAME;
        fArr["mandatory"] = groupLS.MANDATORY;
        fArr["fieldname"] = groupLS.GROUPTYPENAME.toLowerCase();

        if (groupLS.MANDATORY == 0) {
          if (selectedGroup.length > 0) {
            fn.userform.addControl(
              groupLS.GROUPTYPENAME.toLowerCase(),
              new FormControl(selectedGroup[0]["GROUPID"])
            );
          } else {
            fn.userform.addControl(
              groupLS.GROUPTYPENAME.toLowerCase(),
              new FormControl(0)
            );
          }
        } else {
          if (selectedGroup.length > 0) {
            fn.userform.addControl(
              groupLS.GROUPTYPENAME.toLowerCase(),
              new FormControl(selectedGroup[0]["GROUPID"], [
                Validators.required,
                Validators.min(1),
              ])
            );
          } else {
            fn.userform.addControl(
              groupLS.GROUPTYPENAME.toLowerCase(),
              new FormControl(0, [Validators.required, Validators.min(1)])
            );
          }
        }
        fn.tempGroups.push(groupLS.GROUPTYPENAME.toLowerCase());
        fn.finalGroups.push(fArr);
      });

      var defaultValue = res["validDetails"];
      this.userNameLength = defaultValue[0].USERNAMELEN;
      this.passLength = defaultValue[0].PWDLEN;
      this.passAlphaLength = defaultValue[0].PWDALPHA;
      this.passNumLength = defaultValue[0].PWDNUMERIC;
      this.passSpecialLength = defaultValue[0].PWDSPECCHAR;
      this.passwordComplexity = defaultValue[0].PWDCOMPLEXITY;

      if (this.userValue != 1) {
        this.userform.controls["defaultRole"].setValidators([
          Validators.required,
          Validators.min(1),
        ]);
      }
      if (defaultValue[0].PWDCOMPLEXITY == 0) {
        this.userform.controls["userPassword"].setValidators([
          // 1. Password Field is Required
          Validators.required,
          // 3. Has a minimum length of characters
          Validators.minLength(defaultValue[0].PWDLEN),
        ]);
      } else if (defaultValue[0].PWDCOMPLEXITY == 1) {
        this.passMessage = "Password must contain only numeric characters.";
        this.userform.controls["userPassword"].setValidators([
          // 1. Password Field is Required
          Validators.required,
          // 2. check whether the entered password has a number
          Validators.pattern("^[0-9]*$"),
          // 3. Has a minimum length of numeric
          Validators.minLength(defaultValue[0].PWDLEN),
        ]);
      } else if (defaultValue[0].PWDCOMPLEXITY == 2) {
        this.passMessage =
          "Password must contain atleast " +
          this.passAlphaLength +
          " alphabets " +
          this.passNumLength +
          " numeric characters.";
        this.userform.controls["userPassword"].setValidators([
          // 1. Password Field is Required
          Validators.required,
          // 2. check whether the entered password has alphanumeric
          Validators.pattern(
            "(?=(.*\\d){" +
              this.passNumLength +
              "})(?=(.*[A-Za-z]){" +
              this.passAlphaLength +
              "})[a-zA-Z0-9]+$"
          ),
          // 3. Has a minimum length of characters
          Validators.minLength(defaultValue[0].PWDLEN),
        ]);
      } else if (defaultValue[0].PWDCOMPLEXITY == 3) {
        this.passMessage =
          "Password must contain atleast " +
          this.passAlphaLength +
          " alphabets " +
          this.passNumLength +
          " numeric and " +
          this.passSpecialLength +
          " special characters.";
        this.userform.controls["userPassword"].setValidators([
          // 1. Password Field is Required
          Validators.required,
          // 2. check whether the entered password has a alphanumeric+special characters
          Validators.pattern(
            "(?=(.*\\d){" +
              this.passNumLength +
              "})(?=(.*[A-Za-z]){" +
              this.passAlphaLength +
              "})(?=(.*[!@#$%^&+*?]){" +
              this.passSpecialLength +
              "})(?!.*[\\s])^.*"
          ),
          // 3. Has a minimum length of characters
          Validators.minLength(defaultValue[0].PWDLEN),
        ]);
      }
      this.targetOpen();
      this.initialGeneralFormData = this.userform.getRawValue();
      this.userValue = this.userform.value["userType"];
      if (this.userValue == 1) {
        this.userform.get("defaultRole").clearValidators();
        this.userform.get("defaultRole").updateValueAndValidity();
        this.userform.get("emailId").clearValidators();
        this.userform.get("emailId").updateValueAndValidity();
        this.userform.get("userPassword").clearValidators();
        this.userform.get("userPassword").updateValueAndValidity();
        this.userform.get("confirmPassword").clearValidators();
        this.userform.get("confirmPassword").updateValueAndValidity();
      }

      this.isLoading = false;
      this.onSubmitTouched = false;
    });
  }

  createFormBuilder(data) {
    this.userform = this.fb.group(
      {
        userName: new FormControl(data.username, [
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
          this.customValidatorsService.notStartsWithSpace,
          this.customValidatorsService.maxLengthValidator(
            this.userNameMaxLength
          ),
        ]),
        firstName: new FormControl(data.firstname, [
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
        ]),
        lastName: new FormControl(data.lastname, [
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
        ]),
        emailId: [
          data.emailid,
          [
            Validators.required,
            this.customValidatorsService.noSpaceValidator,
            // Validators.pattern(
            //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
            // ),
            this.customValidatorsService.isEmailValid,
            Validators.maxLength(256),
          ],
        ],
        userPassword: [
          data.userpassword,
          [
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
          ],
        ],
        confirmPassword: [data.confirmpassword, [Validators.required]],
        status: new FormControl(data.status),
        jobTitle: new FormControl(data.jobtitle),
        userType: new FormControl(data.usertype),
        defaultRole: new FormControl(data.defaultrole, [
          Validators.required,
          Validators.min(1),
        ]),
        userOffline: new FormControl(data.useroffline),
        userOfflineApprove: new FormControl(data.userofflineapprove),
        siteIds: new FormControl(data.siteids),
        userDefaultSite: new FormControl(
          data.defualtsiteid,
          Validators.required
        ),
        languageId: new FormControl(data.languageid),
        userDateFormat: new FormControl(data.userdateformat),
        userTimeZone: new FormControl(data.usertimezone),
      },
      {
        validator: MustMatch("userPassword", "confirmPassword"),
      }
    );
  }

  getSites(defaultSiteId) {
    var siteids = this.userform.value["siteIds"];
    var fn = this;
    this.defaultSite = [];
    if (siteids.length > 0) {
      siteids.forEach(function (id) {
        var site;
        site = fn.siteListing.filter(function (site) {
          return site["value"] == id;
        });
        fn.defaultSite.push(site[0]);
      });
      if (defaultSiteId != undefined && typeof defaultSiteId == "number") {
        this.userform.patchValue({
          userDefaultSite: defaultSiteId.toString(),
        });
      } else if (typeof defaultSiteId == "object") {
        defaultSiteId["value"].forEach((item) => {
          if (
            item["value"] != this.userform.controls["userDefaultSite"].value
          ) {
            this.userform.patchValue({
              userDefaultSite: defaultSiteId["value"][0],
            });
          } else {
          }
        });
      } else {
        this.userform.patchValue({
          userDefaultSite: defaultSiteId["value"][0],
        });
      }
    } else {
      fn.defaultSite = [];
      this.userform.controls["userDefaultSite"].setValue(null);
    }
    this.getTimeZone();
  }

  checkUser() {
    this.userValue = this.userform.value["userType"];

    if (this.userValue == 1) {
      this.userform.get("defaultRole").clearValidators();
      this.userform.get("defaultRole").updateValueAndValidity();
      this.userform.get("emailId").clearValidators();
      this.userform.get("emailId").updateValueAndValidity();
      this.userform.get("userPassword").clearValidators();
      this.userform.get("userPassword").updateValueAndValidity();
      this.userform.get("confirmPassword").clearValidators();
      this.userform.get("confirmPassword").updateValueAndValidity();

      if (this.userform.value["emailId"] !== "") {
        this.userform.get("emailId").setValidators([
          Validators.email,
          this.customValidatorsService.noSpaceValidator,
          // Validators.pattern(
          //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
          // ),
          this.customValidatorsService.isEmailValid,
          Validators.maxLength(256),
        ]);
        this.userform.get("emailId").updateValueAndValidity();
      }
    } else if (this.userValue != 1) {
      this.userform
        .get("defaultRole")
        .setValidators([Validators.required, Validators.min(1)]);
      this.userform.get("emailId").setValidators([
        Validators.required,
        Validators.email,
        this.customValidatorsService.noSpaceValidator,
        // Validators.pattern(
        //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
        // ),
        this.customValidatorsService.isEmailValid,
        Validators.maxLength(256),
      ]);
      this.userform.get("emailId").updateValueAndValidity();
      this.userform.get("defaultRole").updateValueAndValidity();
      this.userform.get("userPassword").setValidators([Validators.required]);
      this.userform.get("userPassword").updateValueAndValidity();
      this.userform.get("confirmPassword").setValidators([Validators.required]);
      this.userform.get("confirmPassword").updateValueAndValidity();
    }
  }

  onSubmit(bType) {
    this.submitted = true;
    if (this.userform.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.generalLoading = true;
      this.onSubmitTouched = true;
      var groupIds = [];
      var fn = this;
      this.tempGroups.forEach(function (group) {
        if (fn.userform.value[group] != null) {
          groupIds.push(fn.userform.value[group]);
        }
        delete fn.userform.value[group];
      });
      this.userform.value["groupIds"] = groupIds.join();
      this.userform.value["siteIds"] = this.userform.value["siteIds"].join();
      delete this.userform.value["confirmPassword"];
      this.userform.value["userId"] = +this.userId;
      this.userform.value["firstName"] = this.userform.value[
        "firstName"
      ].replace(/\s/g, "");
      this.userform.value["lastName"] = this.userform.value["lastName"].replace(
        /\s/g,
        ""
      );
      this.userservice.updateUser(this.userform.value).subscribe((res) => {
        if (res["status"] == true) {
          if (bType) {
            this.cookieService.set("edit-user", "yes");
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            setTimeout(() => {
              this.router.navigate(["./add-users"], {
                skipLocationChange: true,
              });
            }, 2000);
          } else {
            this.submitted = false;
            this.getAddUserData();
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          }
          this.generalLoading = false;
        } else {
          this.onSubmitTouched = false;
          this.userform.value["siteIds"] = this.userform.controls[
            "siteIds"
          ].value;
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
          this.generalLoading = false;
        }
      });
    }
  }

  targetOpen() {
    this.weeeklyTargetsSubmitted = false;
    this.targetLoading = true;
    var parameters = {
      userId: this.userId,
      siteId: this.targetForm.value["siteid"],
      targets: this.targetForm.value["targets"],
    };
    this.userservice.getUserTarget(parameters).subscribe((res) => {
      this.targetdefaultSite = res["site"];
      if (res["status"] == true) {
        var target = res["target"][0];
        if (this.targetForm.value["targets"] == 0) {
          this.tTarget = true;
          this.wTarget = false;
          if (res["target"].length > 0) {
            this.targetForm.controls["targets"].setValue(0);
            this.targetForm.controls["allmonth"].setValue("");
            this.targetForm.controls["jan"].setValue(target["JAN"]);
            this.targetForm.controls["feb"].setValue(target["FEB"]);
            this.targetForm.controls["mar"].setValue(target["MAR"]);
            this.targetForm.controls["apr"].setValue(target["APR"]);
            this.targetForm.controls["may"].setValue(target["MAY"]);
            this.targetForm.controls["jun"].setValue(target["JUN"]);
            this.targetForm.controls["jul"].setValue(target["JUL"]);
            this.targetForm.controls["aug"].setValue(target["AUG"]);
            this.targetForm.controls["sep"].setValue(target["SEP"]);
            this.targetForm.controls["oct"].setValue(target["OCT"]);
            this.targetForm.controls["nov"].setValue(target["NOV"]);
            this.targetForm.controls["dec"].setValue(target["DEC"]);
          } else {
            this.targetForm.controls["targets"].setValue(
              this.targetForm.value["targets"]
            );
            this.targetForm.controls["allmonth"].setValue(null);
            this.targetForm.controls["jan"].setValue(null);
            this.targetForm.controls["feb"].setValue(null);
            this.targetForm.controls["mar"].setValue(null);
            this.targetForm.controls["apr"].setValue(null);
            this.targetForm.controls["may"].setValue(null);
            this.targetForm.controls["jun"].setValue(null);
            this.targetForm.controls["jul"].setValue(null);
            this.targetForm.controls["aug"].setValue(null);
            this.targetForm.controls["sep"].setValue(null);
            this.targetForm.controls["oct"].setValue(null);
            this.targetForm.controls["nov"].setValue(null);
            this.targetForm.controls["dec"].setValue(null);
          }
          this.maxOfMonthlyTargets = Math.max(
            this.targetForm.value["jan"],
            this.targetForm.value["feb"],
            this.targetForm.value["mar"],
            this.targetForm.value["apr"],
            this.targetForm.value["may"],
            this.targetForm.value["jun"],
            this.targetForm.value["jul"],
            this.targetForm.value["aug"],
            this.targetForm.value["sep"],
            this.targetForm.value["oct"],
            this.targetForm.value["nov"],
            this.targetForm.value["dec"]
          );
          this.targetForm.controls["weeklyTarget"].clearValidators();
          this.targetForm.controls["weeklyTarget"].updateValueAndValidity();
          this.targetLoading = false;
        } else {
          this.tTarget = false;
          this.wTarget = true;
          this.targetForm.controls["weeklyTarget"].setValidators(
            Validators.required
          );
          if (res["target"].length > 0) {
            this.targetForm.controls["weeklyTarget"].setValue(
              target["WEEKTARGET"]
            );
          } else {
            this.targetForm.controls["weeklyTarget"].reset();
          }
          this.targetLoading = false;
        }
      } else {
        this.targetLoading = false;
      }
      this.initialTargetFormData = this.targetForm.getRawValue();
    });
    this.targetForm.get("siteid").markAsPristine();
    this.targetForm.get("targets").markAsPristine();
  }

  targetSubmit(value: string) {
    if (this.targetForm.value["targets"] == 1) {
      this.weeeklyTargetsSubmitted = true;
    }

    if (this.targetForm.valid) {
      this.targetForm.value["userId"] = parseInt(this.userId);
      delete this.targetForm.value["all"];
      this.targetForm.value["targetAllSite"] = this.targetForm.value[
        "targetAllSite"
      ]
        ? 1
        : 0;
      const jan = this.targetForm.value["jan"];
      const feb = this.targetForm.value["feb"];
      const mar = this.targetForm.value["mar"];
      const apr = this.targetForm.value["apr"];
      const may = this.targetForm.value["may"];
      const jun = this.targetForm.value["jun"];
      const jul = this.targetForm.value["jul"];
      const aug = this.targetForm.value["aug"];
      const sep = this.targetForm.value["sep"];
      const oct = this.targetForm.value["oct"];
      const nov = this.targetForm.value["nov"];
      const dec = this.targetForm.value["dec"];

      const checkAllNull =
        jan == null &&
        feb == null &&
        mar == null &&
        apr == null &&
        may == null &&
        jun == null &&
        jul == null &&
        aug == null &&
        sep == null &&
        oct == null &&
        nov == null &&
        dec == null;

      this.maxOfMonthlyTargets = Math.max(
        +this.targetForm.value["jan"],
        +this.targetForm.value["feb"],
        +this.targetForm.value["mar"],
        +this.targetForm.value["apr"],
        +this.targetForm.value["may"],
        +this.targetForm.value["jun"],
        +this.targetForm.value["jul"],
        +this.targetForm.value["aug"],
        +this.targetForm.value["sep"],
        +this.targetForm.value["oct"],
        +this.targetForm.value["nov"],
        +this.targetForm.value["dec"]
      );
      var weeklyMinLength = Math.min(
        this.targetForm.value["jan"],
        this.targetForm.value["feb"],
        this.targetForm.value["mar"],
        this.targetForm.value["apr"],
        this.targetForm.value["may"],
        this.targetForm.value["jun"],
        this.targetForm.value["jul"],
        this.targetForm.value["aug"],
        this.targetForm.value["sep"],
        this.targetForm.value["oct"],
        this.targetForm.value["nov"],
        this.targetForm.value["dec"]
      );
      const checkAllNullOrEmpty =
        !jan &&
        !feb &&
        !mar &&
        !apr &&
        !may &&
        !jun &&
        !jul &&
        !aug &&
        !sep &&
        !oct &&
        !nov &&
        !dec;

      if (
        this.targetForm.value["targets"] == 0 &&
        checkAllNullOrEmpty &&
        this.targetForm.value["targets"] != 1
      ) {
        this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      } else if (
        this.maxOfMonthlyTargets < 0 ||
        (this.maxOfMonthlyTargets > 100 && !checkAllNullOrEmpty)
      ) {
        this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
        setTimeout(() => {
          this.targetForm.controls["weeklyTarget"].reset({
            value: "",
            disabled: false,
          });
        }, 3200);
        this.targetForm.controls["weeklyTarget"].reset({
          value: "",
          disabled: true,
        });
      } else if (
        this.maxOfMonthlyTargets < this.targetForm.value["weeklyTarget"] &&
        !checkAllNullOrEmpty
      ) {
        this.translate.get("ALTTARGETPERWEEK").subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      } else if (
        this.targetForm.value["weeklyTarget"] < 0 ||
        (this.targetForm.value["weeklyTarget"] > 100 && !checkAllNullOrEmpty)
      ) {
        this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      } else {
        if (this.targetForm.invalid) {
          this.targetSubmitted = true;
          this.customValidatorsService.scrollToError();
        } else {
          if (this.targetForm.value["targetAllSite"] == 1) {
            this.translate.get("ALTALLSITESTARGET").subscribe((res) => {
              this.confirmationService.confirm({
                message: res,
                accept: () => {
                  this.saveTargetData();
                },
              });
            });
          } else {
            this.saveTargetData();
          }
        }
      }
    }
  }

  saveTargetData() {
    this.targetSubmitted = false;
    this.targetLoading = true;
    this.userservice
      .insertUserTargetData(this.targetForm.value)
      .subscribe((res) => {
        if (res["status"] == true) {
          this.initialTargetFormData = this.targetForm.getRawValue();
          const formData = this.targetForm.getRawValue();
          this.targetForm.reset();
          this.targetForm.patchValue(formData);
          this.targetForm.controls["allmonth"].setValue("");
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "success",
              detail: res,
            });
          });
          this.targetLoading = false;
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
          this.targetLoading = false;
        }
        this.targetForm.controls["targetAllSite"].setValue(false);
      });
  }

  setTargetValue(allMonthVal, rowName) {
    if (
      isNaN(allMonthVal) == true ||
      allMonthVal > 100 ||
      allMonthVal < 0 ||
      allMonthVal.startsWith(" ")
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      setTimeout(() => {
        this.targetForm.controls[rowName].reset();
      }, 3200);
      this.targetForm.controls[rowName].reset();
      this.targetForm.patchValue(this.initialTargetFormData);
    } else {
      this.targetForm.controls["jan"].setValue(allMonthVal);
      this.targetForm.controls["feb"].setValue(allMonthVal);
      this.targetForm.controls["mar"].setValue(allMonthVal);
      this.targetForm.controls["apr"].setValue(allMonthVal);
      this.targetForm.controls["may"].setValue(allMonthVal);
      this.targetForm.controls["jun"].setValue(allMonthVal);
      this.targetForm.controls["jul"].setValue(allMonthVal);
      this.targetForm.controls["aug"].setValue(allMonthVal);
      this.targetForm.controls["sep"].setValue(allMonthVal);
      this.targetForm.controls["oct"].setValue(allMonthVal);
      this.targetForm.controls["nov"].setValue(allMonthVal);
      this.targetForm.controls["dec"].setValue(allMonthVal);
    }
  }

  setValue(monthVal, rowName) {
    if (
      isNaN(monthVal) == true ||
      monthVal > 100 ||
      monthVal < 0 ||
      monthVal.startsWith(" ")
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      setTimeout(() => {
        this.targetForm.controls[rowName].reset({ value: "", disabled: false });
      }, 2000);
      this.targetForm.controls[rowName].reset({ value: "", disabled: true });
    }
  }
  setWeeklyValue(monthVal) {
    if (
      isNaN(monthVal) == true ||
      monthVal > 100 ||
      monthVal < 0 ||
      monthVal.startsWith(" ")
    ) {
      this.translate.get("ALTENTERNUMERICVALUE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
      setTimeout(() => {
        this.targetForm.controls["weeklyTarget"].reset({
          value: "",
          disabled: false,
        });
      }, 3200);
      this.targetForm.controls["weeklyTarget"].reset({
        value: "",
        disabled: true,
      });
    }
  }

  onReset() {
    let defaultSiteID = +this.initialGeneralFormData["userDefaultSite"];
    this.userform.patchValue(this.initialGeneralFormData);
    this.getSites(defaultSiteID);
    this.targetForm.patchValue(this.initialTargetFormData);
  }
  onSelectAll() {
    const selected = this.siteListing.map((item) => item.SITEID);
    this.userform.get("siteIds").patchValue(selected);
    this.userform.get("siteIds").markAsDirty();
    this.userform.patchValue({
      userDefaultSite: selected[0],
    });
    this.userform.get("userDefaultSite").markAsDirty();
    this.defaultSite = this.siteListing;
    this.getTimeZone();
  }

  onClearAll() {
    this.userform.get("siteIds").patchValue([]);
    this.userform.get("siteIds").markAsDirty();
    this.userform.get("userDefaultSite").patchValue(null);
    this.userform.get("userDefaultSite").markAsDirty();
    this.defaultSite = [];
  }
  getTimeZone() {
    const defaultSite = +this.userform.get("userDefaultSite").value;
    const timeZone = this.allSiteTimeZone.filter((item) => {
      if (+item["SITEID"] === defaultSite) {
        return item;
      }
    });
    if (timeZone.length > 0) {
      this.defaultTimeZone = this.translate.get(timeZone[0]["TIMEZONENAME"])[
        "value"
      ];
      this.userform.patchValue({
        userTimeZone: +timeZone[0]["TIMEZONEID"],
      });
    }
  }

  handleChange(e) {
    if (
      (this.userform.dirty && this.lastIndex === 0) ||
      (this.targetForm.dirty && this.lastIndex === 1)
    ) {
      this.translate.get("LBLCONFIRMYESNO").subscribe((text: string) => {
        this.confirmationService.confirm({
          message: text,
          accept: () => {
            this.lastIndex = e.index;
            this.userform.reset();
            this.targetForm.reset();
            this.userform.patchValue(this.initialGeneralFormData);
            this.targetForm.patchValue(this.initialTargetFormData);
            if (e.index == 0) {
              this.targetForm.value["targets"] = 0;
              this.targetOpen();
            }
            this.setHelpFile(e.index);
          },
          reject: () => {
            this.index = this.lastIndex;
            this.setHelpFile(this.lastIndex);
          },
        });
      });
    } else {
      this.lastIndex = e.index;
      this.setHelpFile(e.index);
      if (e.index == 0) {
        this.targetForm.value["targets"] = 0;
        this.targetOpen();
      }
    }
  }
  setHelpFile(index) {
    if (index == 0) {
      this.breadcrumbService.setItems([
        { label: "LBLUSERMGMT", url: "./assets/help/user-listing-add.md" },
        { label: "LBLUSERS", routerLink: ["/users"] },
      ]);
    } else {
      this.breadcrumbService.setItems([
        { label: "LBLUSERMGMT", url: "./assets/help/user-add-targets.md" },
        { label: "LBLUSERS", routerLink: ["/users"] },
      ]);
    }
  }
}
