import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { SelectItem } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
import { UserService } from "../../_services/user.service";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
declare var $: any;

@Component({
  selector: "app-group-listing",
  templateUrl: "./group-listing.component.html",
  styleUrls: ["./group-listing.component.css"],
  providers: [ConfirmationService, MessageService],
})
export class GroupListingComponent implements OnInit {
  //  Variable Declaration
  filterForm: FormGroup;
  groupCols: any[] = [];
  groupCount: any;
  userlevel: any;
  tableCols: any;
  groupListing: any[];
  groupTypeListing: SelectItem[];
  totalGroups: number;
  selectedGroup: any = [];
  selectedGroupType: any;
  selectedGroupId: any = 0;
  isLoading: boolean;
  errormsg: any;
  allGroups: any[];
  selectedTableCol: string = "GROUPNAME";
  searchText: any;
  visibleTable: boolean = true;
  isLoaded: boolean;
  confirmClass: any;
  appName: any;
  roleId: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private cookieService: CookieService,
    private userservice: UserService,
    private route: Router,
    private translate: TranslateService,
    private env: EnvService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLUSERMGMT", url: "./assets/help/group-listing.md" },
      { label: "LBLGROUPS", routerLink: ["/group-listing"] },
    ]);
  }

  ngOnInit() {
    this.roleId = this.auth.UserInfo["roleId"];
    this.getGroupTyps();
    this.translate
      .get(["LBLGROUPNAME", "LBLSTATUS"])
      .subscribe((res: Object) => {
        this.groupCols = [
          { field: "GROUPNAME", header: res["LBLGROUPNAME"] },
          { field: "STATUS", header: res["LBLSTATUS"] },
        ];

        this.tableCols = [{ label: res["LBLGROUPNAME"], value: "GROUPNAME" }];

        this.groupCount = this.groupCols.length;
      });
    this.appName = this.env.appName;
  }

  onTextSearch() {
    var field = this.selectedTableCol;
    var stext = this.searchText.toLowerCase();
    var fn = this;
    this.visibleTable = false;

    if (stext != "") {
      var newList = this.allGroups.filter((item) => {
        return item[field].toLowerCase().indexOf(stext) >= 0;
      });
      setTimeout(function () {
        fn.visibleTable = true;
      }, 10);
      this.groupListing = newList;
    } else {
      setTimeout(function () {
        fn.visibleTable = true;
      }, 10);
      this.groupListing = this.allGroups;
    }
  }

  getGroupTyps() {
    this.userservice.getGroupType().subscribe((res) => {
      this.groupTypeListing = res["Data"];
      if (res["status"] == true) {
        if (this.cookieService.get("grouptypeid")) {
          this.selectedGroupId = parseInt(
            this.cookieService.get("grouptypeid")
          );
          // this.cookieService.delete("grouptypeid");
        } else {
          this.selectedGroupId = this.groupTypeListing[0]["value"];
          this.cookieService.set("grouptypeid", this.selectedGroupId);
        }
        this.selectedGroupType = this.selectedGroupId;
        this.getGroups();
      } else {
        this.getGroups();
      }
    });
  }

  getGroups() {
    // this.cookieService.set("grouptypeid", this.selectedGroupType);
    this.userservice
      .getGroups({ groupTypeId: this.selectedGroupId })
      .subscribe((grpData) => {
        if (grpData["status"] == true) {
          this.allGroups = grpData["Data"];
          this.groupListing = grpData["Data"];
        } else {
          this.allGroups = [];
          this.groupListing = [];
          this.isLoading = false;
          this.errormsg = "LBLRPTNORECFND";
        }
        this.isLoaded = true;
      });
  }

  deleteRecord() {
    this.confirmClass = "warning-msg";
    this.translate.get("ALTDELGROUPCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var groupIds = [];
          $.each(this.selectedGroup, function (key, GroupId) {
            groupIds.push(GroupId["GROUPID"]);
          });
          var groupid = groupIds.join();
          var parameters = {
            groupTypeId: this.selectedGroupType,
            groupId: groupid,
          };
          this.userservice.deleteGroups(parameters).subscribe((res) => {
            this.selectedGroup = [];
            this.searchText = "";
            this.getGroups();
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          });
        },
        reject: () => {
          this.selectedGroup = [];
          this.searchText = "";
        },
      });
    });
  }

  changeStatus(type, id) {
    this.confirmClass = "info-msg";
    event.stopPropagation();
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var groupIds = [];
          if (id == "") {
            $.each(this.selectedGroup, function (key, GroupId) {
              groupIds.push(GroupId["GROUPID"]);
            });
          } else {
            groupIds = [id];
          }
          var groupid = groupIds.join();
          var parameters = {
            groupTypeId: this.selectedGroupType,
            groupId: groupid,
          };

          this.userservice.updateGroupStatus(parameters).subscribe((res) => {
            this.selectedGroup = [];
            this.searchText = "";
            this.getGroups();
            if (res["status"] == true) {
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            } else {
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            }
          });
        },
        reject: () => {
          this.selectedGroup = [];
          this.searchText = "";
        },
      });
    });
  }

  serchByType() {
    // this.cookieService.set("grouptypeid", this.selectedGroupType);
    this.userservice
      .getGroups({ groupTypeId: this.selectedGroupType })
      .subscribe((grpData) => {
        this.selectedGroupId = this.selectedGroupType;
        this.allGroups = grpData["Data"];
        this.searchText = "";
        this.groupListing = grpData["Data"];
      });
  }

  routeToEditGroup(groupId) {
    var selectedGroup = this.groupTypeListing.filter((item) => {
      return item["value"] === this.selectedGroupType;
    });
    this.cookieService.set("groupId", groupId);
    this.cookieService.set("groupTypeName", selectedGroup[0]["label"]);
    this.route.navigate(["./edit-group"], {
      skipLocationChange: true,
    });
  }

  onRowSelect(data) {
    this.cookieService.set("grouptypeid", this.selectedGroupType);
    this.route.navigate(["/edit-group", data.GROUPID], {
      skipLocationChange: true,
    });
  }

  setGroupType() {
    this.cookieService.set("grouptypeid", this.selectedGroupType);
  }
}
