import { Component, OnInit, ElementRef } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";

import { EnvService } from "src/env.service";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { UserService } from "../../_services/user.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CustomValidatorsService } from "../../_services/custom-validators.service";
import { TranslateService } from "@ngx-translate/core";

declare var $: any;

@Component({
  selector: "app-edit-group",
  templateUrl: "./edit-group.component.html",
  styleUrls: ["./edit-group.component.css"],
  providers: [MessageService, CustomValidatorsService, ConfirmationService],
})
export class EditGroupComponent implements OnInit {
  updateGroupForm: FormGroup;
  editGroupAssignment: FormGroup;
  initialGeneralFormDataAssign: any;
  groupTypeId: any;
  groupId: any;
  groupTypeName: any;
  submitted: boolean;
  Status: any;
  userList: any;
  initialGeneralFormData: any;
  groupGeneralSubmit: boolean = false;
  groupAssignSubmit: boolean = false;
  submitted2: boolean = false;
  groupNameMaxLength: number = 128;
  index = 0;
  lastIndex = 0;
  isLoading: boolean = true;
  appName: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private activatedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private userservice: UserService,
    private router: Router,
    private elementRef: ElementRef,
    private customValidatorsService: CustomValidatorsService,
    private translate: TranslateService,
    private confirmationService: ConfirmationService,
    private env: EnvService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLUSERMGMT", url: "./assets/help/edit-group.md" },
      { label: "LBLGROUPS", routerLink: ["/group-listing"] },
    ]);
  }

  ngOnInit() {
    this.updateGroupForm = this.fb.group({
      groupName: new FormControl("", [
        Validators.required,
        this.customValidatorsService.noSpecialCharValidator,
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.notStartsWithSpace,
        this.customValidatorsService.maxLengthValidator(
          this.groupNameMaxLength
        ),
      ]),
      groupTypeId: new FormControl(0),
      status: new FormControl(1, Validators.required),
    });

    this.editGroupAssignment = this.fb.group({
      userIds: new FormControl(null, Validators.required),
      groupTypeId: new FormControl(0),
    });

    this.groupId = this.activatedRoute.snapshot.paramMap.get("groupId");

    var parameters = {
      groupId: parseInt(this.groupId),
    };
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: object) => {
        this.Status = [];
        this.Status.push({ label: res["LBLACTIVE"], value: 1 });
        this.Status.push({ label: res["LBLINACTIVE"], value: 0 });
      });

    this.loadData(parameters);
    this.appName = this.env.appName;
  }

  loadData(parameters) {
    this.userservice.getGroupData(parameters).subscribe((res) => {
      this.editGroupAssignment.reset();
      this.updateGroupForm.reset();
      if (res["status"] == true) {
        var groupInfo = res["groupInfo"][0];
        this.groupTypeId = groupInfo["GROUPTYPEID"];
        this.groupTypeName = groupInfo["GROUPTYPENAME"];
        this.updateGroupForm.controls["groupTypeId"].setValue(this.groupTypeId);
        this.updateGroupForm.controls["groupName"].setValue(
          res["groupInfo"][0]["GROUPNAME"]
        );
        this.updateGroupForm.controls["status"].setValue(
          parseInt(res["groupInfo"][0]["STATUS"])
        );

        // this.userList = res["userAvail"];
        this.userList = [];
        var siteList = res["userAvail"];
        siteList.map((data) => {
          this.userList.push({
            label: data["NAME"],
            value: data["USERID"],
          });
        });

        this.editGroupAssignment.controls["groupTypeId"].setValue(
          this.groupTypeId
        );
        if (groupInfo["userIds"]) {
          this.editGroupAssignment.controls["userIds"].setValue(
            groupInfo["userIds"].split(",")
          );
        } else {
          this.editGroupAssignment.controls["userIds"].setValue(null);
        }
      } else {
      }
      this.initialGeneralFormData = this.updateGroupForm.getRawValue();
      this.initialGeneralFormDataAssign = this.editGroupAssignment.getRawValue();
      this.isLoading = false;
    });
  }

  get groupForm() {
    return this.updateGroupForm.controls;
  }
  get groupForm2() {
    return this.editGroupAssignment.controls;
  }

  submitGroup() {
    this.submitted = true;
    this.groupGeneralSubmit = true;
    if (!this.updateGroupForm.valid) {
      let target;

      target = this.elementRef.nativeElement.querySelector(".ng-invalid");

      if (target) {
        $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
        target.focus();
      }
    } else {
      this.updateGroupForm.value["groupId"] = this.groupId;
      this.userservice
        .updateGroup(this.updateGroupForm.value)
        .subscribe((res) => {
          if (res["status"] === true) {
            this.translate.get(res["message"]).subscribe((msg: string) => {
              if (res["message"] == "ALTGROUPEXIST") {
                this.messageService.add({
                  severity: "error",
                  detail: msg,
                });
              } else {
                this.messageService.add({
                  severity: "success",
                  detail: msg,
                });
              }
            });
            this.initialGeneralFormData = this.updateGroupForm.getRawValue();
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
          }
          this.loadData({
            groupId: parseInt(this.groupId),
          });
        });
    }
  }

  onSubmit(bType) {
    this.groupGeneralSubmit = true;
    if (!this.updateGroupForm.valid) {
      let target;

      target = this.elementRef.nativeElement.querySelector(".ng-invalid");

      if (target) {
        $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
        target.focus();
      }
    } else {
      this.updateGroupForm.value["groupId"] = this.groupId;
      this.userservice
        .updateGroup(this.updateGroupForm.value)
        .subscribe((res) => {
          if (res["status"] === true) {
            if (bType) {
              this.groupGeneralSubmit = false;
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
              this.router.navigate(["./add-group/"], {
                skipLocationChange: true,
              });
            } else {
              // this.cookieService.delete("grouptypeid");
              this.cookieService.set("group-new", "yes");
              this.router.navigate(["./add-group/"], {
                skipLocationChange: true,
              });
            }
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
          }
        });
    }
  }

  updateGroupAssign() {
    this.submitted2 = true;
    this.groupAssignSubmit = true;
    if (!this.editGroupAssignment.valid) {
      let target;

      target = this.elementRef.nativeElement.querySelector(".ng-invalid");

      if (target) {
        $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
        target.focus();
      }
    } else {
      this.editGroupAssignment.value["groupId"] = this.groupId;
      this.editGroupAssignment.value[
        "userIds"
      ] = this.editGroupAssignment.value["userIds"].join();
      this.userservice
        .updateGroupAssign(this.editGroupAssignment.value)
        .subscribe((res) => {
          if (res["status"] == true) {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.initialGeneralFormDataAssign = this.editGroupAssignment.getRawValue();
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
          }
          this.loadData({
            groupId: parseInt(this.groupId),
          });
        });
    }
  }
  onReset() {
    this.updateGroupForm.patchValue(this.initialGeneralFormData);
  }
  onResetAssign() {
    this.editGroupAssignment.patchValue(this.initialGeneralFormDataAssign);
  }
  onSelectAll() {
    const selected = this.userList.map((item) => item.USERID);
    this.editGroupAssignment.get("userIds").patchValue(selected);
    this.editGroupAssignment.get("userIds").markAsDirty();
  }

  onClearAll() {
    this.editGroupAssignment.get("userIds").patchValue([]);
    this.editGroupAssignment.get("userIds").markAsDirty();
  }

  handleChange(e) {
    if (
      (this.updateGroupForm.dirty && this.lastIndex === 0) ||
      (this.editGroupAssignment.dirty && this.lastIndex === 1)
    ) {
      this.translate.get("LBLCONFIRMYESNO").subscribe((text: string) => {
        this.confirmationService.confirm({
          message: text,
          accept: () => {
            this.lastIndex = e.index;
            this.updateGroupForm.reset();
            this.editGroupAssignment.reset();
            this.updateGroupForm.patchValue(this.initialGeneralFormData);
            this.editGroupAssignment.patchValue(
              this.initialGeneralFormDataAssign
            );
            this.setHelpFile(e.index);
          },
          reject: () => {
            this.index = this.lastIndex;
            this.setHelpFile(this.lastIndex);
          },
        });
      });
    } else {
      this.lastIndex = e.index;
      this.setHelpFile(e.index);
    }
  }
  setHelpFile(index) {
    if (index == 0) {
      this.breadcrumbService.setItems([
        { label: "LBLUSERMGMT", url: "./assets/help/edit-group.md" },
        { label: "LBLGROUPS", routerLink: ["/group-listing"] },
      ]);
    } else if (index == 1) {
      this.breadcrumbService.setItems([
        {
          label: "LBLUSERMGMT",
          url: "./assets/help/edit-group-assignments.md",
        },
        { label: "LBLGROUPS", routerLink: ["/group-listing"] },
      ]);
    }
  }
}
