import {
  Component,
  OnInit,
  ElementRef,
  OnChanges,
  DoCheck,
} from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  Form,
} from "@angular/forms";

import { EnvService } from "src/env.service";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { UserService } from "../../_services/user.service";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

declare var $: any;

@Component({
  selector: "app-edit-role",
  templateUrl: "./edit-role.component.html",
  styleUrls: ["./edit-role.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class EditRoleComponent implements OnInit {
  updateRoleForm: FormGroup;
  updatePermissionForm: FormGroup;
  roleId: any;
  roleGeneral: any = [];
  Status: any;
  copyForms: any;
  copyFrom: any;
  rowGroupMetadata: any;
  permissions: any;
  assignUsers: any;
  reports: any;
  finalPermission: any = [];
  selectedData: any;
  selectedPermission: any[] = [];
  availUsers: any;
  availReports: any;
  selectedUsers: any = [];
  selectedReports: any = [];
  roleName: any;
  submitted: boolean = false;
  permissionCols: any;
  initialGeneralFormData: any;
  initialPermissionFormData: any;
  initialAssignFormData: any;
  initialReportFormData: any;
  index = 0;
  lastIndex = 0;
  isAvailUsersChanged: boolean;
  isAvailReportsChanged: boolean;
  isPermissionChanged: boolean;
  isLoading: boolean = true;
  appName: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private activatedRoute: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private cookieService: CookieService,
    private userservice: UserService,
    private route: Router,
    private elementRef: ElementRef,
    private env: EnvService,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLUSERMGMT", url: "./assets/help/edit-role.md" },
      { label: "LBLROLES", routerLink: ["/role-listing"] },
    ]);
  }

  ngOnInit() {
    this.roleId = this.activatedRoute.snapshot.paramMap.get("roleId");

    this.updateRoleForm = this.fb.group({
      roleName: new FormControl("", [
        Validators.required,
        Validators.maxLength(128),
        this.noWhitespaceValidator,
      ]),
      description: new FormControl("", [Validators.maxLength(256)]),
      status: new FormControl(1, Validators.required),
    });

    var parameters = {
      roleId: this.roleId,
    };

    this.Status = [];
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE", "LBLPERMISSION", "LBLACCESS"])
      .subscribe((res: Object) => {
        this.Status.push({ label: res["LBLACTIVE"], value: 1 });
        this.Status.push({ label: res["LBLINACTIVE"], value: 0 });
        this.permissionCols = [
          { field: "permission", header: res["LBLPERMISSION"] },
          { field: "hasAccess", header: res["LBLACCESS"] },
        ];
      });

    this.userservice.getRoleById(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.roleGeneral = res["Data"][0];
        this.updateRoleForm.controls["roleName"].setValue(
          this.roleGeneral["ROLENAME"]
        );
        this.updateRoleForm.controls["description"].setValue(
          this.roleGeneral["DESCRIPTION"]
        );
        this.copyFrom = this.roleGeneral["COPYFROM"];
        this.roleName = this.roleGeneral["ROLENAME"];
        this.updateRoleForm.controls["status"].setValue(
          this.roleGeneral["STATUS"]
        );

        // get permission data
        this.getPermissionData(parameters);

        // get Assign User data
        this.getRoleAssign(parameters);

        //get Reports data
        this.getReportsData(parameters);

        this.initialGeneralFormData = this.updateRoleForm.getRawValue();
      }
      this.isLoading = false;
    });
    this.appName = this.env.appName;
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  get uform() {
    return this.updateRoleForm.controls;
  }

  // get Role Permissions
  getPermissionData(parameters) {
    this.userservice.getRolePermission(parameters).subscribe((res) => {
      this.finalPermission = [];
      this.permissions = res["Data"];
      let parentPermission = this.permissions.filter((item) => {
        if (item.PERMISSIONVALUE === 1) {
          this.selectedPermission.push({
            data: {
              permission: this.translate.get(item.PERMISSIONNAME)["value"],
              permissionId: item.PERMISSIONID,
              parentId: item.PARENTID,
              permissionValue: item.PERMISSIONVALUE,
            },
          });
        }
        return item.PARENTID === 0;
      });
      parentPermission.map((permission) => {
        let finalArr = this.permissions.filter((data) => {
          return data.PARENTID === permission.PERMISSIONID;
        });
        this.finalPermission.push({
          data: {
            permission: this.translate.get(permission.PERMISSIONNAME)["value"],
            permissionId: permission.PERMISSIONID,
            parentId: permission.PARENTID,
            permissionValue: permission.PERMISSIONVALUE,
          },
          children: finalArr.map((obj) => ({
            data: {
              permission: this.translate.get(obj.PERMISSIONNAME)["value"],
              permissionId: obj.PERMISSIONID,
              parentId: obj.PARENTID,
              permissionValue: obj.PERMISSIONVALUE,
            },
          })),
        });
        this.initialPermissionFormData = this.selectedPermission;
      });
    });
  }

  // get Role Assign users data
  getRoleAssign(parameters) {
    this.userservice.getRoleAssign(parameters).subscribe((assignUsers) => {
      if (assignUsers["status"] == true) {
        this.availUsers = [];
        var availUsersList = assignUsers["avail"];
        availUsersList.map((data) => {
          this.availUsers.push({
            label: data["NAME"],
            value: data["USERID"],
          });
        });
        if (this.selectedUsers.USERID) {
          this.rearrangeSelect(this.availUsers, this.selectedUsers.USERID);
        }
        if (assignUsers["assign"][0]["userIds"]) {
          this.selectedUsers = assignUsers["assign"][0]["userIds"].split(",");
          this.initialAssignFormData = this.selectedUsers;
        }
      }
    });
  }

  // get User Assign Reports Data
  getReportsData(parameters) {
    this.userservice.getRoleReport(parameters).subscribe((reports) => {
      if (reports["status"] == true) {
        var availReportsList = reports["avail"];
        this.availReports = [];
        availReportsList.map((item) => {
          this.translate.get(item.REPORTNAME).subscribe((label) => {
            this.availReports.push({ label: label, value: item.REPORTID });
          });
        });
        if (this.selectedReports.REPORTID) {
          this.rearrangeSelect(
            this.availReports,
            this.selectedReports.REPORTID
          );
        }
        if (reports["assign"][0]["reportId"]) {
          this.selectedReports = reports["assign"][0]["reportId"]
            .split(",")
            .map(Number);
          this.initialReportFormData = this.selectedReports;
        }
      }
    });
  }

  // Data Table
  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.finalPermission) {
      for (let i = 0; i < this.finalPermission.length; i++) {
        let rowData = this.finalPermission[i];
        let label = rowData.label;
        if (i == 0) {
          this.rowGroupMetadata[label] = { index: 0, size: 1 };
        } else {
          let previousRowData = this.finalPermission[i - 1];
          let previousRowGroup = previousRowData.label;
          if (label === previousRowGroup) this.rowGroupMetadata[label].size++;
          else this.rowGroupMetadata[label] = { index: i, size: 1 };
        }
      }
    }
  }

  updateRole() {
    this.submitted = true;
    if (this.updateRoleForm.invalid) {
      this.customValidatorsService.scrollToError();
      // target = this.elementRef.nativeElement.querySelector(".ng-invalid");

      // if (target) {
      //   $("html,body").animate({ scrollTop: $(target).offset().top }, "slow");
      //   target.focus();
      // }
    } else {
      const formValue = this.updateRoleForm.value;
      this.updateRoleForm.value["roleId"] = this.roleId;
      this.userservice
        .updateRole(this.updateRoleForm.value)
        .subscribe((res) => {
          if (res["status"] == true) {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
          }
          this.updateRoleForm.reset();
          this.updateRoleForm.patchValue(formValue);
        });
    }
  }

  updatePermission() {
    let permissionIds = this.selectedPermission.map((node) => {
      return node.data.permissionId;
    });
    let parentPermissionIds = this.permissions.forEach((permission) => {
      if (permissionIds.indexOf(permission.PERMISSIONID, 0) !== -1) {
        if (permissionIds.indexOf(permission.PARENTID, 0) == -1) {
          permissionIds.push(permission.PARENTID);
        }
      }
    });

    permissionIds.concat(parentPermissionIds);
    var parameters = {
      roleId: this.roleId,
      permissionId: permissionIds.join(),
    };

    this.userservice.updateRolePermission(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.initialPermissionFormData = this.selectedPermission;
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "success",
            detail: res,
          });
        });
      } else {
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      }
      this.isPermissionChanged = false;
    });
  }

  updateRoleAssign() {
    var parameters = {
      roleId: this.roleId,
      userIds: this.selectedUsers.join(),
    };

    this.userservice.updateRoleAssign(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.initialAssignFormData = this.selectedUsers;
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "success",
            detail: res,
          });
        });
        var parameters = {
          roleId: this.roleId,
        };
        this.getRoleAssign(parameters);
      } else {
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      }
      this.isAvailUsersChanged = false;
    });
  }

  updateRoleReports() {
    var parameters = {
      roleId: this.roleId,
      reportIds: this.selectedReports.join(),
    };

    this.userservice.updateRoleReport(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.initialReportFormData = this.selectedReports;
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "success",
            detail: res,
          });
        });
        var parameters = {
          roleId: this.roleId,
        };
        this.getReportsData(parameters);
      } else {
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      }
      this.isAvailReportsChanged = false;
    });
  }

  onUsersSelectAll() {
    let selectedUsers = [];
    this.availUsers.map((item) => {
      selectedUsers.push(item.USERID);
    });
    this.isAvailUsersChanged = true;
    this.selectedUsers = selectedUsers;
  }

  onUsersClearAll() {
    this.selectedUsers = [];
    this.isAvailUsersChanged = true;
  }

  onReportsSelectAll() {
    let selectedReports = [];
    this.availReports.map((item) => {
      selectedReports.push(item.REPORTID);
    });
    this.isAvailReportsChanged = true;
    this.selectedReports = selectedReports;
  }

  onReportsClearAll() {
    this.selectedReports = [];
    this.isAvailReportsChanged = true;
  }

  resetRoleForm() {
    this.updateRoleForm.patchValue(this.initialGeneralFormData);
  }

  resetPermissionForm() {
    this.selectedPermission = this.initialPermissionFormData;
  }

  resetAssignForm() {
    this.selectedUsers = this.initialAssignFormData;
  }

  resetReportForm() {
    this.selectedReports = this.initialReportFormData;
  }

  onAvailUsersChanged() {
    this.isAvailUsersChanged = true;
  }

  onAvailReportsChanged() {
    this.isAvailReportsChanged = true;
  }

  handleChange(e) {
    if (
      (this.updateRoleForm.dirty && this.lastIndex === 0) ||
      (this.isPermissionChanged && this.lastIndex === 1) ||
      (this.isAvailUsersChanged && this.lastIndex === 2) ||
      (this.isAvailReportsChanged && this.lastIndex === 3)
    ) {
      this.translate.get("LBLCONFIRMYESNO").subscribe((text: string) => {
        this.confirmationService.confirm({
          message: text,

          accept: () => {
            if (this.lastIndex === 0) {
              this.updateRoleForm.reset();
              this.updateRoleForm.patchValue(this.initialGeneralFormData);
            } else if (this.lastIndex === 1) {
              this.selectedPermission = this.initialPermissionFormData;
              this.isPermissionChanged = false;
            } else if (this.lastIndex === 2) {
              this.selectedUsers = this.initialAssignFormData;
              this.isAvailUsersChanged = false;
            } else if (this.lastIndex === 3) {
              this.selectedReports = this.initialReportFormData;
              this.isAvailReportsChanged = false;
            }

            this.lastIndex = e.index;
            this.setHelpFile(e.index);
          },
          reject: () => {
            this.index = this.lastIndex;
            this.setHelpFile(this.lastIndex);
          },
        });
      });
    } else {
      this.lastIndex = e.index;
      this.setHelpFile(e.index);
    }
  }

  onPermissionChanges() {
    this.isPermissionChanged = true;
  }
  setHelpFile(index) {
    if (index == 0) {
      this.breadcrumbService.setItems([
        { label: "LBLUSERMGMT", url: "./assets/help/edit-role.md" },
        { label: "LBLROLES", routerLink: ["/role-listing"] },
      ]);
    } else if (index == 1) {
      this.breadcrumbService.setItems([
        { label: "LBLUSERMGMT", url: "./assets/help/edit-role-permissions.md" },
        { label: "LBLROLES", routerLink: ["/role-listing"] },
      ]);
    } else if (index == 2) {
      this.breadcrumbService.setItems([
        { label: "LBLUSERMGMT", url: "./assets/help/edit-role-assignments.md" },
        { label: "LBLROLES", routerLink: ["/role-listing"] },
      ]);
    } else if (index == 3) {
      this.breadcrumbService.setItems([
        { label: "LBLUSERMGMT", url: "./assets/help/edit-role-reports.md" },
        { label: "LBLROLES", routerLink: ["/role-listing"] },
      ]);
    }
  }

  rearrangeSelect(dataArray, selectedIDs) {
    dataArray.map((records) => {
      var tempArr = [];
      var tempDataArr;
      tempDataArr = records.Data;
      selectedIDs.map((id) => {
        records.Data.map((data, key) => {
          if (data.value == id) {
            tempArr.push(data);
            tempDataArr.splice(key, 1);
          }
        });
      });
      var temp = tempArr.concat(tempDataArr);
      records.Data = temp;
    });
  }
}
