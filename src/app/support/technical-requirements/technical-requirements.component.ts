import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-technical-requirements",
  templateUrl: "./technical-requirements.component.html",
  styleUrls: ["./technical-requirements.component.css"]
})
export class TechnicalRequirementsComponent implements OnInit {
  constructor(
    private breadcrumbService: BreadcrumbService,
    private translate: TranslateService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSUPPORT" },
      { label: "LBLTECHNICALREQ" }
    ]);
  }

  ngOnInit() {}
}
