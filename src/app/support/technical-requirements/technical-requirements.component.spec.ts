import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicalRequirementsComponent } from './technical-requirements.component';

describe('TechnicalRequirementsComponent', () => {
  let component: TechnicalRequirementsComponent;
  let fixture: ComponentFixture<TechnicalRequirementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicalRequirementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalRequirementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
