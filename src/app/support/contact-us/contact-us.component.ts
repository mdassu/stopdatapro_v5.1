import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { TranslateService } from "@ngx-translate/core";
import { ContactUsService } from "src/app/_services/contact-us.service";

@Component({
  selector: "app-contact-us",
  templateUrl: "./contact-us.component.html",
  styleUrls: ["./contact-us.component.css"]
})
export class ContactUsComponent implements OnInit {
  userCols: any;
  userCount: any;
  searchBy: any[];
  visibleTable: boolean;
  isLoading: boolean;
  allUsers: any;
  userListing: any;
  isLoaded: boolean;
  selectedField: any = "USERNAME";
  searchText: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private translate: TranslateService,
    private contactUsService: ContactUsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLSUPPORT" },
      { label: "LBLCONTACTUS" }
    ]);
  }

  ngOnInit() {
    this.getData();
    this.translate
      .get(["LBLNAME", "LBLEMAILID", "LBLROLE"])
      .subscribe((res: Object) => {
        //Table Header
        this.userCols = [
          { field: "USERNAME", header: res["LBLNAME"] },
          { field: "EMAIL", header: res["LBLEMAILID"] },
          { field: "ROLENAME", header: res["LBLROLE"] }
        ];

        //  user count
        this.userCount = this.userCols.length;

        // Search List
        this.searchBy = [];
        this.searchBy.push({ label: res["LBLNAME"], value: "USERNAME" });
        this.searchBy.push({ label: res["LBLEMAILID"], value: "EMAIL" });
        this.searchBy.push({ label: res["LBLROLE"], value: "ROLENAME" });
      });
  }
  getData() {
    this.contactUsService.getContactUs({}).subscribe(res => {
      if (res["status"] == true) {
        this.allUsers = res["data"];
        this.userListing = res["data"];
        this.visibleTable = true;
        this.isLoading = false;
      } else {
        this.allUsers = [];
        this.userListing = [];
        this.visibleTable = true;
        this.isLoading = false;
      }

      this.isLoaded = true;
    });
  }

  onTextSearch() {
    var field = this.selectedField;
    var stext = this.searchText.toLowerCase();
    var fn = this;
    this.visibleTable = false;
    if (stext != "") {
      var newList = this.allUsers.filter(item => {
        return item[field].toLowerCase().indexOf(stext) >= 0;
      });
      this.userListing = newList;
      setTimeout(function() {
        fn.visibleTable = true;
      }, 10);
    } else {
      setTimeout(function() {
        fn.visibleTable = true;
      }, 10);
      this.userListing = this.allUsers;
    }
  }
}
