import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { User } from "../_models/user";
import {
  SelectItem,
  MessageService,
  ConfirmationService,
} from "primeng/primeng";
import { Subscription } from "rxjs";
import { BreadcrumbService } from "../breadcrumb.service";
import { ProfileService } from "../_services/profile.service";
import { Profile } from "../_models/profile";
import { Sites, language, timeZone } from "../_helpers/interface-items";
import { AuthenticationService } from "../_services/authentication.service";
import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";
import { MustMatch } from "../_helpers/must-match.validator";

@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.css"],
  providers: [CustomValidatorsService],
})
export class UserProfileComponent implements OnInit, OnDestroy {
  users: User[];
  user: User;
  userForm: FormGroup;
  defaultSites: Array<Sites>;
  languages: Array<language>;
  dateFormats: SelectItem[] = [];
  timeZones: Array<timeZone>;
  allTimeZones: Array<timeZone>;
  subscription: Subscription;
  profile: any = new Profile();
  userName: string;
  initialData: any;
  submitted: boolean;
  isLanguageChanged = false;
  passwordMinLength: number;
  passSpecChars: number;
  passNumChars: number;
  passAlphaChars: number;
  minSpecialCharacter: boolean;
  minAlphaCharacters: boolean;
  minNumericCharacters: boolean;
  timeZoneValueArr: Array<timeZone>;
  isLoaded: boolean;
  crUserId: any;
  passwordComplexity: number = 0;
  passMessage: any;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private readonly breadcrumbService: BreadcrumbService,
    private profileService: ProfileService,
    public authenticationService: AuthenticationService,
    private router: Router,
    private customValidatorsService: CustomValidatorsService,
    private translate: TranslateService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLPROFILE", url: "./assets/help/user-profile.md" },
      { label: "LBLDASHBOARD", routerLink: ["/dashboard"] },
    ]);
  }

  ngOnInit() {
    this.buildForm(this.profile);

    this.subscription = this.route.data.subscribe((data) => {
      this.user = data.userResolver;
    });
    this.dateFormats.push({ label: "DD/MM/YYYY", value: "103" });
    this.dateFormats.push({ label: "MM/DD/YYYY", value: "101" });
    this.dateFormats.push({ label: "YYYY/MM/DD", value: "111" });
  }

  buildForm(profile: Profile) {
    this.userForm = this.fb.group(
      {
        userId: [0 || profile.userId, Validators.required],
        email: [
          "" || profile.email,
          [
            Validators.required,
            this.customValidatorsService.noSpaceValidator,
            // Validators.pattern(
            //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
            // ),
            this.customValidatorsService.isEmailValid,
            Validators.maxLength(256),
          ],
        ],
        userName: [
          { value: this.userName, disabled: true } || profile.userName,
          Validators.required,
        ],
        firstName: [
          "" || profile.firstName,
          [
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
          ],
        ],
        lastName: [
          "" || profile.lastName,
          [
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
          ],
        ],
        password: [
          "" || profile.password,
          [
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
          ],
        ],
        confirmPassword: ["" || profile.password, [Validators.required]],
        siteId: [0 || profile.siteId, Validators.required],
        dateFormat: [0 || profile.dateFormat],
        languageId: [0 || profile.languageId],
        timeZone: [
          { value: profile["timeZone"], disabled: true },
          Validators.required,
        ],
      },
      {
        validator: MustMatch("password", "confirmPassword"),
      }
    );
    this.onLoadData();
  }
  get uform() {
    return this.userForm.controls;
  }

  onLoadData() {
    const dateFormats = [
      { dateID: 103, value: "dd/MM/yyyy" },
      { dateID: 101, value: "MM/dd/yyyy" },
      { dateID: 111, value: "yyyy/MM/dd" },
    ];
    // const currUser = JSON.parse(localStorage["currentUser"]);
    const currUser = this.authenticationService.UserInfo;
    this.profileService.getProfileData({}).subscribe((res) => {
      this.timeZones = res["timeZone"].filter((item) => {
        if (item.TIMEZONEID == res["userInfo"]["TIMEZONE"]) {
          return true;
        }
      });
      dateFormats.forEach((item) => {
        if (item["dateID"] === res["userInfo"]["DATEFORMAT"]) {
          currUser["dateFormat"] = item["value"];
        }
      });
      currUser["defaultSiteId"] = res["userInfo"]["DEFAULTSITEID"];
      this.crUserId = currUser["userId"];
      localStorage.setItem(
        "currentUser-" + currUser["userId"],
        JSON.stringify(currUser)
      );
      this.translate.get(this.timeZones[0]["TIMEZONENAME"]).subscribe((res) => {
        this.timeZones[0]["TIMEZONENAME"] = res;
      });
      this.allTimeZones = res["allTimeZone"];
      this.languages = res["language"];
      this.defaultSites = res["site"];
      const profile = {};
      this.userName = res["userInfo"]["USERNAME"];
      this.userForm.controls["userName"].setValue(res["userInfo"]["USERNAME"]);
      this.userForm.controls["firstName"].setValue(
        res["userInfo"]["FIRSTNAME"]
      );
      this.userForm.controls["lastName"].setValue(res["userInfo"]["LASTNAME"]);
      this.userForm.controls["email"].setValue(res["userInfo"]["EMAIL"]);
      this.userForm.controls["password"].setValue(res["userInfo"]["PASSWORD"]);
      this.userForm.controls["confirmPassword"].setValue(
        res["userInfo"]["CONFIRMPASSWORD"]
      );
      if (res["site"].length > 0 && res["userInfo"]["DEFAULTSITEID"] > 0) {
        var site = res["site"].filter((item) => {
          return item.SITEID == res["userInfo"]["DEFAULTSITEID"];
        });

        if (site.length >= 1) {
          this.userForm.controls["siteId"].setValue(
            res["userInfo"]["DEFAULTSITEID"]
          );
        } else {
          this.userForm.controls["siteId"].setValue(null);
        }
      }
      this.userForm.controls["dateFormat"].setValue(
        res["userInfo"]["DATEFORMAT"].toString()
      );
      this.userForm.controls["languageId"].setValue(
        res["userInfo"]["LANGUAGEID"]
      );
      this.userForm.controls["userId"].setValue(res["userId"]);
      this.userForm.controls["timeZone"].setValue(
        res["userInfo"]["TIMEZONE"].toString()
      );
      this.profile = profile;
      this.passwordMinLength = res["globalDetail"]["PWDLEN"];
      this.passSpecChars = res["globalDetail"]["PWDSPECCHAR"] || 0;
      this.passNumChars = res["globalDetail"]["PWDNUMERIC"] || 0;
      this.passAlphaChars = res["globalDetail"]["PWDALPHA"] || 0;
      this.passwordComplexity = res["globalDetail"]["PWDCOMPLEXITY"];
      this.allTimeZones.forEach((item) => {
        if (item.SITEID === res["userInfo"]["DEFAULTSITEID"].toString()) {
          this.translate.get(item["TIMEZONENAME"]).subscribe((res) => {
            item["TIMEZONENAME"] = res;
          });
          this.timeZoneValueArr = [];
          this.timeZoneValueArr.push(item);
        }
      });
      if (res["globalDetail"]["PWDCOMPLEXITY"] == 0) {
        this.userForm.controls["password"].setValidators([
          // 1. Password Field is Required
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
          // 3. Has a minimum length of characters
          Validators.minLength(res["globalDetail"]["PWDLEN"]),
        ]);
      } else if (res["globalDetail"]["PWDCOMPLEXITY"] == 1) {
        this.passMessage = "Password must contain only numeric characters.";
        this.userForm.controls["password"].setValidators([
          // 1. Password Field is Required
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
          // 2. check whether the entered password has a number
          Validators.pattern("^[0-9]*$"),
          // 3. Has a minimum length of numeric
          Validators.minLength(res["globalDetail"]["PWDLEN"]),
        ]);
      } else if (res["globalDetail"]["PWDCOMPLEXITY"] == 2) {
        this.passMessage =
          "Password must contain atleast " +
          this.passAlphaChars +
          " alphabets " +
          this.passNumChars +
          " numeric characters.";
        this.userForm.controls["password"].setValidators([
          // 1. Password Field is Required
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
          // 2. check whether the entered password has alphanumeric
          Validators.pattern(
            "(?=(.*\\d){" +
              this.passNumChars +
              "})(?=(.*[A-Za-z]){" +
              this.passAlphaChars +
              "})[a-zA-Z0-9]+$"
          ),
          // 3. Has a minimum length of characters
          Validators.minLength(res["globalDetail"]["PWDLEN"]),
        ]);
      } else if (res["globalDetail"]["PWDCOMPLEXITY"] == 3) {
        this.passMessage =
          "Password must contain atleast " +
          this.passAlphaChars +
          " alphabets " +
          this.passNumChars +
          " numeric and " +
          this.passSpecChars +
          " special characters.";
        this.userForm.controls["password"].setValidators([
          // 1. Password Field is Required
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
          // 2. check whether the entered password has a alphanumeric+special characters
          Validators.pattern(
            "(?=(.*\\d){" +
              this.passNumChars +
              "})(?=(.*[A-Za-z]){" +
              this.passAlphaChars +
              "})(?=(.*[!@#$%^&+*?]){" +
              this.passSpecChars +
              "})(?!.*[\\s])^.*"
          ),
          // 3. Has a minimum length of characters
          Validators.minLength(res["globalDetail"]["PWDLEN"]),
        ]);
      }
      // this.buildForm(this.profile);
      this.initialData = this.userForm.getRawValue();
      this.isLoaded = false;
    });
  }

  passwordChange(data) {
    const specCharCount = (
      data.match(/[!@#$%^&+*()_+\-=\[\]{};':"\\|,.<>\/?]/g) || []
    ).length;
    this.minSpecialCharacter =
      specCharCount >= this.passSpecChars ? false : true;
    const alphaCount = (data.match(/[a-zA-Z]/g) || []).length;
    this.minAlphaCharacters = alphaCount >= this.passAlphaChars ? false : true;
    const numCount = (data.match(/[0-9]/g) || []).length;
    this.minNumericCharacters = numCount >= this.passNumChars ? false : true;
  }

  languageChange() {
    this.isLanguageChanged = true;
  }

  siteChange(data) {
    let searchStr = data.toString();
    this.allTimeZones.forEach((item) => {
      if (item.SITEID === searchStr) {
        this.userForm.controls["timeZone"].setValue(item.TIMEZONEID);
        this.translate.get(item["TIMEZONENAME"]).subscribe((res) => {
          item["TIMEZONENAME"] = res;
        });
        this.timeZoneValueArr = [];
        this.timeZoneValueArr.push(item);
      }
    });
  }

  onSubmit(value) {
    if (this.userForm.invalid) {
      this.customValidatorsService.scrollToError();
      this.submitted = true;
    } else {
      this.submitted = false;
      if (this.isLanguageChanged) {
        this.isLanguageChanged = false;
        this.translate
          .get("ALTCONFIRMPROFILELANGUAGE")
          .subscribe((res: string) => {
            this.confirmationService.confirm({
              message: res,
              accept: () => {
                value["userName"] = this.userName;
                this.profileService
                  .updateProfileData(value)
                  .subscribe((res) => {
                    if (res["status"]) {
                      this.translate
                        .get(res["message"])
                        .subscribe((res: string) => {
                          this.messageService.add({
                            severity: "success",
                            detail: res,
                          });
                        });
                      setTimeout(() => {
                        this.authenticationService.logout();
                        this.router.navigate(["./login"], {
                          skipLocationChange: true,
                        });
                      }, 2000);
                    } else {
                      this.translate
                        .get(res["message"])
                        .subscribe((res: string) => {
                          this.messageService.add({
                            severity: "error",
                            detail: res,
                          });
                        });
                    }
                  });
              },
              reject: () => {
                value["userName"] = this.userName;
                this.profileService
                  .updateProfileData(value)
                  .subscribe((res) => {
                    if (res["status"]) {
                      this.translate
                        .get(res["message"])
                        .subscribe((res: string) => {
                          this.messageService.add({
                            severity: "success",
                            detail: res,
                          });
                        });
                      this.onLoadData();
                    } else {
                      this.translate
                        .get(res["message"])
                        .subscribe((res: string) => {
                          this.messageService.add({
                            severity: "error",
                            detail: res,
                          });
                        });
                      this.onLoadData();
                    }
                  });
              },
            });
          });
      } else {
        this.isLoaded = true;
        value["userName"] = this.userName;
        this.profileService.updateProfileData(value).subscribe((res) => {
          if (res["status"]) {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.onLoadData();
            this.isLoaded = false;
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
            this.onLoadData();
            this.isLoaded = false;
          }
        });
      }
    }
  }
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onReset() {
    this.userForm.patchValue(this.initialData);
  }
}
