import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { first } from "rxjs/operators";
import { AuthenticationService } from "../_services/authentication.service";
import { MessageService, SelectItem, ConfirmationService } from "primeng/api";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { User } from "../_models/user";
import { RoleService } from "../_services/role.service";
import { Subscription } from "rxjs";
import { TranslateService } from "@ngx-translate/core";
import { UtilService } from "../_services/util.service";
import { SettingsService as SettingsService1 } from "../_services/settings.service";
import { GlobalDataService } from "../_services/global-data.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
  providers: [MessageService],
})
export class LoginComponent implements OnInit, OnDestroy {
  user: User;
  userform: FormGroup;
  submitted: boolean;
  genders: SelectItem[];
  description: string;
  subscription: Subscription;
  imagePath: any;
  imageName: any = "";
  isLoaded: boolean = true;
  gotColor: boolean = false;
  companyStatus: Number = 1;
  statusMsg: String = "";
  statusComment: String = "";

  constructor(
    private authenticationService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private fb: FormBuilder,
    private roleService: RoleService,
    private translate: TranslateService,
    private settingsService: SettingsService1,
    private util: UtilService,
    private globalService: GlobalDataService
  ) {}

  ngOnInit() {
    this.globalService.getGlobalData({}).subscribe((res) => {
      this.translate.use(res["globalData"]["GLOBALLANGUAGE"]);
      localStorage.setItem("globalData", JSON.stringify(res["globalData"]));
      if (res["globalData"]["CUSTOMCOLOR"].indexOf("#") != -1) {
        this.util.themeChange(
          this.util.hexToHSL(res["globalData"]["CUSTOMCOLOR"])
        );
        this.gotColor = true;
      } else {
        this.util.themeChange(res["globalData"]["CUSTOMCOLOR"]);
        this.gotColor = true;
      }
      if (res["globalData"]["FONTCOLOR"].indexOf("#") != -1) {
        this.util.fontChange(
          this.util.hexToHSL(res["globalData"]["FONTCOLOR"])
        );
      } else {
        this.util.fontChange(res["globalData"]["FONTCOLOR"]);
      }

      if (res["globalData"]["COMPANYLOGO"]) {
        this.imageName = res["globalData"]["COMPANYLOGO"];
        this.imagePath = res["uploadUrl"] + res["globalData"]["COMPANYID"];
      } else {
        this.imageName = "logo_dupont.png";
        // this.imagePath = res["uploadUrl"] + res["globalData"]["COMPANYID"];
        this.imagePath = "/assets/images";
      }

      this.companyStatus = res["globalData"]["STATUS"];
      if (this.companyStatus == 0) {
        this.statusMsg = "Your DataPro application is Inactive.";
      } else if (this.companyStatus == -1) {
        this.statusMsg = "Your DataPro application is Under Maintenance.";
        this.statusComment = res["globalData"]["MESSAGE"];
      } else if (this.companyStatus == -3) {
        this.statusMsg = "Your DataPro application license has expired.";
      }
    });

    this.buildForm();
  }

  buildForm() {
    this.userform = this.fb.group({
      username: new FormControl("", [
        Validators.required,
        Validators.pattern(
          "^(?=.*[a-zA-Zd0-9@#$%&*!^].*)[a-zA-Z0-9d!^)-_(@#$%&*]{1,}$"
        ),
      ]),
      password: new FormControl("", Validators.required),
    });
  }

  get uform() {
    return this.userform.controls;
  }

  onSubmit(value: string) {
    this.isLoaded = false;
    this.user = eval(value);

    this.subscription = this.authenticationService
      .login(this.user.username, this.user.password)
      .pipe(first())
      .subscribe((data) => {
        if (data.status) {
          let currentUser = this.authenticationService.UserInfo;
          var jsonData = {
            languageId: currentUser["languageId"],
            labelType: 1,
          };
          this.settingsService
            .getConfigurationTranslations(jsonData)
            .subscribe((res) => {
              this.setTranslation(res);
            });
          if (currentUser["alertMessageFlag"] == "0") {
            //this.translate.get("ALTLOCKED").subscribe((res) => {
            this.confirmationService.confirm({
              message: currentUser["alertMessage"],
              accept: () => {
                var param = { userId: currentUser["userId"] };
                this.authenticationService
                  .updateUserAlert(param)
                  .subscribe((res) => {
                    if (res["status"] == true) {
                      this.router.navigate(["/dashboard"], {
                        skipLocationChange: true,
                      });
                    }
                  });
              },
              acceptLabel: "Got It",
              reject: () => {
                this.router.navigate(["/dashboard"], {
                  skipLocationChange: true,
                });
              },
              rejectLabel: "Cancel",
              //rejectVisible:false,
              header: "Notification",
            });
            //});
          } else {
            this.settingsService.getGeneralData().subscribe((res) => {
              if (res["status"] == true) {
                var generalData = res["GeneralData"];

                // set custom color
                if (generalData["CustomColor"].length > 7) {
                  this.util.themeChange(generalData["CustomColor"]);
                  localStorage.setItem(
                    "CustomColor",
                    generalData["CustomColor"]
                  );
                } else {
                  this.util.themeChange(
                    this.util.hexToHSL(generalData["CustomColor"])
                  );
                  localStorage.setItem(
                    "CustomColor",
                    this.util.hexToHSL(generalData["CustomColor"])
                  );
                }
                //Set custom font
                if (generalData["CustomFont"].length > 7) {
                  this.util.fontChange(generalData["CustomFont"]);
                  localStorage.setItem("CustomFont", generalData["CustomFont"]);
                } else {
                  this.util.fontChange(
                    this.util.hexToHSL(generalData["CustomFont"])
                  );
                  localStorage.setItem(
                    "CustomFont",
                    this.util.hexToHSL(generalData["CustomFont"])
                  );
                }
                localStorage.setItem(
                  "AutoLogoutInterval",
                  generalData["AutoLogout"]
                );
              }
              if (
                currentUser["firstLogin"] == 0 ||
                currentUser["passwordExpired"] == true
              ) {
                this.router.navigate(["/forcePasswordChange"], {
                  skipLocationChange: true,
                });
              } else {
                this.isLoaded = true;
                this.router.navigate(["/dashboard"], {
                  skipLocationChange: true,
                });
              }
            });
          }
        } else {
          const messages = data.message.split(" ");
          if (messages.length === 1) {
            if (messages[0] == "ALTLOCKED") {
              this.translate.get("ALTLOCKED").subscribe((res) => {
                this.confirmationService.confirm({
                  message: res,
                  accept: () => {
                    this.router.navigate(["/forgotPassword"], {
                      skipLocationChange: true,
                    });
                  },
                  reject: () => {
                    this.router.navigate(["/login"], {
                      skipLocationChange: true,
                    });
                  },
                });
              });
            } else {
              this.translate.get(data.message).subscribe((translation) => {
                this.router.navigate(["/login"], {
                  skipLocationChange: true,
                });
                this.messageService.add({
                  severity: "error",
                  detail: translation,
                });
              });
            }
          } else {
            this.translate
              .get([messages[0], messages[2]])
              .subscribe((translation) => {
                this.router.navigate(["/login"], {
                  skipLocationChange: true,
                });
                this.messageService.add({
                  severity: "error",
                  detail: `${translation[messages[0]]} ${messages[1]} ${
                    translation[messages[2]]
                  }`,
                });
              });
          }
          this.isLoaded = true;
        }
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  setTranslation(data) {
    let translationObj = {};
    data["label"].forEach((item) => {
      translationObj[item.LABELNAME] = item.LABELVALUE;
    });

    // let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    let currentUser = this.authenticationService.UserInfo;
    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang("en-us");
    if (currentUser && currentUser.languageCode) {
      // the lang to use, if the lang isn't available, it will use the current loader to get them
      localStorage.setItem("translationObj", JSON.stringify(translationObj));
      this.translate.use(currentUser.languageCode);
      this.translate.setTranslation(
        currentUser.languageCode,
        translationObj,
        true
      );
    }
  }

  getCustomColor() {
    this.settingsService.getGeneralData().subscribe((res) => {});
  }
  bookmarkThisPage() {
    alert("Please Press CTRL+D to bookmark this page");
  }
}
