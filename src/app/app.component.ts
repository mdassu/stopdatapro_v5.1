import { Component, OnInit, HostListener } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import cssVars from "css-vars-ponyfill";
import { SettingsService } from "./_services/settings.service";
import { UtilService } from "./_services/util.service";
import { Router, NavigationStart } from "@angular/router";
import { AuthenticationService } from "./_services/authentication.service";
import { environment } from "../environments/environment";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
})
export class AppComponent implements OnInit {
  // @HostListener("window:beforeunload", ["$event"])
  // clearLocalStorage(event) {
  //   localStorage.clear();
  // }
  imageName: any;
  location: Location;
  constructor(
    private translate: TranslateService,
    private util: UtilService,
    private router: Router,
    private auth: AuthenticationService
  ) {
    // let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    let currentUser = this.auth.UserInfo;
    let isForcePasswordPage = localStorage.getItem("force-password-change");
    if (currentUser) {
      if (isForcePasswordPage == "true") {
        this.router.navigate(["/"]);
        this.router.navigate(["forcePasswordChange"], {
          skipLocationChange: true,
        });
      } else {
        this.auth.logout();
        this.router.navigate(["/"]);
        this.router.navigate(["login"], { skipLocationChange: true });
      }
    } else {
      this.router.navigate(["/"]);
      this.router.navigate(["login"], { skipLocationChange: true });
    }
  }

  ngOnInit(): void {
    if (environment.production) {
      if (location.protocol === "http:") {
        window.location.href = location.href.replace("http", "https");
      }
    }
    let customColor = localStorage.getItem("CustomColor");
    let customFont = localStorage.getItem("customFont");
    if (customColor) {
      this.util.themeChange(customColor);
    } else {
      this.util.themeChange(this.util.defaultBrandColor);
    }

    if (customFont) {
      this.util.fontChange(customFont);
    } else {
      this.util.fontChange(this.util.defaultFontColor);
    }

    // let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    let currentUser = this.auth.UserInfo;
    let translationObj = JSON.parse(localStorage.getItem("translationObj"));
    if (currentUser && currentUser.languageCode && translationObj) {
      this.translate.use(currentUser.languageCode);
      this.translate
        .getTranslation(currentUser.languageCode)
        .subscribe((res) => {
          this.translate.setTranslation(
            currentUser.languageCode,
            translationObj,
            true
          );
        });
    } else {
      this.translate.use("en-us");
    }
  }
}
