import { Directive, Host } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Calendar } from "primeng/calendar";

@Directive({
  selector: "[calendarLocale]",
})
export class PCalendarLocaleDirective {
  // This is the correct way but it is not working for now, use this after the bug fixed.
  // @HostBinding('locale')

  // This way, injecting the component by @Host, is a workaround, this is an open issue in angular: https://github.com/angular/angular/issues/13776
  constructor(
    @Host() private pCalendar: Calendar,
    private translate: TranslateService
  ) {
    this.translate
      .get([
        "FMKSUNDAY",
        "FMKMONDAY",
        "FMKTUESDAY",
        "FMKWEDNESDAY",
        "FMKTHURSDAY",
        "FMKFRIDAY",
        "FMKSATURDAY",
        "FMKSUN",
        "FMKMON",
        "FMKTUE",
        "FMKWED",
        "FMKTHU",
        "FMKFRI",
        "FMKSAT",
        "FMKSU",
        "FMKMO",
        "FMKTU",
        "FMKWE",
        "FMKTH",
        "FMKFR",
        "FMKSA",
        "FMKJANUARY",
        "FMKFEBRUARY",
        "FMKMARCH",
        "FMKAPRIL",
        "FMKMAY",
        "FMKJUNE",
        "FMKJULY",
        "FMKAUGUST",
        "FMKSEPTEMBER",
        "FMKOCTOBER",
        "FMKNOVEMBER",
        "FMKDECEMBER",
        "FMKJAN",
        "FMKFEB",
        "FMKMAR",
        "FMKAPR",
        "FMKMAY",
        "FMKJUN",
        "FMKJUL",
        "FMKAUG",
        "FMKSEP",
        "FMKOCT",
        "FMKNOV",
        "FMKDEC",
        "FMKCURRENTTEXT",
        "FMKCLR",
      ])
      .subscribe((resLabel) => {
        var locale = {
          firstDayOfWeek: 0,
          dayNames: [
            resLabel["FMKSUNDAY"],
            resLabel["FMKMONDAY"],
            resLabel["FMKTUESDAY"],
            resLabel["FMKWEDNESDAY"],
            resLabel["FMKTHURSDAY"],
            resLabel["FMKFRIDAY"],
            resLabel["FMKSATURDAY"],
          ],
          dayNamesShort: [
            resLabel["FMKSUN"],
            resLabel["FMKMON"],
            resLabel["FMKTUE"],
            resLabel["FMKWED"],
            resLabel["FMKTHU"],
            resLabel["FMKFRI"],
            resLabel["FMKSAT"],
          ],
          dayNamesMin: [
            resLabel["FMKSU"],
            resLabel["FMKMO"],
            resLabel["FMKTU"],
            resLabel["FMKWE"],
            resLabel["FMKTH"],
            resLabel["FMKFR"],
            resLabel["FMKSA"],
          ],
          monthNames: [
            resLabel["FMKJANUARY"],
            resLabel["FMKFEBRUARY"],
            resLabel["FMKMARCH"],
            resLabel["FMKAPRIL"],
            resLabel["FMKMAY"],
            resLabel["FMKJUNE"],
            resLabel["FMKJULY"],
            resLabel["FMKAUGUST"],
            resLabel["FMKSEPTEMBER"],
            resLabel["FMKOCTOBER"],
            resLabel["FMKNOVEMBER"],
            resLabel["FMKDECEMBER"],
          ],
          monthNamesShort: [
            resLabel["FMKJAN"],
            resLabel["FMKFEB"],
            resLabel["FMKMAR"],
            resLabel["FMKAPR"],
            resLabel["FMKMAY"],
            resLabel["FMKJUN"],
            resLabel["FMKJUL"],
            resLabel["FMKAUG"],
            resLabel["FMKSEP"],
            resLabel["FMKOCT"],
            resLabel["FMKNOV"],
            resLabel["FMKDEC"],
          ],
          today: resLabel["FMKCURRENTTEXT"],
          clear: resLabel["FMKCLR"],
        };
        this.pCalendar.locale = locale;
      });
  }
}
