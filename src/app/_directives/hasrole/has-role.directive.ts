import { Directive, Input, ViewContainerRef, TemplateRef } from "@angular/core";
import { Subject } from "rxjs";
import { RoleService } from "src/app/_services/role.service";

@Directive({
  selector: "[appHasRole]"
})
export class HasRoleDirective {
  // the role the user must have
  @Input() appHasRole: Array<String>;

  stop$ = new Subject();

  isVisible = false;

  constructor(
    private viewContainerRef: ViewContainerRef,
    private templateRef: TemplateRef<any>,
    private roleService: RoleService
  ) {}

  ngOnInit() {
    if (this.appHasRole.includes(this.roleService.getRole())) {
      // If it is already visible (which can happen if
      // his roles changed) we do not need to add it a second time
      if (!this.isVisible) {
        // We update the `isVisible` property and add the
        // templateRef to the view using the
        // 'createEmbeddedView' method of the viewContainerRef
        this.isVisible = true;
        this.viewContainerRef.createEmbeddedView(this.templateRef);
      }
    } else {
      // If the user does not have the role,
      // we update the `isVisible` property and clear
      // the contents of the viewContainerRef
      this.isVisible = false;
      this.viewContainerRef.clear();
    }
  }
}
