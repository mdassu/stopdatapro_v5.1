import {
  Directive,
  OnInit,
  ViewContainerRef,
  TemplateRef
} from "@angular/core";
import { AuthenticationService } from "../_services/authentication.service";

@Directive({
  selector: "[visibleToOnlySuperAdmin]"
})
export class VisibleToOnlySuperAdminDirective implements OnInit {
  constructor(
    private viewContainerRef: ViewContainerRef,
    private templateRef: TemplateRef<any>,
    private auth: AuthenticationService
  ) {}

  ngOnInit(): void {
    // const currentUser = JSON.parse(localStorage.getItem("currentUser"));
    const currentUser = this.auth.UserInfo;
    if (currentUser.roleId && currentUser.roleId === 1) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainerRef.clear();
    }
  }
}
