import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { SelectItem } from "primeng/primeng";
import { Subscription } from "rxjs";
import { ChecklistService } from "src/app/_services/checklist.service";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

@Component({
  selector: "app-add-checklist-setup",
  templateUrl: "./add-checklist-setup.component.html",
  styleUrls: ["./add-checklist-setup.component.css"],
  providers: [MessageService, CustomValidatorsService],
})
export class AddChecklistSetupComponent implements OnInit {
  addGeneral: FormGroup;
  siteStatus = true;
  addAssignment: FormGroup;
  submitted: boolean;
  submitted2: boolean;
  radioValue: string;
  status: Array<SelectItem>;
  GroupType: Array<SelectItem>;
  gType: Array<SelectItem>;
  Groupname: Array<SelectItem>;
  ChangeStatus: Array<SelectItem>;
  generalOptions: Array<SelectItem>;
  formgroupname: any;
  checklistData: Array<any>;
  checklistId: number;
  usersname: any;
  id: number;
  subscription: Subscription;
  initialFormData: any;
  isLoading: boolean = true;

  constructor(
    private readonly breadcrumbService: BreadcrumbService,
    private readonly checklistService: ChecklistService,
    private readonly fb: FormBuilder,
    private readonly messageService: MessageService,
    private customValidatorsService: CustomValidatorsService,
    private readonly router: Router,
    private translate: TranslateService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLCHECKLISTCONFIG",
        url: "./assets/help/add-checklist-setup.md",
      },
      { label: "LBLCHKLISTSETUP", routerLink: ["/checklist-setup-listing"] },
    ]);
  }

  ngOnInit() {
    this.checklistService.getChecklistData().subscribe((res) => {
      this.translate.get("LBLNONE").subscribe((restran: string) => {
        this.checklistData = res["checkList"];
        this.checklistData.push({
          CHECKLISTSETUPID: 0,
          "Checklist Setup Name": restran,
          STATUS: "LBLACTIVE",
        });
      });
      this.initialFormData = this.addGeneral.getRawValue();
      this.isLoading = false;
    });
    this.generalOptions = [];
    this.status = [];
    this.translate
      .get(["LBLYES", "LBLNO", "LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: Object) => {
        const generalOptions = [];
        const status = [];
        generalOptions.push({ label: res["LBLYES"], value: 1 });
        generalOptions.push({ label: res["LBLNO"], value: 0 });
        this.generalOptions = generalOptions;
        status.push({ label: res["LBLACTIVE"], value: 1 });
        status.push({ label: res["LBLINACTIVE"], value: 0 });
        this.status = status;
      });

    this.addGeneral = this.fb.group({
      checkListName: new FormControl("", [
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      status: new FormControl(1),
      copyFrom: new FormControl(""),
      allSafeCheck: new FormControl(0),
      allSites: new FormControl(0),
      enableSafeUnsafe: new FormControl(0),
    });
    this.addGeneral.patchValue({
      copyFrom: 0,
    });
  }

  get uform() {
    return this.addGeneral.controls;
  }

  onSubmit(value) {
    this.submitted = true;
    if (this.addGeneral.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.addGeneral.value["checkListName"] = this.addGeneral.value[
        "checkListName"
      ].trim();
      this.subscription = this.checklistService
        .insertGeneralData(this.addGeneral.value)
        .subscribe((res) => {
          if (res["status"] === true) {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            setTimeout(() => {
              this.router.navigate(["/checklist-edit/", res["id"]], {
                skipLocationChange: true,
              });
            }, 1000);
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
          }
        });
    }
  }

  onReset() {
    this.addGeneral.patchValue(this.initialFormData);
  }
}
