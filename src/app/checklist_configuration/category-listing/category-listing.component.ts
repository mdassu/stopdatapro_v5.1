import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { SelectItem } from "primeng/primeng";
import { FormGroup } from "@angular/forms";
import { ConfirmationService, MessageService } from "primeng/api";
import { EnvService } from "src/env.service";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { ChecklistService } from "src/app/_services/checklist.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";

@Component({
  selector: "app-category-listing",
  templateUrl: "./category-listing.component.html",
  styleUrls: ["./category-listing.component.css"],
  providers: [ConfirmationService, MessageService],
})
export class CategoryListingComponent implements OnInit {
  //  Variable Declaration
  filterForm: FormGroup;
  public show_dialog = false;
  public show_advance_search = false;
  public button_name: any = "Show Login Form!";
  cities1: Array<SelectItem>;
  cols: Array<any>;
  cities2: Array<SelectItem>;
  colsCount: any;
  cars1: any;
  userlevel: any;
  allCategory: any;
  dataCols: Array<any>;
  catListing: any;
  dataCount: any;
  filterCat: Array<any>;
  selectedfilterCat = "MAINCATEGORYNAME";
  searchText: any;
  visibleTable = true;
  selectedData: any = [];
  isLoaded: boolean;
  confirmClass: any;
  appName: any;
  roleId: any;

  // Breadcrumb Service
  constructor(
    private readonly breadcrumbService: BreadcrumbService,
    private readonly confirmationService: ConfirmationService,
    private readonly messageService: MessageService,
    private readonly checklistservice: ChecklistService,
    private cookieService: CookieService,
    private translate: TranslateService,
    private env: EnvService,
    private router: Router,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLCATEGORIESLIST", url: "./assets/help/category-listing.md" },
      { label: "LBLCATEGORIES", routerLink: ["/categories-listing"] },
    ]);
  }

  ngOnInit() {
    // get Category
    this.roleId = this.auth.UserInfo["roleId"];
    this.getCategory();
    this.translate
      .get(["LBLMAINCATEGORY", "LBLSTATUS"])
      .subscribe((res: Object) => {
        this.dataCols = [
          { field: "MAINCATEGORYNAME", header: res["LBLMAINCATEGORY"] },
          { field: "STATUS", header: res["LBLSTATUS"] },
        ];
        //  col of status
        this.dataCount = this.dataCols.length;

        this.filterCat = [];
        this.filterCat.push({
          label: res["LBLMAINCATEGORY"],
          value: "MAINCATEGORYNAME",
        });
      });
    this.appName = this.env.appName;
  }

  getCategory() {
    this.checklistservice.getCategory().subscribe((res) => {
      if (res["status"] === true) {
        this.allCategory = res["GeneralData"];
        this.catListing = res["GeneralData"];

        this.cookieService.set(
          "mainCategoryId",
          this.catListing[0]["MAINCATEGORYID"]
        );
      }
      this.isLoaded = true;
    });
  }

  deleteRecord() {
    this.confirmClass = "warning-msg";
    this.translate.get("ALTDELCATEGORYWARNING").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          this.checklistservice
            .deleteCategory({
              mainCategoryId: Array.prototype.map
                .call(this.selectedData, (s) => s.MAINCATEGORYID)
                .toString(),
            })
            .subscribe((res) => {
              this.selectedData = [];
              this.getCategory();
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedData = [];
          this.searchText = "";
          this.getCategory();
        },
      });
    });
  }

  changeStatus(id) {
    this.confirmClass = "info-msg";
    event.stopPropagation();
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          this.checklistservice
            .updateCategoryStatus({
              mainCategoryId: id
                ? id
                : Array.prototype.map
                    .call(this.selectedData, (s) => s.MAINCATEGORYID)
                    .toString(),
            })
            .subscribe((res) => {
              this.selectedData = [];
              this.getCategory();
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            });
        },
        reject: () => {
          this.selectedData = [];
          this.searchText = "";
          this.getCategory();
        },
      });
    });
  }

  onTextSearch() {
    const field = this.selectedfilterCat;
    const stext = this.searchText.toLowerCase();
    const fn = this;
    this.visibleTable = false;

    if (stext != "") {
      var newList = this.allCategory.filter((item) => {
        return item[field].toLowerCase().indexOf(stext) >= 0;
      });
      setTimeout(function () {
        fn.visibleTable = true;
      }, 10);
      this.catListing = newList;
    } else {
      setTimeout(function () {
        fn.visibleTable = true;
      }, 10);
      this.catListing = this.allCategory;
    }
  }

  onRowSelect(data): void {
    this.router.navigate(["/edit-categories", data.MAINCATEGORYID], {
      skipLocationChange: true,
    });
  }
}
