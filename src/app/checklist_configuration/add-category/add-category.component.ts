import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { ConfirmationService, MessageService } from "primeng/api";
import { SelectItem } from "primeng/primeng";
import { CategoryEditService } from "src/app/_services/category-edit.service";
import { BreadcrumbService } from "../../breadcrumb.service";
import { CustomValidatorsService } from "../../_services/custom-validators.service";
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: "app-add-category",
  templateUrl: "./add-category.component.html",
  styleUrls: ["./add-category.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class AddCategoryComponent implements OnInit {
  addCategoryGeneral: FormGroup;
  submitted: boolean;
  status: Array<SelectItem>;
  cols: Array<any>;
  categoryNameMaxLength: number = 128;
  isLoading: boolean = true;
  initialGeneralFormData: any;

  constructor(
    private readonly breadcrumbService: BreadcrumbService,
    private readonly fb: FormBuilder,
    private readonly messageService: MessageService,
    private readonly categoryEditService: CategoryEditService,
    private readonly route: Router,
    private customValidatorsService: CustomValidatorsService,
    private translate: TranslateService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLCHECKLISTCONFIG", url: "./assets/help/add-category.md" },
      {
        label: "LBLCATEGORIES",
        routerLink: ["/categories-listing"],
      },
    ]);
  }

  ngOnInit() {
    this.status = [];
    this.translate
      .get(["LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: Object) => {
        this.status.push({ label: res["LBLACTIVE"], value: 1 });
        this.status.push({ label: res["LBLINACTIVE"], value: 0 });
      });

    this.addCategoryGeneral = this.fb.group({
      mainCategoryName: new FormControl("", [
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.notStartsWithSpace,
        this.customValidatorsService.maxLengthValidator(
          this.categoryNameMaxLength
        ),
      ]),
      status: new FormControl(1, Validators.required),
    });
    this.initialGeneralFormData = this.addCategoryGeneral.getRawValue();
    this.isLoading = false;
  }

  get uform() {
    return this.addCategoryGeneral.controls;
  }

  onSubmit(value: string) {
    this.submitted = true;
    if (this.addCategoryGeneral.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      const body = this.addCategoryGeneral.getRawValue();
      this.categoryEditService.addCategory(body).subscribe((res) => {
        if (res["status"] == true) {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "success",
              detail: res,
            });
          });
          setTimeout(() => {
            this.route.navigate(["/edit-categories", res["id"]], {
              skipLocationChange: true,
            });
          }, 1000);
        } else {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              detail: res,
            });
          });
        }
      });
    }
  }

  onReset() {
    this.addCategoryGeneral.patchValue(this.initialGeneralFormData);
  }
}
