import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Router, RoutesRecognized } from "@angular/router";
import { ConfirmationService, MessageService } from "primeng/api";
import { SelectItem } from "primeng/primeng";
import { Item } from "src/app/_models/item";
import { UserFilters } from "src/app/_models/userFilter";
import { ChecklistService } from "src/app/_services/checklist.service";
import { BreadcrumbService } from "../../breadcrumb.service";
import { SlideUpDownAnimations } from "../../_animations/slide-up-down.animations";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
import { Table } from "primeng/table";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { pairwise, filter } from "rxjs/operators";
@Component({
  selector: "app-checklist-setup-listing",
  templateUrl: "./checklist-setup-listing.component.html",
  styleUrls: ["./checklist-setup-listing.component.css"],
  providers: [ConfirmationService, MessageService],
  animations: [SlideUpDownAnimations],
})
export class ChecklistSetupListingComponent implements OnInit {
  //  Variable Declaration
  filterForm: FormGroup;
  visibleTable: boolean = true;
  public show_dialog: boolean = false;
  public show_advance_search: boolean = false;
  public button_name: any = "Show Login Form!";
  cities1: Array<SelectItem>;
  cities2: Array<SelectItem>;
  userlevel: any;
  cols: Array<object>;
  checklistCount: any;
  checklists: [];
  selectedRowId: Array<UserFilters>;
  checklistSearch: Array<any>;
  searchInput: string;
  selectedChecklist: any;
  filterScreen = false;
  selectedData: any = [];
  sites: [];
  chkstatus: [];
  filterData: any;
  optionalFilter: any;
  isLoaded: boolean;
  selectedFilter: any = [];
  animationState = "out";
  confirmClass: any;
  appName: any;
  parameters: any;
  previousUrl: string;
  userId: any;
  roleId: any;

  @ViewChild(Table) tableComponent: Table;
  // Breadcrumb Service
  constructor(
    private readonly checklistService: ChecklistService,
    private readonly router: Router,
    private readonly breadcrumbService: BreadcrumbService,
    private readonly confirmationService: ConfirmationService,
    private readonly messageService: MessageService,
    private env: EnvService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLCHECKLISTCONFIG",
        url: "./assets/help/checklist-setup-listing.md",
      },
      {
        label: "LBLCHKLISTSETUP",
        routerLink: ["/checklist-setup-listing"],
      },
    ]);
  }

  ngOnInit() {
    this.userId = this.auth.UserInfo["userId"];
    this.roleId = this.auth.UserInfo["roleId"];
    this.router.events
      .pipe(
        filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((e: any) => {
        var url = e[0].urlAfterRedirects; // previous url
        localStorage.setItem("previousUrl-" + this.userId, url);
      });

    this.translate.get(["LBLSETUP", "LBLSTATUS"]).subscribe((res: Object) => {
      this.cols = [
        { field: "Checklist Setup Name", header: res["LBLSETUP"] },
        { field: "STATUS", header: res["LBLSTATUS"] },
      ];
      this.checklistCount = this.cols.length;
      this.checklistSearch = [];
      this.checklistSearch.push({
        label: res["LBLSETUP"],
        value: "Checklist Setup Name",
      });
      this.selectedChecklist = "Checklist Setup Name";
    });
    this.checkFilter();
    this.appName = this.env.appName;
  }
  toggleFilterShow() {
    this.animationState = this.animationState === "out" ? "in" : "out";
  }

  checkFilter() {
    this.parameters = {};
    this.previousUrl = localStorage.getItem("previousUrl-" + this.userId);
    if (this.previousUrl) {
      if (
        this.previousUrl.search("/add-checklist-setup") != -1 ||
        this.previousUrl.search("/checklist-edit/") != -1
      ) {
        if (localStorage.getItem("filterInfo")) {
          this.toggleFilterShow();
          // this.animationState = "in";
          this.parameters = JSON.parse(localStorage.getItem("filterInfo"));
          var keys = Object.keys(this.parameters);
          keys.map((param) => {
            if (this.parameters[param]) {
              if (param == "status") {
                this.selectedFilter[param + "Id"] = this.parameters[param]
                  .split(",")
                  .map(Number);
              } else {
                this.selectedFilter[param] = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
            }
          });
          localStorage.removeItem("previousUrl-" + this.userId);
        }
      } else {
        localStorage.removeItem("filterInfo");
      }
    }

    this.loadData(this.parameters);
  }

  loadData(data) {
    this.checklistService.getChecklistTableData(data).subscribe((res) => {
      this.tableComponent.reset();
      this.tableComponent.value = [];
      this.checklists = res["checkList"];
      this.sites = res["sites"];
      this.chkstatus = res["checkStatus"];
      this.setFilters(this.sites);
      this.setOptionalFilters(this.chkstatus);
      this.isLoaded = true;
    });
  }

  onTextSearch(tableValue) {
    tableValue.filter(this.searchInput, "Checklist Setup Name", "contains");
    this.isLoaded = true;
  }

  onRowSelect(data): void {
    this.router.navigate(["/checklist-edit", data.CHECKLISTSETUPID], {
      skipLocationChange: true,
    });
  }

  advance_search() {
    this.show_advance_search = !this.show_advance_search;
  }
  deleteRecord() {
    this.confirmClass = "warning-msg";
    if (this.selectedData.length > 1) {
      this.translate.get("ALTNOTMOREONEITEMFORDEL").subscribe((res: string) => {
        this.confirmationService.confirm({
          message: res,
          key: "deleteRecordConfirmation",
          accept: () => {
            this.selectedData.length = 0;
            this.searchInput = "";
            this.loadData({});
          },
          reject: () => {
            this.selectedData = [];
            this.searchInput = "";
            this.loadData({});
          },
        });
      });
    } else {
      let body = {};
      body["checkListSetupId"] = this.selectedData
        .map((x) => x["CHECKLISTSETUPID"])
        .join();
      body["delFlag"] = 0;
      this.checklistService.deleteChecklist(body).subscribe((res) => {
        if (res["delStatus"] === 1) {
          this.translate.get(res["message"]).subscribe((res: string) => {
            this.messageService.add({
              severity: "error",
              key: "custom",
              detail: res,
            });
          });
          this.selectedData.length = 0;
          this.searchInput = "";
          this.loadData({});
        } else {
          this.translate.get("ALTDELCONFIRM").subscribe((res: string) => {
            this.confirmationService.confirm({
              message: res,
              accept: () => {
                let body = {};
                body["checkListSetupId"] = this.selectedData
                  .map((x) => x["CHECKLISTSETUPID"])
                  .join();
                body["delFlag"] = 1;
                this.checklistService.deleteChecklist(body).subscribe((res) => {
                  this.translate
                    .get(res["message"])
                    .subscribe((res: string) => {
                      this.messageService.add({
                        severity: "success",
                        detail: res,
                      });
                    });
                  this.selectedData.length = 0;
                  this.searchInput = "";
                  this.loadData({});
                });
              },
              reject: () => {
                this.selectedData = [];
                this.searchInput = "";
                this.loadData({});
              },
            });
          });
        }
      });
    }
  }

  changeStatus() {
    this.confirmClass = "info-msg";
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          const body = {};
          body["checkListSetupId"] = this.selectedData
            .map((x) => x["CHECKLISTSETUPID"])
            .join();
          this.checklistService.updateChecklistStatus(body).subscribe((res) => {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.searchInput = "";
            this.selectedData = [];
            this.loadData({});
          });
        },
        reject: () => {
          this.selectedData = [];
          this.searchInput = "";
          this.loadData({});
        },
      });
    });
  }

  changeStatusBtn(id) {
    event.stopPropagation();
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          const body = {};
          body["checkListSetupId"] = id;
          this.checklistService.updateChecklistStatus(body).subscribe((res) => {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          });
          this.searchInput = "";
          this.selectedData = [];
          this.loadData({});
        },
      });
    });
  }

  setFilters(filterData) {
    this.filterData = [];
    let filterObj = {};
    filterData.map((site) => {
      site["label"] = site.SITENAME;
      site["value"] = site.SITEID;
    });
    filterObj["Name"] = "LBLBSITES";
    filterObj["Data"] = filterData;
    filterObj["field"] = "siteId";
    this.filterData.push(filterObj);
    if (this.selectedFilter["siteId"] == undefined) {
      this.selectedFilter["siteId"] = [this.auth.UserInfo["defaultSiteId"]];
    }
    if (this.selectedFilter.siteId) {
      this.auth.rearrangeSelect(this.filterData, this.selectedFilter.siteId);
    }
  }

  setOptionalFilters(optionalFilter) {
    this.optionalFilter = [];
    const filterObj = {};
    optionalFilter.map((status) => {
      this.translate.get(status["VALUE"]).subscribe((label) => {
        status["label"] = label;
        status["value"] = status["VALUEID"];
      });
    });
    filterObj["Name"] = "Status";
    filterObj["Data"] = optionalFilter;
    filterObj["field"] = "statusId";
    this.optionalFilter.push(filterObj);
    if (this.selectedFilter.statusId) {
      this.auth.rearrangeSelect(
        this.optionalFilter,
        this.selectedFilter.statusId
      );
    }
  }

  filtering() {
    this.visibleTable = false;
    var selectedFilter = this.selectedFilter;
    var siteId = "";
    var status = "";
    if (selectedFilter.siteId) {
      siteId = selectedFilter.siteId.join();
    }
    if (selectedFilter.statusId) {
      status = selectedFilter.statusId.join(",");
    }

    var data = {
      siteId: siteId,
      status: status,
    };
    localStorage.removeItem("filterInfo");
    localStorage.setItem("filterInfo", JSON.stringify(data));
    this.loadData(data);
  }
  clearFilter(tableValue) {
    const defaultValue = {
      siteId: "",
      status: "",
    };
    this.selectedFilter = defaultValue;
    this.visibleTable = false;
    this.searchInput = "";
    this.loadData({});
    this.onTextSearch(tableValue);
  }

  onSelectAll(i) {
    const selected = this.filterData[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.filterData[i]["field"]] = selected;
  }

  onClearAll(i) {
    this.selectedFilter[this.filterData[i]["field"]] = [];
  }
  onSelectAllAdv(i) {
    const selected = this.optionalFilter[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.optionalFilter[i]["field"]] = selected;
  }

  onClearAllAdv(i) {
    this.selectedFilter[this.optionalFilter[i]["field"]] = [];
  }
}
