import { Component, OnInit } from "@angular/core";
import { ChecklistService } from "src/app/_services/checklist.service";
import { MessageService } from "primeng/api";
import { BreadcrumbService } from "src/app/breadcrumb.service";

@Component({
  selector: "app-category-translations",
  templateUrl: "./category-translations.component.html",
  styleUrls: ["./category-translations.component.css"]
})
export class CategoryTranslationsComponent implements OnInit {
  constructor(
    private breadcrumbService: BreadcrumbService,
    private messageService: MessageService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLCHECKLISTCONFIG",
        url: "./assets/help/category-translate.md"
      },
      { label: "LBLCATEGORIES", routerLink: ["/categories-listing"] }
    ]);
  }

  ngOnInit() { }
}
