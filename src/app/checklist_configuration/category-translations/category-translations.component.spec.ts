import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryTranslationsComponent } from './category-translations.component';

describe('CategoryTranslationsComponent', () => {
  let component: CategoryTranslationsComponent;
  let fixture: ComponentFixture<CategoryTranslationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryTranslationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryTranslationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
