import { Component, OnInit, ViewChild } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { SelectItem, ConfirmationService } from "primeng/primeng";
import { Subscription } from "rxjs";
import {
  MainCategories,
  Sites,
  SubCatagories,
} from "src/app/_helpers/interface-items";
import { GeneralControls } from "src/app/_models/checklist";
import { ChecklistService } from "src/app/_services/checklist.service";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TranslateService } from "@ngx-translate/core";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";
import { EnvService } from "src/env.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
// import { $ } from "protractor";

declare var $: any;
@Component({
  selector: "app-edit-checklist-setup",
  templateUrl: "./edit-checklist-setup.component.html",
  styleUrls: ["./edit-checklist-setup.component.css"],
  providers: [CustomValidatorsService],
})
export class EditChecklistSetupComponent implements OnInit {
  addGeneral: FormGroup;
  addSubCategory: FormGroup;
  checklistSetupName: string;
  mainCategory: Array<Number> = [];
  checklistData: Array<any>;
  mainCategoryPreview: Array<any>;
  subCategoryPreview: [];
  checkListId: number;
  subCategoriesDropdown: Array<MainCategories>;
  subCategoriesSelectorDropdown: Array<SubCatagories>;
  siteData: Array<Sites>;
  generalOptions = [];
  status = [];
  subCategories: Array<SubCatagories>;
  selectedSubCategories: Array<SubCatagories>;
  subCategorySelector: number;
  updateMainCategoryData = {};
  enableSites: boolean;
  updateSubCategoryData = {};
  selectedSiteIDs = [];
  updateSitesData = {};
  updatePreviewData = {};
  mainCategoryItems: any = [];
  subscription: Subscription;
  id: number;
  general: GeneralControls = new GeneralControls();
  submitted: boolean;
  loaded: boolean;
  copyFromValueArray = [];
  loginData = {};
  previewDisplay: boolean;
  display: boolean;
  index = 0;
  lastIndex = 0;
  appName: any;
  isMainCategoriesDropdownChanged: boolean;
  isSubCategoriesDropdownChanged: boolean;
  isSiteDataChanged: boolean;
  initData: any;
  mainCategoryDisable: boolean = true;
  subCategoryDisable: boolean = true;
  siteDisable: boolean = true;
  isLoading: boolean = true;
  copyFrom = "LBLNONE";
  generalLoading: boolean = false;
  mainCatLoading: boolean = false;
  subCatLoading: boolean = false;
  siteLoading: boolean = false;
  mainCategoriesDropdownDrop: any = [];
  subCategoriesSelectorDropdownDr: any = [];
  siteDataDrop: any = [];
  sourceMainCat: any = [];
  targetMainCat: any = [];
  sourceSubCat: any = [];
  targetSubCat: any = [];
  tempMainCat: any;

  @ViewChild("p-pickList") pl: Element;

  constructor(
    private fb: FormBuilder,
    private readonly checklistService: ChecklistService,
    private readonly breadcrumbService: BreadcrumbService,
    private readonly activetedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly messageService: MessageService,
    private customValidatorsService: CustomValidatorsService,
    private translate: TranslateService,
    private env: EnvService,
    private confirmationService: ConfirmationService,
    private auth: AuthenticationService
  ) {
    this.activetedRoute.params.subscribe((params) => {
      this.checkListId = params["id"];
    });
    this.breadcrumbService.setItems([
      {
        label: "LBLCHECKLISTCONFIG",
        url: "./assets/help/edit-checklist-setup-general.md",
      },
      { label: "LBLCHKLISTSETUP", routerLink: ["/checklist-setup-listing"] },
    ]);
    this.subscription = this.activetedRoute.queryParamMap.subscribe(
      (params) => {
        this.id = +params.get("id");
      }
    );
  }

  ngOnInit() {
    this.buildGeneralTab(this.general);
    const optionsArr = [];
    const statusArr = [];
    // this.loginData = JSON.parse(localStorage.getItem("currentUser"));
    this.loginData = this.auth.UserInfo;
    this.translate
      .get(["LBLYES", "LBLNO", "LBLACTIVE", "LBLINACTIVE"])
      .subscribe((res: object) => {
        optionsArr.push({ label: res["LBLYES"], value: 1 });
        optionsArr.push({ label: res["LBLNO"], value: 0 });
        statusArr.push({ label: res["LBLACTIVE"], value: 1 });
        statusArr.push({ label: res["LBLINACTIVE"], value: 0 });
        this.generalOptions = optionsArr;
        this.status = statusArr;
      });
    this.loadCopyFrom();
    this.refreshData();
    this.appName = this.env.appName;
  }

  loadCopyFrom() {
    let arr = [];
    let searchStr;
    this.checklistService
      .getChecklistSetupData({ checkListId: this.checkListId })
      .subscribe((res) => {
        searchStr = res["checkListData"]["COPYFROM"];
        this.checklistService.getChecklistTableData({}).subscribe((res) => {
          arr = res["checkList"];
          arr.forEach((x) => {
            if (x["Checklist Setup Name"] == searchStr) {
              this.copyFromValueArray.push(x);
            }
            if (searchStr == "LBLNONE") {
              this.copyFromValueArray.push({
                CHECKLISTSETUPID: 0,
                "Checklist Setup Name": "LBLNONE",
                STATUS: "LBLACTIVE",
              });
            }
          });
        });
        this.general = res["checkListData"];
        this.copyFrom = res["checkListData"]["COPYFROM"];
        this.buildGeneralTab(this.general);
        if (this.general.ALLSITES == 1) {
          this.enableSites = false;
        }
        if (this.general.ALLSITES == 0) {
          this.enableSites = true;
        }
      });
  }

  buildGeneralTab(general: GeneralControls) {
    this.addGeneral = this.fb.group({
      checklistId: [this.checkListId],
      checkListName: new FormControl(
        general.checkListName || general.SETUPNAME,
        [
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
        ]
      ),
      allSites: [general.allSites || general.ALLSITES],
      status: [general.status || general.STATUS],
      allSafeCheck: [general.allSafeCheck || general.ALLSAFECHECK],
      enableSafeUnsafe: [general.enableSafeUnsafe || general.ENABLESAFEUNSAFE],
      copyFrom: new FormControl({
        value: general.COPYFROM,
        disabled: true,
      }),
    });
  }

  get uform() {
    return this.addGeneral.controls;
  }

  refreshData(tabSwitcher = false) {
    this.checklistService
      .getChecklistSetupData({ checkListId: this.checkListId })
      .subscribe((res) => {
        this.initData = res;
        this.buildGeneralTab(res["checkListData"]);
        this.subCategoriesDropdown = res["mainCategoryList"];
        this.mainCategoriesDropdownDrop = [];
        // var mainCatList = res["category"];
        this.sourceMainCat = res["categoryAvailable"];
        this.targetMainCat = res["categoryAssigned"];
        localStorage.setItem(
          "sourceMainCat",
          JSON.stringify(res["categoryAvailable"])
        );
        localStorage.setItem(
          "targetMainCat",
          JSON.stringify(res["categoryAssigned"])
        );

        const catArray = [];
        const siteArray = [];
        this.siteData = res["sites"];
        this.siteDataDrop = [];
        var subCatList = res["sites"];
        subCatList.map((data) => {
          this.siteDataDrop.push({
            label: data["SITENAME"],
            value: data["SITEID"],
          });
        });
        if (res["categoryAssigned"].length == 0) {
          this.mainCategory = [];
          if (tabSwitcher) {
            this.display = true;
          }
        } else {
          this.display = false;
        }
        if (res["sitesId"] === "" || res["sitesId"] === null) {
          const siteArrayVal = res["sites"].find((x) => {
            return x["SITENAME"] == this.loginData["defaultSiteName"];
          });
          if (siteArrayVal) {
            siteArray.push(siteArrayVal["SITEID"]);
            this.selectedSiteIDs = siteArray;
          }
        } else {
          this.selectedSiteIDs = res["sitesId"].split(",").map(Number);
        }
        this.checklistSetupName = res["checkListData"]["SETUPNAME"];
        if (this.tempMainCat) {
          this.subCategorySelector = this.tempMainCat;
          this.tempMainCat = "";
          this.mainCategoryChange();
        } else {
          this.subCategorySelector =
            this.targetMainCat.length > 0
              ? res["selectedCategoryId"]
              : this.subCategorySelector;
          this.sourceSubCat = res["subCategoryAvailable"];
          this.targetSubCat = res["subCategoryAssigned"];
          localStorage.setItem(
            "sourceSubCat",
            JSON.stringify(res["subCategoryAvailable"])
          );
          localStorage.setItem(
            "targetSubCat",
            JSON.stringify(res["subCategoryAssigned"])
          );
        }

        this.subCategoryDisable = true;
        this.mainCategoryPreview = res["mainCategoryPreview"];

        this.subCategoryPreview = res["subCategoryPreview"];

        this.mainCategoryItems = [];
        if (this.mainCategoryPreview) {
          this.mainCategoryPreview.forEach((item) => {
            let obj = {};
            obj["mainCategoryName"] = item["MAINCATEGORYNAME"];
            let subCategoryItems = [];
            this.subCategoryPreview.forEach((item1) => {
              if (item["MAINCATEGORYID"] === item1["MAINCATEGORYID"]) {
                subCategoryItems.push(item1);
              }
            });
            obj["subCategoryItems"] = subCategoryItems;
            this.mainCategoryItems.push(obj);
          });
        }
        this.loaded = true;
        this.isLoading = false;
      });
  }

  mainCategoryChange() {
    this.subCatLoading = true;
    this.subCategoryDisable = false;
    let subCatIdArray = [];
    const subCatArray = [];
    const tempSubCategory = [];
    this.checklistService
      .getSubCatData({
        checkListId: this.checkListId,
        mainCatId: this.subCategorySelector,
      })
      .subscribe((res) => {
        this.sourceSubCat = res["subCategoryAvailable"];
        this.targetSubCat = res["subCategoryAssigned"];
        localStorage.setItem(
          "sourceSubCat",
          JSON.stringify(res["subCategoryAvailable"])
        );
        localStorage.setItem(
          "targetSubCat",
          JSON.stringify(res["subCategoryAssigned"])
        );
        this.subCatLoading = false;
      });
  }

  onUpdateMainCategories() {
    if (this.targetMainCat.length === 0) {
      this.translate.get("ALTASGNCATEGORY").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
    } else {
      this.mainCatLoading = true;
      this.mainCategoryDisable = true;
      var mainCatAssign = [];
      this.targetMainCat.map((item) => {
        mainCatAssign.push(item["MAINCATEGORYID"]);
      });
      this.updateMainCategoryData["mainCategoryId"] = mainCatAssign.join();
      this.updateMainCategoryData["checkListSetupId"] = this.checkListId;
      this.checklistService
        .updateChecklistMainCategories(this.updateMainCategoryData)
        .subscribe((res) => {
          localStorage.removeItem("sourceMainCat");
          localStorage.removeItem("targetMainCat");
          this.isMainCategoriesDropdownChanged = false;
          if (res["status"]) {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.refreshData();
            this.mainCatLoading = false;
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
            this.mainCatLoading = false;
          }
        });
    }
  }

  onUpdateSubCategories() {
    if (this.targetSubCat.length === 0) {
      this.translate.get("ALTASGNSUBCTG").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
    } else {
      this.subCatLoading = true;
      this.subCategoryDisable = true;
      var subCatAssign = [];
      this.targetSubCat.map((item) => {
        subCatAssign.push(item["SUBCATEGORYID"]);
      });
      this.updateSubCategoryData["checkListSetupId"] = this.checkListId;
      this.updateSubCategoryData["mainCategoryId"] = this.subCategorySelector;
      this.updateSubCategoryData["subCategoryId"] = subCatAssign.join(",");
      this.tempMainCat = this.subCategorySelector;
      this.checklistService
        .updateChecklistSubCategories(this.updateSubCategoryData)
        .subscribe((res) => {
          if (res["status"]) {
            localStorage.removeItem("sourceSubCat");
            localStorage.removeItem("targetSubCat");
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.refreshData();
            // this.subCatLoading = false;
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
            this.subCatLoading = false;
          }
          this.isSubCategoriesDropdownChanged = false;
        });
    }
  }

  onSubmit(value) {
    this.submitted = true;
    if (this.addGeneral.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.generalLoading = true;
      this.addGeneral.value["checkListName"] = this.addGeneral.value[
        "checkListName"
      ].trim();
      this.checklistSetupName = this.addGeneral.value["checkListName"];
      this.submitted = true;
      this.subscription = this.checklistService
        .updateGeneral(this.addGeneral.value)
        .subscribe((res) => {
          if (res["status"] === true) {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            if (this.addGeneral.value["allSites"] == 1) {
              this.enableSites = false;
            }
            if (this.addGeneral.value["allSites"] == 0) {
              this.enableSites = true;
            }
            this.addGeneral.reset();
            this.submitted = false;
            this.refreshData();
            this.generalLoading = false;
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
            this.generalLoading = false;
          }
        });
    }
  }

  onUpdateSites() {
    if (this.selectedSiteIDs.length === 0) {
      this.translate.get("ALTASGNSITE").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
    } else {
      this.siteLoading = true;
      this.siteDisable = true;
      this.updateSitesData["siteId"] = this.selectedSiteIDs.join();
      this.updateSitesData["checkListSetupId"] = this.checkListId;
      this.checklistService
        .updateChecklistSites(this.updateSitesData)
        .subscribe((res) => {
          if (res["status"]) {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
            this.refreshData();
            this.siteLoading = false;
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
            this.siteLoading = false;
          }
          this.isSiteDataChanged = false;
        });
    }
  }

  onClearAll(dropdownString) {
    if (dropdownString == "mainCategories") {
      this.mainCategory = [];
      this.mainCategoryDisable = false;
    } else {
      this.selectedSubCategories = [];
      this.subCategoryDisable = false;
    }
  }

  onSiteDataSelectAll() {
    let selectedSiteIDs = [];
    this.siteData.map((item) => {
      selectedSiteIDs.push(item.SITEID);
    });
    this.selectedSiteIDs = selectedSiteIDs;
    this.siteDisable = false;
  }
  onSiteDataClearAll() {
    this.selectedSiteIDs = [];
    this.siteDisable = false;
  }

  reset() {
    this.buildGeneralTab(this.initData["checkListData"]);

    this.mainCategoriesDropdownDrop = [];
    var sourceMainCat = JSON.parse(localStorage.getItem("sourceMainCat"));
    var targetMainCat = JSON.parse(localStorage.getItem("targetMainCat"));
    this.sourceMainCat = sourceMainCat;
    this.targetMainCat = targetMainCat;

    var sourceSubCat = JSON.parse(localStorage.getItem("sourceSubCat"));
    var targetSubCat = JSON.parse(localStorage.getItem("targetSubCat"));
    this.sourceSubCat = sourceSubCat;
    this.targetSubCat = targetSubCat;

    const catArray = [];
    const siteArray = [];
    this.siteData = this.initData["sites"];
    this.siteDataDrop = [];
    var subCatList = this.initData["sites"];
    subCatList.map((data) => {
      this.siteDataDrop.push({
        label: data["SITENAME"],
        value: data["SITEID"],
      });
    });
    if (targetMainCat.length != 0) {
      this.display = false;
    }
    if (this.initData["sitesId"] === "" || this.initData["sitesId"] === null) {
      const siteArrayVal = this.initData["sites"].find((item) => {
        return item["SITENAME"] == this.loginData["defaultSiteName"];
      });
      if (siteArrayVal) {
        siteArray.push(siteArrayVal["SITEID"]);
        this.selectedSiteIDs = siteArray;
      }
    } else {
      this.selectedSiteIDs = this.initData["sitesId"].split(",").map(Number);
    }
    this.checklistSetupName = this.initData["checkListData"]["SETUPNAME"];
    this.subCategorySelector =
      this.subCategoriesDropdown.length > 0
        ? this.subCategoriesDropdown[0]["MAINCATEGORYID"]
        : this.subCategorySelector;
    this.mainCategoryChange();
    this.subCategoryDisable = true;
    this.mainCategoryPreview = this.initData["mainCategoryPreview"];
    this.subCategoryPreview = this.initData["subCategoryPreview"];
    this.mainCategoryItems = [];
    if (this.mainCategoryPreview) {
      this.mainCategoryPreview.forEach((item) => {
        let obj = {};
        obj["mainCategoryName"] = item["MAINCATEGORYNAME"];
        let subCategoryItems = [];
        this.subCategoryPreview.forEach((item1) => {
          if (item["MAINCATEGORYID"] === item1["MAINCATEGORYID"]) {
            subCategoryItems.push(item1);
          }
        });
        obj["subCategoryItems"] = subCategoryItems;
        this.mainCategoryItems.push(obj);
      });
    }
    this.loaded = true;
  }

  onMainCategoriesDropdownChange() {
    this.mainCategoryDisable = false;
    this.isMainCategoriesDropdownChanged = true;
  }

  onSubCategoriesSelectorDropdown() {
    this.subCategoryDisable = false;
    this.isSubCategoriesDropdownChanged = true;
  }

  onSiteDataChanged() {
    this.siteDisable = false;
    this.isSiteDataChanged = true;
  }

  handleChange(e) {
    // document.getElementById("mainCatIds").click();
    // document.getElementById("subCatIds").click();
    if (this.subCategoriesDropdown.length > 0) {
      this.subCategorySelector = this.subCategoriesDropdown[0][
        "MAINCATEGORYID"
      ];
    }
    this.mainCategoryChange();
    let index = e.index;
    if ((this.enableSites == false && index == 3) || index == 4) {
      this.previewDisplay = this.mainCategoryPreview.length == 0;
    }

    if (
      (this.addGeneral.dirty && this.lastIndex === 0) ||
      (this.isMainCategoriesDropdownChanged && this.lastIndex === 1) ||
      (this.isSubCategoriesDropdownChanged && this.lastIndex === 2) ||
      (this.isSiteDataChanged && this.lastIndex === 3 && this.enableSites)
    ) {
      this.translate.get("LBLCONFIRMYESNO").subscribe((text: string) => {
        this.confirmationService.confirm({
          message: text,

          key: "tabChangeConfirmation",
          accept: () => {
            this.lastIndex = e.index;
            this.refreshData();
            this.isMainCategoriesDropdownChanged = false;
            this.isSubCategoriesDropdownChanged = false;
            this.isSiteDataChanged = false;
            this.setHelpFile(e.index);
          },
          reject: () => {
            this.index = this.lastIndex;
            this.setHelpFile(this.lastIndex);
          },
        });
      });
    } else {
      this.lastIndex = e.index;
      this.setHelpFile(e.index);
    }
  }

  setHelpFile(index) {
    if (index == 0) {
      this.breadcrumbService.setItems([
        {
          label: "LBLCHECKLISTCONFIG",
          url: "./assets/help/edit-checklist-setup-general.md",
        },
        { label: "LBLCHKLISTSETUP", routerLink: ["/checklist-setup-listing"] },
      ]);
    } else if (index == 1 || index == 2) {
      this.breadcrumbService.setItems([
        {
          label: "LBLCHECKLISTCONFIG",
          url: "./assets/help/edit-checklist-setup-main-category.md",
        },
        { label: "LBLCHKLISTSETUP", routerLink: ["/checklist-setup-listing"] },
      ]);
    } else if (index == 3) {
      this.breadcrumbService.setItems([
        {
          label: "LBLCHECKLISTCONFIG",
          url: "./assets/help/edit-checklist-setup-sites.md",
        },
        { label: "LBLCHKLISTSETUP", routerLink: ["/checklist-setup-listing"] },
      ]);
    } else if (index == 4) {
      this.breadcrumbService.setItems([
        {
          label: "LBLCHECKLISTCONFIG",
          url: "./assets/help/edit-checklist-setup-preview.md",
        },
        { label: "LBLCHKLISTSETUP", routerLink: ["/checklist-setup-listing"] },
      ]);
    }
  }
}
