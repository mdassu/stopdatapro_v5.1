import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChecklistSetupComponent } from './edit-checklist-setup.component';

describe('EditChecklistSetupComponent', () => {
  let component: EditChecklistSetupComponent;
  let fixture: ComponentFixture<EditChecklistSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditChecklistSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChecklistSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
