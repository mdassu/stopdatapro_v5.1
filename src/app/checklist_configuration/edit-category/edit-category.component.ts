import { Component, OnInit, ElementRef } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { ConfirmationService, MessageService } from "primeng/api";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { CategoryEditService } from "src/app/_services/category-edit.service";
import { ActivatedRoute } from "@angular/router";
import { CustomValidatorsService } from "../../_services/custom-validators.service";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
@Component({
  selector: "app-edit-category",
  templateUrl: "./edit-category.component.html",
  styleUrls: ["./edit-category.component.css"],
  providers: [MessageService, CustomValidatorsService],
})
export class EditCategoryComponent implements OnInit {
  addCategoryGeneral: FormGroup;
  addSubCategoryAssignment: FormGroup;
  subCategoryName: string;
  submitted: boolean;
  submitSubCategory: boolean;
  cols: any[];
  mainCategoryId: number;
  selectedSubCategories: any = [];
  subCategories: any = [];
  subCatBtnTogggle: boolean;
  selectedSubCategoriesToDelete: any = [];
  subCategorySearchTxt: string;
  loaded: boolean;
  categoryNameMaxLength: number = 128;
  subCategoryNameMaxLength: number = 128;
  initDataSet = {};
  index = 0;
  lastIndex = 0;
  appName: any;
  isSubcategoriesChanged: boolean;
  isLoading: boolean = true;
  generalLoading: boolean = false;
  assignLoading: boolean = false;
  subCategoriesDl: any = [];
  rowSubCategoryData: any;

  constructor(
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private editCategoryService: CategoryEditService,
    private elementRef: ElementRef,
    private activetedRoute: ActivatedRoute,
    private customValidatorsService: CustomValidatorsService,
    private env: EnvService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {
    this.activetedRoute.params.subscribe((params) => {
      this.mainCategoryId = params["id"];
    });
  }

  ngOnInit() {
    this.breadcrumbService.setItems([
      {
        label: "LBLCHECKLISTGLOBALCONFIG",
        url: "./assets/help/edit-category-general.md",
      },
      { label: "LBLCATEGORIES", routerLink: ["/categories-listing"] },
    ]);

    // Table header
    this.translate.get("LBLSUBCATNAME").subscribe((res: Object) => {
      this.cols = [{ field: "SUBCATEGORYNAME", header: res }];
    });
    this.addCategoryGeneral = this.fb.group({
      MainCategoryName: new FormControl("", [
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.notStartsWithSpace,
        this.customValidatorsService.maxLengthValidator(
          this.categoryNameMaxLength
        ),
      ]),
      status: new FormControl("", Validators.required),
    });
    this.addSubCategoryAssignment = this.fb.group({
      subCategoryName: new FormControl("", [
        this.customValidatorsService.noWhitespaceValidator,
        this.customValidatorsService.notStartsWithSpace,
        this.customValidatorsService.maxLengthValidator(
          this.subCategoryNameMaxLength
        ),
      ]),
    });
    this.rowSubCategoryData = this.addSubCategoryAssignment.getRawValue();
    this.getCategoryDataForEdit();
    this.appName = this.env.appName;
  }
  onSubCategoryAssign() {
    if (this.selectedSubCategories) {
      this.subCategories = this.auth.rearrangeSelects(
        this.subCategories,
        this.selectedSubCategories
      );
    }
    this.assignLoading = true;
    let body = {
      mainCategoryId: this.mainCategoryId,
      subCategoryIds: this.selectedSubCategories.toString(),
    };
    this.editCategoryService.updateSubCategoryAssign(body).subscribe((res) => {
      if (res["status"] == true) {
        this.initDataSet["subCategoryId"] = res["subCategoryId"];
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "success",
            detail: res,
          });
        });
        this.assignLoading = false;
      } else {
        this.translate.get(res["message"]).subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
        this.assignLoading = false;
      }
      this.isSubcategoriesChanged = false;
    });
  }

  getCategoryDataForEdit() {
    let body = { mainCategoryId: this.mainCategoryId };
    this.editCategoryService.getCategoryDataForEdit(body).subscribe((res) => {
      this.addCategoryGeneral.reset();
      this.initDataSet = res;
      const generalData = res["GeneralData"];
      this.subCategoriesDl = res["subCategory"];
      this.subCategories = [];
      var subCatList = res["subCategory"];
      subCatList.map((data) => {
        this.subCategories.push({
          label: data["SUBCATEGORYNAME"],
          value: data["SUBCATEGORYID"],
        });
      });

      this.translate.get(generalData.STATUS).subscribe((label) => {
        this.selectedSubCategories = res["subCategoryId"]
          ? res["subCategoryId"].split(",").map(Number)
          : [];
        this.addCategoryGeneral.patchValue({
          MainCategoryName: generalData.MAINCATEGORYNAME,
          status: label,
        });
      });

      this.loaded = true;
      this.isLoading = false;
    });
  }

  onReset() {
    this.addCategoryGeneral.reset();
    this.addSubCategoryAssignment.reset();
    this.isSubcategoriesChanged = false;
    const generalData = this.initDataSet["GeneralData"];
    // this.subCategories = this.initDataSet["subCategory"];
    this.subCategoriesDl = this.initDataSet["subCategory"];
    this.subCategories = [];
    var subCatList = this.initDataSet["subCategory"];
    subCatList.map((data) => {
      this.subCategories.push({
        label: data["SUBCATEGORYNAME"],
        value: data["SUBCATEGORYID"],
      });
    });

    this.selectedSubCategories = this.initDataSet["subCategoryId"]
      ? this.initDataSet["subCategoryId"].split(",").map(Number)
      : [];
    this.translate.get(generalData.STATUS).subscribe((res) => {
      this.addCategoryGeneral.patchValue({
        MainCategoryName: generalData.MAINCATEGORYNAME,
        status: res,
      });
    });
  }

  get uform() {
    return this.addCategoryGeneral.controls;
  }
  get subCategoryForm() {
    return this.addSubCategoryAssignment.controls;
  }

  onSubmit(value: string) {
    if (value === "mainCategory") {
      this.submitted = true;
      if (this.addCategoryGeneral.invalid) {
        this.customValidatorsService.scrollToError();
      } else {
        this.generalLoading = true;
        const body = {
          mainCategoryId: this.mainCategoryId,
          mainCategoryName: this.addCategoryGeneral.get("MainCategoryName")
            .value,
        };
        this.editCategoryService.updateMainCategory(body).subscribe((res) => {
          if (res["status"] == true) {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });
          }
          this.submitted = false;
          this.generalLoading = false;
          this.getCategoryDataForEdit();
          this.addCategoryGeneral.reset();
        });
      }
    } else {
      if (!this.addSubCategoryAssignment.valid) {
        this.submitSubCategory = true;
      } else {
        this.submitSubCategory = false;

        let body = {
          subCategoryName: this.subCategoryForm.subCategoryName.value,
          mainCategoryId: this.mainCategoryId,
        };
        this.editCategoryService.addSubCategory(body).subscribe((res) => {
          if (res["status"] == true) {
            // this.subCategories = res["subCategory"];
            this.subCategoriesDl = res["subCategory"];
            this.subCategories = [];
            var subCatList = res["subCategory"];
            subCatList.map((data) => {
              this.subCategories.push({
                label: data["SUBCATEGORYNAME"],
                value: data["SUBCATEGORYID"],
              });
            });
            this.selectedSubCategories = res["subCategoryId"]
              ? res["subCategoryId"].split(",").map(Number)
              : [];
            // this.initDataSet["subCategory"] = this.subCategories;
            this.initDataSet["subCategory"] = res["subCategory"];
            this.initDataSet["subCategoryId"] = res["subCategoryId"];
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "success",
                detail: res,
              });
            });

            this.addSubCategoryAssignment = this.fb.group({
              subCategoryName: new FormControl("", [
                this.customValidatorsService.noWhitespaceValidator,
                this.customValidatorsService.notStartsWithSpace,
                this.customValidatorsService.maxLengthValidator(
                  this.subCategoryNameMaxLength
                ),
              ]),
            });
          } else {
            this.translate.get(res["message"]).subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
          }
        });
      }
    }
  }

  deleteRecord() {
    this.translate.get("ALTDELETESUBCATEGORY").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          const body = {
            mainCategoryId: this.mainCategoryId,
            subCategoryIds: Array.prototype.map
              .call(this.selectedSubCategoriesToDelete, (s) => s.SUBCATEGORYID)
              .toString(),
          };
          this.editCategoryService.deleteSubCategory(body).subscribe((res) => {
            const selectedSubCatogoriesDelete = Array.prototype.map.call(
              this.selectedSubCategoriesToDelete,
              (s) => s.SUBCATEGORYID
            );

            if (res["status"] == true) {
              this.getCategoryDataForEdit();
              this.subCategoryName = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
              this.selectedSubCategoriesToDelete = [];
            } else {
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "error",
                  detail: res,
                });
              });
            }
          });
        },
        reject: () => {
          this.selectedSubCategoriesToDelete = [];
        },
      });
    });
  }

  searchSubCat(dt) {
    dt.filter(this.subCategorySearchTxt, "SUBCATEGORYNAME", "contains");
  }
  onSelectAll() {
    const selected = this.subCategories.map((item) => item.SUBCATEGORYID);
    this.selectedSubCategories = selected;
    this.isSubcategoriesChanged = true;
  }

  onClearAll() {
    this.selectedSubCategories = [];
    this.isSubcategoriesChanged = true;
  }

  onSubcategoriesChange() {
    this.isSubcategoriesChanged = true;
  }

  handleChange(e) {
    this.selectedSubCategoriesToDelete = [];
    if (
      (this.addCategoryGeneral.dirty && this.lastIndex === 0) ||
      ((this.addSubCategoryAssignment.dirty || this.isSubcategoriesChanged) &&
        this.lastIndex === 1)
    ) {
      this.translate.get("LBLCONFIRMYESNO").subscribe((text: string) => {
        this.confirmationService.confirm({
          message: text,

          key: "tabChangeConfirmation",
          accept: () => {
            this.onReset();
            this.lastIndex = e.index;
            this.setHelpFile(e.index);
          },
          reject: () => {
            this.index = this.lastIndex;
            this.setHelpFile(this.lastIndex);
          },
        });
      });
    } else {
      this.lastIndex = e.index;
      this.setHelpFile(e.index);
    }
  }

  setHelpFile(index) {
    if (index == 0) {
      this.breadcrumbService.setItems([
        {
          label: "LBLCHECKLISTGLOBALCONFIG",
          url: "./assets/help/edit-category-general.md",
        },
        { label: "LBLCATEGORIES", routerLink: ["/categories-listing"] },
      ]);
    } else if (index == 1) {
      this.breadcrumbService.setItems([
        {
          label: "LBLCHECKLISTGLOBALCONFIG",
          url: "./assets/help/edit-category-assignments.md",
        },
        { label: "LBLCATEGORIES", routerLink: ["/categories-listing"] },
      ]);
    } else if (index == 2) {
      this.breadcrumbService.setItems([
        {
          label: "LBLCHECKLISTGLOBALCONFIG",
          url: "./assets/help/edit-category-delete.md",
        },
        { label: "LBLCATEGORIES", routerLink: ["/categories-listing"] },
      ]);
    }
  }
}
