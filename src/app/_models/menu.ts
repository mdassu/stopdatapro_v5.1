const menus = [
  {
    label: "LBLDASHBOARD",
    icon: "dashboard",
    visible: true,
    routerLink: ["/dashboard"]
  },
  {
    label: "LBLUSERMGMT",
    icon: "person",
    items: [
      {
        label: "LBLUSERS",
        icon: "person",
        routerLink: ["/users"]
      },
      { label: "LBLGROUPS", icon: "people", routerLink: ["/group-listing"] },
      { label: "LBLROLES", icon: "how_to_reg", routerLink: ["/role-listing"] },
      {
        label: "LBLMULTIUSERTARGET",
        icon: "center_focus_strong",
        routerLink: ["/targets"]
      }
    ]
  },
  {
    label: "LBLSITEMGMT",
    icon: "place",
    items: [
      {
        label: "LBLBSITES",
        icon: "pin_drop",
        routerLink: ["/site-listing"]
      },
      { label: "LBLAREAS", icon: "map", routerLink: ["/area-listing"] },
      {
        label: "LBLSUBAREAS",
        icon: "map",
        routerLink: ["/subarea-listing"]
      },
      { label: "LBLSHIFTS", icon: "update", routerLink: ["/shifts-listing"] },
      {
        label: "LBLUSERDEFINEDFEILDS",
        icon: "group",
        routerLink: ["/user-defind-field"]
      }
    ]
  },
  {
    label: "LBLCHECKLISTCONFIG",
    icon: "featured_play_list",
    items: [
      {
        label: "LBLCATEGORIES",
        icon: "subject",
        routerLink: ["/categories-listing"]
      },
      {
        label: "LBLCHKLISTSETUP",
        icon: "subtitles",
        routerLink: ["/checklist-setup-listing"]
      }
    ]
  },
  {
    label: "LBLDATAENTRY",
    icon: "dvr",
    items: [
      {
        label: "LBLOBSERVATIONCHECKLIST",
        icon: "list_alt",
        routerLink: ["/observation-listing"]
      },
      {
        label: "LBLCAPA",
        icon: "playlist_add_check",
        routerLink: ["/corrective-action-list"]
      },
      {
        label: "LBLREDFLAGS",
        icon: "flag",
        routerLink: ["/red-flags-listing"]
      },
      {
        label: "LBLINJURYSTATS",
        icon: "assessment",
        routerLink: ["/injury-statistics-listing"]
      },
      {
        label: "LBLAPPROVAL",
        icon: "check_box",
        routerLink: ["/approval-listing"]
      },
      {
        label: "LBLOBSCHKLISTGROUPS",
        icon: "assignment_turned_in",
        routerLink: ["/edit-groups-on-checklists"]
      }
    ]
  },
  {
    label: "LBLSETTCONFIG",
    icon: "settings",
    items: [
      {
        label: "LBLGLOBALOPTIONS",
        icon: "public",
        routerLink: ["/global-options"]
      },
      {
        label: "LBLTRANSLATION",
        icon: "repeat",
        routerLink: ["/translations"]
      }
    ]
  },
  {
    label: "LBLREPORTMGMT",
    icon: "assessment",
    items: [
      { label: "LBLREPORTS", icon: "desktop_mac", routerLink: ["/reports"] },
      {
        label: "LBLSCHEDULEREPORTS",
        icon: "desktop_mac",
        routerLink: ["/schedule-reports"]
      },
      {
        label: "LBLANALYTICS",
        icon: "desktop_mac",
        routerLink: ["/analytics-dashboard"]
      }
    ]
  },
  {
    label: "LBLEMAILCONFIG",
    icon: "email",
    items: [
      {
        label: "LBLEMAILNOTIFICATION",
        icon: "notifications",
        routerLink: ["/email-listing"]
      }
    ]
  },
  {
    label: "LBLSUPPORT",
    icon: "help",
    items: [
      {
        label: "LBLTECHNICALREQ",
        icon: "desktop_mac",
        routerLink: ["/technical-requirements"]
      },
      {
        label: "LBLCONTACTUS",
        icon: "call",
        routerLink: ["/contact-us"]
      },
      {
        label: "LBLHELPFILE",
        icon: "help",
        target: "blank",
        url: ""
      }
    ]
  }
];
export default menus;
