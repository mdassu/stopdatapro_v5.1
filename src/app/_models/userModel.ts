export class UserModel {
  status: number;
  alertMessageFlag: string;
  alertMessage: string;
  userId: number;
  languageId: number;
  roleId: number;
  userRolePermission: string;
  isDefaultId: number;
  dateFormat: string;
  firstLogin: number;
  firstName: string;
  lastName: string;
  passwordExpired: boolean;
  defaultSiteName: string;
  languageCode: string;
  sessionID: number;
  sslId: number;
  editObservation: number;
  timeZoneId: number;
  addNewObserver: number;
  copyFrom: number;
  unsafeAlertPermission: number;
  unsafeAlertSbuscribe: number;
  obsApprovalpc: number;
  addCapa: number;
  token: string;
}
