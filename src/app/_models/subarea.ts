export class Subarea {
    subAreaId: number;
    siteId: number;
    areaId: number;
    subAreaName:string;
    status:string;
}
