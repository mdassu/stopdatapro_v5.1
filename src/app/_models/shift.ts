export class Shift {
    shiftId: number;
    siteId: number;
    shiftName: string;
    status: string;
}