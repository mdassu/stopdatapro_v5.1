export class Col {
  field: string;
  header: string;
  width?: string;
}
