export class User {
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  role: string;
  email: string;
  token?: string;
  status: string;
  confirmPassword: string;
  defaultSite: string;
  dateFormat: string;
}


