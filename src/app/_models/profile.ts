export class Profile {
    userId: number;
    email: string;
    userName: number
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
    siteId: number;
    dateFormat: number;
    languageId: number;
}