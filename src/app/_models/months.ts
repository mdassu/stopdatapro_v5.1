export const months = [
  {"label": "January", "value": "jan"},
  {"label": "February", "value": "feb"},
  {"label": "March", "value": "mar"},
  {"label": "April", "value": "apr"},
  {"label": "May", "value": "may"},
  {"label": "June", "value": "jun"},
  {"label": "July", "value": "jul"},
  {"label": "August", "value": "aug"},
  {"label": "September", "value": "sep"},
  {"label": "October", "value": "oct"},
  {"label": "November", "value": "nov"},
  {"label": "December", "value": "dec"}
];
