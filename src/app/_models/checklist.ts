export class GeneralControls {
    checkListName: string;
    allSites: number;
    copyFrom: number;
    status: number;
    allSafeCheck: number;
    enableSafeUnsafe: number;
    SETUPNAME: string;
    ALLSITES: number;
    COPYFROM: number;
    STATUS: number;
    ALLSAFECHECK: number;
    ENABLESAFEUNSAFE: number;
}