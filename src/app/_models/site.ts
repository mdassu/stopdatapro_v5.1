export class Site {
  siteId: number;
  siteName: string;
  address: string;
  city: string;
  state: string;
  zipCode:string;
  comments: string;
  status: string;
  employees:string;
  countryId: number;
  defaultTimeZone: string;
}

export class AssignSite {
  siteId: number;
  assignType: number;
  groupTypeId: number;
  assignIds: string;
}

export class Target {
  siteId: number;
  jan:number;
  feb:number;
  mar:number;
  apr:number;
  may:number;
  jun:number;
  jul:number;
  aug:number;
  sep:number;
  oct:number;
  nov:number;
  dec:number; 
  applyToAllObser: number;
  target: number;
  weeklyTarget: number;
}
