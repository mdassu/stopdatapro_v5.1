export enum Role {
  superAdministrator = "Super Administrator",
  siteAdministrator = "Site Administrator",
  reportAdministrator = "Report Administrator",
  dataEntryOperator = "Data Entry Operator",
  observerOnly = "Observer Only",
  dssDemo = "DSS Demo"
}
