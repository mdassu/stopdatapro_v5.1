export class Area {
    areaId: number;
    siteId: number;
    areaName: string;
    status: string;
}
