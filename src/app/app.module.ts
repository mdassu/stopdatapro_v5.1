import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import {
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpClientModule,
} from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgSelectModule } from "@ng-select/ng-select";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { AppRoutes } from "./app.routes";

import { AppComponent } from "./app.component";
import { AppFooterComponent } from "./app.footer.component";
import { AppMainComponent } from "./app.main.component";
import { AppMenuComponent, AppSubMenuComponent } from "./app.menu.component";
import { AppTopBarComponent } from "./app.topbar.component";
import { AppBreadcrumbComponent } from "./app.breadcrumb.component";

import { HasRoleDirective } from "./_directives/hasrole/has-role.directive";
import { ErrorInterceptor } from "./_helpers/error.interceptor";
import { fakeBackendProvider } from "./_helpers/fake-backend";
import { BreadcrumbService } from "./breadcrumb.service";

import { CookieService } from "ngx-cookie-service";
import { CardModule } from "primeng/card";
import { TableModule } from "primeng/table";

import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { DataViewModule } from "primeng/dataview";
import { DropdownModule } from "primeng/dropdown";
import { InputSwitchModule } from "primeng/inputswitch";
import { InputTextModule } from "primeng/inputtext";
import { InputTextareaModule } from "primeng/inputtextarea";
import { MessageModule } from "primeng/message";
import { MessagesModule } from "primeng/messages";
import { MultiSelectModule } from "primeng/multiselect";
import { OverlayPanelModule } from "primeng/overlaypanel";
import {
  MessageService,
  ConfirmationService,
  AccordionModule,
  CalendarModule,
  DialogModule,
  FileUploadModule,
  GrowlModule,
  PanelModule,
  PickListModule,
  ScrollPanelModule,
  TabViewModule,
  TreeModule,
  TooltipModule,
  ProgressSpinnerModule,
} from "primeng/primeng";
import { RadioButtonModule } from "primeng/radiobutton";
import { ToastModule } from "primeng/toast";
import { TreeTableModule } from "primeng/treetable";
import { JwtInterceptor } from "./_helpers/jwt.interceptor";
import { UserResolver } from "./_services/user.resolver";

import { LoginComponent } from "./login/login.component";

import { ColorPickerModule } from "ngx-color-picker";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/");
}

import { VisibleToOnlySuperAdminDirective } from "./_directives/visible-to-only-super-admin.directive";
import { EnvServiceProvider } from "src/env.service.provider";

import { ChartModule } from "primeng/chart";

// TinyMCE
import { EditorModule } from "@tinymce/tinymce-angular";
import { NgxScrollToFirstInvalidModule } from "@ismaestro/ngx-scroll-to-first-invalid";
import { MarkdownModule } from "ngx-markdown";

//Html Print
import { NgxPrintModule } from "ngx-print";
import { HelpfileComponent } from "./helpfile/helpfile.component";
import { EncrDecrService } from "../app/_services/encr-decr.service";
import { RouterModule } from "@angular/router";
import { PCalendarLocaleDirective } from "./_directives/p-calendar-locale.directive";

@NgModule({
  imports: [
    BrowserModule,
    NgSelectModule,
    FormsModule,
    AppRoutes,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ColorPickerModule,
    EditorModule,
    ChartModule,
    ButtonModule,
    DropdownModule,
    ConfirmDialogModule,
    OverlayPanelModule,
    MultiSelectModule,
    InputTextareaModule,
    InputTextModule,
    DataViewModule,
    CardModule,
    TableModule,
    ScrollPanelModule,
    InputTextModule,
    DropdownModule,
    MultiSelectModule,
    ButtonModule,
    InputTextModule,
    InputTextareaModule,
    MessagesModule,
    MessageModule,
    ToastModule,
    DropdownModule,
    PanelModule,
    TabViewModule,
    TreeModule,
    RadioButtonModule,
    InputSwitchModule,
    FileUploadModule,
    CheckboxModule,
    TreeTableModule,
    CalendarModule,
    AccordionModule,
    DialogModule,
    GrowlModule,
    PickListModule,
    TooltipModule,
    ProgressSpinnerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NgxPrintModule,
    MarkdownModule.forRoot(),
    NgxScrollToFirstInvalidModule,
  ],
  declarations: [
    AppComponent,
    AppMainComponent,
    AppMenuComponent,
    AppSubMenuComponent,
    AppTopBarComponent,
    AppFooterComponent,
    LoginComponent,
    AppBreadcrumbComponent,
    HasRoleDirective,
    VisibleToOnlySuperAdminDirective,
    HelpfileComponent,
    PCalendarLocaleDirective
  ],
  providers: [
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    BreadcrumbService,
    fakeBackendProvider,
    MessageService,
    ConfirmationService,
    UserResolver,
    CookieService,
    EnvServiceProvider,
    EncrDecrService,
  ],
  bootstrap: [AppComponent],
  exports: [RouterModule],
})
export class AppModule {}
