import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../breadcrumb.service";
import {
  SelectItem,
  MessageService,
  ConfirmationService,
  SortEvent
} from "primeng/api";
import { EmailInboxService } from "../_services/email-inbox.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";

import * as moment from "moment";

enum emailTab {
  Unread,
  Mails,
  Reports,
  AllMails
}

@Component({
  selector: "app-email-inbox",
  templateUrl: "./email-inbox.component.html",
  styleUrls: ["./email-inbox.component.css"]
})
export class EmailInboxComponent implements OnInit {
  searchBy: SelectItem[];
  searchDropdownValue: string = "FROM";
  searchText: string;
  unreadCols: any[];
  emailsCols: any[];
  emailsCount: number;
  unread: any;
  emails: any;
  scheduledReports: any;
  allEmails: any;
  selectedUnread: [];
  selectedEmails: [];
  selectedReports: [];
  selectedAllEmails: [];
  isLoaded: boolean;
  tableCols: any[];
  tableValues: any;
  tableSelected: [];
  key: string = "unread";
  displayData: {};
  messageBody: string;
  colsCount: number;
  tempRowData: {};
  selectedMail: Array<number> = [];
  parentIndex = 0;
  searchTypePlaceholder: any;
  dateFormat: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private emailInboxService: EmailInboxService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLINBOX", url: "./assets/help/email-inbox.md" },
      { label: "LBLDASHBOARD", routerLink: ["/dashboard"] }
    ]);
  }

  ngOnInit() {
    this.unreadCols = [
      { field: "FROM", header: "LBLFROM" },
      { field: "SUBJECT", header: "LBLMAILSUBJECT" },
      { field: "SENTDATE", header: "LBLMAILRECEIVEDDATE" }
    ];
    this.emailsCols = [
      { field: "FROM", header: "LBLFROM" },
      { field: "SUBJECT", header: "LBLMAILSUBJECT" },
      { field: "SENTDATE", header: "LBLMAILRECEIVEDDATE" },
      { field: "MAILREADSTATUS", header: "LBLSTATUS" }
    ];
    this.emailsCount = this.emailsCols.length;
    this.setSearchDropDownValues();
    // this.loadInitialData();
    this.loadData();
    this.tableSelected = [];
    this.dateFormat = this.auth.UserInfo["dateFormat"];
  }

  setSearchDropDownValues() {
    this.translate
      .get(["LBLFROM", "LBLMAILSUBJECT", "LBLMAILRECEIVEDDATE", "LBLSTATUS"])
      .subscribe(res => {
        if (this.key == "unread") {
          this.searchBy = [
            { label: res["LBLFROM"], value: "FROM" },
            { label: res["LBLMAILSUBJECT"], value: "SUBJECT" },
            { label: res["LBLMAILRECEIVEDDATE"], value: "SENTDATE" }
          ];
        } else {
          this.searchBy = [
            { label: res["LBLFROM"], value: "FROM" },
            { label: res["LBLMAILSUBJECT"], value: "SUBJECT" },
            { label: res["LBLMAILRECEIVEDDATE"], value: "SENTDATE" },
            { label: res["LBLSTATUS"], value: "MAILREADSTATUS" }
          ];
        }
      });
  }

  loadData() {
    this.emailCount();
    this.tableSelected = [];
    if (this.parentIndex == 0 && this.unread) {
      this.tableValues = this.unread;
      this.isLoaded = true;
    } else if (this.parentIndex == 1 && this.emails) {
      this.tableValues = this.emails;
      this.isLoaded = true;
    } else if (this.parentIndex == 2 && this.scheduledReports) {
      this.tableValues = this.scheduledReports;
      this.isLoaded = true;
    } else if (this.parentIndex == 3 && this.allEmails) {
      this.tableValues = this.allEmails;
      this.isLoaded = true;
    } else {
      this.emailInboxService.getEmailData({}).subscribe(res => {
        this.unread = res["unRead"];
        this.emails = res["emails"];
        this.scheduledReports = res["scheduleReport"];
        this.allEmails = res["allEmails"];
        this.key = "unread";
        this.tableValues = this.unread;
        this.colsCount = this.unreadCols.length;
        this.tableCols = this.unreadCols;
        this.messageBody = "unreadBody";
        this.isLoaded = true;
      });
    }
  }

  onTabChange(index) {
    this.isLoaded = false;
    this.emailInboxService.getEmailData({}).subscribe(res => {
      this.unread = res["unRead"];
      this.emails = res["emails"];
      this.scheduledReports = res["scheduleReport"];
      this.allEmails = res["allEmails"];
      if (index == 0) {
        this.tableValues = this.unread;
      } else if (index == 1) {
        this.tableValues = this.emails;
      } else if (index == 2) {
        this.tableValues = this.scheduledReports;
      } else {
        this.tableValues = this.allEmails;
      }
      this.isLoaded = true;
    });
    this.parentIndex = index;
    delete this.displayData;
    delete this.tempRowData;
    this.searchText = "";
    this.searchDropdownValue = "FROM";
    this.translate.get("LBLFROM").subscribe(res => {
      this.searchTypePlaceholder = res;
    });
    this.selectedMail = [];
    if (index == emailTab.Unread) {
      this.key = "unread";
      this.tableCols = this.unreadCols;
      this.messageBody = "unreadBody";
      this.colsCount = this.unreadCols.length;
      this.tableValues = this.unread;
      this.setSearchDropDownValues();
    } else if (index == emailTab.Mails) {
      this.loadData();
      this.key = "mails";
      this.tableCols = this.emailsCols;
      this.tableValues = this.emails;
      this.messageBody = "emailsBody";
      this.colsCount = this.emailsCols.length;
      this.setSearchDropDownValues();
    } else if (index == emailTab.Reports) {
      this.loadData();
      this.key = "reports";
      this.tableCols = this.emailsCols;
      this.tableValues = this.scheduledReports;
      this.messageBody = "reportBody";
      this.colsCount = this.emailsCols.length;
      this.setSearchDropDownValues();
    } else if (index == emailTab.AllMails) {
      this.loadData();
      this.key = "allMails";
      this.tableCols = this.emailsCols;
      this.tableValues = this.allEmails;
      this.messageBody = "allMailsBody";
      this.colsCount = this.emailsCols.length;
      this.setSearchDropDownValues();
    }
  }

  onRowSelect(data, id, index) {
    switch (this.key) {
      case "unread":
        this.selectedMail = [];
        this.selectedMail.push(index);
        const ID = { inboxId: id };
        this.emailInboxService.getEmailByID(ID).subscribe(res => {
          this.displayData = data;
          setTimeout(() => {
            document.getElementById("unreadBody").innerHTML =
              res["data"][0]["BODY"];
          }, 0);
        });
        this.emailCount();
        break;
      case "mails":
        this.showData(data, id, index, "emailsBody");
        break;
      case "reports":
        this.showData(data, id, index, "reportBody");
        break;
      case "allMails":
        this.showData(data, id, index, "allMailsBody");
        break;

      default:
        break;
    }
  }

  showData(data, id, index, messageBody: string) {
    this.selectedMail = [];
    this.selectedMail.push(index);
    if (data["MAILREADSTATUS"] == "LBLUNREAD") {
      this.tableValues[index]["MAILREADSTATUS"] = "LBLREAD";
      const ID = { inboxId: id };
      this.emailInboxService.getEmailByID(ID).subscribe(res => {
        this.displayData = data;
        setTimeout(() => {
          document.getElementById(messageBody).innerHTML =
            res["data"][0]["BODY"];
        }, 0);
      });
    } else {
      this.displayData = data;
      setTimeout(() => {
        document.getElementById(messageBody).innerHTML = data["BODY"];
      }, 0);
    }
    this.emailCount();
  }

  isVisible(index) {
    return this.selectedMail.includes(index) ? true : false;
  }

  deleteRecord() {
    let id;
    if (this.tableSelected) {
      id = this.tableSelected.map(res => {
        return res["ID"];
      });
      const ID = { inboxId: id.join() };
      this.translate.get("ALTDELCONFIRM").subscribe((message: string) => {
        this.confirmationService.confirm({
          message: message,
          accept: () => {
            this.emailInboxService.deleteEmailData(ID).subscribe(res => {
              this.translate.get(res["message"]).subscribe(message => {
                this.messageService.add({
                  severity: "success",
                  detail: message
                });
                this.unread = "";
                this.isLoaded = false;
                this.loadData();
              });
            });
          }
        });
      });
    }
  }

  onSearch() {
    this.selectedMail = [];
    if (this.searchText && this.searchDropdownValue) {
      let searchString;
      if (/\s/.test(this.searchText)) {
        const searchStringArray = this.searchText.split(" ");
        let x = [];
        searchStringArray.forEach(res => {
          if (res.length > 0) {
            x.push(res.toLowerCase());
          }
        });
        searchString = x.join("");
        this.searchText = searchString;
        this.searching(searchString);
      } else {
        searchString = this.searchText.toLowerCase();
        this.searching(searchString);
      }
    } else {
      this.loadData();
    }
  }

  dropchange() {
    this.searchBy.forEach(item => {
      if (item["value"] == this.searchDropdownValue) {
        this.searchTypePlaceholder = item["label"];
      }
    });
  }

  searching(searchString) {
    this.emailCount();
    let searchResult: [];
    if (searchString != "") {
      switch (this.key) {
        case "unread":
          searchResult = this.unread.filter(item => {
            if (item[this.searchDropdownValue]) {
              return (
                item[this.searchDropdownValue]
                  .toLowerCase()
                  .indexOf(searchString) >= 0
              );
            }
          });
          this.tableValues = searchResult;
          break;
        case "mails":
          searchResult = this.emails.filter(item => {
            if (this.searchDropdownValue == "MAILREADSTATUS") {
              return (
                item[this.searchDropdownValue].toLowerCase() ==
                ("LBL" + searchString).toLowerCase()
              );
            } else {
              return (
                item[this.searchDropdownValue]
                  .toLowerCase()
                  .indexOf(searchString) >= 0
              );
            }
          });
          this.tableValues = searchResult;
          break;
        case "reports":
          searchResult = this.scheduledReports.filter(item => {
            if (this.searchDropdownValue == "MAILREADSTATUS") {
              return (
                item[this.searchDropdownValue].toLowerCase() ==
                ("LBL" + searchString).toLowerCase()
              );
            } else {
              return (
                item[this.searchDropdownValue]
                  .toLowerCase()
                  .indexOf(searchString) >= 0
              );
            }
          });
          this.tableValues = searchResult;
          break;
        case "allMails":
          searchResult = this.allEmails.filter(item => {
            if (this.searchDropdownValue == "MAILREADSTATUS") {
              return (
                item[this.searchDropdownValue].toLowerCase() ==
                ("LBL" + searchString).toLowerCase()
              );
            } else {
              return (
                item[this.searchDropdownValue]
                  .toLowerCase()
                  .indexOf(searchString) >= 0
              );
            }
          });
          this.tableValues = searchResult;
          break;
        default:
          break;
      }
    } else {
      this.loadData();
    }
  }

  emailCount() {
    let acc = 0;
    this.emailInboxService.getEmailData({}).subscribe(res => {
      if (res["allEmails"].length > 0) {
        res["allEmails"].forEach((val, keys) => {
          if (val["MAILREADSTATUS"] == "LBLUNREAD") {
            acc += 1;
          }

          if (document.getElementById("topbarEmailCount")) {
            document.getElementById(
              "topbarEmailCount"
            ).innerHTML = acc.toString();
          }

          if (res["allEmails"].length - 1 == keys && acc == 0) {
            if (document.getElementById("topbarEmailCount")) {
              document.getElementById("topbarEmailCount").remove();
            }
          }
        });
      } else {
        // document.getElementById("topbarEmailCount").innerHTML = "0";
        // document.getElementById("topbarEmailCount").remove();
      }
    });
  }

  customSort(event: SortEvent) {
    event.data.sort((data1, data2) => {
      let value1 = data1[event.field];
      let value2 = data2[event.field];

      let result = null;

      if (value1 == null && value2 != null) result = -1;
      else if (value1 != null && value2 == null) result = 1;
      else if (value1 == null && value2 == null) result = 0;
      else if (typeof value1 === "string" && typeof value2 === "string") {
        const date1 = moment(
          value1,
          this.auth.UserInfo["dateFormat"].toUpperCase()
        );
        const date2 = moment(
          value2,
          this.auth.UserInfo["dateFormat"].toUpperCase()
        );

        let result: number = -1;
        if (moment(date2).isBefore(date1, "day")) {
          result = 1;
        }

        return result * event.order;
      }
      // result = value1.localeCompare(value2);
      else result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;

      return event.order * result;
    });
  }
}
