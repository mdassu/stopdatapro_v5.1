import { async, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { ScrollPanelModule } from "primeng/primeng";
import { AppComponent } from "./app.component";
import { AppMainComponent } from "./app.main.component";
import { AppMenuComponent, AppSubMenuComponent } from "./app.menu.component";
import { AppTopBarComponent } from "./app.topbar.component";

describe("AppComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, ScrollPanelModule],
      declarations: [
        AppComponent,
        AppMainComponent,
        AppMenuComponent,
        AppSubMenuComponent,
        AppTopBarComponent
      ]
    }).compileComponents();
  }));

  it("should create the app", async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
