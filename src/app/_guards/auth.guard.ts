import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { AuthenticationService } from "../_services/authentication.service";
import menus from "../_models/menu";
import { RoleService } from "../_services/role.service";
import { map } from "rxjs/operators";
import { Observable, forkJoin, of } from "rxjs";

@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthenticationService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    // const role = this.roleService.getRole();
    // const roledata = this.roleService.getRoleData(role);

    // const isauthenticated = roledata.pipe(
    //   map(data => {
    //     if (
    //       data[route.data.menu] &&
    //       data[route.data.menu].indexOf(route.data.submenu) > -1
    //     ) {
    //       return true;
    //     } else {
    //       return false;
    //     }
    //   })
    // );
    var userId;
    this.auth.currentUserId.subscribe((id) => {
      userId = id;
    });
    if (localStorage.getItem("currentUser-" + userId)) {
      const roleData = JSON.parse(
        localStorage.getItem("rolePermission-" + userId)
      );
      if (route.data.label) {
        let isFound;
        roleData.forEach((role) => {
          if (role.PERMISSIONNAME === route.data.label) {
            isFound = true;
          }
        });

        return of(isFound);
      }

      return of(true);
    } else {
      return of(false);
    }
  }
}
