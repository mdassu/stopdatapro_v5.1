import { Component, OnInit } from "@angular/core";
import * as Chart from "chart.js";
import { DashboardService } from "../_services/dashboard.service";
import { BreadcrumbService } from "../breadcrumb.service";
import { AlertUnsafeService } from "../_services/alert-unsafe.service";

import menus from "../_models/menu";
import { AuthenticationService } from "../_services/authentication.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-dashboard-latest",
  templateUrl: "./dashboard-latest.component.html",
  styleUrls: ["./dashboard-latest.component.css"],
})
export class DashboardLatestComponent implements OnInit {
  options: any;
  ctx: any;
  weekCanvas: any;
  monthCanvas: any;
  actionCtx: any;
  dupontNews: any;
  userRoleID: number;
  dashboardData: any;
  toggle: string;
  approvalCount: any;
  comingDue: any;
  pastDue: any;
  roleName: any;
  siteName: any;
  emailAlerts: any;
  unsafeEmailCount: any;
  weeklyObs: any;
  monthlyObs: any;
  weeklyTarget: any;
  monthlyTarget: any;
  weeklyObsPercentage: any;
  monthlyObsPercentage: any;
  alertUnafeRoute: any;
  unsafeALertTitle: string;
  weekChartVisible: boolean = true;
  monthChartVisible: boolean = true;
  commonWMessage: boolean;
  commonMessage: boolean;
  dashBoardValue: any;
  isLoaded: boolean = false;
  newMenus: any = [];
  addObsStatus: boolean = false;
  editObsStatus: boolean = false;
  addChecklist: boolean = false;
  editChecklist: boolean = false;
  supportStatus: boolean = false;
  roleCopyFrom: number;
  autoLogout: any;
  constructor(
    private readonly breadcrumbService: BreadcrumbService,
    private dashboardService: DashboardService,
    private alertUnsafeService: AlertUnsafeService,
    private auth: AuthenticationService,
    private translate: TranslateService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLDASHBOARD", url: "./assets/help/dashboard.md" },
    ]);
  }

  ngOnInit() {
    const menusString = JSON.stringify(menus);
    this.newMenus = JSON.parse(menusString);
    var userId = this.auth.UserInfo["userId"];
    const roleData = JSON.parse(
      localStorage.getItem("dashboardRolePermission-" + userId)
    );
    this.newMenus.map((menuItem) => {
      roleData.forEach((role) => {
        if (menuItem.label === role.PERMISSIONNAME) {
          menuItem.items.map((subMenu) => {
            roleData.forEach((subRole) => {
              if (subRole.PERMISSIONNAME === "LBLADDOBSERVATION") {
                this.addObsStatus = true;
              }
              if (subRole.PERMISSIONNAME === "LBLEDITOBSERVATION") {
                this.editObsStatus = true;
              }
              if (subRole.PERMISSIONNAME === "LBLADDCORRECTIVEACTIONS") {
                this.addChecklist = true;
              }
              if (subRole.PERMISSIONNAME === "LBLEDITCORRECTIVEACTIONS") {
                this.editChecklist = true;
              }
            });
          });
          if (role.PERMISSIONNAME === "LBLSUPPORT") {
            this.supportStatus = true;
          }
        }
      });
    });
    this.alertUnsafeService.getAlertUnsafeData({}).subscribe((res) => {
      const unsafeAlertValue = res["unsafeAlert"];
      if (unsafeAlertValue == 1) {
        this.alertUnafeRoute = "/email-inbox";
        this.unsafeALertTitle = 'LBLUNSAFEALTEMAIL';
      } else {
        this.alertUnafeRoute = "/alert-unsafe";
        this.unsafeALertTitle = "LBLRECEIVEDALYUNSAFEALTEMAIL";
      }

      this.isLoaded = true;
    });
    this.dashBoardValue = this.auth.dashboardInfo;
    this.dashboardService.getDashboardData({}).subscribe((res) => {
      this.dashboardData = res;
      this.userRoleID = this.auth.UserInfo["roleId"];
      this.roleCopyFrom = this.auth.UserInfo["rolesCopyFrom"];
      if (
        (this.userRoleID == 1 ||
          this.userRoleID == 2 ||
          this.userRoleID == 3 ||
          this.roleCopyFrom == 2 ||
          this.roleCopyFrom == 3 ||
          this.roleCopyFrom == -1) &&
        this.userRoleID != 5 &&
        this.userRoleID != 4
      ) {
        this.toggle = "admin";
        this.weeklyTarget = res["dashboard"][0]["WEEKLYTARGETCOUNT"];
        this.monthlyTarget = res["dashboard"][0]["MONTHLYTARGETCOUNT"];
        this.weeklyObs = res["dashboard"][0]["WEEKLYOBSCOUNT"];
        this.monthlyObs = res["dashboard"][0]["MONTHLYOBSCOUNT"];
        if (this.weeklyObs == "N/A" || this.weeklyObs == "--") {
          this.weeklyObs = 0;
        } else {
          this.weeklyObs = +this.weeklyObs;
        }
        if (this.monthlyObs == "N/A" || this.monthlyObs == "--") {
          this.monthlyObs = 0;
        } else {
          this.monthlyObs = +this.monthlyObs;
        }
        if (this.weeklyTarget > 0) {
          this.weeklyObsPercentage = Math.round(
            (this.weeklyObs / this.weeklyTarget) * 100
          );
        } else {
          this.weeklyObsPercentage = 0;
        }
        if (this.monthlyTarget > 0) {
          this.monthlyObsPercentage = Math.round(
            (this.monthlyObs / this.monthlyTarget) * 100
          );
        } else {
          this.monthlyObsPercentage = 0;
        }
        if (
          this.weeklyTarget == "N/A" ||
          this.weeklyTarget == "--" ||
          this.weeklyTarget == "0" ||
          this.weeklyTarget == undefined
        ) {
          this.weekChartVisible = false;
          this.commonWMessage = true;
        } else {
          this.weekChartVisible = true;
          this.commonWMessage = false;
        }
        setTimeout(() => {
          this.loadCharts();
        }, 2000);
        if (
          this.monthlyTarget == "N/A" ||
          this.monthlyTarget == "--" ||
          this.monthlyTarget == "0" ||
          this.monthlyTarget == undefined
        ) {
          this.monthChartVisible = false;
          this.commonMessage = true;
        } else {
          this.monthChartVisible = true;
          this.commonMessage = false;
        }
      } else {
        this.toggle = "nonAdmin";
        this.weeklyObs = this.dashboardData["dashboard"][0]["WEEKLYOBSCOUNT"];
        this.monthlyObs = this.dashboardData["dashboard"][0]["MONTHLYOBSCOUNT"];
        this.weeklyTarget = this.dashboardData["dashboard"][0][
          "WEEKLYTARGETCOUNT"
        ];
        this.monthlyTarget = this.dashboardData["dashboard"][0][
          "MONTHLYTARGETCOUNT"
        ];
      }

      this.dupontNews = this.dashboardData["dupontNews"][0]["NEWS"];
      if (this.dupontNews) {
        this.dupontNews = this.dupontNews.replace(/↵/, "");
      }
      if (document.getElementById("topbarEmailCount")) {
        document.getElementById(
          "topbarEmailCount"
        ).innerHTML = this.dashboardData["dashboard"][0]["INBOXCOUNT"];
      }
      this.approvalCount = this.dashboardData["dashboard"][0]["APPROVALCOUNT"];
      this.comingDue = this.dashboardData["dashboard"][0]["OPENWITHINRD"];
      this.pastDue = this.dashboardData["dashboard"][0]["OPENBEYONTRD"];
      this.roleName = this.dashboardData["dashboard"][0]["ROLENAME"];
      this.siteName = this.dashboardData["dashboard"][0]["SITENAME"];
      this.emailAlerts = this.dashboardData["dashboard"][0]["INBOXCOUNT"];
      this.unsafeEmailCount = this.dashboardData["dashboard"][0][
        "UNSAFEALERTMAILCOUNT"
      ];
    });
  }

  loadCharts() {
    if (this.weekChartVisible && this.monthChartVisible) {
      this.weekCanvas = document.getElementById("weekChart");
      this.ctx = this.weekCanvas.getContext("2d");
      this.monthCanvas = document.getElementById("monthChart");
      this.actionCtx = this.monthCanvas.getContext("2d");
      this.weekChart();
      this.monthChart();
    } else if (this.weekChartVisible) {
      this.weekCanvas = document.getElementById("weekChart");
      this.ctx = this.weekCanvas.getContext("2d");
      this.weekChart();
    } else if (this.monthChartVisible) {
      this.monthCanvas = document.getElementById("monthChart");
      this.actionCtx = this.monthCanvas.getContext("2d");
      this.monthChart();
    }

    Chart.plugins.register({
      beforeDraw: (chart) => {
        if (chart.config.centerText != undefined) {
          if (
            chart.config.centerText.display.display !== null &&
            typeof chart.config.centerText.display !== "undefined" &&
            chart.config.centerText.display
          ) {
            var width = chart.chart.width,
              height = chart.chart.height,
              ctx = chart.chart.ctx;
            ctx.restore();
            var fontSize = chart.config.centerText.fontSize;
            ctx.font = fontSize + "em sans-serif";
            ctx.textBaseline = "top";

            var text = chart.config.centerText.text,
              textX = Math.round((width - ctx.measureText(text).width) / 2),
              textY = height / 2 + 5;

            ctx.fillText(text, textX, textY);
            ctx.save();
          }
        }
      },
    });
  }
  monthChart() {
    var mothRemain =
      this.monthlyTarget - this.monthlyObs < 0
        ? 0
        : this.monthlyTarget - this.monthlyObs;
    this.translate.get("LBLBYMONTH").subscribe(label => {

      let monthChart = new Chart(this.actionCtx, {
        type: "doughnut",
        data: {
          labels: ["Completed", "Remaining"],
          datasets: [
            {
              data: [
                this.monthlyObs,
                this.monthlyTarget - this.monthlyObs < 0
                  ? 0
                  : this.monthlyTarget - this.monthlyObs,
              ],
              backgroundColor: ["#004809", "#FF0000"],
            },
          ],
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          cutoutPercentage: 70,
          legend: {
            onClick: function (event) { },
            display: false,
            position: "top",
            labels: {
              boxWidth: 12,
              padding: 35,
            },
          },
          tooltips: { enabled: true },
          hover: { mode: null },
          title: {
            display: true,
            text: label,
            fontSize: 17,
            fontColor: "#979797",
          },
        },
        centerText: {
          display: true,
          text: `${this.monthlyObsPercentage}%`,
          fontSize: window.innerWidth > 800 ? "2" : "1",
        },
      });
    })
  }
  weekChart() {
    var weekRemain =
      this.weeklyTarget - this.weeklyObs < 0
        ? 0
        : this.weeklyTarget - this.weeklyObs;
    this.translate.get("LBLBYWEEK").subscribe(label => {
      let weekChart = new Chart(this.ctx, {
        type: "doughnut",
        data: {
          labels: ["Completed", "Remaining"],
          datasets: [
            {
              data: [
                this.weeklyObs,
                this.weeklyTarget - this.weeklyObs < 0
                  ? 0
                  : this.weeklyTarget - this.weeklyObs,
              ],
              backgroundColor: ["#004809", "#FF0000"],
            },
          ],
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          cutoutPercentage: 70,
          legend: {
            onClick: function (event) { },
            display: false,
            position: "top",
            labels: {
              boxWidth: 12,
              padding: 35,
            },
          },
          tooltips: { enabled: true },
          hover: { mode: null },
          title: {
            display: true,
            text: label,
            fontSize: 17,
            fontColor: "#979797",
          },
        },
        centerText: {
          display: true,
          text: `${this.weeklyObsPercentage}%`,
          fontSize: window.innerWidth > 800 ? "2" : "1",
        },
      });
    })
  }
}
