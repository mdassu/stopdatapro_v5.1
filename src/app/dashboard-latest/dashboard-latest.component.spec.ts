import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardLatestComponent } from './dashboard-latest.component';

describe('DashboardLatestComponent', () => {
  let component: DashboardLatestComponent;
  let fixture: ComponentFixture<DashboardLatestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardLatestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardLatestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
