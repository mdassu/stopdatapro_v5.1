import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./_guards/auth.guard";
import { AppMainComponent } from "./app.main.component";
import { LoginComponent } from "./login/login.component";
import { TranslationsComponent } from "./setting_configurations/translations/translations.component";

export const routes: Routes = [
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "forgotPassword",
    loadChildren:
      "./forgot-password/forgot-password.module#ForgotPasswordModule"
  },
  {
    path: "forcePasswordChange",
    loadChildren:
      "./force-password-change/force-password-change.module#ForcePasswordChangeModule"
  },
  {
    path: "",
    component: AppMainComponent,
    children: [
      {
        path: "dashboard",
        loadChildren:
          "./dashboard-latest/dashboard-latest.module#DashboardLatestModule"
      },
      {
        path: "users",
        loadChildren: "./user_management/users/users.module#UsersModule"
      },
      {
        path: "add-users",
        loadChildren: "./user_management/add-user/add-user.module#AddUserModule"
      },
      {
        path: "edit-user/:userid",
        loadChildren:
          "./user_management/edit-user/edit-user.module#EditUserModule"
      },
      {
        path: "group-listing",
        loadChildren:
          "./user_management/group-listing/group-listing.module#GroupListingModule"
      },
      {
        path: "add-group",
        loadChildren:
          "./user_management/add-group/add-group.module#AddGroupModule"
      },
      {
        path: "edit-group/:groupId",
        loadChildren:
          "./user_management/edit-group/edit-group.module#EditGroupModule"
      },
      {
        path: "group-translate",
        loadChildren:
          "./user_management/group-translations/group-translations.module#GroupTranslationsModule"
      },
      {
        path: "role-listing",
        loadChildren:
          "./user_management/role-listing/role-listing.module#RoleListingModule"
      },
      {
        path: "add-role",
        loadChildren: "./user_management/add-role/add-role.module#AddRoleModule"
      },
      {
        path: "edit-role/:roleId",
        loadChildren:
          "./user_management/edit-role/edit-role.module#EditRoleModule"
      },
      {
        path: "targets",
        loadChildren: "./user_management/targets/targets.module#TargetsModule"
      },

      //site management

      {
        path: "site-listing",
        loadChildren:
          "./site_management/sites-listing/sites-listing.module#SitesListingModule"
      },
      {
        path: "add-site",
        loadChildren: "./site_management/add-site/add-site.module#AddSiteModule"
      },
      {
        path: "edit-site/:siteid",
        loadChildren:
          "./site_management/edit-site/edit-site.module#EditSiteModule"
      },
      {
        path: "area-listing",
        loadChildren:
          "./site_management/area-listing/area-listing.module#AreaListingModule"
      },
      {
        path: "add-area",
        loadChildren: "./site_management/add-area/add-area.module#AddAreaModule"
      },
      {
        path: "edit-area/:areaid",
        loadChildren:
          "./site_management/edit-area/edit-area.module#EditAreaModule"
      },
      {
        path: "subarea-listing",
        loadChildren:
          "./site_management/sub-area-listing/sub-area-listing.module#SubAreaListingModule"
      },
      {
        path: "add-sub-area",
        loadChildren:
          "./site_management/add-subarea/add-subarea.module#AddSubareaModule"
      },
      {
        path: "edit-subarea/:subareaid",
        loadChildren:
          "./site_management/edit-subarea/edit-subarea.module#EditSubareaModule"
      },
      {
        path: "shifts-listing",
        loadChildren:
          "./site_management/shifts-listing/shifts-listing.module#ShiftsListingModule"
      },
      {
        path: "add-shift",
        loadChildren:
          "./site_management/add-shift/add-shift.module#AddShiftModule"
      },
      {
        path: "edit-shift/:shiftid",
        loadChildren:
          "./site_management/edit-shift/edit-shift.module#EditShiftModule"
      },
      {
        path: "add-field-value/:userdefindid",
        loadChildren:
          "./site_management/user-defind-fields/user-defind-fields.module#UserDefindFieldsModule"
      },
      {
        path: "user-defind-field",
        loadChildren:
          "./site_management/user-defined-field-listing/user-defined-field-listing.module#UserDefinedFieldListingModule"
      },
      {
        path: "user-defind-translations",
        loadChildren:
          "./site_management/user-defined-fields-translations/user-defined-fields-translations.module#UserDefinedFieldsTranslationsModule"
      },

      //Checklist Configuration

      {
        path: "categories-listing",
        loadChildren:
          "./checklist_configuration/category-listing/category-listing.module#CategoryListingModule"
      },
      {
        path: "add-categories",
        loadChildren:
          "./checklist_configuration/add-category/add-category.module#AddCategoryModule"
      },
      {
        path: "edit-categories/:id",
        loadChildren:
          "./checklist_configuration/edit-category/edit-category.module#EditCategoryModule"
      },
      {
        path: "categories-translate",
        loadChildren:
          "./checklist_configuration/category-translations/category-translations.module#CategoryTranslationsModule"
      },
      {
        path: "checklist-setup-listing",
        loadChildren:
          "./checklist_configuration/checklist-setup-listing/checklist-setup-listing.module#ChecklistSetupListingModule"
      },
      {
        path: "add-checklist-setup",
        loadChildren:
          "./checklist_configuration/add-checklist-setup/add-checklist-setup.module#AddChecklistSetupModule"
      },
      {
        path: "checklist-edit/:id",
        loadChildren:
          "./checklist_configuration/edit-checklist-setup/edit-checklist-setup.module#EditChecklistSetupModule"
      },

      //data Entry

      {
        path: "observation-listing",
        loadChildren:
          "./data_entry/observation-listing/observation-listing.module#ObservationListingModule"
      },
      {
        path: "add-observation-list",
        loadChildren:
          "./data_entry/add-observatio-check/add-observatio-check.module#AddObservatioCheckModule"
      },
      {
        path: "edit-observation-list/:cardId/:cardReqId",
        loadChildren:
          "./data_entry/edit-observation-list/edit-observation-list.module#EditObservationListModule"
      },
      {
        path: "corrective-action-list/:caType",
        loadChildren:
          "./data_entry/corrective-action-listing/corrective-action-listing.module#CorrectiveActionListingModule"
      },
      {
        path: "corrective-action-list",
        loadChildren:
          "./data_entry/corrective-action-listing/corrective-action-listing.module#CorrectiveActionListingModule"
      },
      {
        path: "add-corrective-actions",
        loadChildren:
          "./data_entry/add-corrective-actions/add-corrective-actions.module#AddCorrectiveActionsModule"
      },
      {
        path: "edit-corrective-action-listing/:cardId/:caId/:stopCardType",
        loadChildren:
          "./data_entry/edit-corrective-action-listing/edit-corrective-action-listing.module#EditCorrectiveActionListingModule"
      },
      {
        path: "red-flags-listing",
        loadChildren:
          "./data_entry/red-flags-list/red-flags-list.module#RedFlagsListModule"
      },
      {
        path: "add-red-flags",
        loadChildren:
          "./data_entry/add-red-flags/add-red-flags.module#AddRedFlagsModule"
      },
      {
        path: "edit-red-flags/:redflagid",
        loadChildren:
          "./data_entry/edit-red-flags/edit-red-flags.module#EditRedFlagsModule"
      },
      {
        path: "injury-statistics-listing",
        loadChildren:
          "./data_entry/injury-statistics-listing/injury-statistics-listing.module#InjuryStatisticsListingModule"
      },
      {
        path: "add-injury-statistics",
        loadChildren:
          "./data_entry/add-injury-statistics/add-injury-statistics.module#AddInjuryStatisticsModule"
      },
      {
        path: "edit-injury-statistics/:injuryid",
        loadChildren:
          "./data_entry/edit-injury-statistics/edit-injury-statistics.module#EditInjuryStatisticsModule"
      },
      {
        path: "approval-listing",
        loadChildren:
          "./data_entry/approval-listing/approval-listing.module#ApprovalListingModule"
      },
      {
        path: "edit-approval/:approvalId",
        loadChildren:
          "./data_entry/add-approval/add-approval.module#AddApprovalModule"
      },
      {
        path: "edit-groups-on-checklists",
        loadChildren:
          "./data_entry/edit-groups-on-checklists/edit-groups-on-checklists.module#EditGroupsOnChecklistsModule"
      },
      {
        path: "edit-groups-checklists/:userid",
        loadChildren:
          "./data_entry/edit-groups-checklists/edit-groups-checklists.module#EditGroupsChecklistsModule"
      },

      // settings & configuration

      {
        path: "global-options",
        loadChildren:
          "./setting_configurations/global-options/global-options.module#GlobalOptionsModule"
      },

      {
        path: "translations",
        component: TranslationsComponent,
        canActivate: [AuthGuard],
        data: { label: "LBLTRANSLATION" }
      },

      // Report Management

      {
        path: "reports",
        loadChildren: "./report_management/reports/reports.module#ReportsModule"
      },
      {
        path: "report-filters/:reportId",
        loadChildren:
          "./report_management/filter-report/filter-report.module#FilterReportModule"
      },
      {
        path: "edit-reports/:reportId",
        loadChildren:
          "./report_management/edit-report/edit-report.module#EditReportModule"
      },
      {
        path: "report-results/:id",
        loadChildren:
          "./report_management/report-results/report-results.module#ReportResultsModule"
      },
      {
        path: "schedule-reports",
        loadChildren:
          "./report_management/schedule-reports/schedule-reports.module#ScheduleReportsModule"
      },
      {
        path: "add-schedule-reports",
        loadChildren:
          "./report_management/add-schedule-reports/add-schedule-reports.module#AddScheduleReportsModule"
      },
      {
        path: "edit-schedule-reports/:scheduleId",
        loadChildren:
          "./report_management/edit-schedule-reports/edit-schedule-reports.module#EditScheduleReportsModule"
      },
      {
        path: "analytics-dashboard",
        loadChildren:
          "./report_management/analytics-dashboard/analytics-dashboard.module#AnalyticsDashboardModule"
      },

      //email configuration
      {
        path: "email-listing",
        loadChildren:
          "./email_configuration/email-listing/email-listing.module#EmailListingModule"
      },
      {
        path: "add-email",
        loadChildren:
          "./email_configuration/add-email/add-email.module#AddEmailModule"
      },
      {
        path: "edit-email/:id",
        loadChildren:
          "./email_configuration/edit-email/edit-email.module#EditEmailModule"
      },

      // Support
      {
        path: "technical-requirements",
        loadChildren:
          "./support/technical-requirements/technical-requirements.module#TechnicalRequirementsModule"
      },
      {
        path: "contact-us",
        loadChildren: "./support/contact-us/contact-us.module#ContactUsModule"
      },
      {
        path: "user-profile",
        loadChildren: "./user-profile/user-profile.module#UserProfileModule"
      },
      {
        path: "email-inbox",
        loadChildren: "./email-inbox/email-inbox.module#EmailInboxModule"
      },
      {
        path: "alert-unsafe",
        loadChildren: "./alert-unsafe/alert-unsafe.module#AlertUnsafeModule"
      }
    ]
  }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, {
  scrollPositionRestoration: "enabled"
});
