export interface Users {
  USERID: string;
  FULLNAME: string;
}

export interface Sites {
  SITEID: number;
  SITENAME: string;
}

export interface MainCategories {
  MAINCATEGORYID: number;
  ENTITYVALUE: string;
}

export interface SubCatagories {
  SUBCATEGORYID: number;
  ENTITYVALUE: string;
  MAINCATEGORYID: number;
}

export interface General {
  SETUPNAME: string;
  COPYFROM: string;
  ALLSAFECHECK: number;
  ALLSITES: number;
  STATUS: number;
  ENABLESAFEUNSAFE: number;
}

export interface timeZone {
  SITEID: string;
  TIMEZONEID: string;
  TIMEZONENAME: string;
}

export interface language {
  LANGUAGEID: number;
  LANGUAGENAME: string;
}