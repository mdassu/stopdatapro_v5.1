import {
  HTTP_INTERCEPTORS,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of, throwError } from "rxjs";
import { delay, dematerialize, materialize, mergeMap } from "rxjs/operators";
import { Department } from "../_models/department";
import { Division } from "../_models/division";
import { Group } from "../_models/group";
import { ObserverStatus } from "../_models/observerStatus";
import { Region } from "../_models/region";
import { Role } from "../_models/role";
import { Site } from "../_models/site";
import { Title } from "../_models/title";
import { User } from "../_models/user";
import { UserLevel } from "../_models/userlevel";

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const users: User[] = [
      {
        id: 1,
        username: "dup-sa",
        password: "sdp4",
        firstName: "Super Admin",
        lastName: "User",
        email: "dupsa@gmail.com",
        status: "Active",
        role: Role.superAdministrator,
        confirmPassword: "sdp4",
        defaultSite: "",
        dateFormat: ""
      },
      {
        id: 2,
        username: "dup-site",
        password: "sdp4",
        firstName: "Normal",
        lastName: "User",
        email: "dupsite@gmail.com",
        status: "Active",
        role: Role.siteAdministrator,
        confirmPassword: "sdp4",
        defaultSite: "",
        dateFormat: ""
      }
    ];

    const superAdmin = {
      "User Management": ["Users", "Groups", "Roles", "Targets"],
      "Site Management": [
        "Sites",
        "Areas",
        "Sub Areas",
        "Shifts",
        "User Defined Fields"
      ],
      "Checklist Configuration": ["Categories", "Checklist Setup"],
      "Data Entry": [
        "Add New Observer",
        "Edit Observation",
        "Observation Checklist",
        "Corrective Actions",
        "Red Flags",
        "Injury Statistics",
        "Approval",
        "Edit Groups On Checklist"
      ],
      "Settings & Configurations": ["Global Options", "Translations"],
      "Report Management": ["Reports", "Shedule Reports"],
      "Email Configurations": ["Email Notifications"],
      Support: ["Alert Unsafe", "Technical Requirements", "Contact Us"]
    };

    const siteAdmin = {
      "User Management": ["Users"],
      "Site Management": ["Sites", "Areas", "Sub Areas", "Shifts"],
      "Checklist Configuration": ["Checklist Setup"],
      "Data Entry": [
        "Edit Obseravtion",
        "Observation Checklist",
        "Corrective Actions",
        "Red Flags",
        "Approval"
      ],
      "Report Management": ["Reports", "Shedule Reports"],
      Support: ["Alert Unsafe", "Technical Requirements", "Contact Us"]
    };

    const sites: Site[] = [
      // {
      //   id: 1,
      //   siteName: "Chennai",
      //   country: "India",
      //   city: "Taramani Chennai",
      //   status: "Active"
      // },
      // {
      //   id: 2,
      //   siteName: "Galway",
      //   country: "Ireland",
      //   city: "Galway",
      //   status: "Active"
      // },
      // {
      //   id: 3,
      //   siteName: "Virginia Beach",
      //   country: "United States Of America",
      //   city: "Virginia Beach",
      //   status: "Active"
      // }
    ];
    const regions: Region[] = [
      {
        id: 1,
        regionName: "Asia-Pac"
      },
      {
        id: 2,
        regionName: "CentroAmerica"
      },
      {
        id: 3,
        regionName: "Europe"
      }
    ];
    const groups: Group[] = [
      {
        id: 1,
        groupName: "Administration",
        status: "Active"
      },
      {
        id: 2,
        groupName: "Engineering",
        status: "Active"
      },
      {
        id: 3,
        groupName: "Manufacturing",
        status: "Active"
      }
    ];
    const divisions: Division[] = [
      {
        id: 1,
        divisionName: "Administration"
      },
      {
        id: 2,
        divisionName: "Engineering"
      },
      {
        id: 3,
        divisionName: "Manufacturing"
      }
    ];
    const departments: Department[] = [
      {
        id: 1,
        departmentName: "Operations"
      },
      {
        id: 2,
        departmentName: "Packaging"
      },
      {
        id: 3,
        departmentName: "Quality Control"
      }
    ];
    const titles: Title[] = [
      {
        id: 1,
        titleName: "EHS Manager"
      },
      {
        id: 2,
        titleName: "IH Engineer"
      },
      {
        id: 3,
        titleName: "Safety Engineer"
      }
    ];
    const observerstatus: ObserverStatus[] = [
      {
        id: 1,
        status: "Active"
      },
      {
        id: 2,
        status: "Inactive"
      },
      {
        id: 3,
        status: "Locked"
      }
    ];
    const userlevels: UserLevel[] = [
      {
        id: 1,
        userLevelName: "User"
      },
      {
        id: 2,
        userLevelName: "Observer"
      },
      {
        id: 3,
        userLevelName: "User & Observer"
      }
    ];
    const authHeader = request.headers.get("Authorization");
    const isLoggedIn =
      authHeader && authHeader.startsWith("Bearer fake-jwt-token");
    const roleString = isLoggedIn && authHeader.split(".")[1];
    const role = roleString ? Role[roleString] : null;

    // wrap in delayed observable to simulate server api call
    return (
      of(null)
        .pipe(
          // tslint:disable-next-line: cyclomatic-complexity
          mergeMap(() => {
            // authenticate - public
            if (
              request.url.endsWith("/users/authenticate") &&
              request.method === "POST"
            ) {
              const user = users.find(
                x =>
                  x.username === request.body.username &&
                  x.password === request.body.password
              );
              if (!user) {
                return error("Username or password is incorrect");
              }

              return ok({
                id: user.id,
                username: user.username,
                firstName: user.firstName,
                lastName: user.lastName,
                role: user.role,
                token: `fake-jwt-token.${user.role}`
              });
            }

            // get all users
            if (request.url.endsWith("/users") && request.method === "GET") {
              return ok(users);
            }
            //get LoggedIn user data

            if (
              request.url.match(/\/users\/\d+$/) &&
              request.method === "GET"
            ) {
              let urlParts = request.url.split("/");
              let id = parseInt(urlParts[urlParts.length - 1]);
              const user = users.find(user => user.id == id);
              return ok(user);
            }
            // get all usernames
            if (
              request.url.endsWith("/users/username") &&
              request.method === "GET"
            ) {
              const users1 = [];
              users.forEach(user => {
                users1.push({ label: user["username"], value: user["id"] });
              });
              return ok(users1);
            }

            // get all sites
            if (request.url.endsWith("/sites") && request.method === "GET") {
              const sites1 = [];
              sites.forEach(site => {
                sites1.push({
                  label: site["siteName"],
                  value: site["siteName"]
                });
              });

              return ok(sites1);
            }

            // get all regions
            if (request.url.endsWith("/regions") && request.method === "GET") {
              const regions1 = [];
              regions.forEach(region => {
                regions1.push({
                  label: region["regionName"],
                  value: region["regionName"]
                });
              });

              return ok(regions1);
            }

            // get all groups
            if (request.url.endsWith("/groups") && request.method === "GET") {
              const groups1 = [];
              groups.forEach(group => {
                groups1.push({
                  label: group["groupName"],
                  value: group["groupName"]
                });
              });

              return ok(groups1);
            }

            // get all divisions
            if (
              request.url.endsWith("/divisions") &&
              request.method === "GET"
            ) {
              const divisions1 = [];
              divisions.forEach(division => {
                divisions1.push({
                  label: division["divisionName"],
                  value: division["divisionName"]
                });
              });

              return ok(divisions1);
            }

            // get all departments
            if (
              request.url.endsWith("/departments") &&
              request.method === "GET"
            ) {
              const departments1 = [];
              departments.forEach(department => {
                departments1.push({
                  label: department["departmentName"],
                  value: department["departmentName"]
                });
              });

              return ok(departments1);
            }
            // get all titles
            if (request.url.endsWith("/titles") && request.method === "GET") {
              const titles1 = [];
              titles.forEach(title => {
                titles1.push({
                  label: title["titleName"],
                  value: title["titleName"]
                });
              });

              return ok(titles1);
            }
            // get all observerstatus
            if (
              request.url.endsWith("/observerstatus") &&
              request.method === "GET"
            ) {
              const observerstatus1 = [];
              observerstatus.forEach(observerStatus => {
                observerstatus1.push({
                  label: observerStatus["status"],
                  value: observerStatus["status"]
                });
              });

              return ok(observerstatus1);
            }

            // get all Userlevels
            if (
              request.url.endsWith("/userlevels") &&
              request.method === "GET"
            ) {
              const userlevel1 = [];
              userlevels.forEach(userLevel => {
                userlevel1.push({
                  label: userLevel["userLevelName"],
                  value: userLevel["userLevelName"]
                });
              });

              return ok(userlevel1);
            }
            // get all GroupData
            if (request.url.endsWith("/grpdata") && request.method === "GET") {
              return ok(groups);
            }

            // get role data
            if (
              request.url.endsWith("/role/data") &&
              request.method === "POST"
            ) {
              if (request.body.role === "Super Administrator") {
                return ok(superAdmin);
              } else if (request.body.role === "Site Administrator") {
                return ok(siteAdmin);
              }
            }

            // pass through any requests not handled above
            return next.handle(request);
          })
        )
        // call materialize and dematerialize to ensure delay even if an error is thrown
        // (https://github.com/Reactive-Extensions/RxJS/issues/648)
        .pipe(materialize())
        .pipe(delay(500))
        .pipe(dematerialize())
    );

    // private helper functions

    function ok(body) {
      return of(new HttpResponse({ status: 200, body }));
    }

    function unauthorised() {
      return throwError({ status: 401, error: { message: "Unauthorised" } });
    }

    function error(message) {
      return throwError({ status: 400, error: { message } });
    }
  }
}

export let fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
