import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpfileComponent } from './helpfile.component';

describe('HelpfileComponent', () => {
  let component: HelpfileComponent;
  let fixture: ComponentFixture<HelpfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
