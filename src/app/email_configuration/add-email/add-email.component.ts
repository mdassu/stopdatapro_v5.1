import { Component, OnInit, AfterViewInit, OnDestroy } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { MessageService } from "primeng/api";
import { EmailConfigurationService } from "src/app/_services/email-configuration.service";
import { SlideUpDownAnimations } from "src/app/_animations/slide-up-down.animations";

import tinymce from "tinymce/tinymce";

// A theme is also required
import "tinymce/themes/modern/theme";

// Any plugins you want to use has to be imported
import "tinymce/plugins/paste";
import "tinymce/plugins/link";
import "tinymce/plugins/table";
import "tinymce/plugins/preview";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";
import { CookieService } from "ngx-cookie-service";
import { UtilService } from "src/app/_services/util.service";
import { AuthenticationService } from "src/app/_services/authentication.service";

@Component({
  selector: "app-add-email",
  templateUrl: "./add-email.component.html",
  styleUrls: ["./add-email.component.css"],
  providers: [MessageService, CustomValidatorsService],
  animations: [SlideUpDownAnimations],
})
export class AddEmailComponent implements OnInit, OnDestroy {
  // Varialbe for fields name
  AddEmailGeneral: FormGroup;
  Subject: any;
  maximumEmailUser: any;
  ReplyTo: any;
  EmailOptionName: any;
  BodyContent: any;
  submitted: boolean;
  emailType: any;
  mailDetailsFrom: any;
  sites: any;
  region: any;
  groups: any;
  divisions: any;
  departments: any;
  designation: any;
  roles: any;
  sendThisMail: any = [];
  startTime: any = [];
  status: any;
  animationState = "out";
  filterData: any[] = [];
  optionalFilter: any[] = [];
  selectedFilter: any = [];
  show_advance_search: boolean = false;
  keywords: any;
  menuList = [];
  Keyword: any = "Keywords";
  selectedMailType: number = 1;
  days = [];
  scheduleTypeId: number;
  filterGroupList = [];
  isLoading: boolean = true;
  iconType = "ui-icon-add";
  filterSite: any[] = [];
  scheduleStatus: boolean = true;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private emailService: EmailConfigurationService,
    private translate: TranslateService,
    private customValidatorsService: CustomValidatorsService,
    private router: Router,
    private cookieService: CookieService,
    private util: UtilService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLEMAILCONFIG", url: "./assets/help/add-email.md" },
      { label: "LBLMAILLIST", routerLink: ["/email-listing"] },
    ]);

    this.translate.get("LBLKEYWORDS").subscribe((res) => {
      this.Keyword = res;
    });
    this.translate
      .get(["LBLDAY", "LBLWEEKLY", "LBLMONTHLY"])
      .subscribe((res) => {
        const temparray = [];
        temparray.push(
          { name: res["LBLDAY"], id: 1 },
          { name: res["LBLWEEKLY"], id: 2 },
          { name: res["LBLMONTHLY"], id: 3 }
        );
        this.sendThisMail = temparray;
      });
    this.status = [];
    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((res) => {
      let temparray = [];
      temparray.push(
        { name: res["LBLACTIVE"], id: 1 },
        { name: res["LBLINACTIVE"], id: 0 }
      );
      this.status = temparray;
    });
  }

  ngOnInit() {
    this.startTime = this.util.startTime;

    this.days = Array(28)
      .fill(0)
      .map((e, i) => {
        const tempObj = {};
        tempObj["label"] = i + 1;
        tempObj["value"] = i + 1;
        return tempObj;
      });

    this.loadData({});

    this.AddEmailGeneral = this.fb.group({
      EmailOptionName: new FormControl(null, [
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      ReplyTo: new FormControl(null, [
        Validators.required,
        Validators.email,
        this.customValidatorsService.noCommaValidator,
        this.customValidatorsService.noSpaceValidator,
        // Validators.pattern(
        //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
        // ),
        this.customValidatorsService.isEmailValid,
      ]),
      Subject: new FormControl(null, [
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      BodyContent: new FormControl(null, Validators.required),
      MaximumEmailUser: new FormControl(null, [
        Validators.required,
        Validators.maxLength(2),
        Validators.min(1),
        Validators.pattern("[0-9]*"),
      ]),
      siteId: new FormControl(null),
      roleId: new FormControl(null),
      Day: new FormControl(1),
      SendThisMail: new FormControl(1),
      StartTime: new FormControl(null),
      EmailType: new FormControl(null),
      ScheduleId: new FormControl(null),
      CopyFrom: new FormControl(null, [
        this.customValidatorsService.noCommaValidator,
        Validators.pattern(
          "^(\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*;)*(\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*)$"
        ),
      ]),
      Status: new FormControl(null),
      DayList: new FormControl(null),
      EveryDWM: new FormControl(null),
      DWM: new FormControl(null),
    });
  }

  onChangeEmailType(data) {
    this.getEmailTypeData(data.SCHEDULETYPEID);
    this.scheduleTypeId = data.SCHEDULETYPEID;
    if (this.scheduleTypeId == 3 || this.scheduleTypeId == 4) {
      this.scheduleStatus = false;
      this.AddEmailGeneral.get("MaximumEmailUser").clearValidators();
      this.AddEmailGeneral.get("MaximumEmailUser").updateValueAndValidity();
    } else {
      this.scheduleStatus = true;
    }
    this.AddEmailGeneral.controls["ScheduleId"].setValue(null);
    this.translate
      .get(["LBLDAY", "LBLWEEKLY", "LBLMONTHLY"])
      .subscribe((res) => {
        if (this.scheduleTypeId == 7) {
          this.sendThisMail = [{ name: res["LBLWEEKLY"], id: 2 }];
          this.AddEmailGeneral.controls["SendThisMail"].setValue(2);
          this.onSendThisMailChange({ id: 2 });
        } else if (this.scheduleTypeId == 8) {
          this.sendThisMail = [
            { name: res["LBLWEEKLY"], id: 2 },
            { name: res["LBLMONTHLY"], id: 3 },
          ];
          this.AddEmailGeneral.controls["SendThisMail"].setValue(2);
          this.onSendThisMailChange({ id: 2 });
        } else {
          this.sendThisMail = [
            { name: res["LBLDAY"], id: 1 },
            { name: res["LBLWEEKLY"], id: 2 },
            { name: res["LBLMONTHLY"], id: 3 },
          ];
          this.AddEmailGeneral.controls["SendThisMail"].setValue(1);
          this.onSendThisMailChange({ id: 1 });
        }
      });
  }

  onChangeEmailDetailsFrom(data) {
    this.getEmaiDetailFrom(data.SCHEDULEID);
  }

  getEmaiDetailFrom(scheduleId) {
    this.emailService.getEmaiDetailFrom({ scheduleId }).subscribe((res) => {
      if (res["status"]) {
        this.AddEmailGeneral.patchValue({
          Subject: res["Data"].SUBJECT,
          BodyContent: res["Data"].BODY,
          CopyFrom: res["Data"].CC,
          ReplyTo: res["Data"].REPLYTO,
        });
        tinymce.get("tinymceEditor").setContent(res["Data"].BODY);
        this.getEditBody(this.scheduleTypeId);
      } else {
        this.AddEmailGeneral.patchValue({
          Subject: "",
          BodyContent: "",
          CopyFrom: "",
          ReplyTo: "",
        });
        tinymce.get("tinymceEditor").setContent("");
        this.getEditBody(this.scheduleTypeId);
      }
    });
  }
  onSendThisMailChange(data) {
    this.selectedMailType = data.id;
    if (this.selectedMailType === 2) {
      this.AddEmailGeneral.get("EveryDWM").clearValidators();
      this.AddEmailGeneral.get("EveryDWM").reset();
      this.AddEmailGeneral.get("EveryDWM").updateValueAndValidity();
      this.AddEmailGeneral.get("DWM").clearValidators();
      this.AddEmailGeneral.get("DWM").reset();
      this.AddEmailGeneral.get("DWM").updateValueAndValidity();
      this.AddEmailGeneral.get("DayList").setValidators([Validators.required]);
      this.AddEmailGeneral.get("DayList").updateValueAndValidity();
    } else if (this.selectedMailType === 3) {
      this.AddEmailGeneral.get("DayList").clearValidators();
      this.AddEmailGeneral.get("DayList").reset();
      this.AddEmailGeneral.get("DayList").updateValueAndValidity();
      this.AddEmailGeneral.get("EveryDWM").setValidators([Validators.required]);
      this.AddEmailGeneral.get("EveryDWM").updateValueAndValidity();
      this.AddEmailGeneral.get("DWM").setValidators([Validators.required]);
      this.AddEmailGeneral.get("DWM").updateValueAndValidity();
    }
  }

  getEmailTypeData(scheduleTypeId) {
    this.emailService.getEmailTypeData({ scheduleTypeId }).subscribe((res) => {
      if (res["status"]) {
        this.mailDetailsFrom = res["emailType"];
        this.AddEmailGeneral.patchValue({
          Subject: res["emailDetail"].DEFAULTSUBJECT,
          BodyContent: res["emailDetail"].TEMPLATEBODY,
        });
        if (tinymce.get("tinymceEditor")) {
          tinymce
            .get("tinymceEditor")
            .setContent(res["emailDetail"].TEMPLATEBODY);
        }

        this.getEditBody(scheduleTypeId);
      }
    });
  }

  getEditBody(scheduleTypeId) {
    this.emailService.getEditBody({ scheduleTypeId }).subscribe((res) => {
      if (res["status"]) {
        this.keywords = JSON.parse(res["emailBody"]["KEYWORD"]);

        let items = this.keywords.map((item) => {
          this.translate.get(item.label).subscribe((res) => {
            item["label"] = res;
          });

          return item;
        });
        const menuList = [];
        items.forEach((item) => {
          let tempItem = {};
          tempItem["text"] = item["label"];
          tempItem[
            "value"
          ] = `&nbsp;<span id=${item["keyWordId"]} class="mceNonEditable" contenteditable="false"><strong>[${item["label"]}]</strong></span>&nbsp;`;
          menuList.push(tempItem);
        });

        var languageTinymce = "";
        let currentUser = this.auth.UserInfo;
        if (currentUser.languageCode == "da-dk") {
          var languageTinymce = "da";
        } else if (currentUser.languageCode == "de-de") {
          var languageTinymce = "de";
        } else if (currentUser.languageCode == "el") {
          var languageTinymce = "el";
        } else if (currentUser.languageCode == "en-us") {
          var languageTinymce = "en";
        } else if (currentUser.languageCode == "es-es") {
          var languageTinymce = "es";
        } else if (currentUser.languageCode == "fr-fr") {
          var languageTinymce = "fr-FR";
        } else if (currentUser.languageCode == "hu") {
          var languageTinymce = "hu_HU";
        } else if (currentUser.languageCode == "it-it") {
          var languageTinymce = "it";
        } else if (currentUser.languageCode == "ja-jp") {
          var languageTinymce = "ja";
        } else if (currentUser.languageCode == "ko") {
          var languageTinymce = "ko_KR";
        } else if (currentUser.languageCode == "nl-nl") {
          var languageTinymce = "nl";
        } else if (currentUser.languageCode == "pl-pl") {
          var languageTinymce = "pl";
        } else if (currentUser.languageCode == "pt-pt") {
          var languageTinymce = "pt_PT";
        } else if (currentUser.languageCode == "ru-ru") {
          var languageTinymce = "ru";
        } else if (currentUser.languageCode == "th-th") {
          var languageTinymce = "th_TH";
        } else if (currentUser.languageCode == "zh-cn") {
          var languageTinymce = "zh_CN";
        }

        var that = this;
        tinymce.remove();
        tinymce.init({
          selector: "#tinymceEditor",
          base_url: "/tinymce",
          language: languageTinymce,
          height: "200",
          suffix: ".min",
          skin_url: "tinymce/skins/lightgray",
          plugins: "table preview",
          menubar: false,
          toolbar:
            "bold italic underline alignleft aligncenter alignright alignjustify numlist outdent indent table fontselect fontsizeselect mybutton preview ",
          setup: function (editor) {
            editor.addButton("mybutton", {
              type: "listbox",
              text: that.Keyword,
              icon: false,
              onselect: function (e) {
                editor.insertContent(this.value());
              },
              values: menuList,
            });
            editor.on("change", function (e) {
              that.AddEmailGeneral.patchValue({
                BodyContent: editor.getContent(),
              });
              that.AddEmailGeneral.get("BodyContent").markAsDirty();
            });
          },
        });
      }
    });
  }

  loadData(data) {
    this.filterData = [];
    this.optionalFilter = [];
    this.emailService.getEmailAdd(data).subscribe((res) => {
      this.emailType = res["emailType"];
      this.mailDetailsFrom = res["schedule"];
      this.translate.get("LBLNONE").subscribe((noneValue) => {
        this.mailDetailsFrom.push({
          SCHEDULEID: -1,
          SCHEDULENAME: noneValue,
        });
        this.mailDetailsFrom = this.mailDetailsFrom.slice();
      });
      this.Subject = res["emailDetail"]["DEFAULTSUBJECT"];
      this.BodyContent = res["emailDetail"]["TEMPLATEBODY"];
      const siteFilterData = res["filterSite"];
      const optionalFilter = res["filterRole"];
      const groupFilterData = res["filterGroup"];

      //set filter data
      siteFilterData["field"] = "siteId";
      siteFilterData["Data"] = JSON.parse(siteFilterData["Data"]);
      siteFilterData["Data"] = siteFilterData["Data"].map((data) => {
        return { label: data["SITENAME"], value: data["SITEID"] };
      });
      this.filterSite.push(siteFilterData);
      groupFilterData.map((filterItem) => {
        this.filterGroupList.push(
          filterItem["REPORTPARAM"].toLowerCase() + "Id"
        );
        filterItem["field"] = filterItem["REPORTPARAM"].toLowerCase() + "Id";
        filterItem["Data"] = JSON.parse(filterItem["Data"]);
        filterItem["Data"] = filterItem["Data"].map((data) => {
          return { label: data["GROUPNAME"], value: data["GROUPID"] };
        });
        this.filterData.push(filterItem);
      });

      if (optionalFilter) {
        optionalFilter["field"] = "roleId";
        optionalFilter["Data"] = JSON.parse(optionalFilter["Data"]);
        optionalFilter["Data"] = optionalFilter["Data"].map((data) => {
          return { label: data["ROLENAME"], value: data["ROLEID"] };
        });
        // set advanced filter data
        this.optionalFilter.push(optionalFilter);
      }

      this.getEmailTypeData(this.emailType[0]["SCHEDULETYPEID"]);
      this.getEditBody(this.emailType[0]["SCHEDULETYPEID"]);

      this.AddEmailGeneral = this.fb.group({
        EmailOptionName: new FormControl(null, [
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
        ]),
        ReplyTo: new FormControl(null, [
          Validators.required,
          Validators.email,
          this.customValidatorsService.isEmailValid,
          this.customValidatorsService.noSpaceValidator,
          // Validators.pattern(
          //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
          // ),
          this.customValidatorsService.isEmailValid,
        ]),
        Subject: new FormControl(this.Subject, [
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
        ]),
        BodyContent: new FormControl(this.BodyContent, Validators.required),
        MaximumEmailUser: new FormControl(null, [
          Validators.required,
          Validators.maxLength(2),
          Validators.min(1),
          Validators.pattern("[0-9]*"),
        ]),
        siteId: new FormControl(null),
        roleId: new FormControl(null),
        Day: new FormControl(1),
        SendThisMail: new FormControl(1),
        StartTime: new FormControl("00:00"),
        EmailType: new FormControl(this.emailType[0]["SCHEDULETYPEID"]),
        ScheduleId: new FormControl(-1),
        CopyFrom: new FormControl(null, [
          // this.customValidatorsService.noCommaValidator,
          this.customValidatorsService.ccEmailValid,
          // Validators.pattern(
          //   "^(\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*;)*(\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*)$"
          // ),
        ]),
        Status: new FormControl(1),
        DayList: new FormControl(null),
        EveryDWM: new FormControl(null),
        DWM: new FormControl(null),
      });

      this.selectedMailType = 1;
      this.scheduleTypeId = this.emailType[0]["SCHEDULETYPEID"];
      if (tinymce.get("tinymceEditor")) {
        tinymce.get("tinymceEditor").setContent(this.BodyContent);
      }

      this.filterGroupList.forEach((group) => {
        this.AddEmailGeneral.addControl(group, new FormControl(""));
      });
      this.isLoading = false;
    });
  }

  advance_search() {
    this.iconType =
      this.iconType === "ui-icon-add" ? "ui-icon-remove" : "ui-icon-add";
    this.show_advance_search = !this.show_advance_search;
  }

  get GeneralSubForm() {
    return this.AddEmailGeneral.controls;
  }

  onSubmit(value: string) {
    this.AddEmailGeneral.patchValue({
      BodyContent: tinymce.get("tinymceEditor").getContent(),
    });
    const formData = this.AddEmailGeneral.value;
    const timeArray = formData["StartTime"].split(":");
    const body = {
      scheduleName: formData["EmailOptionName"],
      scheduleTypeId: formData["EmailType"],
      duplicateEmailFrom: formData["ScheduleId"],
      replyTo: formData["ReplyTo"],
      cc: formData["CopyFrom"],
      subject: formData["Subject"],
      body: tinymce.get("tinymceEditor").getContent(),
      status: formData["Status"],
      maxEmails: formData["MaximumEmailUser"],
      startTime: parseInt(timeArray[0]) * 60 + parseInt(timeArray[1]),
      siteId: formData["siteId"] ? formData["siteId"].join(",") : "",
      roleId: formData["roleId"] ? formData["roleId"].join(",") : "",
    };

    this.filterGroupList.forEach((group) => {
      body[group] = formData[group] ? formData[group].join(",") : "";
    });

    if (this.selectedMailType === 1) {
      body["dwm"] = 1;
      body["everyDwm"] = formData["Day"];
      body["day"] = 0;
      body["dayList"] = 0;
    } else if (this.selectedMailType === 2) {
      body["dwm"] = 2;
      body["everyDwm"] = 0;
      body["day"] = formData["DayList"];
      const tempDataArr = [0, 0, 0, 0, 0, 0, 0];
      tempDataArr[formData["DayList"]] = 1;
      body["dayList"] = tempDataArr.join("");
    } else if (this.selectedMailType === 3) {
      body["dwm"] = 3;
      body["everyDwm"] = formData["EveryDWM"];
      body["day"] = formData["DayList"];
      const tempDataArr = [0, 0, 0, 0, 0, 0, 0];
      tempDataArr[formData["DWM"]] = 1;
      body["dayList"] = tempDataArr.join("");
    }

    this.submitted = true;
    if (this.AddEmailGeneral.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.emailService.insertEmailConfig(body).subscribe((res) => {
        if (res["status"]) {
          this.translate.get(res["message"]).subscribe((message) => {
            this.cookieService.set("add-email", message);
            this.router.navigate(["/edit-email", res["scheduleId"]], {
              skipLocationChange: true,
            });
          });
        } else {
          this.translate.get(res["message"]).subscribe((message) => {
            this.messageService.add({
              severity: "error",
              detail: message,
            });
          });
        }
      });
    }
  }

  toggleFilterShow() {
    this.animationState = this.animationState === "out" ? "in" : "out";
  }

  ngOnDestroy(): void {
    var editorCache = tinymce.get("tinymceEditor"); //get from cache
    if (!editorCache) {
      editorCache.destroy();
    }
  }

  onSelectAllSite(i) {
    const selected = this.filterSite[i]["Data"].map((item) => item.value);
    this.AddEmailGeneral.controls[this.filterSite[i]["field"]].setValue(
      selected
    );
  }
  onClearAllSite(i) {
    this.AddEmailGeneral.controls[this.filterSite[i]["field"]].setValue([]);
  }

  onSelectAll(i) {
    const selected = this.filterData[i]["Data"].map((item) => item.value);
    this.AddEmailGeneral.controls[this.filterData[i]["field"]].setValue(
      selected
    );
  }
  onClearAll(i) {
    this.AddEmailGeneral.controls[this.filterData[i]["field"]].setValue([]);
  }
  onSelectAllAdv(i) {
    const selected = this.optionalFilter[i]["Data"].map((item) => item.value);
    this.AddEmailGeneral.controls[this.optionalFilter[i]["field"]].setValue(
      selected
    );
  }

  onClearAllAdv(i) {
    this.AddEmailGeneral.controls[this.optionalFilter[i]["field"]].setValue([]);
  }

  onReset() {
    this.loadData({});
  }
}
