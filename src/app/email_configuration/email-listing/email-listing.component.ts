import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ConfirmationService, MessageService } from "primeng/api";
import { EmailConfigurationService } from "src/app/_services/email-configuration.service";
import { TranslateService } from "@ngx-translate/core";
import { Router, RoutesRecognized } from "@angular/router";
import { SlideUpDownAnimations } from "src/app/_animations/slide-up-down.animations";
import { EnvService } from "src/env.service";
import { filter, pairwise } from "rxjs/operators";
import { AuthenticationService } from "../../_services/authentication.service";
@Component({
  selector: "app-email-listing",
  templateUrl: "./email-listing.component.html",
  styleUrls: ["./email-listing.component.css"],
  providers: [ConfirmationService, MessageService],
  animations: [SlideUpDownAnimations],
})
export class EmailListingComponent implements OnInit {
  cols: any[];
  colsCount: any;
  emailList: any;
  searchBy: any;
  emailType: any;
  status: any;
  selectedField: any;
  selectedEmailType: any;
  selectedData: any = [];
  selectedStatus: any;
  isLoaded: boolean;
  searchInput: any;
  animationState = "out";
  confirmClass: any;
  appName: any;
  parameters: any;
  previousUrl: any;
  userId: any;
  roleId: any;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private emailService: EmailConfigurationService,
    private translate: TranslateService,
    private readonly router: Router,
    private auth: AuthenticationService,
    private env: EnvService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLEMAILCONFIG", url: "./assets/help/email-listing.md" },
      { label: "LBLMAILLIST", routerLink: ["/email-listing"] },
    ]);
  }
  ngOnInit() {
    this.userId = this.auth.UserInfo["userId"];
    this.roleId = this.auth.UserInfo["roleId"];
    this.router.events
      .pipe(
        filter((e: any) => e instanceof RoutesRecognized),
        pairwise()
      )
      .subscribe((e: any) => {
        var url = e[0].urlAfterRedirects; // previous url
        localStorage.setItem("previousUrl-" + this.userId, url);
      });
    // Table header
    this.cols = [
      { field: "Email Option Name", header: "LBLMAILOPTIONNAME" },
      { field: "STATUS", header: "LBLSTATUS" },
    ];
    this.colsCount = this.cols.length;
    this.translate.get("LBLMAILOPTIONNAME").subscribe((res) => {
      this.searchBy = [{ id: 1, name: res }];
      this.selectedField = 1;
    });
    this.checkFilter();
    this.appName = this.env.appName;
  }

  checkFilter() {
    this.parameters = {};
    this.previousUrl = localStorage.getItem("previousUrl-" + this.userId);
    if (this.previousUrl) {
      if (
        this.previousUrl.search("/add-email") != -1 ||
        this.previousUrl.search("/edit-email/") != -1
      ) {
        if (localStorage.getItem("filterInfo")) {
          this.toggleFilterShow();
          // this.animationState = "in";
          this.parameters = JSON.parse(localStorage.getItem("filterInfo"));
          var keys = Object.keys(this.parameters);
          keys.map((param) => {
            if (this.parameters[param]) {
              if (param == "scheduleId") {
                this.selectedEmailType = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
              if (param == "status") {
                this.selectedStatus = this.parameters[param]
                  .split(",")
                  .map(Number);
              }
            }
          });
          localStorage.removeItem("previousUrl-" + this.userId);
        }
      } else {
        localStorage.removeItem("filterInfo");
      }
    } else {
      localStorage.removeItem("filterInfo");
    }

    this.loadData(this.parameters);
  }

  loadData(data) {
    this.emailService.getEmailList(data).subscribe((res) => {
      this.emailList = res["data"];
      this.emailType = [];
      var emailList = res["filterEmailType"];
      emailList.map((data) => {
        this.emailType.push({
          label: data["SCHEDULETYPENAME"],
          value: data["SCHEDULETYPEID"],
        });
      });

      this.status = [];
      var statusList = res["filterStatus"];
      statusList.map((data) => {
        this.translate.get(data["VALUE"]).subscribe((label) => {
          this.status.push({ label: label, value: data["VALUEID"] });
        });
      });
      this.isLoaded = true;
      this.selectedData = [];
      if (this.selectedEmailType) {
        this.emailType = this.auth.rearrangeSelects(
          this.emailType,
          this.selectedEmailType
        );
      }
      if (this.selectedStatus) {
        this.status = this.auth.rearrangeSelects(
          this.status,
          this.selectedStatus
        );
      }
    });
  }

  filterEmails() {
    const body = {};
    if (this.selectedEmailType) {
      body["scheduleId"] = this.selectedEmailType.join(",");
    }
    if (this.selectedStatus) {
      body["status"] = this.selectedStatus.join(",");
    }
    localStorage.removeItem("filterInfo");
    localStorage.setItem("filterInfo", JSON.stringify(body));
    this.loadData(body);
  }

  clearFilter(tableValue) {
    this.selectedEmailType = [];
    this.selectedStatus = [];
    this.searchInput = "";
    this.loadData({});
    this.onTextSearch(tableValue);
  }

  deleteRecord() {
    this.confirmClass = "warning-msg";
    if (this.selectedData) {
      const selectedScheduleId = this.selectedData.map((x) => x["SCHEDULEID"]);
      let isDefaultIdsExits = false;
      let isOtherIdsExists = false;
      this.translate
        .get(["ALTDELCONFIRM", "ALTDELCONFIRMEMAILTYPE", "ALTDELDEFMAILTYPE"])
        .subscribe((res) => {
          selectedScheduleId.forEach((element) => {
            if (element === 1 || element === 2 || element === 3) {
              isDefaultIdsExits = true;
            } else {
              isOtherIdsExists = true;
            }
          });
          if (isDefaultIdsExits && isOtherIdsExists) {
            this.deleteRecordDiaglog(res["ALTDELCONFIRMEMAILTYPE"]);
          } else if (isDefaultIdsExits) {
            this.messageService.add({
              severity: "error",
              detail: res["ALTDELDEFMAILTYPE"],
            });
            this.selectedData = [];
            this.searchInput = "";
            this.loadData({});
          } else {
            this.deleteRecordDiaglog(res["ALTDELCONFIRM"]);
          }
        });
    } else {
      this.translate.get("ALTSELECTDELETE").subscribe((res) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
    }
  }

  deleteRecordDiaglog(message) {
    this.confirmationService.confirm({
      message: message,
      accept: () => {
        let body = {};
        body["scheduleId"] = this.selectedData
          .map((x) => x["SCHEDULEID"])
          .join();

        this.emailService.deleteEmail(body).subscribe((res) => {
          this.translate.get(res["message"]).subscribe((res) => {
            this.messageService.add({
              severity: "success",
              detail: res,
            });
          });
          this.searchInput = "";
          this.selectedData = [];
          this.loadData({});
        });
      },
      reject: () => {
        this.selectedData = [];
        this.searchInput = "";
        this.loadData({});
      },
    });
  }

  changeStatus() {
    this.confirmClass = "info-msg";
    if (this.selectedData) {
      const selectedScheduleId = this.selectedData.map((x) => x["SCHEDULEID"]);
      let isDefaultIdsExits = false;
      let isOtherIdsExists = false;
      this.translate
        .get([
          "ALTCHANGESTATUSCONFIRM",
          "ALTCHGSTATUSDEFMAILTYPE",
          "ALTCHGSTATUSCONFIRMEMAILTYPE",
        ])
        .subscribe((res) => {
          selectedScheduleId.forEach((element) => {
            if (element === 1 || element === 2 || element === 3) {
              isDefaultIdsExits = true;
            } else {
              isOtherIdsExists = true;
            }
          });
          if (isDefaultIdsExits && isOtherIdsExists) {
            this.changeStatusDialog(res["ALTCHGSTATUSCONFIRMEMAILTYPE"]);
          } else if (isDefaultIdsExits) {
            this.messageService.add({
              severity: "error",
              detail: res["ALTCHGSTATUSDEFMAILTYPE"],
            });
            this.selectedData = [];
            this.searchInput = "";
            this.loadData({});
          } else {
            this.changeStatusDialog(res["ALTCHANGESTATUSCONFIRM"]);
          }
        });
    } else {
      this.translate.get("ALTSELECTCHANGESTATUS").subscribe((res) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
    }
  }

  changeStatusDialog(message) {
    this.confirmationService.confirm({
      message: message,
      accept: () => {
        const body = {};
        body["scheduleId"] = this.selectedData
          .map((x) => x["SCHEDULEID"])
          .join();
        this.emailService.changeEmailStatus(body).subscribe((res) => {
          this.translate.get(res["message"]).subscribe((res) => {
            this.messageService.add({
              severity: "success",
              detail: res,
            });
            this.loadData({});
            this.searchInput = "";
            this.selectedData = [];
          });
        });
      },
      reject: () => {
        this.selectedData = [];
        this.loadData({});
        this.searchInput = "";
      },
    });
  }

  changeStatusBtn(id) {
    event.stopPropagation();
    this.translate
      .get(["ALTCHANGESTATUSCONFIRM", "ALTCHGSTATUSDEFMAILTYPE"])
      .subscribe((res) => {
        if (id == 1 || id == 2 || id == 3) {
          this.messageService.add({
            severity: "error",
            detail: res["ALTCHGSTATUSDEFMAILTYPE"],
          });
          this.selectedData = [];
          this.searchInput = "";
          this.loadData({});
        } else {
          this.confirmationService.confirm({
            message: res["ALTCHANGESTATUSCONFIRM"],
            accept: () => {
              const body = {};
              body["scheduleId"] = id;
              this.emailService.changeEmailStatus(body).subscribe((res) => {
                this.translate.get(res["message"]).subscribe((res) => {
                  this.messageService.add({
                    severity: "success",
                    detail: res,
                  });
                  this.searchInput = "";
                  this.loadData({});
                  this.loadData({});
                });
              });
            },
            reject: () => {
              this.searchInput = "";
              this.loadData({});
              this.selectedData = [];
            },
          });
        }
      });
  }

  onTextSearch(tableValue) {
    tableValue.filter(this.searchInput, "Email Option Name", "contains");
    this.isLoaded = true;
  }

  onSelectAll() {
    const selected = this.emailType.map((item) => item.SCHEDULETYPEID);
    this.selectedEmailType = selected;
  }

  onClearAll() {
    this.selectedEmailType = [];
  }
  onSelectAllStatus() {
    const selectedStatus = this.status.map((item) => item.VALUEID);
    this.selectedStatus = selectedStatus;
  }

  onClearAllStatus() {
    this.selectedStatus = [];
  }

  toggleFilterShow() {
    this.animationState = this.animationState === "out" ? "in" : "out";
  }
}
