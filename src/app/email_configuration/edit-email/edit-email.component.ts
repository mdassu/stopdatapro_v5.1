import { Component, OnInit, OnDestroy } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { MessageService } from "primeng/api";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";
import { EmailConfigurationService } from "src/app/_services/email-configuration.service";
import { ActivatedRoute } from "@angular/router";
import { AuthenticationService } from "../../_services/authentication.service";

import tinymce from "tinymce/tinymce";

// A theme is also required
import "tinymce/themes/modern/theme";

// Any plugins you want to use has to be imported
import "tinymce/plugins/paste";
import "tinymce/plugins/link";
import "tinymce/plugins/table";
import "tinymce/plugins/preview";
import { TranslateCompiler, TranslateService } from "@ngx-translate/core";
import { CookieService } from "ngx-cookie-service";
import { SlideUpDownAnimations } from "src/app/_animations/slide-up-down.animations";
import { UtilService } from "src/app/_services/util.service";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

@Component({
  selector: "app-edit-email",
  templateUrl: "./edit-email.component.html",
  styleUrls: ["./edit-email.component.css"],
  providers: [MessageService, CustomValidatorsService],
  animations: [SlideUpDownAnimations],
})
export class EditEmailComponent implements OnInit, OnDestroy {
  // Varialbe for fields name
  editEmailGeneral: FormGroup;
  multilingualContent: FormGroup;
  Subject: any;
  ReplyTo: any;
  EmailOptionName: any;
  BodyContent: any;
  submitted: boolean;
  submitted1: boolean;
  emailType: any;
  mailDetailsFrom: any;
  languages: any;
  scheduleId: number;
  keywords: any;
  scheduleTypeId: number;
  Keyword: any = "Keywords";
  animationState = "out";
  show_advance_search: boolean = false;
  filterData: any[] = [];
  optionalFilter: any[] = [];
  scheduleToggle: boolean = false;
  filterToggle: boolean = false;
  sendThisMail: any = [];
  startTime: any = [];
  status: any;
  selectedMailType: number = 1;
  days = [];
  initialGeneralForm: FormGroup;
  initialMultilingualForm: FormGroup;
  filterGroupList = [];
  filterSetting: any = {};
  isLoading: boolean = true;
  index: number = 0;
  iconType = "ui-icon-add";
  filterSite: any[] = [];
  generalLoading: boolean = false;
  mailLoading: boolean = false;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private emailService: EmailConfigurationService,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private cookieService: CookieService,
    private auth: AuthenticationService,
    private util: UtilService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.startTime = this.util.startTime;
    this.breadcrumbService.setItems([
      { label: "LBLEMAILCONFIG", url: "./assets/help/add-email.md" },
      { label: "LBLMAILLIST", routerLink: ["/email-listing"] },
    ]);

    this.translate.get("LBLKEYWORDS").subscribe((res) => {
      this.Keyword = res;
    });
    // this.translate.get(["LBLDAY", "LBLWEEKLY", "LBLMONTHLY"]).subscribe(res => {
    //   const temparray = [];
    //   temparray.push(
    //     { name: res["LBLDAY"], id: 1 },
    //     { name: res["LBLWEEKLY"], id: 2 },
    //     { name: res["LBLMONTHLY"], id: 3 }
    //   );
    //   this.sendThisMail = temparray;
    // });
    this.status = [];
    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((res) => {
      let temparray = [];
      temparray.push(
        { name: res["LBLACTIVE"], id: 1 },
        { name: res["LBLINACTIVE"], id: 0 }
      );
      this.status = temparray;
    });
  }
  ngOnDestroy(): void {
    var editorCache1 = tinymce.get("tinymceEditor1"); //get from cache
    if (editorCache1) {
      editorCache1.remove();
      editorCache1.destroy();
    }
    var editorCache2 = tinymce.get("tinymceEditor1"); //get from cache
    if (editorCache2) {
      editorCache2.remove();
      editorCache2.destroy();
    }
  }

  ngOnInit() {
    if (this.cookieService.get("add-email")) {
      var fn = this;
      setTimeout(function () {
        fn.messageService.add({
          severity: "success",
          detail: fn.cookieService.get("add-email"),
        });
        fn.cookieService.delete("add-email");
      }, 1000);
    }
    this.days = Array(28)
      .fill(0)
      .map((e, i) => {
        const tempObj = {};
        tempObj["label"] = i + 1;
        tempObj["value"] = i + 1;
        return tempObj;
      });
    this.scheduleId = +this.activatedRoute.snapshot.paramMap.get("id");
    const data = {
      emailOptionName: "",
      replyTo: "",
      cc: "",
      subject: "",
      bodyContent: "",
      scheduleTypeName: "",
      mailDetailsFrom: "",
      day: 1,
      sendThisMail: 1,
      startTime: "",
      status: "",
      dayList: "",
      everyDWM: null,
      dwm: "",
      maximumEmailUser: "",
    };
    this.createFormBuilder(data);

    this.loadData();

    if (this.scheduleId == 1 || this.scheduleId == 2) {
      this.multilingualContent = this.fb.group({
        Language: new FormControl(4, Validators.required),
        Subject: new FormControl(null, [
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
        ]),
        BodyContent: new FormControl(null, Validators.required),
      });
    }
  }

  onLanguageChange(data) {
    this.submitted1 = false;
    this.emailService
      .getMultilingualContent({
        scheduleId: this.scheduleId,
        scheduleTypeId: this.scheduleTypeId,
        languageId: data.LANGUAGEID,
      })
      .subscribe((res) => {
        this.languages = res["language"];
        this.keywords = JSON.parse(res["emailBody"]["KEYWORD"]);
        this.multilingualContent.patchValue({
          Subject: res["scheduleDetail"]
            ? res["scheduleDetail"]["SUBJECT"]
            : "",
          BodyContent: res["scheduleDetail"]
            ? res["scheduleDetail"]["BODY"]
            : "",
        });
        setTimeout(() => {
          tinymce
            .get("tinymceEditor2")
            .setContent(
              res["scheduleDetail"] ? res["scheduleDetail"]["BODY"] : ""
            );
        }, 1000);
        let items = this.keywords.map((item) => {
          this.translate.get(item.label).subscribe((res) => {
            item["label"] = res;
          });

          return item;
        });
        const menuList = [];
        items.forEach((item) => {
          let tempItem = {};
          tempItem["text"] = item["label"];
          tempItem[
            "value"
          ] = `&nbsp;<span id=${item["keyWordId"]} class="mceNonEditable" contenteditable="false"><strong>[${item["label"]}]</strong></span>&nbsp;`;
          menuList.push(tempItem);
        });
        var languageTinymce = "";
        let currentUser = this.auth.UserInfo;
        if (currentUser.languageCode == "da-dk") {
          var languageTinymce = "da";
        } else if (currentUser.languageCode == "de-de") {
          var languageTinymce = "de";
        } else if (currentUser.languageCode == "el") {
          var languageTinymce = "el";
        } else if (currentUser.languageCode == "en-us") {
          var languageTinymce = "en";
        } else if (currentUser.languageCode == "es-es") {
          var languageTinymce = "es";
        } else if (currentUser.languageCode == "fr-fr") {
          var languageTinymce = "fr-FR";
        } else if (currentUser.languageCode == "hu") {
          var languageTinymce = "hu_HU";
        } else if (currentUser.languageCode == "it-it") {
          var languageTinymce = "it";
        } else if (currentUser.languageCode == "ja-jp") {
          var languageTinymce = "ja";
        } else if (currentUser.languageCode == "ko") {
          var languageTinymce = "ko_KR";
        } else if (currentUser.languageCode == "nl-nl") {
          var languageTinymce = "nl";
        } else if (currentUser.languageCode == "pl-pl") {
          var languageTinymce = "pl";
        } else if (currentUser.languageCode == "pt-pt") {
          var languageTinymce = "pt_PT";
        } else if (currentUser.languageCode == "ru-ru") {
          var languageTinymce = "ru";
        } else if (currentUser.languageCode == "th-th") {
          var languageTinymce = "th_TH";
        } else if (currentUser.languageCode == "zh-cn") {
          var languageTinymce = "zh_CN";
        }
        var that = this;
        tinymce.remove("#tinymceEditor2");
        tinymce.init({
          selector: "#tinymceEditor2",
          base_url: "/tinymce",
          language: languageTinymce,
          height: "200",
          suffix: ".min",
          skin_url: "tinymce/skins/lightgray",
          plugins: "table preview",
          menubar: false,
          toolbar:
            "bold italic underline alignleft aligncenter alignright alignjustify numlist outdent indent table fontselect fontsizeselect mybutton preview ",
          setup: function (editor) {
            editor.addButton("mybutton", {
              type: "listbox",
              text: that.Keyword,
              icon: false,
              onselect: function (e) {
                editor.insertContent(this.value());
              },
              values: menuList,
            });
            editor.on("change", function (e) {
              that.multilingualContent.patchValue({
                BodyContent: editor.getContent(),
              });
              that.multilingualContent.get("BodyContent").markAsDirty();
            });
          },
        });
        this.initialMultilingualForm = this.multilingualContent.getRawValue();
      });
  }

  get MultilingualContents() {
    return this.multilingualContent.controls;
  }
  get GeneralSubForm() {
    return this.editEmailGeneral.controls;
  }

  loadData() {
    this.emailService
      .getEmailForEdit({ scheduleId: this.scheduleId })
      .subscribe((res) => {
        const editData = res["editData"];
        const siteFilterData = res["filterSite"];
        const groupFilterData = res["filterGroup"];
        const optionalFilter = res["filterRole"] || {};
        this.filterSetting = res["filterSetting"];

        if (this.filterSetting.constructor === Array) {
          this.filterToggle = this.filterSetting.length > 0 ? true : false;
        } else {
          this.filterToggle = true;
        }

        //set filter data
        this.filterSite = [];
        if (siteFilterData) {
          siteFilterData["field"] = "siteId";
          siteFilterData["Data"] = JSON.parse(siteFilterData["Data"]);
          siteFilterData["Data"] = siteFilterData["Data"].map((data) => {
            return { label: data["SITENAME"], value: data["SITEID"] };
          });
          this.filterSite.push(siteFilterData);
        }

        this.filterData = [];
        groupFilterData.map((filterItem) => {
          this.filterGroupList.push(
            filterItem["REPORTPARAM"].toLowerCase() + "Id"
          );
          filterItem["field"] = filterItem["REPORTPARAM"].toLowerCase() + "Id";
          filterItem["Data"] = JSON.parse(filterItem["Data"]);
          filterItem["Data"] = filterItem["Data"].map((data) => {
            return { label: data["GROUPNAME"], value: data["GROUPID"] };
          });
          this.filterData.push(filterItem);
        });

        this.optionalFilter = [];
        optionalFilter["field"] = "roleId";
        if (optionalFilter["Data"]) {
          optionalFilter["Data"] = JSON.parse(optionalFilter["Data"]);
          optionalFilter["Data"] = optionalFilter["Data"].map((data) => {
            return { label: data["ROLENAME"], value: data["ROLEID"] };
          });
        } else {
          optionalFilter["Data"] = [];
        }

        // set advanced filter data
        this.optionalFilter.push(optionalFilter);

        //show Email schedule  only if data Exists
        var sendVal;
        this.scheduleTypeId = editData.SCHEDULETYPEID;
        this.translate
          .get(["LBLDAY", "LBLWEEKLY", "LBLMONTHLY"])
          .subscribe((res) => {
            if (this.scheduleTypeId == 7) {
              const temparray = [];
              temparray.push({ name: res["LBLWEEKLY"], id: 2 });
              this.sendThisMail = temparray;
              sendVal = "2";
            } else if (this.scheduleTypeId == 8) {
              const temparray = [];
              temparray.push(
                { name: res["LBLWEEKLY"], id: 2 },
                { name: res["LBLMONTHLY"], id: 3 }
              );
              this.sendThisMail = temparray;
              sendVal = "2";
            } else {
              const temparray = [];
              temparray.push(
                { name: res["LBLDAY"], id: 1 },
                { name: res["LBLWEEKLY"], id: 2 },
                { name: res["LBLMONTHLY"], id: 3 }
              );
              this.sendThisMail = temparray;
              sendVal = "1";
            }
          });
        const data = {
          emailOptionName: editData.SCHEDULENAME,
          replyTo: editData.REPLYTO,
          scheduleTypeName: editData.SCHEDULETYPENAME,
          mailDetailsFrom: editData.DUPLICATEMAILFROM,
          cc: editData.CC,
          subject: editData.SUBJECT,
          bodyContent: editData.BODY,
          day: 1,
          sendThisMail: sendVal,
          startTime: "",
          status: "",
          dayList: "",
          everyDWM: null,
          dwm: "",
          maximumEmailUser: "",
        };

        if (editData.DWM) {
          this.scheduleToggle = true;
          data["maximunEmailUser"] = editData["MAXEMAILS"];
          data["startTime"] = this.time_convert(+editData["STARTTIME"]);
          data["status"] = editData["STATUS"];
          if (editData.DWM === 1) {
            data["sendThisMail"] = 1;
            this.onSendThisMailChange({ id: 1 });
            data["day"] = editData["EVERYDWM"];
          } else if (editData.DWM === 2) {
            data["sendThisMail"] = 2;
            this.onSendThisMailChange({ id: 2 });
            let dayListArr = editData["DAYLIST"].split("");
            dayListArr = dayListArr.map(Number);
            data["dayList"] = dayListArr.indexOf(1);
          } else if (editData.DWM === 3) {
            data["sendThisMail"] = 3;
            this.onSendThisMailChange({ id: 3 });
            let dayListArr = editData["DAYLIST"].split("");
            dayListArr = dayListArr.map(Number);
            data["everyDWM"] = +editData["EVERYDWM"];
            data["dwm"] = dayListArr.indexOf(1);
          }
        } else {
          this.editEmailGeneral.removeControl("Day");
          this.editEmailGeneral.removeControl("SendThisMail");
          this.editEmailGeneral.removeControl("StartTime");
          this.editEmailGeneral.removeControl("Status");
          this.editEmailGeneral.removeControl("DayList");
          this.editEmailGeneral.removeControl("EveryDWM");
          this.editEmailGeneral.removeControl("DWM");
          this.editEmailGeneral.removeControl("MaximumEmailUser");
        }

        this.createFormBuilder(data);

        this.emailService
          .getEditBody({ scheduleTypeId: this.scheduleTypeId })
          .subscribe((res) => {
            if (res["status"]) {
              this.keywords = JSON.parse(res["emailBody"]["KEYWORD"]);

              let items = this.keywords.map((item) => {
                this.translate.get(item.label).subscribe((res) => {
                  item["label"] = res;
                });

                return item;
              });
              const menuList = [];
              items.forEach((item) => {
                let tempItem = {};
                tempItem["text"] = item["label"];
                tempItem[
                  "value"
                ] = `&nbsp;<span id=${item["keyWordId"]} class="mceNonEditable" contenteditable="false"><strong>[${item["label"]}]</strong></span>&nbsp;`;
                menuList.push(tempItem);
              });

              var that = this;
              tinymce.remove("#tinymceEditor1");
              var languageTinymce = "";
              let currentUser = this.auth.UserInfo;
              if (currentUser.languageCode == "de-de") {
                var languageTinymce = "de";
              } else if (currentUser.languageCode == "en-us") {
                var languageTinymce = "en";
              } else if (currentUser.languageCode == "hu") {
                var languageTinymce = "hu_HU";
              } else if (currentUser.languageCode == "el") {
                var languageTinymce = "el";
              } else if (currentUser.languageCode == "it-it") {
                var languageTinymce = "it";
              } else if (currentUser.languageCode == "ko") {
                var languageTinymce = "ko_KR";
              } else if (currentUser.languageCode == "nl-nl") {
                var languageTinymce = "nl";
              } else if (currentUser.languageCode == "pl-pl") {
                var languageTinymce = "pl";
              } else if (currentUser.languageCode == "pt-pt") {
                var languageTinymce = "pt_PT";
              } else if (currentUser.languageCode == "ru-ru") {
                var languageTinymce = "ru";
              }
              tinymce.init({
                selector: "#tinymceEditor1",
                base_url: "/tinymce",
                language: languageTinymce,
                height: "200",
                suffix: ".min",
                skin_url: "tinymce/skins/lightgray",
                plugins: "table preview",
                menubar: false,
                toolbar:
                  "bold italic underline alignleft aligncenter alignright alignjustify numlist outdent indent table fontselect fontsizeselect mybutton preview ",
                setup: function (editor) {
                  editor.addButton("mybutton", {
                    type: "listbox",
                    text: that.Keyword,
                    icon: false,
                    onselect: function (e) {
                      editor.insertContent(this.value());
                    },
                    values: menuList,
                  });
                  editor.on("change", function (e) {
                    that.editEmailGeneral.patchValue({
                      BodyContent: editor.getContent(),
                    });
                    that.editEmailGeneral.get("BodyContent").markAsDirty();
                  });
                },
              });
            }
          });

        if (this.scheduleId == 1 || this.scheduleId == 2) {
          this.emailService
            .getMultilingualContent({
              scheduleId: this.scheduleId,
              scheduleTypeId: editData.SCHEDULETYPEID,
              languageId: 4,
            })
            .subscribe((res) => {
              this.languages = res["language"];
              this.keywords = JSON.parse(res["emailBody"]["KEYWORD"]);
              this.multilingualContent.patchValue({
                Language: 4,
                Subject: res["scheduleDetail"]
                  ? res["scheduleDetail"]["SUBJECT"]
                  : "",
                BodyContent: res["scheduleDetail"]
                  ? res["scheduleDetail"]["BODY"]
                  : "",
              });

              setTimeout(() => {
                tinymce
                  .get("tinymceEditor2")
                  .setContent(
                    res["scheduleDetail"] ? res["scheduleDetail"]["BODY"] : ""
                  );
              }, 1000);
              let items = this.keywords.map((item) => {
                this.translate.get(item.label).subscribe((res) => {
                  item["label"] = res;
                });

                return item;
              });
              const menuList = [];
              items.forEach((item) => {
                let tempItem = {};
                tempItem["text"] = item["label"];
                tempItem[
                  "value"
                ] = `&nbsp;<span id=${item["keyWordId"]} class="mceNonEditable" contenteditable="false"><strong>[${item["label"]}]</strong></span>&nbsp;`;
                menuList.push(tempItem);
              });
              var that = this;
              tinymce.remove("#tinymceEditor2");
              var languageTinymce = "";
              let currentUser = this.auth.UserInfo;
              if (currentUser.languageCode == "de-de") {
                var languageTinymce = "de";
              } else if (currentUser.languageCode == "en-us") {
                var languageTinymce = "en";
              } else if (currentUser.languageCode == "hu") {
                var languageTinymce = "hu_HU";
              } else if (currentUser.languageCode == "el") {
                var languageTinymce = "el";
              } else if (currentUser.languageCode == "it-it") {
                var languageTinymce = "it";
              } else if (currentUser.languageCode == "ko") {
                var languageTinymce = "ko_KR";
              } else if (currentUser.languageCode == "nl-nl") {
                var languageTinymce = "nl";
              } else if (currentUser.languageCode == "pl-pl") {
                var languageTinymce = "pl";
              } else if (currentUser.languageCode == "pt-pt") {
                var languageTinymce = "pt_PT";
              } else if (currentUser.languageCode == "ru-ru") {
                var languageTinymce = "ru";
              }
              tinymce.init({
                selector: "#tinymceEditor2",
                base_url: "/tinymce",
                language: languageTinymce,
                height: "200",
                suffix: ".min",
                skin_url: "tinymce/skins/lightgray",
                plugins: "table preview",
                menubar: false,
                toolbar:
                  "bold italic underline alignleft aligncenter alignright alignjustify numlist outdent indent table fontselect fontsizeselect mybutton preview ",
                setup: function (editor) {
                  editor.addButton("mybutton", {
                    type: "listbox",
                    text: that.Keyword,
                    icon: false,
                    onselect: function (e) {
                      editor.insertContent(this.value());
                    },
                    values: menuList,
                  });
                  editor.on("change", function (e) {
                    that.multilingualContent.patchValue({
                      BodyContent: editor.getContent(),
                    });
                    that.multilingualContent.get("BodyContent").markAsDirty();
                  });
                },
              });
              this.initialMultilingualForm = this.multilingualContent.getRawValue();
            });
        }
        this.filterGroupList.forEach((group) => {
          this.editEmailGeneral.addControl(group, new FormControl(""));
        });

        this.initialGeneralForm = this.editEmailGeneral.getRawValue();
        this.isLoading = false;
      });
  }

  createFormBuilder(data) {
    this.editEmailGeneral = this.fb.group({
      EmailOptionName: new FormControl(data.emailOptionName, [
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      ReplyTo: new FormControl(data.replyTo, [
        Validators.required,
        Validators.email,
        this.customValidatorsService.isEmailValid,
        this.customValidatorsService.noSpaceValidator,
        // Validators.pattern(
        //   "^\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*$"
        // ),
      ]),
      ScheduleTypeName: new FormControl(
        { value: data.scheduleTypeName, disabled: true },
        [Validators.required]
      ),
      MailDetailsFrom: new FormControl(
        { value: data.mailDetailsFrom, disabled: true },
        [Validators.required]
      ),
      CarbonCopy: new FormControl(data.cc, [
        this.customValidatorsService.ccEmailValid,
        // Validators.pattern(
        //   "^(\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*;)*(\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*)$"
        // ),
      ]),
      Subject: new FormControl(data.subject, [
        Validators.required,
        this.customValidatorsService.noWhitespaceValidator,
      ]),
      BodyContent: new FormControl(data.bodyContent, [Validators.required]),
      Day: new FormControl(data.day),
      SendThisMail: new FormControl(data.sendThisMail),
      StartTime: new FormControl(data.startTime),
      Status: new FormControl(data.status),
      DayList: new FormControl(data.dayList),
      EveryDWM: new FormControl(data.everyDWM),
      DWM: new FormControl(data.dwm),
      MaximumEmailUser: new FormControl(data.maximunEmailUser, [
        Validators.required,
        Validators.maxLength(2),
        Validators.min(1),
        Validators.pattern("[0-9]*"),
      ]),
    });
    if (
      this.scheduleTypeId == 3 ||
      this.scheduleTypeId == 4 ||
      this.scheduleTypeId == 5 ||
      this.scheduleTypeId == 6 ||
      this.scheduleTypeId == 11
    ) {
      this.editEmailGeneral.get("MaximumEmailUser").clearValidators();
      this.editEmailGeneral.get("MaximumEmailUser").updateValueAndValidity();
    }
    if (this.filterSetting.length === 0) {
      this.editEmailGeneral.addControl("roleId", new FormControl(null));
      this.editEmailGeneral.addControl("siteId", new FormControl(null));
    }

    Object.keys(this.filterSetting).forEach((key) => {
      if (key === "ROLEID") {
        const roleId =
          this.filterSetting[key] === ""
            ? []
            : this.filterSetting[key].split(",").map(Number);
        this.editEmailGeneral.addControl("roleId", new FormControl(roleId));
        this.auth.rearrangeSelect(this.optionalFilter, roleId);
      } else if (key === "SITEID") {
        const siteId =
          this.filterSetting[key] === ""
            ? []
            : this.filterSetting[key].split(",").map(Number);
        this.editEmailGeneral.addControl("siteId", new FormControl(siteId));
        this.auth.rearrangeSelect(this.filterSite, siteId);
      } else {
        this.editEmailGeneral.addControl(
          key.toLowerCase() + "Id",
          new FormControl(
            this.filterSetting[key] === ""
              ? []
              : this.filterSetting[key].split(",").map(Number)
          )
        );
        this.auth.rearrangeSelect(
          this.filterData,
          this.filterSetting[key].split(",").map(Number)
        );
      }
    });
  }

  toggleFilterShow() {
    this.animationState = this.animationState === "out" ? "in" : "out";
  }
  advance_Search() {
    this.iconType =
      this.iconType === "ui-icon-add" ? "ui-icon-remove" : "ui-icon-add";
    this.show_advance_search = !this.show_advance_search;
  }

  onSendThisMailChange(data) {
    this.selectedMailType = data.id;
    if (this.selectedMailType === 2) {
      this.editEmailGeneral.get("EveryDWM").clearValidators();
      this.editEmailGeneral.get("EveryDWM").updateValueAndValidity();
      this.editEmailGeneral.get("DWM").clearValidators();
      this.editEmailGeneral.get("DWM").updateValueAndValidity();
      this.editEmailGeneral.get("DayList").setValidators([Validators.required]);
      this.editEmailGeneral.get("DayList").updateValueAndValidity();
    } else if (this.selectedMailType === 3) {
      this.editEmailGeneral.get("DayList").clearValidators();
      this.editEmailGeneral.get("DayList").updateValueAndValidity();
      this.editEmailGeneral
        .get("EveryDWM")
        .setValidators([Validators.required]);
      this.editEmailGeneral.get("EveryDWM").updateValueAndValidity();
      this.editEmailGeneral.get("DWM").setValidators([Validators.required]);
      this.editEmailGeneral.get("DWM").updateValueAndValidity();
    }
  }

  onSubmit(value: string) {
    this.editEmailGeneral.patchValue({
      BodyContent: tinymce.get("tinymceEditor1").getContent(),
    });
    this.submitted = true;
    if (this.editEmailGeneral.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.generalLoading = true;
      const formData = this.editEmailGeneral.value;
      const timeArray = formData["StartTime"].split(":");
      const body = {
        scheduleId: this.scheduleId,
        scheduleName: formData["EmailOptionName"],
        scheduleTypeId: this.scheduleTypeId,
        replyTo: formData["ReplyTo"],
        cc: formData["CarbonCopy"],
        subject: formData["Subject"],
        body: tinymce.get("tinymceEditor1").getContent(),
        status: formData["Status"],
        maxEmails: formData["MaximumEmailUser"],
        startTime: parseInt(timeArray[0]) * 60 + parseInt(timeArray[1]),
        siteId: formData["siteId"] ? formData["siteId"].join(",") : "",
        roleId: formData["roleId"] ? formData["roleId"].join(",") : "",
      };

      this.filterGroupList.forEach((group) => {
        body[group] = formData[group] ? formData[group].join(",") : "";
      });

      if (this.selectedMailType === 1) {
        body["dwm"] = 1;
        body["everyDwm"] = formData["Day"];
        body["day"] = 0;
        body["dayList"] = 0;
      } else if (this.selectedMailType === 2) {
        body["dwm"] = 2;
        body["everyDwm"] = 0;
        body["day"] = formData["DayList"];
        const tempDataArr = [0, 0, 0, 0, 0, 0, 0];
        tempDataArr[formData["DayList"]] = 1;
        body["dayList"] = tempDataArr.join("");
      } else if (this.selectedMailType === 3) {
        body["dwm"] = 3;
        body["everyDwm"] = formData["EveryDWM"];
        body["day"] = formData["DayList"];
        const tempDataArr = [0, 0, 0, 0, 0, 0, 0];
        tempDataArr[formData["DWM"]] = 1;
        body["dayList"] = tempDataArr.join("");
      }
      this.emailService.updateEmailConfig(body).subscribe((res) => {
        if (res["status"]) {
          this.translate.get(res["message"]).subscribe((message) => {
            this.messageService.add({
              severity: "success",
              detail: message,
            });
          });
          this.generalLoading = false;
          this.loadData();
        } else {
          this.translate.get(res["message"]).subscribe((message) => {
            this.messageService.add({
              severity: "error",
              detail: message,
            });
          });
          this.generalLoading = false;
          this.loadData();
        }
      });
    }
  }
  onSubmit1(value: string) {
    this.submitted1 = true;
    const formData = this.multilingualContent.value;
    if (this.multilingualContent.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.mailLoading = true;
      let body = {
        scheduleId: this.scheduleId,
        languageId: formData["Language"],
        subject: formData["Subject"],
        body: tinymce.get("tinymceEditor2").getContent(),
      };
      this.emailService.updateInsertMultiLingual(body).subscribe((res) => {
        if (res["status"]) {
          this.translate.get(res["message"]).subscribe((message) => {
            this.messageService.add({
              severity: "success",
              detail: message,
            });
          });
          this.mailLoading = false;
        } else {
          this.translate.get(res["message"]).subscribe((message) => {
            this.messageService.add({
              severity: "error",
              detail: message,
            });
          });
          this.mailLoading = false;
        }
      });
    }
  }
  time_convert(num) {
    let hours: any = Math.floor(num / 60);
    let minutes: any = num % 60;
    if (hours < 10) {
      hours = "0" + hours;
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    return hours + ":" + minutes;
  }

  onResetGeneral() {
    this.editEmailGeneral.patchValue(this.initialGeneralForm);
    this.selectedMailType = this.initialGeneralForm["SendThisMail"];
  }

  onResetMutlilingualContent() {
    this.multilingualContent.patchValue(this.initialMultilingualForm);
  }

  onSelectAllSite(i) {
    const selected = this.filterSite[i]["Data"].map((item) => item.value);
    this.editEmailGeneral.controls[this.filterSite[i]["field"]].setValue(
      selected
    );
  }
  onClearAllSite(i) {
    this.editEmailGeneral.controls[this.filterSite[i]["field"]].setValue([]);
  }

  onSelectAll(i) {
    const selected = this.filterData[i]["Data"].map((item) => item.value);
    this.editEmailGeneral.controls[this.filterData[i]["field"]].setValue(
      selected
    );
  }
  onClearAll(i) {
    this.editEmailGeneral.controls[this.filterData[i]["field"]].setValue([]);
  }

  onSelectAllAdv(i) {
    const selected = this.optionalFilter[i]["Data"].map((item) => item.value);
    this.editEmailGeneral.controls[this.optionalFilter[i]["field"]].setValue(
      selected
    );
  }

  onClearAllAdv(i) {
    this.editEmailGeneral.controls[this.optionalFilter[i]["field"]].setValue(
      []
    );
  }

  onMultilingualContentFormReset() {
    this.multilingualContent.patchValue(this.initialMultilingualForm);
    tinymce
      .get("tinymceEditor2")
      .setContent(this.initialMultilingualForm["BodyContent"]);
  }
  onTabChange(data) {
    if (data.index == 0) {
      this.breadcrumbService.setItems([
        { label: "LBLEMAILCONFIG", url: "./assets/help/add-email.md" },
        { label: "LBLMAILLIST", routerLink: ["/email-listing"] },
      ]);
    } else {
      this.breadcrumbService.setItems([
        {
          label: "LBLEMAILCONFIG",
          url: "./assets/help/edit-email-multilingual.md",
        },
        { label: "LBLMAILLIST", routerLink: ["/email-listing"] },
      ]);
    }
  }
}
