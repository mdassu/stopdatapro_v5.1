import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { environment } from "../../environments/environment";
import { User } from "../_models/user";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({ providedIn: "root" })
export class UserService extends ApiBaseService {
  getAll() {
    return this.httpGet(`${this._serviceURL}/users`);
  }

  getById(id: number) {
    return this.httpGet(`${this._serviceURL}/users/${id}`);
  }

  getAllUserNames() {
    return this.httpGet(`${this._serviceURL}/users/username`);
  }

  getUserData(data) {
    return this.httpPost(`${this._serviceURL}/User/GetUserData`, data);
  }

  deleteUsers(data) {
    return this.httpPost(`${this._serviceURL}/User/Delete`, data);
  }

  getUserTargetData(data) {
    return this.httpPost(`${this._serviceURL}/User/GetUserDataTarget`, data);
  }

  changeUsersStatus(data) {
    return this.httpPost(`${this._serviceURL}/User/UpdateStatus`, data);
  }

  updateUserTargetData(data) {
    return this.httpPost(`${this._serviceURL}/User/InsertTargetData`, data);
  }

  getUserDataAdd(data) {
    return this.httpPost(`${this._serviceURL}/User/userAdd`, data);
  }

  updateUser(data) {
    const response = this.httpPost(`${this._serviceURL}/User/Update`, data);

    return response;
  }

  insertUser(data) {
    return this.httpPost(`${this._serviceURL}/User/Insert`, data);
  }

  getGroupType() {
    return this.httpPost(this._serviceURL + "/UserGroup/GetGroupType", {});
  }

  addGroup(formdata) {
    return this.httpPost(this._serviceURL + "/UserGroup/Insert", formdata);
  }

  addRole(formdata) {
    return this.httpPost(this._serviceURL + "/UserRole/Insert", formdata);
  }

  addUser() {
    return this.httpPost(this._serviceURL + "/User/userAdd", {});
  }

  getUsers(userId) {
    return this.httpPost(this._serviceURL + "/User/GetUserData", userId);
  }

  editUser() {
    // return this.httpPost(this._serviceURL+'/User/Delete', userId, {headers : this.GetHttpHeaders()});
  }

  getUserTarget(data) {
    return this.httpPost(this._serviceURL + "/User/GetUserDataTarget", data);
  }

  insertUserTargetData(data) {
    return this.httpPost(this._serviceURL + "/User/InsertTargetData", data);
  }

  updateUserStatus(userId) {
    return this.httpPost(this._serviceURL + "/User/UpdateStatus", userId);
  }

  getGroups(groupId) {
    return this.httpPost(this._serviceURL + "/UserGroup/GetUserData", groupId);
  }

  getGroupData(data) {
    return this.httpPost(this._serviceURL + "/UserGroup/GetUserData", data);
  }

  deleteGroups(data) {
    return this.httpPost(this._serviceURL + "/UserGroup/Delete", data);
  }

  updateGroup(data) {
    return this.httpPost(this._serviceURL + "/UserGroup/update", data);
  }

  updateGroupAssign(data) {
    return this.httpPost(this._serviceURL + "/UserGroup/updateAssign", data);
  }

  editGroups() {
    // return this.httpPost(this._serviceURL+'/User/Delete', userId, {headers : this.GetHttpHeaders()});
  }

  updateGroupStatus(data) {
    return this.httpPost(this._serviceURL + "/UserGroup/UpdateStatus", data);
  }

  getRole() {
    return this.httpPost(this._serviceURL + "/UserRole/GetRoleData", {});
  }

  getRoleById(data) {
    return this.httpPost(this._serviceURL + "/UserRole/GetRoleData", data);
  }

  getRolePermission(data) {
    return this.httpPost(
      this._serviceURL + "/UserRole/getRolePermission",
      data
    );
  }

  getRoleAssign(data) {
    return this.httpPost(this._serviceURL + "/UserRole/getRoleAssign", data);
  }

  getRoleReport(data) {
    return this.httpPost(this._serviceURL + "/UserRole/getRoleReport", data);
  }

  deleteRole(roleId) {
    return this.httpPost(this._serviceURL + "/UserRole/Delete", roleId);
  }

  updateRolePermission(data) {
    return this.httpPost(
      this._serviceURL + "/UserRole/updateRolePermission",
      data
    );
  }

  updateRoleAssign(data) {
    return this.httpPost(this._serviceURL + "/UserRole/updateRoleAssign", data);
  }

  updateRoleReport(data) {
    return this.httpPost(this._serviceURL + "/UserRole/updateRoleReport", data);
  }

  updateRole(data) {
    return this.httpPost(this._serviceURL + "/UserRole/updateRole", data);
  }

  updateRoleStatus(roleId) {
    return this.httpPost(this._serviceURL + "/UserRole/UpdateStatus", roleId);
  }

  // Target API //
  getTargetData(data) {
    return this.httpPost(this._serviceURL + "/UserTarget/getTargetData", data);
  }

  //update Target data
  updateTargetData(data) {
    return this.httpPost(this._serviceURL + "/UserTarget/insert", data);
  }
}
