import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class AlertUnsafeService extends ApiBaseService {
  getAlertUnsafeData(data) {
    return this.httpPost(`${this._serviceURL}/Profile/getAlertUnsafe`, data);
  }

  insertAlertUnsafeData(data) {
    return this.httpPost(`${this._serviceURL}/Profile/insertAlertUnsafe`, data);
  }
}
