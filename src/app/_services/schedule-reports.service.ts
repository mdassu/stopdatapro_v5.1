import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class ScheduleReportsService extends ApiBaseService {
  getScheduleReports(parameters) {
    return this.httpPost(
      this._serviceURL + "/ScheduleReport/getScheduleList",
      parameters
    );
  }

  deleteScheduleReports(scheduleId) {
    return this.httpPost(
      this._serviceURL + "/ScheduleReport/delete",
      scheduleId
    );
  }

  updateScheduleReportsStatus(scheduleId) {
    return this.httpPost(
      this._serviceURL + "/ScheduleReport/changeStatus",
      scheduleId
    );
  }

  getDataForAdd(scheduleId) {
    return this.httpPost(
      this._serviceURL + "/ScheduleReport/getDataForAdd",
      scheduleId
    );
  }

  getReportData(reportId) {
    return this.httpPost(
      this._serviceURL + "/ScheduleReport/getReportData",
      reportId
    );
  }

  getSavedFilterData(filterId) {
    return this.httpPost(
      this._serviceURL + "/ScheduleReport/getSavedFilterData",
      filterId
    );
  }

  getCheckListFilterMainCatg(chkListSetupId) {
    return this.httpPost(
      this._serviceURL + "/Reports/getCheckListFilterMainCatg",
      chkListSetupId
    );
  }

  getGroupByFilter(groupData) {
    return this.httpPost(
      this._serviceURL + "/Reports/getGroupByFilter",
      groupData
    );
  }

  insertScheduleReport(scheduleData) {
    return this.httpPost(
      this._serviceURL + "/ScheduleReport/insertScheduleReport",
      scheduleData
    );
  }

  getEditScheduleData(scheduleId) {
    return this.httpPost(
      this._serviceURL + "/ScheduleReport/getEditScheduleData",
      scheduleId
    );
  }

  updateScheduleReport(scheduleData) {
    return this.httpPost(
      this._serviceURL + "/ScheduleReport/updateScheduleReport",
      scheduleData
    );
  }
  
}
