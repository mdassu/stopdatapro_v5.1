import { FormControl, ValidatorFn, AbstractControl } from "@angular/forms";

export class CustomValidatorsService {
  public noSpaceValidator(control: FormControl) {
    const isSpace = (control.value || "").indexOf(" ") >= 0;
    const isValid = !isSpace;
    return isValid ? null : { space: true };
  }

  public noWhitespaceValidator(control: FormControl) {
    var val;
    if (control.value == null) {
      val = "";
    } else {
      val = control.value.toString();
    }
    const isWhitespace = (val || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }
  public noSpecialCharValidator(control: FormControl) {
    if (control.value != null) {
      var arr = control.value.split("");
      const noSpecialChar = arr[0] == "$";
      const isValidOrNot = !noSpecialChar;
      return isValidOrNot ? null : { specialChar: true };
    }
  }
  public maxLengthValidator(max: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (control.value && control.value.length > max) {
        return { maxLength: true };
      }
      return null;
    };
  }

  public numericValidator(len: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      var val = control.value;
      const isNumericOrNot = isNaN(val);
      if (isNumericOrNot || val.toString().length > len) {
        return { numeric: true };
      }
      return null;
    };
  }

  public noCommaValidator(control: FormControl) {
    if (control.value) {
      const isCommaExists = (control.value || "").indexOf(",") > -1;
      return isCommaExists ? { comma: true } : null;
    } else {
      return null;
    }
  }

  public notStartsWithSpace(control: FormControl) {
    if (control.value) {
      const isSpaceExist = control.value.startsWith(" ");
      return isSpaceExist ? { space: true } : null;
    } else {
      return null;
    }
  }

  public ccEmailValid(control: FormControl) {
    if (control.value) {
      const value = control.value.split(",");
      //var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      // var EMAIL_REGEXP = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      var EMAIL_REGEXP = /\S+@\S+\.[a-zA-Z.]{2,20}/;

      for (var i = 0; i < value.length; i++) {
        if (!EMAIL_REGEXP.test(value[i])) {
          return { ccEmail: true };
          break;
        }
      }
      return null;
    } else {
      return null;
    }
  }

  public isEmailValid(control: FormControl) {
    if (control.value) {
      const value = control.value.split(",");
      //var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      var EMAIL_REGEXP = /\S+@\S+\.[a-zA-Z.]{2,20}/;

      for (var i = 0; i < value.length; i++) {
        if (!EMAIL_REGEXP.test(value[i])) {
          return { isEmail: true };
          break;
        }
      }
      return null;
    } else {
      return null;
    }
  }

  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: "smooth", block: "center" });
    }
  }

  public scrollToError(): void {
    var firstElementWithError = document.querySelector(
      ".ng-invalid[formControlName]"
    );
    if (firstElementWithError == null) {
      firstElementWithError = document.querySelector(".ng-invalid");
    }
    this.scrollTo(firstElementWithError);
  }
}
