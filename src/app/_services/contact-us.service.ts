import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class ContactUsService extends ApiBaseService {
  getContactUs(data) {
    return this.httpPost(`${this._serviceURL}/Profile/getContactDetail`, data);
  }
}
