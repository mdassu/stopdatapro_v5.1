import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class CorractiveActionsService extends ApiBaseService {
  getCorrectiveActionList(params) {
    return this.httpPost(
      this._serviceURL + "/CorrectiveAction/getCorrectiveActions",
      params
    );
  }

  deleteCorrectiveAction(params) {
    return this.httpPost(this._serviceURL + "/CorrectiveAction/delete", params);
  }

  updateCorrectiveAction(params) {
    return this.httpPost(
      this._serviceURL + "/CorrectiveAction/changeStatus",
      params
    );
  }

  getCorrectiveActionForAdd() {
    return this.httpPost(
      this._serviceURL + "/CorrectiveAction/getCorrActionAdd",
      {}
    );
  }

  getAreaFilter(data) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/getAreaFilter",
      data
    );
  }

  getSubAreaByAreaId(params) {
    return this.httpPost(
      this._serviceURL + "/CorrectiveAction/getCAArea",
      params
    );
  }

  getSiteData(params) {
    return this.httpPost(
      this._serviceURL + "/CorrectiveAction/getSiteCAData",
      params
    );
  }

  insertCorrectiveAction(params) {
    return this.httpPost(
      this._serviceURL + "/CorrectiveAction/insertCorrectiveAction",
      params
    );
  }

  getCorrectiveEditData(params) {
    return this.httpPost(
      this._serviceURL + "/CorrectiveAction/getCorrectiveActionForEdit",
      params
    );
  }

  updateCorrectiveActions(params) {
    return this.httpPost(
      this._serviceURL + "/CorrectiveAction/updateCorrectiveAction",
      params
    );
  }

  updateGeneralData(params) {
    return this.httpPost(
      this._serviceURL + "/CorrectiveAction/updateCorrectiveActionGeneral",
      params
    );
  }

  getCategoryComments(params) {
    return this.httpPost(
      this._serviceURL + "/CorrectiveAction/getObservationComment",
      params
    );
  }

  getSubCategoryComments(params) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/getCategoryComment",
      params
    );
  }

  getSubAreas(params) {
    return this.httpPost(
      this._serviceURL + "/CorrectiveAction/getCAArea",
      params
    );
  }
}
