import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({ providedIn: "root" })
export class CheckListConfigService extends ApiBaseService {

  getCheckListConfigData(data) {
    return this.httpPost(
      this._serviceURL + "/Configuration/getChecklistConfigData",
      data
    );
  }

  updateCheckListConfigData(data) {
    return this.httpPost(
      this._serviceURL + "/Configuration/updateChecklistConfigData",
      data
    );
  }
}
