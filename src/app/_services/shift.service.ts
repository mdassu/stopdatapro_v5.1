import { Injectable } from "@angular/core";
import {
  HttpClient
} from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { tap } from "rxjs/internal/operators";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class ShiftService extends ApiBaseService{

  // constructor(
  //   private cookieService: CookieService,
  //   private router: Router,
  //   http: HttpClient,
  //   env: EnvService
  // ) {
  //   super(http, env)
  //  }

  
  //Insert Shift
  insertShift(formData) {
    return this.httpPost(
        this._serviceURL + "/SiteShift/Insert",
        formData
      
      );
  }

  //Update Shift
  updateShift(formData) {
    return this.httpPost(
        this._serviceURL + "/SiteShift/update",
        formData
      
      );
  }

  //Get Shift
  getShiftData(shiftId) {
    return this.httpPost(
        this._serviceURL + "/SiteShift/GetSiteData",
        shiftId
      
      );
  }


  //Get Add Shift Data
  getAddShiftData() {
    return this.httpPost(
        this._serviceURL + "/SiteShift/addShiftData",
        {}
     
      );
  }

  //Delete Shift
  deleteShift(siteId) {
    return this.httpPost(
        this._serviceURL + "/SiteShift/Delete",
        siteId
      
      );
  }

  //Update Shift Status
  updateShifttatus(siteId) {
    return this.httpPost(
        this._serviceURL + "/SiteShift/updatestatus",
        siteId
      
      );
  }

}
