import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { tap } from "rxjs/internal/operators";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class SubAreaService extends ApiBaseService {
  // constructor(
  //   private cookieService: CookieService,
  //   private router: Router,
  //   http: HttpClient,
  //   env: EnvService
  // ) {
  //   super(http, env);
  // }

  //Get Sub Area
  getSubAreaData(subAreaId) {
    return this.httpPost(
      this._serviceURL + "/SiteSubArea/GetSiteData",
      subAreaId
    
    );
  }

  //Insert Sub Area
  insertSubArea(formdata) {
    return this.httpPost(
      this._serviceURL + "/SiteSubArea/Insert",
      formdata
   
    );
  }

  //Update Sub Area
  updateSubArea(formdata) {
    return this.httpPost(
      this._serviceURL + "/SiteSubArea/update",
      formdata
    
    );
  }

  //Get Add Sub Area
  getAddSubAreaData() {
    return this.httpPost(
      this._serviceURL + "/SiteSubArea/addSubAreaData",
      {}
    
    );
  }

  //Delete Sub Area
  deleteSubArea(subAreaId) {
    return this.httpPost(
      this._serviceURL + "/SiteSubArea/Delete",
      subAreaId
    
    );
  }

  //Update Sub Area Status
  updateSubAreaStatus(subAreaId) {
    return this.httpPost(
      this._serviceURL + "/SiteSubArea/UpdateStatus",
      subAreaId
   
    );
  }
}
