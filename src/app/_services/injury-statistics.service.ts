import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class InjuryStatisticsService extends ApiBaseService {
  getInjuryList(parameters) {
    return this.httpPost(this._serviceURL + "/Injury/getInjury", parameters);
  }

  updateInjuryStatus(injuryId) {
    return this.httpPost(this._serviceURL + "/Injury/changeStatus", injuryId);
  }

  deleteInjury(injuryId) {
    return this.httpPost(this._serviceURL + "/Injury/deleteInjury", injuryId);
  }

  getInjuryAddData() {
    return this.httpPost(this._serviceURL + "/Injury/getInjuryAdd", {});
  }

  insertInjuryData(formData) {
    return this.httpPost(this._serviceURL + "/Injury/insert", formData);
  }

  updateInjuryData(formData) {
    return this.httpPost(this._serviceURL + "/Injury/update", formData);
  }
}
