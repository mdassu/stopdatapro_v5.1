import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class ForcePasswordChangeService extends ApiBaseService {
  forcePasswordChange(data) {
    return this.httpPost(`${this._serviceURL}/Login/channgePassword`, data);
  }
  getPasswordComplexity(data) {
    return this.httpPost(`${this._serviceURL}/Login/passwordComplex`, data);
  }
}
