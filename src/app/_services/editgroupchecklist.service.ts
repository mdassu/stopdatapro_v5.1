import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class EditgroupchecklistService extends ApiBaseService {
  getGroupChecklist(filterData) {
    return this.httpPost(
      this._serviceURL + "/GroupOnChecklist/getGroupChecklist",
      filterData
    );
  }

  getGroupChecklistData(userId) {
    return this.httpPost(
      this._serviceURL + "/GroupOnChecklist/getGroupChecklistAdd",
      userId
    );
  }

  SaveGrouponChecklistData(formData) {
    return this.httpPost(
      this._serviceURL + "/GroupOnChecklist/SaveGroupChecklist",
      formData
    );
  }
}
