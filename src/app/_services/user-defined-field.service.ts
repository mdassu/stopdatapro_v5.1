import { Injectable } from "@angular/core";
import {
  HttpClient
} from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
import { tap } from "rxjs/internal/operators";
import { UserDefinedField } from "../_models/user-defined-field";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class UserDefinedFieldService extends ApiBaseService {

  // constructor(
  //   private cookieService: CookieService,
  //   private router: Router,
  //   http: HttpClient,
  //   env: EnvService
  // ) {
  //   super(http, env)
  //  }

  //Get User Defined Data
  getUserDefinedData(fieldId) {
    return this.httpPost(
        this._serviceURL + "/UserDefined/getUserDefinedData",
        fieldId
     
      );
  }

  //Insert User Defined value
  insertUserDefinedData(fieldId) {
    return this.httpPost(
        this._serviceURL + "/UserDefined/insert",
        fieldId
      
      );
  }

  //Get Assign User Defined data
  getAssignUserDefinedData(fieldId) {
    return this.httpPost(
        this._serviceURL + "/UserDefined/getUserDefinedAssign",
        fieldId
      
      );
  }

  //Delete User Defined field
  deleteUserDefunedField(fieldId) {
    return this.httpPost(
        this._serviceURL + "/UserDefined/delete",
        fieldId
      
      );
  }

  //Update Assign User Defined data
  updatUserDefunedData(formdata) {
    return this.httpPost(
        this._serviceURL + "/UserDefined/updateAssign",
        formdata
      
      );
  }

  getTranslationsData(data) {
    return this.httpPost(`${this._serviceURL}/UserDefined/getUserDefinedTranslation`, data);
  }

  updateTranslations(data) {
    return this.httpPost(`${this._serviceURL}/UserDefined/updateTranslation`, data);
  }
}
