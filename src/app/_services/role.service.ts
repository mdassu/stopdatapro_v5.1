import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Role } from "../_models/role";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({ providedIn: "root" })

export class RoleService extends ApiBaseService {

  role = Role.superAdministrator;

  // constructor(
  //   http: HttpClient,
  //   env: EnvService
  // ){
  //   super(http, env)
  // }

  getRole() {
    return this.role;
  }

  getRoleData(role: String) {
    return this.httpPost(this._serviceURL + "/role/data", {
      role
    });
  }
  setRole(role: Role) {
    this.role = role;
  }
  getAllRoles() {
    const response = this.httpPost(
      this._serviceURL + "/UserRole/GetRoleData",
      {}
    );

    return response;
  }

  getRoleEditData(id: number) {
    let data = { roleId: id };
    const response = this.httpPost(
      this._serviceURL + "/UserRole/GetRoleData",
      data
    );

    return response;
  }

  deleteRoles(data) {
    const response = this.httpPost(this._serviceURL + "/UserRole/delete", data);

    return response;
  }

  changeRoleStatus(data) {
    const response = this.httpPost(
      this._serviceURL + "/UserRole/updateStatus",
      data
    );

    return response;
  }

  getRoleAssign(data) {
    const response = this.httpPost(
      this._serviceURL + "/UserRole/getRoleAssign",
      data
    );

    return response;
  }

  updateRole(data) {
    const response = this.httpPost(
      this._serviceURL + "/UserRole/updateRole",
      data
    );

    return response;
  }

  updateUserRoleAssign(data) {
    const response = this.httpPost(
      this._serviceURL + "/UserRole/updateRoleAssign",
      data
    );

    return response;
  }

  userRoleInsert(data) {
    const response = this.httpPost(this._serviceURL + "/UserRole/insert", data);

    return response;
  }
  getRolePermission(data) {
    return this.httpPost(
      this._serviceURL + "/UserRole/getRolePermission",
      data
    );
  }

  getRoleReport(data) {
    return this.httpPost(this._serviceURL + "/UserRole/getRoleReport", data);
  }

  updateRoleReport(data) {
    return this.httpPost(this._serviceURL + "/UserRole/updateRoleReport", data);
  }

  updateRolePermission(data) {
    return this.httpPost(
      this._serviceURL + "/UserRole/updateRolePermission",
      data
    );
  }
}
