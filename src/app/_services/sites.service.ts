import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { tap } from "rxjs/internal/operators";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class SitesService extends ApiBaseService {
 

  //Get Add Sites
  getAddSiteData() {
    return this.httpPost(this._serviceURL + "/Site/siteAdd", {}
    );
  }

  //Get Sites
  getSiteData(siteId) {
    return this.httpPost(this._serviceURL + "/Site/GetSiteData", siteId
    );
  }

  //site Insert
  insertSite(formData) {
    return this.httpPost(this._serviceURL + "/Site/Insert", formData
    );
  }

  //site Update
  updateSite(formData) {
    return this.httpPost(this._serviceURL + "/Site/Update", formData
    );
  }

  //Delete Sites
  deleteSites(siteId) {
    return this.httpPost(this._serviceURL + "/Site/Delete", siteId
    );
  }

  //Update Sites
  updateSiteStatus(siteId) {
    return this.httpPost(this._serviceURL + "/Site/updatestatus", siteId
    );
  }

  //Get Site Assignment
  getSiteAssignData(siteId) {
    return this.httpPost(
      this._serviceURL + "/Site/GetSiteAssignData",
      siteId
    
    );
  }

  //Update Site Assignment
  updateSiteAssign(siteId) {
    return this.httpPost(
      this._serviceURL + "/Site/updateSiteAssign",
      siteId
   
    );
  }

  //Update Target Data
  updateTargetData(siteId) {
    return this.httpPost(this._serviceURL + "/Site/TargetData", siteId
    );
  }

  getKeyWord(data) {
    return this.httpPost(this._serviceURL + "/Site/getKeyWord", data);
  }

  sendMailForSite(data) {
    return this.httpPost(this._serviceURL + "/Site/sendMailForSite", data);
  }

  getAreaManagerData (data) {
    return this.httpPost(
      this._serviceURL + "/Site/getAreaManagerData",
      data
   
    );
  }

  updateAreaManager(data) {
    return this.httpPost(
      this._serviceURL + "/Site/updateAreaManager",
      data
   
    );
  }
}
