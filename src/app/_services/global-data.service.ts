import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class GlobalDataService extends ApiBaseService {
  get GlobalData() {
    if (localStorage.getItem("globalData")) {
      // logged in so return user details
      const global = JSON.parse(localStorage.getItem("globalData"));
      if (global) {
        return global;
      } else {
        return null;
      }
    }
  }

  getGlobalData(data) {
    return this.httpPost(`${this._serviceURL}/Profile/globalData`, data);
  }
}
