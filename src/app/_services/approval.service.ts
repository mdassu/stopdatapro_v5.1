import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class ApprovalService extends ApiBaseService {
  getApprovalList(parameters) {
    return this.httpPost(
      this._serviceURL + "/Approval/getApprovalData",
      parameters
    );
  }

  getSiteFilterData(siteId) {
    return this.httpPost(this._serviceURL + "/Approval/getSiteFilter", siteId);
  }

  getAreaFilterData(areaId) {
    return this.httpPost(
      this._serviceURL + "/Approval/getSubAreaFilter",
      areaId
    );
  }

  deleteApproval(approvalId) {
    return this.httpPost(
      this._serviceURL + "/Approval/approvalDelete",
      approvalId
    );
  }

  getApprovalGeneralData(approvalId) {
    return this.httpPost(
      this._serviceURL + "/Approval/getGroupChecklistEdit",
      approvalId
    );
  }

  getObservationsData(parameters) {
    return this.httpPost(
      this._serviceURL + "/Approval/getObservation",
      parameters
    );
  }

  insertObservationsGnData(formData) {
    return this.httpPost(
      this._serviceURL + "/Approval/updateApproval",
      formData
    );
  }

  approveApproval(formData) {
    return this.httpPost(this._serviceURL + "/Approval/approvalData", formData);
  }

  getUpdateRequest(parameters) {
    return this.httpPost(
      this._serviceURL + "/Approval/getUpdateRequest",
      parameters
    );
  }

  updateRequest(parameters) {
    return this.httpPost(
      this._serviceURL + "/Approval/insertUpdateReq",
      parameters
    );
  }

  getCategoryComments(parameters) {
    return this.httpPost(
      this._serviceURL + "/Approval/getObservationComment",
      parameters
    );
  }

  addObservationData(parameters) {
    return this.httpPost(
      this._serviceURL + "/Approval/insertUpdateComment",
      parameters
    );
  }

  changeApprovalStatus(params) {
    return this.httpPost(this._serviceURL + "/Approval/approvalData", params);
  }
}
