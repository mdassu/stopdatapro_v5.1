import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { tap } from "rxjs/internal/operators";
import { Area } from "../_models/area";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class AreaService extends ApiBaseService {
  //Insert Area
  insertArea(formData) {
    return this.httpPost(this._serviceURL + "/SiteArea/Insert", formData
    );
  }

  //Update Area
  updateArea(formData) {
    return this.httpPost(this._serviceURL + "/SiteArea/Update", formData
    );
  }

  //Get Area
  getAreaData(areaId) {
    return this.httpPost(
      this._serviceURL + "/SiteArea/GetSiteData",
      areaId
    );
  }

  //Get Add Area
  getAddAreaData() {
    return this.httpPost(this._serviceURL + "/SiteArea/addAreaData", {}
    );
  }

  //Delete Area
  deleteArea(areaId) {
    return this.httpPost(this._serviceURL + "/SiteArea/Delete", areaId );
  }

  //Update Area Status
  updateAreaStatus(areaId) {
    return this.httpPost(
      this._serviceURL + "/SiteArea/updatestatus",
      areaId
    );
  }
}
