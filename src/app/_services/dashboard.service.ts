import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class DashboardService extends ApiBaseService {
  getDashboardData(data) {
    return this.httpPost(`${this._serviceURL}/Profile/dashBoardInfo`, data);
  }
}
