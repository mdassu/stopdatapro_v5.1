import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class EmailInboxService extends ApiBaseService {
  getEmailData(data) {
    var res = this.httpPost(`${this._serviceURL}/Inbox/getInboxData`, data);
    return res;
  }

  deleteEmailData(data) {
    return this.httpPost(`${this._serviceURL}/Inbox/deleteInbox`, data);
  }

  getEmailByID(data) {
    return this.httpPost(`${this._serviceURL}/Inbox/getInboxDataInfo`, data);
  }
}
