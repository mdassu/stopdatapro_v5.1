import { Injectable } from '@angular/core';
import { ApiBaseService } from './api-base.service';
import { HttpClient } from '@angular/common/http';
import { EnvService } from 'src/env.service';


@Injectable({
  providedIn: 'root'
})
export class GeneralSettingsService extends ApiBaseService{

  // constructor (
  //   http: HttpClient,
  //   env: EnvService
  // ) {
  //   super(http, env)
  // }

  getGeneralSettings(data){
    return this.httpPost(`${this._serviceURL}/Configuration/getGeneralData`, data);
  }

  updateGeneralSettings(data){
    return this.httpPost(`${this._serviceURL}/Configuration/updateGeneralData`, data);
  }
}
