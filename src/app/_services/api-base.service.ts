import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Headers } from "@angular/http";
import { EnvService } from "src/env.service";
import { EncrDecrService } from "./encr-decr.service";
import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";

@Injectable({ providedIn: "root" })
export class ApiBaseService {
  _headers: any;
  _translate: any;
  _serviceURL: string;
  _encryp: any;
  encrypKey: string = "Se5ZfFTzJ36PH7T6";
  constructor(
    private _http: HttpClient,
    private env: EnvService,
    private EncrDecr: EncrDecrService,
    private translate: TranslateService
  ) {
    this._headers = new Headers({
      "Content-Type": "application/json"
    });

    this._serviceURL = this.env.apiUrl;
    this._translate = this.translate;
  }

  private generateParam(data: object): HttpParams {
    let params = new HttpParams();
    // tslint:disable-next-line: forin
    for (const i in data) {
      params = params.append(i, data[i]);
    }

    return params;
  }

  protected httpGet(url: string, data: object = null): Observable<any> {
    let params = new HttpParams();
    const headers = this._headers;
    if (data != null) {
      params = this.generateParam(data);
    }
    const response = this._http
      .get(url, { params: params, headers: headers })
      .pipe(map((res: Response) => res));
    return response;
  }

  protected httpPost(url: string, data: any): Observable<any> {
    const encData = this.EncrDecr.set(this.encrypKey, JSON.stringify(data));
    const response = this._http
      .post(url, { hdndata: encData }, { headers: this._headers })
      .pipe(map((res: Response) => res));
    return response;
  }
}
