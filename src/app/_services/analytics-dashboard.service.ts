import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { tap } from "rxjs/internal/operators";
import { AuthenticationService } from "./authentication.service";
import { BehaviorSubject } from "rxjs";
import { EnvService } from "src/env.service";

@Injectable({
  providedIn: "root"
})
export class AnalyticsDashboardService {
  _serviceURL: string;
  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private router: Router,
    private env: EnvService,
    private auth: AuthenticationService
  ) {
    this._serviceURL = this.env.apiUrl;
  }

  GetHttpHeaders(): HttpHeaders {
    const headers = new HttpHeaders().set(
      "Authorization",
      decodeURI("Bearer " + this.auth.UserInfo["token"])
    );
    return headers;
  }

  checkTPCookie() {
    return this.http.get(this._serviceURL + "/Profile/getcookiesValue");
  }

  getTableauTicket(userName) {
    return this.http.post(
      this._serviceURL + "/AnalyticsDashboard/getTableauTicket",
      userName
    );
  }
}
