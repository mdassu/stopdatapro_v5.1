import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ObservationChecklistService extends ApiBaseService {
  getObservationList(params) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/getObservationList",
      params
    );
  }

  getObsFormData(params) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/getObservationAdd",
      params
    );
  }

  getDataOfNewObserver() {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/getObserverAdd",
      {}
    );
  }

  getSubAreaData(parameters) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/getSubareadata",
      parameters
    );
  }

  addObservationForm(formData) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/insertChecklistObservation",
      formData
    );
  }

  deleteChecklist(formData) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/deleteObservationChecklist",
      formData
    );
  }

  getObserverAddData() {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/getObserverAdd",
      {}
    );
  }

  addNewObserver(userData) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/insertObserver",
      userData
    );
  }

  getAreaFilter(data) {
    return this.httpPost(this._serviceURL + "/Approval/getSiteFilter", data);
  }

  getSubAreaFilter(data) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/getSubAreaFilter",
      data
    );
  }

  getEditFormData(data) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/getObservationListForEdit",
      data
    );
  }

  editObservationForm(formData) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/updateChecklistObservation",
      formData
    );
  }

  addObservationData(params) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/insertUpdateComment",
      params
    );
  }

  getCategoryComments(params) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/getCategoryComment",
      params
    );
  }

  getCorrectiveData(params) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/getCorrectiveAction",
      params
    );
  }

  addCorrectiveAction(params) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/insertCorrectiveAction",
      params
    );
  }

  updateCorrectiveAction(params) {
    return this.httpPost(
      this._serviceURL + "/ObservationChecklist/updateCorrectiveAction",
      params
    );
  }
}
