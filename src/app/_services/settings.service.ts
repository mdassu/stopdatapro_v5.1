import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { tap } from "rxjs/internal/operators";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class SettingsService extends ApiBaseService {
  // constructor(
  //   private cookieService: CookieService,
  //   private router: Router,
  //   http: HttpClient,
  //   env: EnvService
  // ) {
  //   super(http, env);
  // }

  // GetHttpHeaders(): HttpHeaders {
  //   const headers = new HttpHeaders()
  //     .set(
  //       "Authorization",
  //       decodeURI("Bearer " + this.cookieService.get("access-token"))
  //     )
  //     .set("Content-Type", "application/json");
  //   return headers;
  // }

  //General Setting
  getGeneralData() {
    return this.httpPost(
      this._serviceURL + "/Configuration/getGeneralData",
      {}
    );
  }

  updateGeneral(formdata) {
    return this.httpPost(
      this._serviceURL + "/Configuration/updateGeneralData",
      formdata
    );
  }

  //Application Setting
  getApplicationData() {
    return this.httpPost(
      this._serviceURL + "/Configuration/getAppConfigData",
      {}
    );
  }

  updateApplication(formdata) {
    return this.httpPost(
      this._serviceURL + "/Configuration/updateAppConfigData",
      formdata
    );
  }

  //CheckList Setting
  getCheckListData() {
    return this.httpPost(
      this._serviceURL + "/Configuration/getChecklistConfigData",
      {}
    );
  }

  updateCheckList(formdata) {
    return this.httpPost(
      this._serviceURL + "/Configuration/updateChecklistConfigData",
      formdata
    );
  }

  getConfigurationTranslations(formdata) {
    return this.httpPost(
      this._serviceURL + "/Configuration/getTranslation",
      formdata
    );
  }
  updateConfigurationTranslations(formdata) {
    return this.httpPost(
      this._serviceURL + "/Configuration/updateTranslation",
      formdata
    );
  }

  updateGeneral2(formdata) {
    return this.httpPost(
      this._serviceURL + "/Configuration/updateGeneralData2",
      formdata
    );
  }

  removeImage() {
    return this.httpPost(this._serviceURL + "/Profile/updateLogo", {});
  }
}
