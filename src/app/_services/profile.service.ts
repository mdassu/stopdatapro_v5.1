import { Injectable } from '@angular/core';
import { ApiBaseService } from './api-base.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService extends ApiBaseService{

  getProfileData (data) {
    return this.httpPost(`${this._serviceURL}/Profile/getProfileInfo`, data);
  }

  updateProfileData (data) {
    return this.httpPost(`${this._serviceURL}/Profile/updateProfileInfo`, data);
  }

}
