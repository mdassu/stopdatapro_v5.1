import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { UserModel } from "../_models/userModel";
import { HttpClient } from "@angular/common/http";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";
import { BehaviorSubject } from "rxjs";

@Injectable({ providedIn: "root" })
export class AuthenticationService extends ApiBaseService {
  private userSource = new BehaviorSubject("");
  currentUserId = this.userSource.asObservable();

  get UserInfo(): UserModel {
    var userId;
    this.currentUserId.subscribe((id) => {
      userId = id;
    });
    if (localStorage.getItem("currentUser-" + userId)) {
      // logged in so return user details
      const user: UserModel = JSON.parse(
        localStorage.getItem("currentUser-" + userId)
      );
      if (user) {
        return user;
      } else {
        return null;
      }
    }
  }

  calLang() {
    var lang;

    this._translate
      .get([
        "FMKSUNDAY",
        "FMKMONDAY",
        "FMKTUESDAY",
        "FMKWEDNESDAY",
        "FMKTHURSDAY",
        "FMKFRIDAY",
        "FMKSATURDAY",
        "FMKSUN",
        "FMKMON",
        "FMKTUE",
        "FMKWED",
        "FMKTHU",
        "FMKFRI",
        "FMKSAT",
        "FMKSU",
        "FMKMO",
        "FMKTU",
        "FMKWE",
        "FMKTH",
        "FMKFR",
        "FMKSA",
        "FMKJANUARY",
        "FMKFEBRUARY",
        "FMKMARCH",
        "FMKAPRIL",
        "FMKMAY",
        "FMKJUNE",
        "FMKJULY",
        "FMKAUGUST",
        "FMKSEPTEMBER",
        "FMKOCTOBER",
        "FMKNOVEMBER",
        "FMKDECEMBER",
        "FMKJAN",
        "FMKFEB",
        "FMKMAR",
        "FMKAPR",
        "FMKMAY",
        "FMKJUN",
        "FMKJUL",
        "FMKAUG",
        "FMKSEP",
        "FMKOCT",
        "FMKNOV",
        "FMKDEC",
        "FMKCURRENTTEXT",
        "FMKCLR",
        "LBLDMY",
        "LBLMDY",
        "LBLYMD",
      ])
      .subscribe((resLabel) => {
        lang = {
          firstDayOfWeek: 0,
          dayNames: [
            resLabel["FMKSUNDAY"],
            resLabel["FMKMONDAY"],
            resLabel["FMKTUESDAY"],
            resLabel["FMKWEDNESDAY"],
            resLabel["FMKTHURSDAY"],
            resLabel["FMKFRIDAY"],
            resLabel["FMKSATURDAY"],
          ],
          dayNamesShort: [
            resLabel["FMKSUN"],
            resLabel["FMKMON"],
            resLabel["FMKTUE"],
            resLabel["FMKWED"],
            resLabel["FMKTHU"],
            resLabel["FMKFRI"],
            resLabel["FMKSAT"],
          ],
          dayNamesMin: [
            resLabel["FMKSU"],
            resLabel["FMKMO"],
            resLabel["FMKTU"],
            resLabel["FMKWE"],
            resLabel["FMKTH"],
            resLabel["FMKFR"],
            resLabel["FMKSA"],
          ],
          monthNames: [
            resLabel["FMKJANUARY"],
            resLabel["FMKFEBRUARY"],
            resLabel["FMKMARCH"],
            resLabel["FMKAPRIL"],
            resLabel["FMKMAY"],
            resLabel["FMKJUNE"],
            resLabel["FMKJULY"],
            resLabel["FMKAUGUST"],
            resLabel["FMKSEPTEMBER"],
            resLabel["FMKOCTOBER"],
            resLabel["FMKNOVEMBER"],
            resLabel["FMKDECEMBER"],
          ],
          monthNamesShort: [
            resLabel["FMKJAN"],
            resLabel["FMKFEB"],
            resLabel["FMKMAR"],
            resLabel["FMKAPR"],
            resLabel["FMKMAY"],
            resLabel["FMKJUN"],
            resLabel["FMKJUL"],
            resLabel["FMKAUG"],
            resLabel["FMKSEP"],
            resLabel["FMKOCT"],
            resLabel["FMKNOV"],
            resLabel["FMKDEC"],
          ],
          today: resLabel["FMKCURRENTTEXT"],
          clear: resLabel["FMKCLR"],
        };
      });
    return lang;
  }

  get dashboardInfo() {
    var userId;
    this.currentUserId.subscribe((id) => {
      userId = id;
    });
    if (localStorage.getItem("dashboard-" + userId)) {
      const info = JSON.parse(localStorage.getItem("dashboard-" + userId));
      if (info) {
        return info;
      }
    }
  }

  //update UserAlert
  updateUserAlert(formData) {
    return this.httpPost(this._serviceURL + "/Login/UpdateUserAlert", formData);
  }

  changeUser(id: any) {
    this.userSource.next(id);
  }

  public login(username: string, password: string) {
    const userdata = {
      userName: username,
      pass: password,
    };

    const response = this.httpPost(
      this._serviceURL + "/Login/LogCheckToken",
      userdata
    );

    return response.pipe(
      map((data) => {
        // login successful if there's a Bearer token in the response
        if (data && data["status"] && data["Data"].token) {
          this.changeUser(data["Data"]["userId"]);
          this.currentUserId.subscribe((userId) => {
            localStorage.setItem(
              "currentUser-" + userId,
              JSON.stringify(data["Data"])
            );
            localStorage.setItem(
              "rolePermission-" + userId,
              JSON.stringify(data["rolePermission"])
            );
            localStorage.setItem(
              "dashboardRolePermission-" + userId,
              JSON.stringify(data["userDashboardPermission"])
            );
            localStorage.setItem(
              "dashboard-" + userId,
              JSON.stringify(data["dashboard"][0])
            );
          });
          return { status: true, message: data["message"] };
        } else {
          return { status: false, message: data["message"] };
        }
      })
    );
  }

  logout() {
    // remove user from local storage to log user out
    // localStorage.clear();
    if (this.UserInfo) {
      localStorage.removeItem("rolePermission-" + this.UserInfo["userId"]);
      localStorage.removeItem("dashboard-" + this.UserInfo["userId"]);
      localStorage.removeItem(
        "dashboardRolePermission-" + this.UserInfo["userId"]
      );
      localStorage.removeItem("previousUrl-" + this.UserInfo["userId"]);
      localStorage.removeItem("currentUser-" + this.UserInfo["userId"]);
    }
  }

  rearrangeSelect(dataArray, selectedIDs) {
    dataArray.map((records) => {
      var tempArr = [];
      var tempDataArr;
      tempDataArr = records.Data;
      selectedIDs.map((id) => {
        records.Data.map((data, key) => {
          if (data.value == id) {
            tempArr.push(data);
            tempDataArr.splice(key, 1);
          }
        });
      });
      var temp = tempArr.concat(tempDataArr);
      records.Data = temp;
    });
  }

  rearrangeSelects(dataArray, selectedIDs) {
    var tempArr = [];
    var tempDataArr;
    tempDataArr = dataArray;
    selectedIDs.map((id) => {
      dataArray.map((data, key) => {
        if (data.value == id) {
          tempArr.push(data);
          tempDataArr.splice(key, 1);
        }
      });
    });
    var temp = tempArr.concat(tempDataArr);
    return temp;
  }

  changeCellValueType(value) {
    var dateFormat = this.UserInfo["dateFormat"];
    var numFmt;
    if (value && !isNaN(value)) {
      return (numFmt = "0");
    } else if (value && (value.match(/\//g) || []).length == 2) {
      return (numFmt = dateFormat.toLowerCase());
    } else {
      return "";
    }
  }
}
