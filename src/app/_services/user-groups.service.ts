import { Injectable } from "@angular/core";
import { EnvService } from "src/env.service";
import { HttpClient } from "@angular/common/http";
import { ApiBaseService } from "./api-base.service";

@Injectable({ providedIn: "root" })
export class UserGroupService extends ApiBaseService {
  

  getAllGroups() {
    return this.httpPost(this._serviceURL + "/UserGroup/GetGroupType", {});
  }

  getUserGroups(groupTypeId) {
    return this.httpPost(this._serviceURL + "/UserGroup/GetUserData", {
      groupTypeId: groupTypeId
    });
  }

  getGroupEditData(groupTypeId, id: number) {
    let data = { groupTypeId: groupTypeId, groupId: id };
    const response = this.httpPost(
      this._serviceURL + "/UserGroup/GetUserData",
      data
    );

    return response;
  }

  updateUserGroupData(data) {
    const response = this.httpPost(
      this._serviceURL + "/UserGroup/update",
      data
    );

    return response;
  }

  deleteUserGroup(data) {
    const response = this.httpPost(
      this._serviceURL + "/UserGroup/Delete",
      data
    );

    return response;
  }
  changeGroupStatus(data) {
    const response = this.httpPost(
      this._serviceURL + "/UserGroup/UpdateStatus",
      data
    );

    return response;
  }

  addGroupData(data) {
    const response = this.httpPost(
      this._serviceURL + "/UserGroup/Insert",
      data
    );

    return response;
  }

  updateUserAssign(data) {
    return this.httpPost(this._serviceURL + "/UserGroup/updateAssign", data);
  }

  getUserTranslationData(data) {
    return this.httpPost(
      `${this._serviceURL}/UserGroup/getGroupTranslation`,
      data
    );
  }

  updateUserGroupTranslations(data) {
    return this.httpPost(
      `${this._serviceURL}/UserGroup/updateTranslation`,
      data
    );
  }
}
