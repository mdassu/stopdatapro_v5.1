import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class RedFlagService extends ApiBaseService {
  getRedFlagList(parameters) {
    return this.httpPost(
      this._serviceURL + "/RedFlag/getRedFlagList",
      parameters
    );
  }

  deleteRedFlag(flagId) {
    return this.httpPost(this._serviceURL + "/RedFlag/delete", flagId);
  }

  updateRedFlagStatus(flagId) {
    return this.httpPost(this._serviceURL + "/RedFlag/changeStatus", flagId);
  }

  getAreaBySiteId(siteId) {
    return this.httpPost(this._serviceURL + "/RedFlag/getAreaFilter", siteId);
  }

  getSubAreaByAreaId(areaId) {
    return this.httpPost(
      this._serviceURL + "/RedFlag/getSubAreaFilter",
      areaId
    );
  }

  redFlagAddData() {
    return this.httpPost(this._serviceURL + "/RedFlag/getRedFlagAdd", {});
  }

  getSiteData(siteId) {
    return this.httpPost(this._serviceURL + "/RedFlag/getRedFlagSite", siteId);
  }

  getAreaData(areaId) {
    return this.httpPost(this._serviceURL + "/RedFlag/getRedFlagArea", areaId);
  }

  getCheckListData(checklistId) {
    return this.httpPost(
      this._serviceURL + "/RedFlag/getRedFlagChecklist",
      checklistId
    );
  }

  getMainCatData(mainCatData) {
    return this.httpPost(
      this._serviceURL + "/RedFlag/getRedFlagMainCateg",
      mainCatData
    );
  }

  insertRedFlag(formData) {
    return this.httpPost(this._serviceURL + "/RedFlag/insert", formData);
  }

  editRedFlagData(redFlagId) {
    return this.httpPost(
      this._serviceURL + "/RedFlag/getRedFlagDataEdit",
      redFlagId
    );
  }

  updateRedFlag(formData) {
    return this.httpPost(this._serviceURL + "/RedFlag/update", formData);
  }
}
