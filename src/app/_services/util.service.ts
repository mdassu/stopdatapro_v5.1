import { Injectable } from "@angular/core";
import cssVars from "css-vars-ponyfill";

@Injectable({
  providedIn: "root"
})
export class UtilService {
  startTime: any = [
    "00:00",
    "00:30",
    "01:00",
    "01:30",
    "02:00",
    "02:30",
    "03:00",
    "03:30",
    "04:00",
    "04:30",
    "05:00",
    "05:30",
    "06:00",
    "06:30",
    "07:00",
    "07:30",
    "08:00",
    "08:30",
    "09:00",
    "09:30",
    "10:00",
    "10:30",
    "11:00",
    "11:30",
    "12:00",
    "12:30",
    "13:00",
    "13:30",
    "14:00",
    "14:30",
    "15:00",
    "15:30",
    "16:00",
    "16:30",
    "17:00",
    "17:30",
    "18:00",
    "18:30",
    "19:00",
    "19:30",
    "20:00",
    "20:30",
    "21:00",
    "21:30",
    "22:00",
    "22:30",
    "23:00",
    "23:30"
  ];
  defaultBrandColor = "hsl(174,100%,29.4117647059%)";
  defaultFontColor = "hsl(0,100%,100%)";
  cssVarObj = {
    "--primary-color": "",
    "--primary-color-h": "",
    "--primary-color-s": "",
    "--primary-color-l": "",
    "--primary-color-lighten-10": "",
    "--primary-color-lighten-25": "",
    "--primary-color-lighten-6": "",
    "--primary-color-darken-10": "",
    "--primary-color-lighten-25-desaturate-25": "",
    "--buttonTextColor": ""
  };

  themeChange(hslvalue) {
    let colorArr = /hsla?\((\d+),\s*([\d.]+)%,\s*([\d.]+)%(,.+)?/.exec(
      hslvalue
    );
    const primaryColor = hslvalue;
    const primaryColorH = colorArr[1];
    const primaryColorS = colorArr[2] + "%";
    const primaryColorL = colorArr[3] + "%";
    const primaryColorLighten10 = `hsl(${colorArr[1]},${colorArr[2] +
      "%"},${Number(colorArr[3]) + 10 + "%"})`;
    const primaryColorLighten25 = `hsl(${colorArr[1]},${colorArr[2] +
      "%"},${Number(colorArr[3]) + 25 + "%"})`;
    const primaryColorLighten6 = `hsl(${colorArr[1]},${colorArr[2] +
      "%"},${Number(colorArr[3]) + 6 + "%"})`;
    const primaryColorDarken10 = `hsl(${colorArr[1]},${colorArr[2] +
      "%"},${Number(colorArr[3]) - 10 + "%"})`;
    const primaryColorLighten25Desaturate25 = `hsl(${colorArr[1]},${Number(
      colorArr[2]
    ) -
      25 +
      "%"},${Number(colorArr[3]) - 25 + "%"})`;
    document.documentElement.style.setProperty("--primary-color", hslvalue);
    document.documentElement.style.setProperty(
      "--primary-color-h",
      primaryColorH
    );
    document.documentElement.style.setProperty(
      "--primary-color-s",
      primaryColorS
    );
    document.documentElement.style.setProperty(
      "--primary-color-l",
      primaryColorL
    );
    document.documentElement.style.setProperty(
      `--primary-color-lighten-10`,
      primaryColorLighten10
    );
    document.documentElement.style.setProperty(
      `--primary-color-lighten-25`,
      primaryColorLighten25
    );
    document.documentElement.style.setProperty(
      `--primary-color-lighten-6`,
      primaryColorLighten6
    );
    document.documentElement.style.setProperty(
      `--primary-color-darken-10`,
      primaryColorDarken10
    );
    document.documentElement.style.setProperty(
      `--primary-color-lighten-25-desaturate-25`,
      primaryColorLighten25Desaturate25
    );

    this.cssVarObj["--primary-color"] = primaryColor;
    this.cssVarObj["--primary-color-h"] = primaryColorH;
    this.cssVarObj["--primary-color-s"] = primaryColorS;
    this.cssVarObj["--primary-color-l"] = primaryColorL;
    this.cssVarObj["--primary-color-lighten-10"] = primaryColorLighten10;
    this.cssVarObj["--primary-color-lighten-25"] = primaryColorLighten25;
    this.cssVarObj["--primary-color-lighten-6"] = primaryColorLighten6;
    this.cssVarObj["--primary-color-darken-10"] = primaryColorDarken10;
    this.cssVarObj[
      "--primary-color-lighten-25-desaturate-25"
    ] = primaryColorLighten25Desaturate25;

    cssVars({
      variables: this.cssVarObj
    });
  }

  fontChange(hslvalue) {
    document.documentElement.style.setProperty(`--buttonTextColor`, hslvalue);
    this.cssVarObj["--buttonTextColor"] = hslvalue;
    cssVars({
      variables: this.cssVarObj
    });
  }

  hexToHSL(H) {
    // Convert hex to RGB first
    let r: any = 0,
      g: any = 0,
      b: any = 0;
    if (H.length == 4) {
      r = "0x" + H[1] + H[1];
      g = "0x" + H[2] + H[2];
      b = "0x" + H[3] + H[3];
    } else if (H.length == 7) {
      r = "0x" + H[1] + H[2];
      g = "0x" + H[3] + H[4];
      b = "0x" + H[5] + H[6];
    }
    // Then to HSL
    r /= 255;
    g /= 255;
    b /= 255;
    let cmin = Math.min(r, g, b),
      cmax = Math.max(r, g, b),
      delta = cmax - cmin,
      h = 0,
      s = 0,
      l = 0;

    if (delta == 0) h = 0;
    else if (cmax == r) h = ((g - b) / delta) % 6;
    else if (cmax == g) h = (b - r) / delta + 2;
    else h = (r - g) / delta + 4;

    h = Math.round(h * 60);

    if (h < 0) h += 360;

    l = (cmax + cmin) / 2;
    s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
    s = +(s * 100).toFixed(1);
    l = +(l * 100).toFixed(1);

    return "hsl(" + h + "," + s + "%," + l + "%)";
  }
}
