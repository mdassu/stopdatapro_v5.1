import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({ providedIn: "root" })
export class ApplicationConfigService extends ApiBaseService {
  getApplicationConfigData(data) {
    return this.httpPost(
      this._serviceURL + "/Configuration/getAppConfigData",
      data
    );
  }

  updateApplicationConfigData(data) {
    return this.httpPost(
      this._serviceURL + "/Configuration/updateAppConfigData",
      data
    );
  }
}
