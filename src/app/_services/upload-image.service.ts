import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { tap } from "rxjs/internal/operators";
import { AuthenticationService } from "./authentication.service";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class UploadImageService {

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private router: Router,
    private auth: AuthenticationService
  ) {http}

  GetHttpHeaders(): HttpHeaders {
    const headers = new HttpHeaders()
      .set(
        "Authorization",
        decodeURI("Bearer " + this.auth.UserInfo["token"])
      );
      
    return headers;
  }

  private imageSource = new BehaviorSubject("");
  currentImage = this.imageSource.asObservable();

  //Upload Image
  uploadImage(formData) {
    return this.http.post(
        this.auth.UserInfo["uploadUrl"],
        formData,
        { headers: this.GetHttpHeaders() }
      )
  }

  changeImage(img: any){
    this.imageSource.next(img);
  }

  //Upload Image
  uploadImageNotAuth(formData) {
    return this.http.post(
        'https://customcontentdp50qcssl.stopdatapro.com/api/Upload/fileupload',
        formData
        // { headers: this.GetHttpHeaders() }
      )
  }

  //Upload Image
  uploadImageNotLocalAuth(formData) {
    return this.http.post(
      'http://13.235.72.18/api/Upload/fileuploadwithauth',
        // this.auth.UserInfo["uploadUrl"],
        formData,
        { headers: this.GetHttpHeaders() }
      )
  }
}