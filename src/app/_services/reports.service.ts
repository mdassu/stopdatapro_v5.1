import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

export class ComplaintsWithPercent {
  complaint: string;
  count: number;
  cumulativePercent: number;
}

@Injectable({
  providedIn: "root"
})
export class ReportsService extends ApiBaseService {
  // constructor(http: HttpClient, env: EnvService) {
  //   super(http, env);
  // }

  get FilterInfo() {
    if (localStorage.getItem("filterInfo")) {
      const filter = JSON.parse(localStorage.getItem("filterInfo"));
      if (filter) {
        return filter;
      } else {
        return null;
      }
    }
  }

  convertHslToHex(h, s, l) {
    h /= 360;
    s /= 100;
    l /= 100;
    let r, g, b;
    if (s === 0) {
      r = g = b = l; // achromatic
    } else {
      const hue2rgb = (p, q, t) => {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
        return p;
      };
      const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      const p = 2 * l - q;
      r = hue2rgb(p, q, h + 1 / 3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1 / 3);
    }
    const toHex = x => {
      const hex = Math.round(x * 255).toString(16);
      return hex.length === 1 ? "0" + hex : hex;
    };
    return `${toHex(r)}${toHex(g)}${toHex(b)}`;
  }

  svgString2Image(svgString, width, height, format, callback) {
    // set default for format parameter
    format = format ? format : "png";
    // SVG data URL from SVG string
    var svgData =
      "data:image/svg+xml;base64," +
      btoa(unescape(encodeURIComponent(svgString)));
    // create canvas in memory(not in DOM)
    var canvas = document.createElement("canvas");
    // get canvas context for drawing on canvas
    var context = canvas.getContext("2d");
    // set canvas size
    canvas.width = width;
    canvas.height = height;
    // create image in memory(not in DOM)
    var image = new Image();
    // later when image loads run this
    image.onload = function() {
      // async (happens later)
      // clear canvas
      context.clearRect(20, 20, width, height - 24);
      // draw image with SVG data to canvas
      context.drawImage(image, 20, 20, width, height - 24);
      // snapshot canvas as png
      var pngData = canvas.toDataURL("image/" + format);
      // pass png data URL to callback
      callback(pngData);
    }; // end async
    // start loading SVG data into in memory image
    image.src = svgData;
  }

  getReportList() {
    return this.httpPost(`${this._serviceURL}/Reports/getReportList`, {});
  }

  getFiltersData(params) {
    return this.httpPost(`${this._serviceURL}/Reports/getReportFilter`, params);
  }

  getEditFilter(params) {
    return this.httpPost(
      `${this._serviceURL}/Reports/getEditReportData`,
      params
    );
  }

  editFilter(params) {
    return this.httpPost(
      `${this._serviceURL}/Reports/updateEditReportData`,
      params
    );
  }

  saveNewFilter(params) {
    return this.httpPost(`${this._serviceURL}/Reports/saveFilter`, params);
  }

  deleteFilterCriteria(params) {
    return this.httpPost(`${this._serviceURL}/Reports/deleteFilter`, params);
  }

  getReportData(request) {
    return this.httpPost(`${this._serviceURL}/Reports/SitesReport`, request);
  }

  getDataBySite(params) {
    return this.httpPost(`${this._serviceURL}/Reports/getSiteFilter`, params);
  }

  getGroupByList(params) {
    return this.httpPost(
      `${this._serviceURL}/Reports/getGroupByFilter`,
      params
    );
  }

  getDataByArea(params) {
    return this.httpPost(
      `${this._serviceURL}/Reports/getSubAreaFilter`,
      params
    );
  }

  getSubReportData(params) {
    return this.httpPost(
      `${this._serviceURL}/Reports/getSubReportData`,
      params
    );
  }
}
