import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class CategoryEditService extends ApiBaseService {

  getCategoryDataForEdit(data) {
    return this.httpPost(
      this._serviceURL + "/ChecklistConfig/viewCategoryInfo",
      data
    );
  }

  addSubCategory(data) {
    return this.httpPost(
      this._serviceURL + "/ChecklistConfig/InsertSubCategory",
      data
    );
  }

  updateSubCategoryAssign(data) {
    return this.httpPost(
      this._serviceURL + "/ChecklistConfig/updateSubCategoryAssign",
      data
    );
  }

  deleteSubCategory(data) {
    return this.httpPost(
      this._serviceURL + "/ChecklistConfig/deleteSubCategory",
      data
    );
  }

  updateMainCategory(data) {
    return this.httpPost(
      this._serviceURL + "/ChecklistConfig/updateCategory",
      data
    );
  }

  addCategory(data) {
    return this.httpPost(
      this._serviceURL + "/ChecklistConfig/insertCategory",
      data
    );
  }
}
