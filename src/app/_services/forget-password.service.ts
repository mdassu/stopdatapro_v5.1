import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class ForgetPasswordService extends ApiBaseService {
  forgetPassword(data) {
    return this.httpPost(`${this._serviceURL}/Login/ForgetPass`, data);
  }
}
