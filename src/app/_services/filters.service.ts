import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class FiltersService extends ApiBaseService{
  // constructor(http: HttpClient,env: EnvService) {
  //   super(http, env)
  // }

  getAllSites() {
    return this.httpGet(`${this._serviceURL}/sites`);
  }
  getAllRegions() {
    return this.httpGet(`${this._serviceURL}/regions`);
  }
  getAllGroups() {
    return this.httpGet(`${this._serviceURL}/groups`);
  }
  getAllDivisions() {
    return this.httpGet(`${this._serviceURL}/divisions`);
  }
  getAllDepartments() {
    return this.httpGet(`${this._serviceURL}/departments`);
  }
  getAllTitles() {
    return this.httpGet(`${this._serviceURL}/titles`);
  }
  getAllObserverStatus() {
    return this.httpGet(
      `${this._serviceURL}/observerstatus`
    );
  }
  getAllUserLevels() {
    return this.httpGet(`${this._serviceURL}/userlevels`);
  }
  getAllGroupData() {
    return this.httpGet(`${this._serviceURL}/grpdata`);
  }
}
