import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EnvService } from "src/env.service";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class TargetsService extends ApiBaseService {
  

  insert(data) {
    return this.httpPost(`${this._serviceURL}/UserTarget/insert`, data);
  }

  getUserData(data) {
    return this.httpPost(`${this._serviceURL}/UserTarget/getTargetData`, data);
  }
}
