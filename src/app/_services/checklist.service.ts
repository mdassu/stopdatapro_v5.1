import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";

@Injectable({
  providedIn: "root"
})
export class ChecklistService extends ApiBaseService {
  //Main Category
  getCategory() {
    return this.httpPost(
      this._serviceURL + "/ChecklistConfig/getCategoryData",
      {}
    );
  }

  getCategoryTranslations(data) {
    return this.httpPost(
      `${this._serviceURL}/ChecklistConfig/getCatTranslationData`,
      data
    );
  }

  updateCategoryTranslations(data) {
    return this.httpPost(
      `${this._serviceURL}/ChecklistConfig/updateTranslation`,
      data
    );
  }

  deleteCategory(mainCatId) {
    return this.httpPost(
      this._serviceURL + "/ChecklistConfig/deleteCategory",
      mainCatId
    );
  }

  updateCategoryStatus(mainCatId) {
    return this.httpPost(
      this._serviceURL + "/ChecklistConfig/updateStatus",
      mainCatId
    );
  }

  getChecklistSetupData(data) {
    return this.httpPost(
      `${this._serviceURL}/ChecklistSetup/getChecklistData`,
      data
    );
  }

  getSubCatData(data) {
    return this.httpPost(
      `${this._serviceURL}/ChecklistSetup/getSubCatData`,
      data
    );
  }

  getChecklistTableData(data) {
    return this.httpPost(
      `${this._serviceURL}/ChecklistSetup/getChecklistSetup`,
      data
    );
  }

  updateChecklistMainCategories(data) {
    return this.httpPost(
      `${this._serviceURL}/ChecklistSetup/updateMainCatAssign`,
      data
    );
  }

  updateChecklistSubCategories(data) {
    return this.httpPost(
      `${this._serviceURL}/ChecklistSetup/updateSubCatAssign`,
      data
    );
  }

  updateChecklistSites(data) {
    return this.httpPost(
      `${this._serviceURL}/ChecklistSetup/updateSiteAssign`,
      data
    );
  }

  deleteChecklist(data) {
    return this.httpPost(
      `${this._serviceURL}/ChecklistSetup/deleteCheckListSetup`,
      data
    );
  }

  updateChecklistStatus(data) {
    return this.httpPost(
      `${this._serviceURL}/ChecklistSetup/updateStatus`,
      data
    );
  }

  getChecklistData(data = {}) {
    return this.httpPost(
      this._serviceURL + "/ChecklistSetup/getChecklistSetup",
      data
    );
  }

  getChecklistPreview(data) {
    return this.httpPost(
      this._serviceURL + "/ChecklistSetup/getChecklistData",
      data
    );
  }

  insertGeneralData(data) {
    return this.httpPost(
      this._serviceURL + "/ChecklistSetup/insertCheckList",
      data
    );
  }

  updateGeneral(data) {
    return this.httpPost(
      this._serviceURL + "/ChecklistSetup/updateCheckList",
      data
    );
  }
}
