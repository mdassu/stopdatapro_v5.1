import { Injectable } from "@angular/core";
import { UserService } from "../_services/user.service";
import { Resolve } from "@angular/router";
import { UserModel } from "../_models/userModel";
import { AuthenticationService } from "./authentication.service";

@Injectable()
export class UserResolver implements Resolve<any> {
  public userModel: UserModel;
  constructor(
    private userService: UserService,
    private auth: AuthenticationService
  ) {}

  resolve() {
    // this.userModel = JSON.parse(localStorage.getItem("currentUser"));
    this.userModel = this.auth.UserInfo;
    return this.userService.getById(this.userModel.userId);
  }
}
