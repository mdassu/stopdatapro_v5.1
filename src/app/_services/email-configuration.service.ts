import { Injectable } from "@angular/core";
import { ApiBaseService } from "./api-base.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class EmailConfigurationService extends ApiBaseService {
  getEmailList(data): Observable<any> {
    return this.httpPost(`${this._serviceURL}/Email/getEmailList`, data);
  }

  deleteEmail(data): Observable<any> {
    return this.httpPost(`${this._serviceURL}/Email/deleteEmail`, data);
  }

  changeEmailStatus(data): Observable<any> {
    return this.httpPost(`${this._serviceURL}/Email/changeStatus`, data);
  }

  getEmailAdd(data): Observable<any> {
    return this.httpPost(`${this._serviceURL}/Email/getEmailAdd`, data);
  }

  getEmailTypeData(data): Observable<any> {
    return this.httpPost(`${this._serviceURL}/Email/getEmaiTypeData`, data);
  }

  getEditBody(data): Observable<any> {
    return this.httpPost(`${this._serviceURL}/Email/getEditBody`, data);
  }

  getEmailForEdit(data): Observable<any> {
    return this.httpPost(`${this._serviceURL}/Email/getEmailForEdit`, data);
  }

  getMultilingualContent(data): Observable<any> {
    return this.httpPost(
      `${this._serviceURL}/Email/getMultilingualContent`,
      data
    );
  }

  getEmaiDetailFrom(data): Observable<any> {
    return this.httpPost(`${this._serviceURL}/Email/getEmaiDetailFrom`, data);
  }

  insertEmailConfig(data): Observable<any> {
    return this.httpPost(`${this._serviceURL}/Email/insertEmailConfig`, data);
  }

  updateEmailConfig(data): Observable<any> {
    return this.httpPost(`${this._serviceURL}/Email/updateEmailConfig`, data);
  }

  updateInsertMultiLingual(data): Observable<any> {
    return this.httpPost(
      `${this._serviceURL}/Email/updateInsertMultiLingual`,
      data
    );
  }
}
