import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { ForcePasswordChangeService } from "../_services/force-password-change.service";
import { TranslateService } from "@ngx-translate/core";
import { MessageService } from "primeng/api";
import { MustMatch } from "../_helpers/must-match.validator";
import { Router } from "@angular/router";
import { GlobalDataService } from "../_services/global-data.service";
import { CustomValidatorsService } from "../_services/custom-validators.service";

@Component({
  selector: "app-force-password-change",
  templateUrl: "./force-password-change.component.html",
  styleUrls: ["./force-password-change.component.css"],
  providers: [CustomValidatorsService],
})
export class ForcePasswordChangeComponent implements OnInit {
  forcePasswordChangeForm: FormGroup;
  submitted: boolean;
  passwordMinLength: number;
  passSpecChars: number;
  passNumChars: number;
  passAlphaChars: number;
  minSpecialCharacter: boolean;
  minAlphaCharacters: boolean;
  minNumericCharacters: boolean;
  toolTipString = "";
  passMessage: any;
  passwordComplexity: number = 0;

  constructor(
    private fb: FormBuilder,
    private forcePasswordChangeService: ForcePasswordChangeService,
    private messageService: MessageService,
    private router: Router,
    private translate: TranslateService,
    private globalService: GlobalDataService,
    private customValidatorsService: CustomValidatorsService
  ) {}
  ngOnInit() {
    localStorage.setItem("force-password-change", "false");
    this.buildForm();
    this.globalService.getGlobalData({}).subscribe((res) => {
      this.translate.use(res["globalData"]["GLOBALLANGUAGE"]);
    });
    this.forcePasswordChangeService
      .getPasswordComplexity({})
      .subscribe((res) => {
        this.passwordMinLength = +res["Data"]["PWDLEN"];
        this.passSpecChars = +res["Data"]["PWDSPECCHAR"];
        this.passNumChars = +res["Data"]["PWDNUMERIC"];
        this.passAlphaChars = +res["Data"]["PWDALPHA"];
        this.toolTipMessage(+res["Data"]["PWDCOMPLEXITY"]);

        this.passwordComplexity = res["Data"]["PWDCOMPLEXITY"];

        if (res["Data"]["PWDCOMPLEXITY"] == 0) {
          this.forcePasswordChangeForm.controls["newPass"].setValidators([
            // 1. Password Field is Required
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
            // 3. Has a minimum length of characters
            Validators.minLength(res["Data"]["PWDLEN"]),
          ]);
        } else if (res["Data"]["PWDCOMPLEXITY"] == 1) {
          this.passMessage = "Password must contain only numeric characters.";
          this.forcePasswordChangeForm.controls["newPass"].setValidators([
            // 1. Password Field is Required
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
            // 2. check whether the entered password has a number
            Validators.pattern("^[0-9]*$"),
            // 3. Has a minimum length of numeric
            Validators.minLength(res["Data"]["PWDLEN"]),
          ]);
        } else if (res["Data"]["PWDCOMPLEXITY"] == 2) {
          this.passMessage =
            "Password must contain atleast " +
            this.passAlphaChars +
            " alphabets " +
            this.passNumChars +
            " numeric characters.";
          this.forcePasswordChangeForm.controls["newPass"].setValidators([
            // 1. Password Field is Required
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
            // 2. check whether the entered password has alphanumeric
            Validators.pattern(
              "(?=(.*\\d){" +
                this.passNumChars +
                "})(?=(.*[A-Za-z]){" +
                this.passAlphaChars +
                "})[a-zA-Z0-9]+$"
            ),
            // 3. Has a minimum length of characters
            Validators.minLength(res["Data"]["PWDLEN"]),
          ]);
        } else if (res["Data"]["PWDCOMPLEXITY"] == 3) {
          this.passMessage =
            "Password must contain atleast " +
            this.passAlphaChars +
            " alphabets " +
            this.passNumChars +
            " numeric and " +
            this.passSpecChars +
            " special characters.";
          this.forcePasswordChangeForm.controls["newPass"].setValidators([
            // 1. Password Field is Required
            Validators.required,
            this.customValidatorsService.noWhitespaceValidator,
            // 2. check whether the entered password has a alphanumeric+special characters
            Validators.pattern(
              "(?=(.*\\d){" +
                this.passNumChars +
                "})(?=(.*[A-Za-z]){" +
                this.passAlphaChars +
                "})(?=(.*[!@#$%^&+*?]){" +
                this.passSpecChars +
                "})(?!.*[\\s])^.*"
            ),
            // 3. Has a minimum length of characters
            Validators.minLength(res["Data"]["PWDLEN"]),
          ]);
        }
      });
    this.buildForm();
  }

  toolTipMessage(complexity) {
    this.translate
      .get([
        "ALTPWDATLEAST",
        "LBLCHAR",
        "LBLPWDSPEC",
        "LBLPWDNUMCHAR",
        "LBLPWDCOMPLEXNONE",
        "ALTPWDMUST",
      ])
      .subscribe((translated) => {
        switch (complexity) {
          case 0:
            this.toolTipString = `${translated["LBLPWDCOMPLEXNONE"]}`;
            break;
          case 1:
            this.toolTipString = `${translated["ALTPWDMUST"]} ${translated["LBLPWDNUMCHAR"]}`;
            break;
          case 2:
            this.toolTipString = `${translated["ALTPWDATLEAST"]} ${this.passAlphaChars} ${translated["LBLCHAR"]} ${this.passNumChars} ${translated["LBLPWDNUMCHAR"]}`;
            break;
          case 3:
            this.toolTipString = `${translated["ALTPWDATLEAST"]} ${this.passAlphaChars} ${translated["LBLCHAR"]} ${this.passSpecChars} ${translated["LBLPWDSPEC"]} ${this.passNumChars} ${translated["LBLPWDNUMCHAR"]}`;
            break;
        }
      });
  }

  buildForm() {
    this.forcePasswordChangeForm = this.fb.group(
      {
        oldPass: new FormControl("", Validators.required),
        newPass: new FormControl("", [
          Validators.required,
          this.customValidatorsService.noWhitespaceValidator,
        ]),
        confirmPass: new FormControl("", Validators.required),
      },
      {
        validator: MustMatch("newPass", "confirmPass"),
      }
    );
  }
  get uform() {
    return this.forcePasswordChangeForm.controls;
  }
  passwordChange(data) {
    const specCharCount = (
      data.match(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g) || []
    ).length;
    this.minSpecialCharacter =
      specCharCount >= this.passSpecChars ? false : true;
    const alphaCount = (data.match(/[a-zA-Z]/g) || []).length;
    this.minAlphaCharacters = alphaCount >= this.passAlphaChars ? false : true;
    const numCount = (data.match(/[0-9]/g) || []).length;
    this.minNumericCharacters = numCount >= this.passNumChars ? false : true;
  }
  onSubmit() {
    if (this.forcePasswordChangeForm.invalid) {
      this.submitted = true;
      this.customValidatorsService.scrollToError();
    } else {
      this.submitted = false;
      const formData = this.forcePasswordChangeForm.value;
      if (formData["newPass"] == formData["oldPass"]) {
        this.translate.get("ALTPWDNOTEQUAL").subscribe((message) => {
          this.messageService.add({
            severity: "error",
            detail: message,
          });
        });
      } else {
        delete formData["confirmPass"];
        this.forcePasswordChangeService
          .forcePasswordChange(formData)
          .subscribe((res) => {
            if (res["status"]) {
              this.translate.get(res["message"]).subscribe((message) => {
                this.messageService.add({
                  severity: "success",
                  detail: message,
                });
              });
              setTimeout(() => {
                this.router.navigate(["/dashboard"], {
                  skipLocationChange: true,
                });
              }, 1000);
            } else {
              this.translate.get(res["message"]).subscribe((message) => {
                this.messageService.add({
                  severity: "error",
                  detail: message,
                });
              });
            }
          });
      }
    }
  }

  onReset() {
    this.forcePasswordChangeForm.reset();
  }
}
