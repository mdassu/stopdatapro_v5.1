import { Component, OnInit, DoCheck } from "@angular/core";
import { UserModel } from "./_models/userModel";
import { AuthenticationService } from "./_services/authentication.service";
import { AppMainComponent } from "./app.main.component";
import { EmailInboxService } from "./_services/email-inbox.service";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmationService } from "primeng/api";
import { Router } from "@angular/router";
import { EnvService } from "src/env.service";
@Component({
  selector: "app-topbar",
  templateUrl: "./app.topbar.component.html",
  styleUrls: ["./app.topbar.component.css"],
})
export class AppTopBarComponent implements OnInit, DoCheck {
  userInfo: UserModel;
  inboxCount: number;
  autoLogoutInterval: any;
  emailCountRefresh: any;
  confirmClass: any;
  appName: any;
  autoLogoutPopupValue: boolean;
  constructor(
    public app: AppMainComponent,
    private authenticationService: AuthenticationService,
    private emailInboxService: EmailInboxService,
    private confirmationService: ConfirmationService,
    private translate: TranslateService,
    private env: EnvService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.userInfo = this.authenticationService.UserInfo;
    this.inboxCount = this.userInfo["inboxCount"];
    this.emailCount();
    this.startLogoutIntervalTimer();
    this.appName = this.env.appName;
  }

  emailCount() {
    let acc = 0;
    this.emailInboxService.getEmailData({}).subscribe((res) => {
      if (res["allEmails"] && res["allEmails"].length > 0) {
        res["allEmails"].forEach((val) => {
          if (val["MAILREADSTATUS"] == "LBLUNREAD") {
            acc += 1;
          }
          this.inboxCount = acc;
        });
      } else {
        this.inboxCount = 0;
      }
    });
  }

  ngDoCheck() {
    clearTimeout(this.autoLogoutInterval);
    this.startLogoutIntervalTimer();
  }

  startLogoutIntervalTimer() {
    if (localStorage["AutoLogoutInterval"]) {
      var interval;
      if (this.router.url == "/analytics-dashboard") {
        interval = 7;
      } else {
        interval = localStorage["AutoLogoutInterval"];
      }
      this.autoLogoutInterval = setTimeout(() => {
        this.autoLogoutPopup();
      }, +interval * 60000);
    }
  }

  autoLogoutPopup() {
    clearTimeout(this.emailCountRefresh);
    this.autoLogoutPopupValue = true;
    this.authenticationService.logout();
    // localStorage.removeItem("AutoLogoutInterval");
  }

  autoLogout() {
    this.authenticationService.logout();
    this.autoLogoutPopupValue = false;
    this.router.navigate(["/login"], {
      skipLocationChange: true,
    });
  }

  logoutPopup(event, logout) {
    this.confirmClass = "warning-msg";
    this.translate.get("ALTLOGOUT").subscribe((res) => {
      this.confirmationService.confirm({
        message: res,
        key: "logout",
        accept: () => {
          clearTimeout(this.autoLogoutInterval);
          this.app.logout(event, logout);
        },
      });
    });
  }
}
