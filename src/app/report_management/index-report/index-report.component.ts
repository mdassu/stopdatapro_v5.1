import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { formatDate } from "@angular/common";
import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { AuthenticationService } from "src/app/_services/authentication.service";

@Component({
  selector: "app-index-report",
  templateUrl: "./index-report.component.html",
  styleUrls: ["./index-report.component.css"],
})
export class IndexReportComponent implements OnInit {
  @Input() reportId: any;
  cols: any[];
  tableData: any = [];
  indexData: any = [];
  title: any;
  selectedReportView: any;
  isLoaded: boolean;
  errormsg: any;
  rowGroupMetadata: any;
  parameters: any;
  allSiteSelected: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any = 0;
  tableRows: any;
  columns: any;
  unFTableRows: any;
  unFColumns: any;
  pdfData: any = [];
  dataKey: any = "SITENAME";
  colId: any;
  mainHead: any;
  finalData: any = [];
  subAreaOption: number;
  colleps: number = 0;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.translate.get("LBLINDEXRPT").subscribe((title) => {
      this.title = title;
    });

    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT", url: "./assets/help/index-report.md" },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);

    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.allSiteSelected = this.parameters["SITEID"];
      this.selectedReportView = this.parameters["designId"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }
    this.getReports();

    this.translate
      .get([
        "LBLAREANAME",
        "LBLSUBAREAFULLNAME",
        "LBLSHIFTNAME",
        "LBLMAINCATEGORY",
        "LBLSUBCATNAME",
        "LBLSAFE",
        "LBLUNSAFE",
      ])
      .subscribe((resLabel) => {
        this.subAreaOption = this.auth.UserInfo["subAreaOptionValue"];
        if (this.subAreaOption == 0) {
          this.colleps = 1;
          this.cols = [
            {
              field: "AREANAME",
              header: resLabel["LBLAREANAME"],
            },
            {
              field: "SHIFTNAME",
              header: resLabel["LBLSHIFTNAME"],
            },
            {
              field: "MAINCATEGORYNAME",
              header: resLabel["LBLMAINCATEGORY"],
            },
            {
              field: "SUBCATEGORYNAME",
              header: resLabel["LBLSUBCATNAME"],
            },
            {
              field: "SAFE",
              header: resLabel["LBLSAFE"],
            },
            {
              field: "UNSAFE",
              header: resLabel["LBLUNSAFE"],
            },
          ];
        } else {
          this.cols = [
            {
              field: "AREANAME",
              header: resLabel["LBLAREANAME"],
            },
            {
              field: "SUBAREANAME",
              header: resLabel["LBLSUBAREAFULLNAME"],
            },
            {
              field: "SHIFTNAME",
              header: resLabel["LBLSHIFTNAME"],
            },
            {
              field: "MAINCATEGORYNAME",
              header: resLabel["LBLMAINCATEGORY"],
            },
            {
              field: "SUBCATEGORYNAME",
              header: resLabel["LBLSUBCATNAME"],
            },
            {
              field: "SAFE",
              header: resLabel["LBLSAFE"],
            },
            {
              field: "UNSAFE",
              header: resLabel["LBLUNSAFE"],
            },
          ];
        }
      });

    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = res["Data"];
        this.updateRowGroupMetaData();

        this.isLoaded = true;
        this.manageExportData();
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.tableData) {
      this.mainHead = [];
      for (let i = 0; i < this.tableData.length; i++) {
        let tableData = this.tableData[i];
        let sitename = tableData.SITENAME;
        if (i == 0) {
          this.rowGroupMetadata[sitename] = { index: 0, size: 1 };
          this.mainHead.push({
            SITENAME: sitename,
            SAFECOUNT: tableData.SAFECOUNT,
            UNSAFECOUNT: tableData.UNSAFECOUNT,
          });
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.SITENAME;
          if (sitename === previousRowGroup) {
            this.rowGroupMetadata[sitename].size++;
          } else {
            this.rowGroupMetadata[sitename] = { index: i, size: 1 };
            this.mainHead.push({
              SITENAME: sitename,
              SAFECOUNT: tableData.SAFECOUNT,
              UNSAFECOUNT: tableData.UNSAFECOUNT,
            });
          }
        }
      }
    }
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    if (this.tableData.length > 0 && this.cols.length > 0) {
      // get data for Excel
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempDataJson = {};
        // var tempRows = [];
        this.translate
          .get(["LBLSITENAME", "LBLSAFECOUNT", "LBLUNSAFECOUNT"])
          .subscribe((resLabel) => {
            if (key == 0) {
              this.unFColumns.push(resLabel["LBLSITENAME"]);
              this.unFColumns.push(resLabel["LBLSAFECOUNT"]);
              this.unFColumns.push(resLabel["LBLUNSAFECOUNT"]);
            }
            tempDataJson[resLabel["LBLSITENAME"]] = data["SITENAME"];
            tempDataJson[resLabel["LBLSAFECOUNT"]] = data["SAFECOUNT"];
            tempDataJson[resLabel["LBLUNSAFECOUNT"]] = data["UNSAFECOUNT"];
          });
        this.cols.map((item, index) => {
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];

          // tempRows.push(data[item["field"]]);
          if (this.tableData.length - 1 == key) {
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
          }
        });
        dataJson["SITENAME"] = data["SITENAME"];
        dataJson["SAFECOUNT"] = data["SAFECOUNT"];
        dataJson["UNSAFECOUNT"] = data["UNSAFECOUNT"];
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
        // this.pdfData.push(tempRows);
      });

      // get data for PDF
      var mainColumns = [];
      this.translate
        .get(["LBLSITENAME", "LBLSAFEPERCENTAGE", "LBLUNSAFEPERCENTAGE"])
        .subscribe((resLabel) => {
          mainColumns.push(resLabel["LBLSITENAME"]);
          mainColumns.push(resLabel["LBLSAFEPERCENTAGE"]);
          mainColumns.push(resLabel["LBLUNSAFEPERCENTAGE"]);
        });
      var tempMainColums = Object.keys(this.mainHead[0]);
      var subColumns = this.columns;
      this.mainHead.map((head) => {
        var mainRows = [];
        var subRows = [];
        // main Table
        var tempMainRows = [];
        tempMainColums.map((col) => {
          tempMainRows.push(head[col]);
        });
        mainRows.push(tempMainRows);

        // sub Table
        this.tableData.map((data) => {
          var tempSubRows = [];
          if (data["SITENAME"] == head["SITENAME"]) {
            this.cols.map((subCol) => {
              tempSubRows.push(data[subCol.field]);
            });
            subRows.push(tempSubRows);
          }
        });
        this.pdfData.push({
          mainColumns: mainColumns,
          mainRows: mainRows,
          subColumns: subColumns,
          subRows: subRows,
        });
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    const fileName = "INDEXREPORTS";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      this.exportAsExcelFile(this.unFTableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;
      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      // Report scope sheet
      let firstWorksheet = workbook.addWorksheet("Report Scope");
      firstWorksheet.addRow([]);
      let firstHeaderRow = firstWorksheet.addRow([this.title]);

      firstWorksheet.getCell("A2").font = {
        name: "Arial Unicode MS",
        family: 4,
        size: 18,
        bold: true,
        color: { argb: "00000000" },
      };
      firstWorksheet.getCell("A2").alignment = {
        vertical: "middle",
        horizontal: "left",
        indent: 1,
      };
      firstWorksheet.getColumn("A").width = 100;
      firstWorksheet.getRow(2).height = 30;

      this.translate
        .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        .subscribe((resLabel) => {
          if (this.parameters["OBSDATE"]) {
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([resLabel["LBLOBSDATE"]]).eachCell((cell) => {
              cell.font = {
                name: "Arial Unicode MS",
                family: 4,
                size: 14,
                bold: true,
                color: { argb: "00000000" },
              };
              cell.alignment = {
                vertical: "middle",
                horizontal: "left",
                indent: 2,
              };
            });

            firstWorksheet.lastRow.height = 25;
            var bothDate = this.parameters["OBSDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            firstWorksheet
              .addRow([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ])
              .eachCell((cell) => {
                cell.font = {
                  name: "Arial Unicode MS",
                  family: 4,
                  size: 12,
                  color: { argb: "00000000" },
                };
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "left",
                  indent: 3,
                };
              });
          }

          firstWorksheet.lastRow.height = 25;
        });

      // Report View sheet
      let worksheet = workbook.addWorksheet("Sheet1");

      if (this.selectedExport == 4 || this.selectedExport == 5) {
        for (var i = 1; i <= 15; i++) {
          worksheet.getColumn(i).width = 25;
        }

        let headerRow = worksheet.addRow(unFColumns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };

          cell.alignment = {
            vertical: "middle",
            horizontal: "left",
          };
        });

        // Add Data and Conditional Formatting
        unFTableRows.map((element, index) => {
          let eachRow = [];
          unFColumns.map((headers, key) => {
            if (headers == "User Level" || headers == "Status") {
              if (element[headers]) {
                this.translate.get(element[headers]).subscribe((val) => {
                  eachRow.push(val);
                });
              } else {
                eachRow.push(element[headers]);
              }
            } else {
              eachRow.push(element[headers]);
            }
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            if (unFTableRows.length - 1 == index) {
              worksheet
                .addRow(eachRow)
                .eachCell({ includeEmpty: true }, (cell, number) => {
                  cell.border = {
                    bottom: { style: "medium", color: { argb: customColor } },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                  };
                });
            } else {
              worksheet.addRow(eachRow).eachCell((cell, number) => {
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "left",
                };
              });
            }
          }
        });
      } else {
        let subHeader = header;
        var beforeDataCount = 1;
        if (this.subAreaOption == 1) {
          this.colId = "G";
        } else {
          this.colId = "F";
        }
        this.mainHead.map((header, key) => {
          var rowsData = data.filter((item) => {
            return item[this.dataKey] == header[this.dataKey];
          });
          for (var i = 1; i <= 15; i++) {
            worksheet.getColumn(i).width = 25;
          }
          if (key == 0) {
            worksheet.mergeCells(
              "C" + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              "C" +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                (beforeDataCount + 1)
            );
            this.translate
              .get(["LBLSITENAME", "LBLSAFEPERCENTAGE", "LBLUNSAFEPERCENTAGE"])
              .subscribe((resLabel) => {
                worksheet.getCell("A" + beforeDataCount).value =
                  resLabel["LBLSITENAME"];
                worksheet.getCell("B" + beforeDataCount).value =
                  resLabel["LBLSAFEPERCENTAGE"];
                worksheet.getCell("C" + beforeDataCount).value =
                  resLabel["LBLUNSAFEPERCENTAGE"];
                worksheet.getCell("B" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
                worksheet.getCell("C" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              });
            worksheet.getCell("A" + (beforeDataCount + 1)).value =
              header["SITENAME"];
            worksheet.getCell("B" + (beforeDataCount + 1)).value =
              header["SAFECOUNT"];
            worksheet.getCell("C" + (beforeDataCount + 1)).value =
              header["UNSAFECOUNT"];
            worksheet.getCell("B" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("C" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("B" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("C" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("B" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("C" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 5;
          } else {
            worksheet.mergeCells(
              "C" + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              "C" +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                (beforeDataCount + 1)
            );
            this.translate
              .get(["LBLSITENAME", "LBLSAFEPERCENTAGE", "LBLUNSAFEPERCENTAGE"])
              .subscribe((resLabel) => {
                worksheet.getCell("A" + beforeDataCount).value =
                  resLabel["LBLSITENAME"];
                worksheet.getCell("B" + beforeDataCount).value =
                  resLabel["LBLSAFEPERCENTAGE"];
                worksheet.getCell("C" + beforeDataCount).value =
                  resLabel["LBLUNSAFEPERCENTAGE"];
                worksheet.getCell("B" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
                worksheet.getCell("C" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              });
            worksheet.getCell("A" + (beforeDataCount + 1)).value =
              header["SITENAME"];
            worksheet.getCell("B" + (beforeDataCount + 1)).value =
              header["SAFECOUNT"];
            worksheet.getCell("C" + (beforeDataCount + 1)).value =
              header["UNSAFECOUNT"];
            worksheet.getCell("B" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("C" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("B" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("C" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("B" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("C" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 4 + beforeDataCount;
          }

          let headerRow = worksheet.addRow(subHeader);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: "EEECE1" },
            };
            cell.font = {
              color: { argb: "000000" },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };

            if (this.subAreaOption == 1 && number > 5) {
              cell.alignment = {
                vertical: "middle",
                horizontal: "center",
              };
            } else if (this.subAreaOption == 0 && number > 4) {
              cell.alignment = {
                vertical: "middle",
                horizontal: "center",
              };
            }
          });

          // Add Data and Conditional Formatting
          rowsData.map((element, index) => {
            let eachRow = [];
            this.columns.map((headers, key) => {
              if (headers == "User Level" || headers == "Status") {
                if (element[headers]) {
                  this.translate.get(element[headers]).subscribe((val) => {
                    eachRow.push(val);
                  });
                } else {
                  eachRow.push(element[headers]);
                }
              } else {
                eachRow.push(element[headers]);
              }
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (rowsData.length - 1 == index) {
                worksheet
                  .addRow(eachRow)
                  .eachCell({ includeEmpty: true }, (cell, number) => {
                    cell.border = {
                      bottom: { style: "medium", color: { argb: customColor } },
                    };
                    if (this.subAreaOption == 1 && number > 5) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                      };
                    } else if (this.subAreaOption == 0 && number > 4) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                      };
                    }
                  });
              } else {
                worksheet.addRow(eachRow).eachCell((cell, number) => {
                  if (this.subAreaOption == 1 && number > 5) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  } else if (this.subAreaOption == 0 && number > 4) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                });
              }
            }
          });
        });
      }

      worksheet.addRow([]);
      //Generate Excel File with given name
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape", "mm", [1030, 800]);
      // report scope
      pdf.text(this.title, 15, 20);
      let finalX = 10;
      this.translate
        .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        .subscribe((resLabel) => {
          if (this.parameters["OBSDATE"]) {
            let obsDate = [];
            var bothDate = this.parameters["OBSDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            obsDate.push([
              fDate +
                " - " +
                tDate +
                " (" +
                this.auth.UserInfo["dateFormat"].toUpperCase() +
                ")",
            ]);
            pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
              startY: finalX + 13,
              headStyles: {
                fillColor: customColor,
                textColor: customFontColor,
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
            });
            finalX = pdf.previousAutoTable.finalX;
          }
          pdf.addPage();
        });

      // report view
      let finalY = 10;
      var fn = this;
      this.pdfData.map((data) => {
        pdf.autoTable(data.mainColumns, data.mainRows, {
          startY: finalY,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          didParseCell: (data) => {
            fn.headAlignCol(data, customColor, fn.subAreaOption);
          },
        });
        finalY = pdf.previousAutoTable.finalY;
        pdf.autoTable(data.subColumns, data.subRows, {
          startY: finalY,
          headStyles: {
            fillColor: "#D8D8D8",
            textColor: "#000000",
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          columnStyles: {
            0: { cellWidth: 20 },
            1: { cellWidth: 20 },
            2: { cellWidth: 20 },
            3: { cellWidth: 20 },
            4: { cellWidth: 20 },
            5: { cellWidth: 20 },
            6: { cellWidth: 20 },
          },
          didParseCell: (data) => {
            fn.rowsAlignCol(data, customColor, fn.subAreaOption);
          },
        });
        finalY = pdf.previousAutoTable.finalY + 10;
      });
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }

  headAlignCol = function (data, customColor, subAreaOption) {
    if (data.column.index > 0) {
      data.cell.styles.halign = "center";
    }
  };

  rowsAlignCol = function (data, customColor, subAreaOption) {
    if (data.column.index > 4 && subAreaOption == 1) {
      data.cell.styles.halign = "center";
    } else if (data.column.index > 3 && subAreaOption == 0) {
      data.cell.styles.halign = "center";
    }
  };
}
