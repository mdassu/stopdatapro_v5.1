import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { isArray } from "util";
import { TranslateService } from "@ngx-translate/core";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { AuthenticationService } from "src/app/_services/authentication.service";

@Component({
  selector: "app-sites-report",
  templateUrl: "./sites-report.component.html",
  styleUrls: ["./sites-report.component.css"],
})
export class SitesReportComponent implements OnInit {
  @Input() reportId: any;
  summaryCols: any = [];
  summaryTableCols: any = [];
  observersCols: any = [];
  usersCols: any = [];
  areasCols: any = [];
  shiftsCols: any = [];
  title: any;
  reportView: any = [];
  selectedReportView: any;
  tableData: any = [];
  summaryData: any = [];
  observersData: any = [];
  usersData: any = [];
  areasData: any = [];
  shiftsData: any = [];
  siteList: any = [];
  isLoaded: boolean;
  errormsg: any;
  width: any;
  height: any;
  type: any;
  dataFormat: any;
  chartData: any = [];
  chartDataSource: any = [];
  parameters: any;
  allSiteSelected: any;
  rowGroupObserverdata: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any = 0;
  tableRows: any;
  columns: any;
  unFColumns: any;
  unFTableRows: any;
  pdfData: any = [];
  dataKey: any = "SITENAME";
  cols: any;
  colId: any;
  mainHead: any;
  siteTableRows: any = [];
  sitePDFRows: any = [];
  siteColumns: any = [];
  chart: any;
  grandTotal: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.translate.get("LBLSITESRPT").subscribe((title) => {
      this.title = title;
    });
    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT", url: "./assets/help/sites-report.md" },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.allSiteSelected = this.parameters["SITEID"];
      this.selectedReportView = this.parameters["designId"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    if (this.selectedReportView == 5) {
      this.getReports();
    } else {
      this.changeLBLSITESRPT();
    }
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (isArray(res["designData"])) {
        this.reportView = [];
        var designData = res["designData"];
        designData.map((item) => {
          this.translate.get(item.RPTNAME).subscribe((label) => {
            this.reportView.push({
              label: label,
              value: item.DESIGNID,
            });
          });
        });
      }
      if (res["status"] == true) {
        this.summaryData = res["siteChart"];
        this.tableData = res["siteDetails"];
        var fn = this;
        var i = 1;
        this.chartData = [];
        this.translate
          .get(["LBLSITENAME", "LBLNOOFUSERS"])
          .subscribe((resLabel) => {
            if (this.summaryData.length > 0) {
              this.summaryData.forEach(function (summary) {
                var data = {
                  label: summary.SITENAME,
                  value: summary.USERCOUNT,
                };
                fn.chartData.push(data);
                if (fn.summaryData.length == i) {
                  const datasource = {
                    chart: {
                      // caption: "Top 5 High-Income Careers",
                      xaxisname: resLabel["LBLSITENAME"],
                      yaxisname: resLabel["LBLNOOFUSERS"],
                      showvalues: "1",
                      theme: "fusion",
                    },
                    data: fn.chartData,
                  };
                  fn.width = "100%";
                  fn.height = "40%";
                  fn.type = "bar3d";
                  fn.dataFormat = "json";
                  fn.chartDataSource = datasource;
                  fn.isLoaded = true;
                }
                i++;
              });
            }

            this.summaryCols = [
              { field: "SITENAME", header: resLabel["LBLSITENAME"] },
              { field: "USERCOUNT", header: resLabel["LBLNOOFUSERS"] },
            ];
          });

        this.translate
          .get([
            "LBLUSERNAME",
            "LBLNAME",
            "LBLEMAILID",
            "LBLUSERROLE",
            "LBLUSERLEVEL",
            "LBLSTATUS",
          ])
          .subscribe((res) => {
            this.summaryTableCols = [
              { field: "USERNAME", header: res["LBLUSERNAME"] },
              { field: "NAME", header: res["LBLNAME"] },
              { field: "EMAIL", header: res["LBLEMAILID"] },
              { field: "ROLENAME", header: res["LBLUSERROLE"] },
              { field: "USERLEVEL", header: res["LBLUSERLEVEL"] },
              { field: "STATUS", header: res["LBLSTATUS"] },
            ];
          });
        this.updateRowGroupObserverData();
        this.isLoaded = true;
        this.manageExportData();
        // this.siteList);
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  changeLBLSITESRPT() {
    this.isLoaded = false;
    this.tableData = [];
    this.errormsg = "";
    if (this.selectedReportView == 5) {
      this.parameters["designId"] = this.selectedReportView;
      this.getReports();
    } else if (this.selectedReportView == 24) {
      var parameters = {
        reportId: this.reportId,
        DESIGNID: this.selectedReportView,
      };
      this.reportsService.getReportData(parameters).subscribe((res) => {
        if (isArray(res["designData"])) {
          this.reportView = [];
          var designData = res["designData"];
          designData.map((item) => {
            this.translate.get(item.RPTNAME).subscribe((label) => {
              this.reportView.push({
                label: label,
                value: item.DESIGNID,
              });
            });
          });
        }
        if (res["status"] == true) {
          this.tableData = res["Data"];
          this.siteList = [];
          var fn = this;
          var i = 1;
          this.updateRowGroupObserverData();
          this.isLoaded = true;
          // this.siteList);
          this.translate
            .get([
              "LBLNAME",
              "LBLUSERLEVEL",
              "FMKJAN",
              "FMKFEB",
              "FMKMAR",
              "FMKAPR",
              "FMKMAY",
              "FMKJUN",
              "FMKJUL",
              "FMKAUG",
              "FMKSEP",
              "FMKOCT",
              "FMKNOV",
              "FMKDEC",
              "LBLSTATUS",
            ])
            .subscribe((res) => {
              this.observersCols = [
                { field: "FULLNAME", header: res["LBLNAME"] },
                { field: "ROLENAME", header: res["LBLUSERLEVEL"] },
                { field: "JAN", header: res["FMKJAN"] },
                { field: "FEB", header: res["FMKFEB"] },
                { field: "MAR", header: res["FMKMAR"] },
                { field: "APR", header: res["FMKAPR"] },
                { field: "MAY", header: res["FMKMAY"] },
                { field: "JUN", header: res["FMKJUN"] },
                { field: "JUL", header: res["FMKJUL"] },
                { field: "AUG", header: res["FMKAUG"] },
                { field: "SEP", header: res["FMKSEP"] },
                { field: "OCT", header: res["FMKOCT"] },
                { field: "NOV", header: res["FMKNOV"] },
                { field: "DEC", header: res["FMKDEC"] },
                { field: "STATUS", header: res["LBLSTATUS"] },
              ];
            });
          this.manageExportData();
        } else {
          this.isLoaded = true;
          this.errormsg = "LBLRPTNORECFND";
        }
      });
    } else if (this.selectedReportView == 25) {
      var parameters = {
        reportId: this.reportId,
        DESIGNID: this.selectedReportView,
      };

      this.reportsService.getReportData(parameters).subscribe((res) => {
        if (res["status"] == true) {
          this.tableData = res["Data"];
          this.siteList = [];
          var fn = this;
          var i = 1;
          this.updateRowGroupObserverData();
          this.isLoaded = true;

          // this.siteList);

          this.translate
            .get([
              "LBLNAME",
              "LBLEMAILID",
              "LBLJOBTITLE",
              "LBLUSERROLE",
              "LBLUSERLEVEL",
              "LBLSTATUS",
            ])
            .subscribe((resLabel) => {
              this.usersCols = [
                { field: "FULLNAME", header: resLabel["LBLNAME"] },
                { field: "EMAIL", header: resLabel["LBLEMAILID"] },
                { field: "JOBTITLE", header: resLabel["LBLJOBTITLE"] },
                { field: "ROLENAME", header: resLabel["LBLUSERROLE"] },
                { field: "USERLEVEL", header: resLabel["LBLUSERLEVEL"] },
                { field: "STATUS", header: resLabel["LBLSTATUS"] },
              ];
            });
          this.manageExportData();
        } else {
          this.isLoaded = true;
          this.errormsg = "LBLRPTNORECFND";
        }
      });
    } else if (this.selectedReportView == 26) {
      var parameters = {
        reportId: this.reportId,
        DESIGNID: this.selectedReportView,
      };

      this.reportsService.getReportData(parameters).subscribe((res) => {
        if (res["status"] == true) {
          this.tableData = res["Data"];
          this.siteList = [];
          var fn = this;
          var i = 1;
          this.updateRowGroupObserverData();
          this.isLoaded = true;

          // this.siteList);
          this.translate
            .get(["LBLAREANAME", "LBLSTATUS"])
            .subscribe((resLabel) => {
              this.areasCols = [
                { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                { field: "STATUS", header: resLabel["LBLSTATUS"] },
              ];
            });
          this.manageExportData();
        } else {
          this.isLoaded = true;
          this.errormsg = "LBLRPTNORECFND";
        }
      });
    } else if (this.selectedReportView == 27) {
      var parameters = {
        reportId: this.reportId,
        DESIGNID: this.selectedReportView,
      };

      this.reportsService.getReportData(parameters).subscribe((res) => {
        if (res["status"] == true) {
          this.tableData = res["Data"];
          this.siteList = [];
          var fn = this;
          var i = 1;
          this.updateRowGroupObserverData();
          this.isLoaded = true;
          // this.siteList);
          this.translate
            .get(["LBLSHIFTNAME", "LBLSTATUS"])
            .subscribe((resLabel) => {
              this.shiftsCols = [
                { field: "SHIFTNAME", header: resLabel["LBLSHIFTNAME"] },
                { field: "STATUS", header: resLabel["LBLSTATUS"] },
              ];
            });
          this.manageExportData();
        } else {
          this.isLoaded = true;
          this.errormsg = "LBLRPTNORECFND";
        }
      });
    }
  }

  onSort() {
    this.updateRowGroupObserverData();
  }

  updateRowGroupObserverData() {
    this.rowGroupObserverdata = {};
    this.mainHead = [];
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let rowData = this.tableData[i];
        let sitename = rowData.SITENAME;
        if (i == 0) {
          this.rowGroupObserverdata[sitename] = { index: 0, size: 1 };
          if (this.selectedReportView == 5) {
            this.mainHead.push({
              SITENAME: sitename,
              ADDRESS: rowData.ADDRESS,
              CITY: rowData.CITY,
              STATE: rowData.STATE,
              EMPCOUNT: rowData.EMPCOUNT,
            });
          } else {
            this.mainHead.push({
              SITENAME: sitename,
            });
          }
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.SITENAME;
          if (sitename === previousRowGroup) {
            this.rowGroupObserverdata[sitename].size++;
          } else {
            this.rowGroupObserverdata[sitename] = { index: i, size: 1 };
            if (this.selectedReportView == 5) {
              this.mainHead.push({
                SITENAME: sitename,
                ADDRESS: rowData.ADDRESS,
                CITY: rowData.CITY,
                STATE: rowData.STATE,
                EMPCOUNT: rowData.EMPCOUNT,
              });
            } else {
              this.mainHead.push({
                SITENAME: sitename,
              });
            }
          }
        }
      }
    }
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    this.siteTableRows = [];
    this.sitePDFRows = [];
    this.siteColumns = [];
    // if (this.selectedReportView == 22) {
    // columns.map(item => {
    if (this.selectedReportView == 5) {
      this.colId = "F";
      this.cols = this.summaryTableCols;
      if (this.summaryData.length > 0) {
        this.grandTotal = 0;
        this.summaryData.map((data, key) => {
          var siteDataJson = {};
          var siteTempRows = [];
          this.summaryCols.map((item, index) => {
            this.grandTotal = this.grandTotal + data["USERCOUNT"];
            siteDataJson[item["header"]] = data[item["field"]];
            siteTempRows.push(data[item["field"]]);
            if (this.summaryData.length - 1 == key) {
              this.siteColumns.push(item.header);
            }
          });
          this.siteTableRows.push(siteDataJson);
          this.sitePDFRows.push(siteTempRows);
        });
        this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
          var totalJson = [label, this.grandTotal];
          this.sitePDFRows.push(totalJson);
        });
      }
    } else if (this.selectedReportView == 24) {
      this.colId = "O";
      this.cols = this.observersCols;
    } else if (this.selectedReportView == 25) {
      this.colId = "F";
      this.cols = this.usersCols;
    } else if (this.selectedReportView == 26) {
      this.colId = "B";
      this.cols = this.areasCols;
    } else if (this.selectedReportView == 27) {
      this.colId = "B";
      this.cols = this.shiftsCols;
    }
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempDataJson = {};
        this.translate
          .get([
            "LBLSITENAME",
            "LBLCITY",
            "LBLSTATE",
            "LBLADDRESS",
            "LBLEMPLOYEES",
          ])
          .subscribe((resLabel) => {
            if (key == 0) {
              this.unFColumns.push(resLabel["LBLSITENAME"]);
              if (this.selectedReportView == 5) {
                this.unFColumns.push(resLabel["LBLADDRESS"]);
                this.unFColumns.push(resLabel["LBLCITY"]);
                this.unFColumns.push(resLabel["LBLSTATE"]);
                this.unFColumns.push(resLabel["LBLEMPLOYEES"]);
              }
            }
            tempDataJson[resLabel["LBLSITENAME"]] = data["SITENAME"];
            if (this.selectedReportView == 5) {
              tempDataJson[resLabel["LBLADDRESS"]] = data["ADDRESS"];
              tempDataJson[resLabel["LBLCITY"]] = data["CITY"];
              tempDataJson[resLabel["LBLSTATE"]] = data["STATE"];
              tempDataJson[resLabel["LBLEMPLOYEES"]] = data["EMPCOUNT"];
            }
          });
        this.cols.map((item, index) => {
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];
          this.translate
            .get(["LBLUSERLEVEL", "LBLSTATUS"])
            .subscribe((resLabel) => {
              if (
                item["header"] == resLabel["LBLUSERLEVEL"] ||
                item["header"] == resLabel["LBLSTATUS"]
              ) {
                if (data[item["field"]]) {
                  this.translate.get(data[item["field"]]).subscribe((label) => {
                    dataJson[item["header"]] = label;
                    tempDataJson[item["header"]] = label;
                  });
                }
              }
            });
          if (this.tableData.length - 1 == key) {
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
          }
        });
        dataJson["SITENAME"] = data["SITENAME"];
        dataJson["ADDRESS"] = data["ADDRESS"];
        dataJson["CITY"] = data["CITY"];
        dataJson["STATE"] = data["STATE"];
        dataJson["EMPCOUNT"] = data["EMPCOUNT"];
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
      });
      // get data for PDF
      var mainColumns = [];
      this.translate
        .get([
          "LBLSITENAME",
          "LBLADDRESS",
          "LBLCITY",
          "LBLSTATE",
          "LBLEMPLOYEES",
        ])
        .subscribe((resLabel) => {
          mainColumns.push(resLabel["LBLSITENAME"]);
          if (this.selectedReportView == 5) {
            mainColumns.push(resLabel["LBLADDRESS"]);
            mainColumns.push(resLabel["LBLCITY"]);
            mainColumns.push(resLabel["LBLSTATE"]);
            mainColumns.push(resLabel["LBLEMPLOYEES"]);
          }
        });
      var tempMainColums = Object.keys(this.mainHead[0]);
      var subColumns = this.columns;
      this.mainHead.map((head) => {
        var mainRows = [];
        var subRows = [];
        // main Table
        var tempMainRows = [];
        tempMainColums.map((col) => {
          tempMainRows.push(head[col]);
        });
        mainRows.push(tempMainRows);

        // sub Table
        this.tableData.map((data) => {
          var tempSubRows = [];
          if (data["SITENAME"] == head["SITENAME"]) {
            this.cols.map((subCol) => {
              if (
                subCol["header"] == "User Level" ||
                subCol["header"] == "Status"
              ) {
                if (data[subCol.field]) {
                  this.translate.get(data[subCol.field]).subscribe((val) => {
                    tempSubRows.push(val);
                  });
                } else {
                  tempSubRows.push(data[subCol.field]);
                }
              } else {
                tempSubRows.push(data[subCol.field]);
              }
            });
            subRows.push(tempSubRows);
          }
        });
        this.pdfData.push({
          mainColumns: mainColumns,
          mainRows: mainRows,
          subColumns: subColumns,
          subRows: subRows,
        });
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    var fileName = "SITEREPORTS";
    if (this.selectedExport == 5) {
      var fileName = "SITESUMMARY";
    } else if (this.selectedExport == 24) {
      var fileName = "SITEOBSERVERS";
    } else if (this.selectedExport == 25) {
      var fileName = "SITEUSERS";
    } else if (this.selectedExport == 26) {
      var fileName = "SITEAREAS";
    } else if (this.selectedExport == 27) {
      var fileName = "SITESHIFTS";
    }
    var encodedData;
    if (this.selectedReportView == 5) {
      this.chart.getSVGString((svg) => {
        this.reportsService.svgString2Image(
          svg,
          1030,
          524,
          "png",
          function (base64String) {
            encodedData = base64String;
          }
        );
      });
    }
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    setTimeout(() => {
      if (this.selectedExport == 6) {
        var siteObject = Object.keys(this.siteTableRows[0]);
        var object = Object.keys(this.unFTableRows[0]);
        var newArr = [];
        var dataJson = {};
        this.siteTableRows.map((site, siteKey) => {
          object.map((item, key) => {
            if (key <= 1) {
              siteObject.map((obj, index) => {
                if (key == index) {
                  dataJson[item] = site[obj];
                }
              });
            } else {
              dataJson[item] = "";
            }
            if (object.length - 1 == key) {
              newArr.push(dataJson);
            }
          });
        });
        newArr = this.siteTableRows.concat(this.unFTableRows);
        this.exportAsExcelFile(newArr, fileName);
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
      } else if (
        this.selectedExport == 1 ||
        this.selectedExport == 3 ||
        this.selectedExport == 4 ||
        this.selectedExport == 5
      ) {
        var EXCEL_EXTENSION;
        if (this.selectedExport == 1 || this.selectedExport == 5) {
          EXCEL_EXTENSION = ".xlsx";
        } else if (this.selectedExport == 3 || this.selectedExport == 4) {
          EXCEL_EXTENSION = ".xls";
        }
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        //Excel Title, Header, Data
        const header = this.columns;
        const data = this.tableRows;
        const siteHeader = this.siteColumns;
        const siteData = this.siteTableRows;

        const unFColumns = this.unFColumns;
        const unFTableRows = this.unFTableRows;

        const EXCEL_TYPE =
          "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

        //Create workbook and worksheet
        let workbook = new ExcelJS.Workbook();
        let worksheet = workbook.addWorksheet("Sheet1");
        var beforeDataCount;
        if (this.selectedReportView == 5) {
          worksheet.getColumn(1).width = 40;
          worksheet.getColumn(2).width = 50;
          if (this.selectedExport != 4 && this.selectedExport != 5) {
            worksheet.getRow(1).height = 250;
            worksheet.mergeCells("A1:D1");
            var imageId = workbook.addImage({
              base64: encodedData,
              extension: "png",
            });
            worksheet.addImage(imageId, {
              tl: { col: 0, row: 0 },
              ext: { width: 800, height: 300 },
            });
          }
          let subHeader = header;
          //Add Header Row
          let siteHeaderRow = worksheet.addRow(siteHeader);
          siteHeaderRow.height = 20;
          // Cell Style : Fill and Border
          siteHeaderRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            cell.font = {
              color: { argb: customFontColor },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
            if (number > 1) {
              cell.alignment = { vertical: "middle", horizontal: "center" };
            } else {
              cell.alignment = { vertical: "middle", horizontal: "left" };
            }
          });
          // Add Data and Conditional Formatting
          let sumOfData = 0;
          siteData.forEach((element, index) => {
            let eachRow = [];
            this.siteColumns.map((headers, key) => {
              if (key == 1) {
                sumOfData = sumOfData + element[headers];
              }
              eachRow.push(element[headers]);
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (siteData.length - 1 == index) {
                worksheet.addRow(eachRow).eachCell((cell, number) => {
                  if (number > 1) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                  cell.border = {
                    bottom: { style: "medium", color: { argb: customColor } },
                  };
                  cell.numFmt = this.auth.changeCellValueType(cell.value);
                });
              } else {
                worksheet.addRow(eachRow).eachCell((cell, number) => {
                  if (number > 1) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                  cell.numFmt = this.auth.changeCellValueType(cell.value);
                });
              }
            }
          });
          this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
            let finalRow = worksheet.addRow([label, this.grandTotal]);
            finalRow.getCell(2).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            finalRow.eachCell((cell) => {
              cell.font = {
                size: 12,
                bold: true,
              };
              cell.border = {
                top: { style: "thin", color: { argb: customColor } },
                // left: { style: "thin", color: {argb: customColor} },
                bottom: { style: "thin", color: { argb: customColor } },
                // right: { style: "thin", color: {argb: customColor} },
              };
              cell.numFmt = this.auth.changeCellValueType(cell.value);
            });
          });

          worksheet.addRow([]);
          // var mainHeader = Object.keys(this.rowGroupObserverdata);
          if (this.selectedExport == 4 || this.selectedExport == 5) {
            for (var i = 1; i <= 12; i++) {
              worksheet.getColumn(i).width = 30;
            }

            let headerRow = worksheet.addRow(unFColumns);
            // Cell Style : Fill and Border
            headerRow.eachCell((cell, number) => {
              cell.fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              cell.font = {
                color: { argb: customFontColor },
              };
              cell.border = {
                top: { style: "thin" },
                left: { style: "thin" },
                bottom: { style: "thin" },
                right: { style: "thin" },
              };
            });

            // Add Data and Conditional Formatting
            unFTableRows.map((element, index) => {
              let eachRow = [];
              unFColumns.map((headers, key) => {
                if (headers == "User Level" || headers == "Status") {
                  if (element[headers]) {
                    this.translate.get(element[headers]).subscribe((val) => {
                      eachRow.push(val);
                    });
                  } else {
                    eachRow.push(element[headers]);
                  }
                } else {
                  eachRow.push(element[headers]);
                }
              });
              if (element.isDeleted === "Y") {
                let deletedRow = worksheet.addRow(eachRow);
                deletedRow.eachCell((cell, number) => {
                  cell.font = {
                    name: "Calibri",
                    family: 4,
                    size: 11,
                    bold: false,
                    strike: true,
                  };
                });
              } else {
                if (unFTableRows.length - 1 == index) {
                  worksheet
                    .addRow(eachRow)
                    .eachCell({ includeEmpty: true }, (cell, number) => {
                      cell.border = {
                        bottom: {
                          style: "medium",
                          color: { argb: customColor },
                        },
                      };
                      cell.numFmt = this.auth.changeCellValueType(cell.value);
                    });
                } else {
                  worksheet.addRow(eachRow).eachCell((cell) => {
                    cell.numFmt = this.auth.changeCellValueType(cell.value);
                  });
                }
              }
            });
          } else {
            beforeDataCount = siteData.length + 5;
            this.mainHead.map((header, key) => {
              var rowsData = data.filter((item) => {
                return item[this.dataKey] == header[this.dataKey];
              });
              for (var i = 1; i <= 12; i++) {
                worksheet.getColumn(i).width = 30;
              }
              if (key == 0) {
                worksheet.mergeCells(
                  "E" + beforeDataCount + ":F" + beforeDataCount
                );
                worksheet.mergeCells(
                  "E" +
                    (beforeDataCount + 1) +
                    ":" +
                    this.colId +
                    +(beforeDataCount + 1)
                );

                this.translate
                  .get([
                    "LBLSITENAME",
                    "LBLADDRESS",
                    "LBLCITY",
                    "LBLSTATE",
                    "LBLEMPLOYEES",
                  ])
                  .subscribe((resLabel) => {
                    worksheet.getCell("A" + beforeDataCount).value =
                      resLabel["LBLSITENAME"];
                    worksheet.getCell("B" + beforeDataCount).value =
                      resLabel["LBLADDRESS"];
                    worksheet.getCell("C" + beforeDataCount).value =
                      resLabel["LBLCITY"];
                    worksheet.getCell("D" + beforeDataCount).value =
                      resLabel["LBLSTATE"];
                    worksheet.getCell("E" + beforeDataCount).value =
                      resLabel["LBLEMPLOYEES"];
                  });
                worksheet.getCell("A" + (beforeDataCount + 1)).value =
                  header["SITENAME"];
                worksheet.getCell("B" + (beforeDataCount + 1)).value =
                  header["ADDRESS"];
                worksheet.getCell("C" + (beforeDataCount + 1)).value =
                  header["CITY"];
                worksheet.getCell("D" + (beforeDataCount + 1)).value =
                  header["STATE"];
                worksheet.getCell("E" + (beforeDataCount + 1)).value =
                  header["EMPCOUNT"];
                worksheet.getCell("A" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("B" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("C" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("D" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("E" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("A" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                worksheet.getCell("B" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                worksheet.getCell("C" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                worksheet.getCell("D" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                worksheet.getCell("E" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                beforeDataCount = rowsData.length + beforeDataCount + 4;
              } else {
                worksheet.mergeCells(
                  "E" + beforeDataCount + ":F" + beforeDataCount
                );
                worksheet.mergeCells(
                  "E" +
                    (beforeDataCount + 1) +
                    ":" +
                    this.colId +
                    +(beforeDataCount + 1)
                );

                this.translate
                  .get([
                    "LBLSITENAME",
                    "LBLADDRESS",
                    "LBLCITY",
                    "LBLSTATE",
                    "LBLEMPLOYEES",
                  ])
                  .subscribe((resLabel) => {
                    worksheet.getCell("A" + beforeDataCount).value =
                      resLabel["LBLSITENAME"];
                    worksheet.getCell("B" + beforeDataCount).value =
                      resLabel["LBLADDRESS"];
                    worksheet.getCell("C" + beforeDataCount).value =
                      resLabel["LBLCITY"];
                    worksheet.getCell("D" + beforeDataCount).value =
                      resLabel["LBLSTATE"];
                    worksheet.getCell("E" + beforeDataCount).value =
                      resLabel["LBLEMPLOYEES"];
                  });
                worksheet.getCell("A" + (beforeDataCount + 1)).value =
                  header["SITENAME"];
                worksheet.getCell("B" + (beforeDataCount + 1)).value =
                  header["ADDRESS"];
                worksheet.getCell("C" + (beforeDataCount + 1)).value =
                  header["CITY"];
                worksheet.getCell("D" + (beforeDataCount + 1)).value =
                  header["STATE"];
                worksheet.getCell("E" + (beforeDataCount + 1)).value =
                  header["EMPCOUNT"];
                worksheet.getCell("A" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("B" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("C" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("D" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("E" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("A" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                worksheet.getCell("B" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                worksheet.getCell("C" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                worksheet.getCell("D" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                worksheet.getCell("E" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                beforeDataCount = rowsData.length + 4 + beforeDataCount;
              }
              let headerRow = worksheet.addRow(subHeader);
              // Cell Style : Fill and Border
              headerRow.eachCell((cell, number) => {
                cell.fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: "EEECE1" },
                };
                cell.font = {
                  color: { argb: "000000" },
                };
                cell.border = {
                  top: { style: "thin" },
                  left: { style: "thin" },
                  bottom: { style: "thin" },
                  right: { style: "thin" },
                };
              });

              // Add Data and Conditional Formatting
              rowsData.map((element, index) => {
                let eachRow = [];
                this.columns.map((headers, key) => {
                  if (headers == "User Level" || headers == "Status") {
                    if (element[headers]) {
                      this.translate.get(element[headers]).subscribe((val) => {
                        eachRow.push(val);
                      });
                    } else {
                      eachRow.push(element[headers]);
                    }
                  } else {
                    eachRow.push(element[headers]);
                  }
                });
                if (element.isDeleted === "Y") {
                  let deletedRow = worksheet.addRow(eachRow);
                  deletedRow.eachCell((cell, number) => {
                    cell.font = {
                      name: "Calibri",
                      family: 4,
                      size: 11,
                      bold: false,
                      strike: true,
                    };
                  });
                } else {
                  if (rowsData.length - 1 == index) {
                    worksheet
                      .addRow(eachRow)
                      .eachCell({ includeEmpty: true }, (cell, number) => {
                        cell.border = {
                          bottom: {
                            style: "medium",
                            color: { argb: customColor },
                          },
                        };
                      });
                  } else {
                    worksheet.addRow(eachRow);
                  }
                }
              });
            });
          }
        } else {
          if (this.selectedExport == 4 || this.selectedExport == 5) {
            for (var i = 1; i <= 15; i++) {
              worksheet.getColumn(i).width = 25;
            }

            let headerRow = worksheet.addRow(unFColumns);
            // Cell Style : Fill and Border
            headerRow.eachCell((cell, number) => {
              cell.fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              cell.font = {
                color: { argb: customFontColor },
              };
              cell.border = {
                top: { style: "thin" },
                left: { style: "thin" },
                bottom: { style: "thin" },
                right: { style: "thin" },
              };
              if (number > 2 && number < 14 && this.selectedReportView == 24) {
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "center",
                  wrapText: true,
                };
              }
            });

            // Add Data and Conditional Formatting
            unFTableRows.map((element, index) => {
              let eachRow = [];
              unFColumns.map((headers, key) => {
                if (headers == "User Level" || headers == "Status") {
                  if (element[headers]) {
                    this.translate.get(element[headers]).subscribe((val) => {
                      eachRow.push(val);
                    });
                  } else {
                    eachRow.push(element[headers]);
                  }
                } else {
                  eachRow.push(element[headers]);
                }
              });
              if (element.isDeleted === "Y") {
                let deletedRow = worksheet.addRow(eachRow);
                deletedRow.eachCell((cell, number) => {
                  cell.font = {
                    name: "Calibri",
                    family: 4,
                    size: 11,
                    bold: false,
                    strike: true,
                  };
                });
              } else {
                if (unFTableRows.length - 1 == index) {
                  worksheet.addRow(eachRow).eachCell((cell, number) => {
                    cell.border = {
                      bottom: {
                        style: "medium",
                        color: { argb: customColor },
                      },
                    };
                    if (
                      number > 2 &&
                      number < 15 &&
                      this.selectedReportView == 24
                    ) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                        wrapText: true,
                      };
                    } else {
                      cell.alignment = {
                        wrapText: true,
                      };
                    }
                    cell.numFmt = this.auth.changeCellValueType(cell.value);
                  });
                } else {
                  worksheet.addRow(eachRow).eachCell((cell, number) => {
                    if (
                      number > 2 &&
                      number < 15 &&
                      this.selectedReportView == 24
                    ) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                        wrapText: true,
                      };
                    } else {
                      cell.alignment = {
                        wrapText: true,
                      };
                    }
                    cell.numFmt = this.auth.changeCellValueType(cell.value);
                  });
                }
              }
            });
          } else {
            let subHeader = header;
            var mainHeader = Object.keys(this.rowGroupObserverdata);
            beforeDataCount = 1;
            mainHeader.map((header, key) => {
              var rowsData = data.filter((item) => {
                return item[this.dataKey] == header;
              });
              for (var i = 1; i <= 15; i++) {
                worksheet.getColumn(i).width = 25;
              }
              if (key == 0) {
                worksheet.mergeCells(
                  "A" + beforeDataCount + ":" + this.colId + +beforeDataCount
                );
                worksheet.mergeCells(
                  "A" +
                    (beforeDataCount + 1) +
                    ":" +
                    this.colId +
                    +(beforeDataCount + 1)
                );
                this.translate.get("LBLSITENAME").subscribe((label) => {
                  worksheet.getCell("A" + beforeDataCount).value = label;
                });
                this.translate.get(header).subscribe((label) => {
                  worksheet.getCell("A" + (beforeDataCount + 1)).value = label;
                });
                worksheet.getCell("A" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("A" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                beforeDataCount = rowsData.length + 5;
              } else {
                worksheet.mergeCells(
                  "A" + beforeDataCount + ":" + this.colId + +beforeDataCount
                );
                worksheet.mergeCells(
                  "A" +
                    (beforeDataCount + 1) +
                    ":" +
                    this.colId +
                    +(beforeDataCount + 1)
                );
                this.translate.get("LBLSITENAME").subscribe((label) => {
                  worksheet.getCell("A" + beforeDataCount).value = label;
                });
                worksheet.getCell("A" + (beforeDataCount + 1)).value = header;

                worksheet.getCell("A" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("A" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                beforeDataCount = rowsData.length + 4 + beforeDataCount;
              }

              let headerRow = worksheet.addRow(subHeader);
              // Cell Style : Fill and Border
              headerRow.eachCell((cell, number) => {
                cell.fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: "EEECE1" },
                };
                cell.font = {
                  color: { argb: "000000" },
                };
                cell.border = {
                  top: { style: "thin" },
                  left: { style: "thin" },
                  bottom: { style: "thin" },
                  right: { style: "thin" },
                };
                if (
                  number > 2 &&
                  number < 15 &&
                  this.selectedReportView == 24
                ) {
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "center",
                    wrapText: true,
                  };
                }
              });

              // Add Data and Conditional Formatting
              rowsData.map((element, index) => {
                let eachRow = [];
                this.columns.map((headers, key) => {
                  if (headers == "User Level" || headers == "Status") {
                    if (element[headers]) {
                      this.translate.get(element[headers]).subscribe((val) => {
                        eachRow.push(val);
                      });
                    } else {
                      eachRow.push(element[headers]);
                    }
                  } else {
                    eachRow.push(element[headers]);
                  }
                });
                if (element.isDeleted === "Y") {
                  let deletedRow = worksheet.addRow(eachRow);
                  deletedRow.eachCell((cell, number) => {
                    cell.font = {
                      name: "Calibri",
                      family: 4,
                      size: 11,
                      bold: false,
                      strike: true,
                    };
                  });
                } else {
                  if (rowsData.length - 1 == index) {
                    worksheet
                      .addRow(eachRow)
                      .eachCell({ includeEmpty: true }, (cell, number) => {
                        cell.border = {
                          bottom: {
                            style: "medium",
                            color: { argb: customColor },
                          },
                        };
                        if (
                          number > 2 &&
                          number < 15 &&
                          this.selectedReportView == 24
                        ) {
                          cell.alignment = {
                            vertical: "middle",
                            horizontal: "center",
                            wrapText: true,
                          };
                        } else {
                          cell.alignment = {
                            wrapText: true,
                          };
                        }
                      });
                  } else {
                    worksheet.addRow(eachRow).eachCell((cell, number) => {
                      if (
                        number > 2 &&
                        number < 15 &&
                        this.selectedReportView == 24
                      ) {
                        cell.alignment = {
                          vertical: "middle",
                          horizontal: "center",
                          wrapText: true,
                        };
                      } else {
                        cell.alignment = {
                          wrapText: true,
                        };
                      }
                    });
                  }
                }
              });
            });
          }
        }

        worksheet.addRow([]);
        //Generate Excel File with given name
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        workbook.xlsx.writeBuffer().then((data) => {
          let blob = new Blob([data], { type: EXCEL_TYPE });
          fs.saveAs(
            blob,
            fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
          );
        });
      } else if (this.selectedExport == 2) {
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        var pdf = new jsPDF("landscape", "mm", [1200, 800]);
        let finalY = 10;
        var fn = this;
        if (this.selectedReportView == 5) {
          if (encodedData) {
            pdf.addImage(encodedData, "PNG", 20, 0, 220, 120);
            finalY = 130;
          }

          pdf.autoTable(this.siteColumns, this.sitePDFRows, {
            startY: finalY,
            headStyles: {
              fillColor: customColor,
              textColor: customFontColor,
            },
            alternateRowStyles: {
              fillColor: "#FFFFFF",
            },
            willDrawCell: this.drawCell,
            didParseCell: (data) => {
              fn.alignCol(data, customColor, fn.sitePDFRows.length);
            },
          });

          finalY = pdf.previousAutoTable.finalY + 10;

          this.pdfData.map((data) => {
            pdf.autoTable(data.mainColumns, data.mainRows, {
              startY: finalY,
              headStyles: {
                fillColor: customColor,
                textColor: customFontColor,
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
              columnStyles: {
                0: { cellWidth: 30 },
                1: { cellWidth: 30 },
                2: { cellWidth: 30 },
                3: { cellWidth: 30 },
                4: { cellWidth: 30 },
              },
            });
            finalY = pdf.previousAutoTable.finalY;
            pdf.autoTable(data.subColumns, data.subRows, {
              startY: finalY,
              headStyles: {
                fillColor: "#D8D8D8",
                textColor: "#000000",
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
              columnStyles: {
                0: { cellWidth: 30 },
                1: { cellWidth: 30 },
                2: { cellWidth: 30 },
                3: { cellWidth: 30 },
                4: { cellWidth: 30 },
                5: { cellWidth: 30 },
              },
            });
            finalY = pdf.previousAutoTable.finalY + 10;
          });
        } else {
          this.pdfData.map((data) => {
            pdf.autoTable(data.mainColumns, data.mainRows, {
              startY: finalY,
              headStyles: {
                fillColor: customColor,
                textColor: customFontColor,
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
            });
            finalY = pdf.previousAutoTable.finalY;
            pdf.autoTable(data.subColumns, data.subRows, {
              startY: finalY,
              headStyles: {
                fillColor: "#D8D8D8",
                textColor: "#000000",
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
              columnStyles: {
                0: { cellWidth: 30 },
              },
            });
            finalY = pdf.previousAutoTable.finalY + 10;
          });
        }
        this.selectedExport = 0;
        pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
      }
    }, 1000);
  }

  initialized($event) {
    this.chart = $event.chart; // saving chart instance
  }

  alignCol = function (data, customColor, rowCount) {
    if (data.column.index == 1) {
      data.cell.styles.halign = "center";
    }
    var s = data.cell.styles;
    if (data.row.index == rowCount - 1) {
      s.lineColor = "#" + customColor;
      s.lineWidth = 0.5;
      s.borders = "t";
    }
  };

  drawCell = function (data) {
    var doc = data.doc;
    var rows = data.table.body;
    if (rows.length === 1) {
    } else if (data.row.index === rows.length - 1) {
      doc.setFontStyle("bold");
      doc.setFontSize("12");
    }
  };
}
