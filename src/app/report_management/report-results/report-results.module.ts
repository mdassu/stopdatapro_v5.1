import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ReportResultsComponent } from "./report-results.component";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "../../_guards/auth.guard";
import { ColorPickerModule } from "ngx-color-picker";

import { TranslateLoader, TranslateModule } from "@ngx-translate/core";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "assets/i18n/");
}

import { CardModule } from "primeng/card";
import { TableModule } from "primeng/table";
import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { DataViewModule } from "primeng/dataview";
import { DropdownModule } from "primeng/dropdown";
import { InputSwitchModule } from "primeng/inputswitch";
import { InputTextModule } from "primeng/inputtext";
import { InputTextareaModule } from "primeng/inputtextarea";
import { MessageModule } from "primeng/message";
import { MessagesModule } from "primeng/messages";
import { MultiSelectModule } from "primeng/multiselect";
import { OverlayPanelModule } from "primeng/overlaypanel";
import {
  AccordionModule,
  CalendarModule,
  DialogModule,
  FileUploadModule,
  GrowlModule,
  PanelModule,
  PickListModule,
  ScrollPanelModule,
  TabViewModule,
  TreeModule,
  TooltipModule,
  ProgressSpinnerModule,
} from "primeng/primeng";
import { RadioButtonModule } from "primeng/radiobutton";
import { ToastModule } from "primeng/toast";
import { TreeTableModule } from "primeng/treetable";
import { ChartModule } from "primeng/chart";
import { HttpClient } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { NgSelectModule } from "@ng-select/ng-select";
import { ChecklistCountComponent } from "../checklist-count/checklist-count.component";
import { ChecklistCountSummaryComponent } from "../checklist-count-summary/checklist-count-summary.component";
import { CorrectiveActionReportComponent } from "../corrective-action-report/corrective-action-report.component";
import { SiteObservationStatsComponent } from "../site-observation-stats/site-observation-stats.component";
import { SitesReportComponent } from "../sites-report/sites-report.component";
import { TargetVsActivityMonthlyComponent } from "../target-vs-activity-monthly/target-vs-activity-monthly.component";
import { TargetVsActivityWeeklyComponent } from "../target-vs-activity-weekly/target-vs-activity-weekly.component";
import { CorrectiveActionsCoverageReportComponent } from "../corrective-actions-coverage-report/corrective-actions-coverage-report.component";
import { UserObserverListComponent } from "../user-observer-list/user-observer-list.component";
import { CategoryCommentsComponent } from "../category-comments/category-comments.component";
import { ChecklistReportByCategoryComponent } from "../checklist-report-by-category/checklist-report-by-category.component";
import { ChecklistReportByMetricsComponent } from "../checklist-report-by-metrics/checklist-report-by-metrics.component";
import { IndexReportComponent } from "../index-report/index-report.component";
import { MonthlySafeUnsafeStatsComponent } from "../monthly-safe-unsafe-stats/monthly-safe-unsafe-stats.component";
import { ParetoChartComponent } from "../pareto-chart/pareto-chart.component";
import { RedFlagComponent } from "../red-flag/red-flag.component";
import { StatsByCategoryComponent } from "../stats-by-category/stats-by-category.component";
import { TrendLineReportComponent } from "../trend-line-report/trend-line-report.component";
import { SubChecklistCountComponent } from "../sub-reports/sub-checklist-count/sub-checklist-count.component";
import { SubChecklistCountSummaryComponent } from "../sub-reports/sub-checklist-count-summary/sub-checklist-count-summary.component";
import { SubCorrectiveActionReportComponent } from "../sub-reports/sub-corrective-action-report/sub-corrective-action-report.component";
import { SubCorrectiveActionsCoverageReportComponent } from "../sub-reports/sub-corrective-actions-coverage-report/sub-corrective-actions-coverage-report.component";
import { SubMonthlySafeUnsafeStatsComponent } from "../sub-reports/sub-monthly-safe-unsafe-stats/sub-monthly-safe-unsafe-stats.component";
import { SubParetoChartComponent } from "../sub-reports/sub-pareto-chart/sub-pareto-chart.component";
import { SubRedFlagComponent } from "../sub-reports/sub-red-flag/sub-red-flag.component";
import { SubChecklistReportByCategoryComponent } from "../sub-reports/sub-checklist-report-by-category/sub-checklist-report-by-category.component";
import { FusionChartsModule } from "angular-fusioncharts";

// Import FusionCharts library
import * as FusionCharts from "fusioncharts";

// Load FusionCharts Individual Charts
import * as Charts from "fusioncharts/fusioncharts.charts";

import * as FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import { TargetVsActivityPeriodComponent } from "../target-vs-activity-period/target-vs-activity-period.component";
FusionCharts.options["creditLabel"] = false;
FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme);

let routes: Routes = [
  {
    path: "",
    component: ReportResultsComponent,
    canActivate: [AuthGuard],
    data: { label: "LBLREPORTS" },
  },
];

@NgModule({
  declarations: [
    ReportResultsComponent,
    ChecklistCountComponent,
    ChecklistCountSummaryComponent,
    CorrectiveActionReportComponent,
    SiteObservationStatsComponent,
    SitesReportComponent,
    TargetVsActivityMonthlyComponent,
    TargetVsActivityPeriodComponent,
    TargetVsActivityWeeklyComponent,
    CorrectiveActionsCoverageReportComponent,
    UserObserverListComponent,
    CategoryCommentsComponent,
    ChecklistReportByCategoryComponent,
    ChecklistReportByMetricsComponent,
    IndexReportComponent,
    MonthlySafeUnsafeStatsComponent,
    ParetoChartComponent,
    RedFlagComponent,
    StatsByCategoryComponent,
    TrendLineReportComponent,
    SubChecklistCountComponent,
    SubChecklistCountSummaryComponent,
    SubChecklistReportByCategoryComponent,
    SubCorrectiveActionReportComponent,
    SubCorrectiveActionsCoverageReportComponent,
    SubMonthlySafeUnsafeStatsComponent,
    SubParetoChartComponent,
    SubRedFlagComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    TableModule,
    ButtonModule,
    CheckboxModule,
    ConfirmDialogModule,
    DataViewModule,
    DropdownModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    MessageModule,
    MessagesModule,
    MultiSelectModule,
    OverlayPanelModule,
    AccordionModule,
    CalendarModule,
    DialogModule,
    FileUploadModule,
    GrowlModule,
    PanelModule,
    PickListModule,
    ScrollPanelModule,
    TabViewModule,
    TreeModule,
    TooltipModule,
    ProgressSpinnerModule,
    RadioButtonModule,
    ToastModule,
    TreeTableModule,
    ChartModule,
    NgSelectModule,
    ColorPickerModule,
    FusionChartsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    RouterModule.forChild(routes),
  ],
})
export class ReportResultsModule {}
