import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ReportsService } from '../../_services/reports.service';


@Component({
    selector: 'app-report-results',
    templateUrl: './report-results.component.html',
    styleUrls: ['./report-results.component.css']
})
export class ReportResultsComponent implements OnInit {

    reportId: any;

    constructor(private activatedRoute: ActivatedRoute, private reportsService: ReportsService) { }

    ngOnInit() {
        this.reportId = this.activatedRoute.snapshot.paramMap.get('id');
    }

}
