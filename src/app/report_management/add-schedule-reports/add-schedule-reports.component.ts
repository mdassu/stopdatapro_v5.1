import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { ConfirmationService, MessageService } from "primeng/api";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";
import { ScheduleReportsService } from "src/app/_services/schedule-reports.service";
import { CustomValidatorsService } from "../../_services/custom-validators.service";
import { TranslateService } from "@ngx-translate/core";
import { ReportsService } from "src/app/_services/reports.service";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
declare var $: any;

import tinymce from "tinymce/tinymce";

// A theme is also required
import "tinymce/themes/modern/theme";

// Any plugins you want to use has to be imported
import "tinymce/plugins/paste";
import "tinymce/plugins/link";
import "tinymce/plugins/table";
import "tinymce/plugins/preview";
import { AuthenticationService } from "src/app/_services/authentication.service";

@Component({
  selector: "app-add-schedule-reports",
  templateUrl: "./add-schedule-reports.component.html",
  styleUrls: ["./add-schedule-reports.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class AddScheduleReportsComponent implements OnInit {
  // Varialbe for fields name
  addScheduleGeneral: FormGroup;
  submitted: boolean;

  exportFileType: any = [];
  exportFileTypeList: any = [];
  relativeDate: any = [];
  relativeDateList: any = [];
  chooseDate: any = [];
  chooseDateList: any = [];
  relativeOption: any = [];
  relativeOptions: any = [];
  relativeOptionList: any = [];
  relativeOptionsList: any = [];
  sendEmailOption: any = [];
  sendEmailOptionList: any = [];
  exportFormat: any = [];
  exportFormatList: any = [];
  reportName: any = [];
  reportNameList: any = [];
  reportView: boolean = false;
  reportSaved: boolean = false;
  reportDate: boolean = false;
  reportViewDataList: any = [];
  savedDataList: any = [];
  filterList: any;
  filterGroupList: any;
  toUser: any;
  toUserList: any;
  showAdvanceSearch: boolean = false;
  showSearch: boolean = false;
  filterOptional: any;
  filterChart: any;
  showSearchBox: boolean = false;
  showOpSearchBox: boolean = false;
  reportSavedId: any;
  filterSetting: any;
  reportValue: number = 0;
  emailValue: number = 2;
  theDay: any = [];
  theDayList: any = [];
  weekDay: any = [];
  weekDayList: any = [];
  everyDwm: any = [];
  timeZoneList: any = [];
  Status: any = [];
  StatusList: any = [];
  startTimeList: any = [];
  timeList: any = [];
  initialGeneralFormData: any;
  keywords: any;
  menuList = [];
  Keyword: any = "Keywords";
  dateFormatType: any;
  dateFormatTypeNew: any;
  dateFormat: any;
  onSubmitTouched: boolean = false;
  filterGroups: any;
  filterCategory: any;
  locale: any;
  previousValError: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private scheduleReports: ScheduleReportsService,
    private customValidatorsService: CustomValidatorsService,
    private reportService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private cookieService: CookieService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT", url: "./assets/help/scheduled-reports-add.md" },
      {
        label: "LBLSCHEDULEREPORTS",
        routerLink: ["/schedule-reports"],
      },
    ]);
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    this.dateFormatType = this.auth.UserInfo["dateFormat"].toLowerCase();
    // this.dateFormatType = this.auth.UserInfo["dateFormat"];
    if (this.auth.UserInfo["dateFormat"] == "MM/dd/yyyy") {
      this.dateFormatTypeNew = "LBLMDY";
    } else if (this.auth.UserInfo["dateFormat"] == "dd/MM/yyyy") {
      this.dateFormatTypeNew = "LBLDMY";
    } else if (this.auth.UserInfo["dateFormat"] == "yyyy/MM/dd") {
      this.dateFormatTypeNew = "LBLYMD";
    }
    this.dateFormat = this.dateFormatType.replace("yyyy", "yy");

    //Export File Type
    this.exportFileType.push({ label: "LBLPDF", value: 1 });
    this.exportFileType.push({ label: "LBLXLS", value: 2 });
    this.exportFileType.push({ label: "LBLXLSX", value: 3 });
    this.exportFileType.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.exportFileTypeList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Export Format
    this.exportFormat.push({ label: "LBLFORMATTED", value: 0 });
    this.exportFormat.push({ label: "LBLUNFORMATTED", value: 1 });
    this.exportFormat.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.exportFormatList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Coose Date
    this.chooseDate = [];
    this.chooseDateList = [];
    this.chooseDate.push({ label: "LBLSELECT", value: 0 });
    this.chooseDate.push({ label: "LBLCURRENT", value: 1 });
    this.chooseDate.push({ label: "LBLPREVIOUS", value: 2 });
    this.chooseDate.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.chooseDateList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Relative Option
    this.relativeOptions = [];
    this.relativeOptionsList = [];
    this.relativeOptions.push({ label: "LBLSELECT", value: 0 });
    this.relativeOptions.push({ label: "LBLWEEKS", value: 1 });
    this.relativeOptions.push({ label: "LBLMONTHS", value: 2 });
    this.relativeOptions.push({ label: "LBLYEARS", value: 3 });
    this.relativeOptions.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.relativeOptionsList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Relative Option
    this.relativeOption = [];
    this.relativeOptionList = [];
    this.relativeOption.push({ label: "LBLSELECT", value: 0 });
    this.relativeOption.push({ label: "LBLWEEK", value: 1 });
    this.relativeOption.push({ label: "LBLMONTH", value: 2 });
    this.relativeOption.push({ label: "LBLYEAR", value: 3 });
    this.relativeOption.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.relativeOptionList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Send Email Option
    this.sendEmailOption.push({ label: "LBLWEEKLY", value: 2 });
    this.sendEmailOption.push({ label: "LBLMONTHLY", value: 3 });
    this.sendEmailOption.push({ label: "LBLDAY", value: 1 });
    this.sendEmailOption.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.sendEmailOptionList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //The Day
    this.theDay = [];
    this.theDayList = [];
    this.theDay.push({ label: "LBLFIRST", value: 0 });
    this.theDay.push({ label: "LBLSEC", value: 1 });
    this.theDay.push({ label: "LBLTHIRD", value: 2 });
    this.theDay.push({ label: "LBLFOUR", value: 3 });
    this.theDay.push({ label: "LBLLAST", value: 4 });
    this.theDay.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.theDayList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Week Day
    this.weekDay = [];
    this.weekDayList = [];
    this.weekDay.push({ label: "LBLSUNDAY", value: 0 });
    this.weekDay.push({ label: "LBLMONDAY", value: 1 });
    this.weekDay.push({ label: "LBLTUEDAY", value: 2 });
    this.weekDay.push({ label: "LBLWEDDAY", value: 3 });
    this.weekDay.push({ label: "LBLTHUDAY", value: 4 });
    this.weekDay.push({ label: "LBLFRIDAY", value: 5 });
    this.weekDay.push({ label: "LBLSATDAY", value: 6 });
    this.weekDay.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.weekDayList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Every Month Value
    for (let i = 1; i < 29; i++) {
      this.everyDwm.push({ label: i, value: i });
    }

    //Status
    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((resLabel) => {
      this.StatusList.push({ label: resLabel["LBLACTIVE"], value: 1 });
      this.StatusList.push({ label: resLabel["LBLINACTIVE"], value: 0 });
      this.StatusList.map((item, key) => {
        this.translate.get(item.label).subscribe((text: string) => {
          this.Status.push({
            value: item.value,
            label: text,
          });
        });
      });
    });

    var quarterHours = ["00", "30"];
    var hour;
    for (var hours = 0; hours < 24; hours++) {
      for (var min = 0; min < 2; min++) {
        var minutes = +hours * 60 + +quarterHours[min];
        if (hours.toString().length == 1) {
          hour = "0" + hours;
        } else {
          hour = hours;
        }
        this.timeList.push({
          label: hour + ":" + quarterHours[min],
          value: minutes,
        });
      }
    }

    //Schedule Form
    this.addScheduleGeneral = this.fb.group({
      scheduleName: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),
      exportType: new FormControl(1),
      exportFormat: new FormControl(0),
      reportId: new FormControl(
        0,
        Validators.compose([Validators.required, Validators.min(1)])
      ),
      designId: new FormControl(""),
      savedFilter: new FormControl(0),
      relativeFor: new FormControl(0),
      relativeOption: new FormControl(0),
      relativeOptionType: new FormControl(0),
      previousValue: new FormControl(""),
      tillDate: new FormControl(""),
      emailTo: new FormControl("", Validators.required),
      schedulecc: new FormControl("", [
        // Validators.pattern(
        //   "^(\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*;)*(\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*)$"
        // ),
        this.customValidatorsService.ccEmailValid,
      ]),
      scheduleSubject: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),
      scheduleBody: new FormControl("", Validators.required),
      scheduleDay: new FormControl(null, Validators.required),
      dwm: new FormControl(2),
      everyDwm: new FormControl(1),
      monthlyDwm: new FormControl(null),
      startDate: new FormControl(null, Validators.required),
      endDate: new FormControl(null, Validators.required),
      timeZoneId: new FormControl(null),
      status: new FormControl(1),
      options: new FormControl(null),
      startTime: new FormControl(0),
    });
    this.getDataForAdd();
  }

  //Replace by index
  replaceAt(str, index, ch) {
    return str.replace(/./g, (c, i) => (i == index ? ch : c));
  }

  getDataForAdd() {
    this.scheduleReports.getDataForAdd({}).subscribe((res) => {
      if (res["status"] == true) {
        var reportName = res["report"];
        reportName.map((item, key) => {
          this.translate.get(item.REPORTNAME).subscribe((text: string) => {
            this.reportName.push({
              REPORTID: item.REPORTID,
              REPORTNAME: text,
            });
          });
        });
        this.toUserList = [];
        var userList = res["toUser"];
        userList.map((data) => {
          this.toUserList.push({
            label: data["EMAIL"],
            value: data["USERID"],
          });
        });
        var timeZoneList = res["timeZone"];
        timeZoneList.map((item, key) => {
          this.translate.get(item.TIMEZONENAME).subscribe((text: string) => {
            this.timeZoneList.push({
              TIMEZONEID: item.TIMEZONEID,
              TIMEZONENAME: text,
            });
          });
        });
        this.addScheduleGeneral.controls["timeZoneId"].setValue(
          timeZoneList[0]["TIMEZONEID"]
        );

        this.keywords = res["bodyKeyword"];
        let items = this.keywords.map((item) => {
          this.translate.get(item.label).subscribe((res) => {
            item["label"] = res;
          });

          return item;
        });
        const menuList = [];
        items.forEach((item) => {
          let tempItem = {};
          tempItem["text"] = item["label"];
          tempItem[
            "value"
          ] = `&nbsp;<span id=${item["keyWordId"]} class="mceNonEditable" contenteditable="false"><strong>[${item["label"]}]</strong></span>&nbsp;`;
          menuList.push(tempItem);
        });

        var that = this;
        tinymce.remove();
        // tinymce.init({
        //   selector: "#tinymceEditor",
        //   base_url: "/tinymce",
        //   height: "200",
        //   suffix: ".min",
        //   skin_url: "tinymce/skins/lightgray",
        //   plugins: "table",
        //   toolbar: "table fontselect fontsizeselect mybutton",
        //   setup: function(editor) {
        //     editor.addButton("mybutton", {
        //       type: "listbox",
        //       text: that.Keyword,
        //       icon: false,
        //       onselect: function(e) {
        //         editor.insertContent(this.value());
        //       },
        //       values: menuList
        //     });
        //     editor.on("change", function(e) {
        //       that.addScheduleGeneral.patchValue({
        //         scheduleBody: editor.getContent()
        //       });
        //       that.addScheduleGeneral.get("scheduleBody").markAsDirty();
        //     });
        //   }
        // });
        var languageTinymce = "";
        let currentUser = this.auth.UserInfo;
        if (currentUser.languageCode == "da-dk") {
          var languageTinymce = "da";
        } else if (currentUser.languageCode == "de-de") {
          var languageTinymce = "de";
        } else if (currentUser.languageCode == "el") {
          var languageTinymce = "el";
        } else if (currentUser.languageCode == "en-us") {
          var languageTinymce = "en";
        } else if (currentUser.languageCode == "es-es") {
          var languageTinymce = "es";
        } else if (currentUser.languageCode == "fr-fr") {
          var languageTinymce = "fr-FR";
        } else if (currentUser.languageCode == "hu") {
          var languageTinymce = "hu_HU";
        } else if (currentUser.languageCode == "it-it") {
          var languageTinymce = "it";
        } else if (currentUser.languageCode == "ja-jp") {
          var languageTinymce = "ja";
        } else if (currentUser.languageCode == "ko") {
          var languageTinymce = "ko_KR";
        } else if (currentUser.languageCode == "nl-nl") {
          var languageTinymce = "nl";
        } else if (currentUser.languageCode == "pl-pl") {
          var languageTinymce = "pl";
        } else if (currentUser.languageCode == "pt-pt") {
          var languageTinymce = "pt_PT";
        } else if (currentUser.languageCode == "ru-ru") {
          var languageTinymce = "ru";
        } else if (currentUser.languageCode == "th-th") {
          var languageTinymce = "th_TH";
        } else if (currentUser.languageCode == "zh-cn") {
          var languageTinymce = "zh_CN";
        }

        tinymce.init({
          selector: "#tinymceEditor",
          base_url: "/tinymce",
          language: languageTinymce,
          height: "200",
          suffix: ".min",
          skin_url: "tinymce/skins/lightgray",
          plugins: "table",
          menubar: false,
          toolbar:
            "bold italic underline alignleft aligncenter alignright alignjustify numlist outdent indent table fontselect fontsizeselect mybutton preview ",
          setup: function (editor) {
            editor.addButton("mybutton", {
              type: "listbox",
              text: that.Keyword,
              icon: false,
              onselect: function (e) {
                editor.insertContent(this.value());
              },
              values: menuList,
            });
            editor.on("change", function (e) {
              that.addScheduleGeneral.patchValue({
                scheduleBody: editor.getContent(),
              });
              that.addScheduleGeneral.get("scheduleBody").markAsDirty();
            });
          },
        });
        this.initialGeneralFormData = this.addScheduleGeneral.getRawValue();
      }
      this.onSubmitTouched = false;
    });
  }

  getFilters(reportId) {
    var parameters = {
      reportId: reportId,
    };
    var fn = this;
    this.reportSavedId = reportId;
    if (reportId == 0) {
      this.reportViewDataList = [];
      this.reportView = false;
      this.savedDataList = [];
      this.reportSaved = false;
      this.showSearchBox = false;
      this.showOpSearchBox = false;
    } else {
      this.scheduleReports.getReportData(parameters).subscribe((res) => {
        if (res["status"] == true) {
          this.reportViewDataList = [];
          this.savedDataList = [];
          this.relativeDate = [];
          this.relativeDateList = [];

          //Relative Date
          if (reportId == 14) {
            this.relativeDate.push({ label: "LBLSELECT", value: 0 });
            this.relativeDate.push({ label: "LBLOBSDATE", value: 1 });
            this.relativeDate.push({ label: "LBLCREATEDDATE", value: 2 });
          } else if (reportId == 15) {
            this.relativeDate.push({ label: "LBLSELECT", value: 0 });
            this.relativeDate.push({ label: "LBLOBSDATE", value: 1 });
            this.relativeDate.push({
              label: "LBLRESPONSEREQUIREDDATE",
              value: 3,
            });
            this.relativeDate.push({ label: "LBLRESPONDEDDATE", value: 4 });
          } else {
            this.relativeDate.push({ label: "LBLSELECT", value: 0 });
            this.relativeDate.push({ label: "LBLOBSDATE", value: 1 });
          }
          this.relativeDate.map((item, key) => {
            this.translate.get(item.label).subscribe((text: string) => {
              this.relativeDateList.push({
                value: item.value,
                label: text,
              });
            });
          });

          if (res["reportVal"]["REPORTDISPLAY"] == 1) {
            this.reportView = true;
            var reportViewDataList = res["reportDesign"];
            reportViewDataList.map((item, key) => {
              this.translate.get(item.RPTNAME).subscribe((text: string) => {
                this.reportViewDataList.push({
                  DESIGNID: item.DESIGNID,
                  RPTNAME: text,
                });
              });
            });
            this.addScheduleGeneral.controls["designId"].setValue(
              reportViewDataList[0]["DESIGNID"]
            );
          } else {
            if (this.reportViewDataList != "") {
              var reportViewDataList = res["reportDesign"];
              reportViewDataList.map((item, key) => {
                this.translate.get(item.RPTNAME).subscribe((text: string) => {
                  this.reportViewDataList.push({
                    DESIGNID: item.DESIGNID,
                    RPTNAME: text,
                  });
                });
              });
              this.addScheduleGeneral.controls["designId"].setValue(
                reportViewDataList[0]["DESIGNID"]
              );
            }
            // this.reportViewDataList = [];
            this.reportView = false;
          }
          if (res["reportVal"]["FILTEREXISTS"] == 1) {
            this.reportSaved = true;
            var savedDataList = res["reportSaveFilter"];
            savedDataList.map((item, key) => {
              this.translate.get(item.FILTERNAME).subscribe((text: string) => {
                this.savedDataList.push({
                  FILTERID: item.FILTERID,
                  FILTERNAME: text,
                });
              });
            });
          } else {
            this.savedDataList = [];
            this.reportSaved = false;
          }

          if (res["reportVal"]["RELATIVEDATE"] == 1) {
            this.reportDate = true;
          } else {
            this.reportDate = false;
          }

          this.filterList = res["filter"];
          this.filterList.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                var lableData = data[keies[1]];
                tempData.push({ label: lableData, value: data[keies[0]] });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue("");
          });

          if (this.filterList != "") {
            this.showSearchBox = true;
          } else {
            this.showSearchBox = false;
          }

          this.filterGroups = res["filterGroup"];
          this.filterGroups.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                var lableData = data[keies[1]];
                tempData.push({ label: lableData, value: data[keies[0]] });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue("");
          });

          this.filterCategory = res["filterCategories"];
          this.filterCategory.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                var lableData = data[keies[1]];
                tempData.push({ label: lableData, value: data[keies[0]] });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue("");
          });

          this.filterGroupList = res["filterGroupBy"];
          this.filterGroupList.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                this.translate.get(data[keies[1]]).subscribe((label) => {
                  tempData.push({ label: label, value: data[keies[0]] });
                });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl(null, Validators.required)
            );
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
          });

          //Optional Filter
          this.filterOptional = res["optionalFilter"];
          this.filterOptional.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                this.translate.get(data[keies[1]]).subscribe((label) => {
                  tempData.push({ label: label, value: data[keies[0]] });
                });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue("");
          });

          //Optional Filter Chart
          this.filterChart = res["filterChartType"];
          this.filterChart.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                this.translate.get(data[keies[1]]).subscribe((label) => {
                  tempData.push({ label: label, value: data[keies[0]] });
                });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl(null, Validators.required)
            );
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
          });

          if (this.filterOptional != "") {
            this.showOpSearchBox = true;
          } else {
            this.showOpSearchBox = false;
          }

          if (res["reportVal"]["RELATIVEDATE"] == 1) {
            this.addScheduleGeneral.controls["relativeFor"].setValidators([
              Validators.compose([Validators.required, Validators.min(1)]),
            ]);

            this.addScheduleGeneral.controls["relativeOption"].setValidators([
              Validators.compose([Validators.required, Validators.min(1)]),
            ]);
          } else {
            this.addScheduleGeneral.get("relativeFor").clearValidators();
            this.addScheduleGeneral.get("relativeFor").updateValueAndValidity();
            this.addScheduleGeneral.get("relativeOption").clearValidators();
            this.addScheduleGeneral
              .get("relativeOption")
              .updateValueAndValidity();
          }
          this.addScheduleGeneral.controls["relativeFor"].setValue(0);
          this.addScheduleGeneral.controls["relativeOption"].setValue(0);
          this.addScheduleGeneral.controls["relativeOptionType"].setValue(0);
          this.addScheduleGeneral.controls["previousValue"].setValue(null);
          this.addScheduleGeneral.controls["tillDate"].setValue(0);
          this.showRelativeOption(0);
        } else {
        }
        this.showSearch = true;
      });
    }

    if (reportId == 19) {
      this.relativeOptionsList.pop();
      this.relativeOptionList.pop();
    } else {
      if (this.relativeOptionsList.length < 4) {
        this.translate.get("LBLYEARS").subscribe((text: string) => {
          this.relativeOptionsList.push({
            value: 3,
            label: text,
          });
        });
      }
      if (this.relativeOptionList.length < 4) {
        this.translate.get("LBLYEAR").subscribe((text: string) => {
          this.relativeOptionList.push({
            value: 3,
            label: text,
          });
        });
      }
    }
  }

  getSavedFilters(filterId) {
    var parameters = {
      filterId: filterId,
      reportId: this.reportSavedId,
    };
    var fn = this;
    this.scheduleReports.getSavedFilterData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.filterSetting = res["filterSetting"];
        this.filterList = res["filter"];
        this.filterList.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              tempData.push({ label: data[keies[1]], value: data[keies[0]] });
            });
            item.Data = tempData;
          }
          if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
          }
        });

        this.filterGroups = res["filterGroup"];
        this.filterGroups.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              tempData.push({ label: data[keies[1]], value: data[keies[0]] });
            });
            item.Data = tempData;
          }
          if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
          }
        });

        this.filterCategory = res["filterCategories"];
        this.filterCategory.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              tempData.push({ label: data[keies[1]], value: data[keies[0]] });
            });
            item.Data = tempData;
          }
          if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
          }
        });

        this.filterGroupList = res["filterGroupBy"];
        this.filterGroupList.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              tempData.push({ label: data[keies[1]], value: data[keies[0]] });
            });
            item.Data = tempData;
          }
          fn.addScheduleGeneral.addControl(
            item.REPORTPARAM,
            new FormControl(null, Validators.required)
          );
          this.addScheduleGeneral.controls[item.REPORTPARAM].setValue("");
        });

        //Optional Filter
        this.filterOptional = res["optionalFilter"];
        this.filterOptional.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              this.translate.get(data[keies[1]]).subscribe((label) => {
                tempData.push({ label: label, value: data[keies[0]] });
              });
            });
            item.Data = tempData;
          }
          if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
          }
        });

        //Optional Filter Chart
        this.filterChart = res["filterChartType"];
        this.filterChart.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              this.translate.get(data[keies[1]]).subscribe((label) => {
                tempData.push({ label: label, value: data[keies[0]] });
              });
            });
            item.Data = tempData;
          }
          if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
              parseInt(this.filterSetting[item.REPORTPARAM])
            );
          }
        });
      }
    });
  }

  getOtherData(reportParam) {
    if (reportParam == "SITEID") {
      var siteParam = {
        siteId: this.addScheduleGeneral.value[reportParam].join(),
      };
      this.reportService.getDataBySite(siteParam).subscribe((res) => {
        if (res["status"] == true) {
          this.filterList.map((item) => {
            if (item["REPORTPARAM"] == "AREAID") {
              var tempData = [];
              if (res["area"].length > 0) {
                res["area"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
              this.addScheduleGeneral.controls[item["REPORTPARAM"]].setValue(
                ""
              );
            } else if (item["REPORTPARAM"] == "SHIFTID") {
              var tempData = [];
              if (res["shift"].length > 0) {
                res["shift"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
              this.addScheduleGeneral.controls[item["REPORTPARAM"]].setValue(
                ""
              );
            } else if (item["REPORTPARAM"] == "OBSERVERID") {
              var tempData = [];
              if (res["observer"].length > 0) {
                res["observer"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
              this.addScheduleGeneral.controls[item["REPORTPARAM"]].setValue(
                ""
              );
            } else if (item["REPORTPARAM"] == "CUSTOMFIELD1") {
              var tempData = [];
              if (res["customField1"].length > 0) {
                res["customField1"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
              this.addScheduleGeneral.controls[item["REPORTPARAM"]].setValue(
                ""
              );
            } else if (item["REPORTPARAM"] == "CUSTOMFIELD2") {
              var tempData = [];
              if (res["customField2"].length > 0) {
                res["customField2"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
              this.addScheduleGeneral.controls[item["REPORTPARAM"]].setValue(
                ""
              );
            } else if (item["REPORTPARAM"] == "CUSTOMFIELD3") {
              var tempData = [];
              if (res["customField3"].length > 0) {
                res["customField3"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
              this.addScheduleGeneral.controls[item["REPORTPARAM"]].setValue(
                ""
              );
            } else if (item["REPORTPARAM"] == "SETUPID") {
              var tempData = [];
              if (res["checkList"].length > 0) {
                res["checkList"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
              this.addScheduleGeneral.controls[item["REPORTPARAM"]].setValue(
                ""
              );
            }
          });
        }
      });
    } else if (reportParam == "AREAID") {
      var areaParam = {
        areaId: this.addScheduleGeneral.value[reportParam].join(),
      };
      this.reportService.getDataByArea(areaParam).subscribe((res) => {
        if (res["status"] == true) {
          this.filterList.map((item) => {
            if (item["REPORTPARAM"] == "SUBAREAID") {
              var tempData = [];
              if (res["subArea"].length > 0) {
                res["subArea"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
            }
          });
        }
      });
    } else if (reportParam == "SETUPID") {
      var checkParam = {
        chkListSetupId: this.addScheduleGeneral.value[reportParam].join(),
      };
      this.scheduleReports
        .getCheckListFilterMainCatg(checkParam)
        .subscribe((res) => {
          if (res["status"] == true) {
            this.filterList.map((item) => {
              if (item["REPORTPARAM"] == "MAINCATEGORYID") {
                var tempData = [];
                if (res["mainCat"].length > 0) {
                  res["mainCat"].map((data) => {
                    var keies = Object.keys(data);
                    tempData.push({
                      label: data[keies[1]],
                      value: data[keies[0]],
                    });
                  });
                }
                item.Data = tempData;
              }
            });
          }
        });
    } else if (
      reportParam == "CHKSUMGROUP1" ||
      reportParam == "CHKSUMGROUP2" ||
      reportParam == "CHKSUMGROUP3"
    ) {
      var groupParam = {
        group1: this.addScheduleGeneral.value["CHKSUMGROUP1"],
        group2: this.addScheduleGeneral.value["CHKSUMGROUP2"],
        group3: this.addScheduleGeneral.value["CHKSUMGROUP3"],
      };
      this.scheduleReports.getGroupByFilter(groupParam).subscribe((res) => {
        if (res["status"] == true) {
          this.filterGroupList.map((item) => {
            if (item["REPORTPARAM"] == "CHKSUMGROUP1") {
              var tempData = [];
              if (res["group1"].length > 0) {
                res["group1"].map((data) => {
                  var keies = Object.keys(data);
                  this.translate.get(data[keies[1]]).subscribe((label) => {
                    tempData.push({ label: label, value: data[keies[0]] });
                  });
                });
              }
              item.Data = tempData;
            }

            if (item["REPORTPARAM"] == "CHKSUMGROUP2") {
              var tempData = [];
              if (res["group2"].length > 0) {
                res["group2"].map((data) => {
                  var keies = Object.keys(data);
                  this.translate.get(data[keies[1]]).subscribe((label) => {
                    tempData.push({ label: label, value: data[keies[0]] });
                  });
                });
              }
              item.Data = tempData;
            }
            if (item["REPORTPARAM"] == "CHKSUMGROUP3") {
              var tempData = [];
              if (res["group3"].length > 0) {
                res["group3"].map((data) => {
                  var keies = Object.keys(data);
                  this.translate.get(data[keies[1]]).subscribe((label) => {
                    tempData.push({ label: label, value: data[keies[0]] });
                  });
                });
              }
              item.Data = tempData;
            }
          });
        }
      });
      this.filterGroupList.map((item) => {
        this.addScheduleGeneral.get(item.REPORTPARAM).clearValidators();
        this.addScheduleGeneral.get(item.REPORTPARAM).updateValueAndValidity();
      });
    }
  }

  showRelativeOption(rlValue) {
    this.addScheduleGeneral.controls["relativeOptionType"].setValue(0);
    this.reportValue = rlValue;
    if (rlValue == 1) {
      this.addScheduleGeneral.controls["relativeOptionType"].setValidators([
        Validators.compose([Validators.required, Validators.min(1)]),
      ]);
    } else if (rlValue == 2) {
      this.addScheduleGeneral.controls["relativeOptionType"].setValidators([
        Validators.compose([Validators.required, Validators.min(1)]),
      ]);
      this.addScheduleGeneral.controls["previousValue"].setValidators([
        Validators.compose([
          Validators.maxLength(2),
          Validators.pattern("[0-9]*"),
        ]),
      ]);
    }
  }

  relativeOptionTypeChange() {
    this.translate
      .get([
        "ALTPREVIOUSWEEK",
        "ALTPREVIOUSMONTH",
        "ALTPREVIOUSYEAR",
        "ALTPREVIOUSMONTHTVA",
        "ALTOBSERVATIONDATEMONTHPERIOD",
        "ALTOBSERVATIONDATEMONTHPERIODWEEK",
      ])
      .subscribe((resLabel) => {
        if (
          this.addScheduleGeneral.value["reportId"] == 6 ||
          this.addScheduleGeneral.value["reportId"] == 23
        ) {
          if (this.addScheduleGeneral.value["relativeOptionType"] == 1) {
            this.addScheduleGeneral.controls["previousValue"].setValidators([
              Validators.compose([
                Validators.maxLength(2),
                Validators.max(52),
                Validators.pattern("[0-9]*"),
              ]),
            ]);
            this.previousValError = resLabel["ALTPREVIOUSWEEK"];
            this.addScheduleGeneral.controls[
              "previousValue"
            ].updateValueAndValidity();
          } else if (this.addScheduleGeneral.value["relativeOptionType"] == 2) {
            this.addScheduleGeneral.controls["previousValue"].setValidators([
              Validators.compose([
                Validators.maxLength(2),
                Validators.max(12),
                Validators.pattern("[0-9]*"),
              ]),
            ]);
            this.previousValError = resLabel["ALTPREVIOUSMONTH"];
            this.addScheduleGeneral.controls[
              "previousValue"
            ].updateValueAndValidity();
          } else if (this.addScheduleGeneral.value["relativeOptionType"] == 3) {
            this.addScheduleGeneral.controls["previousValue"].setValidators([
              Validators.compose([
                Validators.maxLength(2),
                Validators.max(1),
                Validators.pattern("[0-9]*"),
              ]),
            ]);
            this.previousValError = resLabel["ALTPREVIOUSYEAR"];
            this.addScheduleGeneral.controls[
              "previousValue"
            ].updateValueAndValidity();
          }
        } else if (this.addScheduleGeneral.value["reportId"] == 19) {
          if (this.addScheduleGeneral.value["relativeOptionType"] == 1) {
            this.addScheduleGeneral.controls["previousValue"].setValidators([
              Validators.compose([
                Validators.maxLength(2),
                Validators.max(12),
                Validators.pattern("[0-9]*"),
              ]),
            ]);
            this.previousValError =
              resLabel["ALTOBSERVATIONDATEMONTHPERIODWEEK"];
            this.addScheduleGeneral.controls[
              "previousValue"
            ].updateValueAndValidity();
          } else if (this.addScheduleGeneral.value["relativeOptionType"] == 2) {
            this.addScheduleGeneral.controls["previousValue"].setValidators([
              Validators.compose([
                Validators.maxLength(2),
                Validators.max(3),
                Validators.pattern("[0-9]*"),
              ]),
            ]);
            this.previousValError = resLabel["ALTPREVIOUSMONTHTVA"];
            this.addScheduleGeneral.controls[
              "previousValue"
            ].updateValueAndValidity();
          }
        }
      });
  }

  sendEmailType(emailVal) {
    this.emailValue = emailVal;
    if (emailVal == 1) {
      this.addScheduleGeneral.controls["everyDwm"].setValidators([
        Validators.required,
      ]);
      this.addScheduleGeneral.get("monthlyDwm").clearValidators();
      this.addScheduleGeneral.get("scheduleDay").clearValidators();
    } else if (emailVal == 2) {
      this.addScheduleGeneral.controls["scheduleDay"].setValidators([
        Validators.required,
      ]);
      this.addScheduleGeneral.get("monthlyDwm").clearValidators();
      this.addScheduleGeneral.get("everyDwm").clearValidators();
    } else if (emailVal == 3) {
      this.addScheduleGeneral.controls["monthlyDwm"].setValidators([
        Validators.required,
      ]);
      this.addScheduleGeneral.controls["scheduleDay"].setValidators([
        Validators.required,
      ]);
      this.addScheduleGeneral.get("everyDwm").clearValidators();
    }
    this.addScheduleGeneral.get("everyDwm").updateValueAndValidity();
    this.addScheduleGeneral.get("monthlyDwm").updateValueAndValidity();
    this.addScheduleGeneral.get("scheduleDay").updateValueAndValidity();
  }

  // Advance serach
  advance_search() {
    this.showAdvanceSearch = !this.showAdvanceSearch;
  }

  get GeneralSubForm() {
    return this.addScheduleGeneral.controls;
  }

  //Clear Filter
  clearFilter() {
    if (this.filterList != undefined) {
      this.filterList.map((item) => {
        this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
      });
    }
    if (this.filterGroupList != undefined) {
      this.filterGroupList.map((item) => {
        this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
      });
    }
    if (this.filterOptional != undefined) {
      this.filterOptional.map((item) => {
        this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
      });
    }
    if (this.filterChart != undefined) {
      this.filterChart.map((item) => {
        this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
      });
    }
  }

  //Form Submit
  onSubmit(value: string) {
    this.submitted = true;
    if (this.addScheduleGeneral.invalid) {
      this.customValidatorsService.scrollToError();
      // const invalid = [];
      // const controls = this.addScheduleGeneral.controls;
      // for (const name in controls) {
      //   if (controls[name].invalid) {
      //     invalid.push(name);
      //   }
      // }
    } else {
      this.onSubmitTouched = true;
      let startDate = new Date(this.addScheduleGeneral.value["startDate"]);
      this.addScheduleGeneral.value["startDate"] = `${
        startDate.getMonth() + 1
      }/${startDate.getDate()}/${startDate.getFullYear()}`;

      let endDate = new Date(this.addScheduleGeneral.value["endDate"]);
      this.addScheduleGeneral.value["endDate"] = `${
        endDate.getMonth() + 1
      }/${endDate.getDate()}/${endDate.getFullYear()}`;
      if (startDate > endDate) {
        this.translate.get("ALTDATEVALIDATE").subscribe((sdRes: string) => {
          this.messageService.add({
            severity: "error",
            detail: sdRes,
          });
        });
        this.onSubmitTouched = false;
      } else {
        var newSelectedFilter = {};
        this.filterList.map((item) => {
          if (item.REPORTPARAM != "") {
            if (this.addScheduleGeneral.value[item.REPORTPARAM] != "") {
              newSelectedFilter[
                item.REPORTPARAM
              ] = this.addScheduleGeneral.value[item.REPORTPARAM].join();
            }
            delete this.addScheduleGeneral.value[item.REPORTPARAM];
          }
        });

        this.filterGroups.map((item) => {
          if (item.REPORTPARAM != "") {
            if (this.addScheduleGeneral.value[item.REPORTPARAM] != "") {
              newSelectedFilter[
                item.REPORTPARAM
              ] = this.addScheduleGeneral.value[item.REPORTPARAM].join();
            }
            delete this.addScheduleGeneral.value[item.REPORTPARAM];
          }
        });

        this.filterCategory.map((item) => {
          if (item.REPORTPARAM != "") {
            if (this.addScheduleGeneral.value[item.REPORTPARAM] != "") {
              newSelectedFilter[
                item.REPORTPARAM
              ] = this.addScheduleGeneral.value[item.REPORTPARAM].join();
            }
            delete this.addScheduleGeneral.value[item.REPORTPARAM];
          }
        });

        if (this.filterGroupList != "") {
          this.filterGroupList.map((item) => {
            if (
              this.addScheduleGeneral.value[item.REPORTPARAM] != "" &&
              this.addScheduleGeneral.value[item.REPORTPARAM] != null
            ) {
              newSelectedFilter[
                item.REPORTPARAM
              ] = this.addScheduleGeneral.value[item.REPORTPARAM];
            }
            delete this.addScheduleGeneral.value[item.REPORTPARAM];
          });
        }

        this.filterOptional.map((item) => {
          if (this.addScheduleGeneral.value[item.REPORTPARAM] != "") {
            newSelectedFilter[item.REPORTPARAM] = this.addScheduleGeneral.value[
              item.REPORTPARAM
            ].join();
          }
          delete this.addScheduleGeneral.value[item.REPORTPARAM];
        });

        if (this.filterChart != "") {
          this.filterChart.map((item) => {
            if (this.addScheduleGeneral.value[item.REPORTPARAM] != "") {
              newSelectedFilter[
                item.REPORTPARAM
              ] = this.addScheduleGeneral.value[item.REPORTPARAM];
            }
            delete this.addScheduleGeneral.value[item.REPORTPARAM];
          });
        }

        this.addScheduleGeneral.value["options"] = [newSelectedFilter];

        this.addScheduleGeneral.value[
          "emailTo"
        ] = this.addScheduleGeneral.value["emailTo"].join();

        if (this.addScheduleGeneral.value["tillDate"] != "") {
          this.addScheduleGeneral.value[
            "tillDate"
          ] = this.addScheduleGeneral.value["tillDate"].join();
        }
        var dayListPos = this.addScheduleGeneral.value["scheduleDay"];
        var dayList = "0000000";
        dayList = this.replaceAt(dayList, dayListPos, "1");
        this.addScheduleGeneral.value["dayList"] = dayList;

        this.addScheduleGeneral.value["scheduleBody"] = tinymce
          .get("tinymceEditor")
          .getContent();
        this.scheduleReports
          .insertScheduleReport(this.addScheduleGeneral.value)
          .subscribe((res) => {
            this.translate.get(res["message"]).subscribe((message) => {
              if (res["status"] == true) {
                // this.messageService.add({
                //   severity: "success",
                //   detail: message
                // });
                this.cookieService.set("add-new-schedule", message);
                this.router.navigate(["./edit-schedule-reports/" + res["id"]], {
                  skipLocationChange: true,
                });
              } else {
                this.onSubmitTouched = false;
                this.messageService.add({
                  severity: "error",
                  detail: message,
                });
              }
            });
          });
      }
    }
  }

  resetScheduleForm() {
    // this.addScheduleGeneral.patchValue(this.initialGeneralFormData);
    this.getFilters(0);
    this.sendEmailType(2);
    this.ngOnInit();
  }

  toggle() {
    this.showSearch = !this.showSearch;
  }
}
