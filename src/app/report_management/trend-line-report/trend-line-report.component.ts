import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { formatDate } from "@angular/common";
import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";

import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";

@Component({
  selector: "app-trend-line-report",
  templateUrl: "./trend-line-report.component.html",
  styleUrls: ["./trend-line-report.component.css"],
})
export class TrendLineReportComponent implements OnInit {
  @Input() reportId: any;
  tableCols: any[] = [];
  selectedReportView: any;
  selectedSiteId: any = "";
  tableData: any = [];
  title: any;
  isLoaded: boolean = true;
  errormsg: any;
  siteList: any = [{ label: "All", value: "" }];
  chartType: any = "line";
  dataFormat: any = "json";
  dataSource: any;
  parameters: any;
  allSiteSelected: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  pdfRows: any = [];
  chart: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.translate.get("LBLTRENDLINERPT").subscribe((title) => {
      this.title = title;
    });

    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT", url: "./assets/help/trend-line-report.md" },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);

    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      if (this.parameters["CHARTTYPE"] == "1") {
        this.parameters["designId"] = 36;
        this.selectedReportView = 36;
      } else if (this.parameters["CHARTTYPE"] == "2") {
        this.parameters["designId"] = 37;
        this.selectedReportView = 37;
      } else {
        this.parameters["designId"] = 38;
        this.selectedReportView = 38;
      }
      if (this.parameters["SITEID"]) {
        this.allSiteSelected = this.parameters["SITEID"];
      } else {
        this.allSiteSelected = "";
      }
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    this.getReports();

    this.translate
      .get(["LBLMONTHTOMONTH", "LBLTHREEMONTHAVG", "LBLSAFEPERCENTAGE"])
      .subscribe((res) => {
        if (this.selectedReportView == 36) {
          this.tableCols = [
            { field: "MONTHYEAR", header: res["LBLMONTHTOMONTH"] },
            { field: "SAFE", header: res["LBLSAFEPERCENTAGE"] },
          ];
        }
        if (this.selectedReportView == 37) {
          this.tableCols = [
            { field: "THREEMON", header: res["LBLTHREEMONTHAVG"] },
            { field: "SAFE", header: res["LBLSAFEPERCENTAGE"] },
          ];
        }
      });
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.translate
          .get([
            "LBLMONTHTOMONTH",
            "LBLYEARTOYEAR",
            "LBLTHREEMONTHAVG",
            "LBLSAFEPERCENTAGE",
            "FMKMONTHS",
          ])
          .subscribe((resLabel) => {
            this.tableData = res["Details"];
            var tempData = [];
            var chartMonth = [];
            var chartYearData = [];
            this.tableData.map((item) => {
              if (this.selectedReportView == 36) {
                item.SAFE = parseFloat(item.SAFE).toFixed(2);
                tempData.push({ label: item.MONTHYEAR, value: item.SAFE });
              }
              if (this.selectedReportView == 37) {
                item.SAFE = parseFloat(item.SAFE).toFixed(2);
                tempData.push({ label: item.THREEMON, value: item.SAFE });
              }
            });

            if (this.selectedReportView == 38) {
              var cols = Object.keys(this.tableData[0]);
              var tempCols = cols.filter(function (item) {
                return item !== "MONNO" && item !== "MONTH";
              });
              this.tableCols = [
                { field: "MONTH", header: resLabel["FMKMONTHS"] },
              ];
              var i = 1;
              tempCols.map((col) => {
                var tempColData = [];
                this.tableCols.push({ field: col, header: col + " %" });
                this.tableData.map((data) => {
                  data[col] = parseFloat(data[col]).toFixed(2);
                  tempData.push({ label: data.MONTH, value: data[col] });
                  if (i == 1) {
                    chartMonth.push({ label: data.MONTH });
                  }
                  tempColData.push({
                    value: (data[col] = parseFloat(data[col]).toFixed(2)),
                  });
                });
                chartYearData.push({ seriesname: col, data: tempColData });
                i++;
              });
            }

            if (this.siteList.length == 1) {
              res["sitelist"].map((data) => {
                this.siteList.push({
                  label: data.SITENAME,
                  value: data.SITEID,
                });
              });
            }
            var chartData;
            if (this.selectedReportView == 36) {
              chartData = {
                chart: {
                  caption: resLabel["LBLMONTHTOMONTH"],
                  xaxisname: resLabel["LBLMONTHTOMONTH"],
                  yaxisname: resLabel["LBLSAFEPERCENTAGE"],
                  xAxisNameFontColor: "#" + customColor,
                  yAxisNameFontColor: "#" + customColor,
                  outCnvBaseFontColor: "#" + customColor,
                  xAxisNameFontBold: 1,
                  yAxisNameFontBold: 1,
                  numbersuffix: "%",
                  rotatelabels: "0",
                  theme: "fusion",
                  showValues: "1",
                  rotateValues: "1",
                  showHoverEffect: "0",
                },
                data: tempData,
              };
            } else if (this.selectedReportView == 37) {
              chartData = {
                chart: {
                  caption: resLabel["LBLTHREEMONTHAVG"],
                  xaxisname: resLabel["LBLTHREEMONTHAVG"],
                  yaxisname: resLabel["LBLSAFEPERCENTAGE"],
                  xAxisNameFontColor: "#" + customColor,
                  yAxisNameFontColor: "#" + customColor,
                  outCnvBaseFontColor: "#" + customColor,
                  xAxisNameFontBold: 1,
                  yAxisNameFontBold: 1,
                  numbersuffix: "%",
                  rotatelabels: "0",
                  showValues: "1",
                  rotateValues: "1",
                  theme: "fusion",
                  showHoverEffect: "0",
                },
                data: tempData,
              };
            } else if (this.selectedReportView == 38) {
              chartData = {
                chart: {
                  caption: resLabel["LBLYEARTOYEAR"],
                  xaxisname: resLabel["FMKMONTHS"],
                  yaxisname: resLabel["LBLSAFEPERCENTAGE"],
                  xAxisNameFontColor: "#" + customColor,
                  yAxisNameFontColor: "#" + customColor,
                  outCnvBaseFontColor: "#" + customColor,
                  xAxisNameFontBold: 1,
                  yAxisNameFontBold: 1,
                  numbersuffix: "%",
                  showValues: "1",
                  rotateValues: "1",
                  formatnumberscale: "1",
                  plottooltext: "$seriesName, $label, $dataValue",
                  theme: "fusion",
                },
                categories: [
                  {
                    category: chartMonth,
                  },
                ],
                dataset: chartYearData,
              };
              this.chartType = "mscolumn2d";
            }

            this.dataSource = chartData;

            this.isLoaded = true;
          });
        this.manageExportData();
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  changeReport() {
    this.isLoaded = false;
    this.parameters["designId"] = this.selectedReportView;
    if (this.selectedSiteId == 0) {
      this.parameters["SITEID"] = this.allSiteSelected;
    } else {
      this.parameters["SITEID"] = this.selectedSiteId;
    }
    this.getReports();
  }

  manageExportData(): void {
    this.tableRows = [];
    this.pdfRows = [];
    this.columns = [];
    if (this.tableData.length > 0 && this.tableCols.length > 0) {
      this.tableData.map((data) => {
        var dataJson = {};
        this.tableCols.map((item) => {
          dataJson[item.header] = data[item.field];
        });
        // dataJson[this.tableCols[0].header] = data[this.tableCols[0].field];
        // dataJson[this.tableCols[1].header] = data[this.tableCols[1].field];
        this.tableRows.push(dataJson);
        var tempPdfData = [];
        this.tableCols.map((item) => {
          tempPdfData.push(data[item.field]);
        });
        this.pdfRows.push(tempPdfData);
      });
    }
    // this.columns[0] = this.tableCols[0].header;
    // this.columns[1] = this.tableCols[1].header;
    this.tableCols.map((item) => {
      this.columns.push(item.header);
    });
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    var fileName = "TRENDLINEREPORT";
    var encodedData;
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    this.chart.getSVGString((svg) => {
      this.reportsService.svgString2Image(
        svg,
        1382,
        500,
        "png",
        function (base64String) {
          encodedData = base64String;
        }
      );
    });
    setTimeout(() => {
      if (this.selectedExport == 6) {
        this.exportAsExcelFile(this.tableRows, fileName);
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
      } else if (
        this.selectedExport == 1 ||
        this.selectedExport == 3 ||
        this.selectedExport == 4 ||
        this.selectedExport == 5
      ) {
        var EXCEL_EXTENSION;
        if (this.selectedExport == 1 || this.selectedExport == 5) {
          EXCEL_EXTENSION = ".xlsx";
        } else if (this.selectedExport == 3 || this.selectedExport == 4) {
          EXCEL_EXTENSION = ".xls";
        }
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        //Excel Title, Header, Data
        const header = this.columns;
        const data = this.tableRows;

        const EXCEL_TYPE =
          "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

        //Create workbook and worksheet
        let workbook = new ExcelJS.Workbook();
        // Report scope sheet
        let firstWorksheet = workbook.addWorksheet("Report Scope");
        firstWorksheet.addRow([]);
        let firstHeaderRow = firstWorksheet.addRow([this.title]);

        firstWorksheet.getCell("A2").font = {
          name: "Arial Unicode MS",
          family: 4,
          size: 18,
          bold: true,
          color: { argb: "00000000" },
        };
        firstWorksheet.getCell("A2").alignment = {
          vertical: "middle",
          horizontal: "left",
          indent: 1,
        };
        firstWorksheet.getColumn("A").width = 100;
        firstWorksheet.getRow(2).height = 30;

        this.translate
          .get(["LBLOBSDATE", "LBLCREATEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              firstWorksheet.addRow([]);
              firstWorksheet.addRow([]);
              firstWorksheet
                .addRow([resLabel["LBLOBSDATE"]])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 14,
                    bold: true,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 2,
                  };
                });

              firstWorksheet.lastRow.height = 25;
              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              firstWorksheet
                .addRow([
                  fDate +
                    " - " +
                    tDate +
                    " (" +
                    this.auth.UserInfo["dateFormat"].toUpperCase() +
                    ")",
                ])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 12,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 3,
                  };
                });
            }

            firstWorksheet.lastRow.height = 25;
          });

        // Report View sheet
        let worksheet = workbook.addWorksheet("Sheet1");
        //Add Header Row
        if (this.selectedExport == 1 && this.selectedExport == 2) {
          worksheet.getColumn(1).width = 40;
          worksheet.getColumn(2).width = 50;
          worksheet.getRow(1).height = 250;
          worksheet.mergeCells("A1:B1");
          var imageId = workbook.addImage({
            base64: encodedData,
            extension: "png",
          });
          worksheet.addImage(imageId, {
            tl: { col: 0, row: 0 },
            ext: { width: 800, height: 300 },
          });
        }

        let headerRow = worksheet.addRow(header);
        headerRow.height = 20;
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          if (number == 2) {
            cell.alignment = { vertical: "middle", horizontal: "center" };
          } else {
            cell.alignment = { vertical: "middle", horizontal: "left" };
          }
        });
        // Add Data and Conditional Formatting
        let sumOfData = 0;
        data.map((element) => {
          let eachRow = [];
          this.columns.map((headers, key) => {
            if (key == 1) {
              sumOfData = sumOfData + element[headers];
            }
            eachRow.push(element[headers]);
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            worksheet.addRow(eachRow).getCell(2).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
          }
        });
        // worksheet.getColumn(1).width = 40;
        // worksheet.getColumn(2).width = 30;
        worksheet.addRow([]);
        //Generate Excel File with given name
        workbook.xlsx.writeBuffer().then((data) => {
          let blob = new Blob([data], { type: EXCEL_TYPE });
          fs.saveAs(
            blob,
            fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
          );
        });
      } else if (this.selectedExport == 2) {
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        var pdf = new jsPDF("landscape");
        var fn = this;
        // report scope
        pdf.text(this.title, 15, 20);
        let finalX = 10;
        this.translate
          .get(["LBLOBSDATE", "LBLCREATEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              let obsDate = [];
              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
                startY: finalX + 13,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }
            pdf.addPage();
          });

        // report view
        pdf.addImage(encodedData, "JPEG", 10, 0, 270, 120);
        pdf.autoTable(this.columns, this.pdfRows, {
          startY: 130,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          didParseCell: (data) => {
            fn.alignCol(data);
          },
        });
        pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
      }
      this.selectedExport = 0;
    }, 1000);
  }

  alignCol = function (data) {
    if (data.column.index > 0) {
      data.cell.styles.halign = "center";
    }
  };

  initialized($event) {
    this.chart = $event.chart; // saving chart instance
  }
}
