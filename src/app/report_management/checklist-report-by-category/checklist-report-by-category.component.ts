import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { isArray } from "util";
import { formatDate } from "@angular/common";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { AuthenticationService } from "src/app/_services/authentication.service";

@Component({
  selector: "app-checklist-report-by-category",
  templateUrl: "./checklist-report-by-category.component.html",
  styleUrls: ["./checklist-report-by-category.component.css"],
})
export class ChecklistReportByCategoryComponent implements OnInit {
  @Input() reportId: any;
  title: any;
  cols: any = [];
  tableData: any = [];
  isLoaded: boolean;
  selectedSiteName: any = "";
  selectedMainCategory: any = "";
  siteList: any = [{ label: "All", value: "" }];
  selectedReportView: any;
  reportView: any = [];
  mainCategoryList: any = [{ label: "All", value: "" }];
  errormsg: any;
  width: any;
  height: any;
  type: any;
  dataFormat: any;
  dataSource: any;
  parameters: any;
  allSiteSelected: any;
  selectedCategoryId: any;
  displaySubReport: boolean;
  subReportId: any = 3;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  pdfRows: any = [];
  chart: any;
  categoryName: any;
  subAreaOption: number;
  colleps: number = 0;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.translate.get("LBLCHKBYCATGRPT").subscribe((title) => {
      this.title = title;
    });

    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT", url: "./assets/help/report-by-category.md" },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);

    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.allSiteSelected = this.parameters["SITEID"];
      this.selectedReportView = this.parameters["designId"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }
    this.getReports();

    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (isArray(res["designData"])) {
        this.reportView = [];
        var designData = res["designData"];
        designData.map((item) => {
          this.translate.get(item.RPTNAME).subscribe((label) => {
            this.reportView.push({
              label: label,
              value: item.DESIGNID,
            });
          });
        });
      }
      this.translate
        .get([
          "LBLMAINCATEGORY",
          "LBLSAFECOUNT",
          "LBLUNSAFECOUNT",
          "LBLSUBCATNAME",
        ])
        .subscribe((res) => {
          if (this.selectedReportView == 16) {
            this.cols = [
              { field: "MAINCATEGORYNAME", header: res["LBLMAINCATEGORY"] },
              { field: "SAFECNT", header: res["LBLSAFECOUNT"] },
            ];
          } else if (this.selectedReportView == 17) {
            this.cols = [
              { field: "MAINCATEGORYNAME", header: res["LBLMAINCATEGORY"] },
              { field: "UNSAFECNT", header: res["LBLUNSAFECOUNT"] },
            ];
          } else if (this.selectedReportView == 18) {
            this.cols = [
              { field: "SUBCATEGORYNAME", header: res["LBLSUBCATNAME"] },
              { field: "UNSAFECNT", header: res["LBLUNSAFECOUNT"] },
            ];
          }
        });
      if (res["status"] == true) {
        this.tableData = res["Data"];
        // this.siteList = [{ label: "All", value: "" }];
        // this.mainCategoryList = [{ label: "All", value: "" }];

        if (this.siteList.length == 1) {
          var sitelist = res["sitelist"];
          sitelist.map((site) => {
            this.siteList.push({ label: site.SITENAME, value: site.SITEID });
          });
        }

        if (res["mainCategory"] && this.mainCategoryList.length == 1) {
          var maincategory = res["mainCategory"];
          maincategory.map((category) => {
            this.mainCategoryList.push({
              label: category.MAINCATEGORYNAME,
              value: category.MAINCATEGORYID,
            });
          });
        }

        var i = 1;
        var chartData = [];
        this.tableData.map((tdata) => {
          this.translate
            .get([
              "LBLMAINCATEGORY",
              "LBLSAFECOUNT",
              "LBLUNSAFECOUNT",
              "LBLSUBCATNAME",
            ])
            .subscribe((resLabel) => {
              var xname, yname;
              if (this.selectedReportView == 16) {
                chartData.push({
                  label: tdata.MAINCATEGORYNAME,
                  value: tdata.SAFECNT,
                });
                xname = resLabel["LBLMAINCATEGORY"];
                yname = resLabel["LBLSAFECOUNT"];
              } else if (this.selectedReportView == 17) {
                chartData.push({
                  label: tdata.MAINCATEGORYNAME,
                  value: tdata.UNSAFECNT,
                });
                xname = resLabel["LBLMAINCATEGORY"];
                yname = resLabel["LBLUNSAFECOUNT"];
              } else if (this.selectedReportView == 18) {
                chartData.push({
                  label: tdata.SUBCATEGORYNAME,
                  value: tdata.UNSAFECNT,
                });
                xname = resLabel["LBLSUBCATNAME"];
                yname = resLabel["LBLUNSAFECOUNT"];
              }

              if (this.tableData.length == i) {
                const data = {
                  chart: {
                    id: "fuchart1",
                    xaxisname: xname,
                    yaxisname: yname,
                    xAxisNameFontColor: "#" + customColor,
                    yAxisNameFontColor: "#" + customColor,
                    outCnvBaseFontColor: "#" + customColor,
                    borderThickness: "4",
                    showvalues: "1",
                    theme: "fusion",
                    palettecolors:
                      "5d62b5,29c3be,f2726f,A9BDCD,D4BB6C,9DB4B6,CE9B7A,6BA8A9,889D72,A96269,69A7C9,A26F45,A194AF,B9895E,A09B55",
                  },
                  data: chartData,
                };

                this.width = "100%";
                this.height = 400;
                this.type = "bar3d";
                this.dataFormat = "json";
                this.dataSource = data;

                this.isLoaded = true;
              }
              i++;
            });
        });
        this.manageExportData();
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  barClick(data) {
    this.tableData.map((item) => {
      if (item["MAINCATEGORYNAME"] == data["dataObj"]["categoryLabel"]) {
        this.openSubReport(item["MAINCATEGORYID"], item["MAINCATEGORYNAME"]);
      }
      if (item["SUBCATEGORYNAME"] == data["dataObj"]["categoryLabel"]) {
        this.openSubReport(item["SUBCATEGORYID"], item["MAINCATEGORYNAME"]);
      }
    });
  }

  changeLBLCHKBYCATGRPT(type) {
    this.isLoaded = false;
    this.tableData = [];
    this.parameters["designId"] = this.selectedReportView;
    if (this.selectedReportView == 16) {
      this.subReportId = 3;
    } else if (this.selectedReportView == 17) {
      this.subReportId = 4;
    } else {
      this.subReportId = 5;
    }
    if (this.selectedSiteName == 0) {
      this.parameters["SITEID"] = this.allSiteSelected;
    } else {
      this.parameters["SITEID"] = this.selectedSiteName;
    }
    // if (this.selectedMainCategory) {
    this.parameters["MAINCATEGORYID"] = this.selectedMainCategory;
    // }
    this.getReports();
  }

  openSubReport(categoryId, categoryName) {
    this.selectedCategoryId = categoryId;
    this.categoryName = categoryName;
    this.displaySubReport = true;
  }
  manageExportData(): void {
    this.tableRows = [];
    this.pdfRows = [];
    this.columns = [];
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.tableData.map((data) => {
        var dataJson = {};
        dataJson[this.cols[0].header] = data[this.cols[0].field];
        dataJson[this.cols[1].header] = data[this.cols[1].field];
        this.tableRows.push(dataJson);
        this.pdfRows.push([data[this.cols[0].field], data[this.cols[1].field]]);
      });
    }
    this.columns[0] = this.cols[0].header;
    this.columns[1] = this.cols[1].header;
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    var encodedData;
    this.chart.getSVGString((svg) => {
      this.reportsService.svgString2Image(
        svg,
        1030,
        524,
        "png",
        function (base64String) {
          encodedData = base64String;
        }
      );
    });
    var fileName = "CHECKLISTCATEGORYSAFEMAIN";
    if (this.selectedReportView == 16) {
      fileName = "CHECKLISTCATEGORYSAFEMAIN";
    } else if (this.selectedReportView == 17) {
      fileName = "CHECKLISTCATEGORYUNSAFEMAIN";
    } else if (this.selectedReportView == 18) {
      fileName = "CHECKLISTCATEGORYUNSAFESUB";
    }
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    setTimeout(() => {
      if (
        this.selectedExport == 1 ||
        this.selectedExport == 3 ||
        this.selectedExport == 4 ||
        this.selectedExport == 5
      ) {
        var EXCEL_EXTENSION;
        if (this.selectedExport == 1 || this.selectedExport == 5) {
          EXCEL_EXTENSION = ".xlsx";
        } else if (this.selectedExport == 3 || this.selectedExport == 4) {
          EXCEL_EXTENSION = ".xlsx";
        }
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        //Excel Title, Header, Data
        const header = this.columns;
        const data = this.tableRows;

        const EXCEL_TYPE =
          "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

        //Create workbook and worksheet
        let workbook = new ExcelJS.Workbook();
        // Report scope sheet
        let firstWorksheet = workbook.addWorksheet("Report Scope");
        firstWorksheet.addRow([]);
        let firstHeaderRow = firstWorksheet.addRow([this.title]);

        firstWorksheet.getCell("A2").font = {
          name: "Arial Unicode MS",
          family: 4,
          size: 18,
          bold: true,
          color: { argb: "00000000" },
        };
        firstWorksheet.getCell("A2").alignment = {
          vertical: "middle",
          horizontal: "left",
          indent: 1,
        };
        firstWorksheet.getColumn("A").width = 100;
        firstWorksheet.getRow(2).height = 30;

        this.translate
          .get(["LBLOBSDATE", "LBLCREATEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              firstWorksheet.addRow([]);
              firstWorksheet.addRow([]);
              firstWorksheet
                .addRow([resLabel["LBLOBSDATE"]])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 14,
                    bold: true,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 2,
                  };
                });

              firstWorksheet.lastRow.height = 25;
              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              firstWorksheet
                .addRow([
                  fDate +
                    " - " +
                    tDate +
                    " (" +
                    this.auth.UserInfo["dateFormat"].toUpperCase() +
                    ")",
                ])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 12,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 3,
                  };
                });
            }

            firstWorksheet.lastRow.height = 25;
          });

        // Report View sheet
        let worksheet = workbook.addWorksheet("Sheet1");
        //Add Header Row
        if (this.selectedExport != 4 || this.selectedExport != 5) {
          worksheet.getColumn(1).width = 60;
          worksheet.getColumn(2).width = 60;
          worksheet.getRow(1).height = 250;
          worksheet.mergeCells("A1:B1");
          var imageId = workbook.addImage({
            base64: encodedData,
            extension: "png",
          });
          worksheet.addImage(imageId, {
            tl: { col: 0, row: 0 },
            ext: { width: 800, height: 300 },
          });
        }
        let headerRow = worksheet.addRow(header);
        headerRow.height = 20;
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          if (number == 2) {
            cell.alignment = { vertical: "middle", horizontal: "center" };
          } else {
            cell.alignment = { vertical: "middle", horizontal: "left" };
          }
        });
        // Add Data and Conditional Formatting
        let sumOfData = 0;
        data.map((element) => {
          let eachRow = [];
          this.columns.map((headers, key) => {
            if (key == 1) {
              sumOfData = sumOfData + element[headers];
            }
            eachRow.push(element[headers]);
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            worksheet.addRow(eachRow).getCell(2).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
          }
        });
        // worksheet.getColumn(1).width = 40;
        // worksheet.getColumn(2).width = 30;
        worksheet.addRow([]);
        //Generate Excel File with given name
        workbook.xlsx.writeBuffer().then((data) => {
          let blob = new Blob([data], { type: EXCEL_TYPE });
          fs.saveAs(
            blob,
            fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
          );
        });
      } else if (this.selectedExport == 2) {
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        var pdf = new jsPDF("landscape");
        // report scope
        pdf.text(this.title, 15, 20);
        let finalX = 10;
        this.translate
          .get(["LBLOBSDATE", "LBLCREATEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              let obsDate = [];
              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
                startY: finalX + 13,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }
            pdf.addPage();
          });

        // report view
        pdf.addImage(encodedData, "PNG", 20, 0, 220, 120);
        var fn = this;
        pdf.autoTable(this.columns, this.pdfRows, {
          startY: 130,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          didParseCell: (data) => {
            // Instance of <tr> element
            fn.alignCol(data);
          },
        });
        this.parameters[""];
        pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
      } else if (this.selectedExport == 6) {
        this.exportAsExcelFile(this.tableRows, fileName);
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
      }
      this.selectedExport = 0;
    }, 1000);
  }

  alignCol = function (data) {
    if (data.column.index == 1) {
      data.cell.styles.halign = "center";
    }
  };

  initialized($event) {
    this.chart = $event.chart; // saving chart instance
  }
}
