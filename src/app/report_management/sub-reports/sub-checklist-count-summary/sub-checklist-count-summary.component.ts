import { Component, OnInit, Input } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { Router } from "@angular/router";
import { ReportsService } from "src/app/_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";
// var _ = require("lodash");
import { _ } from "lodash";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { GlobalDataService } from "src/app/_services/global-data.service";

@Component({
  selector: "app-sub-checklist-count-summary",
  templateUrl: "./sub-checklist-count-summary.component.html",
  styleUrls: ["./sub-checklist-count-summary.component.css"],
})
export class SubChecklistCountSummaryComponent implements OnInit {
  @Input() reportId: any;
  @Input() subReportId: any;
  @Input() reportParam: any;
  title: any;
  parameters: any;
  tableData: any = [];
  cols: any = [];
  isLoaded: boolean;
  errormsg: any = "";
  rowGroupMetadata: any;
  fieldCols: any = [];
  colCount: any;
  extraColCount: any = 0;
  mainHeaderKeys: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any = 0;
  tableRows: any;
  columns: any;
  unFColumns: any;
  unFTableRows: any;
  pdfData: any = [];
  dataKey: any = "SITENAME";
  colId: any;
  mainHead: any;
  headingValue: any;
  dateFormat: any;
  subAreaOption: number;
  colleps: number = 0;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private auth: AuthenticationService,
    private globalData: GlobalDataService
  ) {}

  ngOnInit() {
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.parameters["subReportId"] = this.subReportId;
      this.parameters["SUBRPTPARAM"] = this.reportParam;
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    this.getReports();

    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
    this.dateFormat = this.auth.UserInfo["dateFormat"];
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getSubReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = [];
        this.headingValue = res["headingValue"];
        this.mainHeaderKeys = Object.keys(res["headingValue"]);
        this.tableData = res["data"];
        if (this.tableData.length > 0) {
          var keys = Object.keys(this.tableData[0]);
        }
        var tempColumn = [
          "AREAID",
          "AREANAME",
          "CARDID",
          "CUSPOSITION",
          "ENTEREDBY",
          "ENTEREDDATE",
          "OBSDATE",
          "OBSERVATIONDATE",
          "OBSERVERNAME",
          "OBSERVERNAME1",
          "OBSERVERID",
          "SHIFTID",
          "SHIFTNAME",
          "SITEID",
          "SITENAME",
          "SUBAREAID",
          "SUBAREANAME",
        ];
        keys.map((label) => {
          if (tempColumn.indexOf(label) == -1) {
            this.fieldCols.push(label);
          }
        });
        // if (this.subReportId == 5) {
        //   this.extraColCount = this.colCount % 3;
        // } else {
        //   this.extraColCount = this.colCount % 2;
        // }

        this.translate
          .get([
            "LBLOBSDATE",
            "LBLOBSERVERNAME",
            "LBLAREANAME",
            "LBLSUBAREAFULLNAME",
            "LBLSHIFTNAME",
            "LBLENTEREDBY",
            "LBLENTEREDDATE",
          ])
          .subscribe((resLabel) => {
            this.subAreaOption = this.auth.UserInfo["subAreaOptionValue"];
            if (this.subAreaOption == 0) {
              this.colleps = 1;
              this.cols = [
                { field: "OBSERVATIONDATE", header: resLabel["LBLOBSDATE"] },
                { field: "OBSERVERNAME", header: resLabel["LBLOBSERVERNAME"] },
                { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                { field: "SHIFTNAME", header: resLabel["LBLSHIFTNAME"] },
                { field: "ENTEREDBY", header: resLabel["LBLENTEREDBY"] },
                { field: "ENTEREDDATE", header: resLabel["LBLENTEREDDATE"] },
              ];
            } else {
              this.colleps = 0;
              this.cols = [
                { field: "OBSERVATIONDATE", header: resLabel["LBLOBSDATE"] },
                { field: "OBSERVERNAME", header: resLabel["LBLOBSERVERNAME"] },
                { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                {
                  field: "SUBAREANAME",
                  header: resLabel["LBLSUBAREAFULLNAME"],
                },
                { field: "SHIFTNAME", header: resLabel["LBLSHIFTNAME"] },
                { field: "ENTEREDBY", header: resLabel["LBLENTEREDBY"] },
                { field: "ENTEREDDATE", header: resLabel["LBLENTEREDDATE"] },
              ];
            }
          });
        this.updateRowGroupMetaData();
        this.colCount =
          (7 + this.fieldCols.length) / this.mainHeaderKeys.length;

        this.extraColCount =
          (7 + this.fieldCols.length) % this.mainHeaderKeys.length;
        this.manageExportData();
        this.isLoaded = true;
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    this.mainHead = [];
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let rowData = this.tableData[i];
        let sitename = rowData[this.mainHeaderKeys[0]];
        if (i == 0) {
          this.rowGroupMetadata[sitename] = { index: 0, size: 1 };

          var dataJson = {};
          this.mainHeaderKeys.map((item) => {
            this.fieldCols.map((data, j) => {
              if (data == item) {
                this.fieldCols.splice(j, 1);
              }
            });
            dataJson[item] = rowData[item];
          });
          this.mainHead.push(dataJson);
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData[this.mainHeaderKeys[0]];
          if (sitename === previousRowGroup) {
            this.rowGroupMetadata[sitename].size++;
          } else {
            this.rowGroupMetadata[sitename] = { index: i, size: 1 };

            var dataJson = {};
            this.mainHeaderKeys.map((item) => {
              this.fieldCols.map((data, j) => {
                if (data == item) {
                  this.fieldCols.splice(j, 1);
                }
              });
              dataJson[item] = rowData[item];
            });
            this.mainHead.push(dataJson);
          }
        }
      }
    }
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    var enableSubarea = 0;
    if (this.globalData.GlobalData["ENABLESUBAREA"] != 1) {
      enableSubarea = 1;
    }
    if (this.tableData.length > 0 && this.cols.length > 0) {
      // get data for Excel
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempDataJson = {};
        // var tempRows = [];
        this.mainHeaderKeys.map((item) => {
          this.translate
            .get([
              "LBLSITE",
              "LBLAREA",
              "LBLSUBAREANAME",
              "LBLSHIFT",
              "LBLOBSERVER",
            ])
            .subscribe((resLabel) => {
              if (item == "SITENAME") {
                if (key == 0) {
                  this.unFColumns.push(resLabel["LBLSITE"]);
                }
                tempDataJson[resLabel["LBLSITE"]] = data[item];
              } else if (item == "AREANAME") {
                if (key == 0) {
                  this.unFColumns.push(resLabel["LBLAREA"]);
                }
                tempDataJson[resLabel["LBLAREA"]] = data[item];
              } else if (item == "SUBAREANAME") {
                if (key == 0) {
                  this.unFColumns.push(resLabel["LBLSUBAREANAME"]);
                }
                tempDataJson[resLabel["LBLSUBAREANAME"]] = data[item];
              } else if (item == "SHIFTNAME") {
                if (key == 0) {
                  this.unFColumns.push(resLabel["LBLSHIFT"]);
                }
                tempDataJson[resLabel["LBLSHIFT"]] = data[item];
              } else if (item == "OBSERVERNAME") {
                if (key == 0) {
                  this.unFColumns.push(resLabel["LBLOBSERVER"]);
                }
                tempDataJson[resLabel["LBLOBSERVER"]] = data[item];
              } else {
                tempDataJson[item] = data[item];
              }
            });
        });
        this.cols.map((item, index) => {
          if (item["field"] == "OBSERVERNAME") {
            var regexHash = new RegExp("<BR>", "g");
            if (
              data[item["field"]] &&
              data[item["field"]].search("<BR>") != -1
            ) {
              data[item["field"]] = data[item["field"]].replace(
                regexHash,
                "\n"
              );
            }
          }
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];
          if (index == 4 - enableSubarea) {
            this.fieldCols.map((fieldCol) => {
              dataJson[fieldCol] = data[fieldCol];
              tempDataJson[fieldCol] = data[fieldCol];
            });
          }
          // tempRows.push(data[item["field"]]);
          if (this.tableData.length - 1 == key) {
            if (index == 5 - enableSubarea) {
              this.fieldCols.map((fieldCol) => {
                this.columns.push(fieldCol);
                this.unFColumns.push(fieldCol);
                if (this.cols.length - 1 == index) {
                  this.colId = String.fromCharCode(
                    97 + this.columns.length - 1
                  ).toUpperCase();
                }
              });
            }
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
          }
        });
        this.mainHeaderKeys.map((item) => {
          dataJson[item] = data[item];
        });
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
        // this.pdfData.push(tempRows);
      });

      // get data for PDF
      var mainColumns = this.mainHeaderKeys;
      var tempMainColums = Object.keys(this.mainHead[0]);
      var subColumns = this.columns;
      this.mainHead.map((head) => {
        var mainRows = [];
        var subRows = [];
        // main Table
        var tempMainRows = [];
        tempMainColums.map((col) => {
          tempMainRows.push(head[col]);
        });
        mainRows.push(tempMainRows);

        // sub Table
        this.tableData.map((data) => {
          var tempSubRows = [];
          if (data[this.mainHeaderKeys[0]] == head[this.mainHeaderKeys[0]]) {
            this.cols.map((subCol, key) => {
              tempSubRows.push(data[subCol.field]);
              if (key == 4 - enableSubarea) {
                this.fieldCols.map((fieldCol) => {
                  tempSubRows.push(data[fieldCol]);
                });
              }
            });
            subRows.push(tempSubRows);
          }
        });
        this.pdfData.push({
          mainColumns: mainColumns,
          mainRows: mainRows,
          subColumns: subColumns,
          subRows: subRows,
        });
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    const fileName = "CHECKLISTCOUNTSUMMARYDETAIL";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      this.exportAsExcelFile(this.unFTableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var startColId;
      if (this.mainHeaderKeys.length == 1) {
        startColId = "A";
      } else if (this.mainHeaderKeys.length == 2) {
        startColId = "B";
      } else if (this.mainHeaderKeys.length == 3) {
        startColId = "C";
      }
      // ("C");
      // ("C");
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;
      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      let worksheet = workbook.addWorksheet("Sheet1");

      if (this.selectedExport == 4 || this.selectedExport == 5) {
        for (var i = 1; i <= 15; i++) {
          worksheet.getColumn(i).width = 25;
        }

        let headerRow = worksheet.addRow(unFColumns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });

        // Add Data and Conditional Formatting
        unFTableRows.map((element, index) => {
          let eachRow = [];
          unFColumns.map((headers, key) => {
            if (headers == "User Level" || headers == "Status") {
              if (element[headers]) {
                this.translate.get(element[headers]).subscribe((val) => {
                  eachRow.push(val);
                });
              } else {
                eachRow.push(element[headers]);
              }
            } else {
              eachRow.push(element[headers]);
            }
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            if (unFTableRows.length - 1 == index) {
              worksheet
                .addRow(eachRow)
                .eachCell({ includeEmpty: true }, (cell, number) => {
                  cell.border = {
                    bottom: { style: "medium", color: { argb: customColor } },
                  };
                  cell.numFmt = this.auth.changeCellValueType(cell.value);
                });
            } else {
              worksheet.addRow(eachRow).eachCell((cell) => {
                cell.numFmt = this.auth.changeCellValueType(cell.value);
              });
            }
          }
        });
      } else {
        let subHeader = header;
        var beforeDataCount = 1;
        this.mainHead.map((header, key) => {
          var rowsData = data.filter((item) => {
            return (
              item[this.manageExportData[0]] == header[this.manageExportData[0]]
            );
          });
          for (var i = 1; i <= 15; i++) {
            worksheet.getColumn(i).width = 25;
          }
          if (key == 0) {
            worksheet.mergeCells(
              startColId + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              startColId +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                +(beforeDataCount + 1)
            );
            this.translate
              .get([
                "LBLSITE",
                "LBLAREA",
                "LBLSUBAREANAME",
                "LBLSHIFT",
                "LBLOBSERVER",
              ])
              .subscribe((resLabel) => {
                if (this.mainHeaderKeys.length == 1) {
                  // worksheet.mergeCells(
                  //   "A" +
                  //     (beforeDataCount + this.columns.length) +
                  //     ":" +
                  //     this.colId +
                  //     +(beforeDataCount + 1)
                  // );
                  worksheet.getCell("A" + beforeDataCount).value =
                    this.mainHeaderKeys[0] == "SITENAME"
                      ? resLabel["LBLSITE"]
                      : this.mainHeaderKeys[0] == "AREANAME"
                      ? resLabel["LBLAREA"]
                      : this.mainHeaderKeys[0] == "SUBAREANAME"
                      ? resLabel["LBLSUBAREANAME"]
                      : this.mainHeaderKeys[0] == "SHIFTNAME"
                      ? resLabel["LBLSHIFT"]
                      : this.mainHeaderKeys[0] == "OBSERVERNAME"
                      ? resLabel["LBLOBSERVER"]
                      : this.mainHeaderKeys[0];

                  worksheet.getCell("A" + (beforeDataCount + 1)).value =
                    header[this.mainHeaderKeys[0]];

                  worksheet.getCell("A" + beforeDataCount).fill = {
                    type: "pattern",
                    pattern: "solid",
                    fgColor: { argb: customColor },
                  };
                  worksheet.getCell("A" + beforeDataCount).font = {
                    color: { argb: customFontColor },
                  };
                } else if (this.mainHeaderKeys.length == 2) {
                  worksheet.getCell("A" + beforeDataCount).value =
                    this.mainHeaderKeys[0] == "SITENAME"
                      ? resLabel["LBLSITE"]
                      : this.mainHeaderKeys[0] == "AREANAME"
                      ? resLabel["LBLAREA"]
                      : this.mainHeaderKeys[0] == "SUBAREANAME"
                      ? resLabel["LBLSUBAREANAME"]
                      : this.mainHeaderKeys[0] == "SHIFTNAME"
                      ? resLabel["LBLSHIFT"]
                      : this.mainHeaderKeys[0] == "OBSERVERNAME"
                      ? resLabel["LBLOBSERVER"]
                      : this.mainHeaderKeys[0];
                  worksheet.getCell("B" + beforeDataCount).value =
                    this.mainHeaderKeys[1] == "SITENAME"
                      ? resLabel["LBLSITE"]
                      : this.mainHeaderKeys[1] == "AREANAME"
                      ? resLabel["LBLAREA"]
                      : this.mainHeaderKeys[1] == "SUBAREANAME"
                      ? resLabel["LBLSUBAREANAME"]
                      : this.mainHeaderKeys[1] == "SHIFTNAME"
                      ? resLabel["LBLSHIFT"]
                      : this.mainHeaderKeys[1] == "OBSERVERNAME"
                      ? resLabel["LBLOBSERVER"]
                      : this.mainHeaderKeys[1];

                  worksheet.getCell("A" + (beforeDataCount + 1)).value =
                    header[this.mainHeaderKeys[0]];
                  worksheet.getCell("B" + (beforeDataCount + 1)).value =
                    header[this.mainHeaderKeys[1]];

                  worksheet.getCell("A" + beforeDataCount).fill = {
                    type: "pattern",
                    pattern: "solid",
                    fgColor: { argb: customColor },
                  };
                  worksheet.getCell("B" + beforeDataCount).fill = {
                    type: "pattern",
                    pattern: "solid",
                    fgColor: { argb: customColor },
                  };
                  worksheet.getCell("A" + beforeDataCount).font = {
                    color: { argb: customFontColor },
                  };
                  worksheet.getCell("B" + beforeDataCount).font = {
                    color: { argb: customFontColor },
                  };
                } else if (this.mainHeaderKeys.length == 3) {
                  worksheet.getCell("A" + beforeDataCount).value =
                    this.mainHeaderKeys[0] == "SITENAME"
                      ? resLabel["LBLSITE"]
                      : this.mainHeaderKeys[0] == "AREANAME"
                      ? resLabel["LBLAREA"]
                      : this.mainHeaderKeys[0] == "SUBAREANAME"
                      ? resLabel["LBLSUBAREANAME"]
                      : this.mainHeaderKeys[0] == "SHIFTNAME"
                      ? resLabel["LBLSHIFT"]
                      : this.mainHeaderKeys[0] == "OBSERVERNAME"
                      ? resLabel["LBLOBSERVER"]
                      : this.mainHeaderKeys[0];
                  worksheet.getCell("B" + beforeDataCount).value =
                    this.mainHeaderKeys[1] == "SITENAME"
                      ? resLabel["LBLSITE"]
                      : this.mainHeaderKeys[1] == "AREANAME"
                      ? resLabel["LBLAREA"]
                      : this.mainHeaderKeys[1] == "SUBAREANAME"
                      ? resLabel["LBLSUBAREANAME"]
                      : this.mainHeaderKeys[1] == "SHIFTNAME"
                      ? resLabel["LBLSHIFT"]
                      : this.mainHeaderKeys[1] == "OBSERVERNAME"
                      ? resLabel["LBLOBSERVER"]
                      : this.mainHeaderKeys[1];
                  worksheet.getCell("C" + beforeDataCount).value =
                    this.mainHeaderKeys[2] == "SITENAME"
                      ? resLabel["LBLSITE"]
                      : this.mainHeaderKeys[2] == "AREANAME"
                      ? resLabel["LBLAREA"]
                      : this.mainHeaderKeys[2] == "SUBAREANAME"
                      ? resLabel["LBLSUBAREANAME"]
                      : this.mainHeaderKeys[2] == "SHIFTNAME"
                      ? resLabel["LBLSHIFT"]
                      : this.mainHeaderKeys[2] == "OBSERVERNAME"
                      ? resLabel["LBLOBSERVER"]
                      : this.mainHeaderKeys[2];

                  worksheet.getCell("A" + (beforeDataCount + 1)).value =
                    header[this.mainHeaderKeys[0]];
                  worksheet.getCell("B" + (beforeDataCount + 1)).value =
                    header[this.mainHeaderKeys[1]];
                  worksheet.getCell("C" + (beforeDataCount + 1)).value =
                    header[this.mainHeaderKeys[2]];

                  worksheet.getCell("A" + beforeDataCount).fill = {
                    type: "pattern",
                    pattern: "solid",
                    fgColor: { argb: customColor },
                  };
                  worksheet.getCell("B" + beforeDataCount).fill = {
                    type: "pattern",
                    pattern: "solid",
                    fgColor: { argb: customColor },
                  };
                  worksheet.getCell("C" + beforeDataCount).fill = {
                    type: "pattern",
                    pattern: "solid",
                    fgColor: { argb: customColor },
                  };
                  worksheet.getCell("A" + beforeDataCount).font = {
                    color: { argb: customFontColor },
                  };
                  worksheet.getCell("B" + beforeDataCount).font = {
                    color: { argb: customFontColor },
                  };
                  worksheet.getCell("C" + beforeDataCount).font = {
                    color: { argb: customFontColor },
                  };
                }
              });

            beforeDataCount = rowsData.length + 5;
          } else {
            worksheet.mergeCells(
              startColId + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              startColId +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                +(beforeDataCount + 1)
            );
            if (this.mainHeaderKeys.length == 1) {
              worksheet.getCell(
                "A" + beforeDataCount
              ).value = this.mainHeaderKeys[0];

              worksheet.getCell(
                "A" + beforeDataCount
              ).value = this.mainHeaderKeys[0];

              worksheet.getCell("A" + (beforeDataCount + 1)).value =
                header[this.mainHeaderKeys[0]];

              worksheet.getCell("A" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("A" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
            } else if (this.mainHeaderKeys.length == 2) {
              worksheet.getCell(
                "A" + beforeDataCount
              ).value = this.mainHeaderKeys[0];
              worksheet.getCell(
                "B" + beforeDataCount
              ).value = this.mainHeaderKeys[1];

              worksheet.getCell(
                "A" + beforeDataCount
              ).value = this.mainHeaderKeys[0];
              worksheet.getCell(
                "B" + beforeDataCount
              ).value = this.mainHeaderKeys[1];

              worksheet.getCell("A" + (beforeDataCount + 1)).value =
                header[this.mainHeaderKeys[0]];
              worksheet.getCell("B" + (beforeDataCount + 1)).value =
                header[this.mainHeaderKeys[1]];

              worksheet.getCell("A" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("B" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("A" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              worksheet.getCell("B" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
            } else if (this.mainHeaderKeys.length == 3) {
              worksheet.getCell(
                "A" + beforeDataCount
              ).value = this.mainHeaderKeys[0];
              worksheet.getCell(
                "B" + beforeDataCount
              ).value = this.mainHeaderKeys[1];
              worksheet.getCell(
                "C" + beforeDataCount
              ).value = this.mainHeaderKeys[2];

              worksheet.getCell("A" + (beforeDataCount + 1)).value =
                header[this.mainHeaderKeys[0]];
              worksheet.getCell("B" + (beforeDataCount + 1)).value =
                header[this.mainHeaderKeys[1]];
              worksheet.getCell("C" + (beforeDataCount + 1)).value =
                header[this.mainHeaderKeys[2]];

              worksheet.getCell("A" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("B" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("C" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("A" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              worksheet.getCell("B" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              worksheet.getCell("C" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
            }
            beforeDataCount = rowsData.length + 4 + beforeDataCount;
          }

          let headerRow = worksheet.addRow(subHeader);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: "EEECE1" },
            };
            cell.font = {
              color: { argb: "000000" },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
          });

          // Add Data and Conditional Formatting
          rowsData.map((element, index) => {
            let eachRow = [];
            this.columns.map((headers, key) => {
              if (headers == "User Level" || headers == "Status") {
                if (element[headers]) {
                  this.translate.get(element[headers]).subscribe((val) => {
                    eachRow.push(val);
                  });
                } else {
                  eachRow.push(element[headers]);
                }
              } else {
                eachRow.push(element[headers]);
              }
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (rowsData.length - 1 == index) {
                worksheet
                  .addRow(eachRow)
                  .eachCell({ includeEmpty: true }, (cell, number) => {
                    cell.border = {
                      bottom: { style: "medium", color: { argb: customColor } },
                    };
                    if (cell.value && cell.value.search("/") != -1) {
                      cell.numFmt = "mm/dd/yyyy";
                    }
                  });
              } else {
                worksheet.addRow(eachRow).eachCell((cell) => {
                  if (cell.value && cell.value.search("/") != -1) {
                    cell.numFmt = "mm/dd/yyyy";
                  }
                });
              }
            }
          });
        });
      }

      worksheet.addRow([]);
      //Generate Excel File with given name
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape");
      let finalY = 10;
      this.pdfData.map((data) => {
        var mainCol = [];
        data.mainColumns.map((item) => {
          if (item == "SITENAME") {
            item = "Site";
            mainCol.push(item);
          } else if (item == "AREANAME") {
            item = "Area";
            mainCol.push(item);
          } else if (item == "SHIFTNAME") {
            item = "Shift";
            mainCol.push(item);
          } else if (item == "OBSERVERNAME") {
            item = "Observer";
            mainCol.push(item);
          } else {
            mainCol.push(item);
          }
        });
        pdf.autoTable(mainCol, data.mainRows, {
          startY: finalY,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
        });
        finalY = pdf.previousAutoTable.finalY;
        pdf.autoTable(data.subColumns, data.subRows, {
          startY: finalY,
          headStyles: {
            fillColor: "#D8D8D8",
            textColor: "#000000",
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
        });
        finalY = pdf.previousAutoTable.finalY + 10;
      });
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }
}
