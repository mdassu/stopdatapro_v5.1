import { Component, OnInit, Input } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { Router } from "@angular/router";
import { ReportsService } from "src/app/_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";

@Component({
  selector: "app-sub-checklist-count",
  templateUrl: "./sub-checklist-count.component.html",
  styleUrls: ["./sub-checklist-count.component.css"],
})
export class SubChecklistCountComponent implements OnInit {
  @Input() reportId: any;
  @Input() subReportId: any;
  @Input() observerId: any;
  title: any;
  parameters: any;
  tableData: any = [];
  cols: any = [];
  isLoaded: boolean;
  errormsg: any = "";
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  tableCSVRows: any;
  columns: any;
  pdfData: any = [];
  mainHead: any;
  grandTotal: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.parameters["subReportId"] = this.subReportId;
      this.parameters["OBSERVERID"] = this.observerId;
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    this.getReports();
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getSubReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = res["data"];
        this.translate
          .get(["LBLSITENAME", "LBLOBSCHECKLIST"])
          .subscribe((resLabel) => {
            this.cols = [
              { field: "SITENAME", header: resLabel["LBLSITENAME"] },
              { field: "CHECKLISTCOUNT", header: resLabel["LBLOBSCHECKLIST"] },
            ];
          });
        this.manageExportData();
        this.isLoaded = true;
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  manageExportData(): void {
    this.tableRows = [];
    this.tableCSVRows = [];
    this.pdfData = [];
    this.columns = [];
    // if (this.selectedReportView == 22) {
    // columns.map(item => {
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.grandTotal = 0;
      this.tableData.map((data, key) => {
        this.grandTotal = this.grandTotal + data["CHECKLISTCOUNT"];
        var dataJson = {};
        var tempRows = [];
        this.cols.map((item, index) => {
          dataJson[item["header"]] = data[item["field"]];

          tempRows.push(data[item["field"]]);
          if (this.tableData.length - 1 == key) {
            this.columns.push(item.header);
          }
        });
        this.tableRows.push(dataJson);
        this.tableCSVRows.push(dataJson);
        this.pdfData.push(tempRows);
        this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
          var totalJson = [label, this.grandTotal];
          this.pdfData.push(totalJson);
        });
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    var newData = json;
    this.translate
      .get(["LBLGRANDTOTAL", "LBLSITENAME", "LBLOBSCHECKLIST"])
      .subscribe((label) => {
        var jn = {};
        jn[label["LBLSITENAME"]] = label["LBLGRANDTOTAL"];
        jn[label["LBLOBSCHECKLIST"]] = this.grandTotal;
        newData.push(jn);
      });
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(newData);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
    this.tableRows.pop();
  }

  exportData(): void {
    const fileName = "CHECKLISTCOUNT";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      this.exportAsExcelFile(this.tableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      let worksheet = workbook.addWorksheet("Sheet1");

      //Add Header Row
      let headerRow = worksheet.addRow(header);
      headerRow.height = 20;
      // Cell Style : Fill and Border
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: customColor },
        };
        cell.font = {
          color: { argb: customFontColor },
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
        if (number == 2) {
          cell.alignment = { vertical: "middle", horizontal: "center" };
        } else {
          cell.alignment = { vertical: "middle", horizontal: "left" };
        }
      });
      // Add Data and Conditional Formatting
      let sumOfData = 0;
      data.forEach((element) => {
        let eachRow = [];
        this.columns.map((headers, key) => {
          if (key == 1) {
            sumOfData = sumOfData + element[headers];
          }
          eachRow.push(element[headers]);
        });
        if (element.isDeleted === "Y") {
          let deletedRow = worksheet.addRow(eachRow);
          deletedRow.eachCell((cell, number) => {
            cell.font = {
              name: "Calibri",
              family: 4,
              size: 11,
              bold: false,
              strike: true,
            };
          });
        } else {
          var addedRow = worksheet.addRow(eachRow);
          addedRow.getCell(2).alignment = {
            vertical: "middle",
            horizontal: "center",
          };
          if (this.selectedExport == 5) {
            addedRow.getCell(2).numFmt = "0";
          }
        }
      });
      this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
        let finalRow = worksheet.addRow([label, sumOfData]);
        finalRow.getCell(2).alignment = {
          vertical: "middle",
          horizontal: "center",
        };
        if (this.selectedExport == 5) {
          finalRow.getCell(2).numFmt = "0";
        }
        finalRow.eachCell((cell) => {
          cell.font = {
            size: 12,
            bold: true,
          };
          cell.border = {
            top: { style: "thin", color: { argb: customColor } },
            // left: { style: "thin", color: {argb: customColor} },
            bottom: { style: "thin", color: { argb: customColor } },
            // right: { style: "thin", color: {argb: customColor} },
          };
        });
      });
      worksheet.getColumn(1).width = 40;
      worksheet.getColumn(2).width = 30;
      worksheet.addRow([]);
      //Generate Excel File with given name
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape");
      let fn = this;
      pdf.autoTable(this.columns, this.pdfData, {
        startY: 10,
        headStyles: {
          fillColor: customColor,
          textColor: customFontColor,
        },
        alternateRowStyles: {
          fillColor: "#FFFFFF",
        },
        willDrawCell: this.drawCell,
        didParseCell: (data) => {
          fn.alignCol(data, customColor, fn.pdfData.length);
        },
      });
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }

  alignCol = function (data, customColor, rowCount) {
    if (data.column.index == 1) {
      data.cell.styles.halign = "center";
    }
    var s = data.cell.styles;
    s.lineColor = [255, 255, 255];
    s.lineWidth = 0.1;
    // s.font = "helvetica";
    // s.fillColor = [255, 255, 255];
    if (data.row.index == rowCount - 1) {
      s.lineColor = "#" + customColor;
      // s.lineWidth = 0.5;
      s.borders = "t";
    }
  };

  drawCell = function (data) {
    var doc = data.doc;
    var rows = data.table.body;
    var col = data.column.index;
    if (col == 2) {
      data.cell.styles.halign = "center";
    }
    if (rows.length === 1) {
    } else if (data.row.index === rows.length - 1) {
      doc.setFontStyle("bold");
      doc.setFontSize("12");
    }
  };
}
