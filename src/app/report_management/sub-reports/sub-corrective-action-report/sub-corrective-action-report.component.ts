import { Component, OnInit, Input, ɵConsole } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { Router } from "@angular/router";
import { ReportsService } from "src/app/_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";

@Component({
  selector: "app-sub-corrective-action-report",
  templateUrl: "./sub-corrective-action-report.component.html",
  styleUrls: ["./sub-corrective-action-report.component.css"],
})
export class SubCorrectiveActionReportComponent implements OnInit {
  @Input() reportId: any;
  @Input() subReportId: any;
  @Input() correctionId: any;
  title: any;
  parameters: any;
  tableData: any = [];
  cols: any = [];
  isLoaded: boolean;
  errormsg: any = "";
  rowGroupMetadata: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  unFColumns: any;
  unFTableRows: any;
  pdfData: any = [];
  chart: any;
  dataKey: any = "SITENAME";
  mainHead: any;
  dateFormat: any;
  subAreaOption: number;
  colleps: number = 0;
  selectedCorrectionId: any = "";
  headName: any;
  displaySubReport: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.subAreaOption = this.auth.UserInfo["subAreaOptionValue"];
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.parameters["subReportId"] = this.subReportId;
      if (this.subReportId == 14) {
        this.parameters["RESPONSESTATUSGENERAL"] = this.correctionId;
      } else {
        this.parameters["RESPONSESTATUS"] = this.correctionId;
      }
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    this.getReports();
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
    this.dateFormat = this.auth.UserInfo["dateFormat"];
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getSubReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = [];
        this.tableData = res["data"];

        if (this.subReportId == 14) {
          this.translate
            .get([
              "LBLMAINCATEGORY",
              "LBLSUBCATNAME",
              "LBLSAFE",
              "LBLUNSAFE",
              "LBLSAFECOMMENTS",
              "LBLUNSAFECOMMENTS",
            ])
            .subscribe((resLabel) => {
              this.cols = [
                {
                  field: "MAINCATEGORYNAME",
                  header: resLabel["LBLMAINCATEGORY"],
                },
                { field: "SUBCATEGORYNAME", header: resLabel["LBLSUBCATNAME"] },
                { field: "SAFE", header: resLabel["LBLSAFE"] },
                { field: "UNSAFE", header: resLabel["LBLUNSAFE"] },
                { field: "SAFECOMMENTS", header: resLabel["LBLSAFECOMMENTS"] },
                {
                  field: "UNSAFECOMMENTS",
                  header: resLabel["LBLUNSAFECOMMENTS"],
                },
              ];
            });
        } else {
          this.translate
            .get([
              "LBLCAID",
              "LBLOBSDATE",
              "LBLRESPONSIBLEPERSON",
              "LBLACTOWNER",
              "LBLRESPONSEREQUIREDDATE",
              "LBLRESPONDEDDATE",
              "LBLPRIORITY",
              "LBLSTATUS",
              "LBLACTIONPLANNED",
              "LBLACTIONPERFORMED",
              "LBLENTEREDBY",
            ])
            .subscribe((resLabel) => {
              this.cols = [
                { field: "CORRACTIONID", header: resLabel["LBLCAID"] },
                { field: "OBSERVATIONDATE", header: resLabel["LBLOBSDATE"] },
                {
                  field: "RESPONSIBLEPERSON",
                  header: resLabel["LBLRESPONSIBLEPERSON"],
                },
                { field: "ACTIONOWNER", header: resLabel["LBLACTOWNER"] },
                {
                  field: "RESPONSEREQUIREDDATE",
                  header: resLabel["LBLRESPONSEREQUIREDDATE"],
                },
                { field: "RESPONSEDATE", header: resLabel["LBLRESPONDEDDATE"] },
                { field: "PRIORITY", header: resLabel["LBLPRIORITY"] },
                { field: "CARDSTATUS", header: resLabel["LBLSTATUS"] },
                {
                  field: "ACTIONPLANNED",
                  header: resLabel["LBLACTIONPLANNED"],
                },
                {
                  field: "ACTIONPERFORMED",
                  header: resLabel["LBLACTIONPERFORMED"],
                },
                { field: "ENTEREDBY", header: resLabel["LBLENTEREDBY"] },
              ];
            });
        }
        this.updateRowGroupMetaData();
        this.manageExportData();
        this.isLoaded = true;
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    this.mainHead = [];
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let rowData = this.tableData[i];
        let sitename = rowData.SITENAME;
        if (this.subReportId == 14) {
          var regexHash = new RegExp("<BR>", "g");
          if (
            rowData.OBSERVERNAME &&
            rowData.OBSERVERNAME.search("<BR>") != -1
          ) {
            rowData.OBSERVERNAME = rowData.OBSERVERNAME.replace(
              regexHash,
              "\n"
            );
          }
          rowData.OBSERVERNAME = rowData.OBSERVERNAME.replace(regexHash, "\n");
        }
        if (i == 0) {
          this.rowGroupMetadata[sitename] = { index: 0, size: 1 };
          if (this.subReportId == 14) {
            if (this.subAreaOption == 1) {
              this.mainHead.push({
                SITENAME: rowData["SITENAME"],
                OBSERVERNAME: rowData["OBSERVERNAME"],
                AREANAME: rowData["AREANAME"],
                SUBAREANAME: rowData["SUBAREANAME"],
                SHIFTNAME: rowData["SHIFTNAME"],
                CARDID: rowData["CARDID"],
              });
            } else {
              this.colleps = 1;
              this.mainHead.push({
                SITENAME: rowData["SITENAME"],
                OBSERVERNAME: rowData["OBSERVERNAME"],
                AREANAME: rowData["AREANAME"],
                SHIFTNAME: rowData["SHIFTNAME"],
                CARDID: rowData["CARDID"],
              });
            }
          } else {
            this.mainHead.push({
              SITENAME: rowData["SITENAME"],
            });
          }
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.SITENAME;
          if (sitename === previousRowGroup) {
            this.rowGroupMetadata[sitename].size++;
          } else {
            this.rowGroupMetadata[sitename] = { index: i, size: 1 };
            if (this.subReportId == 14) {
              if (this.subAreaOption == 1) {
                this.mainHead.push({
                  SITENAME: rowData["SITENAME"],
                  OBSERVERNAME: rowData["OBSERVERNAME"],
                  AREANAME: rowData["AREANAME"],
                  SUBAREANAME: rowData["SUBAREANAME"],
                  SHIFTNAME: rowData["SHIFTNAME"],
                  CARDID: rowData["CARDID"],
                });
              } else {
                this.colleps = 1;
                this.mainHead.push({
                  SITENAME: rowData["SITENAME"],
                  OBSERVERNAME: rowData["OBSERVERNAME"],
                  AREANAME: rowData["AREANAME"],
                  SHIFTNAME: rowData["SHIFTNAME"],
                  CARDID: rowData["CARDID"],
                });
              }
            } else {
              this.mainHead.push({
                SITENAME: rowData["SITENAME"],
              });
            }
          }
        }
      }
    }
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    if (this.tableData.length > 0 && this.cols.length > 0) {
      // get data for Excel
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempDataJson = {};
        this.translate
          .get([
            "LBLSITENAME",
            "LBLOBSERVERNAME",
            "LBLAREANAME",
            "LBLSUBAREANAME",
            "LBLSHIFTNAME",
            "LBLCHECKLISTNO",
          ])
          .subscribe((resLebel) => {
            if (this.subReportId == 14) {
              if (key == 0) {
                this.unFColumns.push(resLebel["LBLSITENAME"]);
                this.unFColumns.push(resLebel["LBLOBSERVERNAME"]);
                this.unFColumns.push(resLebel["LBLAREANAME"]);
                if (this.subAreaOption == 1) {
                  this.unFColumns.push(resLebel["LBLSUBAREANAME"]);
                }
                this.unFColumns.push(resLebel["LBLSHIFTNAME"]);
                this.unFColumns.push(resLebel["LBLCHECKLISTNO"]);
              }
              tempDataJson[resLebel["LBLSITENAME"]] = data["SITENAME"];
              tempDataJson[resLebel["LBLOBSERVERNAME"]] = data["OBSERVERNAME"];
              tempDataJson[resLebel["LBLAREANAME"]] = data["AREANAME"];
              if (this.subAreaOption == 1) {
                tempDataJson[resLebel["LBLSUBAREANAME"]] = data["SUBAREANAME"];
              }
              tempDataJson[resLebel["LBLSHIFTNAME"]] = data["SHIFTNAME"];
              tempDataJson[resLebel["LBLCHECKLISTNO"]] = data["CARDID"];
              // this.unFTableRows.push(tempDataJson);
            } else {
              if (key == 0) {
                this.unFColumns.push(resLebel["LBLSITENAME"]);
              }
              tempDataJson[resLebel["LBLSITENAME"]] = data["SITENAME"];
              // this.unFTableRows.push(tempDataJson);
            }
          });
        // var tempRows = [];
        this.cols.map((item, index) => {
          if (item["field"] == "RESPONSIBLEPERSON") {
            var regexHash = new RegExp("<BR>", "g");
            if (
              data[item["field"]] &&
              data[item["field"]].search("<BR>") != -1
            ) {
              data[item["field"]] = data[item["field"]].replace(
                regexHash,
                "\n"
              );
            }
          }
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];

          // tempRows.push(data[item["field"]]);
          if (this.tableData.length - 1 == key) {
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
          }
        });

        if (this.subReportId == 14) {
          dataJson["SITENAME"] = data["SITENAME"];
          dataJson["OBSERVERNAME"] = data["OBSERVERNAME"];
          dataJson["AREANAME"] = data["AREANAME"];
          if (this.subAreaOption == 1) {
            dataJson["SUBAREANAME"] = data["SUBAREANAME"];
          }
          dataJson["SHIFTNAME"] = data["SHIFTNAME"];
          dataJson["CARDID"] = data["CARDID"];
          this.tableRows.push(dataJson);
          this.unFTableRows.push(tempDataJson);
        } else {
          dataJson["SITENAME"] = data["SITENAME"];
          this.tableRows.push(dataJson);
          this.unFTableRows.push(tempDataJson);
        }
      });

      // get data for PDF
      var mainColumns = [];
      this.translate
        .get([
          "LBLSITENAME",
          "LBLOBSERVERNAME",
          "LBLAREANAME",
          "LBLSUBAREAFULLNAME",
          "LBLSHIFTNAME",
          "LBLCHECKLISTNO",
        ])
        .subscribe((resLabel) => {
          if (this.subReportId == 14) {
            mainColumns.push(resLabel["LBLSITENAME"]);
            mainColumns.push(resLabel["LBLOBSERVERNAME"]);
            mainColumns.push(resLabel["LBLAREANAME"]);
            if (this.subAreaOption == 1) {
              mainColumns.push(resLabel["LBLSUBAREAFULLNAME"]);
            }
            mainColumns.push(resLabel["LBLSHIFTNAME"]);
            mainColumns.push(resLabel["LBLCHECKLISTNO"]);
          } else {
            mainColumns.push(resLabel["LBLSITENAME"]);
          }
        });
      var tempMainColums = Object.keys(this.mainHead[0]);
      var subColumns = this.columns;
      this.mainHead.map((head) => {
        var mainRows = [];
        var subRows = [];
        // main Table
        var tempMainRows = [];
        tempMainColums.map((col) => {
          tempMainRows.push(head[col]);
        });
        mainRows.push(tempMainRows);

        // sub Table
        this.tableData.map((data) => {
          var tempSubRows = [];
          if (data["SITENAME"] == head["SITENAME"]) {
            this.cols.map((subCol) => {
              if (subCol.field == "PRIORITY" || subCol.field == "CARDSTATUS") {
                this.translate.get(data[subCol.field]).subscribe((val) => {
                  tempSubRows.push(val);
                });
              } else {
                tempSubRows.push(data[subCol.field]);
              }
            });
            subRows.push(tempSubRows);
          }
        });
        this.pdfData.push({
          mainColumns: mainColumns,
          mainRows: mainRows,
          subColumns: subColumns,
          subRows: subRows,
        });
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    var fileName = "";
    if (this.subReportId == 14) {
      fileName = "CORRECTIVEACTIONGENERAL";
    } else {
      fileName = "CORRECTIVEACTIONGRAPHICALSUBREPORT";
    }
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      this.translate.get(["LBLPRIORITY", "LBLSTATUS"]).subscribe((label) => {
        this.unFTableRows.map((item) => {
          this.translate
            .get([item[label["LBLPRIORITY"]], item[label["LBLSTATUS"]]])
            .subscribe((resLabel) => {
              item[label["LBLPRIORITY"]] = resLabel[item[label["LBLPRIORITY"]]];
              item[label["LBLSTATUS"]] = resLabel[item[label["LBLSTATUS"]]];
            });
        });
      });
      this.exportAsExcelFile(this.unFTableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;
      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      let worksheet = workbook.addWorksheet("Sheet1");

      if (this.selectedExport == 4 || this.selectedExport == 5) {
        for (var i = 1; i <= 15; i++) {
          worksheet.getColumn(i).width = 25;
        }
        let headerRow = worksheet.addRow(unFColumns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          // if (
          //   this.subAreaOption == 1 &&
          //   (number == 6 || number == 9 || number == 10)
          // ) {
          //   cell.alignment = {
          //     vertical: "middle",
          //     horizontal: "center",
          //   };
          // } else if (
          //   this.subAreaOption != 1 &&
          //   (number == 5 || number == 8 || number == 9)
          // ) {
          //   cell.alignment = {
          //     vertical: "middle",
          //     horizontal: "center",
          //   };
          // }
        });
        // Add Data and Conditional Formatting
        unFTableRows.map((element, index) => {
          let eachRow = [];
          unFColumns.map((headers, key) => {
            this.translate
              .get(["LBLPRIORITY", "LBLSTATUS"])
              .subscribe((resLabel) => {
                if (
                  headers == resLabel["LBLPRIORITY"] ||
                  headers == resLabel["LBLSTATUS"]
                ) {
                  if (element[headers]) {
                    this.translate.get(element[headers]).subscribe((val) => {
                      eachRow.push(val);
                    });
                  } else {
                    eachRow.push(element[headers]);
                  }
                } else {
                  eachRow.push(element[headers]);
                }
              });
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            if (unFTableRows.length - 1 == index) {
              let addedRow = worksheet.addRow(eachRow);
              addedRow.eachCell({ includeEmpty: true }, (cell, number) => {
                // cell.border = {
                //   bottom: {
                //     style: "medium",
                //     color: { argb: customColor },
                //   },
                // };
                // if (
                //   this.subAreaOption == 1 &&
                //   (number == 6 || number == 9 || number == 10)
                // ) {
                //   cell.alignment = {
                //     vertical: "middle",
                //     horizontal: "center",
                //   };
                // } else if (
                //   this.subAreaOption != 1 &&
                //   (number == 5 || number == 8 || number == 9)
                // ) {
                //   cell.alignment = {
                //     vertical: "middle",
                //     horizontal: "center",
                //   };
                // }
                cell.numFmt = this.auth.changeCellValueType(cell.value);
              });
            } else {
              worksheet.addRow(eachRow).eachCell((cell, number) => {
                // if (
                //   this.subAreaOption == 1 &&
                //   (number == 6 || number == 9 || number == 10)
                // ) {
                //   cell.alignment = {
                //     vertical: "middle",
                //     horizontal: "center",
                //   };
                // } else if (
                //   this.subAreaOption != 1 &&
                //   (number == 5 || number == 8 || number == 9)
                // ) {
                //   cell.alignment = {
                //     vertical: "middle",
                //     horizontal: "center",
                //   };
                // }
                cell.numFmt = this.auth.changeCellValueType(cell.value);
              });
            }
          }
        });
      } else {
        let subHeader = header;
        var beforeDataCount = 1;
        this.mainHead.map((header, key) => {
          var rowsData = data.filter((item) => {
            return item[this.dataKey] == header[this.dataKey];
          });
          for (var i = 1; i <= 15; i++) {
            worksheet.getColumn(i).width = 25;
          }
          if (key == 0) {
            this.translate
              .get([
                "LBLSITENAME",
                "LBLOBSERVERNAME",
                "LBLAREANAME",
                "LBLSUBAREAFULLNAME",
                "LBLSHIFTNAME",
                "LBLCHECKLISTNO",
              ])
              .subscribe((resLabel) => {
                if (this.subReportId == 14) {
                  if (this.subAreaOption == 1) {
                    worksheet.getCell("A" + beforeDataCount).value =
                      resLabel["LBLSITENAME"];
                    worksheet.getCell("B" + beforeDataCount).value =
                      resLabel["LBLOBSERVERNAME"];
                    worksheet.getCell("C" + beforeDataCount).value =
                      resLabel["LBLAREANAME"];
                    worksheet.getCell("D" + beforeDataCount).value =
                      resLabel["LBLSUBAREAFULLNAME"];
                    worksheet.getCell("E" + beforeDataCount).value =
                      resLabel["LBLSHIFTNAME"];
                    worksheet.getCell("F" + beforeDataCount).value =
                      resLabel["LBLCHECKLISTNO"];

                    worksheet.getCell("F" + beforeDataCount).alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };

                    worksheet.getCell("A" + (beforeDataCount + 1)).value =
                      header["SITENAME"];
                    worksheet.getCell("B" + (beforeDataCount + 1)).value =
                      header["OBSERVERNAME"];
                    worksheet.getCell("C" + (beforeDataCount + 1)).value =
                      header["AREANAME"];
                    worksheet.getCell("D" + (beforeDataCount + 1)).value =
                      header["SUBAREANAME"];
                    worksheet.getCell("E" + (beforeDataCount + 1)).value =
                      header["SHIFTNAME"];
                    worksheet.getCell("F" + (beforeDataCount + 1)).value =
                      header["CARDID"];

                    worksheet.getCell("F" + (beforeDataCount + 1)).alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                    worksheet.getCell("A" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("B" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("C" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("D" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("E" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("F" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("A" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("B" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("C" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("D" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("E" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("F" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                  } else {
                    worksheet.getCell("A" + beforeDataCount).value =
                      resLabel["LBLSITENAME"];
                    worksheet.getCell("B" + beforeDataCount).value =
                      resLabel["LBLOBSERVERNAME"];
                    worksheet.getCell("C" + beforeDataCount).value =
                      resLabel["LBLAREANAME"];
                    worksheet.getCell("D" + beforeDataCount).value =
                      resLabel["LBLSHIFTNAME"];
                    worksheet.getCell("E" + beforeDataCount).value =
                      resLabel["LBLCHECKLISTNO"];

                    worksheet.getCell("E" + beforeDataCount).alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };

                    worksheet.getCell("A" + (beforeDataCount + 1)).value =
                      header["SITENAME"];
                    worksheet.getCell("B" + (beforeDataCount + 1)).value =
                      header["OBSERVERNAME"];
                    worksheet.getCell("C" + (beforeDataCount + 1)).value =
                      header["AREANAME"];
                    worksheet.getCell("D" + (beforeDataCount + 1)).value =
                      header["SHIFTNAME"];
                    worksheet.getCell("E" + (beforeDataCount + 1)).value =
                      header["CARDID"];
                    worksheet.getCell("E" + (beforeDataCount + 1)).alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                    worksheet.getCell("A" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("B" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("C" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("D" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("E" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("A" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("B" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("C" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("D" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("E" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                  }
                } else {
                  worksheet.mergeCells(
                    "A" + beforeDataCount + ":K" + beforeDataCount
                  );
                  worksheet.mergeCells(
                    "A" + (beforeDataCount + 1) + ":K" + (beforeDataCount + 1)
                  );
                  worksheet.getCell("A" + beforeDataCount).value =
                    resLabel["LBLSITENAME"];

                  worksheet.getCell("A" + (beforeDataCount + 1)).value =
                    header["SITENAME"];
                  worksheet.getCell("A" + beforeDataCount).fill = {
                    type: "pattern",
                    pattern: "solid",
                    fgColor: { argb: customColor },
                  };
                  worksheet.getCell("A" + beforeDataCount).font = {
                    color: { argb: customFontColor },
                  };
                }
              });

            beforeDataCount = rowsData.length + 5;
          } else {
            this.translate
              .get([
                "LBLSITENAME",
                "LBLOBSERVERNAME",
                "LBLAREANAME",
                "LBLSUBAREAFULLNAME",
                "LBLSHIFTNAME",
                "LBLCHECKLISTNO",
              ])
              .subscribe((resLabel) => {
                if (this.subReportId == 14) {
                  if (this.subAreaOption == 1) {
                    worksheet.getCell("A" + beforeDataCount).value =
                      resLabel["LBLSITENAME"];
                    worksheet.getCell("B" + beforeDataCount).value =
                      resLabel["LBLOBSERVERNAME"];
                    worksheet.getCell("C" + beforeDataCount).value =
                      resLabel["LBLAREANAME"];
                    worksheet.getCell("D" + beforeDataCount).value =
                      resLabel["LBLSUBAREAFULLNAME"];
                    worksheet.getCell("E" + beforeDataCount).value =
                      resLabel["LBLSHIFTNAME"];
                    worksheet.getCell("F" + beforeDataCount).value =
                      resLabel["LBLCHECKLISTNO"];

                    workbook.getCell("F" + beforeDataCount).alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };

                    worksheet.getCell("A" + (beforeDataCount + 1)).value =
                      header["SITENAME"];
                    worksheet.getCell("B" + (beforeDataCount + 1)).value =
                      header["OBSERVERNAME"];
                    worksheet.getCell("C" + (beforeDataCount + 1)).value =
                      header["AREANAME"];
                    worksheet.getCell("D" + (beforeDataCount + 1)).value =
                      header["SUBAREANAME"];
                    worksheet.getCell("E" + (beforeDataCount + 1)).value =
                      header["SHIFTNAME"];
                    worksheet.getCell("F" + (beforeDataCount + 1)).value =
                      header["CARDID"];
                    worksheet.getCell("A" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("B" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("C" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("D" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("E" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("F" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("A" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("B" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("C" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("D" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("E" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("F" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                  } else {
                    worksheet.getCell("A" + beforeDataCount).value =
                      resLabel["LBLSITENAME"];
                    worksheet.getCell("B" + beforeDataCount).value =
                      resLabel["LBLOBSERVERNAME"];
                    worksheet.getCell("C" + beforeDataCount).value =
                      resLabel["LBLAREANAME"];
                    worksheet.getCell("D" + beforeDataCount).value =
                      resLabel["LBLSHIFTNAME"];
                    worksheet.getCell("E" + beforeDataCount).value =
                      resLabel["LBLCHECKLISTNO"];
                    workbook.getCell("E" + beforeDataCount).alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                    worksheet.getCell("A" + (beforeDataCount + 1)).value =
                      header["SITENAME"];
                    worksheet.getCell("B" + (beforeDataCount + 1)).value =
                      header["OBSERVERNAME"];
                    worksheet.getCell("C" + (beforeDataCount + 1)).value =
                      header["AREANAME"];
                    worksheet.getCell("D" + (beforeDataCount + 1)).value =
                      header["SHIFTNAME"];
                    worksheet.getCell("E" + (beforeDataCount + 1)).value =
                      header["CARDID"];
                    worksheet.getCell("A" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("B" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("C" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("D" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("E" + beforeDataCount).fill = {
                      type: "pattern",
                      pattern: "solid",
                      fgColor: { argb: customColor },
                    };
                    worksheet.getCell("A" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("B" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("C" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("D" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                    worksheet.getCell("E" + beforeDataCount).font = {
                      color: { argb: customFontColor },
                    };
                  }
                } else {
                  worksheet.mergeCells(
                    "A" + beforeDataCount + ":K" + beforeDataCount
                  );
                  worksheet.mergeCells(
                    "A" + (beforeDataCount + 1) + ":K" + (beforeDataCount + 1)
                  );
                  worksheet.getCell("A" + beforeDataCount).value =
                    resLabel["LBLSITENAME"];

                  worksheet.getCell("A" + (beforeDataCount + 1)).value =
                    header["SITENAME"];
                  worksheet.getCell("A" + beforeDataCount).fill = {
                    type: "pattern",
                    pattern: "solid",
                    fgColor: { argb: customColor },
                  };
                  worksheet.getCell("A" + beforeDataCount).font = {
                    color: { argb: customFontColor },
                  };
                }
              });
            beforeDataCount = rowsData.length + 4 + beforeDataCount;
          }

          let headerRow = worksheet.addRow(subHeader);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: "EEECE1" },
            };
            cell.font = {
              color: { argb: "000000" },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
            if (number == 3 || number == 4) {
              cell.alignment = {
                vertical: "middle",
                horizontal: "center",
              };
            }
          });

          // Add Data and Conditional Formatting
          rowsData.map((element, index) => {
            let eachRow = [];
            this.columns.map((headers, key) => {
              this.translate
                .get(["LBLPRIORITY", "LBLSTATUS"])
                .subscribe((resLabel) => {
                  if (
                    headers == resLabel["LBLPRIORITY"] ||
                    headers == resLabel["LBLSTATUS"]
                  ) {
                    if (element[headers]) {
                      this.translate.get(element[headers]).subscribe((val) => {
                        eachRow.push(val);
                      });
                    } else {
                      eachRow.push(element[headers]);
                    }
                  } else {
                    eachRow.push(element[headers]);
                  }
                });
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (rowsData.length - 1 == index) {
                let addedRow = worksheet.addRow(eachRow);
                addedRow.eachCell({ includeEmpty: true }, (cell, number) => {
                  cell.border = {
                    bottom: {
                      style: "medium",
                      color: { argb: customColor },
                    },
                  };
                  if (number == 3 || number == 4) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                    cell.numFmt = "0";
                  }
                });
              } else {
                worksheet.addRow(eachRow).eachCell((cell, number) => {
                  if (number == 3 || number == 4) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                    cell.numFmt = "0";
                  }
                });
              }
            }
          });
        });
      }

      worksheet.addRow([]);
      //Generate Excel File with given name
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape", "mm", [1200, 800]);
      let finalY = 10;
      var fn = this;
      this.pdfData.map((data) => {
        pdf.autoTable(data.mainColumns, data.mainRows, {
          startY: finalY,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          didParseCell: (colData) => {
            fn.mainHeadAlignCol(colData, data.mainColumns.length);
          },
        });
        finalY = pdf.previousAutoTable.finalY;
        pdf.autoTable(data.subColumns, data.subRows, {
          startY: finalY,
          headStyles: {
            fillColor: "#D8D8D8",
            textColor: "#000000",
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          didParseCell: (data) => {
            fn.alignCol(data);
          },
        });
        finalY = pdf.previousAutoTable.finalY + 10;
      });
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }

  alignCol = function (data) {
    if (data.column.index == 2 || data.column.index == 3) {
      data.cell.styles.halign = "center";
    }
  };

  mainHeadAlignCol = function (data, colCount) {
    if (data.column.index == colCount - 1) {
      data.cell.styles.halign = "center";
    }
  };

  openSubReport(corractionId, headName) {
    this.selectedCorrectionId = corractionId;
    this.headName = corractionId;
    this.displaySubReport = true;
  }
}
