import { Component, OnInit, Input } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { Router } from "@angular/router";
import { ReportsService } from "src/app/_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";

@Component({
  selector: "app-sub-red-flag",
  templateUrl: "./sub-red-flag.component.html",
  styleUrls: ["./sub-red-flag.component.css"],
})
export class SubRedFlagComponent implements OnInit {
  @Input() reportId: any;
  @Input() subReportId: any;
  @Input() flagId: any;
  title: any;
  parameters: any;
  tableData: any = [];
  cols: any = [];
  isLoaded: boolean;
  errormsg: any = "";
  rowGroupMetadata: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  unFTableRows: any;
  unFColumns: any;
  pdfData: any = [];
  dataKey: any = "SITENAME";
  mainHead: any;
  subAreaOption: number;
  colleps: number = 0;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.parameters["subReportId"] = this.subReportId;
      this.parameters["REDFLAGID"] = this.flagId;
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    this.getReports();

    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getSubReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = [];
        this.tableData = res["data"];

        this.translate
          .get([
            "LBLOBSERVERNAME",
            "LBLAREANAME",
            "LBLSUBAREAFULLNAME",
            "LBLMAINCATEGORY",
            "LBLSUBCATNAME",
          ])
          .subscribe((resLabel) => {
            this.subAreaOption = this.auth.UserInfo["subAreaOptionValue"];
            if (this.subAreaOption == 0) {
              this.colleps = 1;
              this.cols = [
                { field: "OBSERVERNAME", header: resLabel["LBLOBSERVERNAME"] },
                { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                {
                  field: "MAINCATEGORYNAME",
                  header: resLabel["LBLMAINCATEGORY"],
                },
                { field: "SUBCATEGORYNAME", header: resLabel["LBLSUBCATNAME"] },
              ];
            } else {
              this.colleps = 0;
              this.cols = [
                { field: "OBSERVERNAME", header: resLabel["LBLOBSERVERNAME"] },
                { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                {
                  field: "SUBAREANAME",
                  header: resLabel["LBLSUBAREAFULLNAME"],
                },
                {
                  field: "MAINCATEGORYNAME",
                  header: resLabel["LBLMAINCATEGORY"],
                },
                { field: "SUBCATEGORYNAME", header: resLabel["LBLSUBCATNAME"] },
              ];
            }
          });
        this.updateRowGroupMetaData();
        this.manageExportData();
        this.isLoaded = true;
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    this.mainHead = [];
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let rowData = this.tableData[i];
        let sitename = rowData.SITENAME;
        if (i == 0) {
          this.rowGroupMetadata[sitename] = { index: 0, size: 1 };
          this.mainHead.push({
            SITENAME: sitename,
          });
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.SITENAME;
          if (sitename === previousRowGroup) {
            this.rowGroupMetadata[sitename].size++;
          } else {
            this.rowGroupMetadata[sitename] = { index: i, size: 1 };
            this.mainHead.push({
              SITENAME: sitename,
            });
          }
        }
      }
    }
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    // if (this.selectedReportView == 22) {
    // columns.map(item => {
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempDataJson = {};
        this.translate.get("LBLSITENAME").subscribe((label) => {
          if (key == 0) {
            this.unFColumns.push(label);
          }
          tempDataJson[label] = data["SITENAME"];
        });
        this.cols.map((item, index) => {
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];

          if (this.tableData.length - 1 == key) {
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
          }
        });
        dataJson["SITENAME"] = data["SITENAME"];
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
      });

      // get data for PDF
      var mainColumns = [];
      this.translate.get(["LBLSITENAME"]).subscribe((resLabel) => {
        mainColumns.push(resLabel["LBLSITENAME"]);
      });
      var tempMainColums = Object.keys(this.mainHead[0]);
      var subColumns = this.columns;
      this.mainHead.map((head) => {
        var mainRows = [];
        var subRows = [];
        // main Table
        var tempMainRows = [];
        tempMainColums.map((col) => {
          tempMainRows.push(head[col]);
        });
        mainRows.push(tempMainRows);

        // sub Table
        this.tableData.map((data) => {
          var tempSubRows = [];
          if (data["SITENAME"] == head["SITENAME"]) {
            this.cols.map((subCol, key) => {
              if (key >= 0 && key <= 4) {
                if (data[subCol.field]) {
                  this.translate.get(data[subCol.field]).subscribe((val) => {
                    tempSubRows.push(val);
                  });
                } else {
                  tempSubRows.push(data[subCol.field]);
                }
              } else {
                tempSubRows.push(data[subCol.field]);
              }
            });
            subRows.push(tempSubRows);
          }
        });
        this.pdfData.push({
          mainColumns: mainColumns,
          mainRows: mainRows,
          subColumns: subColumns,
          subRows: subRows,
        });
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    const fileName = "REDFLAG";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      // this.tableRows.map((item) => {
      //   if (item["Sub area Name"]) {
      //     this.translate
      //       .get([
      //         item["Observer Name"],
      //         item["Area Name"],
      //         item["Sub area Name"],
      //         item["Main Category"],
      //         item["Sub Category"],
      //       ])
      //       .subscribe((resLabel) => {
      //         item["Observer Name"] = resLabel[item["Observer Name"]];
      //         item["Area Name"] = resLabel[item["Area Name"]];
      //         item["Sub area Name"] = resLabel[item["Sub area Name"]];
      //         item["Main Category"] = resLabel[item["Main Category"]];
      //         item["Sub Category"] = resLabel[item["Sub Category"]];
      //       });
      //   } else {
      //     this.translate
      //       .get([
      //         item["Observer Name"],
      //         item["Area Name"],
      //         item["Main Category"],
      //         item["Sub Category"],
      //       ])
      //       .subscribe((resLabel) => {
      //         item["Observer Name"] = resLabel[item["Observer Name"]];
      //         item["Area Name"] = resLabel[item["Area Name"]];
      //         item["Main Category"] = resLabel[item["Main Category"]];
      //         item["Sub Category"] = resLabel[item["Sub Category"]];
      //       });
      //   }
      // });
      this.exportAsExcelFile(this.unFTableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const subHeader = this.columns;
      const data = this.tableRows;

      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      let worksheet = workbook.addWorksheet("Sheet1");

      if (this.selectedExport == 4 || this.selectedExport == 5) {
        for (var i = 1; i <= 10; i++) {
          worksheet.getColumn(i).width = 30;
        }

        let headerRow = worksheet.addRow(unFColumns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: "EEECE1" },
          };
          cell.font = {
            color: { argb: "000000" },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });

        // Add Data and Conditional Formatting
        unFTableRows.map((element, index) => {
          let eachRow = [];
          unFColumns.map((headers, key) => {
            if (key >= 0 && key <= 7) {
              this.translate.get(element[headers]).subscribe((val) => {
                eachRow.push(val);
              });
            } else {
              eachRow.push(element[headers]);
            }
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            if (unFTableRows.length - 1 == index) {
              worksheet.addRow(eachRow).border = {
                bottom: { style: "medium", color: { argb: customColor } },
              };
            } else {
              worksheet.addRow(eachRow);
            }
          }
        });
      } else {
        var mainHeader = Object.keys(this.rowGroupMetadata);
        var beforeDataCount = 1;
        mainHeader.map((header, key) => {
          var rowsData = data.filter((item) => {
            return item[this.dataKey] == header;
          });
          for (var i = 1; i <= 10; i++) {
            worksheet.getColumn(i).width = 30;
          }
          if (key == 0) {
            worksheet.mergeCells(
              "A" + beforeDataCount + ":E" + beforeDataCount
            );
            worksheet.mergeCells(
              "A" + (beforeDataCount + 1) + ":E" + (beforeDataCount + 1)
            );
            this.translate.get("LBLSITENAME").subscribe((label) => {
              worksheet.getCell("A" + beforeDataCount).value = label;
            });
            this.translate.get(header).subscribe((label) => {
              worksheet.getCell("A" + (beforeDataCount + 1)).value = label;
            });
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 5;
          } else {
            worksheet.mergeCells(
              "A" + beforeDataCount + ":E" + beforeDataCount
            );
            worksheet.mergeCells(
              "A" + (beforeDataCount + 1) + ":E" + (beforeDataCount + 1)
            );
            this.translate.get("LBLSITENAME").subscribe((label) => {
              worksheet.getCell("A" + beforeDataCount).value = label;
            });
            this.translate.get(header).subscribe((label) => {
              worksheet.getCell("A" + (beforeDataCount + 1)).value = label;
            });
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 4 + beforeDataCount;
          }

          let headerRow = worksheet.addRow(subHeader);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: "EEECE1" },
            };
            cell.font = {
              color: { argb: "000000" },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
          });

          // Add Data and Conditional Formatting
          rowsData.map((element, index) => {
            let eachRow = [];
            this.columns.map((headers, key) => {
              if (key >= 0 && key <= 7) {
                this.translate.get(element[headers]).subscribe((val) => {
                  eachRow.push(val);
                });
              } else {
                eachRow.push(element[headers]);
              }
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (rowsData.length - 1 == index) {
                worksheet.addRow(eachRow).border = {
                  bottom: { style: "medium", color: { argb: customColor } },
                };
              } else {
                worksheet.addRow(eachRow);
              }
            }
          });
        });
      }

      worksheet.addRow([]);
      //Generate Excel File with given name
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape");
      let finalY = 10;
      this.pdfData.map((data) => {
        pdf.autoTable(data.mainColumns, data.mainRows, {
          startY: finalY,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
        });
        finalY = pdf.previousAutoTable.finalY;
        pdf.autoTable(data.subColumns, data.subRows, {
          startY: finalY,
          headStyles: {
            fillColor: "#D8D8D8",
            textColor: "#000000",
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
        });
        finalY = pdf.previousAutoTable.finalY + 10;
      });
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }
}
