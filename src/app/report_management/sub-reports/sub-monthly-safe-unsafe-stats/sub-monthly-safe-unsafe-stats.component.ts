import { Component, OnInit, Input } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { Router } from "@angular/router";
import { ReportsService } from "src/app/_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";

@Component({
  selector: "app-sub-monthly-safe-unsafe-stats",
  templateUrl: "./sub-monthly-safe-unsafe-stats.component.html",
  styleUrls: ["./sub-monthly-safe-unsafe-stats.component.css"],
})
export class SubMonthlySafeUnsafeStatsComponent implements OnInit {
  @Input() reportId: any;
  @Input() subReportId: any;
  @Input() month: any;
  @Input() mainCategory: any;
  @Input() siteName: any;
  title: any;
  parameters: any;
  tableData: any = [];
  cols: any = [];
  isLoaded: boolean;
  errormsg: any = "";
  rowGroupMetadata: any;
  width: any;
  height: any;
  type: any;
  dataFormat: any;
  dataSource: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  pdfRows: any = [];
  chart: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.parameters["subReportId"] = this.subReportId;
      this.parameters["MONTH"] = this.month;
      this.parameters["MAINCATEGORYID"] = this.mainCategory;
      this.parameters["SITEID"] = this.siteName;
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    this.getReports();

    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getSubReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = [];
        this.tableData = res["data"];
        var subCategory = [];
        var safeArr = [];
        var unsafeArr = [];
        var singleData = [];
        var i = 1;
        this.tableData.map((data) => {
          this.translate
            .get([
              "LBLOBSERVATIONBYCATEGORYCHART",
              "LBLSUBCATNAME",
              "LBLSAFECOUNT",
              "LBLUNSAFECOUNT",
            ])
            .subscribe((res) => {
              subCategory.push({ label: data.SUBCATEGORYNAME });
              safeArr.push({ value: parseInt(data.SAFE, 10) });
              unsafeArr.push({ value: parseInt(data.UNSAFE, 10) });

              if (this.subReportId == 7) {
                if (this.tableData.length == i) {
                  const data = {
                    chart: {
                      caption: res["LBLOBSERVATIONBYCATEGORYCHART"],
                      xaxisname: res["LBLSUBCATNAME"],
                      xAxisNameFontBold: 1,
                      numvisibleplot: "24",
                      showvalues: "1",
                      rotateValues: "0",
                      labeldisplay: "auto",
                      theme: "fusion",
                      flatScrollBars: "0",
                    },
                    categories: [
                      {
                        category: subCategory,
                      },
                    ],
                    dataset: [
                      {
                        seriesname: res["LBLSAFECOUNT"],
                        data: safeArr,
                      },
                      {
                        seriesname: res["LBLUNSAFECOUNT"],
                        data: unsafeArr,
                      },
                    ],
                  };

                  this.width = "100%";
                  this.height = 400;
                  this.type = "scrollcolumn2d";
                  this.dataFormat = "json";
                  this.dataSource = data;

                  this.isLoaded = true;
                }
              } else if (this.subReportId == 10) {
                singleData.push({
                  label: data.SUBCATEGORYNAME,
                  value: parseInt(data.SAFE, 10),
                });
                if (this.tableData.length == i) {
                  const data = {
                    chart: {
                      caption: res["LBLOBSERVATIONBYCATEGORYCHART"],
                      xaxisname: res["LBLSUBCATNAME"],
                      xAxisNameFontBold: 1,
                      showvalues: "1",
                      rotateValues: "0",
                      labeldisplay: "auto",
                      theme: "fusion",
                      flatScrollBars: "0",
                    },
                    data: singleData,
                  };

                  this.width = "100%";
                  this.height = 400;
                  this.type = "column3d";
                  this.dataFormat = "json";
                  this.dataSource = data;

                  this.isLoaded = true;
                }
              } else {
                singleData.push({
                  label: data.SUBCATEGORYNAME,
                  value: parseInt(data.UNSAFE, 10),
                });
                if (this.tableData.length == i) {
                  const data = {
                    chart: {
                      caption: res["LBLOBSERVATIONBYCATEGORYCHART"],
                      xaxisname: res["LBLSUBCATNAME"],
                      xAxisNameFontBold: 1,
                      showvalues: "1",
                      rotateValues: "0",
                      labeldisplay: "auto",
                      theme: "fusion",
                      flatScrollBars: "0",
                    },
                    data: singleData,
                  };

                  this.width = "100%";
                  this.height = 400;
                  this.type = "column3d";
                  this.dataFormat = "json";
                  this.dataSource = data;

                  this.isLoaded = true;
                }
              }

              i++;
            });
        });
        this.translate
          .get(["LBLSUBCATNAME", "LBLSAFECOUNT", "LBLUNSAFECOUNT"])
          .subscribe((resLabel) => {
            if (this.subReportId == 7) {
              this.cols = [
                { field: "SUBCATEGORYNAME", header: resLabel["LBLSUBCATNAME"] },
                { field: "SAFE", header: resLabel["LBLSAFECOUNT"] },
                { field: "UNSAFE", header: resLabel["LBLUNSAFECOUNT"] },
              ];
            } else if (this.subReportId == 10) {
              this.cols = [
                { field: "SUBCATEGORYNAME", header: resLabel["LBLSUBCATNAME"] },
                { field: "SAFE", header: resLabel["LBLSAFECOUNT"] },
              ];
            } else {
              this.cols = [
                { field: "SUBCATEGORYNAME", header: resLabel["LBLSUBCATNAME"] },
                { field: "UNSAFE", header: resLabel["LBLUNSAFECOUNT"] },
              ];
            }
          });
        this.manageExportData();
        this.isLoaded = true;
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  initialized($event) {
    this.chart = $event.chart; // saving chart instance
  }

  manageExportData(): void {
    this.tableRows = [];
    this.pdfRows = [];
    this.columns = [];
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempRows = [];
        this.cols.map((item) => {
          dataJson[item["header"]] = data[item["field"]];
          // dataJson[this.cols[0].header] = data[this.cols[0].field];
          // dataJson[this.cols[1].header] = data[this.cols[1].field];
          tempRows.push(data[item["field"]]);
          if (this.tableData.length - 1 == key) {
            this.columns.push(item.header);
          }
        });
        this.tableRows.push(dataJson);
        this.pdfRows.push(tempRows);
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    var encodedData;
    this.chart.getSVGString((svg) => {
      this.reportsService.svgString2Image(
        svg,
        1030,
        524,
        "png",
        function (base64String) {
          encodedData = base64String;
        }
      );
    });
    const fileName = "MONTHLYSAFEUNSAFESTATS";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    setTimeout(() => {
      if (this.selectedExport == 6) {
        this.exportAsExcelFile(this.tableRows, fileName);
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
      } else if (
        this.selectedExport == 1 ||
        this.selectedExport == 3 ||
        this.selectedExport == 4 ||
        this.selectedExport == 5
      ) {
        var EXCEL_EXTENSION;
        if (this.selectedExport == 1 || this.selectedExport == 5) {
          EXCEL_EXTENSION = ".xlsx";
        } else if (this.selectedExport == 3 || this.selectedExport == 4) {
          EXCEL_EXTENSION = ".xls";
        }
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        //Excel Title, Header, Data
        const header = this.columns;
        const data = this.tableRows;
        const EXCEL_TYPE =
          "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";
        //Create workbook and worksheet
        let workbook = new ExcelJS.Workbook();
        let worksheet = workbook.addWorksheet("Sheet1");
        //Add Header Row
        if (this.selectedExport == 4 || this.selectedExport == 5) {
          worksheet.getColumn(1).width = 40;
          worksheet.getColumn(2).width = 40;
          worksheet.getColumn(3).width = 30;
          worksheet.getRow(1).height = 330;
          worksheet.mergeCells("A1:C1");
          var imageId = workbook.addImage({
            base64: encodedData,
            extension: "png",
          });
          worksheet.addImage(imageId, {
            tl: { col: 0, row: 0 },
            ext: { width: 1000, height: 400 },
          });
        }
        let headerRow = worksheet.addRow(header);
        headerRow.height = 20;
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          if (number > 1) {
            cell.alignment = { vertical: "middle", horizontal: "center" };
          } else {
            cell.alignment = { vertical: "middle", horizontal: "left" };
          }
        });
        // Add Data and Conditional Formatting
        let sumOfData = 0;
        data.forEach((element) => {
          let eachRow = [];
          this.columns.map((headers, key) => {
            if (key == 1) {
              sumOfData = sumOfData + element[headers];
            }
            eachRow.push(element[headers]);
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            worksheet.addRow(eachRow).eachCell((cell, number) => {
              if (number > 1) {
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              }
            });
          }
        });
        worksheet.getColumn(1).width = 40;
        worksheet.getColumn(2).width = 30;
        worksheet.addRow([]);
        //Generate Excel File with given name
        workbook.xlsx.writeBuffer().then((data) => {
          let blob = new Blob([data], { type: EXCEL_TYPE });
          fs.saveAs(
            blob,
            fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
          );
        });
      } else if (this.selectedExport == 2) {
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        var fn = this;
        var pdf = new jsPDF("landscape");
        pdf.addImage(encodedData, "PNG", 20, 0, 220, 120);
        pdf.autoTable(this.columns, this.pdfRows, {
          startY: 130,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          didParseCell: (data) => {
            fn.alignCol(data);
          },
        });
        pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
      }
      this.selectedExport = 0;
    }, 1000);
  }

  alignCol = function (data) {
    if (data.column.index > 0) {
      data.cell.styles.halign = "center";
    }
  };
}
