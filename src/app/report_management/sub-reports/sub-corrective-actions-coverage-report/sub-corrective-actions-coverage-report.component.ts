import { Component, OnInit, Input } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { Router } from "@angular/router";
import { ReportsService } from "src/app/_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";

@Component({
  selector: "app-sub-corrective-actions-coverage-report",
  templateUrl: "./sub-corrective-actions-coverage-report.component.html",
  styleUrls: ["./sub-corrective-actions-coverage-report.component.css"],
})
export class SubCorrectiveActionsCoverageReportComponent implements OnInit {
  @Input() reportId: any;
  @Input() subReportId: any;
  @Input() statusId: any;
  title: any;
  parameters: any;
  tableData: any = [];
  cols: any = [];
  isLoaded: boolean;
  errormsg: any = "";
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  pdfRows: any = [];
  subAreaOption: number;
  colleps: number = 0;
  dateFormat: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.dateFormat = this.auth.UserInfo["dateFormat"];
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.parameters["subReportId"] = this.subReportId;
      this.parameters["RESPONSESTATUS"] = this.statusId;
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    this.getReports();
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getSubReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = res["data"];
        this.translate
          .get([
            "LBLCHECKLISTNO",
            "LBLSITENAME",
            "LBLOBSDATE",
            "LBLOBSERVERNAME",
            "LBLAREANAME",
            "LBLSUBAREAFULLNAME",
            "LBLSHIFTNAME",
            "LBLMAINCATEGORY",
            "LBLSUBCATNAME",
            "LBLUNSAFE",
            "LBLUNSAFECOMMENTS",
            "LBLCAID",
            "LBLRESPONSIBLEPERSON",
            "LBLACTOWNER",
            "LBLRESPONSEREQUIREDDATE",
            "LBLRESPONDEDDATE",
            "LBLPRIORITY",
            "LBLSTATUS",
            "LBLACTIONPLANNED",
            "LBLACTIONPERFORMED",
          ])
          .subscribe((resLabel) => {
            this.subAreaOption = this.auth.UserInfo["subAreaOptionValue"];
            if (this.subAreaOption == 0) {
              this.colleps = 1;
              this.cols = [
                { field: "CARDID", header: resLabel["LBLCHECKLISTNO"] },
                { field: "SITENAME", header: resLabel["LBLSITENAME"] },
                { field: "OBSERVATIONDATE", header: resLabel["LBLOBSDATE"] },
                { field: "OBSERVERS", header: resLabel["LBLOBSERVERNAME"] },
                { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                { field: "SHIFTNAME", header: resLabel["LBLSHIFTNAME"] },
                {
                  field: "MAINCATEGORYNAME",
                  header: resLabel["LBLMAINCATEGORY"],
                },
                { field: "SUBCATEGORYNAME", header: resLabel["LBLSUBCATNAME"] },
                { field: "UNSAFE", header: resLabel["LBLUNSAFE"] },
                {
                  field: "UNSAFECOMMENTS",
                  header: resLabel["LBLUNSAFECOMMENTS"],
                },
                { field: "CORRACTIONID", header: resLabel["LBLCAID"] },
                {
                  field: "RESPONSIBLEPERSON",
                  header: resLabel["LBLRESPONSIBLEPERSON"],
                },
                { field: "ACTIONOWNER", header: resLabel["LBLACTOWNER"] },
                {
                  field: "RESPONSEREQUIREDDATE",
                  header: resLabel["LBLRESPONSEREQUIREDDATE"],
                },
                { field: "RESPONSEDATE", header: resLabel["LBLRESPONDEDDATE"] },
                { field: "PRIORITY", header: resLabel["LBLPRIORITY"] },
                { field: "CARDSTATUS", header: resLabel["LBLSTATUS"] },
                {
                  field: "ACTIONPLANNED",
                  header: resLabel["LBLACTIONPLANNED"],
                },
                {
                  field: "ACTIONPERFORMED",
                  header: resLabel["LBLACTIONPERFORMED"],
                },
              ];
            } else {
              this.cols = [
                { field: "CARDID", header: resLabel["LBLCHECKLISTNO"] },
                { field: "SITENAME", header: resLabel["LBLSITENAME"] },
                { field: "OBSERVATIONDATE", header: resLabel["LBLOBSDATE"] },
                { field: "OBSERVERS", header: resLabel["LBLOBSERVERNAME"] },
                { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                {
                  field: "SUBAREANAME",
                  header: resLabel["LBLSUBAREAFULLNAME"],
                },
                { field: "SHIFTNAME", header: resLabel["LBLSHIFTNAME"] },
                {
                  field: "MAINCATEGORYNAME",
                  header: resLabel["LBLMAINCATEGORY"],
                },
                { field: "SUBCATEGORYNAME", header: resLabel["LBLSUBCATNAME"] },
                { field: "UNSAFE", header: resLabel["LBLUNSAFE"] },
                {
                  field: "UNSAFECOMMENTS",
                  header: resLabel["LBLUNSAFECOMMENTS"],
                },
                { field: "CORRACTIONID", header: resLabel["LBLCAID"] },
                {
                  field: "RESPONSIBLEPERSON",
                  header: resLabel["LBLRESPONSIBLEPERSON"],
                },
                { field: "ACTIONOWNER", header: resLabel["LBLACTOWNER"] },
                {
                  field: "RESPONSEREQUIREDDATE",
                  header: resLabel["LBLRESPONSEREQUIREDDATE"],
                },
                { field: "RESPONSEDATE", header: resLabel["LBLRESPONDEDDATE"] },
                { field: "PRIORITY", header: resLabel["LBLPRIORITY"] },
                { field: "CARDSTATUS", header: resLabel["LBLSTATUS"] },
                {
                  field: "ACTIONPLANNED",
                  header: resLabel["LBLACTIONPLANNED"],
                },
                {
                  field: "ACTIONPERFORMED",
                  header: resLabel["LBLACTIONPERFORMED"],
                },
              ];
            }
          });
        this.isLoaded = true;
        this.manageExportData();
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  manageExportData(): void {
    this.tableRows = [];
    this.pdfRows = [];
    this.columns = [];
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempRows = [];
        this.cols.map((item) => {
          if (
            item["field"] == "OBSERVERS" ||
            item["field"] == "RESPONSIBLEPERSON"
          ) {
            var regexHash = new RegExp("<BR>", "g");
            if (
              data[item["field"]] &&
              data[item["field"]].search("<BR>") != -1
            ) {
              data[item["field"]] = data[item["field"]].replace(
                regexHash,
                "\n"
              );
            }
          }

          dataJson[item["header"]] = data[item["field"]];
          // dataJson[this.cols[0].header] = data[this.cols[0].field];
          // dataJson[this.cols[1].header] = data[this.cols[1].field];
          if (item["field"] == "PRIORITY" || item["field"] == "CARDSTATUS") {
            if (data[item["field"]]) {
              this.translate.get(data[item["field"]]).subscribe((label) => {
                dataJson[item["header"]] = label;
                tempRows.push(label);
              });
            }
          } else {
            tempRows.push(data[item["field"]]);
          }

          if (this.tableData.length - 1 == key) {
            this.columns.push(item.header);
          }
        });
        this.tableRows.push(dataJson);
        this.pdfRows.push(tempRows);
      });
    }
    // this.columns[0] = this.cols[0].header;
    // this.columns[1] = this.cols[1].header;
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var fileName = "UNSAFECORRECTIVEACTIONCOVERAGEREPORT";
    if (this.selectedExport == 6) {
      this.exportAsExcelFile(this.tableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;
      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      let worksheet = workbook.addWorksheet(fileName);
      //Add Header Row
      let headerRow = worksheet.addRow(header);
      headerRow.height = 20;
      // Cell Style : Fill and Border
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: customColor },
        };
        cell.font = {
          color: { argb: customFontColor },
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
        if (number == 2) {
          cell.alignment = { vertical: "middle", horizontal: "center" };
        } else {
          cell.alignment = { vertical: "middle", horizontal: "left" };
        }
      });
      // Add Data and Conditional Formatting
      data.forEach((element) => {
        let eachRow = [];
        this.columns.map((headers, key) => {
          eachRow.push(element[headers]);
        });
        if (element.isDeleted === "Y") {
          let deletedRow = worksheet.addRow(eachRow);
          deletedRow.eachCell((cell, number) => {
            cell.font = {
              name: "Calibri",
              family: 4,
              size: 11,
              bold: false,
              strike: true,
            };
          });
        } else {
          worksheet.addRow(eachRow).getCell(2).alignment = {
            vertical: "middle",
            horizontal: "center",
          };
        }
      });

      header.map((item, key) => {
        if (key == 3) {
          worksheet.getColumn(key + 1).width = 30;
        } else if (key > 11) {
          worksheet.getColumn(key + 1).width = 25;
        } else {
          worksheet.getColumn(key + 1).width = 20;
        }
      });

      worksheet.addRow([]);
      //Generate Excel File with given name
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape", "mm", [1500, 800]);
      pdf.autoTable(this.columns, this.pdfRows, {
        startY: 10,
        headStyles: {
          fillColor: customColor,
          textColor: customFontColor,
        },
        alternateRowStyles: {
          fillColor: "#FFFFFF",
        },
        columnStyles: {
          0: { cellWidth: 20 },
          1: { cellWidth: 20 },
          2: { cellWidth: 20 },
          3: { cellWidth: 30 },
          4: { cellWidth: 20 },
          5: { cellWidth: 20 },
          6: { cellWidth: 20 },
          7: { cellWidth: 20 },
          8: { cellWidth: 20 },
          9: { cellWidth: 20 },
          10: { cellWidth: 20 },
          11: { cellWidth: 20 },
          12: { cellWidth: 20 },
          13: { cellWidth: 20 },
          14: { cellWidth: 20 },
          15: { cellWidth: 20 },
          16: { cellWidth: 20 },
          17: { cellWidth: 20 },
          18: { cellWidth: 20 },
          19: { cellWidth: 20 },
        },
      });
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }
}
