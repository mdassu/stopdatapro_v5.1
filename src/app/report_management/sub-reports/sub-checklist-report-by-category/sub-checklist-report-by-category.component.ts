import { Component, OnInit, Input } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { Router } from "@angular/router";
import { ReportsService } from "src/app/_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { GlobalDataService } from "src/app/_services/global-data.service";

@Component({
  selector: "app-sub-checklist-report-by-category",
  templateUrl: "./sub-checklist-report-by-category.component.html",
  styleUrls: ["./sub-checklist-report-by-category.component.css"],
})
export class SubChecklistReportByCategoryComponent implements OnInit {
  @Input() reportId: any;
  @Input() subReportId: any;
  @Input() categoryId: any;
  title: any;
  parameters: any;
  tableData: any = [];
  cols: any = [];
  isLoaded: boolean;
  errormsg: any = "";
  rowGroupMetadata: any;
  fieldCols: any = [];
  colCount: any;
  extraColCount: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any = 0;
  tableRows: any;
  columns: any;
  unFTableRows: any;
  unFColumns: any;
  pdfData: any = [];
  dataKey: any = "SITENAME";
  colId: any;
  mainHead: any;
  totalSafeCount: any = 0;
  totalUnSafeCount: any = 0;
  subAreaOption: number;
  colleps: number = 0;
  dateFormat: any;
  grandTotal: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private auth: AuthenticationService,
    private globalData: GlobalDataService
  ) {}

  ngOnInit() {
    this.dateFormat = this.auth.UserInfo["dateFormat"];
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.parameters["subReportId"] = this.subReportId;
      if (this.subReportId == 3 || this.subReportId == 4) {
        this.parameters["MAINCATEGORYID"] = this.categoryId;
      } else {
        this.parameters["SUBCATEGORYID"] = this.categoryId;
      }
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    this.getReports();

    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getSubReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = [];
        this.tableData = res["data"];
        if (this.tableData.length > 0) {
          var keys = Object.keys(this.tableData[0]);
        }
        var tempColumn = [
          "AREAID",
          "AREANAME",
          "CARDID",
          "CUSPOSITION",
          "DATE",
          "MAINCATEGORYID",
          "MAINCATEGORYNAME",
          "OBSERVERNAME",
          "SAFECHECK",
          "SAFECNT",
          "SAFECOMMENTS",
          "SHIFTID",
          "SHIFTNAME",
          "SITEID",
          "SITENAME",
          "SUBAREAID",
          "SUBAREANAME",
          "SUBCATEGORYID",
          "SUBCATEGORYNAME",
          "UNSAFECNT",
          "UNSAFECOMMENTS",
        ];

        keys.map((label) => {
          if (tempColumn.indexOf(label) == -1) {
            this.fieldCols.push(label);
          }
        });
        var count = 8;
        this.translate
          .get([
            "LBLOBSERVERNAME",
            "LBLAREANAME",
            "LBLSUBAREAFULLNAME",
            "LBLSHIFTNAME",
            "FMKDTRANGE",
            "LBLSAFECOMMENTS",
            "LBLUNSAFECOMMENTS",
            "LBLSAFECOUNT",
            "LBLUNSAFECOUNT",
          ])
          .subscribe((resLabel) => {
            this.subAreaOption = this.auth.UserInfo["subAreaOptionValue"];
            if (this.subAreaOption == 0) {
              this.colleps = 1;
              count = 7;
              this.cols = [
                {
                  field: "OBSERVERNAME",
                  header: resLabel["LBLOBSERVERNAME"],
                },
                {
                  field: "AREANAME",
                  header: resLabel["LBLAREANAME"],
                },
                {
                  field: "SHIFTNAME",
                  header: resLabel["LBLSHIFTNAME"],
                },
                {
                  field: "DATE",
                  header: resLabel["FMKDTRANGE"],
                },
                {
                  field: "SAFECOMMENTS",
                  header: resLabel["LBLSAFECOMMENTS"],
                },
                {
                  field: "UNSAFECOMMENTS",
                  header: resLabel["LBLUNSAFECOMMENTS"],
                },
              ];
            } else {
              this.colleps = 0;
              this.cols = [
                {
                  field: "OBSERVERNAME",
                  header: resLabel["LBLOBSERVERNAME"],
                },
                {
                  field: "AREANAME",
                  header: resLabel["LBLAREANAME"],
                },
                {
                  field: "SUBAREANAME",
                  header: resLabel["LBLSUBAREAFULLNAME"],
                },
                {
                  field: "SHIFTNAME",
                  header: resLabel["LBLSHIFTNAME"],
                },
                {
                  field: "DATE",
                  header: resLabel["FMKDTRANGE"],
                },
                {
                  field: "SAFECOMMENTS",
                  header: resLabel["LBLSAFECOMMENTS"],
                },
                {
                  field: "UNSAFECOMMENTS",
                  header: resLabel["LBLUNSAFECOMMENTS"],
                },
              ];
            }

            if (this.subReportId == 3) {
              this.cols.push({
                field: "SAFECNT",
                header: resLabel["LBLSAFECOUNT"],
              });
            } else if (this.subReportId == 4 || this.subReportId == 5) {
              this.cols.push({
                field: "UNSAFECNT",
                header: resLabel["LBLUNSAFECOUNT"],
              });
            }
          });

        this.colCount = count + this.fieldCols.length;
        if (this.subReportId == 5) {
          this.extraColCount = this.colCount % 3;
        } else {
          this.extraColCount = this.colCount % 2;
        }
        this.updateRowGroupMetaData();
        this.manageExportData();
        this.isLoaded = true;
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.totalSafeCount = 0;
    this.totalUnSafeCount = 0;
    this.rowGroupMetadata = {};
    this.mainHead = [];
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let rowData = this.tableData[i];
        let sitename = rowData.SITENAME;
        if (rowData.SAFECNT) {
          this.totalSafeCount = this.totalSafeCount + rowData.SAFECNT;
        }
        if (rowData.UNSAFECNT) {
          this.totalUnSafeCount = this.totalUnSafeCount + rowData.UNSAFECNT;
        }
        if (i == 0) {
          this.rowGroupMetadata[sitename] = { index: 0, size: 1 };
          if (this.subReportId == 5) {
            this.mainHead.push({
              SITENAME: sitename,
              MAINCATEGORYNAME: rowData.MAINCATEGORYNAME,
              SUBCATEGORYNAME: rowData.SUBCATEGORYNAME,
            });
          } else {
            this.mainHead.push({
              SITENAME: sitename,
              MAINCATEGORYNAME: rowData.MAINCATEGORYNAME,
            });
          }
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.SITENAME;
          if (sitename === previousRowGroup) {
            this.rowGroupMetadata[sitename].size++;
          } else {
            this.rowGroupMetadata[sitename] = { index: i, size: 1 };
            if (this.subReportId == 5) {
              this.mainHead.push({
                SITENAME: sitename,
                MAINCATEGORYNAME: rowData.MAINCATEGORYNAME,
                SUBCATEGORYNAME: rowData.SUBCATEGORYNAME,
              });
            } else {
              this.mainHead.push({
                SITENAME: sitename,
                MAINCATEGORYNAME: rowData.MAINCATEGORYNAME,
              });
            }
          }
        }
      }
    }
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    var enableSubarea = 0;
    if (this.globalData.GlobalData["ENABLESUBAREA"] != 1) {
      enableSubarea = 1;
    }
    if (this.tableData.length > 0 && this.cols.length > 0) {
      // get data for Excel
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempDataJson = {};
        this.translate
          .get(["LBLSITENAME", "LBLMAINCATEGORY", "LBLSUBCATNAME"])
          .subscribe((resLabel) => {
            if (key == 0) {
              this.unFColumns.push(resLabel["LBLSITENAME"]);
              this.unFColumns.push(resLabel["LBLMAINCATEGORY"]);
              if (this.subReportId == 5) {
                this.unFColumns.push(resLabel["LBLSUBCATNAME"]);
              }
            }

            tempDataJson[resLabel["LBLSITENAME"]] = data["SITENAME"];
            tempDataJson[resLabel["LBLMAINCATEGORY"]] =
              data["MAINCATEGORYNAME"];
            if (this.subReportId == 5) {
              tempDataJson[resLabel["LBLSUBCATNAME"]] = data["SUBCATEGORYNAME"];
            }
          });

        this.cols.map((item, index) => {
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];

          if (index == 3 - enableSubarea) {
            this.fieldCols.map((fieldCol) => {
              dataJson[fieldCol] = data[fieldCol];
              tempDataJson[fieldCol] = data[fieldCol];
            });
          }
          if (this.tableData.length - 1 == key) {
            if (index == 4 - enableSubarea) {
              this.fieldCols.map((fieldCol) => {
                this.columns.push(fieldCol);
                this.unFColumns.push(fieldCol);
              });
            }
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
          }
        });
        dataJson["SITENAME"] = data["SITENAME"];
        dataJson["MAINCATEGORYNAME"] = data["MAINCATEGORYNAME"];
        if (this.subReportId == 3) {
          this.grandTotal = this.totalSafeCount;
        } else if (this.subReportId == 4 || this.subReportId == 5) {
          this.grandTotal = this.totalUnSafeCount;
        }
        if (this.subReportId == 5) {
          dataJson["SUBCATEGORYNAME"] = data["UNSAFECOUNT"];
        }
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
        // this.pdfData.push(tempRows);
      });

      // get data for PDF
      var mainColumns = [];
      this.translate
        .get(["LBLSITENAME", "LBLMAINCATEGORY", "LBLSUBCATNAME"])
        .subscribe((resLabel) => {
          mainColumns.push(resLabel["LBLSITENAME"]);
          mainColumns.push(resLabel["LBLMAINCATEGORY"]);
          if (this.subReportId == 5) {
            mainColumns.push(resLabel["LBLSUBCATNAME"]);
          }
        });
      var tempMainColums = Object.keys(this.mainHead[0]);
      var subColumns = this.columns;
      this.mainHead.map((head) => {
        var mainRows = [];
        var subRows = [];
        // main Table
        var tempMainRows = [];
        tempMainColums.map((col) => {
          tempMainRows.push(head[col]);
        });
        mainRows.push(tempMainRows);
        // sub Table
        this.tableData.map((data) => {
          var tempSubRows = [];
          if (data["SITENAME"] == head["SITENAME"]) {
            this.cols.map((subCol, index) => {
              if (index == 4 - enableSubarea) {
                this.fieldCols.map((fieldCol) => {
                  tempSubRows.push(data[fieldCol]);
                });
              }
              tempSubRows.push(data[subCol.field]);
            });
            subRows.push(tempSubRows);
          }
        });
        this.pdfData.push({
          mainColumns: mainColumns,
          mainRows: mainRows,
          subColumns: subColumns,
          subRows: subRows,
        });
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    var fileName = "CHECKLISTCATEGORYSAFEMAIN";
    if (this.subReportId == 3) {
      fileName = "CHECKLISTCATEGORYSAFEMAIN";
    } else if (this.subReportId == 4) {
      fileName = "CHECKLISTCATEGORYUNSAFEMAIN";
    } else if (this.subReportId == 5) {
      fileName = "CHECKLISTCATEGORYUNSAFESUB";
    }
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      this.exportAsExcelFile(this.unFTableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var startColId;
      if (this.subReportId == 5) {
        startColId = "C";
      } else {
        startColId = "B";
      }
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;

      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      let worksheet = workbook.addWorksheet("Sheet1");

      if (this.selectedExport == 4 || this.selectedExport == 5) {
        for (var i = 1; i <= 15; i++) {
          worksheet.getColumn(i).width = 25;
        }

        let headerRow = worksheet.addRow(unFColumns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });

        // Add Data and Conditional Formatting
        unFTableRows.map((element, index) => {
          let eachRow = [];
          this.unFColumns.map((headers, key) => {
            if (headers == "User Level" || headers == "Status") {
              if (element[headers]) {
                this.translate.get(element[headers]).subscribe((val) => {
                  eachRow.push(val);
                });
              } else {
                eachRow.push(element[headers]);
              }
            } else {
              eachRow.push(element[headers]);
            }
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            if (unFTableRows.length - 1 == index) {
              worksheet
                .addRow(eachRow)
                .eachCell({ includeEmpty: true }, (cell) => {
                  cell.border = {
                    bottom: { style: "medium", color: { argb: customColor } },
                  };
                });
            } else {
              worksheet.addRow(eachRow);
            }
          }
        });
      } else {
        let subHeader = header;
        var beforeDataCount = 1;
        this.mainHead.map((header, key) => {
          var rowsData = data.filter((item) => {
            return item[this.dataKey] == header[this.dataKey];
          });
          for (var i = 1; i <= 15; i++) {
            worksheet.getColumn(i).width = 25;
          }
          if (key == 0) {
            worksheet.mergeCells(
              startColId + beforeDataCount + ":G" + beforeDataCount
            );
            worksheet.mergeCells(
              startColId +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                +(beforeDataCount + 1)
            );
            this.translate
              .get(["LBLSITENAME", "LBLMAINCATEGORY", "LBLSUBCATNAME"])
              .subscribe((resLabel) => {
                worksheet.getCell("A" + beforeDataCount).value =
                  resLabel["LBLSITENAME"];
                worksheet.getCell("B" + beforeDataCount).value =
                  resLabel["LBLMAINCATEGORY"];
                if (this.subReportId == 5) {
                  worksheet.getCell(startColId + beforeDataCount).value =
                    resLabel["LBLSUBCATNAME"];
                }
              });
            worksheet.getCell("A" + (beforeDataCount + 1)).value =
              header["SITENAME"];
            worksheet.getCell("B" + (beforeDataCount + 1)).value =
              header["MAINCATEGORYNAME"];
            if (this.subReportId == 5) {
              worksheet.getCell(startColId + (beforeDataCount + 1)).value =
                header["SUBCATEGORYNAME"];
            }
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("B" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            if (this.subReportId == 5) {
              worksheet.getCell("C" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("C" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
            }

            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("B" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };

            beforeDataCount = rowsData.length + 5;
          } else {
            worksheet.mergeCells(
              "C" + beforeDataCount + ":G" + beforeDataCount
            );
            worksheet.mergeCells(
              "C" +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                +(beforeDataCount + 1)
            );
            this.translate
              .get(["LBLSITENAME", "LBLMAINCATEGORY", "LBLSUBCATNAME"])
              .subscribe((resLabel) => {
                worksheet.getCell("A" + beforeDataCount).value =
                  resLabel["LBLSITENAME"];
                worksheet.getCell("B" + beforeDataCount).value =
                  resLabel["LBLMAINCATEGORY"];
                if (this.subReportId == 5) {
                  worksheet.getCell("C" + beforeDataCount).value =
                    resLabel["LBLSUBCATNAME"];
                }
              });
            worksheet.getCell("A" + (beforeDataCount + 1)).value =
              header["SITENAME"];
            worksheet.getCell("B" + (beforeDataCount + 1)).value =
              header["MAINCATEGORYNAME"];
            if (this.subReportId == 5) {
              worksheet.getCell("C" + (beforeDataCount + 1)).value =
                header["SUBCATEGORYNAME"];
            }
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("B" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            if (this.subReportId == 5) {
              worksheet.getCell("C" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("C" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
            }

            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("B" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 4 + beforeDataCount;
          }

          let headerRow = worksheet.addRow(subHeader);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: "EEECE1" },
            };
            cell.font = {
              color: { argb: "000000" },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
          });

          // Add Data and Conditional Formatting
          rowsData.map((element, index) => {
            let eachRow = [];
            this.columns.map((headers, key) => {
              if (headers == "User Level" || headers == "Status") {
                if (element[headers]) {
                  this.translate.get(element[headers]).subscribe((val) => {
                    eachRow.push(val);
                  });
                } else {
                  eachRow.push(element[headers]);
                }
              } else {
                eachRow.push(element[headers]);
              }
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (rowsData.length - 1 == index) {
                worksheet
                  .addRow(eachRow)
                  .eachCell({ includeEmpty: true }, (cell) => {
                    // cell.border = {
                    //   bottom: { style: "medium", color: { argb: customColor } },
                    // };
                  });
              } else {
                worksheet.addRow(eachRow);
              }
            }
          });
        });
      }

      var sumOfData;
      if (this.subReportId == 3) {
        sumOfData = this.totalSafeCount;
      } else if (this.subReportId == 4 || this.subReportId == 5) {
        sumOfData = this.totalUnSafeCount;
      }
      this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
        let finalRow = worksheet.addRow([label]);
        this.columns.map((headers, key) => {
          if (this.columns.length - 1 == key) {
            if (this.selectedExport == 4 || this.selectedExport == 5) {
              finalRow.getCell(key + 3).value = sumOfData;
              finalRow.getCell(key + 3).alignment = {
                vertical: "middle",
                horizontal: "right",
              };
            } else {
              finalRow.getCell(key + 1).value = sumOfData;
              finalRow.getCell(key + 1).alignment = {
                vertical: "middle",
                horizontal: "right",
              };
            }
          }
        });

        finalRow.eachCell({ includeEmpty: true }, (cell) => {
          cell.font = {
            size: 12,
            bold: true,
          };
          cell.border = {
            top: { style: "thin", color: { argb: customColor } },
            // left: { style: "thin", color: {argb: customColor} },
            bottom: { style: "thin", color: { argb: customColor } },
            // right: { style: "thin", color: {argb: customColor} },
          };
        });
      });

      worksheet.getColumn(1).width = 40;
      worksheet.getColumn(2).width = 30;
      worksheet.addRow([]);
      //Generate Excel File with given name
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape", "mm", [1200, 800]);
      let finalY = 10;
      this.pdfData.map((data, key) => {
        pdf.autoTable(data.mainColumns, data.mainRows, {
          startY: finalY,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
        });
        finalY = pdf.previousAutoTable.finalY;
        if (this.pdfData.length - 1 == key) {
          this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
            var grandTotalArr = [];
            for (var i = 1; i <= data.subRows[0].length - 1; i++) {
              if (i == 1) {
                grandTotalArr[i - 1] = label;
              }
              if (i == data.subRows[0].length - 1) {
                if (this.subReportId == 3) {
                  grandTotalArr[i] = this.totalSafeCount;
                } else if (this.subReportId == 4 || this.subReportId == 5) {
                  grandTotalArr[i] = this.totalUnSafeCount;
                }
              }
            }
            data.subRows.push(grandTotalArr);
          });
        }
        var fn = this;
        pdf.autoTable(data.subColumns, data.subRows, {
          startY: finalY,
          headStyles: {
            fillColor: "#D8D8D8",
            textColor: "#000000",
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          willDrawCell: (rowData) => {
            fn.drawCell(rowData, data.subColumns.length);
          },
          didParseCell: (cellData) => {
            fn.alignCol(cellData, customColor, data.subRows.length);
          },
          columnStyles: {
            0: { cellWidth: 20 },
            1: { cellWidth: 20 },
            2: { cellWidth: 20 },
            3: { cellWidth: 30 },
            4: { cellWidth: 20 },
            5: { cellWidth: 20 },
            6: { cellWidth: 20 },
            7: { cellWidth: 20 },
            8: { cellWidth: 20 },
            9: { cellWidth: 20 },
            10: { cellWidth: 20 },
          },
        });

        finalY = pdf.previousAutoTable.finalY + 10;
      });

      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }

  alignCol = function (data, customColor, rowCount) {
    if (data.column.index == 1) {
      data.cell.styles.halign = "center";
    }
    var s = data.cell.styles;
    if (data.row.index == rowCount - 1) {
      s.lineColor = "#" + customColor;
      s.lineWidth = 0.5;
    }
  };

  drawCell = function (data, colCount) {
    var doc = data.doc;
    var rows = data.table.body;
    var col = data.column.index;
    if (col == colCount - 1) {
      data.cell.styles.halign = "center";
    }
    if (rows.length === 1) {
    } else if (data.row.index === rows.length - 1) {
      doc.setFontStyle("bold");
      doc.setFontSize("12");
    }
  };
}
