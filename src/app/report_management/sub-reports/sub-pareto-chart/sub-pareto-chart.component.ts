import { Component, OnInit, Input } from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { Router } from "@angular/router";
import { ReportsService } from "src/app/_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { GlobalDataService } from "src/app/_services/global-data.service";
import { fn } from "@angular/compiler/src/output/output_ast";

@Component({
  selector: "app-sub-pareto-chart",
  templateUrl: "./sub-pareto-chart.component.html",
  styleUrls: ["./sub-pareto-chart.component.css"],
})
export class SubParetoChartComponent implements OnInit {
  @Input() reportId: any;
  @Input() subReportId: any;
  @Input() categoryId: any;
  title: any;
  parameters: any;
  tableData: any = [];
  cols: any = [];
  subCols: any = [];
  isLoaded: boolean;
  errormsg: any = "";
  isSubLoaded: boolean;
  subErrorMsg: any = "";
  rowGroupMetadata: any;
  width: any;
  height: any;
  type: any;
  dataFormat: any;
  dataSource: any;
  viewSubReport: boolean;
  fieldCols: any = [];
  colCount: any;
  extraColCount: any;
  subTableData: any = [];
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  unFTableRows: any;
  unFColumns: any;
  pdfRows: any = [];
  chart: any;
  pdfData: any = [];
  mainHead: any;
  dataKey: any = "SITENAME";
  colId: any = 0;
  headName: any;
  dateFormat: any;
  dateFormatType: any;
  subAreaOption: number;
  colleps: number = 0;
  grandTotal: number = 0;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private auth: AuthenticationService,
    private globalData: GlobalDataService
  ) {}

  ngOnInit() {
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.parameters["subReportId"] = this.subReportId;
      this.parameters["MAINCATEGORYID"] = this.categoryId;
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    this.getReports();
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
    this.dateFormat = this.auth.UserInfo["dateFormat"];
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getSubReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = [];
        this.tableData = res["data"];
        this.translate
          .get(["LBLSUBCATNAME", "LBLUNSAFECOUNT"])
          .subscribe((resLabel) => {
            if (this.subReportId == 1) {
              var chartData = [];
              this.tableData.map((tdata, key) => {
                chartData.push({
                  label: tdata.SUBCATEGORYNAME,
                  value: tdata.UNSAFECNT,
                });
                if (this.tableData.length == key + 1) {
                  const data = {
                    chart: {
                      xaxisname: resLabel["LBLSUBCATNAME"],
                      xAxisNameFontBold: 1,
                      decimals: "1",
                      drawcrossline: "1",
                      showValues: "1",
                      rotateValues: "1",
                      theme: "fusion",
                    },
                    data: chartData,
                  };

                  this.dataSource = data;

                  this.isLoaded = true;
                }
              });
              this.cols = [
                { field: "SUBCATEGORYNAME", header: resLabel["LBLSUBCATNAME"] },
                { field: "UNSAFECNT", header: resLabel["LBLUNSAFECOUNT"] },
              ];
            }
          });
        this.manageExportData();
        this.isLoaded = true;
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  openSubReport(data, chart, headName) {
    var categoryId;
    if (chart) {
      var objValue = data["dataObj"]["toolText"].split(",");
      this.tableData.map((item) => {
        if (item["SUBCATEGORYNAME"] == objValue[0]) {
          categoryId = item["SUBCATEGORYID"];
        }
      });
    } else {
      categoryId = data;
    }
    if (!headName) {
      headName = objValue[0];
    }
    this.viewSubReport = true;
    this.isSubLoaded = false;
    this.parameters["subReportId"] = 2;
    this.parameters["SUBCATEGORYID"] = categoryId;
    this.headName = headName;
    this.getSubReports();
  }

  getSubReports() {
    this.reportsService.getSubReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.subTableData = [];
        this.fieldCols = [];
        this.subTableData = res["data"];
        if (this.subTableData.length > 0) {
          var keys = Object.keys(this.subTableData[0]);
        }
        var tempColumn = [
          "AREANAME",
          "CUSPOSITION",
          "MAINCATEGORYNAME",
          "OBSERVATIONDATE",
          "OBSERVERNAME",
          "SHIFTNAME",
          "SITENAME",
          "SUBAREANAME",
          "SUBCATEGORYNAME",
          "UNSAFECNT",
          "UNSAFECOMMENTS",
        ];

        keys.map((label) => {
          if (tempColumn.indexOf(label) == -1) {
            this.fieldCols.push(label);
          }
        });
        this.colCount = 7 + this.fieldCols.length;
        this.extraColCount = this.colCount % 3;

        this.translate
          .get([
            "LBLOBSDATE",
            "LBLOBSERVERNAME",
            "LBLAREANAME",
            "LBLSUBAREAFULLNAME",
            "LBLSHIFTNAME",
            "LBLUNSAFECOUNT",
            "LBLUNSAFECOMMENTS",
          ])
          .subscribe((resLabel) => {
            this.subAreaOption = this.auth.UserInfo["subAreaOptionValue"];
            if (this.subAreaOption == 0) {
              this.colleps = 1;
              this.subCols = [
                {
                  field: "OBSERVATIONDATE",
                  header: resLabel["LBLOBSDATE"],
                },
                {
                  field: "OBSERVERNAME",
                  header: resLabel["LBLOBSERVERNAME"],
                },
                {
                  field: "AREANAME",
                  header: resLabel["LBLAREANAME"],
                },
                {
                  field: "SHIFTNAME",
                  header: resLabel["LBLSHIFTNAME"],
                },
                {
                  field: "UNSAFECNT",
                  header: resLabel["LBLUNSAFECOUNT"],
                },
                {
                  field: "UNSAFECOMMENTS",
                  header: resLabel["LBLUNSAFECOMMENTS"],
                },
              ];
            } else {
              this.colleps = 0;
              this.subCols = [
                {
                  field: "OBSERVATIONDATE",
                  header: resLabel["LBLOBSDATE"],
                },
                {
                  field: "OBSERVERNAME",
                  header: resLabel["LBLOBSERVERNAME"],
                },
                {
                  field: "AREANAME",
                  header: resLabel["LBLAREANAME"],
                },
                {
                  field: "SUBAREANAME",
                  header: resLabel["LBLSUBAREAFULLNAME"],
                },
                {
                  field: "SHIFTNAME",
                  header: resLabel["LBLSHIFTNAME"],
                },
                {
                  field: "UNSAFECNT",
                  header: resLabel["LBLUNSAFECOUNT"],
                },
                {
                  field: "UNSAFECOMMENTS",
                  header: resLabel["LBLUNSAFECOMMENTS"],
                },
              ];
            }
          });

        this.updateRowGroupMetaData();
        this.subReportManageExportData();
        this.isSubLoaded = true;
        if (this.isSubLoaded) {
          document.getElementById("clickId").click();
        }
      } else {
        this.isSubLoaded = true;
        this.subErrorMsg = "LBLRPTNORECFND";
      }
    });
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    this.mainHead = [];
    this.grandTotal = 0;
    if (this.subTableData) {
      var tempSize = 0;
      for (let i = 0; i < this.subTableData.length; i++) {
        tempSize = tempSize + 1;
        let rowData = this.subTableData[i];
        let sitename = rowData.SITENAME;
        if (i == 0) {
          this.rowGroupMetadata[sitename] = {
            index: 0,
            size: 1,
            tempSize: tempSize,
            total: rowData.UNSAFECNT,
          };
          this.mainHead.push({
            SITENAME: sitename,
            MAINCATEGORYNAME: rowData.MAINCATEGORYNAME,
            SUBCATEGORYNAME: rowData.SUBCATEGORYNAME,
          });
        } else {
          let previousRowData = this.subTableData[i - 1];
          let previousRowGroup = previousRowData.SITENAME;
          if (sitename === previousRowGroup) {
            this.rowGroupMetadata[sitename].size++;
            this.rowGroupMetadata[sitename]["tempSize"] = tempSize;
            this.rowGroupMetadata[sitename]["total"] =
              this.rowGroupMetadata[sitename]["total"] + rowData.UNSAFECNT;
          } else {
            this.rowGroupMetadata[sitename] = {
              index: i,
              size: 1,
              tempSize: tempSize,
              total: rowData.UNSAFECNT,
            };
            // this.rowGroupMetadata[sitename]["total"] =
            //   this.rowGroupMetadata[sitename]["total"] + rowData.UNSAFECNT;
            this.mainHead.push({
              SITENAME: sitename,
              MAINCATEGORYNAME: rowData.MAINCATEGORYNAME,
              SUBCATEGORYNAME: rowData.SUBCATEGORYNAME,
            });
          }
        }
        this.grandTotal = this.grandTotal + rowData.UNSAFECNT;
      }
    }
  }

  initialized($event) {
    this.chart = $event.chart; // saving chart instance
  }

  // subBarClick(data) {
  //   var objValue = data["dataObj"]["toolText"].split(",");
  //   this.tableData.map(item => {
  //     if (item["SUBCATEGORYNAME"] == objValue[0]) {
  //       this.openSubReport(item["SUBCATEGORYID"]);
  //     }
  //   });
  // }

  manageExportData(): void {
    this.tableRows = [];
    this.pdfRows = [];
    this.columns = [];
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.tableData.map((data) => {
        var dataJson = {};
        dataJson[this.cols[0].header] = data[this.cols[0].field];
        dataJson[this.cols[1].header] = data[this.cols[1].field];
        this.tableRows.push(dataJson);
        this.pdfRows.push([data[this.cols[0].field], data[this.cols[1].field]]);
      });
    }
    this.columns[0] = this.cols[0].header;
    this.columns[1] = this.cols[1].header;
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    var fileName = "PARETO";
    var encodedData;
    this.chart.getSVGString((svg) => {
      this.reportsService.svgString2Image(
        svg,
        1030,
        524,
        "png",
        function (base64String) {
          encodedData = base64String;
        }
      );
    });
    setTimeout(() => {
      if (this.selectedExport == 6) {
        this.exportAsExcelFile(this.tableRows, fileName);
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
      } else if (
        this.selectedExport == 1 ||
        this.selectedExport == 3 ||
        this.selectedExport == 4 ||
        this.selectedExport == 5
      ) {
        var EXCEL_EXTENSION;
        if (this.selectedExport == 1 || this.selectedExport == 5) {
          EXCEL_EXTENSION = ".xlsx";
        } else if (this.selectedExport == 3 || this.selectedExport == 4) {
          EXCEL_EXTENSION = ".xls";
        }
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        //Excel Title, Header, Data
        const header = this.columns;
        const data = this.tableRows;
        var hslNumbers = localStorage
          .getItem("CustomColor")
          .match(/\d+/g)
          .map((n) => parseInt(n));
        const customColor = this.reportsService.convertHslToHex(
          hslNumbers[0],
          hslNumbers[1],
          hslNumbers[2]
        );
        var hslNumbers = localStorage
          .getItem("CustomFont")
          .match(/\d+/g)
          .map((n) => parseInt(n));
        const customFontColor = this.reportsService.convertHslToHex(
          hslNumbers[0],
          hslNumbers[1],
          hslNumbers[2]
        );
        const EXCEL_TYPE =
          "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

        //Create workbook and worksheet
        let workbook = new ExcelJS.Workbook();
        let worksheet = workbook.addWorksheet("Sheet1");
        //Add Header Row
        if (this.selectedExport == 1 || this.selectedExport == 2) {
          worksheet.getColumn(1).width = 40;
          worksheet.getColumn(2).width = 50;
          worksheet.getRow(1).height = 250;
          worksheet.mergeCells("A1:B1");
          var imageId = workbook.addImage({
            base64: encodedData,
            extension: "png",
          });
          worksheet.addImage(imageId, {
            tl: { col: 0, row: 0 },
            ext: { width: 800, height: 300 },
          });
        }
        let headerRow = worksheet.addRow(header);
        headerRow.height = 20;
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          if (number == 2) {
            cell.alignment = { vertical: "middle", horizontal: "center" };
          } else {
            cell.alignment = { vertical: "middle", horizontal: "left" };
          }
        });
        // Add Data and Conditional Formatting
        let sumOfData = 0;
        data.map((element) => {
          let eachRow = [];
          this.columns.map((headers, key) => {
            if (key == 1) {
              sumOfData = sumOfData + element[headers];
            }
            eachRow.push(element[headers]);
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            worksheet.addRow(eachRow).getCell(2).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
          }
        });
        // worksheet.getColumn(1).width = 40;
        // worksheet.getColumn(2).width = 30;
        worksheet.addRow([]);
        //Generate Excel File with given name
        workbook.xlsx.writeBuffer().then((data) => {
          let blob = new Blob([data], { type: EXCEL_TYPE });
          fs.saveAs(
            blob,
            fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
          );
        });
      } else if (this.selectedExport == 2) {
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        var fn = this;
        var pdf = new jsPDF("landscape");
        pdf.addImage(encodedData, "PNG", 20, 0, 220, 120);
        pdf.autoTable(this.columns, this.pdfRows, {
          startY: 130,
          didParseCell: (data) => {
            fn.alignCol(data);
          },
        });
        pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
      }
      this.selectedExport = 0;
    }, 1000);
  }

  alignCol = function (data) {
    if (data.column.index > 0) {
      data.cell.styles.halign = "center";
    }
  };

  subReportManageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    var enableSubarea = 0;
    if (this.globalData.GlobalData["ENABLESUBAREA"] != 1) {
      enableSubarea = 1;
    }
    if (this.subTableData.length > 0 && this.subCols.length > 0) {
      // get data for Excel
      this.subTableData.map((data, key) => {
        var dataJson = {};
        var tempDataJson = {};
        // var tempRows = [];
        this.translate
          .get(["LBLSITENAME", "LBLMAINCATEGORY", "LBLSUBCATNAME"])
          .subscribe((resLabel) => {
            if (key == 0) {
              this.unFColumns.push(resLabel["LBLSITENAME"]);
              this.unFColumns.push(resLabel["LBLMAINCATEGORY"]);
              this.unFColumns.push(resLabel["LBLSUBCATNAME"]);
            }
            tempDataJson[resLabel["LBLSITENAME"]] = data["SITENAME"];
            tempDataJson[resLabel["LBLMAINCATEGORY"]] =
              data["MAINCATEGORYNAME"];
            tempDataJson[resLabel["LBLSUBCATNAME"]] = data["SUBCATEGORYNAME"];
          });
        this.subCols.map((item, index) => {
          if (item["field"] == "OBSERVERNAME") {
            var regexHash = new RegExp("<BR>", "g");
            if (
              data[item["field"]] &&
              data[item["field"]].search("<BR>") != -1
            ) {
              data[item["field"]] = data[item["field"]].replace(
                regexHash,
                "\n"
              );
            }
          }
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];
          if (index == 4 - enableSubarea) {
            this.fieldCols.map((fieldCol) => {
              dataJson[fieldCol] = data[fieldCol];
              tempDataJson[fieldCol] = data[fieldCol];
            });
          }
          // tempRows.push(data[item["field"]]);
          if (this.subTableData.length - 1 == key) {
            if (index == 5 - enableSubarea) {
              this.fieldCols.map((fieldCol) => {
                this.columns.push(fieldCol);
                this.unFColumns.push(fieldCol);
              });
            }
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
            if (this.subCols.length - 1 == index) {
              this.colId = String.fromCharCode(
                97 + this.columns.length - 1
              ).toUpperCase();
            }
          }
        });
        dataJson["SITENAME"] = data["SITENAME"];
        dataJson["MAINCATEGORYNAME"] = data["MAINCATEGORYNAME"];
        dataJson["SUBCATEGORYNAME"] = data["SUBCATEGORYNAME"];
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
        // this.pdfData.push(tempRows);
      });

      // get data for PDF
      var mainColumns = [];
      this.translate
        .get(["LBLSITENAME", "LBLMAINCATEGORY", "LBLSUBCATNAME"])
        .subscribe((resLabel) => {
          mainColumns.push(resLabel["LBLSITENAME"]);
          mainColumns.push(resLabel["LBLMAINCATEGORY"]);
          mainColumns.push(resLabel["LBLSUBCATNAME"]);
        });
      var tempMainColums = Object.keys(this.mainHead[0]);
      var subColumns = this.columns;
      this.mainHead.map((head, inkey) => {
        var mainRows = [];
        var subRows = [];
        // main Table
        var tempMainRows = [];
        tempMainColums.map((col) => {
          tempMainRows.push(head[col]);
        });
        mainRows.push(tempMainRows);
        var total = 0;
        // sub Table
        this.subTableData.map((data, key) => {
          var tempSubRows = [];
          if (data["SITENAME"] == head["SITENAME"]) {
            total = total + data["UNSAFECNT"];
            this.subCols.map((subCol, index) => {
              tempSubRows.push(data[subCol.field]);
              if (index == 4 - enableSubarea) {
                this.fieldCols.map((fieldCol) => {
                  tempSubRows.push(data[fieldCol]);
                });
              }
            });
            subRows.push(tempSubRows);
            var number = this.subCols.length + this.fieldCols.length;
            if (this.subTableData.length - 1 == key) {
              tempSubRows = [];
              for (var i = 1; i <= number; i++) {
                if (i == 1) {
                  this.translate.get("LBLTOTAL").subscribe((label) => {
                    tempSubRows.push(label);
                  });
                } else if (i == number - 1) {
                  tempSubRows.push(total);
                } else {
                  tempSubRows.push("");
                }
              }
              subRows.push(tempSubRows);
            }
          }
        });
        var num = this.subCols.length + this.fieldCols.length;
        if (this.mainHead.length - 1 == inkey) {
          var tempSubRows = [];
          for (var i = 1; i <= num; i++) {
            if (i == 1) {
              this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
                tempSubRows.push(label);
              });
            } else if (i == num - 1) {
              tempSubRows.push(this.grandTotal);
            } else {
              tempSubRows.push("");
            }
          }
          subRows.push(tempSubRows);
        }

        this.pdfData.push({
          mainColumns: mainColumns,
          mainRows: mainRows,
          subColumns: subColumns,
          subRows: subRows,
        });
      });
    }
  }

  subReportExport(): void {
    const fileName = "PARETOCHART";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      this.exportAsExcelFile(this.unFTableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;

      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      let worksheet = workbook.addWorksheet("Sheet1");
      let subHeader = header;
      if (this.selectedExport == 4 || this.selectedExport == 5) {
        for (var i = 1; i <= 15; i++) {
          worksheet.getColumn(i).width = 25;
        }

        let headerRow = worksheet.addRow(unFColumns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
        });
        var total = 0;
        // Add Data and Conditional Formatting
        unFTableRows.map((element, index) => {
          let eachRow = [];
          total = total + element["Unsafe Count"];
          unFColumns.map((headers, key) => {
            if (headers == "User Level" || headers == "Status") {
              if (element[headers]) {
                this.translate.get(element[headers]).subscribe((val) => {
                  eachRow.push(val);
                });
              } else {
                eachRow.push(element[headers]);
              }
            } else {
              eachRow.push(element[headers]);
            }
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            if (unFTableRows.length - 1 == index) {
              worksheet.addRow(eachRow).eachCell((cell, number) => {
                cell.border = {
                  bottom: { style: "medium", color: { argb: customColor } },
                };
                if (number == unFColumns.length - 1) {
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "center",
                  };
                }
              });
            } else {
              worksheet.addRow(eachRow).eachCell((cell, number) => {
                if (number == unFColumns.length - 1) {
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "center",
                  };
                }
              });
            }
          }
        });
        this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
          var grandTotalArr = [];
          this.unFColumns.map((item, key) => {
            if (key == 0) {
              grandTotalArr.push(label);
            } else {
              grandTotalArr.push("");
            }
            if (key == this.unFColumns.length - 3) {
              grandTotalArr.push(this.grandTotal);
            }
          });
          let finalRow = worksheet.addRow(grandTotalArr);
          // var fixCols = 6 + this.subAreaOption;
          // var letter;
          // if (fixCols == 6) {
          //   if (this.fieldCols.length == 1) {
          //     letter = "G";
          //   } else if (this.fieldCols.length == 2) {
          //     letter = "H";
          //   } else if (this.fieldCols.length == 3) {
          //     letter = "I";
          //   }
          // } else {
          //   if (this.fieldCols.length == 1) {
          //     letter = "H";
          //   } else if (this.fieldCols.length == 2) {
          //     letter = "I";
          //   } else if (this.fieldCols.length == 3) {
          //     letter = "J";
          //   }
          // }

          finalRow.getCell(this.unFColumns.length - 1).alignment = {
            vertical: "middle",
            horizontal: "center",
          };
          finalRow.eachCell({ includeEmpty: true }, (cell, number) => {
            cell.font = {
              size: 12,
              bold: true,
            };
            if (number <= this.unFColumns.length) {
              cell.border = {
                top: { style: "thin", color: { argb: customColor } },
                // left: { style: "thin", color: {argb: customColor} },
                bottom: { style: "thin", color: { argb: customColor } },
                // right: { style: "thin", color: {argb: customColor} },
              };
            }
          });
        });
      } else {
        var beforeDataCount = 1;
        this.mainHead.map((header, key) => {
          var rowsData = data.filter((item) => {
            return item[this.dataKey] == header[this.dataKey];
          });
          for (var i = 1; i <= 15; i++) {
            worksheet.getColumn(i).width = 25;
          }
          if (key == 0) {
            worksheet.mergeCells(
              "C" + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              "C" +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                +(beforeDataCount + 1)
            );
            this.translate
              .get(["LBLSITENAME", "LBLMAINCATEGORY", "LBLSUBCATNAME"])
              .subscribe((resLabel) => {
                worksheet.getCell("A" + beforeDataCount).value =
                  resLabel["LBLSITENAME"];
                worksheet.getCell("B" + beforeDataCount).value =
                  resLabel["LBLMAINCATEGORY"];
                worksheet.getCell("C" + beforeDataCount).value =
                  resLabel["LBLSUBCATNAME"];
              });
            worksheet.getCell("A" + (beforeDataCount + 1)).value =
              header["SITENAME"];
            worksheet.getCell("B" + (beforeDataCount + 1)).value =
              header["MAINCATEGORYNAME"];
            worksheet.getCell("C" + (beforeDataCount + 1)).value =
              header["SUBCATEGORYNAME"];
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("B" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("C" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("B" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("C" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 5;
          } else {
            worksheet.mergeCells(
              "C" + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              "C" +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                +(beforeDataCount + 1)
            );
            this.translate
              .get(["LBLSITENAME", "LBLMAINCATEGORY", "LBLSUBCATNAME"])
              .subscribe((resLabel) => {
                worksheet.getCell("A" + beforeDataCount).value =
                  resLabel["LBLSITENAME"];
                worksheet.getCell("B" + beforeDataCount).value =
                  resLabel["LBLMAINCATEGORY"];
                worksheet.getCell("C" + beforeDataCount).value =
                  resLabel["LBLSUBCATNAME"];
              });
            worksheet.getCell("A" + (beforeDataCount + 1)).value =
              header["SITENAME"];
            worksheet.getCell("B" + (beforeDataCount + 1)).value =
              header["MAINCATEGORYNAME"];
            worksheet.getCell("C" + (beforeDataCount + 1)).value =
              header["SUBCATEGORYNAME"];

            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("B" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("C" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("B" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("C" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 4 + beforeDataCount;
          }

          let headerRow = worksheet.addRow(subHeader);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: "EEECE1" },
            };
            cell.font = {
              color: { argb: "000000" },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
            if (number == subHeader.length - 1) {
              cell.alignment = {
                vertical: "middle",
                horizontal: "center",
              };
            }
          });
          var total = 0;
          // Add Data and Conditional Formatting
          rowsData.map((element, index) => {
            let eachRow = [];
            total = total + element["Unsafe Count"];
            this.columns.map((headers, key) => {
              if (headers == "User Level" || headers == "Status") {
                if (element[headers]) {
                  this.translate.get(element[headers]).subscribe((val) => {
                    eachRow.push(val);
                  });
                } else {
                  eachRow.push(element[headers]);
                }
              } else {
                eachRow.push(element[headers]);
              }
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (rowsData.length - 1 == index) {
                worksheet
                  .addRow(eachRow)
                  .eachCell({ includeEmpty: true }, (cell, number) => {
                    cell.border = {
                      bottom: { style: "medium", color: { argb: customColor } },
                    };
                    if (number == subHeader.length - 1) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                      };
                    }
                  });
              } else {
                worksheet.addRow(eachRow).eachCell((cell, number) => {
                  if (number == subHeader.length - 1) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                });
              }
            }
          });
          this.translate.get("LBLTOTAL").subscribe((label) => {
            var totalArr = [];
            this.columns.map((item, key) => {
              if (key == 0) {
                totalArr.push(label);
              } else {
                totalArr.push("");
              }
              if (key == this.columns.length - 3) {
                totalArr.push(total);
              }
            });
            let finalRow = worksheet.addRow(totalArr);
            finalRow.getCell(subHeader.length - 1).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            finalRow.eachCell((cell) => {
              cell.font = {
                size: 12,
                bold: true,
              };
            });
          });
        });

        this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
          var grandTotalArr = [];
          this.columns.map((item, key) => {
            if (key == 0) {
              grandTotalArr.push(label);
            } else {
              grandTotalArr.push("");
            }
            if (key == this.columns.length - 3) {
              grandTotalArr.push(this.grandTotal);
            }
          });
          let finalRow = worksheet.addRow(grandTotalArr);
          // var fixCols = 6 + this.subAreaOption;
          // var letter;
          // if (fixCols == 6) {
          //   if (this.fieldCols.length == 1) {
          //     letter = "G";
          //   } else if (this.fieldCols.length == 2) {
          //     letter = "H";
          //   } else if (this.fieldCols.length == 3) {
          //     letter = "I";
          //   }
          // } else {
          //   if (this.fieldCols.length == 1) {
          //     letter = "H";
          //   } else if (this.fieldCols.length == 2) {
          //     letter = "I";
          //   } else if (this.fieldCols.length == 3) {
          //     letter = "J";
          //   }
          // }

          finalRow.getCell(subHeader.length - 1).alignment = {
            vertical: "middle",
            horizontal: "center",
          };
          finalRow.eachCell((cell, number) => {
            cell.font = {
              size: 12,
              bold: true,
            };
            if (number <= subHeader.length) {
              cell.border = {
                top: { style: "thin", color: { argb: customColor } },
                // left: { style: "thin", color: {argb: customColor} },
                bottom: { style: "thin", color: { argb: customColor } },
                // right: { style: "thin", color: {argb: customColor} },
              };
            }
          });
        });
      }

      worksheet.addRow([]);
      //Generate Excel File with given name
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape", "mm", [1200, 800]);
      var fn = this;
      let finalY = 10;
      this.pdfData.map((data) => {
        pdf.autoTable(data.mainColumns, data.mainRows, {
          startY: finalY,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
        });
        finalY = pdf.previousAutoTable.finalY;
        pdf.autoTable(data.subColumns, data.subRows, {
          startY: finalY,
          headStyles: {
            fillColor: "#D8D8D8",
            textColor: "#000000",
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          didParseCell: (cellData) => {
            fn.unSAlignCol(
              cellData,
              data.subColumns.length,
              data.subRows.length,
              customColor
            );
          },
          willDrawCell: this.drawCell,
        });
        finalY = pdf.previousAutoTable.finalY + 10;
      });
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }

  unSAlignCol = function (data, count, rowCount, customColor) {
    if (data.column.index == count - 2) {
      data.cell.styles.halign = "center";
    }

    var s = data.cell.styles;
    if (data.row.index == rowCount - 1) {
      s.lineColor = "#" + customColor;
      s.lineWidth = 0.5;
      s.borders = "t";
    }
  };

  drawCell = function (data) {
    var doc = data.doc;
    var rows = data.table.body;
    if (rows.length === 1) {
    } else if (data.row.index === rows.length - 1) {
      doc.setFontStyle("bold");
      doc.setFontSize("12");
    }
  };
}
