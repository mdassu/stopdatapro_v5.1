import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { ConsoleService } from "@ng-select/ng-select/ng-select/console.service";
import { TranslateService } from "@ngx-translate/core";
import { formatDate } from "@angular/common";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { AuthenticationService } from "src/app/_services/authentication.service";

@Component({
  selector: "app-checklist-count-summary",
  templateUrl: "./checklist-count-summary.component.html",
  styleUrls: ["./checklist-count-summary.component.css"],
})
export class ChecklistCountSummaryComponent implements OnInit {
  @Input() reportId: any;
  tableData: any = [];
  title: any;
  siteList: any = [{ label: "All", value: "" }];
  selectedReportView: any = 33;
  checkSumGroup1: any = -1;
  checkSumGroup2: any = 3;
  checkSumGroup3: any = -3;
  checkSumLength: any = 1;
  isLoaded: boolean;
  tableCols: any = [];
  errormsg: any;
  rowGroupMetadata: any;
  parameters: any;
  allSiteSelected: any;
  displaySubReport: boolean;
  selecteReportParam: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any = 0;
  tableRows: any;
  columns: any;
  pdfData: any = [];
  dataKey: any = "SITENAME";
  cols: any = [];
  colId: any;
  mainHead: any;
  grandTotal: any = 0;
  headName: any;
  filterCols: any = [];
  unFColumns: any = [];
  unFTableRows: any = [];

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.breadcrumbService.setItems([
      {
        label: "LBLREPORTMGMT",
        url: "./assets/help/reports-checklist-count-summary.md",
      },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);
    this.translate.get("LBLCHECKLISTCOUNTSUMMARYRPT").subscribe((title) => {
      this.title = title;
    });
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.allSiteSelected = this.parameters["SITEID"];
      this.selectedReportView = this.parameters["designId"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }
    // var parameters = {
    //   reportId: this.reportId,
    //   OBSDATE: "01/01/2019 - 06/30/2019",
    //   DESIGNID: this.selectedReportView,
    //   CHKSUMGROUP1: this.checkSumGroup1,
    //   CHKSUMGROUP2: this.checkSumGroup2,
    //   CHKSUMGROUP3: this.checkSumGroup3
    // };
    this.getReports();

    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = res["Data"];
        this.checkSumLength = this.tableData[0]["SUBRPTPARAM"].split(
          ","
        ).length;
        if (this.tableData.length > 0) {
          var cols = Object.keys(this.tableData[0]);
          this.filterCols = cols;
          var i = 0;
          var fn = this;
          cols.filter(function (item) {
            if (fn.checkSumLength == 1 && i == 1) {
              fn.tableCols.push(item);
              fn.cols.push(item);
            }
            if (fn.checkSumLength == 2 && (i == 1 || i == 3)) {
              fn.tableCols.push(item);
              fn.cols.push(item);
            }
            if (fn.checkSumLength == 3 && (i == 1 || i == 3 || i == 5)) {
              fn.tableCols.push(item);
              fn.cols.push(item);
            }
            i++;
          });

          this.updateRowGroupMetaData();
          this.manageExportData();
          this.isLoaded = true;
        }
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    this.mainHead = [];
    if (this.tableData && this.checkSumLength > 1) {
      for (let i = 0; i < this.tableData.length; i++) {
        let tableData = this.tableData[i];
        let fieldName = this.tableCols[0];
        let sitename = tableData[fieldName];
        if (i == 0) {
          this.rowGroupMetadata[sitename] = { index: 0, size: 1 };
          var dataJson = {};
          dataJson[fieldName] = sitename;
          this.mainHead.push(dataJson);
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData[fieldName];
          if (sitename === previousRowGroup) {
            this.rowGroupMetadata[sitename].size++;
          } else {
            this.rowGroupMetadata[sitename] = { index: i, size: 1 };
            var dataJson = {};
            dataJson[fieldName] = sitename;
            this.mainHead.push(dataJson);
          }
        }
      }
    }
  }

  openSubReport(reportParam, headName) {
    this.headName = headName;
    this.selecteReportParam = reportParam;
    this.displaySubReport = true;
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    if (this.checkSumLength > 1) {
      this.unFColumns = Object.keys(this.mainHead[0]);
    }

    if (this.cols.length != 1) {
      this.cols.splice(0, 1);
    }
    this.cols.push("COUNT");
    if (this.cols.length <= 2) {
      this.colId = "B";
    } else {
      this.colId = "C";
    }
    if (this.tableData.length > 0 && this.cols.length > 0) {
      var singlePdfTotal = 0;
      this.tableData.map((data, key) => {
        this.grandTotal = this.grandTotal + data["COUNT"];
        var dataJson = {};
        var tempDataJson = {};
        var tempData = [];
        tempDataJson[this.tableCols[0]] = data[this.tableCols[0]];
        this.cols.map((item, index) => {
          if (index == 1) {
            singlePdfTotal = singlePdfTotal + data[item];
          }
          if (item == "COUNT") {
            this.translate.get("LBLOBSCHECKLIST").subscribe((resLabel) => {
              dataJson[resLabel] = data[item];
              tempDataJson[resLabel] = data[item];
            });
          } else {
            dataJson[item] = data[item];
            tempDataJson[item] = data[item];
          }
          tempData.push(data[item]);
          if (this.tableData.length - 1 == key) {
            if (item == "COUNT") {
              this.translate.get("LBLOBSCHECKLIST").subscribe((resLabel) => {
                this.columns.push(resLabel);
                this.unFColumns.push(resLabel);
              });
            } else {
              this.columns.push(item);
              this.unFColumns.push(item);
            }
          }
        });
        dataJson[this.tableCols[0]] = data[this.tableCols[0]];
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
        if (this.tableCols.length == 1) {
          this.pdfData.push(tempData);
          if (this.tableData.length - 1 == key) {
            this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
              this.pdfData.push([label, singlePdfTotal]);
            });
          }
        }
      });

      if (this.tableCols.length > 1) {
        // get data for PDF
        var mainColumns = Object.keys(this.mainHead[0]);
        // this.translate.get(["LBLSITENAME"]).subscribe(resLabel => {
        //   mainColumns.push(resLabel["LBLSITENAME"]);
        // });
        var tempMainColums = Object.keys(this.mainHead[0]);
        var subColumns = this.columns;
        var i = 0;
        var pdfGrandTotal = 0;
        this.mainHead.map((head) => {
          var mainRows = [];
          var subRows = [];
          // main Table
          var tempMainRows = [];
          tempMainColums.map((col) => {
            tempMainRows.push(head[col]);
          });
          mainRows.push(tempMainRows);

          // sub Table
          this.tableData.map((data) => {
            var tempSubRows = [];
            if (data[this.tableCols[0]] == head[this.tableCols[0]]) {
              this.cols.map((subCol, index) => {
                tempSubRows.push(data[subCol]);
                if (index == 1 && this.tableCols.length <= 2) {
                  pdfGrandTotal = pdfGrandTotal + data[subCol];
                }
                if (index == 2 && this.tableCols.length > 2) {
                  pdfGrandTotal = pdfGrandTotal + data[subCol];
                }
              });
              subRows.push(tempSubRows);
            }
          });
          if (this.mainHead.length - 1 == i) {
            this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
              if (this.tableCols.length <= 2) {
                subRows.push([label, pdfGrandTotal]);
              }
              if (this.tableCols.length > 2) {
                subRows.push([label, "", pdfGrandTotal]);
              }
            });
          }
          i++;
          this.pdfData.push({
            mainColumns: mainColumns,
            mainRows: mainRows,
            subColumns: subColumns,
            subRows: subRows,
          });
        });
      }
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
    this.unFTableRows.pop();
  }

  exportData(): void {
    const fileName = "CHECKLISTCOUNTSUMMARY";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      var obj = Object.keys(this.unFTableRows[0]);
      // if (this.tableCols.length == 1) {
      var dataJson = {};
      this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
        obj.map((item, key) => {
          if (key == 0) {
            dataJson[item] = label;
          } else if (key == obj.length - 1) {
            dataJson[item] = this.grandTotal;
          } else {
            dataJson[item] = "";
          }
        });
      });
      this.unFTableRows.push(dataJson);
      // }
      this.exportAsExcelFile(this.unFTableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;
      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();

      // Report scope sheet
      let firstWorksheet = workbook.addWorksheet("Report Scope");
      firstWorksheet.addRow([]);
      let firstHeaderRow = firstWorksheet.addRow([this.title]);

      firstWorksheet.getCell("A2").font = {
        name: "Arial Unicode MS",
        family: 4,
        size: 18,
        bold: true,
        color: { argb: "00000000" },
      };
      firstWorksheet.getCell("A2").alignment = {
        vertical: "middle",
        horizontal: "left",
        indent: 1,
      };
      firstWorksheet.getColumn("A").width = 100;
      firstWorksheet.getRow(2).height = 30;

      this.translate
        .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        .subscribe((resLabel) => {
          if (this.parameters["OBSDATE"]) {
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([resLabel["LBLOBSDATE"]]).eachCell((cell) => {
              cell.font = {
                name: "Arial Unicode MS",
                family: 4,
                size: 14,
                bold: true,
                color: { argb: "00000000" },
              };
              cell.alignment = {
                vertical: "middle",
                horizontal: "left",
                indent: 2,
              };
            });

            firstWorksheet.lastRow.height = 25;

            var bothDate = this.parameters["OBSDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            firstWorksheet
              .addRow([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ])
              .eachCell((cell) => {
                cell.font = {
                  name: "Arial Unicode MS",
                  family: 4,
                  size: 12,
                  color: { argb: "00000000" },
                };
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "left",
                  indent: 3,
                };
              });
          }

          if (this.parameters["CREATEDDATE"]) {
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([]);
            firstWorksheet
              .addRow([resLabel["LBLCREATEDDATE"]])
              .eachCell((cell) => {
                cell.font = {
                  name: "Arial Unicode MS",
                  size: 14,
                  bold: true,
                  color: { argb: "00000000" },
                };
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "left",
                  indent: 2,
                };
              });

            firstWorksheet.lastRow.height = 25;
            var bothDate = this.parameters["CREATEDDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            firstWorksheet
              .addRow([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ])
              .eachCell((cell) => {
                cell.font = {
                  name: "Arial Unicode MS",
                  family: 4,
                  size: 12,
                  color: { argb: "00000000" },
                };
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "left",
                  indent: 3,
                };
              });
          }

          firstWorksheet.lastRow.height = 25;
        });

      let worksheet = workbook.addWorksheet("Sheet1");

      if (this.selectedExport == 4 || this.selectedExport == 5) {
        if (this.tableCols.length > 1) {
          for (var i = 1; i <= 4; i++) {
            worksheet.getColumn(i).width = 25;
          }

          let headerRow = worksheet.addRow(unFColumns);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            cell.font = {
              color: { argb: customFontColor },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
            if (this.tableCols.length == 2 && number == 3) {
              cell.alignment = {
                vertical: "middle",
                horizontal: "center",
              };
            }
            if (this.tableCols.length > 2 && number == 4) {
              cell.alignment = {
                vertical: "middle",
                horizontal: "center",
              };
            }
          });

          // Add Data and Conditional Formatting
          let sumOfData = 0;
          let colLen;
          unFTableRows.map((element, index) => {
            let eachRow = [];
            this.unFColumns.map((headers, key) => {
              if (key == 1) {
                sumOfData = sumOfData + element[headers];
              }
              eachRow.push(element[headers]);
            });
            colLen = eachRow.length;
            // this.columns.map((headers, key) => {
            //   eachRow.push(element[headers]);
            // });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (unFTableRows.length - 1 == index) {
                worksheet.addRow(eachRow).eachCell((cell, number) => {
                  // cell.border = {
                  //   bottom: { style: "medium", color: { argb: customColor } },
                  // };
                  if (unFColumns.length <= 2 && number == 2) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                  if (unFColumns.length == 3 && number == 3) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                  if (unFColumns.length == 4 && number == 4) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                  cell.numFmt = this.auth.changeCellValueType(cell.value);
                  // if (eachRow.length == 2 && number == 2) {
                  //   cell.alignment = {
                  //     vertical: "middle",
                  //     horizontal: "center",
                  //   };
                  // }
                  // if (eachRow.length > 2) {
                  //   cell.alignment = {
                  //     vertical: "middle",
                  //     horizontal: "center",
                  //   };
                  // }
                });
              } else {
                var addRow = worksheet.addRow(eachRow);
                addRow.eachCell((cell) => {
                  cell.numFmt = this.auth.changeCellValueType(cell.value);
                });
                addRow.getCell(eachRow.length).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              }
            }
          });
          // if (data.length - 1 == key) {
          this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
            let finalRow;
            if (unFColumns.length <= 2) {
              finalRow = worksheet.addRow([label, this.grandTotal]);
            }
            if (unFColumns.length == 3) {
              finalRow = worksheet.addRow([label, "", this.grandTotal]);
            }
            if (unFColumns.length == 4) {
              finalRow = worksheet.addRow([label, "", "", this.grandTotal]);
            }
            finalRow.getCell(colLen).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            finalRow.getCell(colLen).numFmt = "0";
            finalRow.eachCell((cell) => {
              cell.font = {
                size: 12,
                bold: true,
              };
              cell.border = {
                top: { style: "thin", color: { argb: customColor } },
                // left: { style: "thin", color: {argb: customColor} },
                bottom: { style: "thin", color: { argb: customColor } },
                // right: { style: "thin", color: {argb: customColor} },
              };
            });
          });
          // }
          // });
        } else {
          //Add Header Row
          let headerRow = worksheet.addRow(header);
          headerRow.height = 20;
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            cell.font = {
              color: { argb: customFontColor },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
            if (number == 2) {
              cell.alignment = { vertical: "middle", horizontal: "center" };
            } else {
              cell.alignment = { vertical: "middle", horizontal: "left" };
            }
          });
          // Add Data and Conditional Formatting
          let sumOfData = 0;
          data.forEach((element) => {
            let eachRow = [];
            this.columns.map((headers, key) => {
              if (key == 1) {
                sumOfData = sumOfData + element[headers];
              }
              eachRow.push(element[headers]);
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              var addRow = worksheet.addRow(eachRow);
              addRow.getCell(2).alignment = {
                vertical: "middle",
                horizontal: "center",
              };
              addRow.getCell(2).numFmt = "0";
            }
          });
          worksheet.getColumn(1).width = 40;
          worksheet.getColumn(2).width = 30;

          this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
            let finalRow = worksheet.addRow([label, this.grandTotal]);
            finalRow.getCell(2).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            finalRow.getCell(2).numFmt = "0";
            finalRow.eachCell((cell) => {
              cell.font = {
                size: 12,
                bold: true,
              };
              cell.border = {
                top: { style: "thin", color: { argb: customColor } },
                // left: { style: "thin", color: {argb: customColor} },
                bottom: { style: "thin", color: { argb: customColor } },
                // right: { style: "thin", color: {argb: customColor} },
              };
            });
          });
        }
      } else {
        if (this.tableCols.length > 1) {
          let subHeader = header;
          var mainHeader = Object.keys(this.rowGroupMetadata);
          var beforeDataCount = 1;
          var topHeader = Object.keys(this.mainHead[0]);
          mainHeader.map((header, key) => {
            var rowsData = data.filter((item) => {
              return item[this.tableCols[0]] == header;
            });
            for (var i = 1; i <= 3; i++) {
              worksheet.getColumn(i).width = 25;
            }
            if (key == 0) {
              worksheet.mergeCells(
                "A" + beforeDataCount + ":" + this.colId + beforeDataCount
              );
              worksheet.mergeCells(
                "A" +
                  (beforeDataCount + 1) +
                  ":" +
                  this.colId +
                  (beforeDataCount + 1)
              );
              worksheet.getCell("A" + beforeDataCount).value = topHeader[0];
              // this.translate.get("LBLSITENAME").subscribe(label => {
              // });
              this.translate.get(header).subscribe((label) => {
                worksheet.getCell("A" + (beforeDataCount + 1)).value = label;
              });
              worksheet.getCell("A" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("A" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              beforeDataCount = rowsData.length + 5;
            } else {
              worksheet.mergeCells(
                "A" + beforeDataCount + ":" + this.colId + beforeDataCount
              );
              worksheet.mergeCells(
                "A" +
                  (beforeDataCount + 1) +
                  ":" +
                  this.colId +
                  (beforeDataCount + 1)
              );
              worksheet.getCell("A" + beforeDataCount).value = topHeader[0];
              // this.translate.get("LBLSITENAME").subscribe(label => {
              // });
              worksheet.getCell("A" + (beforeDataCount + 1)).value = header;

              worksheet.getCell("A" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("A" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              beforeDataCount = rowsData.length + 4 + beforeDataCount;
            }

            let headerRow = worksheet.addRow(subHeader);
            // Cell Style : Fill and Border
            headerRow.eachCell((cell, number) => {
              cell.fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: "EEECE1" },
              };
              cell.font = {
                color: { argb: "000000" },
              };
              cell.border = {
                top: { style: "thin" },
                left: { style: "thin" },
                bottom: { style: "thin" },
                right: { style: "thin" },
              };
              if (this.tableCols.length == 2 && number == 2) {
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              }
              if (this.tableCols.length > 2 && number == 3) {
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              }
            });

            // Add Data and Conditional Formatting
            let sumOfData = 0;
            let colLen;
            rowsData.map((element, index) => {
              let eachRow = [];
              this.columns.map((headers, key) => {
                if (key == 1) {
                  sumOfData = sumOfData + element[headers];
                }
                eachRow.push(element[headers]);
              });
              colLen = eachRow.length;
              // this.columns.map((headers, key) => {
              //   eachRow.push(element[headers]);
              // });
              if (element.isDeleted === "Y") {
                let deletedRow = worksheet.addRow(eachRow);
                deletedRow.eachCell((cell, number) => {
                  cell.font = {
                    name: "Calibri",
                    family: 4,
                    size: 11,
                    bold: false,
                    strike: true,
                  };
                });
              } else {
                if (rowsData.length - 1 == index) {
                  worksheet.addRow(eachRow).eachCell((cell, number) => {
                    cell.border = {
                      bottom: { style: "medium", color: { argb: customColor } },
                    };
                    if (eachRow.length == 2 && number == 2) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                      };
                      cell.numFmt = "0";
                    }
                    if (eachRow.length > 2 && number == 3) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                      };
                      cell.numFmt = "0";
                    }
                  });
                } else {
                  var addRow = worksheet.addRow(eachRow);
                  addRow.getCell(eachRow.length).alignment = {
                    vertical: "middle",
                    horizontal: "center",
                  };
                }
              }
            });
            if (mainHeader.length - 1 == key) {
              this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
                let finalRow;
                if (subHeader.length <= 2) {
                  finalRow = worksheet.addRow([label, this.grandTotal]);
                }
                if (subHeader.length > 2) {
                  finalRow = worksheet.addRow([label, "", this.grandTotal]);
                }
                finalRow.getCell(colLen).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
                finalRow.getCell(colLen).numFmt = "0";
                finalRow.eachCell((cell) => {
                  cell.font = {
                    size: 12,
                    bold: true,
                  };
                  cell.border = {
                    top: { style: "thin", color: { argb: customColor } },
                    // left: { style: "thin", color: {argb: customColor} },
                    bottom: { style: "thin", color: { argb: customColor } },
                    // right: { style: "thin", color: {argb: customColor} },
                  };
                });
              });
            }
          });
        } else {
          //Add Header Row
          let headerRow = worksheet.addRow(header);
          headerRow.height = 20;
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            cell.font = {
              color: { argb: customFontColor },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
            if (number == 2) {
              cell.alignment = { vertical: "middle", horizontal: "center" };
            } else {
              cell.alignment = { vertical: "middle", horizontal: "left" };
            }
          });
          // Add Data and Conditional Formatting
          let sumOfData = 0;
          data.forEach((element) => {
            let eachRow = [];
            this.columns.map((headers, key) => {
              if (key == 1) {
                sumOfData = sumOfData + element[headers];
              }
              eachRow.push(element[headers]);
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              var addRow = worksheet.addRow(eachRow);
              addRow.getCell(2).alignment = {
                vertical: "middle",
                horizontal: "center",
              };
              // addRow.getCell(2).numFmt = "0";
            }
          });
          worksheet.getColumn(1).width = 40;
          worksheet.getColumn(2).width = 30;

          this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
            let finalRow = worksheet.addRow([label, this.grandTotal]);
            finalRow.getCell(2).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            // finalRow.getCell(2).numFmt = "0";
            finalRow.eachCell((cell) => {
              cell.font = {
                size: 12,
                bold: true,
              };
              cell.border = {
                top: { style: "thin", color: { argb: customColor } },
                // left: { style: "thin", color: {argb: customColor} },
                bottom: { style: "thin", color: { argb: customColor } },
                // right: { style: "thin", color: {argb: customColor} },
              };
            });
          });
        }
      }

      worksheet.addRow([]);
      //Generate Excel File with given name
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape");
      pdf.text(this.title, 15, 20);
      let finalX = 10;
      this.translate
        .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        .subscribe((resLabel) => {
          if (this.parameters["OBSDATE"]) {
            let obsDate = [];
            var bothDate = this.parameters["OBSDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            obsDate.push([
              fDate +
                " - " +
                tDate +
                " (" +
                this.auth.UserInfo["dateFormat"].toUpperCase() +
                ")",
            ]);
            pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
              startY: finalX + 13,
              headStyles: {
                fillColor: customColor,
                textColor: customFontColor,
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
            });
            finalX = pdf.previousAutoTable.finalX;
          }

          if (this.parameters["CREATEDDATE"]) {
            let obsDate = [];
            var bothDate = this.parameters["CREATEDDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            obsDate.push([
              fDate +
                " - " +
                tDate +
                " (" +
                this.auth.UserInfo["dateFormat"].toUpperCase() +
                ")",
            ]);
            pdf.autoTable([resLabel["LBLCREATEDDATE"]], obsDate, {
              startY: finalX,
              headStyles: {
                fillColor: customColor,
                textColor: customFontColor,
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
            });
            finalX = pdf.previousAutoTable.finalX;
          }
          pdf.addPage();
        });

      // report view
      let finalY = 10;
      if (this.tableCols.length > 1) {
        var i = 0;
        this.pdfData.map((data) => {
          pdf.autoTable(data.mainColumns, data.mainRows, {
            startY: finalY,
            headStyles: {
              fillColor: customColor,
              textColor: customFontColor,
            },
            alternateRowStyles: {
              fillColor: "#FFFFFF",
            },
          });
          finalY = pdf.previousAutoTable.finalY;
          var tableColCount = this.tableCols.length;
          var fn = this;
          if (this.pdfData.length - 1 == i) {
            pdf.autoTable(data.subColumns, data.subRows, {
              startY: finalY,
              headStyles: {
                fillColor: "#D8D8D8",
                textColor: "#000000",
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
              columnStyles: {
                0: { cellWidth: 20 },
              },
              willDrawCell: this.drawCell,
              didParseCell: (cellData) => {
                if (tableColCount == 2) {
                  fn.alignCol(cellData, customColor, data.subRows.length, 1);
                } else {
                  fn.alignColSec(cellData, customColor, data.subRows.length, 1);
                }
              },
            });
          } else {
            pdf.autoTable(data.subColumns, data.subRows, {
              startY: finalY,
              headStyles: {
                fillColor: "#D8D8D8",
                textColor: "#000000",
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
              columnStyles: {
                0: { cellWidth: 20 },
              },
              didParseCell: (cellData) => {
                if (tableColCount == 2) {
                  fn.alignCol(cellData, customColor, data.subRows.length, 0);
                } else {
                  fn.alignColSec(cellData, customColor, data.subRows.length, 0);
                }
              },
            });
          }
          i++;
          finalY = pdf.previousAutoTable.finalY + 10;
        });
      } else {
        var fn = this;
        pdf.autoTable(this.columns, this.pdfData, {
          startY: finalY,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          columnStyles: {
            0: { cellWidth: 20 },
          },
          willDrawCell: this.drawCell,
          didParseCell: (data) => {
            // Instance of <tr> element
            fn.alignCol(data, customColor, fn.pdfData.length, 1);
          },
        });
      }

      this.selectedExport = 0;
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }

  alignCol = function (data, customColor, rowCount, status) {
    if (data.column.index == 1) {
      data.cell.styles.halign = "center";
    }
    if (status == 1) {
      var s = data.cell.styles;
      if (data.row.index == rowCount - 1) {
        s.lineColor = "#" + customColor;
        s.lineWidth = 0.5;
        s.borders = "t";
      }
    }
  };
  alignColSec = function (data, customColor, rowCount, status) {
    if (data.column.index == 2) {
      data.cell.styles.halign = "center";
    }
    if (status == 1) {
      var s = data.cell.styles;
      if (data.row.index == rowCount - 1) {
        s.lineColor = "#" + customColor;
        s.lineWidth = 0.5;
        s.borders = "t";
      }
    }
  };

  drawCell = function (data) {
    var doc = data.doc;
    var rows = data.table.body;
    if (rows.length === 1) {
    } else if (data.row.index === rows.length - 1) {
      doc.setFontStyle("bold");
      doc.setFontSize("12");
    }
  };
}
