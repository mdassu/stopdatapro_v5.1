import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { formatDate } from "@angular/common";
import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";

import { TranslateService } from "@ngx-translate/core";
import { isArray } from "util";
import { keyframes } from "@angular/animations";
import { fn, THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { AuthenticationService } from "src/app/_services/authentication.service";

@Component({
  selector: "app-stats-by-category",
  templateUrl: "./stats-by-category.component.html",
  styleUrls: ["./stats-by-category.component.css"],
})
export class StatsByCategoryComponent implements OnInit {
  @Input() reportId: any;
  tableCols: any[] = [];
  selectedReportView: any;
  selectedSiteId: any = "";
  selectedYear: any = "";
  cols: any = [];
  tableData: any = [];
  indexData: any = [];
  title: any;
  isLoaded: boolean;
  errormsg: any;
  rowGroupMetadata: any;
  reportView: any = [];
  siteList: any = [{ label: "All", value: "" }];
  yearList: any = [{ label: "All", value: "" }];
  parameters: any;
  allSiteSelected: any;
  colCount: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any = 0;
  tableRows: any;
  columns: any;
  unFTableRows: any;
  unFColumns: any;
  pdfData: any = [];
  dataKey: any = "MAINCATEGORYNAME";
  colId: any;
  mainHead: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.translate.get("LBLSTATSBYCATGRPT").subscribe((title) => {
      this.title = title;
    });

    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT", url: "./assets/help/stats-by-category.md" },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);

    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.selectedReportView = this.parameters["designId"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }
    this.getReports();

    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (isArray(res["designData"])) {
        this.reportView = [];
        var designData = res["designData"];
        designData.map((item) => {
          this.translate.get(item.RPTNAME).subscribe((label) => {
            this.reportView.push({
              label: label,
              value: item.DESIGNID,
            });
          });
        });
      }
      if (res["status"] == true) {
        this.tableData = res["Data"];
        if (this.siteList.length == 1) {
          res["sitelist"].map((site) => {
            this.siteList.push({ label: site.SITENAME, value: site.SITEID });
          });
        }
        if (this.yearList.length == 1) {
          res["year"].map((item) => {
            this.yearList.push({ label: item.YEAR, value: item.YEAR });
          });
        }
        if (this.selectedReportView == 42) {
          var tempColumns = [
            "MAINCATEGORYID",
            "MAINCATEGORYNAME",
            "SORTBY",
            "TOTSAFECNT",
            "TOTUNSAFECNT",
          ];

          if (this.tableData.length > 0) {
            var keys = Object.keys(this.tableData[0]);
          }
          this.cols = [];
          keys.map((label) => {
            if (tempColumns.indexOf(label) == -1) {
              this.translate.get("LBL" + label).subscribe((resLabel) => {
                this.cols.push({
                  field: label,
                  header: resLabel,
                });
              });
            }
          });
          this.colCount = this.cols.length;
        } else {
          var tempColumns = [
            "MAINCATEGORYID",
            "MAINCATEGORYNAME",
            "TOTSAFECNT",
            "TOTUNSAFECNT",
            "SUBCATEGORYID",
          ];
          this.cols = [];
          this.translate
            .get(["LBLSUBCATNAME", "LBLSAFE", "LBLUNSAFE"])
            .subscribe((resLabel) => {
              this.cols = [
                { field: "SUBCATEGORYNAME", header: resLabel["LBLSUBCATNAME"] },
                { field: "SAFECNT", header: resLabel["LBLSAFE"] },
                { field: "UNSAFECNT", header: resLabel["LBLUNSAFE"] },
              ];
            });
        }

        this.updateRowGroupMetaData();
        this.manageExportData();
        this.isLoaded = true;
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.tableData) {
      this.mainHead = [];
      for (let i = 0; i < this.tableData.length; i++) {
        let tableData = this.tableData[i];
        let maincategory = tableData.MAINCATEGORYNAME;
        if (i == 0) {
          this.rowGroupMetadata[maincategory] = { index: 0, size: 1 };
          this.mainHead.push({
            MAINCATEGORYNAME: maincategory,
            TOTSAFECNT: tableData.TOTSAFECNT,
            TOTUNSAFECNT: tableData.TOTUNSAFECNT,
          });
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.MAINCATEGORYNAME;
          if (maincategory === previousRowGroup) {
            this.rowGroupMetadata[maincategory].size++;
          } else {
            this.rowGroupMetadata[maincategory] = { index: i, size: 1 };
            this.mainHead.push({
              MAINCATEGORYNAME: maincategory,
              TOTSAFECNT: tableData.TOTSAFECNT,
              TOTUNSAFECNT: tableData.TOTUNSAFECNT,
            });
          }
        }
      }
    }
  }

  changeReport() {
    this.isLoaded = false;
    this.parameters["designId"] = this.selectedReportView;
    this.parameters["YEAR"] = this.selectedYear;
    if (this.selectedSiteId == 0) {
      this.parameters["SITEID"] = this.allSiteSelected;
    } else {
      this.parameters["SITEID"] = this.selectedSiteId;
    }
    this.getReports();
  }

  manageExportData(): void {
    this.unFTableRows = [];
    this.unFColumns = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempDataJson = {};
        this.translate
          .get(["LBLMAINCATEGORY", "LBLTOTSAFE", "LBLTOTUNSAFE"])
          .subscribe((resLabel) => {
            if (key == 0) {
              this.unFColumns.push(resLabel["LBLMAINCATEGORY"]);
              this.unFColumns.push(resLabel["LBLTOTSAFE"]);
              this.unFColumns.push(resLabel["LBLTOTUNSAFE"]);
            }

            tempDataJson[resLabel["LBLMAINCATEGORY"]] =
              data["MAINCATEGORYNAME"];
            tempDataJson[resLabel["LBLTOTSAFE"]] = data["TOTSAFECNT"];
            tempDataJson[resLabel["LBLTOTUNSAFE"]] = data["TOTUNSAFECNT"];
          });
        this.cols.map((item, index) => {
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];

          if (this.tableData.length - 1 == key) {
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
            if (this.cols.length - 1 == index) {
              this.colId = String.fromCharCode(
                97 + this.columns.length - 1
              ).toUpperCase();
            }
          }
        });
        dataJson["MAINCATEGORYNAME"] = data["MAINCATEGORYNAME"];
        dataJson["TOTSAFECNT"] = data["TOTSAFECNT"];
        dataJson["TOTUNSAFECNT"] = data["TOTUNSAFECNT"];
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
      });

      // get data for PDF
      var mainColumns = [];
      this.translate
        .get(["LBLMAINCATEGORY", "LBLTOTSAFE", "LBLTOTUNSAFE"])
        .subscribe((resLabel) => {
          mainColumns.push(resLabel["LBLMAINCATEGORY"]);
          mainColumns.push(resLabel["LBLTOTSAFE"]);
          mainColumns.push(resLabel["LBLTOTUNSAFE"]);
        });
      var tempMainColums = Object.keys(this.mainHead[0]);
      var subColumns = this.columns;
      this.mainHead.map((head) => {
        var mainRows = [];
        var subRows = [];
        // main Table
        var tempMainRows = [];
        tempMainColums.map((col) => {
          tempMainRows.push(head[col]);
        });
        mainRows.push(tempMainRows);

        // sub Table
        this.tableData.map((data) => {
          var tempSubRows = [];
          if (data["MAINCATEGORYNAME"] == head["MAINCATEGORYNAME"]) {
            this.cols.map((subCol) => {
              if (subCol.field == "METRICS") {
                this.translate.get(data[subCol.field]).subscribe((val) => {
                  tempSubRows.push(val);
                });
              } else {
                tempSubRows.push(data[subCol.field]);
              }
            });
            subRows.push(tempSubRows);
          }
        });
        this.pdfData.push({
          mainColumns: mainColumns,
          mainRows: mainRows,
          subColumns: subColumns,
          subRows: subRows,
        });
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    const fileName = "STATSBYCATEGORY";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      this.tableRows.map((item) => {
        if (item["Metrics"]) {
          this.translate.get([item["Metrics"]]).subscribe((resLabel) => {
            item["Metrics"] = resLabel[item["Metrics"]];
          });
        }
      });
      this.exportAsExcelFile(this.unFTableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;

      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      // Report scope sheet
      let firstWorksheet = workbook.addWorksheet("Report Scope");
      firstWorksheet.addRow([]);
      let firstHeaderRow = firstWorksheet.addRow([this.title]);

      firstWorksheet.getCell("A2").font = {
        name: "Arial Unicode MS",
        family: 4,
        size: 18,
        bold: true,
        color: { argb: "00000000" },
      };
      firstWorksheet.getCell("A2").alignment = {
        vertical: "middle",
        horizontal: "left",
        indent: 1,
      };
      firstWorksheet.getColumn("A").width = 100;
      firstWorksheet.getRow(2).height = 30;

      this.translate
        .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        .subscribe((resLabel) => {
          if (this.parameters["OBSDATE"]) {
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([resLabel["LBLOBSDATE"]]).eachCell((cell) => {
              cell.font = {
                name: "Arial Unicode MS",
                family: 4,
                size: 14,
                bold: true,
                color: { argb: "00000000" },
              };
              cell.alignment = {
                vertical: "middle",
                horizontal: "left",
                indent: 2,
              };
            });

            firstWorksheet.lastRow.height = 25;
            var bothDate = this.parameters["OBSDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            firstWorksheet
              .addRow([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ])
              .eachCell((cell) => {
                cell.font = {
                  name: "Arial Unicode MS",
                  family: 4,
                  size: 12,
                  color: { argb: "00000000" },
                };
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "left",
                  indent: 3,
                };
              });
          }

          firstWorksheet.lastRow.height = 25;
        });

      // Report View sheet

      let worksheet = workbook.addWorksheet("Sheet1");

      if (this.selectedExport == 4 || this.selectedExport == 5) {
        let headerRow = worksheet.addRow(unFColumns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          if (number > 3) {
            cell.alignment = {
              vertical: "middle",
              horizontal: "center",
            };
          }
        });

        // Add Data and Conditional Formatting
        unFTableRows.map((element, index) => {
          let eachRow = [];
          unFColumns.map((headers, key) => {
            this.translate.get("LBLMETRICS").subscribe((label) => {
              if (headers == label) {
                this.translate.get(element[headers]).subscribe((val) => {
                  eachRow.push(val);
                });
              } else {
                eachRow.push(element[headers]);
              }
            });
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            if (unFTableRows.length - 1 == index) {
              worksheet.addRow(eachRow).eachCell((cell, number) => {
                if (number > 3) {
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "center",
                  };
                }
              });
            } else {
              worksheet.addRow(eachRow).eachCell((cell, number) => {
                if (number > 3) {
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "center",
                  };
                }
              });
            }
          }
        });
      } else {
        let subHeader = header;
        var beforeDataCount = 1;
        this.mainHead.map((header, key) => {
          var rowsData = data.filter((item) => {
            return item[this.dataKey] == header[this.dataKey];
          });
          var headerCount;
          if (this.selectedReportView == 42) {
            headerCount = subHeader.length - 2;
          } else {
            headerCount = subHeader.length;
          }
          for (var i = 1; i <= headerCount; i++) {
            worksheet.getColumn(i).width = 25;
          }
          if (key == 0) {
            worksheet.mergeCells(
              "C" + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              "C" +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                +(beforeDataCount + 1)
            );
            this.translate
              .get(["LBLMAINCATEGORY", "LBLTOTSAFE", "LBLTOTUNSAFE"])
              .subscribe((resLabel) => {
                worksheet.getCell("A" + beforeDataCount).value =
                  resLabel["LBLMAINCATEGORY"];
                worksheet.getCell("B" + beforeDataCount).value =
                  resLabel["LBLTOTSAFE"];
                worksheet.getCell("C" + beforeDataCount).value =
                  resLabel["LBLTOTUNSAFE"];
                worksheet.getCell("B" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
                worksheet.getCell("C" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              });
            worksheet.getCell("A" + (beforeDataCount + 1)).value =
              header["MAINCATEGORYNAME"];
            worksheet.getCell("B" + (beforeDataCount + 1)).value =
              header["TOTSAFECNT"];
            worksheet.getCell("C" + (beforeDataCount + 1)).value =
              header["TOTUNSAFECNT"];
            worksheet.getCell("B" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("C" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("B" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("C" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("B" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("C" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 5;
          } else {
            worksheet.mergeCells(
              "C" + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              "C" +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                +(beforeDataCount + 1)
            );
            this.translate
              .get(["LBLMAINCATEGORY", "LBLTOTSAFE", "LBLTOTUNSAFE"])
              .subscribe((resLabel) => {
                worksheet.getCell("A" + beforeDataCount).value =
                  resLabel["LBLMAINCATEGORY"];
                worksheet.getCell("B" + beforeDataCount).value =
                  resLabel["LBLTOTSAFE"];
                worksheet.getCell("C" + beforeDataCount).value =
                  resLabel["LBLTOTUNSAFE"];
                worksheet.getCell("B" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
                worksheet.getCell("C" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              });
            worksheet.getCell("A" + (beforeDataCount + 1)).value =
              header["MAINCATEGORYNAME"];
            worksheet.getCell("B" + (beforeDataCount + 1)).value =
              header["TOTSAFECNT"];
            worksheet.getCell("C" + (beforeDataCount + 1)).value =
              header["TOTUNSAFECNT"];
            worksheet.getCell("B" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("C" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("B" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("C" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("B" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("C" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 4 + beforeDataCount;
          }
          let headerRow = worksheet.addRow(subHeader);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: "EEECE1" },
            };
            cell.font = {
              color: { argb: "000000" },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
            if (number > 1) {
              cell.alignment = {
                vertical: "middle",
                horizontal: "center",
              };
            }
          });

          // Add Data and Conditional Formatting
          rowsData.map((element, index) => {
            let eachRow = [];
            this.columns.map((headers, key) => {
              this.translate.get("LBLMETRICS").subscribe((label) => {
                if (headers == "Metrics") {
                  this.translate.get(element[headers]).subscribe((val) => {
                    eachRow.push(val);
                  });
                } else {
                  eachRow.push(element[headers]);
                }
              });
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (rowsData.length - 1 == index) {
                worksheet.addRow(eachRow).eachCell((cell, number) => {
                  cell.border = {
                    bottom: { style: "medium", color: { argb: customColor } },
                  };
                  if (number > 1) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                });
              } else {
                worksheet.addRow(eachRow).eachCell((cell, number) => {
                  cell.border = {
                    bottom: { style: "medium", color: { argb: customColor } },
                  };
                  if (number > 1) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                });
              }
            }
          });
        });
      }

      worksheet.addRow([]);
      //Generate Excel File with given name
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape");
      pdf.text(this.title, 15, 20);
      let finalX = 10;
      this.translate
        .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        .subscribe((resLabel) => {
          if (this.parameters["OBSDATE"]) {
            let obsDate = [];
            var bothDate = this.parameters["OBSDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            obsDate.push([
              fDate +
                " - " +
                tDate +
                " (" +
                this.auth.UserInfo["dateFormat"].toUpperCase() +
                ")",
            ]);
            pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
              startY: finalX + 13,
              headStyles: {
                fillColor: customColor,
                textColor: customFontColor,
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
            });
            finalX = pdf.previousAutoTable.finalX;
          }

          pdf.addPage();
        });
      let finalY = 10;
      var fn = this;
      this.pdfData.map((data) => {
        pdf.autoTable(data.mainColumns, data.mainRows, {
          startY: finalY,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          didParseCell: (data) => {
            fn.alignCol(data);
          },
        });
        finalY = pdf.previousAutoTable.finalY;
        pdf.autoTable(data.subColumns, data.subRows, {
          startY: finalY,
          headStyles: {
            fillColor: "#D8D8D8",
            textColor: "#000000",
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          didParseCell: (data) => {
            fn.alignCol(data);
          },
        });
        finalY = pdf.previousAutoTable.finalY + 10;
      });
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }

  alignCol = function (data) {
    if (data.column.index > 0) {
      data.cell.styles.halign = "center";
    }
  };
}
