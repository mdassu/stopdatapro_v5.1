import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { formatDate } from "@angular/common";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { AuthenticationService } from "src/app/_services/authentication.service";

@Component({
  selector: "app-corrective-actions-coverage-report",
  templateUrl: "./corrective-actions-coverage-report.component.html",
  styleUrls: ["./corrective-actions-coverage-report.component.css"],
})
export class CorrectiveActionsCoverageReportComponent implements OnInit {
  @Input() reportId: any;
  cols: any[];
  graphicalCols: any[];
  tableData: any = [];
  title: any;
  graphicalData: any = [];
  siteList: any = [{ label: "All", value: "" }];
  isLoaded: boolean;
  selectedSiteName: any = "";
  selectedReportView: any;
  errormsg: any;
  parameters: any;
  allSiteSelected: any;
  displaySubReport: any;
  selectedStatusId: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  pdfRows: any = [];
  chart: any;
  headName: any;
  grandTotal: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.translate.get("LBLUNSAFEACTCOVERREPORT").subscribe((title) => {
      this.title = title;
    });

    this.breadcrumbService.setItems([
      {
        label: "LBLREPORTMGMT",
        url: "./assets/help/corrective-actions-coverage-report.md",
      },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);

    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.allSiteSelected = this.parameters["SITEID"];
      this.selectedReportView = this.parameters["designId"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }
    this.getReports();
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = res["Data"];
        var fn = this;
        if (this.siteList.length == 1) {
          var sitelist = res["sitelist"];
          sitelist.forEach(function (site) {
            fn.siteList.push({ label: site.SITENAME, value: site.SITEID });
          });
        }

        var i = 1;
        var graphValue = [];
        this.tableData.map((data) => {
          this.translate.get(data.CARDSTATUS).subscribe((label) => {
            data.CARDSTATUS = label;
          });

          graphValue.push({
            labelFontColor: "#" + customColor,
            label: data.CARDSTATUS,
            value: data.UNSAFE,
          });

          if (fn.tableData.length == i) {
            setTimeout(function () {
              fn.graphicalData = {
                chart: {
                  showPercentInTooltip: "1",
                  showValues: "1",
                  showPercentValues: "0",
                  decimals: "1",
                  theme: "fusion",
                  labelFontColor: "#" + customColor,
                  baseFontColor: "#" + customColor,
                  legendItemFontColor: "#" + customColor,
                },
                data: graphValue,
              };
              fn.isLoaded = true;
            }, 1000);
          }
          i++;
        });

        this.translate
          .get(["LBLUACACARDSTATUS", "LBLUNSAFECOUNT"])
          .subscribe((res) => {
            this.cols = [
              {
                field: "CARDSTATUS",
                header: res["LBLUACACARDSTATUS"],
              },
              { field: "UNSAFE", header: res["LBLUNSAFECOUNT"] },
            ];
          });
        this.manageExportData();
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  changeLBLUNSAFEACTCOVERREPORT() {
    this.tableData = [];
    this.isLoaded = false;
    this.parameters["designId"] = this.selectedReportView;
    if (this.selectedSiteName == 0) {
      this.parameters["SITEID"] = this.allSiteSelected;
    } else {
      this.parameters["SITEID"] = this.selectedSiteName;
    }
    this.getReports();
  }

  openSubReport(statusId, headName) {
    this.selectedStatusId = statusId;
    this.headName = headName;
    this.displaySubReport = true;
  }

  barClick(data) {
    this.tableData.map((item) => {
      if (item["CARDSTATUS"] == data["dataObj"]["categoryLabel"]) {
        this.openSubReport(item["STATUS"], item["CARDSTATUS"]);
      }
    });
  }

  manageExportData(): void {
    this.tableRows = [];
    this.pdfRows = [];
    this.columns = [];
    this.grandTotal = 0;
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.tableData.map((data) => {
        this.grandTotal = this.grandTotal + data["UNSAFE"];
        var dataJson = {};
        dataJson[this.cols[0].header] = data[this.cols[0].field];
        dataJson[this.cols[1].header] = data[this.cols[1].field];
        this.tableRows.push(dataJson);
        this.pdfRows.push([data[this.cols[0].field], data[this.cols[1].field]]);
      });
      this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
        var totalJson = [label, this.grandTotal];
        this.pdfRows.push(totalJson);
      });
    }
    this.columns[0] = this.cols[0].header;
    this.columns[1] = this.cols[1].header;
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    var fileName = "CORRECTIVEACTIONCOVERAGEREPORT";
    var encodedData;
    this.chart.getSVGString((svg) => {
      this.reportsService.svgString2Image(
        svg,
        1030,
        524,
        "png",
        function (base64String) {
          encodedData = base64String;
        }
      );
    });
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    setTimeout(() => {
      if (this.selectedExport == 6) {
        this.exportAsExcelFile(this.tableRows, fileName);
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
      } else if (
        this.selectedExport == 1 ||
        this.selectedExport == 3 ||
        this.selectedExport == 4 ||
        this.selectedExport == 5
      ) {
        var EXCEL_EXTENSION;
        if (this.selectedExport == 1 || this.selectedExport == 5) {
          EXCEL_EXTENSION = ".xlsx";
        } else if (this.selectedExport == 3 || this.selectedExport == 4) {
          EXCEL_EXTENSION = ".xls";
        }
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        //Excel Title, Header, Data
        const header = this.columns;
        const data = this.tableRows;

        const EXCEL_TYPE =
          "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

        //Create workbook and worksheet
        let workbook = new ExcelJS.Workbook();
        // Report scope sheet
        let firstWorksheet = workbook.addWorksheet("Report Scope");
        firstWorksheet.addRow([]);
        let firstHeaderRow = firstWorksheet.addRow([this.title]);

        firstWorksheet.getCell("A2").font = {
          name: "Arial Unicode MS",
          family: 4,
          size: 18,
          bold: true,
          color: { argb: "00000000" },
        };
        firstWorksheet.getCell("A2").alignment = {
          vertical: "middle",
          horizontal: "left",
          indent: 1,
        };
        firstWorksheet.getColumn("A").width = 100;
        firstWorksheet.getRow(2).height = 30;

        this.translate
          .get(["LBLOBSDATE", "LBLRESPONSEREQUIREDDATE", "LBLRESPONDEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              firstWorksheet.addRow([]);
              firstWorksheet.addRow([]);
              firstWorksheet
                .addRow([resLabel["LBLOBSDATE"]])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 14,
                    bold: true,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 2,
                  };
                });

              firstWorksheet.lastRow.height = 25;
              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              firstWorksheet
                .addRow([
                  fDate +
                    " - " +
                    tDate +
                    " (" +
                    this.auth.UserInfo["dateFormat"].toUpperCase() +
                    ")",
                ])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 12,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 3,
                  };
                });
            }

            if (this.parameters["RESDATEREQ"]) {
              firstWorksheet.addRow([]);
              firstWorksheet.addRow([]);
              firstWorksheet
                .addRow([resLabel["LBLRESPONSEREQUIREDDATE"]])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    size: 14,
                    bold: true,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 2,
                  };
                });

              firstWorksheet.lastRow.height = 25;
              var bothDate = this.parameters["RESDATEREQ"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              firstWorksheet
                .addRow([
                  fDate +
                    " - " +
                    tDate +
                    " (" +
                    this.auth.UserInfo["dateFormat"].toUpperCase() +
                    ")",
                ])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 12,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 3,
                  };
                });
            }

            if (this.parameters["RESPONDEDDATE"]) {
              firstWorksheet.addRow([]);
              firstWorksheet.addRow([]);
              firstWorksheet
                .addRow([resLabel["LBLRESPONDEDDATE"]])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    size: 14,
                    bold: true,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 2,
                  };
                });

              firstWorksheet.lastRow.height = 25;
              var bothDate = this.parameters["RESPONDEDDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              firstWorksheet
                .addRow([
                  fDate +
                    " - " +
                    tDate +
                    " (" +
                    this.auth.UserInfo["dateFormat"].toUpperCase() +
                    ")",
                ])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 12,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 3,
                  };
                });
            }

            firstWorksheet.lastRow.height = 25;
          });

        // Report View sheet
        let worksheet = workbook.addWorksheet(fileName);

        worksheet.getColumn(1).width = 40;
        worksheet.getColumn(2).width = 50;
        worksheet.getRow(1).height = 250;
        worksheet.mergeCells("A1:B1");
        var imageId = workbook.addImage({
          base64: encodedData,
          extension: "png",
        });
        worksheet.addImage(imageId, {
          tl: { col: 0, row: 0 },
          ext: { width: 800, height: 300 },
        });
        //Add Header Row
        let headerRow = worksheet.addRow(header);
        headerRow.height = 20;
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          if (number == 2) {
            cell.alignment = { vertical: "middle", horizontal: "center" };
          } else {
            cell.alignment = { vertical: "middle", horizontal: "left" };
          }
        });
        // Add Data and Conditional Formatting
        let sumOfData = 0;
        data.forEach((element) => {
          let eachRow = [];
          this.columns.map((headers, key) => {
            if (key == 1) {
              sumOfData = sumOfData + element[headers];
            }
            eachRow.push(element[headers]);
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            worksheet.addRow(eachRow).getCell(2).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
          }
        });
        this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
          let finalRow = worksheet.addRow([label, sumOfData]);
          finalRow.getCell(2).alignment = {
            vertical: "middle",
            horizontal: "center",
          };
          finalRow.eachCell((cell) => {
            cell.font = {
              size: 12,
              bold: true,
            };
          });
        });
        // worksheet.getColumn(1).width = 40;
        // worksheet.getColumn(2).width = 30;
        worksheet.addRow([]);
        //Generate Excel File with given name
        workbook.xlsx.writeBuffer().then((data) => {
          let blob = new Blob([data], { type: EXCEL_TYPE });
          fs.saveAs(
            blob,
            fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
          );
        });
      } else if (this.selectedExport == 2) {
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        var pdf = new jsPDF("landscape");
        // report scope
        pdf.text(this.title, 15, 20);
        let finalX = 10;
        this.translate
          .get(["LBLOBSDATE", "LBLRESPONSEREQUIREDDATE", "LBLRESPONDEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              let obsDate = [];
              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
                startY: finalX + 13,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }

            if (this.parameters["RESDATEREQ"]) {
              let obsDate = [];
              var bothDate = this.parameters["RESDATEREQ"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLRESPONSEREQUIREDDATE"]], obsDate, {
                startY: finalX,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }

            if (this.parameters["RESPONDEDDATE"]) {
              let obsDate = [];
              var bothDate = this.parameters["RESPONDEDDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLRESPONDEDDATE"]], obsDate, {
                startY: finalX,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }
            pdf.addPage();
          });

        // report view
        pdf.addImage(encodedData, "PNG", 20, 0, 220, 120);
        pdf.autoTable(this.columns, this.pdfRows, {
          startY: 130,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          willDrawCell: this.drawCell,
        });
        this.parameters[""];
        pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
      }
    }, 1000);
  }

  initialized($event) {
    this.chart = $event.chart; // saving chart instance
  }

  drawCell = function (data) {
    var doc = data.doc;
    var rows = data.table.body;
    if (rows.length === 1) {
    } else if (data.row.index === rows.length - 1) {
      doc.setFontStyle("bold");
      doc.setFontSize("12");
    }
  };
}
