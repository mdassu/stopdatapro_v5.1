import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { isArray } from "util";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { formatDate } from "@angular/common";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";

@Component({
  selector: "app-corrective-action-report",
  templateUrl: "./corrective-action-report.component.html",
  styleUrls: ["./corrective-action-report.component.css"],
})
export class CorrectiveActionReportComponent implements OnInit {
  @Input() reportId: any;
  cols: any[];
  graphicalCols: any[];
  tableData: any = [];
  title: any;
  LBLOPENWITHINRD: any = [];
  LBLOPENBEYONDRD: any = [];
  LBLCLOSEDWITHINRD: any = [];
  LBLCLOSEDBEYONDRD: any = [];
  graphicalData: any = [];
  reportView: any = [];
  siteList: any = [{ label: "All", value: 0 }];
  reportType: any;
  siteName: any;
  selectedReportView: any;
  selectedSiteName: any = 0;
  isLoaded: boolean;
  graphCORRACTIONRPT: any;
  errormsg: any;
  parameters: any;
  allSiteSelected: any;
  dateFormat: any;
  selectedCorrectionId: any;
  displaySubReport: boolean;
  subReportId: any = 14;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  unFTableRows: any;
  unFColumns: any;
  pdfRows: any = [];
  chart: any;
  rowGroupMetadata: any;
  dataKey: any = "RESPONSESTATUS";
  headName: any;
  dateFormatType: any;
  mainHead: any = [];
  pdfData: any = [];
  grandTotal: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.dateFormatType = this.auth.UserInfo["dateFormat"];
    this.dateFormat = this.auth.UserInfo["dateFormat"].toLowerCase();
    this.translate.get("LBLCORRACTIONRPT").subscribe((title) => {
      this.title = title;
    });
    this.breadcrumbService.setItems([
      {
        label: "LBLREPORTMGMT",
        url: "./assets/help/corrective-action-report.md",
      },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.allSiteSelected = this.parameters["SITEID"];
      this.selectedReportView = this.parameters["designId"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }
    this.getReports();
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (isArray(res["designData"])) {
        this.reportView = [];
        var designData = res["designData"];
        designData.map((item) => {
          this.translate.get(item.RPTNAME).subscribe((label) => {
            this.reportView.push({
              label: label,
              value: item.DESIGNID,
            });
          });
        });
      }
      if (res["status"] == true) {
        this.translate
          .get([
            "LBLHIGH",
            "LBLLOW",
            "LBLMEDIUM",
            "LBLOPEN",
            "LBLCOMPLETED",
            "LBLALTERNATEACTION",
            "LBLOPENWITHINRD",
            "LBLOPENBEYONDRD",
            "LBLCLOSEDWITHINRD",
            "LBLCLOSEDBEYONDRD",
            "LBLCAID",
            "LBLOBSDATE",
            "LBLRESPONSIBLEPERSON",
            "LBLACTOWNER",
            "LBLRESPONSEREQUIREDDATE",
            "LBLRESPONDEDDATE",
            "LBLPRIORITY",
            "LBLSTATUS",
            "LBLACTIONPLANNED",
            "LBLACTIONPERFORMED",
            "LBLENTEREDBY",
            "LBLOPENWITHINRD",
            "LBLOPENBEYONDRD",
            "LBLCLOSEDWITHINRD",
            "LBLCLOSEDBEYONDRD",
            "LBLCORRACTIVEACTIONSTATUS",
            "LBLCORRECTIVEACTIONCOUNT",
          ])
          .subscribe((resLabel) => {
            if (this.selectedReportView == 44) {
              this.subReportId = 14;
              var sitelist = res["sitelist"];
              this.tableData = res["Details"];
              var fn = this;
              if (this.siteList.length == 1) {
                sitelist.forEach(function (site) {
                  fn.siteList.push({
                    label: site.SITENAME,
                    value: site.SITEID,
                  });
                });
              }
              this.LBLOPENWITHINRD = [];
              this.LBLOPENBEYONDRD = [];
              this.LBLCLOSEDWITHINRD = [];
              this.LBLCLOSEDBEYONDRD = [];
              var i = 1;
              this.tableData.forEach(function (data) {
                if (data.PRIORITY == "LBLHIGH") {
                  data.PRIORITY = resLabel["LBLHIGH"];
                } else if (data.PRIORITY == "LBLLOW") {
                  data.PRIORITY = resLabel["LBLLOW"];
                } else if (data.PRIORITY == "LBLMEDIUM") {
                  data.PRIORITY = resLabel["LBLMEDIUM"];
                }

                if (data.STATUS == "LBLOPEN") {
                  data.STATUS = resLabel["LBLOPEN"];
                } else if (data.STATUS == "LBLCOMPLETED") {
                  data.STATUS = resLabel["LBLCOMPLETED"];
                } else if (data.STATUS == "LBLALTERNATEACTION") {
                  data.STATUS = resLabel["LBLALTERNATEACTION"];
                }

                if (data.RESPONSESTATUS == "LBLOPENWITHINRD") {
                  fn.LBLOPENWITHINRD.push(data);
                } else if (data.RESPONSESTATUS == "LBLOPENBEYONDRD") {
                  fn.LBLOPENBEYONDRD.push(data);
                } else if (data.RESPONSESTATUS == "LBLCLOSEDWITHINRD") {
                  fn.LBLCLOSEDWITHINRD.push(data);
                } else if (data.RESPONSESTATUS == "LBLCLOSEDBEYONDRD") {
                  fn.LBLCLOSEDBEYONDRD.push(data);
                }
                if (fn.tableData.length == i) {
                  setTimeout(function () {
                    fn.isLoaded = true;
                  }, 1000);
                }
                i++;
              });

              this.cols = [
                { field: "CORRACTIONID", header: resLabel["LBLCAID"] },
                { field: "OBSERVATIONDATE", header: resLabel["LBLOBSDATE"] },
                {
                  field: "RESPONSIBLEPERSON",
                  header: resLabel["LBLRESPONSIBLEPERSON"],
                },
                { field: "ACTIONOWNER", header: resLabel["LBLACTOWNER"] },
                {
                  field: "ACTIONDUEDATE",
                  header: resLabel["LBLRESPONSEREQUIREDDATE"],
                },
                {
                  field: "RESPONDEDDATE",
                  header: resLabel["LBLRESPONDEDDATE"],
                },
                { field: "PRIORITY", header: resLabel["LBLPRIORITY"] },
                { field: "STATUS", header: resLabel["LBLSTATUS"] },
                {
                  field: "ACTIONPLANNED",
                  header: resLabel["LBLACTIONPLANNED"],
                },
                {
                  field: "ACTIONPERFORMED",
                  header: resLabel["LBLACTIONPERFORMED"],
                },
                {
                  field: "ENTEREDBY",
                  header: resLabel["LBLENTEREDBY"],
                },
              ];
              this.updateRowGroupMetaData();
            } else {
              this.subReportId = 6;
              this.graphicalData = res["Graphically"];
              var graphValue = [];
              this.graphicalData.forEach(function (graph) {
                if (graph.RDSTATUS == "LBLOPENWITHINRD") {
                  graph.RDSTATUS = resLabel["LBLOPENWITHINRD"];
                } else if (graph.RDSTATUS == "LBLOPENBEYONDRD") {
                  graph.RDSTATUS = resLabel["LBLOPENBEYONDRD"];
                } else if (graph.RDSTATUS == "LBLCLOSEDWITHINRD") {
                  graph.RDSTATUS = resLabel["LBLCLOSEDWITHINRD"];
                } else if (graph.RDSTATUS == "LBLCLOSEDBEYONDRD") {
                  graph.RDSTATUS = resLabel["LBLCLOSEDBEYONDRD"];
                }
                graphValue.push({
                  labelFontColor: "#" + customColor,
                  label: graph.RDSTATUS,
                  value: graph.RESPONSECOUNT,
                });
              });
              this.graphCORRACTIONRPT = {
                chart: {
                  showPercentInTooltip: "1",
                  labelFontColor: "#" + customColor,
                  baseFontColor: "#" + customColor,
                  legendItemFontColor: "#" + customColor,
                  showValues: "1",
                  showPercentValues: "0",
                  decimals: "1",
                  theme: "fusion",
                },
                data: graphValue,
              };
              this.isLoaded = true;
              this.graphicalCols = [
                {
                  field: "RDSTATUS",
                  header: resLabel["LBLCORRACTIVEACTIONSTATUS"],
                },
                {
                  field: "RESPONSECOUNT",
                  header: resLabel["LBLCORRECTIVEACTIONCOUNT"],
                },
              ];
            }
          });

        this.manageExportData();
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  barClick(data) {
    this.tableData.map((item) => {
      if (item["RDSTATUS"] == data["dataObj"]["categoryLabel"]) {
        this.openSubReport(item["RESPONSESTATUS"], item["RDSTATUS"]);
      }
    });
  }

  changeLBLCORRACTIONRPT() {
    this.isLoaded = false;
    this.parameters["designId"] = this.selectedReportView;
    if (this.selectedSiteName == 0) {
      this.parameters["SITEID"] = this.allSiteSelected;
    } else {
      this.parameters["SITEID"] = this.selectedSiteName;
    }
    this.getReports();
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    this.mainHead = [];
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let tableData = this.tableData[i];
        let responsestatus = tableData.RESPONSESTATUS;
        if (i == 0) {
          this.rowGroupMetadata[responsestatus] = { index: 0, size: 1 };
          this.mainHead.push({
            RESPONSESTATUS: responsestatus,
          });
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.RESPONSESTATUS;
          if (responsestatus === previousRowGroup) {
            this.rowGroupMetadata[responsestatus].size++;
          } else {
            this.rowGroupMetadata[responsestatus] = { index: i, size: 1 };
            this.mainHead.push({
              RESPONSESTATUS: responsestatus,
            });
          }
        }
      }
    }
  }

  openSubReport(corractionId, headName) {
    this.selectedCorrectionId = corractionId;
    this.headName = headName;
    this.displaySubReport = true;
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfRows = [];
    this.pdfData = [];
    this.columns = [];
    if (this.selectedReportView == 23) {
      this.cols = this.graphicalCols;
      this.tableData = this.graphicalData;
    }
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.grandTotal = 0;
      this.tableData.map((data, key) => {
        this.grandTotal = this.grandTotal + data["RESPONSECOUNT"];
        var dataJson = {};
        var tempDataJson = {};
        var tempRows = [];
        if (this.selectedReportView == 44) {
          this.translate
            .get(["LBLCORRACTIVEACTIONSTATUS", data["RESPONSESTATUS"]])
            .subscribe((label) => {
              if (key == 0) {
                this.unFColumns.push(label["LBLCORRACTIVEACTIONSTATUS"]);
              }
              tempDataJson[label["LBLCORRACTIVEACTIONSTATUS"]] =
                label[data["RESPONSESTATUS"]];
            });
        }
        this.cols.map((item) => {
          if (item["field"] == "RESPONSIBLEPERSON") {
            var regexHash = new RegExp("<BR>", "g");
            if (
              data[item["field"]] &&
              data[item["field"]].search("<BR>") != -1
            ) {
              data[item["field"]] = data[item["field"]].replace(
                regexHash,
                "\n"
              );
            }
          }
          dataJson[item["header"]] = data[item["field"]];

          tempDataJson[item["header"]] = data[item["field"]];
          // dataJson[this.cols[0].header] = data[this.cols[0].field];
          // dataJson[this.cols[1].header] = data[this.cols[1].field];
          tempRows.push(data[item["field"]]);
          if (this.tableData.length - 1 == key) {
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
          }
        });
        if (this.selectedReportView == 44) {
          dataJson["RESPONSESTATUS"] = data["RESPONSESTATUS"];
        }
        this.pdfRows.push(tempRows);
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
      });
      this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
        var totalJson = [label, this.grandTotal];
        this.pdfRows.push(totalJson);
      });
      if (this.selectedReportView == 44) {
        var mainColumns = [];
        this.translate
          .get(["LBLCORRACTIVEACTIONSTATUS"])
          .subscribe((resLabel) => {
            mainColumns.push(resLabel["LBLCORRACTIVEACTIONSTATUS"]);
          });
        var tempMainColums = Object.keys(this.mainHead[0]);
        var subColumns = this.columns;
        this.mainHead.map((head) => {
          var mainRows = [];
          var subRows = [];
          // main Table
          var tempMainRows = [];
          tempMainColums.map((col) => {
            this.translate.get(head[col]).subscribe((labelVal) => {
              tempMainRows.push(labelVal);
            });
          });
          mainRows.push(tempMainRows);

          // sub Table

          this.tableData.map((data) => {
            var tempSubRows = [];
            if (data["RESPONSESTATUS"] == head["RESPONSESTATUS"]) {
              this.cols.map((subCol, index) => {
                tempSubRows.push(data[subCol.field]);
              });
              subRows.push(tempSubRows);
            }
          });
          this.pdfData.push({
            mainColumns: mainColumns,
            mainRows: mainRows,
            subColumns: subColumns,
            subRows: subRows,
          });
        });
      }
    }
    // this.columns[0] = this.cols[0].header;
    // this.columns[1] = this.cols[1].header;
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    // json.map((item) => {
    //   if (this.selectedReportView == 44) {
    //     this.translate.get(item["RESPONSESTATUS"]).subscribe((val) => {
    //       item["RESPONSESTATUS"] = val;
    //     });
    //   }
    // });
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    var fileName = "";
    if (this.selectedReportView == 44) {
      var fileName = "CORRECTIVEACTIONDETAIL";
    } else {
      var fileName = "CORRECTIVEACTIONGRAPHICAL";
    }
    var encodedData;
    if (this.selectedReportView == 23) {
      this.chart.getSVGString((svg) => {
        this.reportsService.svgString2Image(
          svg,
          1030,
          524,
          "png",
          function (base64String) {
            encodedData = base64String;
          }
        );
      });
    }

    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );

    setTimeout(() => {
      if (this.selectedExport == 6) {
        var obj = Object.keys(this.unFTableRows[0]);
        // if (this.tableCols.length == 1) {
        var dataJson = {};
        this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
          obj.map((item, key) => {
            if (key == 0) {
              dataJson[item] = label;
            } else if (key == obj.length - 1) {
              dataJson[item] = this.grandTotal;
            } else {
              dataJson[item] = "";
            }
          });
        });
        this.unFTableRows.push(dataJson);
        this.exportAsExcelFile(this.unFTableRows, fileName);
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
      } else if (
        this.selectedExport == 1 ||
        this.selectedExport == 3 ||
        this.selectedExport == 4 ||
        this.selectedExport == 5
      ) {
        var EXCEL_EXTENSION;
        if (this.selectedExport == 1 || this.selectedExport == 5) {
          EXCEL_EXTENSION = ".xlsx";
        } else if (this.selectedExport == 3 || this.selectedExport == 4) {
          EXCEL_EXTENSION = ".xls";
        }
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        //Excel Title, Header, Data
        const subHeader = this.columns;
        const data = this.tableRows;
        const unFColumns = this.unFColumns;
        const unFTableRows = this.unFTableRows;

        const EXCEL_TYPE =
          "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

        //Create workbook and worksheet
        let workbook = new ExcelJS.Workbook();
        // Report scope sheet
        let firstWorksheet = workbook.addWorksheet("Report Scope");
        firstWorksheet.addRow([]);
        let firstHeaderRow = firstWorksheet.addRow([this.title]);

        firstWorksheet.getCell("A2").font = {
          name: "Arial Unicode MS",
          family: 4,
          size: 18,
          bold: true,
          color: { argb: "00000000" },
        };
        firstWorksheet.getCell("A2").alignment = {
          vertical: "middle",
          horizontal: "left",
          indent: 1,
        };
        firstWorksheet.getColumn("A").width = 100;
        firstWorksheet.getRow(2).height = 30;

        this.translate
          .get(["LBLOBSDATE", "LBLRESPONSEREQUIREDDATE", "LBLRESPONDEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              firstWorksheet.addRow([]);
              firstWorksheet.addRow([]);
              firstWorksheet
                .addRow([resLabel["LBLOBSDATE"]])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 14,
                    bold: true,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 2,
                  };
                });

              firstWorksheet.lastRow.height = 25;
              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              firstWorksheet
                .addRow([
                  fDate +
                    " - " +
                    tDate +
                    " (" +
                    this.auth.UserInfo["dateFormat"].toUpperCase() +
                    ")",
                ])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 12,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 3,
                  };
                });
            }

            if (this.parameters["RESDATEREQ"]) {
              firstWorksheet.addRow([]);
              firstWorksheet.addRow([]);
              firstWorksheet
                .addRow([resLabel["LBLRESPONSEREQUIREDDATE"]])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    size: 14,
                    bold: true,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 2,
                  };
                });

              firstWorksheet.lastRow.height = 25;
              var bothDate = this.parameters["RESDATEREQ"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              firstWorksheet
                .addRow([
                  fDate +
                    " - " +
                    tDate +
                    " (" +
                    this.auth.UserInfo["dateFormat"].toUpperCase() +
                    ")",
                ])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 12,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 3,
                  };
                });
            }

            if (this.parameters["RESPONDEDDATE"]) {
              firstWorksheet.addRow([]);
              firstWorksheet.addRow([]);
              firstWorksheet
                .addRow([resLabel["LBLRESPONDEDDATE"]])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    size: 14,
                    bold: true,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 2,
                  };
                });

              firstWorksheet.lastRow.height = 25;
              var bothDate = this.parameters["RESPONDEDDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              firstWorksheet
                .addRow([
                  fDate +
                    " - " +
                    tDate +
                    " (" +
                    this.auth.UserInfo["dateFormat"].toUpperCase() +
                    ")",
                ])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 12,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 3,
                  };
                });
            }

            firstWorksheet.lastRow.height = 25;
          });

        if (this.selectedExport == 4 || this.selectedExport == 5) {
          // Report View sheet
          let worksheet = workbook.addWorksheet("Sheet1");
          if (this.selectedReportView == 23) {
            worksheet.getColumn(1).width = 40;
            worksheet.getColumn(2).width = 30;
            // worksheet.getRow(1).height = 250;
            // worksheet.mergeCells("A1:B1");
            // var imageId = workbook.addImage({
            //   base64: encodedData,
            //   extension: "png",
            // });
            // worksheet.addImage(imageId, {
            //   tl: { col: 0, row: 0 },
            //   ext: { width: 800, height: 300 },
            // });
            //Add Header Row
            let headerRow = worksheet.addRow(subHeader);
            headerRow.height = 20;
            // Cell Style : Fill and Border
            headerRow.eachCell((cell, number) => {
              cell.fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              cell.font = {
                color: { argb: customFontColor },
              };
              cell.border = {
                top: { style: "thin" },
                left: { style: "thin" },
                bottom: { style: "thin" },
                right: { style: "thin" },
              };
              if (number == 2) {
                cell.alignment = { vertical: "middle", horizontal: "center" };
              } else {
                cell.alignment = { vertical: "middle", horizontal: "left" };
              }
            });
            // Add Data and Conditional Formatting
            let sumOfData = 0;
            data.forEach((element) => {
              let eachRow = [];
              this.columns.map((headers, key) => {
                if (key == 1) {
                  sumOfData = sumOfData + element[headers];
                }
                eachRow.push(element[headers]);
              });
              if (element.isDeleted === "Y") {
                let deletedRow = worksheet.addRow(eachRow);
                deletedRow.eachCell((cell, number) => {
                  cell.font = {
                    name: "Calibri",
                    family: 4,
                    size: 11,
                    bold: false,
                    strike: true,
                  };
                });
              } else {
                var addRow = worksheet.addRow(eachRow);
                addRow.getCell(2).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
                addRow.eachCell((cell) => {
                  cell.numFmt = this.auth.changeCellValueType(cell.value);
                });
              }
            });
            this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
              let finalRow = worksheet.addRow([label, sumOfData]);
              finalRow.getCell(2).alignment = {
                vertical: "middle",
                horizontal: "center",
              };
              finalRow.eachCell((cell) => {
                cell.font = {
                  size: 12,
                  bold: true,
                };
                cell.border = {
                  top: { style: "thin", color: { argb: customColor } },
                  // left: { style: "thin", color: {argb: customColor} },
                  bottom: { style: "thin", color: { argb: customColor } },
                  // right: { style: "thin", color: {argb: customColor} },
                };
                cell.numFmt = this.auth.changeCellValueType(cell.value);
              });
            });
            // worksheet.getColumn(1).width = 40;
            // worksheet.getColumn(2).width = 30;
            worksheet.addRow([]);
            //Generate Excel File with given name
            workbook.xlsx.writeBuffer().then((data) => {
              let blob = new Blob([data], { type: EXCEL_TYPE });
              fs.saveAs(
                blob,
                fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
              );
            });
          } else if (this.selectedReportView == 44) {
            var mainHeader = Object.keys(this.rowGroupMetadata);
            var beforeDataCount = 1;
            // mainHeader.map((header, key) => {
            // var rowsData = data.filter((item) => {
            //   return item[this.dataKey] == header;
            // });
            for (var i = 1; i <= 10; i++) {
              worksheet.getColumn(i).width = 30;
            }

            let headerRow = worksheet.addRow(unFColumns);
            // Cell Style : Fill and Border
            headerRow.eachCell((cell, number) => {
              cell.fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              cell.font = {
                color: { argb: customFontColor },
              };
              cell.border = {
                top: { style: "thin" },
                left: { style: "thin" },
                bottom: { style: "thin" },
                right: { style: "thin" },
              };
              if (number == 2) {
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "center",
                  indent: 2,
                };
              }
            });

            // Add Data and Conditional Formatting
            let sumOfData = 0;
            unFTableRows.map((element, index) => {
              let eachRow = [];
              this.unFColumns.map((headers, key) => {
                if (key == 1) {
                  sumOfData = sumOfData + element[headers];
                }
                eachRow.push(element[headers]);
              });
              if (element.isDeleted === "Y") {
                let deletedRow = worksheet.addRow(eachRow);
                deletedRow.eachCell((cell, number) => {
                  cell.font = {
                    name: "Calibri",
                    family: 4,
                    size: 11,
                    bold: false,
                    strike: true,
                  };
                });
              } else {
                if (unFTableRows.length - 1 == index) {
                  let addedRow = worksheet.addRow(eachRow);
                  addedRow.eachCell({ includeEmpty: true }, (cell, number) => {
                    cell.border = {
                      bottom: {
                        style: "medium",
                        color: { argb: customColor },
                      },
                    };
                    if (number == 2) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                        indent: 2,
                      };
                    }
                    cell.numFmt = this.auth.changeCellValueType(cell.value);
                  });
                } else {
                  worksheet.addRow(eachRow).eachCell((cell, number) => {
                    if (number == 2) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                        indent: 2,
                      };
                    }
                    cell.numFmt = this.auth.changeCellValueType(cell.value);
                  });
                }
              }
            });
            // });

            worksheet.addRow([]);
            //Generate Excel File with given name
            workbook.xlsx.writeBuffer().then((data) => {
              let blob = new Blob([data], { type: EXCEL_TYPE });
              fs.saveAs(
                blob,
                fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
              );
            });
          }
        } else {
          // Report View sheet
          let worksheet = workbook.addWorksheet("Sheet1");
          if (this.selectedReportView == 23) {
            worksheet.getColumn(1).width = 60;
            worksheet.getColumn(2).width = 60;
            worksheet.getRow(1).height = 250;
            worksheet.mergeCells("A1:B1");
            var imageId = workbook.addImage({
              base64: encodedData,
              extension: "png",
            });
            worksheet.addImage(imageId, {
              tl: { col: 0, row: 0 },
              ext: { width: 800, height: 300 },
            });
            //Add Header Row
            let headerRow = worksheet.addRow(subHeader);
            headerRow.height = 20;
            // Cell Style : Fill and Border
            headerRow.eachCell((cell, number) => {
              cell.fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              cell.font = {
                color: { argb: customFontColor },
              };
              cell.border = {
                top: { style: "thin" },
                left: { style: "thin" },
                bottom: { style: "thin" },
                right: { style: "thin" },
              };
              if (number == 2) {
                cell.alignment = { vertical: "middle", horizontal: "center" };
              } else {
                cell.alignment = { vertical: "middle", horizontal: "left" };
              }
            });
            // Add Data and Conditional Formatting
            let sumOfData = 0;
            data.forEach((element) => {
              let eachRow = [];
              this.columns.map((headers, key) => {
                if (key == 1) {
                  sumOfData = sumOfData + element[headers];
                }
                eachRow.push(element[headers]);
              });
              if (element.isDeleted === "Y") {
                let deletedRow = worksheet.addRow(eachRow);
                deletedRow.eachCell((cell, number) => {
                  cell.font = {
                    name: "Calibri",
                    family: 4,
                    size: 11,
                    bold: false,
                    strike: true,
                  };
                });
              } else {
                var addRow = worksheet.addRow(eachRow);
                addRow.getCell(2).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
                addRow.getCell(2).numFmt = "0";
              }
            });
            this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
              let finalRow = worksheet.addRow([label, sumOfData]);
              finalRow.getCell(2).alignment = {
                vertical: "middle",
                horizontal: "center",
              };
              finalRow.getCell(2).numFmt = "0";
              finalRow.eachCell((cell) => {
                cell.font = {
                  size: 12,
                  bold: true,
                };
                cell.border = {
                  top: { style: "thin", color: { argb: customColor } },
                  // left: { style: "thin", color: {argb: customColor} },
                  bottom: { style: "thin", color: { argb: customColor } },
                  // right: { style: "thin", color: {argb: customColor} },
                };
              });
            });
            // worksheet.getColumn(1).width = 40;
            // worksheet.getColumn(2).width = 30;
            worksheet.addRow([]);
            //Generate Excel File with given name
            workbook.xlsx.writeBuffer().then((data) => {
              let blob = new Blob([data], { type: EXCEL_TYPE });
              fs.saveAs(
                blob,
                fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
              );
            });
          } else if (this.selectedReportView == 44) {
            var mainHeader = Object.keys(this.rowGroupMetadata);
            var beforeDataCount = 1;
            mainHeader.map((header, key) => {
              var rowsData = data.filter((item) => {
                return item[this.dataKey] == header;
              });
              for (var i = 1; i <= 10; i++) {
                worksheet.getColumn(i).width = 30;
              }
              if (key == 0) {
                worksheet.mergeCells(
                  "A" + beforeDataCount + ":K" + beforeDataCount
                );
                worksheet.mergeCells(
                  "A" + (beforeDataCount + 1) + ":K" + (beforeDataCount + 1)
                );
                this.translate
                  .get("LBLCORRACTIVEACTIONSTATUS")
                  .subscribe((label) => {
                    worksheet.getCell("A" + beforeDataCount).value = label;
                  });
                this.translate.get(header).subscribe((label) => {
                  worksheet.getCell("A" + (beforeDataCount + 1)).value = label;
                });
                worksheet.getCell("A" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("A" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                beforeDataCount = rowsData.length + 5;
              } else {
                worksheet.mergeCells(
                  "A" + beforeDataCount + ":K" + beforeDataCount
                );
                worksheet.mergeCells(
                  "A" + (beforeDataCount + 1) + ":K" + (beforeDataCount + 1)
                );
                this.translate
                  .get("LBLCORRACTIVEACTIONSTATUS")
                  .subscribe((label) => {
                    worksheet.getCell("A" + beforeDataCount).value = label;
                  });
                this.translate.get(header).subscribe((label) => {
                  worksheet.getCell("A" + (beforeDataCount + 1)).value = label;
                });
                worksheet.getCell("A" + beforeDataCount).fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: customColor },
                };
                worksheet.getCell("A" + beforeDataCount).font = {
                  color: { argb: customFontColor },
                };
                beforeDataCount = rowsData.length + 4 + beforeDataCount;
              }

              let headerRow = worksheet.addRow(subHeader);
              // Cell Style : Fill and Border
              headerRow.eachCell((cell, number) => {
                cell.fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: "EEECE1" },
                };
                cell.font = {
                  color: { argb: "000000" },
                };
                cell.border = {
                  top: { style: "thin" },
                  left: { style: "thin" },
                  bottom: { style: "thin" },
                  right: { style: "thin" },
                };
                if (number == 1) {
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "center",
                    indent: 2,
                  };
                }
              });

              // Add Data and Conditional Formatting
              let sumOfData = 0;
              rowsData.map((element, index) => {
                let eachRow = [];
                this.columns.map((headers, key) => {
                  if (key == 1) {
                    sumOfData = sumOfData + element[headers];
                  }
                  eachRow.push(element[headers]);
                });
                if (element.isDeleted === "Y") {
                  let deletedRow = worksheet.addRow(eachRow);
                  deletedRow.eachCell((cell, number) => {
                    cell.font = {
                      name: "Calibri",
                      family: 4,
                      size: 11,
                      bold: false,
                      strike: true,
                    };
                  });
                } else {
                  if (rowsData.length - 1 == index) {
                    let addedRow = worksheet.addRow(eachRow);
                    addedRow.eachCell(
                      { includeEmpty: true },
                      (cell, number) => {
                        cell.border = {
                          bottom: {
                            style: "medium",
                            color: { argb: customColor },
                          },
                        };
                        if (number == 1) {
                          cell.alignment = {
                            vertical: "middle",
                            horizontal: "center",
                            indent: 2,
                          };
                        } else {
                          if (cell.value && cell.value.search("/") != -1) {
                            cell.numFmt = "mm/dd/yyyy";
                          }
                        }
                      }
                    );
                  } else {
                    worksheet.addRow(eachRow).eachCell((cell, number) => {
                      if (number == 1) {
                        cell.alignment = {
                          vertical: "middle",
                          horizontal: "center",
                          indent: 2,
                        };
                      } else {
                        if (cell.value && cell.value.search("/") != -1) {
                          cell.numFmt = "mm/dd/yyyy";
                        }
                      }
                    });
                  }
                }
              });
            });

            worksheet.addRow([]);
            //Generate Excel File with given name
            workbook.xlsx.writeBuffer().then((data) => {
              let blob = new Blob([data], { type: EXCEL_TYPE });
              fs.saveAs(
                blob,
                fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
              );
            });
          }
        }
      } else if (this.selectedExport == 2) {
        setTimeout(() => {
          this.selectedExport = 0;
        }, 100);
        var pdf = new jsPDF("landscape", "mm", [1200, 800]);
        // report scope
        pdf.text(this.title, 15, 20);
        let finalX = 10;
        this.translate
          .get(["LBLOBSDATE", "LBLRESPONSEREQUIREDDATE", "LBLRESPONDEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              let obsDate = [];
              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
                startY: finalX + 13,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }
            if (this.parameters["RESDATEREQ"]) {
              let obsDate = [];
              var bothDate = this.parameters["RESDATEREQ"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLRESPONSEREQUIREDDATE"]], obsDate, {
                startY: finalX,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }

            if (this.parameters["RESPONDEDDATE"]) {
              let obsDate = [];
              var bothDate = this.parameters["RESPONDEDDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLRESPONDEDDATE"]], obsDate, {
                startY: finalX,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }
            pdf.addPage();
          });

        // report view
        var finalY = 10;
        if (encodedData) {
          pdf.addImage(encodedData, "PNG", 20, 0, 220, 120);
          finalY = 130;
        }
        if (this.selectedReportView == 44) {
          let finalY = 10;
          var fn = this;
          this.pdfData.map((data) => {
            pdf.autoTable(data.mainColumns, data.mainRows, {
              startY: finalY,
              headStyles: {
                fillColor: customColor,
                textColor: customFontColor,
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
            });
            finalY = pdf.previousAutoTable.finalY;
            pdf.autoTable(data.subColumns, data.subRows, {
              startY: finalY,
              headStyles: {
                fillColor: "#D8D8D8",
                textColor: "#000000",
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
              didParseCell: (colData) => {
                fn.alignCol(colData, customColor, data.subRows.length);
              },
            });
            finalY = pdf.previousAutoTable.finalY + 10;
          });
        } else {
          var fn = this;
          pdf.autoTable(this.columns, this.pdfRows, {
            startY: finalY,
            headStyles: {
              fillColor: customColor,
              textColor: customFontColor,
            },
            alternateRowStyles: {
              fillColor: "#FFFFFF",
            },
            willDrawCell: this.drawCell,
            didParseCell: (data) => {
              fn.alignCol(data, customColor, fn.pdfRows.length);
            },
          });
        }

        pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
      }
    }, 1000);
  }

  initialized($event) {
    this.chart = $event.chart; // saving chart instance
  }

  alignCol = function (data, customColor, rowCount) {
    if (this.selectedReportView == 44) {
      if (data.column.index == 0) {
        data.cell.styles.halign = "center";
      }
    } else {
      if (data.column.index == 1) {
        data.cell.styles.halign = "center";
      }
      var s = data.cell.styles;
      if (data.row.index == rowCount - 1) {
        s.lineColor = "#" + customColor;
        s.lineWidth = 0.5;
        s.borders = "t";
      }
    }
  };

  drawCell = function (data) {
    var doc = data.doc;
    var rows = data.table.body;
    var col = data.column.index;
    if (col == 2) {
      data.cell.styles.halign = "center";
    }
    if (rows.length === 1) {
    } else if (data.row.index === rows.length - 1) {
      doc.setFontStyle("bold");
      doc.setFontSize("12");
    }
  };
}
