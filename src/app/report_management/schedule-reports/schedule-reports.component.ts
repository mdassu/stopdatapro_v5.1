import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder
} from "@angular/forms";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";

import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
import { ScheduleReportsService } from "src/app/_services/schedule-reports.service";
import { SlideUpDownAnimations } from "../../_animations/slide-up-down.animations";

import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
@Component({
  selector: "app-schedule-reports",
  templateUrl: "./schedule-reports.component.html",
  styleUrls: ["./schedule-reports.component.css"]
})
export class ScheduleReportsComponent implements OnInit {
  allScheduleReports: any;
  scheduleReportsListing: any;
  scheduleReportsCols: any[];
  scheduleReportsCount: number;
  selectedScheduleReports: any = [];
  serchReportListing: any = [];
  selectedReports: any = 0;
  searchBy: any = [];
  selectedfilterName: string = "SCHEDULENAME";
  searchText: any;
  isLoaded: boolean;
  confirmClass: any;
  appName: any;
  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private route: Router,
    private scheduleReports: ScheduleReportsService,
    private cookieService: CookieService,
    private translate: TranslateService,
    private env: EnvService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLREPORTMGMT",
        url: "./assets/help/scheduled-reports-listing.md"
      },
      {
        label: "LBLSCHEDULEREPORTS",
        routerLink: ["/schedule-reports"]
      }
    ]);
  }

  ngOnInit() {
    // Table Header
    this.scheduleReportsCols = [
      { field: "SCHEDULENAME", header: "LBLSCHEDULENAME" },
      { field: "REPORTNAME", header: "LBLREPORTNAME" },
      { field: "STATUS", header: "LBLSTATUS" }
    ];

    // Schedule Reports count
    this.scheduleReportsCount = this.scheduleReportsCols.length;

    this.getScheduleReports();

    this.scheduleReportsCols.map((item, key) => {
      this.translate.get(item.header).subscribe((text: string) => {
        if (key < 1) {
          this.searchBy.push({
            field: item.field,
            header: text
          });
        }
      });
      this.selectedfilterName = "SCHEDULENAME";
    });
    this.appName = this.env.appName;
  }

  getScheduleReports() {
    this.scheduleReports.getScheduleReports({}).subscribe(res => {
      if (res["status"] == true) {
        this.allScheduleReports = res["scheduleData"];
        this.scheduleReportsListing = res["scheduleData"];
        this.scheduleReportsListing.map(item => {
          this.translate.get(item.REPORTNAME).subscribe((reports: string) => {
            item["REPORTNAME"] = reports;
          });
        });
        this.serchReportListing = res["reportData"];
        this.serchReportListing.map(item => {
          this.translate
            .get(item.REPORTNAME)
            .subscribe((reportName: string) => {
              item["REPORTNAME"] = reportName;
            });
        });
      } else {
        this.allScheduleReports = [];
        this.scheduleReportsListing = [];
      }
      this.isLoaded = true;
    });
  }

  serchByReport() {
    this.scheduleReports
      .getScheduleReports({ reportId: this.selectedReports })
      .subscribe(res => {
        this.selectedReports = this.selectedReports;
        this.allScheduleReports = res["scheduleData"];
        this.searchText = "";
        this.scheduleReportsListing = res["scheduleData"];
        this.scheduleReportsListing.map(item => {
          this.translate.get(item.REPORTNAME).subscribe((reports: string) => {
            item["REPORTNAME"] = reports;
          });
        });
      });
  }

  // searching data
  onTextSearch() {
    var field = this.selectedfilterName;
    var stext = this.searchText.toLowerCase();
    var fn = this;
    if (stext != "") {
      var newList = this.allScheduleReports.filter(item => {
        if (item[field] != null) {
          return item[field].toLowerCase().indexOf(stext) >= 0;
        } else {
          return;
        }
      });
      this.scheduleReportsListing = newList;
    } else {
      this.scheduleReportsListing = this.allScheduleReports;
    }
  }

  deleteRecord() {
    this.confirmClass = "warning-msg";
    var msg = "";
    this.translate.get("ALTDELCONFIRMSCHEDULE").subscribe(confirmMsg => {
      msg = confirmMsg;
    });
    if (this.selectedScheduleReports == "") {
    } else {
      this.confirmationService.confirm({
        message: msg,
        accept: () => {
          this.scheduleReports
            .deleteScheduleReports({
              scheduleId: Array.prototype.map
                .call(this.selectedScheduleReports, s => s.SCHEDULEID)
                .toString()
            })
            .subscribe(res => {
              this.getScheduleReports();
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res
                });
              });
            });
          this.selectedScheduleReports = [];
          this.searchText = "";
          this.getScheduleReports();
        },
        reject: () => {
          this.selectedScheduleReports = [];
          this.searchText = "";
          this.getScheduleReports();
        }
      });
    }
  }

  changeStatus(id) {
    this.confirmClass = "info-msg";
    var msg = "";
    this.translate.get("ALTCHANGESTATUSCONFIRM").subscribe(confirmMsg => {
      msg = confirmMsg;
    });
    if (this.selectedScheduleReports == "" && id == "") {
    } else {
      this.confirmationService.confirm({
        message: msg,
        accept: () => {
          this.scheduleReports
            .updateScheduleReportsStatus({
              scheduleId: id
                ? id
                : Array.prototype.map
                    .call(this.selectedScheduleReports, s => s.SCHEDULEID)
                    .toString()
            })
            .subscribe(res => {
              this.selectedScheduleReports = [];
              this.getScheduleReports();
              this.searchText = "";
              this.translate.get(res["message"]).subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res
                });
              });
            });
          this.searchText = "";
          this.getScheduleReports();
          this.selectedScheduleReports = [];
        },
        reject: () => {
          this.selectedScheduleReports = [];
          this.searchText = "";
          this.getScheduleReports();
        }
      });
    }
  }
}
