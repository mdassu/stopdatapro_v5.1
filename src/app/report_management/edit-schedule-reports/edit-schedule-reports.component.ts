import { Component, OnInit, ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { Message } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";

import { Router, ActivatedRoute } from "@angular/router";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { CustomValidatorsService } from "../../_services/custom-validators.service";

import { ScheduleReportsService } from "src/app/_services/schedule-reports.service";
import { ReportsService } from "src/app/_services/reports.service";
import { TranslateService } from "@ngx-translate/core";

import tinymce from "tinymce/tinymce";

// A theme is also required
import "tinymce/themes/modern/theme";

// Any plugins you want to use has to be imported
import "tinymce/plugins/paste";
import "tinymce/plugins/link";
import "tinymce/plugins/table";
import "tinymce/plugins/preview";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { formatDate } from "@angular/common";

@Component({
  selector: "app-edit-schedule-reports",
  templateUrl: "./edit-schedule-reports.component.html",
  styleUrls: ["./edit-schedule-reports.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class EditScheduleReportsComponent implements OnInit {
  // Varialbe for fields name
  addScheduleGeneral: FormGroup;
  submitted: boolean;
  iconType = "ui-icon-add";
  scheduleId: number;
  exportFileType: any = [];
  exportFileTypeList: any = [];
  relativeDate: any = [];
  relativeDateList: any = [];
  chooseDate: any = [];
  chooseDateList: any = [];
  relativeOption: any = [];
  relativeOptions: any = [];
  relativeOptionList: any = [];
  relativeOptionsList: any = [];
  sendEmailOption: any = [];
  sendEmailOptionList: any = [];
  exportFormat: any = [];
  exportFormatList: any = [];
  reportName: any = [];
  reportNameList: any = [];
  reportView: boolean = false;
  reportSaved: boolean = false;
  reportDate: boolean = false;
  reportViewDataList: any = [];
  savedDataList: any = [];
  filterList: any;
  filterGroupList: any;
  toUser: any;
  toUserList: any;
  showAdvanceSearch: boolean = false;
  showSearch: boolean = false;
  filterOptional: any;
  filterChart: any;
  showSearchBox: boolean = false;
  showOpSearchBox: boolean = false;
  reportSavedId: any;
  filterSetting: any;
  reportValue: number = 0;
  emailValue: number = 2;
  setEmailValue: number;

  theDay: any = [];
  theDayList: any = [];

  weekDay: any = [];
  weekDayList: any = [];
  everyDwm: any = [];
  timeZoneList: any = [];

  Status: any = [];
  StatusList: any = [];
  startTimeList: any = [];
  timeList: any = [];
  scheduleDay: any;
  monthlyDwm: any;
  dayDwm: any;
  initialGeneralFormData: any;
  isLoaded: boolean;
  keywords: any;
  menuList = [];
  Keyword: any = "Keywords";
  onSubmitTouched: boolean = false;
  dateFormat: any;
  dateFormatType: any;
  dateFormatTypeNew: any;
  filterGroups: any;
  filterCategory: any;
  locale: any;
  previousValError: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private router: Router,
    private cookieService: CookieService,
    private elementRef: ElementRef,
    private activetedRoute: ActivatedRoute,
    private scheduleReports: ScheduleReportsService,
    private reportService: ReportsService,
    private customValidatorsService: CustomValidatorsService,
    private translate: TranslateService,
    private auth: AuthenticationService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT", url: "./assets/help/scheduled-reports-add.md" },
      {
        label: "LBLSCHEDULEREPORTS",
        routerLink: ["/schedule-reports"],
      },
    ]);

    if (this.cookieService.get("add-new-schedule")) {
      var fn = this;
      setTimeout(function () {
        fn.messageService.add({
          severity: "success",
          detail: fn.cookieService.get("add-new-schedule"),
        });
        fn.cookieService.delete("add-new-schedule");
      }, 1000);
    }
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    this.dateFormatType = this.auth.UserInfo["dateFormat"].toLowerCase();
    if (this.auth.UserInfo["dateFormat"] == "MM/dd/yyyy") {
      this.dateFormatTypeNew = "LBLMDY";
    } else if (this.auth.UserInfo["dateFormat"] == "dd/MM/yyyy") {
      this.dateFormatTypeNew = "LBLDMY";
    } else if (this.auth.UserInfo["dateFormat"] == "yyyy/MM/dd") {
      this.dateFormatTypeNew = "LBLYMD";
    }
    this.dateFormat = this.dateFormatType.replace("yyyy", "yy");
    this.isLoaded = true;
    this.scheduleId = parseInt(
      this.activetedRoute.snapshot.paramMap.get("scheduleId")
    );

    //Export File Type
    this.exportFileType.push({ label: "LBLPDF", value: 1 });
    this.exportFileType.push({ label: "LBLXLS", value: 2 });
    this.exportFileType.push({ label: "LBLXLSX", value: 3 });
    this.exportFileType.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.exportFileTypeList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Export Format
    this.exportFormat.push({ label: "LBLFORMATTED", value: 0 });
    this.exportFormat.push({ label: "LBLUNFORMATTED", value: 1 });
    this.exportFormat.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.exportFormatList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Coose Date
    this.chooseDate.push({ label: "LBLSELECT", value: 0 });
    this.chooseDate.push({ label: "LBLCURRENT", value: 1 });
    this.chooseDate.push({ label: "LBLPREVIOUS", value: 2 });
    this.chooseDate.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.chooseDateList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Relative Option
    this.relativeOptions.push({ label: "LBLSELECT", value: 0 });
    this.relativeOptions.push({ label: "LBLWEEKS", value: 1 });
    this.relativeOptions.push({ label: "LBLMONTHS", value: 2 });
    this.relativeOptions.push({ label: "LBLYEARS", value: 3 });
    this.relativeOptions.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.relativeOptionsList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Relative Option
    this.relativeOption.push({ label: "LBLSELECT", value: 0 });
    this.relativeOption.push({ label: "LBLWEEK", value: 1 });
    this.relativeOption.push({ label: "LBLMONTH", value: 2 });
    this.relativeOption.push({ label: "LBLYEAR", value: 3 });
    this.relativeOption.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.relativeOptionList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Send Email Option
    this.sendEmailOption.push({ label: "LBLWEEKLY", value: 2 });
    this.sendEmailOption.push({ label: "LBLMONTHLY", value: 3 });
    this.sendEmailOption.push({ label: "LBLDAY", value: 1 });
    this.sendEmailOption.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.sendEmailOptionList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //The Day
    this.theDay.push({ label: "LBLFIRST", value: 0 });
    this.theDay.push({ label: "LBLSEC", value: 1 });
    this.theDay.push({ label: "LBLTHIRD", value: 2 });
    this.theDay.push({ label: "LBLFOUR", value: 3 });
    this.theDay.push({ label: "LBLLAST", value: 4 });
    this.theDay.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.theDayList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Week Day
    this.weekDay.push({ label: "LBLSUNDAY", value: 0 });
    this.weekDay.push({ label: "LBLMONDAY", value: 1 });
    this.weekDay.push({ label: "LBLTUEDAY", value: 2 });
    this.weekDay.push({ label: "LBLWEDDAY", value: 3 });
    this.weekDay.push({ label: "LBLTHUDAY", value: 4 });
    this.weekDay.push({ label: "LBLFRIDAY", value: 5 });
    this.weekDay.push({ label: "LBLSATDAY", value: 6 });
    this.weekDay.map((item, key) => {
      this.translate.get(item.label).subscribe((text: string) => {
        this.weekDayList.push({
          value: item.value,
          label: text,
        });
      });
    });

    //Every Month Value
    for (let i = 1; i < 29; i++) {
      this.everyDwm.push({ label: i, value: i });
    }

    //Status
    this.translate.get(["LBLACTIVE", "LBLINACTIVE"]).subscribe((resLabel) => {
      this.StatusList.push({ label: resLabel["LBLACTIVE"], value: 1 });
      this.StatusList.push({ label: resLabel["LBLINACTIVE"], value: 0 });
      this.StatusList.map((item, key) => {
        this.translate.get(item.label).subscribe((text: string) => {
          this.Status.push({
            value: item.value,
            label: text,
          });
        });
      });
    });

    var quarterHours = ["00", "30"];
    var hour;
    for (var hours = 0; hours < 24; hours++) {
      for (var min = 0; min < 2; min++) {
        var minutes = +hours * 60 + +quarterHours[min];
        if (hours.toString().length == 1) {
          hour = "0" + hours;
        } else {
          hour = hours;
        }
        this.timeList.push({
          label: hour + ":" + quarterHours[min],
          value: minutes,
        });
      }
    }

    var data = {
      scheduleName: null,
      exportType: 1,
      exportFormat: 0,
      reportId: 0,
      designId: "",
      savedFilter: 0,
      relativeFor: 0,
      relativeOption: 0,
      relativeOptionType: 0,
      previousValue: "",
      tillDate: false,
      emailTo: "",
      schedulecc: "",
      scheduleSubject: null,
      scheduleBody: "",
      scheduleDay: null,
      dwm: 2,
      everyDwm: 1,
      monthlyDwm: null,
      startDate: null,
      endDate: null,
      timeZoneId: null,
      options: null,
      startTime: 0,
      status: 1,
    };
    this.createFormBuilder(data);
    this.getEditScheduleData();
  }

  //Replace by index
  replaceAt(str, index, ch) {
    return str.replace(/./g, (c, i) => (i == index ? ch : c));
  }

  getEditScheduleData() {
    var parameters = {
      scheduleId: this.scheduleId,
    };
    this.scheduleReports.getEditScheduleData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        var reportName = res["report"];
        reportName.map((item, key) => {
          this.translate.get(item.REPORTNAME).subscribe((text: string) => {
            this.reportName.push({
              REPORTID: item.REPORTID,
              REPORTNAME: text,
            });
          });
        });

        this.toUserList = [];
        var userList = res["toUser"];
        userList.map((data) => {
          this.toUserList.push({
            label: data["EMAIL"],
            value: data["USERID"],
          });
        });

        var timeZoneList = res["timeZone"];
        timeZoneList.map((item, key) => {
          this.translate.get(item.TIMEZONENAME).subscribe((text: string) => {
            this.timeZoneList.push({
              TIMEZONEID: item.TIMEZONEID,
              TIMEZONENAME: text,
            });
          });
        });

        this.keywords = res["bodyKeyword"];
        this.keywords.map((item) => {
          this.translate.get(item.label).subscribe((res) => {
            let newItem = {};
            newItem["text"] = res;
            newItem[
              "value"
            ] = `&nbsp;<span id=${item["keyWordId"]} class="mceNonEditable" contenteditable="false"><strong>[${res}]</strong></span>&nbsp;`;
            this.menuList.push(newItem);
          });
        });

        var that = this;

        var scheduleData = res["generalData"];
        var dayList = scheduleData["DAYLIST"];
        var dayListVal = dayList.indexOf("1");
        this.setEmailValue = scheduleData["DWM"];
        this.scheduleDay = dayListVal;
        this.monthlyDwm = scheduleData.EVERYDWM;
        this.dayDwm = scheduleData.EVERYDWM;
        var emailToId;
        if (scheduleData["EMAILTOID"] != "") {
          emailToId = scheduleData["EMAILTOID"].split(",").map(Number);
        } else {
          emailToId = "";
        }
        var tillData = false;
        if (scheduleData.TILLDATE == 1) {
          tillData = true;
        }
        var data = {
          scheduleName: scheduleData.SCHEDULENAME,
          exportType: scheduleData.EXPORTTYPE,
          exportFormat: scheduleData.EXPORTFORMAT,
          reportId: scheduleData.REPORTID,
          designId: scheduleData.DESIGNID,
          savedFilter: 0,
          relativeFor: scheduleData.RELATIVEFOR,
          relativeOption: scheduleData.RELATIVEOPTION,
          relativeOptionType: scheduleData.RELATIVEOPTIONTYPE,
          previousValue: scheduleData.PREVIOUSVALUE,
          tillDate: tillData,
          emailTo: emailToId,
          schedulecc: scheduleData.CC,
          scheduleSubject: scheduleData.SUBJECT,
          scheduleBody: scheduleData.BODY,
          scheduleDay: dayListVal,
          dwm: scheduleData.DWM,
          everyDwm: scheduleData.EVERYDWM,
          monthlyDwm: scheduleData.EVERYDWM,
          startDate: formatDate(
            scheduleData.STARTDATE,
            this.auth.UserInfo["dateFormat"],
            this.translate.getDefaultLang()
          ),
          endDate: formatDate(
            scheduleData.ENDDATE,
            this.auth.UserInfo["dateFormat"],
            this.translate.getDefaultLang()
          ),
          timeZoneId: scheduleData.TIMEZONE,
          options: null,
          startTime: parseInt(scheduleData.STARTTIME),
          status: scheduleData.STATUS,
        };
        this.createFormBuilder(data);

        this.initialGeneralFormData = this.addScheduleGeneral.getRawValue();
        this.reportSavedId = scheduleData.REPORTID;

        this.relativeDate = [];
        this.relativeDateList = [];

        if (scheduleData["REPORTID"] == 19) {
          this.relativeOptionsList.pop();
          this.relativeOptionList.pop();
        } else {
          if (this.relativeOptionsList.length < 4) {
            this.translate.get("LBLYEARS").subscribe((text: string) => {
              this.relativeOptionsList.push({
                value: 3,
                label: text,
              });
            });
          }
          if (this.relativeOptionList.length < 4) {
            this.translate.get("LBLYEAR").subscribe((text: string) => {
              this.relativeOptionList.push({
                value: 3,
                label: text,
              });
            });
          }
        }

        //Relative Date
        if (scheduleData["REPORTID"] == 14) {
          this.relativeDate.push({ label: "LBLSELECT", value: 0 });
          this.relativeDate.push({ label: "LBLOBSDATE", value: 1 });
          this.relativeDate.push({ label: "LBLCREATEDDATE", value: 2 });
        } else if (scheduleData["REPORTID"] == 15) {
          this.relativeDate.push({ label: "LBLSELECT", value: 0 });
          this.relativeDate.push({ label: "LBLOBSDATE", value: 1 });
          this.relativeDate.push({
            label: "LBLRESPONSEREQUIREDDATE",
            value: 3,
          });
          this.relativeDate.push({ label: "LBLRESPONDEDDATE", value: 4 });
        } else {
          this.relativeDate.push({ label: "LBLSELECT", value: 0 });
          this.relativeDate.push({ label: "LBLOBSDATE", value: 1 });
        }
        this.relativeDate.map((item, key) => {
          this.translate.get(item.label).subscribe((text: string) => {
            this.relativeDateList.push({
              value: item.value,
              label: text,
            });
          });
        });

        //Filter Data Start
        if (scheduleData["REPORTID"] == 0) {
          this.reportViewDataList = [];
          this.reportView = false;
          this.savedDataList = [];
          this.reportSaved = false;
          this.showSearchBox = false;
          this.showOpSearchBox = false;
        } else {
          if (res["reportVal"]["REPORTDISPLAY"] == 1) {
            this.reportViewDataList = [];
            this.savedDataList = [];
            this.reportView = true;
            var reportViewDataList = res["reportDesign"];
            reportViewDataList.map((item, key) => {
              this.translate.get(item.RPTNAME).subscribe((text: string) => {
                this.reportViewDataList.push({
                  DESIGNID: item.DESIGNID,
                  RPTNAME: text,
                });
              });
            });
          } else {
            this.reportViewDataList = [];
            this.reportView = false;
          }
          if (res["reportVal"]["FILTEREXISTS"] == 1) {
            this.reportSaved = true;
            var savedDataList = res["reportSaveFilter"];
            savedDataList.map((item, key) => {
              this.translate.get(item.FILTERNAME).subscribe((text: string) => {
                this.savedDataList.push({
                  FILTERID: item.FILTERID,
                  FILTERNAME: text,
                });
              });
            });
          } else {
            this.savedDataList = [];
            this.reportSaved = false;
          }

          if (res["reportVal"]["RELATIVEDATE"] == 1) {
            this.reportDate = true;
          } else {
            this.reportDate = false;
          }

          this.filterList = res["filter"];
          this.filterSetting = res["filterSetting"];
          var fn = this;
          this.filterList.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                var lableData = data[keies[1]];
                tempData.push({ label: lableData, value: data[keies[0]] });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
              this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
              item.Data = this.auth.rearrangeSelects(
                item.Data,
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
            }
          });

          if (this.filterList != "") {
            this.showSearchBox = true;
          } else {
            this.showSearchBox = false;
          }

          this.filterGroups = res["filterGroup"];
          this.filterGroups.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                var lableData = data[keies[1]];
                tempData.push({ label: lableData, value: data[keies[0]] });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
              this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
              item.Data = this.auth.rearrangeSelects(
                item.Data,
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
            }
          });

          this.filterCategory = res["filterCategories"];
          this.filterCategory.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                var lableData = data[keies[1]];
                tempData.push({ label: lableData, value: data[keies[0]] });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
              this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
              item.Data = this.auth.rearrangeSelects(
                item.Data,
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
            }
          });

          this.filterGroupList = res["filterGroupBy"];
          this.filterGroupList.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                this.translate.get(data[keies[1]]).subscribe((label) => {
                  tempData.push({ label: label, value: data[keies[0]] });
                });
              });
              item.Data = tempData;
            }
            // fn.addScheduleGeneral.addControl(
            //   item.REPORTPARAM,
            //   new FormControl(null, Validators.required)
            // );
            if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
              fn.addScheduleGeneral.addControl(
                item.REPORTPARAM,
                new FormControl(null, Validators.required)
              );
              this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                parseInt(this.filterSetting[item.REPORTPARAM])
              );
              this.addScheduleGeneral.controls[
                item.REPORTPARAM
              ].updateValueAndValidity();
              item.Data = this.auth.rearrangeSelects(
                item.Data,
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
            } else {
              fn.addScheduleGeneral.addControl(
                item.REPORTPARAM,
                new FormControl(null)
              );
            }
          });

          this.filterOptional = res["optionalFilter"];
          this.filterOptional.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                this.translate.get(data[keies[1]]).subscribe((label) => {
                  tempData.push({ label: label, value: data[keies[0]] });
                });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
              this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
              item.Data = this.auth.rearrangeSelects(
                item.Data,
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
            }
          });

          //Optional Filter Chart
          this.filterChart = res["filterChartType"];
          this.filterChart.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                this.translate.get(data[keies[1]]).subscribe((label) => {
                  tempData.push({ label: label, value: data[keies[0]] });
                });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl(null, Validators.required)
            );
            if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
              this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                parseInt(this.filterSetting[item.REPORTPARAM])
              );
              item.Data = this.auth.rearrangeSelects(
                item.Data,
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
            }
          });

          if (this.filterOptional != "") {
            this.showOpSearchBox = true;
          } else {
            this.showOpSearchBox = false;
          }

          if (this.reportDate == true) {
            this.addScheduleGeneral.controls["relativeFor"].setValidators([
              Validators.compose([Validators.required, Validators.min(1)]),
            ]);

            this.addScheduleGeneral.controls["relativeOption"].setValidators([
              Validators.compose([Validators.required, Validators.min(1)]),
            ]);
          } else {
            this.addScheduleGeneral.controls["relativeFor"].clearValidators();
            this.addScheduleGeneral.controls[
              "relativeFor"
            ].updateValueAndValidity();
            this.addScheduleGeneral.controls[
              "relativeOption"
            ].clearValidators();
            this.addScheduleGeneral.controls[
              "relativeOption"
            ].updateValueAndValidity();
          }

          this.showSearch = true;
        }
        //Filter Data End

        this.showRelativeOption(scheduleData["RELATIVEOPTION"]);
        this.sendEmailType(scheduleData["DWM"]);
        this.relativeOptionTypeChange();
      }
      this.isLoaded = false;

      var that = this;
      setTimeout(() => {
        tinymce.remove("#tinymceEditorEditSchedule");
        // tinymce.init({
        //   selector: "#tinymceEditorEditSchedule",
        //   base_url: "/tinymce",
        //   height: "200",
        //   suffix: ".min",
        //   skin_url: "tinymce/skins/lightgray",
        //   plugins: "table",
        //   toolbar: "table fontselect fontsizeselect mybutton",
        //   setup: function(editor) {
        //     editor.addButton("mybutton", {
        //       type: "listbox",
        //       text: that.Keyword,
        //       icon: false,
        //       onselect: function(e) {
        //         editor.insertContent(this.value());
        //       },
        //       values: that.menuList
        //     });
        //     editor.on("change", function(e) {
        //       that.addScheduleGeneral.patchValue({
        //         scheduleBody: editor.getContent()
        //       });
        //       that.addScheduleGeneral.get("scheduleBody").markAsDirty();
        //     });
        //   }
        // });
        var languageTinymce = "";
        let currentUser = this.auth.UserInfo;
        if (currentUser.languageCode == "da-dk") {
          var languageTinymce = "da";
        } else if (currentUser.languageCode == "de-de") {
          var languageTinymce = "de";
        } else if (currentUser.languageCode == "el") {
          var languageTinymce = "el";
        } else if (currentUser.languageCode == "en-us") {
          var languageTinymce = "en";
        } else if (currentUser.languageCode == "es-es") {
          var languageTinymce = "es";
        } else if (currentUser.languageCode == "fr-fr") {
          var languageTinymce = "fr-FR";
        } else if (currentUser.languageCode == "hu") {
          var languageTinymce = "hu_HU";
        } else if (currentUser.languageCode == "it-it") {
          var languageTinymce = "it";
        } else if (currentUser.languageCode == "ja-jp") {
          var languageTinymce = "ja";
        } else if (currentUser.languageCode == "ko") {
          var languageTinymce = "ko_KR";
        } else if (currentUser.languageCode == "nl-nl") {
          var languageTinymce = "nl";
        } else if (currentUser.languageCode == "pl-pl") {
          var languageTinymce = "pl";
        } else if (currentUser.languageCode == "pt-pt") {
          var languageTinymce = "pt_PT";
        } else if (currentUser.languageCode == "ru-ru") {
          var languageTinymce = "ru";
        } else if (currentUser.languageCode == "th-th") {
          var languageTinymce = "th_TH";
        } else if (currentUser.languageCode == "zh-cn") {
          var languageTinymce = "zh_CN";
        }
        tinymce.init({
          selector: "#tinymceEditorEditSchedule",
          base_url: "/tinymce",
          language: languageTinymce,
          height: "200",
          suffix: ".min",
          skin_url: "tinymce/skins/lightgray",
          plugins: "table",
          menubar: false,
          toolbar:
            "bold italic underline alignleft aligncenter alignright alignjustify numlist outdent indent table fontselect fontsizeselect mybutton preview ",
          setup: function (editor) {
            editor.addButton("mybutton", {
              type: "listbox",
              text: that.Keyword,
              icon: false,
              onselect: function (e) {
                editor.insertContent(this.value());
              },
              values: that.menuList,
            });
            editor.on("change", function (e) {
              that.addScheduleGeneral.patchValue({
                scheduleBody: editor.getContent(),
              });
              that.addScheduleGeneral.get("scheduleBody").markAsDirty();
            });
          },
        });
      }, 1200);
    });
    this.onSubmitTouched = false;
  }

  getFilters(reportId) {
    var parameters = {
      reportId: reportId,
    };
    var fn = this;
    this.reportSavedId = reportId;
    if (reportId == 0) {
      this.reportViewDataList = [];
      this.reportView = false;
      this.savedDataList = [];
      this.reportSaved = false;
      this.showSearchBox = false;
      this.showOpSearchBox = false;
    } else {
      this.scheduleReports.getReportData(parameters).subscribe((res) => {
        if (res["status"] == true) {
          this.reportViewDataList = [];
          this.savedDataList = [];
          this.relativeDate = [];
          this.relativeDateList = [];

          //Relative Date
          if (reportId == 14) {
            this.relativeDate.push({ label: "LBLSELECT", value: 0 });
            this.relativeDate.push({ label: "LBLOBSDATE", value: 1 });
            this.relativeDate.push({ label: "LBLCREATEDDATE", value: 2 });
          } else if (reportId == 15) {
            this.relativeDate.push({ label: "LBLSELECT", value: 0 });
            this.relativeDate.push({ label: "LBLOBSDATE", value: 1 });
            this.relativeDate.push({
              label: "LBLRESPONSEREQUIREDDATE",
              value: 3,
            });
            this.relativeDate.push({ label: "LBLRESPONDEDDATE", value: 4 });
          } else {
            this.relativeDate.push({ label: "LBLSELECT", value: 0 });
            this.relativeDate.push({ label: "LBLOBSDATE", value: 1 });
          }
          this.relativeDate.map((item, key) => {
            this.translate.get(item.label).subscribe((text: string) => {
              this.relativeDateList.push({
                value: item.value,
                label: text,
              });
            });
          });

          if (res["reportVal"]["REPORTDISPLAY"] == 1) {
            this.reportView = true;
            var reportViewDataList = res["reportDesign"];
            reportViewDataList.map((item, key) => {
              this.translate.get(item.RPTNAME).subscribe((text: string) => {
                this.reportViewDataList.push({
                  DESIGNID: item.DESIGNID,
                  RPTNAME: text,
                });
              });
            });
            this.addScheduleGeneral.controls["designId"].setValue(
              reportViewDataList[0]["DESIGNID"]
            );
          } else {
            this.reportViewDataList = [];
            this.reportView = false;
          }
          if (res["reportVal"]["FILTEREXISTS"] == 1) {
            this.reportSaved = true;
            var savedDataList = res["reportSaveFilter"];
            savedDataList.map((item, key) => {
              this.translate.get(item.FILTERNAME).subscribe((text: string) => {
                this.savedDataList.push({
                  FILTERID: item.FILTERID,
                  FILTERNAME: text,
                });
              });
            });
          } else {
            this.savedDataList = [];
            this.reportSaved = false;
          }

          if (res["reportVal"]["RELATIVEDATE"] == 1) {
            this.reportDate = true;
          } else {
            this.reportDate = false;
            this.addScheduleGeneral.controls["relativeFor"].clearValidators();
            this.addScheduleGeneral.controls[
              "relativeFor"
            ].updateValueAndValidity();
          }

          this.filterList = res["filter"];
          this.filterList.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                var lableData = data[keies[1]];
                tempData.push({ label: lableData, value: data[keies[0]] });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
              this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
              item.Data = this.auth.rearrangeSelects(
                item.Data,
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
            }
          });

          if (this.filterList != "") {
            this.showSearchBox = true;
          } else {
            this.showSearchBox = false;
          }

          this.filterGroups = res["filterGroup"];
          this.filterGroups.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                var lableData = data[keies[1]];
                tempData.push({ label: lableData, value: data[keies[0]] });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
              this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
              item.Data = this.auth.rearrangeSelects(
                item.Data,
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
            }
          });

          this.filterCategory = res["filterCategories"];
          this.filterCategory.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                var lableData = data[keies[1]];
                tempData.push({ label: lableData, value: data[keies[0]] });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
              this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
              item.Data = this.auth.rearrangeSelects(
                item.Data,
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
            }
          });

          this.filterGroupList = res["filterGroupBy"];
          this.filterGroupList.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                this.translate.get(data[keies[1]]).subscribe((label) => {
                  tempData.push({ label: label, value: data[keies[0]] });
                });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl(null, Validators.required)
            );
            if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
              this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                parseInt(this.filterSetting[item.REPORTPARAM])
              );
              item.Data = this.auth.rearrangeSelects(
                item.Data,
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
            }
          });

          //Optional Filter
          this.filterOptional = res["optionalFilter"];
          this.filterOptional.map((item) => {
            if (item.Data) {
              item.Data = JSON.parse(item.Data);
              var tempData = [];
              item.Data.map((data) => {
                var keies = Object.keys(data);
                this.translate.get(data[keies[1]]).subscribe((label) => {
                  tempData.push({ label: label, value: data[keies[0]] });
                });
              });
              item.Data = tempData;
            }
            fn.addScheduleGeneral.addControl(
              item.REPORTPARAM,
              new FormControl("")
            );
            if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
              this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
              item.Data = this.auth.rearrangeSelects(
                item.Data,
                this.filterSetting[item.REPORTPARAM].split(",").map(Number)
              );
            }
          });

          //Optional Filter
          this.filterChart = res["filterChartType"];
          if (res["filterChartType"] != "") {
            this.filterChart.map((item) => {
              if (item.Data) {
                item.Data = JSON.parse(item.Data);
                var tempData = [];
                item.Data.map((data) => {
                  var keies = Object.keys(data);
                  this.translate.get(data[keies[1]]).subscribe((label) => {
                    tempData.push({ label: label, value: data[keies[0]] });
                  });
                });
                item.Data = tempData;
              }
              fn.addScheduleGeneral.addControl(
                item.REPORTPARAM,
                new FormControl(null, Validators.required)
              );
              if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
                this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
                  parseInt(this.filterSetting[item.REPORTPARAM])
                );
                item.Data = this.auth.rearrangeSelects(
                  item.Data,
                  this.filterSetting[item.REPORTPARAM].split(",").map(Number)
                );
              }
            });
          }

          if (this.filterOptional != "" || this.filterChart != "") {
            this.showOpSearchBox = true;
          } else {
            this.showOpSearchBox = false;
          }

          if (this.reportDate == true) {
            this.addScheduleGeneral.controls["relativeFor"].setValidators([
              Validators.compose([Validators.required, Validators.min(1)]),
            ]);

            this.addScheduleGeneral.controls["relativeOption"].setValidators([
              Validators.compose([Validators.required, Validators.min(1)]),
            ]);
          } else {
            this.addScheduleGeneral.controls["relativeFor"].clearValidators();
            this.addScheduleGeneral.controls[
              "relativeFor"
            ].updateValueAndValidity();
            this.addScheduleGeneral.controls[
              "relativeOption"
            ].clearValidators();
            this.addScheduleGeneral.controls[
              "relativeOption"
            ].updateValueAndValidity();
          }

          this.addScheduleGeneral.controls["relativeFor"].setValue(0);
          this.addScheduleGeneral.controls["relativeOption"].setValue(0);
          this.addScheduleGeneral.controls["relativeOptionType"].setValue(0);
          this.addScheduleGeneral.controls["previousValue"].setValue(null);
          this.addScheduleGeneral.controls["tillDate"].setValue(0);
          this.showRelativeOption(0);
        }
        this.showSearch = true;
      });
    }

    if (reportId == 19) {
      this.relativeOptionsList.pop();
      this.relativeOptionList.pop();
    } else {
      if (this.relativeOptionsList.length < 4) {
        this.translate.get("LBLYEARS").subscribe((text: string) => {
          this.relativeOptionsList.push({
            value: 3,
            label: text,
          });
        });
      }
      if (this.relativeOptionList.length < 4) {
        this.translate.get("LBLYEAR").subscribe((text: string) => {
          this.relativeOptionList.push({
            value: 3,
            label: text,
          });
        });
      }
    }

    if (reportId != 16) {
      if (this.addScheduleGeneral.get("CHKSUMGROUP1")) {
        this.addScheduleGeneral.get("CHKSUMGROUP1").clearValidators();
        this.addScheduleGeneral.get("CHKSUMGROUP1").updateValueAndValidity();
      }
      if (this.addScheduleGeneral.get("CHKSUMGROUP2")) {
        this.addScheduleGeneral.get("CHKSUMGROUP2").clearValidators();
        this.addScheduleGeneral.get("CHKSUMGROUP2").updateValueAndValidity();
      }
      if (this.addScheduleGeneral.get("CHKSUMGROUP3")) {
        this.addScheduleGeneral.get("CHKSUMGROUP3").clearValidators();
        this.addScheduleGeneral.get("CHKSUMGROUP3").updateValueAndValidity();
      }
    }
  }

  getSavedFilters(filterId) {
    var parameters = {
      filterId: filterId,
      reportId: this.reportSavedId,
    };
    var fn = this;
    this.scheduleReports.getSavedFilterData(parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.filterSetting = res["filterSetting"];
        this.filterList = res["filter"];
        this.filterList.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              tempData.push({ label: data[keies[1]], value: data[keies[0]] });
            });
            item.Data = tempData;
          }
          if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
            item.Data = this.auth.rearrangeSelects(
              item.Data,
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
          }
        });

        this.filterGroups = res["filterGroup"];
        this.filterGroups.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              tempData.push({ label: data[keies[1]], value: data[keies[0]] });
            });
            item.Data = tempData;
          }
          if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
            item.Data = this.auth.rearrangeSelects(
              item.Data,
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
          }
        });

        this.filterCategory = res["filterCategories"];
        this.filterCategory.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              tempData.push({ label: data[keies[1]], value: data[keies[0]] });
            });
            item.Data = tempData;
          }
          if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
            item.Data = this.auth.rearrangeSelects(
              item.Data,
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
          }
        });

        this.filterGroupList = res["filterGroupBy"];
        this.filterGroupList.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              tempData.push({ label: data[keies[1]], value: data[keies[0]] });
            });
            item.Data = tempData;
          }
          fn.addScheduleGeneral.addControl(
            item.REPORTPARAM,
            new FormControl(null, Validators.required)
          );
          this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
        });

        this.filterOptional = res["optionalFilter"];
        this.filterOptional.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              this.translate.get(data[keies[1]]).subscribe((label) => {
                tempData.push({ label: label, value: data[keies[0]] });
              });
            });
            item.Data = tempData;
          }
          if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
            this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
            item.Data = this.auth.rearrangeSelects(
              item.Data,
              this.filterSetting[item.REPORTPARAM].split(",").map(Number)
            );
          }
        });

        //Optional Filter Chart
        this.filterChart = res["filterChartType"];
        this.filterChart.map((item) => {
          if (item.Data) {
            item.Data = JSON.parse(item.Data);
            var tempData = [];
            item.Data.map((data) => {
              var keies = Object.keys(data);
              this.translate.get(data[keies[1]]).subscribe((label) => {
                tempData.push({ label: label, value: data[keies[0]] });
              });
            });
            item.Data = tempData;
          }
          fn.addScheduleGeneral.addControl(
            item.REPORTPARAM,
            new FormControl(null, Validators.required)
          );
          this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
        });
      }
    });
  }

  getOtherData(reportParam) {
    if (reportParam == "SITEID") {
      var siteParam = {
        siteId: this.addScheduleGeneral.value[reportParam].join(),
      };
      this.reportService.getDataBySite(siteParam).subscribe((res) => {
        if (res["status"] == true) {
          this.filterList.map((item) => {
            if (item["REPORTPARAM"] == "AREAID") {
              var tempData = [];
              if (res["area"].length > 0) {
                res["area"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
            } else if (item["REPORTPARAM"] == "SHIFTID") {
              var tempData = [];
              if (res["shift"].length > 0) {
                res["shift"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
            } else if (item["REPORTPARAM"] == "OBSERVERID") {
              var tempData = [];
              if (res["observer"].length > 0) {
                res["observer"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
            } else if (item["REPORTPARAM"] == "CUSTOMFIELD1") {
              var tempData = [];
              if (res["customField1"].length > 0) {
                res["customField1"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
            } else if (item["REPORTPARAM"] == "CUSTOMFIELD2") {
              var tempData = [];
              if (res["customField2"].length > 0) {
                res["customField2"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
            } else if (item["REPORTPARAM"] == "CUSTOMFIELD3") {
              var tempData = [];
              if (res["customField3"].length > 0) {
                res["customField3"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
            } else if (item["REPORTPARAM"] == "SETUPID") {
              var tempData = [];
              if (res["checkList"].length > 0) {
                res["checkList"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
            }
          });
        }
      });
    } else if (reportParam == "AREAID") {
      var areaParam = {
        areaId: this.addScheduleGeneral.value[reportParam].join(),
      };
      this.reportService.getDataByArea(areaParam).subscribe((res) => {
        if (res["status"] == true) {
          this.filterList.map((item) => {
            if (item["REPORTPARAM"] == "SUBAREAID") {
              var tempData = [];
              if (res["subArea"].length > 0) {
                res["subArea"].map((data) => {
                  var keies = Object.keys(data);
                  tempData.push({
                    label: data[keies[1]],
                    value: data[keies[0]],
                  });
                });
              }
              item.Data = tempData;
            }
          });
        }
      });
    } else if (reportParam == "SETUPID") {
      var checkParam = {
        chkListSetupId: this.addScheduleGeneral.value[reportParam].join(),
      };
      this.scheduleReports
        .getCheckListFilterMainCatg(checkParam)
        .subscribe((res) => {
          if (res["status"] == true) {
            this.filterList.map((item) => {
              if (item["REPORTPARAM"] == "MAINCATEGORYID") {
                var tempData = [];
                if (res["mainCat"].length > 0) {
                  res["mainCat"].map((data) => {
                    var keies = Object.keys(data);
                    tempData.push({
                      label: data[keies[1]],
                      value: data[keies[0]],
                    });
                  });
                }
                item.Data = tempData;
              }
            });
          }
        });
    } else if (
      reportParam == "CHKSUMGROUP1" ||
      reportParam == "CHKSUMGROUP2" ||
      reportParam == "CHKSUMGROUP3"
    ) {
      var groupParam = {
        group1: this.addScheduleGeneral.value["CHKSUMGROUP1"],
        group2: this.addScheduleGeneral.value["CHKSUMGROUP2"],
        group3: this.addScheduleGeneral.value["CHKSUMGROUP3"],
      };
      this.scheduleReports.getGroupByFilter(groupParam).subscribe((res) => {
        if (res["status"] == true) {
          this.filterGroupList.map((item) => {
            if (item["REPORTPARAM"] == "CHKSUMGROUP1") {
              var tempData = [];
              if (res["group1"].length > 0) {
                res["group1"].map((data) => {
                  var keies = Object.keys(data);
                  this.translate.get(data[keies[1]]).subscribe((label) => {
                    tempData.push({ label: label, value: data[keies[0]] });
                  });
                });
              }
              item.Data = tempData;
            }

            if (item["REPORTPARAM"] == "CHKSUMGROUP2") {
              var tempData = [];
              if (res["group2"].length > 0) {
                res["group2"].map((data) => {
                  var keies = Object.keys(data);
                  this.translate.get(data[keies[1]]).subscribe((label) => {
                    tempData.push({ label: label, value: data[keies[0]] });
                  });
                });
              }
              item.Data = tempData;
            }

            if (item["REPORTPARAM"] == "CHKSUMGROUP3") {
              var tempData = [];
              if (res["group3"].length > 0) {
                res["group3"].map((data) => {
                  var keies = Object.keys(data);
                  this.translate.get(data[keies[1]]).subscribe((label) => {
                    tempData.push({ label: label, value: data[keies[0]] });
                  });
                });
              }
              item.Data = tempData;
            }
          });
        }
      });
      this.filterGroupList.map((item) => {
        this.addScheduleGeneral.get(item.REPORTPARAM).clearValidators();
        this.addScheduleGeneral.get(item.REPORTPARAM).updateValueAndValidity();
      });
      if (this.addScheduleGeneral.get("CHARTTYPE")) {
        this.addScheduleGeneral.get("CHARTTYPE").clearValidators();
        this.addScheduleGeneral.get("CHARTTYPE").updateValueAndValidity();
      }
    } else if (reportParam == "CHARTTYPE") {
      if (this.addScheduleGeneral.get("CHKSUMGROUP1")) {
        this.addScheduleGeneral.get("CHKSUMGROUP1").clearValidators();
        this.addScheduleGeneral.get("CHKSUMGROUP1").updateValueAndValidity();
      }
      if (this.addScheduleGeneral.get("CHKSUMGROUP2")) {
        this.addScheduleGeneral.get("CHKSUMGROUP2").clearValidators();
        this.addScheduleGeneral.get("CHKSUMGROUP2").updateValueAndValidity();
      }
      if (this.addScheduleGeneral.get("CHKSUMGROUP3")) {
        this.addScheduleGeneral.get("CHKSUMGROUP3").clearValidators();
        this.addScheduleGeneral.get("CHKSUMGROUP3").updateValueAndValidity();
      }
    }
  }

  showRelativeOption(rlValue) {
    this.reportValue = rlValue;
    if (rlValue == 1) {
      this.addScheduleGeneral.controls["previousValue"].clearValidators();
      this.addScheduleGeneral.controls[
        "previousValue"
      ].updateValueAndValidity();
      this.addScheduleGeneral.controls["relativeOptionType"].setValidators([
        Validators.compose([Validators.required, Validators.min(1)]),
      ]);
    } else if (rlValue == 2) {
      this.addScheduleGeneral.controls["relativeOptionType"].setValidators([
        Validators.compose([Validators.required, Validators.min(1)]),
      ]);
      this.addScheduleGeneral.controls["previousValue"].setValidators([
        Validators.compose([
          Validators.maxLength(2),
          Validators.pattern("[0-9]*"),
        ]),
      ]);
      this.relativeOptionTypeChange();
    }
  }

  relativeOptionTypeChange() {
    this.translate
      .get([
        "ALTPREVIOUSWEEK",
        "ALTPREVIOUSMONTH",
        "ALTPREVIOUSYEAR",
        "ALTPREVIOUSMONTHTVA",
        "ALTOBSERVATIONDATEMONTHPERIOD",
        "ALTOBSERVATIONDATEMONTHPERIODWEEK",
      ])
      .subscribe((resLabel) => {
        if (
          this.addScheduleGeneral.value["reportId"] == 6 ||
          this.addScheduleGeneral.value["reportId"] == 23
        ) {
          if (this.addScheduleGeneral.value["relativeOptionType"] == 1) {
            this.addScheduleGeneral.controls["previousValue"].setValidators([
              Validators.compose([
                Validators.maxLength(2),
                Validators.max(52),
                Validators.pattern("[0-9]*"),
              ]),
            ]);
            this.previousValError = resLabel["ALTPREVIOUSWEEK"];
            this.addScheduleGeneral.controls[
              "previousValue"
            ].updateValueAndValidity();
          } else if (this.addScheduleGeneral.value["relativeOptionType"] == 2) {
            this.addScheduleGeneral.controls["previousValue"].setValidators([
              Validators.compose([
                Validators.maxLength(2),
                Validators.max(12),
                Validators.pattern("[0-9]*"),
              ]),
            ]);
            this.previousValError = resLabel["ALTPREVIOUSMONTH"];
            this.addScheduleGeneral.controls[
              "previousValue"
            ].updateValueAndValidity();
          } else if (this.addScheduleGeneral.value["relativeOptionType"] == 3) {
            this.addScheduleGeneral.controls["previousValue"].setValidators([
              Validators.compose([
                Validators.maxLength(2),
                Validators.max(1),
                Validators.pattern("[0-9]*"),
              ]),
            ]);
            this.previousValError = resLabel["ALTPREVIOUSYEAR"];
            this.addScheduleGeneral.controls[
              "previousValue"
            ].updateValueAndValidity();
          }
        } else if (this.addScheduleGeneral.value["reportId"] == 19) {
          if (this.addScheduleGeneral.value["relativeOptionType"] == 1) {
            this.addScheduleGeneral.controls["previousValue"].setValidators([
              Validators.compose([
                Validators.maxLength(2),
                Validators.max(12),
                Validators.pattern("[0-9]*"),
              ]),
            ]);
            this.previousValError =
              resLabel["ALTOBSERVATIONDATEMONTHPERIODWEEK"];
            this.addScheduleGeneral.controls[
              "previousValue"
            ].updateValueAndValidity();
          } else if (this.addScheduleGeneral.value["relativeOptionType"] == 2) {
            this.addScheduleGeneral.controls["previousValue"].setValidators([
              Validators.compose([
                Validators.maxLength(2),
                Validators.max(3),
                Validators.pattern("[0-9]*"),
              ]),
            ]);
            this.previousValError = resLabel["ALTPREVIOUSMONTHTVA"];
            this.addScheduleGeneral.controls[
              "previousValue"
            ].updateValueAndValidity();
          }
        }
      });
  }

  sendEmailType(emailVal) {
    this.emailValue = emailVal;
    if (emailVal == 1) {
      this.addScheduleGeneral.controls["everyDwm"].setValidators([
        Validators.required,
      ]);
      this.addScheduleGeneral.get("monthlyDwm").clearValidators();
      this.addScheduleGeneral.get("scheduleDay").clearValidators();
    } else if (emailVal == 2) {
      this.addScheduleGeneral.controls["scheduleDay"].setValidators([
        Validators.required,
      ]);
      this.addScheduleGeneral.get("monthlyDwm").clearValidators();
      this.addScheduleGeneral.get("everyDwm").clearValidators();
    } else if (emailVal == 3) {
      this.addScheduleGeneral.controls["monthlyDwm"].setValidators([
        Validators.required,
      ]);
      this.addScheduleGeneral.controls["scheduleDay"].setValidators([
        Validators.required,
      ]);
      this.addScheduleGeneral.get("everyDwm").clearValidators();
    }

    //Set Value
    if (this.setEmailValue != emailVal) {
      this.addScheduleGeneral.controls["scheduleDay"].setValue("");
      this.addScheduleGeneral.controls["monthlyDwm"].setValue("");
      this.addScheduleGeneral.controls["everyDwm"].setValue(1);
    } else {
      this.addScheduleGeneral.controls["scheduleDay"].setValue(
        this.scheduleDay
      );
      this.addScheduleGeneral.controls["monthlyDwm"].setValue(this.monthlyDwm);
      this.addScheduleGeneral.controls["everyDwm"].setValue(this.dayDwm);
    }
    this.addScheduleGeneral.get("everyDwm").updateValueAndValidity();
    this.addScheduleGeneral.get("monthlyDwm").updateValueAndValidity();
    this.addScheduleGeneral.get("scheduleDay").updateValueAndValidity();
  }

  // Advance serach
  advance_search() {
    this.showAdvanceSearch = !this.showAdvanceSearch;
    this.iconType =
      this.iconType === "ui-icon-add" ? "ui-icon-remove" : "ui-icon-add";
  }

  toggle() {
    this.showSearch = !this.showSearch;
  }

  //Clear Filter
  clearFilter() {
    if (this.filterList != undefined) {
      this.filterList.map((item) => {
        this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
      });
    }
    if (this.filterGroupList != undefined) {
      this.filterGroupList.map((item) => {
        this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
        this.addScheduleGeneral.controls[item.REPORTPARAM].setValidators([
          Validators.compose([Validators.required]),
        ]);
        this.addScheduleGeneral.controls[
          item.REPORTPARAM
        ].updateValueAndValidity();
      });
    }
    if (this.filterOptional != undefined) {
      this.filterOptional.map((item) => {
        this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
      });
    }
    if (this.filterChart != undefined) {
      this.filterChart.map((item) => {
        this.addScheduleGeneral.controls[item.REPORTPARAM].setValue(null);
      });
    }
  }

  get GeneralSubForm() {
    return this.addScheduleGeneral.controls;
  }

  createFormBuilder(data) {
    this.addScheduleGeneral = this.fb.group({
      scheduleName: new FormControl(
        data.scheduleName,
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),
      exportType: new FormControl(data.exportType),
      exportFormat: new FormControl(data.exportFormat),
      reportId: new FormControl(
        data.reportId,
        Validators.compose([Validators.required, Validators.min(1)])
      ),
      designId: new FormControl(data.designId),
      savedFilter: new FormControl(data.savedFilter),
      relativeFor: new FormControl(data.relativeFor),
      relativeOption: new FormControl(data.relativeOption),
      relativeOptionType: new FormControl(data.relativeOptionType),
      previousValue: new FormControl(data.previousValue),
      tillDate: new FormControl(data.tillDate),
      emailTo: new FormControl(data.emailTo, Validators.required),
      schedulecc: new FormControl(data.schedulecc, [
        // this.customValidatorsService.noCommaValidator,
        // Validators.pattern(
        //   "^(\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*;)*(\\w+([-+.']\\w+)*@\\w+([-.]w+)*\\.\\w{2,}([-.]\\w+)*)$"
        // ),
        this.customValidatorsService.ccEmailValid,
      ]),
      scheduleSubject: new FormControl(
        data.scheduleSubject,
        Validators.compose([
          Validators.required,
          Validators.maxLength(256),
          this.customValidatorsService.noWhitespaceValidator,
        ])
      ),
      scheduleBody: new FormControl(data.scheduleBody, Validators.required),
      scheduleDay: new FormControl(data.scheduleDay, Validators.required),
      dwm: new FormControl(data.dwm),
      everyDwm: new FormControl(data.everyDwm),
      monthlyDwm: new FormControl(data.monthlyDwm),
      startDate: new FormControl(data.startDate, Validators.required),
      endDate: new FormControl(data.endDate, Validators.required),
      timeZoneId: new FormControl(data.timeZoneId),
      status: new FormControl(data.status),
      options: new FormControl(data.options),
      startTime: new FormControl(data.startTime),
    });
  }

  resetScheduleForm() {
    this.addScheduleGeneral.patchValue(this.initialGeneralFormData);
  }

  onSubmit(value: string) {
    this.submitted = true;
    this.addScheduleGeneral.patchValue({
      scheduleBody: tinymce.get("tinymceEditorEditSchedule").getContent(),
    });
    if (this.addScheduleGeneral.invalid) {
      this.customValidatorsService.scrollToError();
      // const invalid = [];
      // const controls = this.addScheduleGeneral.controls;
      // for (const name in controls) {
      //   if (controls[name].invalid) {
      //     invalid.push(name);
      //   }
      // }
    } else {
      this.isLoaded = true;
      this.onSubmitTouched = true;
      var startDate = new Date(this.addScheduleGeneral.value["startDate"]);
      var endDate = new Date(this.addScheduleGeneral.value["endDate"]);
      if (this.dateFormatType == "dd/mm/yyyy") {
        if (
          this.addScheduleGeneral.value["startDate"].toString().search("/") !=
          -1
        ) {
          var dateStart = this.addScheduleGeneral.value["startDate"].split("/");
          startDate = new Date(
            dateStart[1] + "/" + dateStart[0] + "/" + dateStart[2]
          );
        }
        if (
          this.addScheduleGeneral.value["endDate"].toString().search("/") != -1
        ) {
          var dateEnd = this.addScheduleGeneral.value["endDate"].split("/");
          endDate = new Date(dateEnd[1] + "/" + dateEnd[0] + "/" + dateEnd[2]);
        }
      }
      this.addScheduleGeneral.value["startDate"] = formatDate(
        startDate,
        "MM/dd/yyyy",
        this.translate.getDefaultLang()
      );
      this.addScheduleGeneral.value["endDate"] = formatDate(
        endDate,
        "MM/dd/yyyy",
        this.translate.getDefaultLang()
      );

      if (startDate > endDate) {
        this.translate.get("ALTDATEVALIDATE").subscribe((sdRes: string) => {
          this.messageService.add({
            severity: "error",
            detail: sdRes,
          });
        });
        this.isLoaded = false;
      } else {
        var newSelectedFilter = {};
        this.filterList.map((item) => {
          if (item.REPORTPARAM != "") {
            if (
              this.addScheduleGeneral.value[item.REPORTPARAM] != "" &&
              this.addScheduleGeneral.value[item.REPORTPARAM] != null
            ) {
              newSelectedFilter[
                item.REPORTPARAM
              ] = this.addScheduleGeneral.value[item.REPORTPARAM].join();
            }
            delete this.addScheduleGeneral.value[item.REPORTPARAM];
          }
        });

        if (this.filterGroups != "") {
          this.filterGroups.map((item) => {
            if (
              this.addScheduleGeneral.value[item.REPORTPARAM] != "" &&
              this.addScheduleGeneral.value[item.REPORTPARAM] != null
            ) {
              newSelectedFilter[
                item.REPORTPARAM
              ] = this.addScheduleGeneral.value[item.REPORTPARAM].join();
            }
            delete this.addScheduleGeneral.value[item.REPORTPARAM];
          });
        }

        if (this.filterGroupList != "") {
          this.filterGroupList.map((item) => {
            if (
              this.addScheduleGeneral.value[item.REPORTPARAM] != "" &&
              this.addScheduleGeneral.value[item.REPORTPARAM] != null
            ) {
              newSelectedFilter[
                item.REPORTPARAM
              ] = this.addScheduleGeneral.value[item.REPORTPARAM];
            }
            delete this.addScheduleGeneral.value[item.REPORTPARAM];
          });
        }

        this.filterOptional.map((item) => {
          if (
            this.addScheduleGeneral.value[item.REPORTPARAM] != "" &&
            this.addScheduleGeneral.value[item.REPORTPARAM] != null
          ) {
            newSelectedFilter[item.REPORTPARAM] = this.addScheduleGeneral.value[
              item.REPORTPARAM
            ].join();
          }
          delete this.addScheduleGeneral.value[item.REPORTPARAM];
        });

        if (this.filterCategory != "") {
          this.filterCategory.map((item) => {
            if (
              this.addScheduleGeneral.value[item.REPORTPARAM] != "" &&
              this.addScheduleGeneral.value[item.REPORTPARAM] != null
            ) {
              newSelectedFilter[
                item.REPORTPARAM
              ] = this.addScheduleGeneral.value[item.REPORTPARAM].join();
            }
            delete this.addScheduleGeneral.value[item.REPORTPARAM];
          });
        }

        this.addScheduleGeneral.value["options"] = [newSelectedFilter];

        this.addScheduleGeneral.value[
          "emailTo"
        ] = this.addScheduleGeneral.value["emailTo"].join();

        var dayListPos = this.addScheduleGeneral.value["scheduleDay"];
        var dayList = "0000000";
        dayList = this.replaceAt(dayList, dayListPos, "1");
        this.addScheduleGeneral.value["dayList"] = dayList;
        this.addScheduleGeneral.value["scheduleId"] = this.scheduleId;
        var tillDate = 0;
        if (this.addScheduleGeneral.value["tillDate"] == true) {
          tillDate = 1;
        }
        this.addScheduleGeneral.value["tillDate"] = tillDate;
        this.scheduleReports
          .updateScheduleReport(this.addScheduleGeneral.value)
          .subscribe((res) => {
            this.translate.get(res["message"]).subscribe((message) => {
              if (res["status"] == true) {
                this.submitted = false;
                this.getEditScheduleData();
                this.messageService.add({
                  severity: "success",
                  detail: message,
                });
                this.isLoaded = false;
              } else {
                this.onSubmitTouched = false;
                this.messageService.add({
                  severity: "error",
                  detail: message,
                });
                this.isLoaded = false;
              }
            });
          });
      }
    }
  }
  onSelectAllAdv(i) {
    const selected = this.filterOptional[i]["Data"].map((item) => item.value);
    this.addScheduleGeneral
      .get(this.filterOptional[i]["REPORTPARAM"])
      .patchValue(selected);
  }

  onClearAllAdv(i) {
    this.addScheduleGeneral
      .get(this.filterOptional[i]["REPORTPARAM"])
      .patchValue([]);
  }

  onSelectAll(i) {
    const selected = this.filterList[i]["Data"].map((item) => item.value);
    this.addScheduleGeneral
      .get(this.filterList[i]["REPORTPARAM"])
      .patchValue(selected);
  }

  onClearAll(i) {
    this.addScheduleGeneral
      .get(this.filterList[i]["REPORTPARAM"])
      .patchValue([]);
  }
}
