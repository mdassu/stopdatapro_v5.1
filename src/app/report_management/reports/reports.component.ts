import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";

@Component({
  selector: "app-reports",
  templateUrl: "./reports.component.html",
  styleUrls: ["./reports.component.css"]
})
export class ReportsComponent implements OnInit {
  reports: any = [{ report: "" }];
  reportsList: any = [];
  volumeAnalysis: any;
  trendAnalysis: any;
  isLoaded: boolean;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private reportsService: ReportsService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT", url: "./assets/help/reports-listing.md" },
      { label: "LBLREPORTS" }
    ]);
  }

  ngOnInit() {
    localStorage.removeItem("filterInfo");
    this.getAllReports();
  }

  getAllReports() {
    this.reportsService.getReportList().subscribe(res => {
      if (res["status"] == true) {
        var list = res["data"];
        var volumeAnalysis = list.filter(item => {
          return item.REPORTTYPE == 1;
        });
        var trendAnalysis = list.filter(item => {
          return item.REPORTTYPE == 2;
        });
        if (volumeAnalysis.length >= trendAnalysis.length) {
          volumeAnalysis.map((report, key) => {
            var trendReport;
            if (trendAnalysis[key]) {
              trendReport = trendAnalysis[key];
            } else {
              trendReport = "";
            }
            this.reportsList.push({
              volumeReport: report,
              trendReport: trendReport
            });
          });
        } else {
          trendAnalysis.map((report, key) => {
            var volumeReport;
            if (trendAnalysis[key]) {
              if (volumeAnalysis[key]) {
                volumeReport = volumeAnalysis[key];
              } else {
                volumeReport = "";
              }
            } else {
              volumeReport = "";
            }
            this.reportsList.push({
              volumeReport: volumeReport,
              trendReport: report
            });
          });
        }
        this.isLoaded = true;
      }
    });
  }

  loader() {
    this.isLoaded = false;
  }
}
