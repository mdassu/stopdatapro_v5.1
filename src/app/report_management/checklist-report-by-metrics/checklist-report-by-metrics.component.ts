import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { formatDate } from "@angular/common";
import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";

import { TranslateService } from "@ngx-translate/core";
import { isArray } from "util";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { GlobalDataService } from "src/app/_services/global-data.service";

@Component({
  selector: "app-checklist-report-by-metrics",
  templateUrl: "./checklist-report-by-metrics.component.html",
  styleUrls: ["./checklist-report-by-metrics.component.css"],
})
export class ChecklistReportByMetricsComponent implements OnInit {
  @Input() reportId: any;
  siteCols: any[] = [];
  selectedReportView: any = 13;
  selectedSiteId: any = "";
  cols: any[];
  tableData: any = [];
  indexData: any = [];
  title: any;
  isLoaded: boolean;
  errormsg: any;
  rowGroupMetadata: any;
  reportView: any = [];
  siteList: any = [{ label: "All", value: "" }];
  parameters: any;
  allSiteSelected: any;
  dateFormat: any;
  fieldCols: any = [];
  siteTableRows: any;
  sitePDFRows: any;
  siteColumns: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  unFColumns: any;
  unFTableRows: any;
  pdfData: any = [];
  dataKey: any = "SITENAME";
  colId: any;
  mainHead: any;
  subAreaOption: number;
  colleps: number = 0;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService,
    private globalData: GlobalDataService
  ) {}

  ngOnInit() {
    this.dateFormat = this.auth.UserInfo["dateFormat"];
    this.translate.get("LBLCHKBYMETRICSRPT").subscribe((title) => {
      this.title = title;
    });

    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT", url: "./assets/help/report-by-metrics.md" },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);

    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.allSiteSelected = this.parameters["SITEID"];
      this.selectedReportView = this.parameters["designId"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }
    this.getReports();
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (isArray(res["designData"])) {
        this.reportView = [];
        var designData = res["designData"];
        designData.map((item) => {
          this.translate.get(item.RPTNAME).subscribe((label) => {
            this.reportView.push({
              label: label,
              value: item.DESIGNID,
            });
          });
        });
      }
      if (res["status"] == true) {
        this.tableData = res["Data"];

        this.siteList = res["sitelist"];

        this.fieldCols = [];
        if (this.tableData.length > 0) {
          var keys = Object.keys(this.tableData[0]);
        }
        var tempColumn = [
          "AREAID",
          "AREANAME",
          "CARDID",
          "CUSPOSITION",
          "DATE",
          "OBSERVATIONDATE",
          "OBSERVERNAME",
          "SAFECOMMENTS",
          "SHIFTID",
          "SHIFTNAME",
          "SITEID",
          "SITENAME",
          "SUBAREAID",
          "SUBAREANAME",
          "UNSAFECOMMENTS",
        ];

        keys.map((label) => {
          if (tempColumn.indexOf(label) == -1) {
            this.fieldCols.push(label);
          }
        });

        this.translate
          .get([
            "LBLCHECKLISTNO",
            "LBLOBSERVERNAME",
            "LBLAREANAME",
            "LBLSUBAREAFULLNAME",
            "LBLSHIFTNAME",
            "FMKDTRANGE",
            "LBLSAFECOMMENTS",
            "LBLUNSAFECOMMENTS",
            "LBLSITENAME",
            "LBLSAFECOUNT",
            "LBLUNSAFECOUNT",
          ])
          .subscribe((resLabel) => {
            this.subAreaOption = this.auth.UserInfo["subAreaOptionValue"];
            if (this.subAreaOption == 0) {
              this.colleps = 1;
              this.cols = [
                {
                  field: "CARDID",
                  header: resLabel["LBLCHECKLISTNO"],
                },
                { field: "OBSERVERNAME", header: resLabel["LBLOBSERVERNAME"] },
                { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                { field: "SHIFTNAME", header: resLabel["LBLSHIFTNAME"] },
                { field: "DATE", header: resLabel["FMKDTRANGE"] },
                { field: "SAFECOMMENTS", header: resLabel["LBLSAFECOMMENTS"] },
                {
                  field: "UNSAFECOMMENTS",
                  header: resLabel["LBLUNSAFECOMMENTS"],
                },
              ];
            } else {
              this.cols = [
                {
                  field: "CARDID",
                  header: resLabel["LBLCHECKLISTNO"],
                },
                { field: "OBSERVERNAME", header: resLabel["LBLOBSERVERNAME"] },
                { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                {
                  field: "SUBAREANAME",
                  header: resLabel["LBLSUBAREAFULLNAME"],
                },
                { field: "SHIFTNAME", header: resLabel["LBLSHIFTNAME"] },
                { field: "DATE", header: resLabel["FMKDTRANGE"] },
                { field: "SAFECOMMENTS", header: resLabel["LBLSAFECOMMENTS"] },
                {
                  field: "UNSAFECOMMENTS",
                  header: resLabel["LBLUNSAFECOMMENTS"],
                },
              ];
            }
            if (this.selectedReportView == 13) {
              this.siteCols = [
                {
                  field: "SITENAME",
                  header: resLabel["LBLSITENAME"],
                },
                {
                  field: "SAFE",
                  header: resLabel["LBLSAFECOUNT"],
                },
                {
                  field: "UNSAFE",
                  header: resLabel["LBLUNSAFECOUNT"],
                },
              ];
            } else if (this.selectedReportView == 14) {
              this.siteCols = [
                {
                  field: "SITENAME",
                  header: resLabel["LBLSITENAME"],
                },
                {
                  field: "SAFE",
                  header: resLabel["LBLSAFECOUNT"],
                },
              ];
            } else if (this.selectedReportView == 15) {
              this.siteCols = [
                {
                  field: "SITENAME",
                  header: resLabel["LBLSITENAME"],
                },
                {
                  field: "UNSAFE",
                  header: resLabel["LBLUNSAFECOUNT"],
                },
              ];
            }
          });

        this.updateRowGroupMetaData();
        this.manageExportData();
        this.isLoaded = true;
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    this.mainHead = [];
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let tableData = this.tableData[i];
        let sitename = tableData.SITENAME;
        if (i == 0) {
          this.rowGroupMetadata[sitename] = { index: 0, size: 1 };
          this.mainHead.push({
            SITENAME: sitename,
          });
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.SITENAME;
          if (sitename === previousRowGroup) {
            this.rowGroupMetadata[sitename].size++;
          } else {
            this.rowGroupMetadata[sitename] = { index: i, size: 1 };
            this.mainHead.push({
              SITENAME: sitename,
            });
          }
        }
      }
    }
  }

  changeReport() {
    this.isLoaded = true;
    this.tableData = [];
    this.parameters["designId"] = this.selectedReportView;

    this.getReports();
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    this.siteTableRows = [];
    this.sitePDFRows = [];
    this.siteColumns = [];
    var enableSubarea = 0;
    if (this.globalData.GlobalData["ENABLESUBAREA"] != 1) {
      enableSubarea = 1;
    }
    // if (this.selectedReportView == 22) {
    // columns.map(item => {
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempDataJson = {};
        this.translate.get("LBLSITENAME").subscribe((label) => {
          if (key == 0) {
            this.unFColumns.push(label);
          }
          tempDataJson[label] = data["SITENAME"];
        });
        this.cols.map((item, index) => {
          if (item["field"] == "OBSERVERNAME") {
            var regexHash = new RegExp("<BR>", "g");
            if (
              data[item["field"]] &&
              data[item["field"]].search("<BR>") != -1
            ) {
              data[item["field"]] = data[item["field"]].replace(
                regexHash,
                "\n"
              );
            }
          }
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];
          if (index == 4 - enableSubarea) {
            this.fieldCols.map((fieldCol) => {
              dataJson[fieldCol] = data[fieldCol];
              tempDataJson[fieldCol] = data[fieldCol];
            });
          }
          if (this.tableData.length - 1 == key) {
            if (index == 5 - enableSubarea) {
              this.fieldCols.map((fieldCol) => {
                this.columns.push(fieldCol);
                this.unFColumns.push(fieldCol);
              });
            }
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
            if (this.cols.length - 1 == index) {
              this.colId = String.fromCharCode(
                97 + this.columns.length - 1
              ).toUpperCase();
            }
          }
        });
        dataJson["SITENAME"] = data["SITENAME"];
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
      });

      // get data for PDF
      var mainColumns = [];
      this.translate.get(["LBLSITENAME"]).subscribe((resLabel) => {
        mainColumns.push(resLabel["LBLSITENAME"]);
      });
      var tempMainColums = Object.keys(this.mainHead[0]);
      var subColumns = this.columns;
      this.mainHead.map((head) => {
        var mainRows = [];
        var subRows = [];
        // main Table
        var tempMainRows = [];
        tempMainColums.map((col) => {
          tempMainRows.push(head[col]);
        });
        mainRows.push(tempMainRows);

        // sub Table
        this.tableData.map((data) => {
          var tempSubRows = [];
          if (data["SITENAME"] == head["SITENAME"]) {
            this.cols.map((subCol, key) => {
              tempSubRows.push(data[subCol.field]);
              if (key == 4 - enableSubarea) {
                this.fieldCols.map((fieldCol) => {
                  tempSubRows.push(data[fieldCol]);
                });
              }
            });
            subRows.push(tempSubRows);
          }
        });
        this.pdfData.push({
          mainColumns: mainColumns,
          mainRows: mainRows,
          subColumns: subColumns,
          subRows: subRows,
        });
      });
    }
    if (this.siteList.length > 0) {
      this.siteList.map((data, key) => {
        var siteDataJson = {};
        var siteTempRows = [];
        this.siteCols.map((item, index) => {
          siteDataJson[item["header"]] = data[item["field"]];
          siteTempRows.push(data[item["field"]]);
          if (this.siteList.length - 1 == key) {
            this.siteColumns.push(item.header);
          }
        });
        this.siteTableRows.push(siteDataJson);
        this.sitePDFRows.push(siteTempRows);
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    const fileName = "CHECKLISTBYMETRICS";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      this.exportAsExcelFile(this.unFTableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const checklistHeader = this.columns;
      const data = this.tableRows;
      const siteHeader = this.siteColumns;
      const siteData = this.siteTableRows;

      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      // Report scope sheet
      let firstWorksheet = workbook.addWorksheet("Report Scope");
      firstWorksheet.addRow([]);
      let firstHeaderRow = firstWorksheet.addRow([this.title]);

      firstWorksheet.getCell("A2").font = {
        name: "Arial Unicode MS",
        family: 4,
        size: 18,
        bold: true,
        color: { argb: "00000000" },
      };
      firstWorksheet.getCell("A2").alignment = {
        vertical: "middle",
        horizontal: "left",
        indent: 1,
      };
      firstWorksheet.getColumn("A").width = 100;
      firstWorksheet.getRow(2).height = 30;

      this.translate
        .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        .subscribe((resLabel) => {
          if (this.parameters["OBSDATE"]) {
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([resLabel["LBLOBSDATE"]]).eachCell((cell) => {
              cell.font = {
                name: "Arial Unicode MS",
                family: 4,
                size: 14,
                bold: true,
                color: { argb: "00000000" },
              };
              cell.alignment = {
                vertical: "middle",
                horizontal: "left",
                indent: 2,
              };
            });

            firstWorksheet.lastRow.height = 25;
            var bothDate = this.parameters["OBSDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            firstWorksheet
              .addRow([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ])
              .eachCell((cell) => {
                cell.font = {
                  name: "Arial Unicode MS",
                  family: 4,
                  size: 12,
                  color: { argb: "00000000" },
                };
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "left",
                  indent: 3,
                };
              });
          }

          firstWorksheet.lastRow.height = 25;
        });

      // Report View sheet
      let worksheet = workbook.addWorksheet("Sheet1");

      // if (this.selectedReportView == 22) {
      //Add Header Row
      let siteHeaderRow = worksheet.addRow(siteHeader);
      siteHeaderRow.height = 20;
      // Cell Style : Fill and Border
      siteHeaderRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: customColor },
        };
        cell.font = {
          color: { argb: customFontColor },
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
        if (number > 1) {
          cell.alignment = { vertical: "middle", horizontal: "center" };
        } else {
          cell.alignment = { vertical: "middle", horizontal: "left" };
        }
      });
      // Add Data and Conditional Formatting
      let sumOfData = 0;
      siteData.forEach((element, index) => {
        let eachRow = [];
        this.siteColumns.map((headers, key) => {
          if (key == 1) {
            sumOfData = sumOfData + element[headers];
          }
          eachRow.push(element[headers]);
        });
        if (element.isDeleted === "Y") {
          let deletedRow = worksheet.addRow(eachRow);
          deletedRow.eachCell((cell, number) => {
            cell.font = {
              name: "Calibri",
              family: 4,
              size: 11,
              bold: false,
              strike: true,
            };
          });
        } else {
          if (siteData.length - 1 == index) {
            worksheet.addRow(eachRow).eachCell((cell, number) => {
              if (number > 1) {
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              }
              cell.border = {
                bottom: { style: "medium", color: { argb: customColor } },
              };
            });
          } else {
            worksheet.addRow(eachRow).eachCell((cell, number) => {
              if (number > 1) {
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              }
            });
          }
        }
      });

      if (this.selectedExport == 4 || this.selectedExport == 5) {
        for (var i = 1; i <= 12; i++) {
          worksheet.getColumn(i).width = 30;
        }

        let headerRow = worksheet.addRow(unFColumns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          if (number == 2) {
            cell.alignment = {
              vertical: "middle",
              horizontal: "center",
            };
          }
        });

        // Add Data and Conditional Formatting
        unFTableRows.map((element, index) => {
          let eachRow = [];
          unFColumns.map((headers, key) => {
            eachRow.push(element[headers]);
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            if (unFTableRows.length - 1 == index) {
              worksheet
                .addRow(eachRow)
                .eachCell({ includeEmpty: true }, (cell, number) => {
                  cell.border = {
                    bottom: { style: "medium", color: { argb: customColor } },
                  };
                  if (number == 2) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                });
            } else {
              worksheet
                .addRow(eachRow)
                .eachCell({ includeEmpty: true }, (cell, number) => {
                  if (number == 2) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                });
            }
          }
        });
      } else {
        // 2nd Table
        var mainHeader = Object.keys(this.rowGroupMetadata);
        var beforeDataCount = siteData.length + 3;
        mainHeader.map((header, key) => {
          var rowsData = data.filter((item) => {
            return item[this.dataKey] == header;
          });
          for (var i = 1; i <= 12; i++) {
            worksheet.getColumn(i).width = 30;
          }
          if (key == 0) {
            worksheet.mergeCells(
              "A" + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              "A" +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                (beforeDataCount + 1)
            );
            this.translate.get("LBLSITENAME").subscribe((label) => {
              worksheet.getCell("A" + beforeDataCount).value = label;
            });
            this.translate.get(header).subscribe((label) => {
              worksheet.getCell("A" + (beforeDataCount + 1)).value = label;
            });
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + beforeDataCount + 4;
          } else {
            worksheet.mergeCells(
              "A" + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              "A" +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                (beforeDataCount + 1)
            );
            this.translate.get("LBLSITENAME").subscribe((label) => {
              worksheet.getCell("A" + beforeDataCount).value = label;
            });
            this.translate.get(header).subscribe((label) => {
              worksheet.getCell("A" + (beforeDataCount + 1)).value = label;
            });
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 4 + beforeDataCount;
          }

          let headerRow = worksheet.addRow(checklistHeader);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: "EEECE1" },
            };
            cell.font = {
              color: { argb: "000000" },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
            if (number == 1) {
              cell.alignment = {
                vertical: "middle",
                horizontal: "center",
              };
            }
          });

          // Add Data and Conditional Formatting
          rowsData.map((element, index) => {
            let eachRow = [];
            this.columns.map((headers, key) => {
              eachRow.push(element[headers]);
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (rowsData.length - 1 == index) {
                worksheet
                  .addRow(eachRow)
                  .eachCell({ includeEmpty: true }, (cell, number) => {
                    cell.border = {
                      bottom: { style: "medium", color: { argb: customColor } },
                    };
                    if (number == 1) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                      };
                    }
                  });
              } else {
                worksheet
                  .addRow(eachRow)
                  .eachCell({ includeEmpty: true }, (cell, number) => {
                    if (number == 1) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                      };
                    }
                  });
              }
            }
          });
        });
      }

      worksheet.addRow([]);
      //Generate Excel File with given name
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape", "mm", [1030, 800]);
      // report scope
      pdf.text(this.title, 15, 20);
      let finalX = 10;
      this.translate
        .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        .subscribe((resLabel) => {
          if (this.parameters["OBSDATE"]) {
            let obsDate = [];
            var bothDate = this.parameters["OBSDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            obsDate.push([
              fDate +
                " - " +
                tDate +
                " (" +
                this.auth.UserInfo["dateFormat"].toUpperCase() +
                ")",
            ]);
            pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
              startY: finalX + 13,
              headStyles: {
                fillColor: customColor,
                textColor: customFontColor,
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
            });
            finalX = pdf.previousAutoTable.finalX;
          }
          pdf.addPage();
        });

      // report view
      let finalY = 10;
      var fn = this;
      pdf.autoTable(this.siteColumns, this.sitePDFRows, {
        startY: finalY,
        headStyles: {
          fillColor: customColor,
          textColor: customFontColor,
        },
        alternateRowStyles: {
          fillColor: "#FFFFFF",
        },
        didParseCell: (data) => {
          fn.alignCol(data, 1);
        },
      });
      finalY = pdf.previousAutoTable.finalY + 10;
      this.pdfData.map((data) => {
        pdf.autoTable(data.mainColumns, data.mainRows, {
          startY: finalY,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
        });
        finalY = pdf.previousAutoTable.finalY;
        pdf.autoTable(data.subColumns, data.subRows, {
          startY: finalY,
          headStyles: {
            fillColor: "#D8D8D8",
            textColor: "#000000",
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          columnStyles: {
            0: { cellWidth: 20 },
            1: { cellWidth: 20 },
            2: { cellWidth: 20 },
            3: { cellWidth: 20 },
            4: { cellWidth: 20 },
            5: { cellWidth: 20 },
            6: { cellWidth: 20 },
            7: { cellWidth: 20 },
            8: { cellWidth: 20 },
            9: { cellWidth: 20 },
            10: { cellWidth: 20 },
          },
          didParseCell: (data) => {
            fn.alignCol(data, 0);
          },
        });
        finalY = pdf.previousAutoTable.finalY + 10;
      });
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }

  alignCol = function (data, key) {
    if (data.column.index > 0 && key == 1) {
      data.cell.styles.halign = "center";
    }
    if (data.column.index == 0 && key == 0) {
      data.cell.styles.halign = "center";
    }
  };
}
