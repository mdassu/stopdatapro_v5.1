import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { formatDate } from "@angular/common";
import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";

import html2canvas from "html2canvas";

import { TranslateService } from "@ngx-translate/core";
import { isArray } from "util";
import { IfStmt } from "@angular/compiler";
import { AuthenticationService } from "src/app/_services/authentication.service";

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "src/assets/pdfmake/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: "app-target-vs-activity-weekly",
  templateUrl: "./target-vs-activity-weekly.component.html",
  styleUrls: ["./target-vs-activity-weekly.component.css"],
})
export class TargetVsActivityWeeklyComponent implements OnInit {
  @Input() reportId: any;
  cols: any[];
  tableData: any = [];
  reportView: any = [];
  title: any;
  selectedReportView: any = 40;
  isLoaded: boolean;
  errormsg: boolean;
  rowGroupMetadata: any;
  tableCols: any;
  colspnlength: any;
  parameters: any;
  allSiteSelected: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any = 0;
  tableRows: any;
  columns: any;
  unFTableRows: any;
  unFColumns: any;
  pdfData: any = [];
  targetPdfData: any = [];
  dataKey: any = "SITENAME";
  colId: any;
  mainHead: any;
  pdfLanguageDefaultStyle: any = {};

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    var langCode = this.auth.UserInfo["languageCode"];
    if (langCode == "ja-jp") {
      pdfMake.fonts = {
        Sawarabi: {
          bold: "Sawarabi_Mincho.ttf",
          normal: "Sawarabi_Mincho.ttf",
        },
      };
      this.pdfLanguageDefaultStyle = {
        font: "Sawarabi",
      };
    } else if (langCode == "ko") {
      pdfMake.fonts = {
        GothicA1: {
          bold: "GothicA1-Regular.ttf",
          normal: "GothicA1-Regular.ttf",
        },
      };
      this.pdfLanguageDefaultStyle = {
        font: "GothicA1",
      };
    } else if (langCode == "th-th") {
      pdfMake.fonts = {
        Trirong: {
          bold: "Trirong-Regular.ttf",
          normal: "Trirong-Regular.ttf",
        },
      };
      this.pdfLanguageDefaultStyle = {
        font: "Trirong",
      };
    } else if (langCode == "zh-cn") {
      pdfMake.fonts = {
        NotoSansSC: {
          bold: "NotoSansSC-Regular.ttf",
          normal: "NotoSansSC-Regular.ttf",
        },
      };
      this.pdfLanguageDefaultStyle = {
        font: "NotoSansSC",
      };
    }
    this.translate.get("LBLTARGETVSACTIVITYWEEKLYRPT").subscribe((title) => {
      this.title = title;
    });
    this.translate
      .get(["LBLREPORTMGMT", "LBLREPORTS", "LBLREPORTFILTERS"])
      .subscribe((res) => {
        this.breadcrumbService.setItems([
          {
            label: res["LBLREPORTMGMT"],
            url: "./assets/help/target-activity-weekly.md",
          },
          { label: res["LBLREPORTS"], routerLink: ["/reports"] },
          {
            label: res["LBLREPORTFILTERS"],
            routerLink: ["/report-filters/" + this.reportId],
          },
        ]);
      });
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.allSiteSelected = this.parameters["SITEID"];
      this.selectedReportView = this.parameters["designId"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }
    this.getReports();

    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        if (this.selectedReportView == 40) {
          this.exportList = [
            { label: resLabel["LBLNONE"], value: 0 },
            { label: resLabel["LBLPDF"], value: 2 },
            // { label: resLabel["LBLUFXLS"], value: 4 },
            { label: resLabel["LBLUXLSX"], value: 5 },
            { label: resLabel["LBLUFCSV"], value: 6 },
          ];
        } else {
          this.exportList = [
            { label: resLabel["LBLNONE"], value: 0 },
            { label: resLabel["LBLXLSX"], value: 1 },
            // { label: resLabel["LBLXLS"], value: 3 },
            { label: resLabel["LBLPDF"], value: 2 },
            // { label: resLabel["LBLUFXLS"], value: 4 },
            { label: resLabel["LBLUXLSX"], value: 5 },
            { label: resLabel["LBLUFCSV"], value: 6 },
          ];
        }
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (isArray(res["designData"])) {
        this.reportView = [];
        var designData = res["designData"];
        designData.map((item) => {
          this.translate.get(item.RPTNAME).subscribe((label) => {
            this.reportView.push({
              label: label,
              value: item.DESIGNID,
            });
          });
        });
      }
      if (res["Data"] != null) {
        this.tableData = res["Data"];
      } else {
        this.tableData = [];
        this.errormsg = true;
      }
      if (this.tableData.length > 0) {
        var cols = Object.keys(this.tableData[0]);
        this.translate.get("LBLOBSERVERNAME").subscribe((label) => {
          this.tableCols = [label];
        });
        var tempCols = cols.filter(function (item) {
          return item.substring(6, 9) == "TGT";
        });

        tempCols.map((data) => {
          var newLabel = data.replace("TGT", "").split(" ");
          this.translate.get(newLabel[0]).subscribe((label) => {
            this.tableCols.push(label + " " + newLabel[1]);
          });
        });

        this.translate
          .get([
            "LBLOBSERVERNAME",
            "LBLTARGET",
            "LBLCOMPLETED",
            "LBLCOMPLETEDTARGETPERCENTAGE",
            "LBLSITENAME",
          ])
          .subscribe((resLabel) => {
            if (this.selectedReportView == 40) {
              this.cols = [
                { field: "SITENAME", header: resLabel["LBLSITENAME"] },
                { field: "OBSERVERNAME", header: resLabel["LBLOBSERVERNAME"] },
              ];
              Object.keys(this.tableData[0]).map((label) => {
                if (
                  label != "SITEID" &&
                  label != "SITENAME" &&
                  label != "OBSERVERID" &&
                  label != "OBSERVERNAME" &&
                  label != "TGTTOTAL" &&
                  label != "OBSTOTAL" &&
                  label != "TOTPER" &&
                  label != "SITETGTTOTAL" &&
                  label != "SITEOBSTOTAL" &&
                  label != "SITETOTPER"
                ) {
                  this.cols.push({ field: label, header: label });
                }
              });
            } else {
              this.cols = [
                { field: "OBSERVERNAME", header: resLabel["LBLOBSERVERNAME"] },
                { field: "TGTTOTAL", header: resLabel["LBLTARGET"] },
                { field: "OBSTOTAL", header: resLabel["LBLCOMPLETED"] },
                {
                  field: "TOTPER",
                  header: resLabel["LBLCOMPLETEDTARGETPERCENTAGE"],
                },
              ];
            }
          });

        this.colspnlength = this.tableCols.length;
      }
      this.updateRowGroupMetaData();
      this.manageExportData();
      this.isLoaded = true;
    });
  }

  changeExort() {
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        if (this.selectedReportView == 40) {
          this.exportList = [
            { label: resLabel["LBLNONE"], value: 0 },
            { label: resLabel["LBLPDF"], value: 2 },
            { label: resLabel["LBLUFXLS"], value: 4 },
            { label: resLabel["LBLUXLSX"], value: 5 },
            { label: resLabel["LBLUFCSV"], value: 6 },
          ];
        } else {
          this.exportList = [
            { label: resLabel["LBLNONE"], value: 0 },
            { label: resLabel["LBLXLSX"], value: 1 },
            { label: resLabel["LBLXLS"], value: 3 },
            { label: resLabel["LBLPDF"], value: 2 },
            { label: resLabel["LBLUFXLS"], value: 4 },
            { label: resLabel["LBLUXLSX"], value: 5 },
            { label: resLabel["LBLUFCSV"], value: 6 },
          ];
        }
        this.selectedExport = 0;
      });
    this.parameters["designId"] = this.selectedReportView;
    this.getReports();
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    this.mainHead = [];
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let tableData = this.tableData[i];
        if (this.parameters["OBSZEROCHECKLISTVALUE"]) {
          if (tableData.OBSTOTAL == 0) {
            this.tableData.splice(i, 1);
          }
        }
        let sitename = tableData.SITENAME;
        if (i == 0) {
          this.rowGroupMetadata[sitename] = { index: 0, size: 1 };
          this.mainHead.push({
            SITENAME: sitename,
            SITETGTTOTAL: tableData.SITETGTTOTAL,
            SITEOBSTOTAL: tableData.SITEOBSTOTAL,
            SITETOTPER: tableData.SITETOTPER,
          });
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.SITENAME;
          if (sitename === previousRowGroup) {
            this.rowGroupMetadata[sitename].size++;
          } else {
            this.rowGroupMetadata[sitename] = { index: i, size: 1 };
            this.mainHead.push({
              SITENAME: sitename,
              SITETGTTOTAL: tableData.SITETGTTOTAL,
              SITEOBSTOTAL: tableData.SITEOBSTOTAL,
              SITETOTPER: tableData.SITETOTPER,
            });
          }
        }
      }
    }
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.targetPdfData = [];
    this.columns = [];
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempDataJson = {};
        this.translate
          .get([
            "LBLSITENAME",
            "LBLTOTAL",
            "LBLTARGET",
            "LBLCOMPLETED",
            "LBLCOMPLETEDTARGETPERCENTAGE",
          ])
          .subscribe((resLabel) => {
            if (key == 0) {
              this.unFColumns.push(resLabel["LBLSITENAME"]);
              this.unFColumns.push(
                resLabel["LBLTOTAL"] + " " + resLabel["LBLTARGET"]
              );
              this.unFColumns.push(
                resLabel["LBLTOTAL"] + " " + resLabel["LBLCOMPLETED"]
              );
              this.unFColumns.push(
                resLabel["LBLTOTAL"] +
                  " " +
                  resLabel["LBLCOMPLETEDTARGETPERCENTAGE"]
              );
            }
            tempDataJson[resLabel["LBLSITENAME"]] = data["SITENAME"];
            tempDataJson[resLabel["LBLTOTAL"] + " " + resLabel["LBLTARGET"]] =
              data["SITETGTTOTAL"];
            tempDataJson[
              resLabel["LBLTOTAL"] + " " + resLabel["LBLCOMPLETED"]
            ] = data["SITEOBSTOTAL"];
            tempDataJson[
              resLabel["LBLTOTAL"] +
                " " +
                resLabel["LBLCOMPLETEDTARGETPERCENTAGE"]
            ] = data["SITETOTPER"];
          });
        this.cols.map((item, index) => {
          if (item["field"] == "OBSERVERNAME") {
            var regexHash = new RegExp("<BR>", "g");
            if (
              data[item["field"]] &&
              data[item["field"]].search("<BR>") != -1
            ) {
              data[item["field"]] = data[item["field"]].replace(
                regexHash,
                "\n"
              );
            }
          }
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];

          if (this.tableData.length - 1 == key) {
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
          }
        });
        if (this.selectedReportView != 40) {
          dataJson["SITENAME"] = data["SITENAME"];
          dataJson["SITETGTTOTAL"] = data["SITETGTTOTAL"];
          dataJson["SITEOBSTOTAL"] = data["SITEOBSTOTAL"];
          dataJson["SITETOTPER"] = data["SITETOTPER"];
        }
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
      });

      // get data for PDF
      var mainColumns = [];
      var targetMainColumns = [];
      this.translate
        .get([
          "LBLSITENAME",
          "LBLTARGET",
          "LBLCOMPLETED",
          "LBLCOMPLETEDTARGETPERCENTAGE",
        ])
        .subscribe((resLabel) => {
          mainColumns.push(resLabel["LBLSITENAME"]);
          mainColumns.push(resLabel["LBLTARGET"]);
          mainColumns.push(resLabel["LBLCOMPLETED"]);
          mainColumns.push(resLabel["LBLCOMPLETEDTARGETPERCENTAGE"]);
          targetMainColumns.push(resLabel["LBLSITENAME"]);
        });
      var tempMainColums = Object.keys(this.mainHead[0]);
      var subColumns = this.columns;
      this.mainHead.map((head) => {
        var mainRows = [];
        var targetMainRows = [];
        var subRows = [];
        var targetSubRows = [];
        // main Table
        var tempMainRows = [];
        var tempTargetMainRows = [];
        tempMainColums.map((col) => {
          tempMainRows.push(head[col]);
        });
        mainRows.push(tempMainRows);
        tempTargetMainRows.push(head["SITENAME"]);
        targetMainRows.push(tempTargetMainRows);
        // sub Table
        this.tableData.map((data) => {
          var tempSubRows = [];
          var tempTargetSubRows = [];
          if (data["SITENAME"] == head["SITENAME"]) {
            this.cols.map((subCol) => {
              tempSubRows.push(data[subCol.field]);
            });
            this.tableCols.map((subCol) => {
              if (data[subCol] != -1) {
                this.translate.get("LBLOBSERVERNAME").subscribe((label) => {
                  if (subCol == label) {
                    tempTargetSubRows.push(data["OBSERVERNAME"]);
                  } else {
                    var monthData = {
                      TGT: data[subCol + "TGT"],
                      OBS: data[subCol],
                      PER: data[subCol + "PER"],
                    };
                    tempTargetSubRows.push(monthData);
                    // tempTargetSubRows.push(
                    //   `${data[subCol + "TGT"]}           ${data[subCol]}\n   ${
                    //     data[subCol + "PER"]
                    //   }%`
                    // );
                  }
                });
              } else {
                tempTargetSubRows.push("");
              }
            });
            targetSubRows.push(tempTargetSubRows);
            subRows.push(tempSubRows);
          }
        });
        this.pdfData.push({
          mainColumns: mainColumns,
          mainRows: mainRows,
          subColumns: subColumns,
          subRows: subRows,
        });
        this.targetPdfData.push({
          mainColumns: targetMainColumns,
          mainRows: targetMainRows,
          subColumns: this.tableCols,
          subRows: targetSubRows,
        });
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    const fileName = "TARGETWEEKLY";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      this.exportAsExcelFile(this.unFTableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;
      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      // Report scope sheet
      let firstWorksheet = workbook.addWorksheet("Report Scope");
      firstWorksheet.addRow([]);
      let firstHeaderRow = firstWorksheet.addRow([this.title]);

      firstWorksheet.getCell("A2").font = {
        name: "Arial Unicode MS",
        family: 4,
        size: 18,
        bold: true,
        color: { argb: "00000000" },
      };
      firstWorksheet.getCell("A2").alignment = {
        vertical: "middle",
        horizontal: "left",
        indent: 1,
      };
      firstWorksheet.getColumn("A").width = 100;
      firstWorksheet.getRow(2).height = 30;

      this.translate
        .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        .subscribe((resLabel) => {
          if (this.parameters["OBSDATE"]) {
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([resLabel["LBLOBSDATE"]]).eachCell((cell) => {
              cell.font = {
                name: "Arial Unicode MS",
                family: 4,
                size: 14,
                bold: true,
                color: { argb: "00000000" },
              };
              cell.alignment = {
                vertical: "middle",
                horizontal: "left",
                indent: 2,
              };
            });

            firstWorksheet.lastRow.height = 25;
            var bothDate = this.parameters["OBSDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            firstWorksheet
              .addRow([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ])
              .eachCell((cell) => {
                cell.font = {
                  name: "Arial Unicode MS",
                  family: 4,
                  size: 12,
                  color: { argb: "00000000" },
                };
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "left",
                  indent: 3,
                };
              });
          }

          firstWorksheet.lastRow.height = 25;
        });

      // Report View sheet
      let worksheet = workbook.addWorksheet("Sheet1");
      if (this.selectedReportView == 40) {
        for (var i = 1; i <= 15; i++) {
          worksheet.getColumn(i).width = 25;
        }
        //Add Header Row
        let headerRow = worksheet.addRow(header);
        headerRow.height = 20;
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          if (number == 2) {
            cell.alignment = { vertical: "middle", horizontal: "center" };
          } else {
            cell.alignment = { vertical: "middle", horizontal: "left" };
          }
        });
        // Add Data and Conditional Formatting
        data.forEach((element) => {
          let eachRow = [];
          this.columns.map((headers, key) => {
            eachRow.push(element[headers]);
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            var addRow = worksheet.addRow(eachRow);
            addRow.eachCell((cell) => {
              cell.numFmt = this.auth.changeCellValueType(cell.value);
            });
            addRow.getCell(2).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
          }
        });
      } else {
        if (this.selectedExport == 4 || this.selectedExport == 5) {
          for (var i = 1; i <= 15; i++) {
            worksheet.getColumn(i).width = 25;
          }

          let headerRow = worksheet.addRow(unFColumns);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            cell.font = {
              color: { argb: customFontColor },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
          });

          // Add Data and Conditional Formatting
          unFTableRows.map((element, index) => {
            let eachRow = [];
            unFColumns.map((headers, key) => {
              eachRow.push(element[headers]);
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (unFTableRows.length - 1 == index) {
                worksheet
                  .addRow(eachRow)
                  .eachCell({ includeEmpty: true }, (cell) => {
                    cell.border = {
                      bottom: { style: "medium", color: { argb: customColor } },
                    };
                    cell.alignment = {
                      horizontal: "left",
                    };
                    cell.numFmt = this.auth.changeCellValueType(cell.value);
                  });
              } else {
                worksheet.addRow(eachRow).eachCell((cell) => {
                  cell.alignment = {
                    horizontal: "left",
                  };
                  cell.numFmt = this.auth.changeCellValueType(cell.value);
                });
              }
            }
          });
        } else {
          let subHeader = header;
          var beforeDataCount = 1;
          this.mainHead.map((header, key) => {
            var rowsData = data.filter((item) => {
              return item[this.dataKey] == header[this.dataKey];
            });
            for (var i = 1; i <= 15; i++) {
              worksheet.getColumn(i).width = 25;
            }
            if (key == 0) {
              // worksheet.mergeCells("A" + beforeDataCount + ":D" + beforeDataCount);
              // worksheet.mergeCells(
              //   "A" + (beforeDataCount + 1) + ":D" + (beforeDataCount + 1)
              // );
              this.translate
                .get([
                  "LBLSITENAME",
                  "LBLTARGET",
                  "LBLCOMPLETED",
                  "LBLCOMPLETEDTARGETPERCENTAGE",
                ])
                .subscribe((resLabel) => {
                  worksheet.getCell("A" + beforeDataCount).value =
                    resLabel["LBLSITENAME"];
                  worksheet.getCell("B" + beforeDataCount).value =
                    resLabel["LBLTARGET"];
                  worksheet.getCell("C" + beforeDataCount).value =
                    resLabel["LBLCOMPLETED"];
                  worksheet.getCell("D" + beforeDataCount).value =
                    resLabel["LBLCOMPLETEDTARGETPERCENTAGE"];
                });
              worksheet.getCell("A" + (beforeDataCount + 1)).value =
                header["SITENAME"];
              worksheet.getCell("B" + (beforeDataCount + 1)).value =
                header["SITETGTTOTAL"];
              worksheet.getCell("C" + (beforeDataCount + 1)).value =
                header["SITEOBSTOTAL"];
              worksheet.getCell("D" + (beforeDataCount + 1)).value =
                header["SITETOTPER"];
              worksheet.getCell("A" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("B" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("C" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("D" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("A" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              worksheet.getCell("B" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              worksheet.getCell("C" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              worksheet.getCell("D" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              beforeDataCount = rowsData.length + 5;
            } else {
              // worksheet.mergeCells("A" + beforeDataCount + ":D" + beforeDataCount);
              // worksheet.mergeCells(
              //   "A" + (beforeDataCount + 1) + ":D" + (beforeDataCount + 1)
              // );
              this.translate
                .get([
                  "LBLSITENAME",
                  "LBLTARGET",
                  "LBLCOMPLETED",
                  "LBLCOMPLETEDTARGETPERCENTAGE",
                ])
                .subscribe((resLabel) => {
                  worksheet.getCell("A" + beforeDataCount).value =
                    resLabel["LBLSITENAME"];
                  worksheet.getCell("B" + beforeDataCount).value =
                    resLabel["LBLTARGET"];
                  worksheet.getCell("C" + beforeDataCount).value =
                    resLabel["LBLCOMPLETED"];
                  worksheet.getCell("D" + beforeDataCount).value =
                    resLabel["LBLCOMPLETEDTARGETPERCENTAGE"];
                });
              worksheet.getCell("A" + (beforeDataCount + 1)).value =
                header["SITENAME"];
              worksheet.getCell("B" + (beforeDataCount + 1)).value =
                header["SITETGTTOTAL"];
              worksheet.getCell("C" + (beforeDataCount + 1)).value =
                header["SITEOBSTOTAL"];
              worksheet.getCell("D" + (beforeDataCount + 1)).value =
                header["SITETOTPER"];

              worksheet.getCell("A" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("B" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("C" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("D" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("A" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              worksheet.getCell("B" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              worksheet.getCell("C" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              beforeDataCount = rowsData.length + 4 + beforeDataCount;
            }

            let headerRow = worksheet.addRow(subHeader);
            // Cell Style : Fill and Border
            headerRow.eachCell((cell, number) => {
              cell.fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: "EEECE1" },
              };
              cell.font = {
                color: { argb: "000000" },
              };
              cell.border = {
                top: { style: "thin" },
                left: { style: "thin" },
                bottom: { style: "thin" },
                right: { style: "thin" },
              };
            });

            // Add Data and Conditional Formatting
            rowsData.map((element, index) => {
              let eachRow = [];
              this.columns.map((headers, key) => {
                eachRow.push(element[headers]);
              });
              if (element.isDeleted === "Y") {
                let deletedRow = worksheet.addRow(eachRow);
                deletedRow.eachCell((cell, number) => {
                  cell.font = {
                    name: "Calibri",
                    family: 4,
                    size: 11,
                    bold: false,
                    strike: true,
                  };
                });
              } else {
                if (rowsData.length - 1 == index) {
                  worksheet.addRow(eachRow).eachCell((cell) => {
                    cell.border = {
                      bottom: { style: "medium", color: { argb: customColor } },
                    };
                  });
                } else {
                  worksheet.addRow(eachRow);
                }
              }
            });
          });
        }
      }

      worksheet.addRow([]);
      //Generate Excel File with given name
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      if (this.selectedReportView == 40) {
        // var pdf = new jsPDF("landscape", "mm", [1200, 800]);
        // // report scope
        // pdf.text(this.title, 15, 20);
        // let finalX = 10;
        // this.translate
        //   .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        //   .subscribe((resLabel) => {
        //     if (this.parameters["OBSDATE"]) {
        //       let obsDate = [];
        //       var bothDate = this.parameters["OBSDATE"].split("-");
        //       var fDate = formatDate(
        //         bothDate[0],
        //         this.auth.UserInfo["dateFormat"],
        //         this.translate.getDefaultLang()
        //       );
        //       var tDate = formatDate(
        //         bothDate[1],
        //         this.auth.UserInfo["dateFormat"],
        //         this.translate.getDefaultLang()
        //       );
        //       obsDate.push([
        //         fDate +
        //           " - " +
        //           tDate +
        //           " (" +
        //           this.auth.UserInfo["dateFormat"].toUpperCase() +
        //           ")",
        //       ]);
        //       pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
        //         startY: finalX + 13,
        //         headStyles: {
        //           fillColor: customColor,
        //           textColor: customFontColor,
        //         },
        //         alternateRowStyles: {
        //           fillColor: "#FFFFFF",
        //         },
        //       });
        //       finalX = pdf.previousAutoTable.finalX;
        //     }
        //     pdf.addPage();
        //   });

        // // report view
        // let finalY = 10;
        // pdf.setFillColor(255, 255, 255);
        // pdf.circle(170, Math.round(finalY), 5, "FD");
        // pdf.text("Not Applicable", 180, Math.round(finalY) + 2);
        // pdf.setFillColor(255, 0, 0);
        // pdf.circle(230, Math.round(finalY), 5, "FD");
        // pdf.text("Below Target", 240, Math.round(finalY) + 2);
        // pdf.setFillColor(255, 255, 53);
        // pdf.circle(290, Math.round(finalY), 5, "FD");
        // pdf.text("Achieved Target", 300, Math.round(finalY) + 2);
        // pdf.setFillColor(122, 158, 92);
        // pdf.circle(350, Math.round(finalY), 5, "FD");
        // pdf.text("Exceeded Target", 360, Math.round(finalY) + 2);
        // // pdf.setFillColor(255, 255, 255);
        // // pdf.circle(20, Math.round(finalY), 5, "FD");
        // // pdf.text("Not Applicable", 30, Math.round(finalY) + 2);
        // // pdf.setFillColor(255, 0, 0);
        // // pdf.circle(80, Math.round(finalY), 5, "FD");
        // // pdf.text("Below Target", 90, Math.round(finalY) + 2);
        // // pdf.setFillColor(255, 255, 53);
        // // pdf.circle(140, Math.round(finalY), 5, "FD");
        // // pdf.text("Achieved Target", 150, Math.round(finalY) + 2);
        // // pdf.setFillColor(122, 158, 92);
        // // pdf.circle(210, Math.round(finalY), 5, "FD");
        // // pdf.text("Exceeded Target", 220, Math.round(finalY) + 2);
        // finalY = 20;
        // this.targetPdfData.map((data) => {
        //   pdf.autoTable(data.mainColumns, data.mainRows, {
        //     startY: finalY,
        //     headStyles: {
        //       fillColor: customColor,
        //       textColor: customFontColor,
        //     },
        //     alternateRowStyles: {
        //       fillColor: "#FFFFFF",
        //     },
        //   });
        //   finalY = pdf.previousAutoTable.finalY;
        //   pdf.autoTable(data.subColumns, data.subRows, {
        //     startY: finalY,
        //     headStyles: {
        //       fillColor: "#D8D8D8",
        //       textColor: "#000000",
        //     },
        //     alternateRowStyles: {
        //       fillColor: "#FFFFFF",
        //     },
        //     styles: { halign: "center" },
        //     columnStyles: {
        //       0: { cellWidth: 20 },
        //     },
        //     didParseCell: function (cellData) {
        //       var letters = /^[0-9]+$/;
        //       if (
        //         cellData.cell.text[0] &&
        //         cellData.cell.text[0].replace(/ /g, "").match(letters)
        //       ) {
        //         var splitData = cellData.cell.text[0].split(" ");
        //         var target = splitData[0];
        //         var observer = splitData[splitData.length - 1];
        //         if (target > observer) {
        //           //Bellow target
        //           cellData.cell.styles.fillColor = "#FF0000";
        //           cellData.cell.styles.textColor = "#000000";
        //           cellData.cell.styles.fontStyle = "bold";
        //           cellData.cell.styles.lineWidth = 0.3;
        //           cellData.cell.styles.lineColor = "#000000";
        //         } else if (target == observer && observer != -1) {
        //           //Archived target
        //           cellData.cell.styles.fillColor = "#FFE135";
        //           cellData.cell.styles.textColor = "#000000";
        //           cellData.cell.styles.fontStyle = "bold";
        //           cellData.cell.styles.lineWidth = 0.3;
        //           cellData.cell.styles.lineColor = "#000000";
        //         } else if (target < observer) {
        //           //Exceeded target
        //           cellData.cell.styles.fillColor = "#7a9e5c";
        //           cellData.cell.styles.textColor = "#000000";
        //           cellData.cell.styles.fontStyle = "bold";
        //           cellData.cell.styles.lineWidth = 0.3;
        //           cellData.cell.styles.lineColor = "#000000";
        //         }
        //       } else if (cellData.cell.section == "body") {
        //         cellData.cell.styles.halign = "left";
        //       } else if (cellData.cell.text[0] == "Observer Name") {
        //         cellData.cell.styles.halign = "left";
        //       }
        //     },
        //   });
        //   finalY = pdf.previousAutoTable.finalY + 10;
        // });
        // pdf.save(fileName + "_" + new Date().getTime() + ".pdf");

        var contentArr = [];
        var legends = {};
        this.translate
          .get(["LBLNOTAPPLICABLE", "LBLBELOWPAR", "LBLATPAR", "LBLABOVEPAR"])
          .subscribe((resLabel) => {
            legends["notApplicable"] = resLabel["LBLNOTAPPLICABLE"];
            legends["belowTarget"] = resLabel["LBLBELOWPAR"];
            legends["archivedTarget"] = resLabel["LBLATPAR"];
            legends["exceededTarget"] = resLabel["LBLABOVEPAR"];
          });
        this.targetPdfData.map((item, key) => {
          // Head table for site group
          var headTableWidths = [];
          var headTableBodyCol = [];
          var headTableBodyRow = [];
          item.mainColumns.map((headCol) => {
            headTableWidths.push("*");
            headTableBodyCol.push({
              fillColor: "#" + customColor,
              color: "#" + customFontColor,
              text: headCol,
              margin: [5, 0],
            });
          });
          item.mainRows.map((headRow) => {
            headTableBodyRow.push({
              text: headRow,
              margin: [5, 0],
            });
          });
          var headTable = {
            style: "tableExample",
            table: {
              widths: headTableWidths,
              body: [headTableBodyCol, headTableBodyRow],
            },
            layout: "noBorders",
            margin: [],
          };

          contentArr.push(headTable);

          // Sub table for Data
          var subTableWidths = [];
          var subTableBodyCol = [];
          var subTableBody = [];
          item.subColumns.map((subCol, subKey) => {
            if (subKey == 0) {
              subTableWidths.push(150);
              subTableBodyCol.push({
                text: "",
                border: [false, false, false, false],
              });
            } else {
              subTableWidths.push("*");
              subTableBodyCol.push({
                text: subCol,
                alignment: "center",
                border: [false, false, false, false],
              });
            }
          });
          subTableBody.push(subTableBodyCol);
          item.subRows.map((subRow, subKey) => {
            var subTableBodyRow = [];
            subRow.map((sRow, sKey) => {
              var data = [];
              if (sKey == 0) {
                data.push({
                  text: sRow,
                  margin: [0, 10],
                  border: [false, false, false, false],
                });
              } else {
                if (sRow == "") {
                  data.push({
                    text: "",
                    border: [false, false, false, false],
                  });
                } else {
                  data.push({
                    table: {
                      widths: ["*", "*"],
                      body: [
                        [
                          {
                            text: sRow.TGT,
                            alignment: "left",
                            border: [true, true, false, false],
                          },
                          {
                            text: sRow.OBS,
                            alignment: "right",
                            border: [false, true, true, false],
                          },
                        ],
                        [
                          {
                            text: sRow.PER + "%",
                            alignment: "center",
                            colSpan: 2,
                            border: [true, false, true, true],
                          },
                        ],
                      ],
                    },
                    fillColor: this.fillColor(sRow.TGT, sRow.OBS),
                  });
                }
              }
              subTableBodyRow.push(data);
            });
            subTableBody.push(subTableBodyRow);
          });

          var subTable = {
            style: "tableExample",
            table: {
              widths: subTableWidths,
              body: subTableBody,
            },
            layout: {
              defaultBorder: false,
              paddingLeft: function (i, node) {
                return 0;
              },
              paddingRight: function (i, node) {
                return 0;
              },
              paddingTop: function (i, node) {
                return 0;
              },
              paddingBottom: function (i, node) {
                return 0;
              },
            },
          };
          contentArr.push(subTable);
        });
        let obsDate = [];
        if (this.parameters["OBSDATE"]) {
          var bothDate = this.parameters["OBSDATE"].split("-");
          var fDate = formatDate(
            bothDate[0],
            this.auth.UserInfo["dateFormat"],
            this.translate.getDefaultLang()
          );
          var tDate = formatDate(
            bothDate[1],
            this.auth.UserInfo["dateFormat"],
            this.translate.getDefaultLang()
          );
          obsDate.push([
            fDate +
              " - " +
              tDate +
              " (" +
              this.auth.UserInfo["dateFormat"].toUpperCase() +
              ")",
          ]);
        }
        var obsKeyword;
        this.translate.get("LBLOBSDATE").subscribe((label) => {
          obsKeyword = label;
        });
        var docDefinition = {
          pageSize: { width: 1200, height: 800 },
          pageOrientation: "landscape",
          pageMargins: 30,
          footer: function (page) {
            if (page != 1) {
              return {
                columns: [
                  { width: "*", text: "" },
                  {
                    width: "auto",
                    style: "tableExample",
                    table: {
                      widths: [40, 100, 40, 100, 40, 100, 40, 100],
                      body: [
                        [
                          {
                            text: "",
                            fillColor: "#ffffff",
                            border: [true, true, true, true],
                          },
                          {
                            text: legends["notApplicable"],
                          },
                          {
                            text: "",
                            fillColor: "#FF0000",
                            border: [true, true, true, true],
                          },
                          {
                            text: legends["belowTarget"],
                          },
                          {
                            text: "",
                            fillColor: "#FFE135",
                            border: [true, true, true, true],
                          },
                          {
                            text: legends["archivedTarget"],
                          },
                          {
                            text: "",
                            fillColor: "#7a9e5c",
                            border: [true, true, true, true],
                          },
                          {
                            text: legends["exceededTarget"],
                          },
                        ],
                      ],
                    },
                    layout: {
                      defaultBorder: false,
                    },
                  },
                  { width: "*", text: "" },
                ],
              };
            }
          },
          content: [
            { text: this.title, fontSize: 18 },
            {
              style: "tableExample",
              table: {
                widths: ["*"],
                body: [
                  [
                    {
                      text: obsKeyword,
                      fillColor: "#" + customColor,
                      color: "#" + customFontColor,
                      margin: [5, 0],
                    },
                  ],
                  [
                    {
                      text: obsDate[0],
                      margin: [5, 0],
                    },
                  ],
                ],
              },
              layout: "noBorders",
              pageBreak: "after",
            },
            contentArr,
          ],
          styles: {
            header: {
              fontSize: 18,
              bold: true,
              margin: [0, 0, 0, 10],
            },
            subheader: {
              fontSize: 16,
              bold: true,
              margin: [0, 10, 0, 5],
            },
            tableExample: {
              margin: [0, 5, 0, 15],
            },
            innerTableMargin: {
              margin: [],
            },
            tableHeader: {
              bold: true,
              fontSize: 13,
              color: "black",
            },
          },
          defaultStyle: this.pdfLanguageDefaultStyle,
        };
        pdfMake
          .createPdf(docDefinition)
          .download(fileName + "_" + new Date().getTime());
      } else {
        var pdf = new jsPDF("landscape");
        // report scope
        pdf.text(this.title, 15, 20);
        let finalX = 10;
        this.translate
          .get(["LBLOBSDATE", "LBLCREATEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              let obsDate = [];
              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
                startY: finalX + 13,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }
            pdf.addPage();
          });

        // report view
        let finalY = 10;
        this.pdfData.map((data) => {
          pdf.autoTable(data.mainColumns, data.mainRows, {
            startY: finalY,
            headStyles: {
              fillColor: customColor,
              textColor: customFontColor,
            },
            alternateRowStyles: {
              fillColor: "#FFFFFF",
            },
          });
          finalY = pdf.previousAutoTable.finalY;
          pdf.autoTable(data.subColumns, data.subRows, {
            startY: finalY,
            headStyles: {
              fillColor: "#D8D8D8",
              textColor: "#000000",
            },
            alternateRowStyles: {
              fillColor: "#FFFFFF",
            },
          });
          finalY = pdf.previousAutoTable.finalY + 10;
        });
        pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
      }
    }
  }

  fillColor(TGT, OBS) {
    if (TGT > OBS) {
      return "#FF0000";
    } else if (TGT == OBS && TGT != -1) {
      return "#FFE135";
    } else if (TGT < OBS) {
      return "#7A9E5C";
    }
  }
}
