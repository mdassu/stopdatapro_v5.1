import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";

import { TranslateService } from "@ngx-translate/core";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { AuthenticationService } from "src/app/_services/authentication.service";

@Component({
  selector: "app-user-observer-list",
  templateUrl: "./user-observer-list.component.html",
  styleUrls: ["./user-observer-list.component.css"],
})
export class UserObserverListComponent implements OnInit {
  @Input() reportId: any;
  cols: any = [];
  tableData: any = [];
  title: any;
  selectedReportView: any = 39;
  isLoaded: boolean;
  errormsg: any;
  parameters: any;
  allSiteSelected: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  pdfRows: any = [];

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    // this.translate.get("LBLUNSAFEACTCOVERREPORT").subscribe(title => {
    //   this.title = title;
    // });

    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT", url: "./assets/help/user-observer-list.md" },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);

    this.title = "Users / Observer List";

    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.allSiteSelected = this.parameters["SITEID"];
      this.selectedReportView = this.parameters["designId"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    this.getReport();
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReport() {
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (res["status"] == true) {
        this.tableData = res["Data"];
        this.tableData.map((i) => {
          this.translate
            .get([
              i["APPROVEOFFLINE"],
              i["OFFLINEACCESS"],
              i["STATUS"],
              i["USERTYPE"],
            ])
            .subscribe((label) => {
              i["APPROVEOFFLINE"] = label[i["APPROVEOFFLINE"]];
              i["OFFLINEACCESS"] = label[i["OFFLINEACCESS"]];
              i["STATUS"] = label[i["STATUS"]];
              i["USERTYPE"] = label[i["USERTYPE"]];
            });
        });
        this.isLoaded = true;
        var cols = Object.keys(this.tableData[0]);
        // var index = cols.indexOf("USERID");
        // if (index > -1) {
        //   cols.splice(index, 1);
        // }

        cols.map((col) => {
          this.translate.get("LBL" + col).subscribe((label) => {
            if ("LBL" + col != label) {
              this.cols.push({ field: col, header: label });
            } else {
              if (col == "USERTYPE") {
                this.translate.get("LBLUSERLEVEL").subscribe((resLabel) => {
                  this.cols.push({ field: col, header: resLabel });
                });
              } else {
                this.cols.push({ field: col, header: col });
              }
            }
          });
        });
        this.manageExportData();
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  manageExportData(): void {
    this.tableRows = [];
    this.pdfRows = [];
    this.columns = [];
    if (this.tableData.length > 0 && this.cols.length > 0) {
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempRows = [];
        this.cols.map((item) => {
          dataJson[item["header"]] = data[item["field"]];
          // dataJson[this.cols[0].header] = data[this.cols[0].field];
          // dataJson[this.cols[1].header] = data[this.cols[1].field];
          tempRows.push(data[item["field"]]);
          if (this.tableData.length - 1 == key) {
            this.columns.push(item.header);
          }
        });
        this.tableRows.push(dataJson);
        this.pdfRows.push(tempRows);
      });
    }
    // this.columns[0] = this.cols[0].header;
    // this.columns[1] = this.cols[1].header;
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    var fileName = "USEROBSERVERLIST";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      this.exportAsExcelFile(this.tableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();
      let worksheet = workbook.addWorksheet("Sheet1");
      //Add Header Row
      let headerRow = worksheet.addRow(header);
      headerRow.height = 20;
      // Cell Style : Fill and Border
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: customColor },
        };
        cell.font = {
          color: { argb: customFontColor },
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
        if (number == 2) {
          cell.alignment = { vertical: "middle", horizontal: "center" };
        } else {
          cell.alignment = { vertical: "middle", horizontal: "left" };
        }
      });
      // Add Data and Conditional Formatting
      let sumOfData = 0;
      data.forEach((element) => {
        let eachRow = [];
        this.columns.map((headers, key) => {
          if (key == 1) {
            sumOfData = sumOfData + element[headers];
          }
          eachRow.push(element[headers]);
        });
        if (element.isDeleted === "Y") {
          let deletedRow = worksheet.addRow(eachRow);
          deletedRow.eachCell((cell, number) => {
            cell.font = {
              name: "Calibri",
              family: 4,
              size: 11,
              bold: false,
              strike: true,
            };
          });
        } else {
          var addRow = worksheet.addRow(eachRow);
          addRow.getCell(2).alignment = {
            vertical: "middle",
            horizontal: "center",
          };
          addRow.eachCell((cell) => {
            cell.numFmt = this.auth.changeCellValueType(cell.value);
          });
        }
      });

      header.map((item, key) => {
        if (key == 3) {
          worksheet.getColumn(key + 1).width = 30;
        } else if (key > 11) {
          worksheet.getColumn(key + 1).width = 25;
        } else {
          worksheet.getColumn(key + 1).width = 20;
        }
      });

      worksheet.addRow([]);
      //Generate Excel File with given name
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape", "mm", [1800, 800]);
      pdf.autoTable(this.columns, this.pdfRows, {
        startY: 10,
        headStyles: {
          fillColor: customColor,
          textColor: customFontColor,
        },
        alternateRowStyles: {
          fillColor: "#FFFFFF",
        },
        columnStyles: {
          0: { cellWidth: 20 },
          1: { cellWidth: 20 },
          2: { cellWidth: 20 },
          3: { cellWidth: 30 },
          4: { cellWidth: 20 },
          5: { cellWidth: 20 },
          6: { cellWidth: 20 },
          7: { cellWidth: 20 },
          8: { cellWidth: 20 },
          9: { cellWidth: 20 },
          10: { cellWidth: 20 },
          11: { cellWidth: 20 },
          12: { cellWidth: 20 },
          13: { cellWidth: 20 },
          14: { cellWidth: 20 },
          15: { cellWidth: 20 },
          16: { cellWidth: 20 },
          17: { cellWidth: 20 },
          18: { cellWidth: 20 },
          19: { cellWidth: 20 },
          20: { cellWidth: 20 },
          21: { cellWidth: 20 },
        },
      });
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }
}
