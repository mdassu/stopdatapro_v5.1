import { Component, OnInit, Input, ɵConsole } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { formatDate } from "@angular/common";
import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";

import { TranslateService } from "@ngx-translate/core";
import { isArray } from "util";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { GlobalDataService } from "src/app/_services/global-data.service";

@Component({
  selector: "app-site-observation-stats",
  templateUrl: "./site-observation-stats.component.html",
  styleUrls: ["./site-observation-stats.component.css"],
})
export class SiteObservationStatsComponent implements OnInit {
  @Input() reportId: any;
  selectedReportView: any = 11;
  selectedSiteId: any = "";
  selectedYear: any = "";
  cols: any[];
  tableData: any = [];
  indexData: any = [];
  title: any;
  isLoaded: boolean;
  errormsg: any;
  rowGroupMetadata: any;
  reportView: any = [];
  siteList: any = [{ label: "All", value: "" }];
  yearList: any = [{ label: "All", value: "" }];
  parameters: any;
  allSiteSelected: any;
  fieldCols: any = [];
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any = 0;
  tableRows: any;
  columns: any;
  unFTableRows: any;
  unFColumns: any;
  pdfData: any = [];
  dataKey: any = "Site Name";
  colId: any;
  mainHead: any;
  totalInfo: any = [];
  subAreaOption: number;
  colleps: number = 0;
  lastHead: any = [];
  lastRow: any = [];

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthenticationService,
    private globalData: GlobalDataService
  ) {}

  ngOnInit() {
    this.breadcrumbService.setItems([
      {
        label: "LBLREPORTMGMT",
        url: "./assets/help/site-observation-stats.md",
      },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);
    this.translate.get("LBLOBSPERHOURRPT").subscribe((title) => {
      this.title = title;
    });
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.allSiteSelected = this.parameters["SITEID"];
      this.selectedReportView = this.parameters["designId"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }
    this.getReports();

    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      if (isArray(res["designData"])) {
        this.reportView = [];
        var designData = res["designData"];
        designData.map((item) => {
          this.translate.get(item.RPTNAME).subscribe((label) => {
            this.reportView.push({
              label: label,
              value: item.DESIGNID,
            });
          });
        });
      }
      if (res["status"] == true) {
        this.tableData = res["Data"];
        this.totalInfo = res["Total"];
        this.lastHead = [];
        this.translate
          .get([
            "LBLTOTALTIMEINMINS",
            "LBLTOTALOBSERVED",
            "LBLTOTALCONTACTED",
            "LBLSAFECOUNT",
            "LBLUNSAFECOUNT",
          ])
          .subscribe((resLabel) => {
            this.lastHead.push(resLabel["LBLTOTALTIMEINMINS"]);
            this.lastHead.push(resLabel["LBLTOTALOBSERVED"]);
            this.lastHead.push(resLabel["LBLTOTALCONTACTED"]);
            if (this.selectedReportView == 11) {
              this.lastHead.push(resLabel["LBLSAFECOUNT"]);
              this.lastHead.push(resLabel["LBLUNSAFECOUNT"]);
            } else if (this.selectedReportView == 29) {
              this.lastHead.push(resLabel["LBLSAFECOUNT"]);
            } else if (this.selectedReportView == 30) {
              this.lastHead.push(resLabel["LBLUNSAFECOUNT"]);
            }
          });
        this.lastRow = [];
        this.lastRow.push(this.totalInfo.RECORDSLENGTH);
        this.lastRow.push(this.totalInfo.OBSERVED);
        this.lastRow.push(this.totalInfo.CONTACTED);
        if (this.selectedReportView == 11) {
          this.lastRow.push(this.totalInfo.SAFE);
          this.lastRow.push(this.totalInfo.UNSAFE);
        } else if (this.selectedReportView == 29) {
          this.lastRow.push(this.totalInfo.SAFE);
        } else if (this.selectedReportView == 30) {
          this.lastRow.push(this.totalInfo.UNSAFE);
        }

        if (this.tableData.length > 0) {
          if (this.siteList.length == 1) {
            res["sites"].map((site) => {
              this.siteList.push({ label: site.SITENAME, value: site.SITEID });
            });
          }
          if (this.yearList.length == 1) {
            res["year"].map((item) => {
              this.yearList.push({ label: item.YEAR, value: item.YEAR });
            });
          }
          this.fieldCols = [];
          if (this.tableData.length > 0) {
            var keys = Object.keys(this.tableData[0]);
          }
          var tempColumn = [
            "AREAID",
            "AREANAME",
            "CARDID",
            "CONTACTED",
            "CUSPOSITION",
            "LENGTH",
            "OBSERVED",
            "OBSERVERNAME",
            "RECORDSLENGTH",
            "SAFE",
            "SHIFTID",
            "SHIFTNAME",
            "SITEID",
            "SITENAME",
            "SUBAREANAME",
            "TOTALCONTACTED",
            "TOTALOBSERVED",
            "UNSAFE",
            "YEAR",
          ];

          keys.map((label) => {
            if (tempColumn.indexOf(label) == -1) {
              this.fieldCols.push(label);
            }
          });

          this.translate
            .get([
              "LBLCHECKLISTNO",
              "LBLOBSERVERNAME",
              "LBLAREANAME",
              "LBLSUBAREAFULLNAME",
              "LBLSHIFTNAME",
              "LBLLENGTH",
              "LBLOBSERVED",
              "LBLCONTACTED",
              "LBLSAFE",
              "LBLUNSAFE",
            ])
            .subscribe((resLabel) => {
              this.subAreaOption = this.auth.UserInfo["subAreaOptionValue"];
              if (this.subAreaOption == 0) {
                this.colleps = 1;
                this.cols = [
                  {
                    field: "CARDID",
                    header: resLabel["LBLCHECKLISTNO"],
                  },
                  {
                    field: "OBSERVERNAME",
                    header: resLabel["LBLOBSERVERNAME"],
                  },
                  {
                    field: "AREANAME",
                    header: resLabel["LBLAREANAME"],
                  },
                  {
                    field: "SHIFTNAME",
                    header: resLabel["LBLSHIFTNAME"],
                  },
                  {
                    field: "LENGTH",
                    header: resLabel["LBLLENGTH"],
                  },
                  {
                    field: "OBSERVED",
                    header: resLabel["LBLOBSERVED"],
                  },
                  {
                    field: "CONTACTED",
                    header: resLabel["LBLCONTACTED"],
                  },
                ];
              } else {
                this.colleps = 0;
                this.cols = [
                  {
                    field: "CARDID",
                    header: resLabel["LBLCHECKLISTNO"],
                  },
                  {
                    field: "OBSERVERNAME",
                    header: resLabel["LBLOBSERVERNAME"],
                  },
                  {
                    field: "AREANAME",
                    header: resLabel["LBLAREANAME"],
                  },
                  {
                    field: "SUBAREANAME",
                    header: resLabel["LBLSUBAREAFULLNAME"],
                  },
                  {
                    field: "SHIFTNAME",
                    header: resLabel["LBLSHIFTNAME"],
                  },
                  {
                    field: "LENGTH",
                    header: resLabel["LBLLENGTH"],
                  },
                  {
                    field: "OBSERVED",
                    header: resLabel["LBLOBSERVED"],
                  },
                  {
                    field: "CONTACTED",
                    header: resLabel["LBLCONTACTED"],
                  },
                ];
              }
              if (this.selectedReportView == 11) {
                this.cols.push({
                  field: "SAFE",
                  header: resLabel["LBLSAFE"],
                });
                this.cols.push({
                  field: "UNSAFE",
                  header: resLabel["LBLUNSAFE"],
                });
              } else if (this.selectedReportView == 29) {
                this.cols.push({
                  field: "SAFE",
                  header: resLabel["LBLSAFE"],
                });
              } else if (this.selectedReportView == 30) {
                this.cols.push({
                  field: "UNSAFE",
                  header: resLabel["LBLUNSAFE"],
                });
              }
            });
          this.updateRowGroupMetaData();
        } else {
          this.errormsg = "LBLRPTNORECFND";
        }
        this.isLoaded = true;
        this.manageExportData();
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    this.mainHead = [];
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let tableData = this.tableData[i];
        let sitename = tableData.SITENAME;
        if (i == 0) {
          this.rowGroupMetadata[sitename] = { index: 0, size: 1 };
          this.mainHead.push({
            SITENAME: sitename,
            RECORDSLENGTH: tableData.RECORDSLENGTH,
            TOTALOBSERVED: tableData.TOTALOBSERVED,
            TOTALCONTACTED: tableData.TOTALCONTACTED,
          });
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.SITENAME;
          if (sitename === previousRowGroup) {
            this.rowGroupMetadata[sitename].size++;
          } else {
            this.rowGroupMetadata[sitename] = { index: i, size: 1 };
            this.mainHead.push({
              SITENAME: sitename,
              RECORDSLENGTH: tableData.RECORDSLENGTH,
              TOTALOBSERVED: tableData.TOTALOBSERVED,
              TOTALCONTACTED: tableData.TOTALCONTACTED,
            });
          }
        }
      }
    }
  }

  changeReport() {
    this.isLoaded = false;
    this.parameters["designId"] = this.selectedReportView;
    if (this.selectedSiteId == 0) {
      this.parameters["SITEID"] = this.allSiteSelected;
    } else {
      this.parameters["SITEID"] = this.selectedSiteId;
    }
    this.getReports();
  }

  yearFilter() {
    if (this.selectedYear != "") {
      var newData = this.tableData.filter((item) => {
        return item.YEAR === this.selectedYear;
      });

      this.tableData = newData;
      this.updateRowGroupMetaData();
    } else {
      this.changeReport();
    }
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    var enableSubarea = 0;
    if (this.globalData.GlobalData["ENABLESUBAREA"] != 1) {
      enableSubarea = 1;
    }
    if (this.tableData.length > 0 && this.cols.length > 0) {
      // get data for Excel
      this.tableData.map((data, key) => {
        var dataJson = {};
        var tempDataJson = {};
        // var tempRows = [];
        this.translate
          .get([
            "LBLSITENAME",
            "LBLTOTALTIMEINMINS",
            "LBLTOTALOBSERVED",
            "LBLTOTALCONTACTED",
          ])
          .subscribe((resLabel) => {
            if (key == 0) {
              this.dataKey = resLabel["LBLSITENAME"];
              this.unFColumns.push(resLabel["LBLSITENAME"]);
              this.unFColumns.push(resLabel["LBLTOTALTIMEINMINS"]);
              this.unFColumns.push(resLabel["LBLTOTALOBSERVED"]);
              this.unFColumns.push(resLabel["LBLTOTALCONTACTED"]);
            }
            dataJson[resLabel["LBLSITENAME"]] = data["SITENAME"];
            dataJson[resLabel["LBLTOTALTIMEINMINS"]] = data["RECORDSLENGTH"];
            dataJson[resLabel["LBLTOTALOBSERVED"]] = data["TOTALOBSERVED"];
            dataJson[resLabel["LBLTOTALCONTACTED"]] = data["TOTALCONTACTED"];
            tempDataJson[resLabel["LBLSITENAME"]] = data["SITENAME"];
            tempDataJson[resLabel["LBLTOTALTIMEINMINS"]] =
              data["RECORDSLENGTH"];
            tempDataJson[resLabel["LBLTOTALOBSERVED"]] = data["TOTALOBSERVED"];
            tempDataJson[resLabel["LBLTOTALCONTACTED"]] =
              data["TOTALCONTACTED"];
          });
        this.cols.map((item, index) => {
          if (item["field"] == "OBSERVERNAME") {
            var regexHash = new RegExp("<BR>", "g");
            if (
              data[item["field"]] &&
              data[item["field"]].search("<BR>") != -1
            ) {
              data[item["field"]] = data[item["field"]].replace(
                regexHash,
                "\n"
              );
            }
          }
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];
          if (index == 4 - enableSubarea) {
            this.fieldCols.map((fieldCol) => {
              dataJson[fieldCol] = data[fieldCol];
              tempDataJson[fieldCol] = data[fieldCol];
              // tempRows.push(data[fieldCol]);
            });
          }
          // tempRows.push(data[item["field"]]);
          if (this.tableData.length - 1 == key) {
            if (index == 5 - enableSubarea) {
              this.fieldCols.map((fieldCol) => {
                this.columns.push(fieldCol);
                this.unFColumns.push(fieldCol);
              });
            }
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
            if (this.cols.length - 1 == index) {
              this.colId = String.fromCharCode(
                97 + this.columns.length - 1
              ).toUpperCase();
            }
          }
        });
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);
        // this.pdfData.push(tempRows);
      });

      // get data for PDF
      var mainColumns = [];
      this.translate
        .get([
          "LBLSITENAME",
          "LBLTOTALTIMEINMINS",
          "LBLTOTALOBSERVED",
          "LBLTOTALCONTACTED",
        ])
        .subscribe((resLabel) => {
          mainColumns.push(resLabel["LBLSITENAME"]);
          mainColumns.push(resLabel["LBLTOTALTIMEINMINS"]);
          mainColumns.push(resLabel["LBLTOTALOBSERVED"]);
          mainColumns.push(resLabel["LBLTOTALCONTACTED"]);
        });
      var tempMainColums = Object.keys(this.mainHead[0]);
      var subColumns = this.columns;
      this.mainHead.map((head) => {
        var mainRows = [];
        var subRows = [];
        // main Table
        var tempMainRows = [];
        tempMainColums.map((col) => {
          tempMainRows.push(head[col]);
        });
        mainRows.push(tempMainRows);

        // sub Table
        this.tableData.map((data) => {
          var tempSubRows = [];
          if (data["SITENAME"] == head["SITENAME"]) {
            this.cols.map((subCol, index) => {
              tempSubRows.push(data[subCol.field]);
              if (index == 4 - enableSubarea) {
                this.fieldCols.map((fieldCol) => {
                  tempSubRows.push(data[fieldCol]);
                });
              }
            });
            subRows.push(tempSubRows);
          }
        });
        this.pdfData.push({
          mainColumns: mainColumns,
          mainRows: mainRows,
          subColumns: subColumns,
          subRows: subRows,
        });
      });
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
    for (var i = 0; i <= 3; i++) {
      this.tableRows.pop();
    }
  }

  exportData(): void {
    var fileName = "SITEOBSSTATS";
    if (this.selectedReportView == 29) {
      fileName = "SITEOBSSTATS_SAFE";
    } else if (this.selectedReportView == 30) {
      fileName = "SITEOBSSTATS_UNSAFE";
    }
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var totalHead;
    this.translate
      .get([
        "LBLTOTALTIMEINMINS",
        "LBLTOTALOBSERVED",
        "LBLTOTALCONTACTED",
        "LBLSAFECOUNT",
        "LBLUNSAFECOUNT",
      ])
      .subscribe((resLabel) => {
        totalHead = [
          resLabel["LBLTOTALTIMEINMINS"],
          resLabel["LBLTOTALOBSERVED"],
          resLabel["LBLTOTALCONTACTED"],
          resLabel["LBLSAFECOUNT"],
          resLabel["LBLUNSAFECOUNT"],
        ];
        if (this.selectedReportView == 29) {
          totalHead = [
            resLabel["LBLTOTALTIMEINMINS"],
            resLabel["LBLTOTALOBSERVED"],
            resLabel["LBLTOTALCONTACTED"],
            resLabel["LBLSAFECOUNT"],
          ];
        } else if (this.selectedReportView == 30) {
          totalHead = [
            resLabel["LBLTOTALTIMEINMINS"],
            resLabel["LBLTOTALOBSERVED"],
            resLabel["LBLTOTALCONTACTED"],
            resLabel["LBLUNSAFECOUNT"],
          ];
        }
      });
    var totalRow = [
      this.totalInfo.RECORDSLENGTH,
      this.totalInfo.OBSERVED,
      this.totalInfo.CONTACTED,
      this.totalInfo.SAFE,
      this.totalInfo.UNSAFE,
    ];
    if (this.selectedReportView == 29) {
      totalRow = [
        this.totalInfo.RECORDSLENGTH,
        this.totalInfo.OBSERVED,
        this.totalInfo.CONTACTED,
        this.totalInfo.SAFE,
      ];
    } else if (this.selectedReportView == 30) {
      totalRow = [
        this.totalInfo.RECORDSLENGTH,
        this.totalInfo.OBSERVED,
        this.totalInfo.CONTACTED,
        this.totalInfo.UNSAFE,
      ];
    }
    if (this.selectedExport == 6) {
      var object = Object.keys(this.tableRows[0]);
      for (var i = 0; i < 4; i++) {
        var dataJson = {};
        if (i < 2) {
          object.map((item, key) => {
            dataJson[item] = "";
            if (object.length - 1 == key) {
              this.tableRows.push(dataJson);
            }
          });
        } else if (i == 2) {
          object.map((item, key) => {
            if (totalHead[key]) {
              dataJson[item] = totalHead[key];
            } else {
              dataJson[item] = "";
            }
            if (object.length - 1 == key) {
              this.tableRows.push(dataJson);
            }
          });
        } else if (i == 3) {
          object.map((item, key) => {
            if (totalRow[key]) {
              dataJson[item] = totalRow[key];
            } else {
              dataJson[item] = "";
            }
            if (object.length - 1 == key) {
              this.tableRows.push(dataJson);
            }
          });
        }
      }
      this.exportAsExcelFile(this.tableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;
      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;

      const EXCEL_TYPE =
        "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();

      // Report scope sheet
      let firstWorksheet = workbook.addWorksheet("Report Scope");
      firstWorksheet.addRow([]);
      let firstHeaderRow = firstWorksheet.addRow([this.title]);

      firstWorksheet.getCell("A2").font = {
        name: "Arial Unicode MS",
        family: 4,
        size: 18,
        bold: true,
        color: { argb: "00000000" },
      };
      firstWorksheet.getCell("A2").alignment = {
        vertical: "middle",
        horizontal: "left",
        indent: 1,
      };
      firstWorksheet.getColumn("A").width = 100;
      firstWorksheet.getRow(2).height = 30;

      this.translate
        .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        .subscribe((resLabel) => {
          if (this.parameters["OBSDATE"]) {
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([]);
            firstWorksheet.addRow([resLabel["LBLOBSDATE"]]).eachCell((cell) => {
              cell.font = {
                name: "Arial Unicode MS",
                family: 4,
                size: 14,
                bold: true,
                color: { argb: "00000000" },
              };
              cell.alignment = {
                vertical: "middle",
                horizontal: "left",
                indent: 2,
              };
            });

            firstWorksheet.lastRow.height = 25;
            var bothDate = this.parameters["OBSDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            firstWorksheet
              .addRow([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ])
              .eachCell((cell) => {
                cell.font = {
                  name: "Arial Unicode MS",
                  family: 4,
                  size: 12,
                  color: { argb: "00000000" },
                };
                cell.alignment = {
                  vertical: "middle",
                  horizontal: "left",
                  indent: 3,
                };
              });
          }

          firstWorksheet.lastRow.height = 25;
        });

      let worksheet = workbook.addWorksheet("Sheet1");

      if (this.selectedExport == 4 || this.selectedExport == 5) {
        for (var i = 1; i <= 15; i++) {
          worksheet.getColumn(i).width = 25;
        }

        let headerRow = worksheet.addRow(unFColumns);
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };

          var from;
          if (this.subAreaOption == 1) {
            from = this.fieldCols.length + 9;
          } else {
            from = this.fieldCols.length + 8;
          }

          if ((number > 1 && number <= 5) || number > from) {
            cell.alignment = {
              vertical: "middle",
              horizontal: "center",
            };
          }
        });

        // Add Data and Conditional Formatting
        unFTableRows.map((element, index) => {
          let eachRow = [];
          unFColumns.map((headers, key) => {
            if (headers == "User Level" || headers == "Status") {
              if (element[headers]) {
                this.translate.get(element[headers]).subscribe((val) => {
                  eachRow.push(val);
                });
              } else {
                eachRow.push(element[headers]);
              }
            } else {
              eachRow.push(element[headers]);
            }
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            if (unFTableRows.length - 1 == index) {
              worksheet
                .addRow(eachRow)
                .eachCell({ includeEmpty: true }, (cell, number) => {
                  cell.border = {
                    bottom: { style: "medium", color: { argb: customColor } },
                  };
                  var from;
                  if (this.subAreaOption == 1) {
                    from = this.fieldCols.length + 9;
                  } else {
                    from = this.fieldCols.length + 8;
                  }

                  if ((number > 1 && number <= 5) || number > from) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                  cell.numFmt = this.auth.changeCellValueType(cell.value);
                });
            } else {
              worksheet.addRow(eachRow).eachCell((cell, number) => {
                var from;
                if (this.subAreaOption == 1) {
                  from = this.fieldCols.length + 9;
                } else {
                  from = this.fieldCols.length + 8;
                }

                if ((number > 1 && number <= 5) || number > from) {
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "center",
                  };
                }
                cell.numFmt = this.auth.changeCellValueType(cell.value);
              });
            }
          }
        });
        worksheet.addRow([]);
        let lastHeaderRow = worksheet.addRow(this.lastHead);
        lastHeaderRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          cell.alignment = {
            vertical: "middle",
            horizontal: "center",
          };
        });
        let lastRow = worksheet.addRow(this.lastRow);
        lastRow.eachCell((cell) => {
          cell.border = {
            bottom: { style: "medium", color: { argb: customColor } },
          };
          cell.alignment = {
            vertical: "middle",
            horizontal: "center",
          };
          cell.numFmt = this.auth.changeCellValueType(cell.value);
        });
      } else {
        let subHeader = header;
        var beforeDataCount = 1;
        this.mainHead.map((header, key) => {
          var rowsData = data.filter((item) => {
            return item[this.dataKey] == header["SITENAME"];
          });
          for (var i = 1; i <= 15; i++) {
            worksheet.getColumn(i).width = 25;
          }
          if (key == 0) {
            worksheet.mergeCells(
              "D" + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              "D" +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                (beforeDataCount + 1)
            );
            this.translate
              .get([
                "LBLSITENAME",
                "LBLTOTALTIMEINMINS",
                "LBLTOTALOBSERVED",
                "LBLTOTALCONTACTED",
              ])
              .subscribe((resLabel) => {
                worksheet.getCell("A" + beforeDataCount).value =
                  resLabel["LBLSITENAME"];
                worksheet.getCell("B" + beforeDataCount).value =
                  resLabel["LBLTOTALTIMEINMINS"];
                worksheet.getCell("C" + beforeDataCount).value =
                  resLabel["LBLTOTALOBSERVED"];
                worksheet.getCell("D" + beforeDataCount).value =
                  resLabel["LBLTOTALCONTACTED"];

                worksheet.getCell("B" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
                worksheet.getCell("C" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
                worksheet.getCell("D" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              });

            worksheet.getCell("A" + (beforeDataCount + 1)).value =
              header["SITENAME"];
            worksheet.getCell("B" + (beforeDataCount + 1)).value =
              header["RECORDSLENGTH"];
            worksheet.getCell("C" + (beforeDataCount + 1)).value =
              header["TOTALOBSERVED"];
            worksheet.getCell("D" + (beforeDataCount + 1)).value =
              header["TOTALCONTACTED"];

            worksheet.getCell("B" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("C" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("D" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("B" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("C" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("D" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("B" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("C" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("D" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 5;
          } else {
            worksheet.mergeCells(
              "D" + beforeDataCount + ":" + this.colId + beforeDataCount
            );
            worksheet.mergeCells(
              "D" +
                (beforeDataCount + 1) +
                ":" +
                this.colId +
                +(beforeDataCount + 1)
            );
            this.translate
              .get([
                "LBLSITENAME",
                "LBLTOTALTIMEINMINS",
                "LBLTOTALOBSERVED",
                "LBLTOTALCONTACTED",
              ])
              .subscribe((resLabel) => {
                worksheet.getCell("A" + beforeDataCount).value =
                  resLabel["LBLSITENAME"];
                worksheet.getCell("B" + beforeDataCount).value =
                  resLabel["LBLTOTALTIMEINMINS"];
                worksheet.getCell("C" + beforeDataCount).value =
                  resLabel["LBLTOTALOBSERVED"];
                worksheet.getCell("D" + beforeDataCount).value =
                  resLabel["LBLTOTALCONTACTED"];
                worksheet.getCell("B" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
                worksheet.getCell("C" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
                worksheet.getCell("D" + beforeDataCount).alignment = {
                  vertical: "middle",
                  horizontal: "center",
                };
              });

            worksheet.getCell("A" + (beforeDataCount + 1)).value =
              header["SITENAME"];
            worksheet.getCell("B" + (beforeDataCount + 1)).value =
              header["RECORDSLENGTH"];
            worksheet.getCell("C" + (beforeDataCount + 1)).value =
              header["TOTALOBSERVED"];
            worksheet.getCell("D" + (beforeDataCount + 1)).value =
              header["TOTALCONTACTED"];
            worksheet.getCell("B" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("C" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("D" + (beforeDataCount + 1)).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            worksheet.getCell("A" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("B" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("C" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("D" + beforeDataCount).fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            worksheet.getCell("A" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("B" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("C" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            worksheet.getCell("D" + beforeDataCount).font = {
              color: { argb: customFontColor },
            };
            beforeDataCount = rowsData.length + 4 + beforeDataCount;
          }
          let headerRow = worksheet.addRow(subHeader);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: "EEECE1" },
            };
            cell.font = {
              color: { argb: "000000" },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
            var from;
            if (this.subAreaOption == 1) {
              from = this.fieldCols.length + 5;
            } else {
              from = this.fieldCols.length + 4;
            }
            if (number == 1 || number > from) {
              cell.alignment = {
                vertical: "middle",
                horizontal: "center",
              };
            }
          });
          // Add Data and Conditional Formatting
          rowsData.map((element, index) => {
            let eachRow = [];
            this.columns.map((headers, key) => {
              if (headers == "User Level" || headers == "Status") {
                if (element[headers]) {
                  this.translate.get(element[headers]).subscribe((val) => {
                    eachRow.push(val);
                  });
                } else {
                  eachRow.push(element[headers]);
                }
              } else {
                eachRow.push(element[headers]);
              }
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
              });
            } else {
              if (rowsData.length - 1 == index) {
                worksheet
                  .addRow(eachRow)
                  .eachCell({ includeEmpty: true }, (cell, number) => {
                    cell.border = {
                      bottom: { style: "medium", color: { argb: customColor } },
                    };
                    var from;
                    if (this.subAreaOption == 1) {
                      from = this.fieldCols.length + 5;
                    } else {
                      from = this.fieldCols.length + 4;
                    }
                    if (number == 1 || number > from) {
                      cell.alignment = {
                        vertical: "middle",
                        horizontal: "center",
                      };
                    }
                  });
              } else {
                worksheet.addRow(eachRow).eachCell((cell, number) => {
                  var from;
                  if (this.subAreaOption == 1) {
                    from = this.fieldCols.length + 5;
                  } else {
                    from = this.fieldCols.length + 4;
                  }
                  if (number == 1 || number > from) {
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "center",
                    };
                  }
                });
              }
            }
          });
        });
        worksheet.addRow([]);
        let lastHeaderRow = worksheet.addRow(this.lastHead);
        lastHeaderRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          cell.alignment = {
            vertical: "middle",
            horizontal: "center",
          };
        });
        let lastRow = worksheet.addRow(this.lastRow);
        lastRow.eachCell((cell) => {
          cell.border = {
            bottom: { style: "medium", color: { argb: customColor } },
          };
          cell.alignment = {
            vertical: "middle",
            horizontal: "center",
          };
        });
      }

      worksheet.addRow([]);
      //Generate Excel File with given name
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
        );
      });
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape", "mm", [1200, 800]);
      // report scope
      pdf.text(this.title, 15, 20);
      let finalX = 10;
      this.translate
        .get(["LBLOBSDATE", "LBLCREATEDDATE"])
        .subscribe((resLabel) => {
          if (this.parameters["OBSDATE"]) {
            let obsDate = [];
            var bothDate = this.parameters["OBSDATE"].split("-");
            var fDate = formatDate(
              bothDate[0],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            var tDate = formatDate(
              bothDate[1],
              this.auth.UserInfo["dateFormat"],
              this.translate.getDefaultLang()
            );
            obsDate.push([
              fDate +
                " - " +
                tDate +
                " (" +
                this.auth.UserInfo["dateFormat"].toUpperCase() +
                ")",
            ]);
            pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
              startY: finalX + 13,
              headStyles: {
                fillColor: customColor,
                textColor: customFontColor,
              },
              alternateRowStyles: {
                fillColor: "#FFFFFF",
              },
            });
            finalX = pdf.previousAutoTable.finalX;
          }
          pdf.addPage();
        });

      // report view
      let finalY = 10;
      var fn = this;
      this.pdfData.map((data) => {
        pdf.autoTable(data.mainColumns, data.mainRows, {
          startY: finalY,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          didParseCell: (colData) => {
            fn.alignCol(colData, 1);
          },
        });
        finalY = pdf.previousAutoTable.finalY;
        pdf.autoTable(data.subColumns, data.subRows, {
          startY: finalY,
          headStyles: {
            fillColor: "#D8D8D8",
            textColor: "#000000",
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          columnStyles: {
            0: { cellWidth: 20 },
            1: { cellWidth: 30 },
            2: { cellWidth: 20 },
            3: { cellWidth: 20 },
            4: { cellWidth: 20 },
            5: { cellWidth: 20 },
            6: { cellWidth: 20 },
            7: { cellWidth: 20 },
            8: { cellWidth: 20 },
            9: { cellWidth: 20 },
            10: { cellWidth: 20 },
            11: { cellWidth: 20 },
            12: { cellWidth: 20 },
          },
          didParseCell: (colData) => {
            fn.alignCol(colData, 2);
          },
        });
        finalY = pdf.previousAutoTable.finalY + 10;
      });
      finalY = pdf.previousAutoTable.finalY;
      pdf.autoTable(this.lastHead, [this.lastRow], {
        startY: finalY,
        headStyles: {
          fillColor: customColor,
          textColor: customFontColor,
        },
        alternateRowStyles: {
          fillColor: "#FFFFFF",
        },
        didParseCell: (colData) => {
          fn.alignCol(colData, 3);
        },
      });
      finalY = pdf.previousAutoTable.finalY + 10;
      pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
    }
  }

  alignCol = function (data, type) {
    var from;
    if (this.subAreaOption == 1) {
      from = this.fieldCols.length + 5;
    } else {
      from = this.fieldCols.length + 4;
    }
    if (type == 1) {
      if (data.column.index > 0) {
        data.cell.styles.halign = "center";
      }
    } else if (type == 2) {
      if (data.column.index == 0 || data.column.index > from - 1) {
        data.cell.styles.halign = "center";
      }
    } else {
      data.cell.styles.halign = "center";
    }
  };
}
