import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ReportsService } from "src/app/_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmationService } from "primeng/api";

@Component({
  selector: "app-edit-report",
  templateUrl: "./edit-report.component.html",
  styleUrls: ["./edit-report.component.css"],
  providers: [ConfirmationService]
})
export class EditReportComponent implements OnInit {
  reportId: any;
  isLoaded: boolean;
  reportList: any = [];
  selectedFilters: any = [];
  oldSelectedFilter: any = [];
  reportName: any;
  checkSelectedItem: boolean = true;
  countSelectedFilter: any;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private reportService: ReportsService,
    private translate: TranslateService,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit() {
    this.reportId = this.activatedRoute.snapshot.paramMap.get("reportId");
    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT" },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId]
      }
    ]);
    var params = {
      reportId: this.reportId
    };
    this.getFilterList(params);
  }

  getFilterList(params) {
    this.reportService.getEditFilter(params).subscribe(res => {
      if (res["status"] == true) {
        this.reportName = res["reportName"];
        this.reportList = res["data"];
        this.reportList.map((item, key) => {
          if (item.REPORTFILTER == 1) {
            this.selectedFilters.push(item);
            this.oldSelectedFilter.push(item);
            if (key == this.reportList.length - 1) {
              this.countSelectedFilter = this.selectedFilters.length;
            }
          }
        });
        this.isLoaded = true;
      }
    });
  }

  submitFilters() {
    this.translate.get("ALTREPORTEDITCONFIRM").subscribe((res: string) => {
      this.confirmationService.confirm({
        message: res,
        accept: () => {
          var filterIds = [];
          this.selectedFilters.map(item => {
            filterIds.push(item.OPTIONID);
          });

          var params = {
            reportId: this.reportId,
            optionId: filterIds.join()
          };
          this.reportService.editFilter(params).subscribe(res => {
            if (res["status"] == true) {
              this.router.navigate(["./report-filters/" + this.reportId], {
                skipLocationChange: true
              });
            }
          });
        }
      });
    });
  }

  checkItem() {
    if (this.selectedFilters.length == this.countSelectedFilter) {
      this.checkSelectedItem = true;
    } else {
      this.checkSelectedItem = false;
    }
  }

  resetFilterForm() {
    this.selectedFilters = this.oldSelectedFilter;
    if (this.selectedFilters.length == this.countSelectedFilter) {
      this.checkSelectedItem = true;
    } else {
      this.checkSelectedItem = false;
    }
  }
}
