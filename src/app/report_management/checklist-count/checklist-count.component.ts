import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BreadcrumbService } from "../../breadcrumb.service";
import { ReportsService } from "../../_services/reports.service";
import { formatDate } from "@angular/common";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";
import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { GlobalDataService } from "src/app/_services/global-data.service";

declare var $: any;

import pdfFonts from "src/assets/pdfmake/vfs_fonts";

@Component({
  selector: "app-checklist-count",
  templateUrl: "./checklist-count.component.html",
  styleUrls: ["./checklist-count.component.css"],
})
export class ChecklistCountComponent implements OnInit {
  @Input() reportId: any;
  cols: any[];
  graphicalCols: any[];
  tableData: any = [];
  title: any;
  reportView: any = [];
  siteList: any = [];
  reportType: any;
  siteName: any;
  selectedReportView: any;
  selectedSiteName: any = 0;
  allSiteSelected: any;
  isLoaded: boolean;
  errormsg: any;
  obsData: any = [];
  rowGroupMetadata: any;
  parameters: any;
  fieldCols: any = [];
  colCount: any;
  dateFormat: any;
  displaySubReport: boolean;
  selectedObserverId: any;
  totalRecords: any = 0;
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  unFTableRows: any;
  unFColumns: any;
  pdfData: any = [];
  dataKey: any = "USERNAME";
  mainHead: any;
  grandTotal: any = 0;
  userName: any;
  subAreaOption: number;
  colleps: number = 0;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private reportsService: ReportsService,
    private translate: TranslateService,
    private auth: AuthenticationService,
    private globalData: GlobalDataService
  ) {}

  ngOnInit() {
    this.breadcrumbService.setItems([
      {
        label: "LBLREPORTMGMT",
        url: "./assets/help/reports-checklist-count.md",
      },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
      {
        label: "LBLREPORTFILTERS",
        routerLink: ["/report-filters/" + this.reportId],
      },
    ]);
    this.dateFormat = this.auth.UserInfo["dateFormat"];
    this.translate.get("LBLCHECKLISTCOUNTRPT").subscribe((title) => {
      this.title = title;
    });
    if (
      this.reportsService.FilterInfo &&
      this.reportsService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportsService.FilterInfo;
      this.allSiteSelected = this.parameters["SITEID"];
    } else {
      this.router.navigate(["./reports"], {
        skipLocationChange: true,
      });
    }

    this.getReports();
    this.translate
      .get([
        "LBLNONE",
        "LBLXLSX",
        "LBLXLS",
        "LBLPDF",
        "LBLUFXLS",
        "LBLUXLSX",
        "LBLUFCSV",
        "LBLALL",
      ])
      .subscribe((resLabel) => {
        this.exportList = [
          { label: resLabel["LBLNONE"], value: 0 },
          { label: resLabel["LBLXLSX"], value: 1 },
          // { label: resLabel["LBLXLS"], value: 3 },
          { label: resLabel["LBLPDF"], value: 2 },
          // { label: resLabel["LBLUFXLS"], value: 4 },
          { label: resLabel["LBLUXLSX"], value: 5 },
          { label: resLabel["LBLUFCSV"], value: 6 },
        ];
        this.selectedExport = 0;
        this.siteList.push({ label: resLabel["LBLALL"], value: 0 });
      });
  }

  getReports() {
    this.errormsg = "";
    this.reportsService.getReportData(this.parameters).subscribe((res) => {
      var designData = res["designData"];
      this.reportView = [];
      designData.map((item) => {
        this.translate.get(item.RPTNAME).subscribe((label) => {
          this.reportView.push({
            label: label,
            value: item.DESIGNID,
          });
        });
      });
      this.selectedReportView = this.parameters["designId"];
      if (res["status"] == true) {
        this.tableData = res["Data"];
        if (this.selectedReportView == 22) {
          var fn = this;
          if (this.siteList.length == 1) {
            var sitelist = res["sitelist"];
            sitelist.forEach(function (site) {
              fn.siteList.push({ label: site.SITENAME, value: site.SITEID });
            });
          }
          this.isLoaded = true;
          this.translate
            .get(["LBLOBSERVERNAME", "LBLOBSCHECKLIST"])
            .subscribe((resLabel) => {
              this.cols = [
                { field: "USERNAME", header: resLabel["LBLOBSERVERNAME"] },
                {
                  field: "CHECKLISTCOUNT",
                  header: resLabel["LBLOBSCHECKLIST"],
                },
              ];
            });
        } else {
          if (this.tableData.length > 0) {
            var keys = Object.keys(this.tableData[0]);
          }
          var tempColumn = [
            "SITEID",
            "CARDID",
            "OBSERVERID",
            "SITENAME",
            "USERNAME",
            "OBSERVATIONDATE",
            "AREANAME",
            "SUBAREANAME",
            "SHIFTNAME",
            "ENTEREDDATE",
            "ENTEREDBY",
            "CUSPOSITION",
          ];

          keys.map((label) => {
            if (tempColumn.indexOf(label) == -1) {
              this.fieldCols.push(label);
            }
          });
          this.colCount = 7 + this.fieldCols.length;
          this.updateRowGroupMetaData();
          this.translate
            .get([
              "LBLOBSDATE",
              "LBLSITENAME",
              "LBLAREANAME",
              "LBLSUBAREAFULLNAME",
              "LBLSHIFTNAME",
              "LBLENTEREDBY",
              "LBLENTEREDDATE",
            ])
            .subscribe((resLabel) => {
              this.subAreaOption = this.auth.UserInfo["subAreaOptionValue"];
              if (this.subAreaOption == 0) {
                this.colleps = 1;
                this.cols = [
                  {
                    field: "OBSERVATIONDATE",
                    header: resLabel["LBLOBSDATE"],
                  },
                  { field: "SITENAME", header: resLabel["LBLSITENAME"] },
                  { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                  { field: "SHIFTNAME", header: resLabel["LBLSHIFTNAME"] },
                  { field: "ENTEREDBY", header: resLabel["LBLENTEREDBY"] },
                  { field: "ENTEREDDATE", header: resLabel["LBLENTEREDDATE"] },
                ];
                // this.cols.splice(3, 1);
              } else {
                this.colleps = 0;
                this.cols = [
                  {
                    field: "OBSERVATIONDATE",
                    header: resLabel["LBLOBSDATE"],
                  },
                  { field: "SITENAME", header: resLabel["LBLSITENAME"] },
                  { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                  {
                    field: "SUBAREANAME",
                    header: resLabel["LBLSUBAREAFULLNAME"],
                  },
                  { field: "SHIFTNAME", header: resLabel["LBLSHIFTNAME"] },
                  { field: "ENTEREDBY", header: resLabel["LBLENTEREDBY"] },
                  { field: "ENTEREDDATE", header: resLabel["LBLENTEREDDATE"] },
                ];
              }
            });
          this.isLoaded = true;
        }
        this.manageExportData();
      } else {
        this.isLoaded = true;
        this.errormsg = "LBLRPTNORECFND";
      }
    });
  }

  changeCheckListCount() {
    this.isLoaded = false;
    this.parameters["designId"] = this.selectedReportView;
    if (this.selectedSiteName == 0) {
      this.parameters["SITEID"] = this.allSiteSelected;
    } else {
      this.parameters["SITEID"] = this.selectedSiteName;
    }
    this.getReports();
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    this.mainHead = [];
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let rowData = this.tableData[i];
        let username = rowData.USERNAME;
        if (i == 0) {
          this.rowGroupMetadata[username] = { index: 0, size: 1 };
          this.mainHead.push({
            USERNAME: username,
          });
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.USERNAME;
          if (username === previousRowGroup) {
            this.rowGroupMetadata[username].size++;
          } else {
            this.rowGroupMetadata[username] = { index: i, size: 1 };
            this.mainHead.push({
              USERNAME: username,
            });
          }
        }
      }
    }
  }

  openSubReport(observerId, userName) {
    this.userName = userName;
    this.selectedObserverId = observerId;
    this.displaySubReport = true;
  }

  manageExportData(): void {
    this.unFColumns = [];
    this.unFTableRows = [];
    this.tableRows = [];
    this.pdfData = [];
    this.columns = [];
    var enableSubarea = 0;
    if (this.globalData.GlobalData["ENABLESUBAREA"] != 1) {
      enableSubarea = 1;
    }
    // if (this.selectedReportView == 22) {
    // columns.map(item => {
    if (this.tableData.length > 0 && this.cols.length > 0) {
      var pdfGrandTotal = 0;
      this.grandTotal = 0;
      this.translate.get(["LBLOBSERVERNAME"]).subscribe((resLabel) => {
        this.unFColumns.push(resLabel["LBLOBSERVERNAME"]);
      });
      this.tableData.map((data, key) => {
        this.grandTotal = this.grandTotal + data["CHECKLISTCOUNT"];
        var dataJson = {};
        var tempDataJson = {};
        var tempRows = [];
        this.cols.map((item, index) => {
          if (this.selectedReportView == 22 && index == 1) {
            pdfGrandTotal = pdfGrandTotal + data[item["field"]];
          }
          this.translate.get(["LBLOBSERVERNAME"]).subscribe((resLabel) => {
            tempDataJson[resLabel["LBLOBSERVERNAME"]] = data["USERNAME"];
          });
          dataJson[item["header"]] = data[item["field"]];
          tempDataJson[item["header"]] = data[item["field"]];
          // dataJson[this.cols[0].header] = data[this.cols[0].field];
          // dataJson[this.cols[1].header] = data[this.cols[1].field];
          if (index == 4 - enableSubarea) {
            this.fieldCols.map((fieldCol) => {
              dataJson[fieldCol] = data[fieldCol];
              tempDataJson[fieldCol] = data[fieldCol];
              tempRows.push(data[fieldCol]);
            });
          }
          tempRows.push(data[item["field"]]);
          if (this.tableData.length - 1 == key) {
            if (index == 5 - enableSubarea) {
              this.fieldCols.map((fieldCol) => {
                this.columns.push(fieldCol);
                this.unFColumns.push(fieldCol);
              });
            }
            this.columns.push(item.header);
            this.unFColumns.push(item.header);
          }
        });
        if (this.selectedReportView == 28) {
          dataJson["USERNAME"] = data["USERNAME"];
        }
        this.tableRows.push(dataJson);
        this.unFTableRows.push(tempDataJson);

        if (this.selectedReportView == 22) {
          this.pdfData.push(tempRows);
        }
      });
      if (this.selectedReportView == 22) {
        this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
          var totalJson = [label, pdfGrandTotal];
          this.pdfData.push(totalJson);
        });
      }
      if (this.selectedReportView == 28) {
        // get data for PDF
        var mainColumns = [];
        this.translate.get(["LBLOBSERVERNAME"]).subscribe((resLabel) => {
          mainColumns.push(resLabel["LBLOBSERVERNAME"]);
        });
        var tempMainColums = Object.keys(this.mainHead[0]);
        var subColumns = this.columns;
        this.mainHead.map((head) => {
          var mainRows = [];
          var subRows = [];
          // main Table
          var tempMainRows = [];
          tempMainColums.map((col) => {
            tempMainRows.push(head[col]);
          });
          mainRows.push(tempMainRows);

          // sub Table

          this.tableData.map((data) => {
            var tempSubRows = [];
            if (data["USERNAME"] == head["USERNAME"]) {
              this.cols.map((subCol, index) => {
                tempSubRows.push(data[subCol.field]);
                if (index == 4 - enableSubarea) {
                  this.fieldCols.map((fieldCol) => {
                    tempSubRows.push(data[fieldCol]);
                  });
                }
              });
              subRows.push(tempSubRows);
            }
          });
          this.pdfData.push({
            mainColumns: mainColumns,
            mainRows: mainRows,
            subColumns: subColumns,
            subRows: subRows,
          });
        });
      }

      // }
      // this.columns[0] = this.cols[0].header;
      // this.columns[1] = this.cols[1].header;
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    if (this.selectedReportView == 22) {
      this.translate
        .get(["LBLGRANDTOTAL", "LBLOBSERVERNAME", "LBLOBSCHECKLIST"])
        .subscribe((label) => {
          var jn = {};
          jn[label["LBLOBSERVERNAME"]] = label["LBLGRANDTOTAL"];
          jn[label["LBLOBSCHECKLIST"]] = this.grandTotal;
          json.push(jn);
        });
    }
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
    this.unFTableRows.pop();
  }

  exportData(): void {
    const fileName = "CHECKLISTCOUNT";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportsService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    if (this.selectedExport == 6) {
      this.exportAsExcelFile(this.unFTableRows, fileName);
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
    } else if (
      this.selectedExport == 1 ||
      this.selectedExport == 3 ||
      this.selectedExport == 4 ||
      this.selectedExport == 5
    ) {
      var EXCEL_EXTENSION;
      var EXCEL_TYPE;
      if (this.selectedExport == 1 || this.selectedExport == 5) {
        EXCEL_EXTENSION = ".xlsx";
        EXCEL_TYPE =
          "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";
      } else if (this.selectedExport == 3 || this.selectedExport == 4) {
        EXCEL_EXTENSION = ".xls";
        EXCEL_TYPE = "application/vnd.ms-excel;charset=utf-8";
      }
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      //Excel Title, Header, Data
      const header = this.columns;
      const data = this.tableRows;
      const unFColumns = this.unFColumns;
      const unFTableRows = this.unFTableRows;
      //const EXCEL_TYPE = "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

      //Create workbook and worksheet
      let workbook = new ExcelJS.Workbook();

      if (this.selectedReportView == 22) {
        // Report scope sheet
        let firstWorksheet = workbook.addWorksheet("Report Scope");
        firstWorksheet.addRow([]);
        let firstHeaderRow;
        this.translate.get("LBLDETAILS").subscribe((label) => {
          firstHeaderRow = firstWorksheet.addRow([label]);
        });

        firstWorksheet.getCell("A2").font = {
          name: "Arial Unicode MS",
          family: 4,
          size: 18,
          bold: true,
          color: { argb: "00000000" },
        };
        firstWorksheet.getCell("A2").alignment = {
          vertical: "middle",
          horizontal: "left",
          indent: 1,
        };
        firstWorksheet.getColumn("A").width = 100;
        firstWorksheet.getRow(2).height = 30;

        this.translate
          .get(["LBLOBSDATE", "LBLCREATEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              firstWorksheet.addRow([]);
              firstWorksheet.addRow([]);
              firstWorksheet
                .addRow([resLabel["LBLOBSDATE"]])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 14,
                    bold: true,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 2,
                  };
                });

              firstWorksheet.lastRow.height = 25;

              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              firstWorksheet
                .addRow([
                  fDate +
                    " - " +
                    tDate +
                    " (" +
                    this.auth.UserInfo["dateFormat"].toUpperCase() +
                    ")",
                ])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 12,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 3,
                  };
                });
            }

            if (this.parameters["CREATEDDATE"]) {
              firstWorksheet.addRow([]);
              firstWorksheet.addRow([]);
              firstWorksheet
                .addRow([resLabel["LBLCREATEDDATE"]])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    size: 14,
                    bold: true,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 2,
                  };
                });

              firstWorksheet.lastRow.height = 25;
              var bothDate = this.parameters["CREATEDDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              firstWorksheet
                .addRow([
                  fDate +
                    " - " +
                    tDate +
                    " (" +
                    this.auth.UserInfo["dateFormat"].toUpperCase() +
                    ")",
                ])
                .eachCell((cell) => {
                  cell.font = {
                    name: "Arial Unicode MS",
                    family: 4,
                    size: 12,
                    color: { argb: "00000000" },
                  };
                  cell.alignment = {
                    vertical: "middle",
                    horizontal: "left",
                    indent: 3,
                  };
                });
            }

            firstWorksheet.lastRow.height = 25;
          });
        let worksheet = workbook.addWorksheet("Sheet1");
        //Add Header Row
        let headerRow = worksheet.addRow(header);
        headerRow.height = 20;
        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
          cell.fill = {
            type: "pattern",
            pattern: "solid",
            fgColor: { argb: customColor },
          };
          cell.font = {
            color: { argb: customFontColor },
          };
          cell.border = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" },
          };
          if (number == 2) {
            cell.alignment = {
              vertical: "middle",
              horizontal: "center",
              wrapText: true,
            };
          } else {
            cell.alignment = {
              vertical: "middle",
              horizontal: "left",
              wrapText: true,
            };
          }
        });
        // Add Data and Conditional Formatting
        let sumOfData = 0;
        data.forEach((element) => {
          let eachRow = [];
          this.columns.map((headers, key) => {
            if (key == 1) {
              sumOfData = sumOfData + element[headers];
            }
            eachRow.push(element[headers]);
          });
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = {
                name: "Calibri",
                family: 4,
                size: 11,
                bold: false,
                strike: true,
              };
            });
          } else {
            var addedRow = worksheet.addRow(eachRow);
            addedRow.eachCell((cell, number) => {
              cell.alignment = {
                wrapText: true,
              };
            });
            addedRow.getCell(2).alignment = {
              vertical: "middle",
              horizontal: "center",
            };
            if (this.selectedExport == 5) {
              addedRow.getCell(2).numFmt = "0";
            }
          }
        });
        this.translate.get("LBLGRANDTOTAL").subscribe((label) => {
          let finalRow = worksheet.addRow([label, sumOfData]);
          finalRow.getCell(2).alignment = {
            vertical: "middle",
            horizontal: "center",
          };
          if (this.selectedExport == 5) {
            finalRow.getCell(2).numFmt = "0";
          }
          finalRow.eachCell((cell) => {
            cell.font = {
              size: 12,
              bold: true,
            };
            cell.border = {
              top: { style: "thin", color: { argb: customColor } },
              // left: { style: "thin", color: {argb: customColor} },
              bottom: { style: "thin", color: { argb: customColor } },
              // right: { style: "thin", color: {argb: customColor} },
            };
          });
        });

        worksheet.getColumn(1).width = 40;
        worksheet.getColumn(2).width = 30;
        worksheet.addRow([]);
        //Generate Excel File with given name
        workbook.xlsx.writeBuffer().then((data) => {
          let blob = new Blob([data], { type: EXCEL_TYPE });
          fs.saveAs(
            blob,
            fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
          );
        });
      } else if (this.selectedReportView == 28) {
        if (this.selectedExport == 4 || this.selectedExport == 5) {
          // Report scope sheet
          let firstWorksheet = workbook.addWorksheet("Report Scope");
          firstWorksheet.addRow([]);
          let firstHeaderRow;
          this.translate.get("LBLDETAILS").subscribe((label) => {
            firstHeaderRow = firstWorksheet.addRow([label]);
          });

          firstWorksheet.getCell("A2").font = {
            name: "Arial Unicode MS",
            family: 4,
            size: 18,
            bold: true,
            color: { argb: "00000000" },
          };
          firstWorksheet.getCell("A2").alignment = {
            vertical: "middle",
            horizontal: "left",
            indent: 1,
          };
          firstWorksheet.getColumn("A").width = 100;
          firstWorksheet.getRow(2).height = 30;

          this.translate
            .get(["LBLOBSDATE", "LBLCREATEDDATE"])
            .subscribe((resLabel) => {
              if (this.parameters["OBSDATE"]) {
                firstWorksheet.addRow([]);
                firstWorksheet.addRow([]);
                firstWorksheet
                  .addRow([resLabel["LBLOBSDATE"]])
                  .eachCell((cell) => {
                    cell.font = {
                      name: "Arial Unicode MS",
                      family: 4,
                      size: 14,
                      bold: true,
                      color: { argb: "00000000" },
                    };
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "left",
                      indent: 2,
                    };
                  });

                firstWorksheet.lastRow.height = 25;

                var bothDate = this.parameters["OBSDATE"].split("-");
                var fDate = formatDate(
                  bothDate[0],
                  this.auth.UserInfo["dateFormat"],
                  this.translate.getDefaultLang()
                );
                var tDate = formatDate(
                  bothDate[1],
                  this.auth.UserInfo["dateFormat"],
                  this.translate.getDefaultLang()
                );
                firstWorksheet
                  .addRow([
                    fDate +
                      " - " +
                      tDate +
                      " (" +
                      this.auth.UserInfo["dateFormat"].toUpperCase() +
                      ")",
                  ])
                  .eachCell((cell) => {
                    cell.font = {
                      name: "Arial Unicode MS",
                      family: 4,
                      size: 12,
                      color: { argb: "00000000" },
                    };
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "left",
                      indent: 3,
                    };
                  });
              }

              if (this.parameters["CREATEDDATE"]) {
                firstWorksheet.addRow([]);
                firstWorksheet.addRow([]);
                firstWorksheet
                  .addRow([resLabel["LBLCREATEDDATE"]])
                  .eachCell((cell) => {
                    cell.font = {
                      name: "Arial Unicode MS",
                      size: 14,
                      bold: true,
                      color: { argb: "00000000" },
                    };
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "left",
                      indent: 2,
                    };
                  });

                firstWorksheet.lastRow.height = 25;
                var bothDate = this.parameters["CREATEDDATE"].split("-");
                var fDate = formatDate(
                  bothDate[0],
                  this.auth.UserInfo["dateFormat"],
                  this.translate.getDefaultLang()
                );
                var tDate = formatDate(
                  bothDate[1],
                  this.auth.UserInfo["dateFormat"],
                  this.translate.getDefaultLang()
                );
                firstWorksheet
                  .addRow([
                    fDate +
                      " - " +
                      tDate +
                      " (" +
                      this.auth.UserInfo["dateFormat"].toUpperCase() +
                      ")",
                  ])
                  .eachCell((cell) => {
                    cell.font = {
                      name: "Arial Unicode MS",
                      family: 4,
                      size: 12,
                      color: { argb: "00000000" },
                    };
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "left",
                      indent: 3,
                    };
                  });
              }

              firstWorksheet.lastRow.height = 25;
            });

          // Report View sheet
          let worksheet = workbook.addWorksheet("Sheet1");
          let subHeader = unFColumns;
          var beforeDataCount = 1;

          for (var i = 1; i <= 10; i++) {
            worksheet.getColumn(i).width = 30;
          }

          let headerRow = worksheet.addRow(subHeader);
          // Cell Style : Fill and Border
          headerRow.eachCell((cell, number) => {
            cell.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: customColor },
            };
            cell.font = {
              color: { argb: customFontColor },
            };
            cell.border = {
              top: { style: "thin" },
              left: { style: "thin" },
              bottom: { style: "thin" },
              right: { style: "thin" },
            };
          });

          // Add Data and Conditional Formatting
          unFTableRows.map((element, index) => {
            let eachRow = [];
            unFColumns.map((headers, key) => {
              eachRow.push(element[headers]);
            });
            if (element.isDeleted === "Y") {
              let deletedRow = worksheet.addRow(eachRow);
              deletedRow.eachCell((cell, number) => {
                cell.font = {
                  name: "Calibri",
                  family: 4,
                  size: 11,
                  bold: false,
                  strike: true,
                };
                cell.border = {
                  bottom: { style: "medium", color: { argb: customColor } },
                };
              });
            } else {
              if (unFTableRows.length - 1 == index) {
                // worksheet.addRow(eachRow).border = {
                //   bottom: { style: "medium", color: { argb: customColor } },
                // };
                let addedRow = worksheet.addRow(eachRow);
                addedRow.eachCell({ includeEmpty: true }, (cell, number) => {
                  cell.border = {
                    bottom: { style: "medium", color: { argb: customColor } },
                  };
                  cell.alignment = {
                    wrapText: true,
                  };
                  cell.numFmt = this.auth.changeCellValueType(cell.value);
                });
              } else {
                worksheet.addRow(eachRow).eachCell((cell, number) => {
                  cell.alignment = {
                    wrapText: true,
                  };
                  cell.numFmt = this.auth.changeCellValueType(cell.value);
                });
              }
            }
          });

          worksheet.addRow([]);
          //Generate Excel File with given name
          workbook.xlsx.writeBuffer().then((data) => {
            let blob = new Blob([data], { type: EXCEL_TYPE });
            fs.saveAs(
              blob,
              fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
            );
          });
        } else {
          // Report scope sheet
          let firstWorksheet = workbook.addWorksheet("Report Scope");
          firstWorksheet.addRow([]);
          let firstHeaderRow;
          this.translate.get("LBLDETAILS").subscribe((label) => {
            firstHeaderRow = firstWorksheet.addRow([label]);
          });

          firstWorksheet.getCell("A2").font = {
            name: "Arial Unicode MS",
            family: 4,
            size: 18,
            bold: true,
            color: { argb: "00000000" },
          };
          firstWorksheet.getCell("A2").alignment = {
            vertical: "middle",
            horizontal: "left",
            indent: 1,
          };
          firstWorksheet.getColumn("A").width = 100;
          firstWorksheet.getRow(2).height = 30;

          this.translate
            .get(["LBLOBSDATE", "LBLCREATEDDATE"])
            .subscribe((resLabel) => {
              if (this.parameters["OBSDATE"]) {
                firstWorksheet.addRow([]);
                firstWorksheet.addRow([]);
                firstWorksheet
                  .addRow([resLabel["LBLOBSDATE"]])
                  .eachCell((cell) => {
                    cell.font = {
                      name: "Arial Unicode MS",
                      family: 4,
                      size: 14,
                      bold: true,
                      color: { argb: "00000000" },
                    };
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "left",
                      indent: 2,
                    };
                  });

                firstWorksheet.lastRow.height = 25;

                var bothDate = this.parameters["OBSDATE"].split("-");
                var fDate = formatDate(
                  bothDate[0],
                  this.auth.UserInfo["dateFormat"],
                  this.translate.getDefaultLang()
                );
                var tDate = formatDate(
                  bothDate[1],
                  this.auth.UserInfo["dateFormat"],
                  this.translate.getDefaultLang()
                );
                firstWorksheet
                  .addRow([
                    fDate +
                      " - " +
                      tDate +
                      " (" +
                      this.auth.UserInfo["dateFormat"].toUpperCase() +
                      ")",
                  ])
                  .eachCell((cell) => {
                    cell.font = {
                      name: "Arial Unicode MS",
                      family: 4,
                      size: 12,
                      color: { argb: "00000000" },
                    };
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "left",
                      indent: 3,
                    };
                  });
              }

              if (this.parameters["CREATEDDATE"]) {
                firstWorksheet.addRow([]);
                firstWorksheet.addRow([]);
                firstWorksheet
                  .addRow([resLabel["LBLCREATEDDATE"]])
                  .eachCell((cell) => {
                    cell.font = {
                      name: "Arial Unicode MS",
                      size: 14,
                      bold: true,
                      color: { argb: "00000000" },
                    };
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "left",
                      indent: 2,
                    };
                  });

                firstWorksheet.lastRow.height = 25;
                var bothDate = this.parameters["CREATEDDATE"].split("-");
                var fDate = formatDate(
                  bothDate[0],
                  this.auth.UserInfo["dateFormat"],
                  this.translate.getDefaultLang()
                );
                var tDate = formatDate(
                  bothDate[1],
                  this.auth.UserInfo["dateFormat"],
                  this.translate.getDefaultLang()
                );
                firstWorksheet
                  .addRow([
                    fDate +
                      " - " +
                      tDate +
                      " (" +
                      this.auth.UserInfo["dateFormat"].toUpperCase() +
                      ")",
                  ])
                  .eachCell((cell) => {
                    cell.font = {
                      name: "Arial Unicode MS",
                      family: 4,
                      size: 12,
                      color: { argb: "00000000" },
                    };
                    cell.alignment = {
                      vertical: "middle",
                      horizontal: "left",
                      indent: 3,
                    };
                  });
              }

              firstWorksheet.lastRow.height = 25;
            });

          // Report View sheet
          let worksheet = workbook.addWorksheet("Sheet1");
          let subHeader = header;
          var mainHeader = Object.keys(this.rowGroupMetadata);
          var beforeDataCount = 1;
          mainHeader.map((header, key) => {
            var rowsData = data.filter((item) => {
              return item[this.dataKey] == header;
            });
            for (var i = 1; i <= 10; i++) {
              worksheet.getColumn(i).width = 30;
            }
            var lastCol = "F";
            if (subHeader.length == 7) {
              lastCol = "G";
            } else if (subHeader.length == 8) {
              lastCol = "H";
            } else if (subHeader.length == 9) {
              lastCol = "I";
            } else if (subHeader.length == 10) {
              lastCol = "J";
            }
            if (key == 0) {
              worksheet.mergeCells(
                "A" + beforeDataCount + ":" + lastCol + beforeDataCount
              );
              worksheet.mergeCells(
                "A" +
                  (beforeDataCount + 1) +
                  ":" +
                  lastCol +
                  (beforeDataCount + 1)
              );
              this.translate.get("LBLOBSERVERNAME").subscribe((label) => {
                worksheet.getCell("A" + beforeDataCount).value = label;
              });
              this.translate.get(header).subscribe((label) => {
                worksheet.getCell("A" + (beforeDataCount + 1)).value = label;
              });
              worksheet.getCell("A" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("A" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              beforeDataCount = rowsData.length + 5;
            } else {
              worksheet.mergeCells(
                "A" + beforeDataCount + ":" + lastCol + beforeDataCount
              );
              worksheet.mergeCells(
                "A" +
                  (beforeDataCount + 1) +
                  ":" +
                  lastCol +
                  (beforeDataCount + 1)
              );
              this.translate.get("LBLOBSERVERNAME").subscribe((label) => {
                worksheet.getCell("A" + beforeDataCount).value = label;
              });
              this.translate.get(header).subscribe((label) => {
                worksheet.getCell("A" + (beforeDataCount + 1)).value = label;
              });
              worksheet.getCell("A" + beforeDataCount).fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: customColor },
              };
              worksheet.getCell("A" + beforeDataCount).font = {
                color: { argb: customFontColor },
              };
              beforeDataCount = rowsData.length + 4 + beforeDataCount;
            }

            let headerRow = worksheet.addRow(subHeader);
            // Cell Style : Fill and Border
            headerRow.eachCell((cell, number) => {
              cell.fill = {
                type: "pattern",
                pattern: "solid",
                fgColor: { argb: "EEECE1" },
              };
              cell.font = {
                color: { argb: "000000" },
              };
              cell.border = {
                top: { style: "thin" },
                left: { style: "thin" },
                bottom: { style: "thin" },
                right: { style: "thin" },
              };
            });

            // Add Data and Conditional Formatting
            rowsData.map((element, index) => {
              let eachRow = [];
              this.columns.map((headers, key) => {
                eachRow.push(element[headers]);
              });
              if (element.isDeleted === "Y") {
                let deletedRow = worksheet.addRow(eachRow);
                deletedRow.eachCell((cell, number) => {
                  cell.font = {
                    name: "Calibri",
                    family: 4,
                    size: 11,
                    bold: false,
                    strike: true,
                  };
                });
              } else {
                if (rowsData.length - 1 == index) {
                  let addedRow = worksheet.addRow(eachRow);
                  addedRow.eachCell({ includeEmpty: true }, (cell, number) => {
                    cell.border = {
                      bottom: { style: "medium", color: { argb: customColor } },
                    };
                    cell.alignment = {
                      wrapText: true,
                    };
                  });
                } else {
                  worksheet.addRow(eachRow).eachCell((cell, number) => {
                    cell.alignment = {
                      wrapText: true,
                    };
                  });
                }
              }
            });
          });

          worksheet.addRow([]);
          //Generate Excel File with given name
          workbook.xlsx.writeBuffer().then((data) => {
            let blob = new Blob([data], { type: EXCEL_TYPE });
            fs.saveAs(
              blob,
              fileName + "_" + new Date().getTime() + EXCEL_EXTENSION
            );
          });
        }
      }
    } else if (this.selectedExport == 2) {
      setTimeout(() => {
        this.selectedExport = 0;
      }, 100);
      var pdf = new jsPDF("landscape");
      pdf.addFileToVFS(
        "Trirong-Regular.ttf",
        pdfFonts.pdfMake.vfs["Trirong-Regular.ttf"]
      );
      pdf.addFont("Trirong-Regular.ttf", "Trirong-Regular", "normal");
      pdf.setFont("Trirong-Regular", "normal"); // set font
      // pdf.setLanguage("th");
      if (this.selectedReportView == 22) {
        pdf.text(this.title, 15, 20);
        let finalX = 10;
        this.translate
          .get(["LBLOBSDATE", "LBLCREATEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              let obsDate = [];
              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
                startY: finalX + 13,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }

            if (this.parameters["CREATEDDATE"]) {
              let obsDate = [];
              var bothDate = this.parameters["CREATEDDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLCREATEDDATE"]], obsDate, {
                startY: finalX,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }
            pdf.addPage();
          });

        // report view
        let finalY = 10;
        let fn = this;
        pdf.autoTable(this.columns, this.pdfData, {
          startY: 10,
          headStyles: {
            fillColor: customColor,
            textColor: customFontColor,
            font: "Trirong-Regular",
            fontStyle: "normal",
          },
          alternateRowStyles: {
            fillColor: "#FFFFFF",
          },
          willDrawCell: this.drawCell,
          didParseCell: (data) => {
            fn.alignCol(data, customColor, fn.pdfData.length);
          },
        });
        this.parameters[""];
        pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
      } else {
        // report scope
        pdf.text(this.title, 15, 20);
        let finalX = 10;
        this.translate
          .get(["LBLOBSDATE", "LBLCREATEDDATE"])
          .subscribe((resLabel) => {
            if (this.parameters["OBSDATE"]) {
              let obsDate = [];
              var bothDate = this.parameters["OBSDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLOBSDATE"]], obsDate, {
                startY: finalX + 13,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }

            if (this.parameters["CREATEDDATE"]) {
              let obsDate = [];
              var bothDate = this.parameters["CREATEDDATE"].split("-");
              var fDate = formatDate(
                bothDate[0],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              var tDate = formatDate(
                bothDate[1],
                this.auth.UserInfo["dateFormat"],
                this.translate.getDefaultLang()
              );
              obsDate.push([
                fDate +
                  " - " +
                  tDate +
                  " (" +
                  this.auth.UserInfo["dateFormat"].toUpperCase() +
                  ")",
              ]);
              pdf.autoTable([resLabel["LBLCREATEDDATE"]], obsDate, {
                startY: finalX,
                headStyles: {
                  fillColor: customColor,
                  textColor: customFontColor,
                },
                alternateRowStyles: {
                  fillColor: "#FFFFFF",
                },
              });
              finalX = pdf.previousAutoTable.finalX;
            }
            pdf.addPage();
          });

        // report view
        let finalY = 10;
        this.pdfData.map((data) => {
          pdf.autoTable(data.mainColumns, data.mainRows, {
            startY: finalY,
            headStyles: {
              fillColor: customColor,
              textColor: customFontColor,
            },
            alternateRowStyles: {
              fillColor: "#FFFFFF",
            },
          });
          finalY = pdf.previousAutoTable.finalY;
          pdf.autoTable(data.subColumns, data.subRows, {
            startY: finalY,
            headStyles: {
              fillColor: "#D8D8D8",
              textColor: "#000000",
            },
            alternateRowStyles: {
              fillColor: "#FFFFFF",
            },
          });
          finalY = pdf.previousAutoTable.finalY + 10;
        });
        pdf.save(fileName + "_" + new Date().getTime() + ".pdf");
      }
    }
  }

  alignCol = function (data, customColor, rowCount) {
    if (data.column.index == 1) {
      data.cell.styles.halign = "center";
    }
    var s = data.cell.styles;
    if (data.row.index == rowCount - 1) {
      s.lineColor = "#" + customColor;
      s.lineWidth = 0.5;
      s.borders = "t";
    }
  };

  drawCell = function (data) {
    var doc = data.doc;
    var rows = data.table.body;
    var col = data.column.index;
    if (col == 2) {
      data.cell.styles.halign = "center";
    }
    if (rows.length === 1) {
    } else if (data.row.index === rows.length - 1) {
      doc.setFontStyle("bold");
      doc.setFontSize("12");
    }
  };
}
