import {
  Component,
  OnInit,
  Inject,
  PLATFORM_ID,
  InjectionToken,
  ElementRef,
  ViewChild,
} from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";
import { AnalyticsDashboardService } from "../../_services/analytics-dashboard.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { TranslateService } from "@ngx-translate/core";
import { EnvService } from "src/env.service";
import { CookieService } from "ngx-cookie-service";
import { DOCUMENT, isPlatformBrowser } from "@angular/common";
import { DomSanitizer } from "@angular/platform-browser";

declare var tableau: any;

@Component({
  selector: "app-analytics-dashboard",
  templateUrl: "./analytics-dashboard.component.html",
  styleUrls: ["./analytics-dashboard.component.css"],
})
export class AnalyticsDashboardComponent implements OnInit {
  viz: any;
  formData: any;
  tableauTicket: any;
  t1: boolean = false;
  t2: boolean = false;
  noRecord: any;
  isBrowser: any;
  baseUrl: any = "";
  cookieError: boolean = false;
  cookieMsg: any = "";

  @ViewChild("iframe") iframe: ElementRef;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private analyticsDashboardService: AnalyticsDashboardService,
    private auth: AuthenticationService,
    private translate: TranslateService,
    private env: EnvService,
    private cookieService: CookieService,
    @Inject(DOCUMENT) private document: any,
    @Inject(PLATFORM_ID) private platformId: InjectionToken<Object>,
    private sanitizer: DomSanitizer
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT" },
      {
        label: "LBLANALYTICS",
        routerLink: ["/analytics-dashboard"],
      },
    ]);
    // this.isBrowser = isPlatformBrowser(this.platformId);
  }

  ngAfterViewInit() {
    // this.baseUrl = this.env.apiUrl.replace("/api", "/APPMOD/setCookie.html");
    this.iframe.nativeElement.setAttribute(
      "src",
      this.env.cookieUrl + "/checkCookie.html"
    );
  }

  ngOnInit() {
    var i = 0;
    var fn = this;
    window.addEventListener(
      "message",
      (evt) => {
        if (evt.data === "MM:3PCunsupported" && i == 0) {
          this.cookieError = true;
          this.cookieMsg =
            "Third-party cookies are blocked on your browser. Please whitelist " +
            fn.env.analyticDashboardUrl +
            " at chrome://settings/content/cookies?search=cookie to see the dashboard content.";
          // alert(
          //   "Third-party cookies are blocked on your browser. Please whitelist " +
          //     fn.env.analyticDashboardUrl +
          //     " at chrome://settings/content/cookies?search=cookie to see the dashboard content."
          // );
          i = 1;
        } else if (evt.data === "MM:3PCsupported") {
          var userEmail = this.auth.UserInfo["email"];
          this.analyticsDashboardService
            .getTableauTicket({
              username: userEmail,
              target_site: "company_" + this.auth.UserInfo["companyId"],
            })
            .subscribe((res) => {
              this.tableauTicket = res;
              var containerDiv = document.getElementById("vizContainer");
              var containerIframe = document.getElementById("checkIframe");

              if (this.tableauTicket != "") {
                if (this.tableauTicket == -1) {
                  this.noRecord =
                    "User unauthorized to access dashboard. Please contact administrator. If you are just added to this application, you will be able to see the dashboard in 2 hours as we are preparing the dashboard for your login.";
                } else {
                  var companyID = this.auth.UserInfo["companyId"];
                  var url =
                    this.env.analyticDashboardUrl +
                    "/trusted/" +
                    this.tableauTicket +
                    "/t/company_" +
                    companyID +
                    "/views/company_" +
                    companyID +
                    "/OVERVIEW?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no&:origin=viz_share_link";
                  // this.refreshThatSucker();
                  var fn = this;
                  var options = {
                    hideToolbar: true,
                    onFirstInteractive: function () {
                      // setInterval(() => {
                      //   fn.refreshThatSucker();
                      // }, 20000);
                    },
                  };
                  this.viz = new tableau.Viz(containerDiv, url, options);
                }
              }
            });
        }
      },
      false
    );
  }

  refreshThatSucker() {
    console.log("Iframe Refreshed");
    this.viz.refreshDataAsync();
  }
}
