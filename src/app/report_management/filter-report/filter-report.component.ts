import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "../../breadcrumb.service";

import { ConfirmationService, MessageService } from "primeng/api";
import { ActivatedRoute, Router } from "@angular/router";
import { ReportsService } from "src/app/_services/reports.service";
import { TranslateService } from "@ngx-translate/core";
import { formatDate } from "@angular/common";
import { isArray } from "util";
import { CookieService } from "ngx-cookie-service";

import * as fs from "file-saver";
declare const ExcelJS: any;
import * as XLSX from "xlsx";

import * as jsPDF from "jspdf";
import * as excelColumnName from "excel-column-name";
import "jspdf-autotable";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { GlobalDataService } from "src/app/_services/global-data.service";

@Component({
  selector: "app-filter-report",
  templateUrl: "./filter-report.component.html",
  styleUrls: ["./filter-report.component.css"],
  providers: [ConfirmationService, MessageService],
})
export class FilterReportComponent implements OnInit {
  //  Variable Declaration
  iconType = "ui-icon-add";
  show_advance_search: boolean = false;
  reportId: any;
  isLoaded: boolean;
  savedFilterList: any = [];
  selectedSavedFilter: any = 0;
  filterList: any;
  reportDetails: any;
  previousMonth: any = [];
  selectedFilter: any = [];
  filterNameEmpty: boolean;
  createNewFilterPopup: boolean;
  filterName: any = "";
  oldFilterName: any = "";
  filterDateList: any = [];
  tempSelectedReportParam: any;
  tempSelectedFilterName: any;
  datePopup: boolean;
  obsDateCheck: any = "YTD";
  dateSelectedData: any = [];
  filterSetting: any;
  selectMonth: any = 1;
  includeToday: boolean;
  rangeDate: any;
  oldFromDate: any;
  oldToDate: any;
  currDate: any = new Date();
  updateFilterPopup: any;
  filterOptional: any;
  reportDesignId: any;
  filterLabel: any;
  mandatoryCriteria: any = [];
  filterGroupBy: any = [];
  parameters: any;
  filterCategory: any = [];
  detaDumpDatePopup: boolean;
  fromMonth: any;
  toMonth: any;
  filterChartType: any = [];
  categoryData: any = [];
  obsData: any = [];
  exportList: any = [];
  selectedExport: any;
  tableRows: any;
  columns: any;
  pdfRows: any = [];
  cols: any = [];
  mainHead: any;
  rowGroupMetadata: any;
  helpFileUrl: string;
  fromDate: any;
  toDate: any;
  yearList: any;
  keys: any;
  selectedDateFilters: boolean = false;
  dateFormat: any;
  oldFromMonth: any = "";
  oldToMonth: any = "";
  viewFilters: boolean;
  oldObsDateCheck: any;
  months: any;
  years: any;
  trendFromMonth: any;
  trendFromMonthString: any;
  trendFromYear: any;
  trendToMonth: any;
  trendToMonthString: any;
  trendToYear: any;
  oldTrendFromMonth: any;
  oldTrendFromYear: any;
  oldTrendToMonth: any;
  oldTrendToYear: any;
  dumpParam: any;
  dateFormatType: any;
  filterGroups: any;
  subAreaOption: number;
  saveFilterDisabled: boolean = false;
  locale: any;
  // Breadcrumb Service
  constructor(
    private breadcrumbService: BreadcrumbService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private activatedRoute: ActivatedRoute,
    private reportService: ReportsService,
    private translate: TranslateService,
    private cookieService: CookieService,
    private router: Router,
    private auth: AuthenticationService,
    private globalData: GlobalDataService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLREPORTMGMT" },
      { label: "LBLREPORTS", routerLink: ["/reports"] },
    ]);
  }

  ngOnInit() {
    this.locale = this.auth.calLang();
    this.reportId = this.activatedRoute.snapshot.paramMap.get("reportId");
    if (this.reportId == 19) {
      this.obsDateCheck = "CMY";
    }
    this.changeHelpFileURl(this.reportId);
    var params;
    if (
      this.reportService.FilterInfo &&
      this.reportService.FilterInfo["reportId"] == this.reportId
    ) {
      this.parameters = this.reportService.FilterInfo;
      if (this.parameters["filterCriteria"] > 0) {
        this.selectedSavedFilter = this.parameters["filterCriteria"];
        params = {
          reportId: this.reportId,
          filterCriteriaId: this.parameters["filterCriteria"],
        };
      } else {
        params = {
          reportId: this.reportId,
          filterCriteriaId: 0,
        };
      }
      if (this.reportId == 17) {
        if (this.parameters) {
          var value = this.parameters["OBSDATE"];
          var newValue = value.split("-");
          var fromDate = new Date(newValue[0]);
          var toDate = new Date(newValue[1]);
          this.trendFromMonth = fromDate.getMonth() + 1;
          this.trendFromYear = fromDate.getFullYear();
          this.trendToMonth = toDate.getMonth() + 1;
          this.trendToYear = toDate.getFullYear();
          this.oldTrendFromMonth = fromDate.getMonth() + 1;
          this.oldTrendFromYear = fromDate.getFullYear();
          this.oldTrendToMonth = toDate.getMonth() + 1;
          this.oldTrendToYear = toDate.getFullYear();

          if (this.trendFromMonth == 1) {
            this.trendFromMonthString = "FMKJANUARY";
          } else if (this.trendFromMonth == 2) {
            this.trendFromMonthString = "FMKFEBRUARY";
          } else if (this.trendFromMonth == 3) {
            this.trendFromMonthString = "FMKMARCH";
          } else if (this.trendFromMonth == 4) {
            this.trendFromMonthString = "FMKAPRIL";
          } else if (this.trendFromMonth == 5) {
            this.trendFromMonthString = "FMKMAY";
          } else if (this.trendFromMonth == 6) {
            this.trendFromMonthString = "FMKJUNE";
          } else if (this.trendFromMonth == 7) {
            this.trendFromMonthString = "FMKJULY";
          } else if (this.trendFromMonth == 8) {
            this.trendFromMonthString = "FMKAUGUST";
          } else if (this.trendFromMonth == 9) {
            this.trendFromMonthString = "FMKSEPTEMBER";
          } else if (this.trendFromMonth == 10) {
            this.trendFromMonthString = "FMKOCTOBER";
          } else if (this.trendFromMonth == 11) {
            this.trendFromMonthString = "FMKNOVEMBER";
          } else if (this.trendFromMonth == 12) {
            this.trendFromMonthString = "FMKDECEMBER";
          }

          if (this.trendToMonth == 1) {
            this.trendToMonthString = "FMKJANUARY";
          } else if (this.trendToMonth == 2) {
            this.trendToMonthString = "FMKFEBRUARY";
          } else if (this.trendToMonth == 3) {
            this.trendToMonthString = "FMKMARCH";
          } else if (this.trendToMonth == 4) {
            this.trendToMonthString = "FMKAPRIL";
          } else if (this.trendToMonth == 5) {
            this.trendToMonthString = "FMKMAY";
          } else if (this.trendToMonth == 6) {
            this.trendToMonthString = "FMKJUNE";
          } else if (this.trendToMonth == 7) {
            this.trendToMonthString = "FMKJULY";
          } else if (this.trendToMonth == 8) {
            this.trendToMonthString = "FMKAUGUST";
          } else if (this.trendToMonth == 9) {
            this.trendToMonthString = "FMKSEPTEMBER";
          } else if (this.trendToMonth == 10) {
            this.trendToMonthString = "FMKOCTOBER";
          } else if (this.trendToMonth == 11) {
            this.trendToMonthString = "FMKNOVEMBER";
          } else if (this.trendToMonth == 12) {
            this.trendToMonthString = "FMKDECEMBER";
          }
        }
      }

      // if (this.parameters["OBSDATE_date"]) {
      //   var newValue = this.parameters["OBSDATE_date"].split("-");

      //   if (newValue[0].indexOf("/") != -1) {
      //     this.includeToday = false;
      //     this.selectMonth = 1;
      //     this.rangeDate = [];
      //     this.fromDate = new Date(newValue[0]);
      //     this.toDate = new Date(newValue[1]);
      //     this.oldFromDate = this.fromDate;
      //     this.oldToDate = this.toDate;
      //   } else {
      //     this.rangeDate = "";
      //     this.oldFromDate = "";
      //     this.oldToDate = "";
      //     this.selectMonth = parseInt(newValue[0]);
      //     if (newValue[1] == "1") {
      //       this.includeToday = true;
      //     } else {
      //       this.includeToday = false;
      //     }
      //   }
      // }
      this.selectedDateFilters = true;
    } else {
      params = {
        reportId: this.reportId,
        filterCriteriaId: 0,
      };
      this.selectedDateFilters = true;
    }

    this.translate
      .get([
        "FMKJANUARY",
        "FMKFEBRUARY",
        "FMKMARCH",
        "FMKAPRIL",
        "FMKMAY",
        "FMKJUNE",
        "FMKJULY",
        "FMKAUGUST",
        "FMKSEPTEMBER",
        "FMKOCTOBER",
        "FMKNOVEMBER",
        "FMKDECEMBER",
      ])
      .subscribe((resLabel) => {
        this.months = [
          { label: resLabel["FMKJANUARY"], value: 1 },
          { label: resLabel["FMKFEBRUARY"], value: 2 },
          { label: resLabel["FMKMARCH"], value: 3 },
          { label: resLabel["FMKAPRIL"], value: 4 },
          { label: resLabel["FMKMAY"], value: 5 },
          { label: resLabel["FMKJUNE"], value: 6 },
          { label: resLabel["FMKJULY"], value: 7 },
          { label: resLabel["FMKAUGUST"], value: 8 },
          { label: resLabel["FMKSEPTEMBER"], value: 9 },
          { label: resLabel["FMKOCTOBER"], value: 10 },
          { label: resLabel["FMKNOVEMBER"], value: 11 },
          { label: resLabel["FMKDECEMBER"], value: 12 },
        ];
      });

    this.getFilters(params);

    this.previousMonth = [];
    if (this.reportId != 19 && this.reportId != 4) {
      for (var i = 1; i <= 30; i++) {
        this.previousMonth.push({ label: i.toString(), value: i });
      }
    } else {
      for (var i = 1; i <= 12; i++) {
        this.previousMonth.push({ label: i.toString(), value: i });
      }
    }

    //Year Rang
    var previous_year = this.currDate.getFullYear() - 10;
    var next_year = this.currDate.getFullYear() + 10;
    this.yearList = previous_year + ":" + next_year;

    this.years = [];
    for (previous_year; previous_year <= next_year; previous_year++) {
      this.years.push({ label: previous_year, value: previous_year });
    }

    this.dateFormatType = this.auth.UserInfo["dateFormat"];
    this.dateFormat = this.auth.UserInfo["dateFormat"].toLowerCase();
    this.dateFormat = this.dateFormat.replace("yyyy", "yy");
    // if (this.dateFormatType == "DD/MM/YYYY") {
    //   this.dateFormat = "dd/mm/yy";
    // } else if (this.dateFormatType == "MM/DD/YYYY") {
    //   this.dateFormat = "mm/dd/yy";
    // } else {
    //   this.dateFormat = "yy/mm/dd";
    // }
    // this.dateFormat = "mm/dd/yy";
  }

  changeHelpFileURl(id) {
    const switchID = +id;
    switch (switchID) {
      case 14:
        this.breadcrumbService.setItems([
          {
            label: "LBLREPORTMGMT",
            url: "./assets/help/reports-checklist-count.md",
          },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 13:
        this.breadcrumbService.setItems([
          { label: "LBLREPORTMGMT", url: "./assets/help/category-comments.md" },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 16:
        this.breadcrumbService.setItems([
          {
            label: "LBLREPORTMGMT",
            url: "./assets/help/reports-checklist-count-summary.md",
          },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 10:
        this.breadcrumbService.setItems([
          {
            label: "LBLREPORTMGMT",
            url: "./assets/help/report-by-category.md",
          },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 15:
        this.breadcrumbService.setItems([
          {
            label: "LBLREPORTMGMT",
            url: "./assets/help/corrective-action-report.md",
          },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 9:
        this.breadcrumbService.setItems([
          { label: "LBLREPORTMGMT", url: "./assets/help/report-by-metrics.md" },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 21:
        this.breadcrumbService.setItems([
          { label: "LBLREPORTMGMT", url: "./assets/help/data-dump.md" },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 5:
        this.breadcrumbService.setItems([
          { label: "LBLREPORTMGMT", url: "./assets/help/index-report.md" },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 7:
        this.breadcrumbService.setItems([
          {
            label: "LBLREPORTMGMT",
            url: "./assets/help/site-observation-stats.md",
          },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 4:
        this.breadcrumbService.setItems([
          {
            label: "LBLREPORTMGMT",
            url: "./assets/help/monthly-safe-unsafe.md",
          },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 3:
        this.breadcrumbService.setItems([
          { label: "LBLREPORTMGMT", url: "./assets/help/sites-report.md" },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 11:
        this.breadcrumbService.setItems([
          { label: "LBLREPORTMGMT", url: "./assets/help/pareto-chart.md" },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 6:
        this.breadcrumbService.setItems([
          {
            label: "LBLREPORTMGMT",
            url: "./assets/help/target-activity-monthly.md",
          },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 8:
        this.breadcrumbService.setItems([
          { label: "LBLREPORTMGMT", url: "./assets/help/red-flag.md" },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 19:
        this.breadcrumbService.setItems([
          {
            label: "LBLREPORTMGMT",
            url: "./assets/help/target-activity-weekly.md",
          },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 20:
        this.breadcrumbService.setItems([
          { label: "LBLREPORTMGMT", url: "./assets/help/stats-by-category.md" },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 22:
        this.breadcrumbService.setItems([
          {
            label: "LBLREPORTMGMT",
            url: "./assets/help/corrective-actions-coverage-report.md",
          },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 17:
        this.breadcrumbService.setItems([
          { label: "LBLREPORTMGMT", url: "./assets/help/trend-line-report.md" },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
      case 18:
        this.breadcrumbService.setItems([
          {
            label: "LBLREPORTMGMT",
            url: "./assets/help/user-observer-list.md",
          },
          { label: "LBLREPORTS", routerLink: ["/reports"] },
        ]);
        break;
    }
  }

  getFilters(params) {
    this.reportService.getFiltersData(params).subscribe((res) => {
      this.reportDetails = res["reportDetail"];
      this.filterList = res["filterList"];
      this.filterDateList = res["filterDate"];
      this.filterOptional = res["filterOptional"];
      this.savedFilterList = res["savedFilter"];
      this.filterSetting = res["filterSetting"];
      this.reportDesignId = res["reportDesignId"];
      this.filterLabel = res["filterLabel"];
      this.filterGroups = res["filterGroup"];
      this.filterGroupBy = res["filterGroupBy"];
      this.filterCategory = res["filterCategories"];
      this.filterChartType = res["filterChartType"];
      var mandatoryCriteria = this.filterLabel.filter((item) => {
        return item.MANDATORYFILTER == 1;
      });
      mandatoryCriteria.map((item) => {
        var dateParam = [];
        this.filterDateList.filter((data) => {
          if (item.OPTIONSCATEGORYID == data.OPTIONSCATEGORYID) {
            dateParam.push(data.REPORTPARAM);
            this.mandatoryCriteria["filterDateList"] = dateParam;
          }
        });
        if (this.filterGroupBy.length > 0) {
          var groupParam = [];
          this.filterGroupBy.filter((data) => {
            if (item.OPTIONSCATEGORYID == data.OPTIONSCATEGORYID) {
              groupParam.push(data.REPORTPARAM);
              this.mandatoryCriteria["filterGroupBy"] = groupParam;
            }
          });
        }

        if (this.filterCategory.length > 0) {
          var groupParam = [];
          this.filterCategory.filter((data) => {
            if (item.OPTIONSCATEGORYID == data.OPTIONSCATEGORYID) {
              groupParam.push(data.REPORTPARAM);
              this.mandatoryCriteria["filterCategory"] = groupParam;
            }
          });
        }

        if (this.filterChartType.length > 0) {
          var groupParam = [];
          this.filterChartType.filter((data) => {
            if (item.OPTIONSCATEGORYID == data.OPTIONSCATEGORYID) {
              groupParam.push(data.REPORTPARAM);
              this.mandatoryCriteria["filterChartType"] = groupParam;
            }
          });
        }
      });
      this.filterList.map((item) => {
        if (item.Data) {
          item.Data = JSON.parse(item.Data);
          var tempData = [];
          item.Data.map((data) => {
            var keys = Object.keys(data);
            tempData.push({ label: data[keys[1]], value: data[keys[0]] });
          });
          item.Data = tempData;
        }
        if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
          this.selectedFilter[item.REPORTPARAM] = this.filterSetting[
            item.REPORTPARAM
          ]
            .split(",")
            .map(Number);

          item.Data = this.auth.rearrangeSelects(
            item.Data,
            this.selectedFilter[item.REPORTPARAM]
          );
        }
      });

      this.filterGroups.map((item) => {
        if (item.Data) {
          item.Data = JSON.parse(item.Data);
          var tempData = [];
          item.Data.map((data) => {
            var keys = Object.keys(data);
            tempData.push({ label: data[keys[1]], value: data[keys[0]] });
          });
          item.Data = tempData;
        }
        if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
          this.selectedFilter[item.REPORTPARAM] = this.filterSetting[
            item.REPORTPARAM
          ]
            .split(",")
            .map(Number);
          item.Data = this.auth.rearrangeSelects(
            item.Data,
            this.selectedFilter[item.REPORTPARAM]
          );
        }
      });

      this.filterGroupBy.map((item) => {
        if (item.Data) {
          item.Data = JSON.parse(item.Data);
          var tempData = [];
          item.Data.map((data) => {
            var keys = Object.keys(data);
            this.translate.get(data[keys[1]]).subscribe((label) => {
              tempData.push({ label: label, value: data[keys[0]] });
            });
          });
          item.Data = tempData;
        }
        if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
          this.selectedFilter[item.REPORTPARAM] = parseInt(
            this.filterSetting[item.REPORTPARAM]
          );
          // item.Data=this.auth.rearrangeSelects(item.Data,this.selectedFilter[item.REPORTPARAM]);
        }
      });

      this.filterCategory.map((item) => {
        if (item.Data) {
          item.Data = JSON.parse(item.Data);
          var tempData = [];
          item.Data.map((data) => {
            var keys = Object.keys(data);
            tempData.push({ label: data[keys[1]], value: data[keys[0]] });
          });
          item.Data = tempData;
        }

        if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
          if (this.reportId != 21) {
            this.selectedFilter[item.REPORTPARAM] = this.filterSetting[
              item.REPORTPARAM
            ]
              .split(",")
              .map(Number);
            // item.Data=this.auth.rearrangeSelects(item.Data,this.selectedFilter[item.REPORTPARAM]);
          } else {
            this.selectedFilter[item.REPORTPARAM] = this.filterSetting[
              item.REPORTPARAM
            ]
              .split(",")
              .map(Number)[0];
            // item.Data = this.auth.rearrangeSelects(
            //   item.Data,
            //   this.selectedFilter[item.REPORTPARAM]
            // );
          }
        }
      });

      this.filterChartType.map((item) => {
        if (item.Data) {
          item.Data = JSON.parse(item.Data);
          var tempData = [];
          item.Data.map((data) => {
            var keys = Object.keys(data);
            this.translate.get(data[keys[1]]).subscribe((label) => {
              tempData.push({ label: label, value: data[keys[0]] });
            });
          });
          item.Data = tempData;
        }
        if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
          this.selectedFilter[item.REPORTPARAM] = parseInt(
            this.filterSetting[item.REPORTPARAM]
          );
          item.Data = this.auth.rearrangeSelects(item.Data, [
            this.selectedFilter[item.REPORTPARAM],
          ]);
        }
      });

      this.filterOptional.map((item) => {
        if (item.Data) {
          item.Data = JSON.parse(item.Data);
          var tempData = [];
          item.Data.map((data) => {
            var keys = Object.keys(data);
            this.translate.get(data[keys[1]]).subscribe((label) => {
              tempData.push({ label: label, value: data[keys[0]] });
            });
          });
          item.Data = tempData;
        }
        if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
          this.selectedFilter[item.REPORTPARAM] = this.filterSetting[
            item.REPORTPARAM
          ]
            .split(",")
            .map(Number);
          item.Data = this.auth.rearrangeSelects(
            item.Data,
            this.selectedFilter[item.REPORTPARAM]
          );
        }
      });

      if (this.filterDateList.length > 0) {
        this.dateSelectedData = [];
        this.filterDateList.map((item, i) => {
          if (this.filterSetting && this.filterSetting[item.REPORTPARAM]) {
            var value = "";
            var obsDateCheck;
            if (
              this.filterSetting[item.REPORTPARAM] != "YTD" &&
              this.filterSetting[item.REPORTPARAM] != "CMY"
            ) {
              value = this.filterSetting[item.REPORTPARAM];
              var newValue = value.split("-");
              if (newValue[0].indexOf("/") != -1) {
                obsDateCheck = "FTDATE";
              } else {
                obsDateCheck = "PRVMONTH";
              }
            } else {
              obsDateCheck = this.filterSetting[item.REPORTPARAM];
              value = this.filterSetting[item.REPORTPARAM];
            }
            if (obsDateCheck == "PRVMONTH") {
              this.dateSelectedData.push({
                reportParam: item.REPORTPARAM,
                obsDateCheck: obsDateCheck,
                value: value,
                month: newValue[0],
                includeToday: newValue[1],
              });
            } else {
              this.dateSelectedData.push({
                reportParam: item.REPORTPARAM,
                obsDateCheck: obsDateCheck,
                value: value,
              });
            }
            this.selectedFilter[item.REPORTPARAM] = value;
            if (this.reportId == 17) {
              var dateArr = value.split("-");
              this.trendFromMonth = new Date(dateArr[0]).getMonth() + 1;
              this.trendFromYear = new Date(dateArr[0]).getFullYear();
              this.trendToMonth = new Date(dateArr[1]).getMonth() + 1;
              this.trendToYear = new Date(dateArr[1]).getFullYear();
              if (this.trendFromMonth == 1) {
                this.trendFromMonthString = "FMKJANUARY";
              } else if (this.trendFromMonth == 2) {
                this.trendFromMonthString = "FMKFEBRUARY";
              } else if (this.trendFromMonth == 3) {
                this.trendFromMonthString = "FMKMARCH";
              } else if (this.trendFromMonth == 4) {
                this.trendFromMonthString = "FMKAPRIL";
              } else if (this.trendFromMonth == 5) {
                this.trendFromMonthString = "FMKMAY";
              } else if (this.trendFromMonth == 6) {
                this.trendFromMonthString = "FMKJUNE";
              } else if (this.trendFromMonth == 7) {
                this.trendFromMonthString = "FMKJULY";
              } else if (this.trendFromMonth == 8) {
                this.trendFromMonthString = "FMKAUGUST";
              } else if (this.trendFromMonth == 9) {
                this.trendFromMonthString = "FMKSEPTEMBER";
              } else if (this.trendFromMonth == 10) {
                this.trendFromMonthString = "FMKOCTOBER";
              } else if (this.trendFromMonth == 11) {
                this.trendFromMonthString = "FMKNOVEMBER";
              } else if (this.trendFromMonth == 12) {
                this.trendFromMonthString = "FMKDECEMBER";
              }

              if (this.trendToMonth == 1) {
                this.trendToMonthString = "FMKJANUARY";
              } else if (this.trendToMonth == 2) {
                this.trendToMonthString = "FMKFEBRUARY";
              } else if (this.trendToMonth == 3) {
                this.trendToMonthString = "FMKMARCH";
              } else if (this.trendToMonth == 4) {
                this.trendToMonthString = "FMKAPRIL";
              } else if (this.trendToMonth == 5) {
                this.trendToMonthString = "FMKMAY";
              } else if (this.trendToMonth == 6) {
                this.trendToMonthString = "FMKJUNE";
              } else if (this.trendToMonth == 7) {
                this.trendToMonthString = "FMKJULY";
              } else if (this.trendToMonth == 8) {
                this.trendToMonthString = "FMKAUGUST";
              } else if (this.trendToMonth == 9) {
                this.trendToMonthString = "FMKSEPTEMBER";
              } else if (this.trendToMonth == 10) {
                this.trendToMonthString = "FMKOCTOBER";
              } else if (this.trendToMonth == 11) {
                this.trendToMonthString = "FMKNOVEMBER";
              } else if (this.trendToMonth == 12) {
                this.trendToMonthString = "FMKDECEMBER";
              }
            }
            // var cDate = new Date();
            // var cMonth = cDate.getMonth() + 1;
            // var cYear = cDate.getFullYear();
            // var formatedDate = formatDate(
            //   cDate,
            //   this.dateFormatType,
            //   this.translate.getDefaultLang()
            // );
            // var fromDate;
            // var toDate;
            // if (obsDateCheck == "YTD") {
            //   fromDate = "01/01/" + cYear;
            //   toDate = formatedDate;
            // } else if (obsDateCheck == "CMY") {
            //   fromDate = cMonth + "/01/" + cYear;
            //   toDate = formatedDate;
            // } else if (value.indexOf("/") == -1) {
            //   var newMonth = cMonth - this.selectMonth;
            //   var previousDate = new Date(cDate.setMonth(newMonth));
            //   var previousMonth = new Date(cDate.setMonth(cMonth - 1));
            //   var previousMonthDays = new Date(
            //     cYear,
            //     previousMonth.getMonth(),
            //     0
            //   ).getDate();
            //   fromDate = formatDate(
            //     previousDate,
            //     this.dateFormatType,
            //     this.translate.getDefaultLang()
            //   );
            //   if (this.includeToday == 1) {
            //     toDate = formatedDate;
            //   } else {
            //     toDate =
            //       previousMonth.getMonth() +
            //       "/" +
            //       previousMonthDays +
            //       "/" +
            //       cYear;
            //   }
            // } else {
            //   fromDate = formatDate(
            //     this.rangeDate[0],
            //     this.dateFormatType,
            //     this.translate.getDefaultLang()
            //   );

            //   toDate = formatDate(
            //     this.rangeDate[1],
            //     this.dateFormatType,
            //     this.translate.getDefaultLang()
            //   );
            // }
            // this.selectedFilter[item.REPORTPARAM] = fromDate + "-" + toDate;
          } else if (this.parameters && this.parameters[item.REPORTPARAM]) {
            var value = "";
            var obsDateCheck;
            if (
              this.parameters[item.REPORTPARAM + "_date"] != "YTD" &&
              this.parameters[item.REPORTPARAM + "_date"] != "CMY"
            ) {
              value = this.parameters[item.REPORTPARAM + "_date"];
              var newValue = value.split("-");
              if (newValue[0].indexOf("/") != -1) {
                obsDateCheck = "FTDATE";
              } else {
                obsDateCheck = "PRVMONTH";
              }
            } else {
              obsDateCheck = this.parameters[item.REPORTPARAM + "_date"];
              value = this.parameters[item.REPORTPARAM + "_date"];
            }
            if (obsDateCheck == "PRVMONTH") {
              this.dateSelectedData.push({
                reportParam: item.REPORTPARAM,
                obsDateCheck: obsDateCheck,
                value: value,
                month: newValue[0],
                includeToday: newValue[1],
              });
            } else {
              this.dateSelectedData.push({
                reportParam: item.REPORTPARAM,
                obsDateCheck: obsDateCheck,
                value: value,
              });
            }
          } else {
            this.dateSelectedData.push({
              reportParam: item.REPORTPARAM,
              obsDateCheck: this.obsDateCheck,
              value: this.obsDateCheck,
            });
          }
        });
      }
      if (this.parameters && this.parameters["filterCriteria"] == 0) {
        var keys = Object.keys(this.parameters);
        keys.map((item) => {
          if (
            item != "reportId" &&
            item != "designId" &&
            item != "filterCriteria"
          ) {
            if (
              item == "OBSDATE" ||
              item == "CREATEDDATE" ||
              item == "RESPONDEDDATE" ||
              item == "RESDATEREQ"
            ) {
              var obsDateCheck;
              if (
                this.parameters[item + "_date"].indexOf("/") == -1 &&
                this.parameters[item + "_date"].indexOf("-") != -1
              ) {
                obsDateCheck = "PRVMONTH";
              } else if (this.parameters[item + "_date"].indexOf("/") != -1) {
                obsDateCheck = "FTDATE";
              } else {
                obsDateCheck = this.parameters[item + "_date"];
              }
              this.dateSelectedData.map((data) => {
                if (data.reportParam == item) {
                  data["obsDateCheck"] = obsDateCheck;
                  data["value"] = this.parameters[item + "_date"];
                }
              });
              this.selectedFilter[item] = this.parameters[item + "_date"];
            } else if (
              item == "OBSDATE_date" ||
              item == "CREATEDDATE_date" ||
              item == "RESPONDEDDATE_date" ||
              item == "RESDATEREQ_date"
            ) {
            } else {
              if (isNaN(this.parameters[item])) {
                this.selectedFilter[item] = this.parameters[item]
                  .split(",")
                  .map(Number);
              } else {
                if (
                  item == "CHARTTYPE" ||
                  item == "CHKSUMGROUP1" ||
                  item == "CHKSUMGROUP2" ||
                  item == "CHKSUMGROUP3"
                ) {
                  this.selectedFilter[item] = this.parameters[item];
                } else {
                  this.selectedFilter[item] = [parseInt(this.parameters[item])];
                }
              }
            }
          }
        });
      }
      this.translate
        .get(this.savedFilterList[0]["FILTERNAME"])
        .subscribe((label) => {
          this.savedFilterList[0]["FILTERNAME"] = label;
        });
      this.keys = Object.keys(this.selectedFilter);
      if (this.parameters && this.parameters["SITEID"]) {
        if (!this.selectedFilter["SITEID"]) {
          this.selectedFilter["SITEID"] = this.parameters["SITEID"]
            .split(",")
            .map(Number);
        }
        this.getOtherData("SITEID");
      }
      if (this.parameters && this.parameters["AREAID"]) {
        if (!this.selectedFilter["AREAID"]) {
          this.selectedFilter["AREAID"] = this.parameters["AREAID"]
            .split(",")
            .map(Number);
        }
        this.getOtherData("AREAID");
      }

      if (this.parameters != "" && this.parameters != undefined) {
        var paramKeys = Object.keys(this.parameters);
        paramKeys.map((item) => {
          if (item != "SITEID" && item != "AREAID") {
            if (
              item != "CREATEDDATE" &&
              item != "CREATEDDATE_date" &&
              item != "OBSDATE" &&
              item != "OBSDATE_date" &&
              item != "RESPONDEDDATE" &&
              item != "RESPONDEDDATE_date" &&
              item != "RESDATEREQ" &&
              item != "RESDATEREQ_date" &&
              item != "filterCriteria" &&
              item != "designId" &&
              item != "reportId" &&
              !Number.isInteger(this.parameters[item])
            ) {
              if (this.parameters[item].search(",") != -1) {
                this.selectedFilter[item] = this.parameters[item]
                  .split(",")
                  .map(Number);
              } else {
                this.selectedFilter[item] = [parseInt(this.parameters[item])];
              }
            }
          }
        });
      }
      if (
        this.parameters &&
        (this.parameters["CHKSUMGROUP1"] ||
          this.parameters["CHKSUMGROUP2"] ||
          this.parameters["CHKSUMGROUP3"])
      ) {
        this.changeGroupList();
      }
      this.isLoaded = true;
      this.viewFilters = true;
    });
  }

  getFilterCriteria() {
    this.isLoaded = false;
    this.viewFilters = false;
    this.selectedFilter = [];
    this.filterSetting = {};
    if (this.parameters) {
      this.parameters["filterCriteria"] = this.selectedSavedFilter;
    }
    var params = {
      reportId: this.reportId,
      filterCriteriaId: this.selectedSavedFilter,
    };
    this.getFilters(params);
  }

  openCreateFilterPopup() {
    this.filterName = "";
    var keys = Object.keys(this.selectedFilter);
    if (keys.length == 0) {
      this.translate.get("ALTREPORTFILTERVALIDATE").subscribe((message) => {
        this.messageService.add({
          severity: "info",
          detail: message,
        });
      });
    } else {
      if (this.filterSetting != "") {
        this.updateFilterPopup = true;
      } else {
        this.createNewFilterPopup = true;
      }
    }
  }

  openNewFilter() {
    this.createNewFilterPopup = true;
    this.updateFilterPopup = false;
  }

  resetNewFilterForm() {
    this.filterName = this.oldFilterName;
    this.fromMonth = this.oldFromMonth;
    this.toMonth = this.oldToMonth;
    this.trendFromMonth = this.oldTrendFromMonth;
    this.trendFromYear = this.oldTrendFromYear;
    this.trendToMonth = this.oldTrendToMonth;
    this.trendToYear = this.oldTrendToYear;
  }

  clearDaterange() {
    this.fromDate = this.oldFromDate;
    this.toDate = this.oldToDate;
    this.obsDateCheck = this.oldObsDateCheck;
  }

  createNewFilter(type) {
    this.saveFilterDisabled = true;
    if (this.filterName.match(/^ *$/) !== null && type == "new") {
      this.filterNameEmpty = true;
      this.saveFilterDisabled = false;
    } else {
      // this.createNewFilterPopup = false;
      // this.updateFilterPopup = false;
      this.filterNameEmpty = false;

      var keys = Object.keys(this.selectedFilter);
      var newSelectedFilter = {};
      keys.map((item) => {
        if (isArray(this.selectedFilter[item])) {
          newSelectedFilter[item] = this.selectedFilter[item].join();
        } else {
          newSelectedFilter[item] = this.selectedFilter[item];
        }
      });
      var filterCriteriaId, filterName;
      if (type == "new") {
        filterCriteriaId = 0;
        filterName = this.filterName;
      } else {
        filterCriteriaId = this.selectedSavedFilter;
        var criteria = this.savedFilterList.filter((item) => {
          return item.FILTERID == this.selectedSavedFilter;
        });
        filterName = criteria[0]["FILTERNAME"];
      }
      var params = {
        reportId: this.reportId,
        filterCriteriaId: filterCriteriaId,
        filterName: filterName,
        options: [newSelectedFilter],
      };
      var savedFilter = this.savedFilterList;
      this.reportService.saveNewFilter(params).subscribe((res) => {
        this.translate.get(res["message"]).subscribe((message) => {
          if (res["status"] == true) {
            this.savedFilterList = [];
            this.messageService.add({
              severity: "success",
              detail: message,
            });
            savedFilter.push({
              FILTERID: res["id"],
              FILTERNAME: filterName,
            });
            this.savedFilterList = savedFilter;
            this.selectedSavedFilter = res["id"];
            this.saveFilterDisabled = false;
            this.createNewFilterPopup = false;
            this.updateFilterPopup = false;
            this.getFilterCriteria();
            // var newParam = {
            //   reportId: this.reportId,
            //   filterCriteriaId: res["id"]
            // };
            // this.filterName = "";
            // this.getFilters(newParam);
          } else {
            this.messageService.add({
              severity: "error",
              detail: message,
            });
          }
        });
      });
    }
  }

  openDateFilter(reportParam, label) {
    this.selectedDateFilters = false;
    this.tempSelectedReportParam = reportParam;
    this.translate.get(label).subscribe((newLabel) => {
      this.tempSelectedFilterName = newLabel;
    });
    if (this.reportId == 17 || this.reportId == 21) {
      this.detaDumpDatePopup = true;
      if (this.reportId == 17) {
        if (this.parameters) {
          var value = this.parameters["OBSDATE"];
          var newValue = value.split("-");
          var fromDate = new Date(newValue[0]);
          var toDate = new Date(newValue[1]);
          this.trendFromMonth = fromDate.getMonth() + 1;
          this.trendFromYear = fromDate.getFullYear();
          this.trendToMonth = toDate.getMonth() + 1;
          this.trendToYear = toDate.getFullYear();
        }
      }
    } else {
      var selectedData = this.dateSelectedData.filter((data) => {
        return data.reportParam == reportParam;
      });
      if (
        selectedData[0].obsDateCheck != "YTD" &&
        selectedData[0].obsDateCheck != "CMY" &&
        selectedData[0].value
      ) {
        var value = selectedData[0].value;
        var newValue = value.split("-");
        if (newValue[0].indexOf("/") != -1) {
          this.includeToday = false;
          this.selectMonth = 1;
          this.rangeDate = [];
          // this.rangeDate[0] = new Date(newValue[0]);
          // this.rangeDate[1] = new Date(newValue[1]);
          if (this.dateFormatType == "dd/MM/yyyy") {
            var fDate = newValue[0].split("/");
            fDate = fDate[1] + "/" + fDate[0] + "/" + fDate[2];
            var tDate = newValue[1].split("/");
            tDate = tDate[1] + "/" + tDate[0] + "/" + tDate[2];
          } else {
            var fDate = newValue[0];
            var tDate = newValue[1];
          }

          this.fromDate = formatDate(
            new Date(fDate),
            this.dateFormatType,
            this.translate.getDefaultLang()
          );
          this.toDate = formatDate(
            new Date(tDate),
            this.dateFormatType,
            this.translate.getDefaultLang()
          );
          this.oldFromDate = this.fromDate;
          this.oldToDate = this.toDate;
        } else {
          this.rangeDate = "";
          this.oldFromDate = "";
          this.oldToDate = "";
          this.selectMonth = parseInt(newValue[0]);
          if (newValue[1] == "1") {
            this.includeToday = true;
          } else {
            this.includeToday = false;
          }
        }
        this.obsDateCheck = selectedData[0].obsDateCheck;
        this.oldObsDateCheck = this.obsDateCheck;
      } else {
        this.obsDateCheck = selectedData[0].obsDateCheck;
        this.oldObsDateCheck = this.obsDateCheck;
        this.rangeDate = "";
        this.oldFromDate = "";
        this.oldToDate = "";
        this.selectMonth = 1;
        this.includeToday = false;
      }
      // this.translate.get(label).subscribe(newLabel => {
      //   this.tempSelectedFilterName = newLabel;
      // });
      this.datePopup = true;
    }
  }

  monthDiff(dateFrom, dateTo) {
    return (
      dateTo.getMonth() -
      dateFrom.getMonth() +
      1 +
      12 * (dateTo.getFullYear() - dateFrom.getFullYear())
    );
  }

  diff_weeks(dt2, dt1) {
    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60 * 60 * 24 * 7;
    return Math.abs(Math.round(diff));
  }

  dateSelected() {
    var proceed = true;

    this.dateSelectedData.map((item) => {
      if (item.reportParam == this.tempSelectedReportParam) {
        if (this.reportId == 17 || this.reportId == 21) {
          if (
            this.trendFromMonth &&
            this.trendFromYear &&
            this.trendToMonth &&
            this.trendToYear
          ) {
            if (this.trendFromMonth == 1) {
              this.trendFromMonthString = "FMKJANUARY";
            } else if (this.trendFromMonth == 2) {
              this.trendFromMonthString = "FMKFEBRUARY";
            } else if (this.trendFromMonth == 3) {
              this.trendFromMonthString = "FMKMARCH";
            } else if (this.trendFromMonth == 4) {
              this.trendFromMonthString = "FMKAPRIL";
            } else if (this.trendFromMonth == 5) {
              this.trendFromMonthString = "FMKMAY";
            } else if (this.trendFromMonth == 6) {
              this.trendFromMonthString = "FMKJUNE";
            } else if (this.trendFromMonth == 7) {
              this.trendFromMonthString = "FMKJULY";
            } else if (this.trendFromMonth == 8) {
              this.trendFromMonthString = "FMKAUGUST";
            } else if (this.trendFromMonth == 9) {
              this.trendFromMonthString = "FMKSEPTEMBER";
            } else if (this.trendFromMonth == 10) {
              this.trendFromMonthString = "FMKOCTOBER";
            } else if (this.trendFromMonth == 11) {
              this.trendFromMonthString = "FMKNOVEMBER";
            } else if (this.trendFromMonth == 12) {
              this.trendFromMonthString = "FMKDECEMBER";
            }

            if (this.trendToMonth == 1) {
              this.trendToMonthString = "FMKJANUARY";
            } else if (this.trendToMonth == 2) {
              this.trendToMonthString = "FMKFEBRUARY";
            } else if (this.trendToMonth == 3) {
              this.trendToMonthString = "FMKMARCH";
            } else if (this.trendToMonth == 4) {
              this.trendToMonthString = "FMKAPRIL";
            } else if (this.trendToMonth == 5) {
              this.trendToMonthString = "FMKMAY";
            } else if (this.trendToMonth == 6) {
              this.trendToMonthString = "FMKJUNE";
            } else if (this.trendToMonth == 7) {
              this.trendToMonthString = "FMKJULY";
            } else if (this.trendToMonth == 8) {
              this.trendToMonthString = "FMKAUGUST";
            } else if (this.trendToMonth == 9) {
              this.trendToMonthString = "FMKSEPTEMBER";
            } else if (this.trendToMonth == 10) {
              this.trendToMonthString = "FMKOCTOBER";
            } else if (this.trendToMonth == 11) {
              this.trendToMonthString = "FMKNOVEMBER";
            } else if (this.trendToMonth == 12) {
              this.trendToMonthString = "FMKDECEMBER";
            }

            var monthDiff = this.monthDiff(
              new Date(this.trendFromYear, this.trendFromMonth - 1, 1),
              new Date(this.trendToYear, this.trendToMonth, 0)
            );
            if (monthDiff >= 0) {
              this.oldTrendFromMonth = this.trendFromMonth;
              this.oldTrendFromYear = this.trendFromYear;
              this.oldTrendToMonth = this.trendToMonth;
              this.oldTrendToYear = this.trendToYear;
              if (this.reportId == 21) {
                if (
                  monthDiff <= this.globalData.GlobalData["DUMPREPORTINTERVAL"]
                ) {
                  var fromMonth = formatDate(
                    new Date(this.trendFromYear, this.trendFromMonth - 1, 1),
                    "MM/dd/yyyy",
                    this.translate.getDefaultLang()
                  );
                  var toMonth = formatDate(
                    new Date(this.trendToYear, this.trendToMonth, 0),
                    "MM/dd/yyyy",
                    this.translate.getDefaultLang()
                  );
                } else {
                  this.translate
                    .get("ALTOBSERVATIONDATEMONTHPERIODINTERVAL")
                    .subscribe((message) => {
                      var newMessage = message.replace(
                        "####",
                        this.globalData.GlobalData["DUMPREPORTINTERVAL"]
                      );
                      this.messageService.add({
                        severity: "error",
                        detail: newMessage,
                      });
                    });
                  proceed = false;
                }
              } else {
                var fromMonth = formatDate(
                  new Date(this.trendFromYear, this.trendFromMonth - 1, 1),
                  "MM/dd/yyyy",
                  this.translate.getDefaultLang()
                );
                var toMonth = formatDate(
                  new Date(this.trendToYear, this.trendToMonth, 0),
                  "MM/dd/yyyy",
                  this.translate.getDefaultLang()
                );
                if (this.parameters) {
                  this.parameters["OBSDATE"] = fromMonth + " - " + toMonth;
                }
              }
              item.value = fromMonth + " - " + toMonth;
            } else {
              this.translate.get("LBLSELMONTHYEAR").subscribe((message) => {
                this.messageService.add({
                  severity: "error",
                  detail: message,
                });
              });
              proceed = false;
            }
          } else {
            this.translate.get("LBLSELMONTHYEAR").subscribe((message) => {
              this.messageService.add({
                severity: "error",
                detail: message,
              });
            });
            proceed = false;
          }
        } else {
          var value = item.value;
          if (this.obsDateCheck != "YTD" && this.obsDateCheck != "CMY") {
            if (this.obsDateCheck == "PRVMONTH") {
              var includeToday;
              if (this.includeToday) {
                includeToday = 1;
              } else {
                includeToday = 0;
              }
              var cDate = new Date();
              var cMonth = cDate.getMonth() + 1;
              var cYear = cDate.getFullYear();
              var formatedDate = formatDate(
                cDate,
                "MM/dd/yyyy",
                this.translate.getDefaultLang()
              );
              if (this.reportId == 19) {
                var ourDate = new Date();
                var currDate = new Date();
                var for7Date = new Date();
                var weekInDays = this.selectMonth * 7;
                var pastDate = ourDate.getDate() - weekInDays;
                var past7Date = for7Date.getDate() - 7;
                ourDate.setDate(pastDate);
                for7Date.setDate(past7Date);
                var fromDate = formatDate(
                  ourDate,
                  "MM/dd/yyyy",
                  this.translate.getDefaultLang()
                );
                if (includeToday == 1) {
                  var toDate = formatDate(
                    currDate,
                    "MM/dd/yyyy",
                    this.translate.getDefaultLang()
                  );
                } else {
                  var toDate = formatDate(
                    for7Date,
                    "MM/dd/yyyy",
                    this.translate.getDefaultLang()
                  );
                }
                if (this.selectMonth == 12 && includeToday == 1) {
                  if (this.reportId == 19) {
                    this.translate
                      .get("ALTOBSERVATIONDATEMONTHPERIODWEEK")
                      .subscribe((label) => {
                        this.messageService.add({
                          severity: "info",
                          detail: label,
                        });
                      });
                  } else {
                    this.translate
                      .get("ALTOBSERVATIONDATEMONTHPERIOD")
                      .subscribe((label) => {
                        this.messageService.add({
                          severity: "info",
                          detail: label,
                        });
                      });
                  }
                  proceed = false;
                }
              } else {
                var newMonth = cMonth - this.selectMonth;

                var previousDate = new Date(cDate.setMonth(newMonth - 1));
                previousDate = new Date(previousDate.setDate(1));
                var previousMonth = new Date(cDate.setMonth(cMonth - 1));
                var previousMonthDays = new Date(
                  cYear,
                  previousMonth.getMonth(),
                  0
                ).getDate();
                var fromDate = formatDate(
                  previousDate,
                  "MM/dd/yyyy",
                  this.translate.getDefaultLang()
                );
                if (includeToday == 1) {
                  var toDate = formatedDate;
                } else {
                  var toDate =
                    previousMonth.getMonth() +
                    "/" +
                    previousMonthDays +
                    "/" +
                    cYear;
                }
                if (
                  this.reportId == 6 ||
                  this.reportId == 4 ||
                  this.reportId == 23
                ) {
                  if (
                    this.selectMonth > 12 ||
                    this.monthDiff(new Date(fromDate), new Date(toDate)) > 12
                  ) {
                    this.translate
                      .get("ALTOBSERVATIONDATEMONTHPERIOD")
                      .subscribe((label) => {
                        this.messageService.add({
                          severity: "info",
                          detail: label,
                        });
                      });
                    proceed = false;
                  }
                }
              }

              value = this.selectMonth + "-" + includeToday;
              item.month = this.selectMonth;
              item.includeToday = includeToday;
            } else {
              var fDate;
              var tDate;
              if (
                this.dateFormatType == "dd/MM/yyyy" &&
                this.fromDate.toString().indexOf("/") != -1
              ) {
                fDate = this.fromDate.split("/");
                fDate = new Date(fDate[1] + "/" + fDate[0] + "/" + fDate[2]);
              } else {
                fDate = new Date(this.fromDate);
              }

              if (
                this.dateFormatType == "dd/MM/yyyy" &&
                this.toDate.toString().indexOf("/") != -1
              ) {
                tDate = this.toDate.split("/");
                tDate = new Date(tDate[1] + "/" + tDate[0] + "/" + tDate[2]);
              } else {
                tDate = new Date(this.toDate);
              }
              this.translate
                .get([
                  "FMKPLS",
                  "FMKFROM",
                  "FMKTO",
                  "FMKPRVD",
                  "ALTOBSERVATIONDATEMONTHPERIODWEEK",
                  "ALTOBSERVATIONDATEMONTHPERIOD",
                ])
                .subscribe((res: Object) => {
                  if (this.fromDate == null || this.fromDate == "") {
                    this.messageService.add({
                      severity: "info",
                      detail: res["FMKPLS"] + res["FMKFROM"],
                    });
                    proceed = false;
                  } else if (this.toDate == null || this.toDate == "") {
                    this.messageService.add({
                      severity: "info",
                      detail: res["FMKPLS"] + res["FMKTO"],
                    });
                    proceed = false;
                  } else if (fDate > tDate) {
                    this.messageService.add({
                      severity: "info",
                      detail: res["FMKPRVD"],
                    });
                    proceed = false;
                  }
                  if (this.reportId == 19) {
                    if (this.diff_weeks(tDate, fDate) > 12) {
                      this.messageService.add({
                        severity: "info",
                        detail: res["ALTOBSERVATIONDATEMONTHPERIODWEEK"],
                      });
                      proceed = false;
                    }
                  }
                  if (
                    this.reportId == 6 ||
                    this.reportId == 4 ||
                    this.reportId == 23
                  ) {
                    if (this.monthDiff(fDate, tDate) > 12) {
                      this.messageService.add({
                        severity: "info",
                        detail: res["ALTOBSERVATIONDATEMONTHPERIODWEEK"],
                      });
                      proceed = false;
                    }
                  }
                });
              if (proceed) {
                var fromDate = formatDate(
                  fDate,
                  this.dateFormatType,
                  this.translate.getDefaultLang()
                );
                var toDate = formatDate(
                  new Date(tDate),
                  this.dateFormatType,
                  this.translate.getDefaultLang()
                );
                value = fromDate + "-" + toDate;
              }
            }
            item.obsDateCheck = this.obsDateCheck;
            item.value = value;
          } else {
            item.obsDateCheck = this.obsDateCheck;
            item.value = this.obsDateCheck;
          }
        }
        if (proceed) {
          this.selectedFilter[item.reportParam] = item.value;
        }
      }
    });

    this.keys = Object.keys(this.selectedFilter);
    if (proceed) {
      this.datePopup = false;
      this.detaDumpDatePopup = false;
      this.selectedDateFilters = true;
    }
  }

  cancelObsDate() {
    this.datePopup = false;
    this.selectedDateFilters = true;
  }

  // Advance serach
  advance_search() {
    this.show_advance_search = !this.show_advance_search;
    this.iconType =
      this.iconType === "ui-icon-add" ? "ui-icon-remove" : "ui-icon-add";
  }

  deleteRecord() {
    if (this.selectedSavedFilter == 0) {
      var msg = "";
      this.translate.get("ALTFILTERRESETVALID").subscribe((message) => {
        msg = message;
      });
      this.messageService.add({
        severity: "error",
        detail: msg,
      });
    } else {
      var msg = "";
      this.translate.get("ALTFILTERRESETCONFIRM").subscribe((message) => {
        msg = message;
      });
      this.confirmationService.confirm({
        message: msg,
        accept: () => {
          var params = {
            reportId: this.reportId,
            filterCriteriaId: this.selectedSavedFilter,
          };

          this.reportService.deleteFilterCriteria(params).subscribe((res) => {
            this.translate.get(res["message"]).subscribe((message) => {
              if (res["status"] == true) {
                this.messageService.add({
                  severity: "success",
                  detail: message,
                });
                this.selectedSavedFilter = 0;
                this.getFilterCriteria();
              } else {
                this.messageService.add({
                  severity: "error",
                  detail: message,
                });
              }
            });
          });
        },
      });
    }
  }

  getOtherData(reportParam) {
    if (reportParam == "SITEID") {
      var siteParam;
      if (this.selectedFilter[reportParam]) {
        siteParam = {
          siteId: this.selectedFilter[reportParam].join(),
        };
      } else {
        siteParam = {
          siteId: "",
        };
      }
      this.reportService.getDataBySite(siteParam).subscribe((res) => {
        if (res["status"] == true) {
          this.filterList.map((item) => {
            if (item["REPORTPARAM"] == "AREAID") {
              var tempData = [];
              if (res["area"].length > 0) {
                res["area"].map((data) => {
                  var keys = Object.keys(data);
                  tempData.push({
                    label: data[keys[1]],
                    value: data[keys[0]],
                  });
                });
              }
              item.Data = tempData;
            } else if (item["REPORTPARAM"] == "SHIFTID") {
              var tempData = [];
              if (res["shift"].length > 0) {
                res["shift"].map((data) => {
                  var keys = Object.keys(data);
                  tempData.push({
                    label: data[keys[1]],
                    value: data[keys[0]],
                  });
                });
              }
              item.Data = tempData;
            } else if (item["REPORTPARAM"] == "OBSERVERID") {
              var tempData = [];
              if (res["observer"].length > 0) {
                res["observer"].map((data) => {
                  var keys = Object.keys(data);
                  tempData.push({
                    label: data[keys[1]],
                    value: data[keys[0]],
                  });
                });
              }
              item.Data = tempData;
            } else if (item["REPORTPARAM"] == "CUSTOMFIELD1") {
              var tempData = [];
              if (res["customField1"].length > 0) {
                res["customField1"].map((data) => {
                  var keys = Object.keys(data);
                  tempData.push({
                    label: data[keys[1]],
                    value: data[keys[0]],
                  });
                });
              }
              item.Data = tempData;
            } else if (item["REPORTPARAM"] == "CUSTOMFIELD2") {
              var tempData = [];
              if (res["customField2"].length > 0) {
                res["customField2"].map((data) => {
                  var keys = Object.keys(data);
                  tempData.push({
                    label: data[keys[1]],
                    value: data[keys[0]],
                  });
                });
              }
              item.Data = tempData;
            } else if (item["REPORTPARAM"] == "CUSTOMFIELD3") {
              var tempData = [];
              if (res["customField3"].length > 0) {
                res["customField3"].map((data) => {
                  var keys = Object.keys(data);
                  tempData.push({
                    label: data[keys[1]],
                    value: data[keys[0]],
                  });
                });
              }
              item.Data = tempData;
            }
          });

          this.filterCategory.map((item) => {
            if (item["REPORTPARAM"] == "SETUPID") {
              var tempData = [];
              if (res["checkList"].length > 0) {
                res["checkList"].map((data) => {
                  var keys = Object.keys(data);
                  tempData.push({
                    label: data[keys[1]],
                    value: data[keys[0]],
                  });
                });
              }
              item.Data = tempData;
            }
          });
        }
      });
    } else if (reportParam == "AREAID") {
      if (this.selectedFilter[reportParam]) {
        var areaParam = {
          areaId: this.selectedFilter[reportParam].join(),
        };
      }
      this.reportService.getDataByArea(areaParam).subscribe((res) => {
        if (res["status"] == true) {
          this.filterList.map((item) => {
            if (item["REPORTPARAM"] == "SUBAREAID") {
              var tempData = [];
              if (res["subArea"].length > 0) {
                res["subArea"].map((data) => {
                  var keys = Object.keys(data);
                  tempData.push({
                    label: data[keys[1]],
                    value: data[keys[0]],
                  });
                });
              }
              item.Data = tempData;
            }
          });
        }
      });
    }
  }

  runReport() {
    this.isLoaded = false;
    this.viewFilters = false;
    var keys = Object.keys(this.selectedFilter);
    keys.map((item, key) => {
      if (isArray(this.selectedFilter[item])) {
        if (this.selectedFilter[item].length == 0) {
          keys.splice(key, 1);
        }
      }
    });
    var mandatorykeys = Object.keys(this.mandatoryCriteria);
    if (keys.length == 0 && mandatorykeys.length != 0) {
      var checkMandotory;
      var label;
      this.mandatoryCriteria["filterDateList"].map((data) => {
        var checkForLabel = this.mandatoryCriteria["filterDateList"].filter(
          (dataParam) => {
            return dataParam == "CREATEDDATE";
          }
        );

        if (checkForLabel.length > 0) {
          label = "ALTCHECKLISTCOUNTREPORTALERT";
        } else {
          label = "ALTOBSERVATIONDATEMANDATORY";
        }

        checkMandotory = keys.filter((item) => {
          return item == data;
        });
      });
      if (checkMandotory.length == 0) {
        this.translate.get(label).subscribe((message) => {
          this.messageService.add({
            severity: "info",
            detail: message,
          });
        });
      }
      this.isLoaded = true;
      this.viewFilters = true;
    } else {
      var proceed = true;
      if (
        this.mandatoryCriteria["filterDateList"] &&
        this.mandatoryCriteria["filterDateList"].length > 0
      ) {
        var checkMandotory;
        var label;
        var i = 1;
        this.mandatoryCriteria["filterDateList"].map((data) => {
          var checkForLabel = this.mandatoryCriteria["filterDateList"].filter(
            (dataParam) => {
              return dataParam == "CREATEDDATE";
            }
          );
          if (checkForLabel.length > 0) {
            label = "ALTCHECKLISTCOUNTREPORTALERT";
          } else {
            label = "ALTOBSERVATIONDATEMANDATORY";
          }

          checkMandotory = keys.filter((item) => {
            return item == data;
          });
          if (checkMandotory.length != 0) {
            if (this.reportId != 15) {
              i++;
            } else if (checkMandotory[0] == "OBSDATE") {
              i++;
            }
          }
        });
        if (i == 1) {
          proceed = false;
          this.translate.get(label).subscribe((message) => {
            this.messageService.add({
              severity: "info",
              detail: message,
            });
          });
        }
      }
      if (
        this.mandatoryCriteria["filterGroupBy"] &&
        this.mandatoryCriteria["filterGroupBy"].length > 0
      ) {
        var checkGroupMandotory;
        var j = 1;
        this.mandatoryCriteria["filterGroupBy"].map((data) => {
          checkGroupMandotory = keys.filter((item) => {
            return item == data;
          });
          if (checkGroupMandotory.length != 0) {
            j++;
          }
        });
        if (j == 1) {
          proceed = false;
          this.translate
            .get("ALTCHECKLISTCOUNTGROUPBYALT")
            .subscribe((message) => {
              this.messageService.add({
                severity: "info",
                detail: message,
              });
            });
        }
      }

      if (
        this.mandatoryCriteria["filterCategory"] &&
        this.mandatoryCriteria["filterCategory"].length > 0
      ) {
        var checkGroupMandotory;
        var k = 1;
        this.mandatoryCriteria["filterCategory"].map((data) => {
          checkGroupMandotory = keys.filter((item) => {
            return item == data;
          });
          if (checkGroupMandotory.length != 0) {
            k++;
          }
        });
        if (k == 1) {
          proceed = false;
          this.translate.get("ALTPROVIDESETUP").subscribe((message) => {
            this.messageService.add({
              severity: "info",
              detail: message,
            });
          });
        }
      }
      if (
        this.mandatoryCriteria["filterChartType"] &&
        this.mandatoryCriteria["filterChartType"].length > 0
      ) {
        var checkGroupMandotory;
        var k = 1;
        this.mandatoryCriteria["filterChartType"].map((data) => {
          checkGroupMandotory = keys.filter((item) => {
            return item == data;
          });
          if (checkGroupMandotory.length != 0) {
            k++;
          }
        });
        if (k == 1) {
          proceed = false;
          this.translate
            .get("ALTCHARTTYPEMANDATORYRPT")
            .subscribe((message) => {
              this.messageService.add({
                severity: "info",
                detail: message,
              });
            });
        } else {
          if (this.reportId == 17) {
            var monthDiff = this.monthDiff(
              new Date(this.trendFromYear, this.trendFromMonth, 0),
              new Date(this.trendToYear, this.trendToMonth, 0)
            );
            if (
              monthDiff > 17 &&
              this.selectedFilter[
                this.mandatoryCriteria["filterChartType"][0]
              ] == 1
            ) {
              proceed = false;
              this.translate
                .get("ALTCHARTTYPE1MANDATORYRPT")
                .subscribe((message) => {
                  this.messageService.add({
                    severity: "info",
                    detail: message,
                  });
                });
            } else if (
              monthDiff > 53 &&
              this.selectedFilter[
                this.mandatoryCriteria["filterChartType"][0]
              ] == 2
            ) {
              proceed = false;
              this.translate
                .get("ALTCHARTTYPE2MANDATORYRPT")
                .subscribe((message) => {
                  this.messageService.add({
                    severity: "info",
                    detail: message,
                  });
                });
            } else if (
              monthDiff > 43 &&
              this.selectedFilter[
                this.mandatoryCriteria["filterChartType"][0]
              ] == 3
            ) {
              proceed = false;
              this.translate
                .get("ALTCHARTTYPE3MANDATORYRPT")
                .subscribe((message) => {
                  this.messageService.add({
                    severity: "info",
                    detail: message,
                  });
                });
            }
          }
        }
      }
      // return false
      if (!proceed) {
        this.isLoaded = true;
        this.viewFilters = true;
        return false;
      } else {
        var newSelectedFilter = {};

        if (keys.length > 0) {
          keys.map((item, key) => {
            if (isArray(this.selectedFilter[item])) {
              newSelectedFilter[item] = this.selectedFilter[item].join();
            } else {
              if (
                item == "OBSDATE" ||
                item == "CREATEDDATE" ||
                item == "RESDATEREQ" ||
                item == "RESPONDEDDATE"
              ) {
                if (this.reportId != 17 && this.reportId != 21) {
                  var cDate = new Date();
                  var cMonth = cDate.getMonth() + 1;
                  var cYear = cDate.getFullYear();
                  var formatedDate = formatDate(
                    cDate,
                    "MM/dd/yyyy",
                    this.translate.getDefaultLang()
                  );
                  var fromDate;
                  var toDate;
                  if (this.selectedFilter[item] == "YTD") {
                    fromDate = "01/01/" + cYear;
                    toDate = formatedDate;
                  } else if (this.selectedFilter[item] == "CMY") {
                    fromDate = cMonth + "/01/" + cYear;
                    toDate = formatedDate;
                  } else if (this.selectedFilter[item].indexOf("/") == -1) {
                    if (this.reportId == 19) {
                      var ourDate = new Date();
                      var currDate = new Date();
                      var for7Date = new Date();
                      var weekInDays =
                        this.selectedFilter[item].split("-")[0] * 7;
                      var pastDate = ourDate.getDate() - weekInDays;
                      var past7Date = for7Date.getDate() - 7;
                      ourDate.setDate(pastDate);
                      for7Date.setDate(past7Date);
                      fromDate = formatDate(
                        ourDate,
                        "MM/dd/yyyy",
                        this.translate.getDefaultLang()
                      );
                      if (this.selectedFilter[item].split("-")[1] == 1) {
                        toDate = formatDate(
                          currDate,
                          "MM/dd/yyyy",
                          this.translate.getDefaultLang()
                        );
                      } else {
                        toDate = formatDate(
                          for7Date,
                          "MM/dd/yyyy",
                          this.translate.getDefaultLang()
                        );
                      }
                    } else {
                      var newMonth =
                        cMonth - this.selectedFilter[item].split("-")[0];

                      var previousDate = new Date(cDate.setMonth(newMonth - 1));
                      previousDate = new Date(previousDate.setDate(1));
                      var previousMonth = new Date(cDate.setMonth(cMonth - 1));
                      var previousMonthDays = new Date(
                        cYear,
                        previousMonth.getMonth(),
                        0
                      ).getDate();
                      fromDate = formatDate(
                        previousDate,
                        "MM/dd/yyyy",
                        this.translate.getDefaultLang()
                      );
                      if (this.selectedFilter[item].split("-")[1] == 1) {
                        toDate = formatedDate;
                      } else {
                        toDate =
                          previousMonth.getMonth() +
                          "/" +
                          previousMonthDays +
                          "/" +
                          cYear;
                      }
                    }
                  } else {
                    var selectedData = this.dateSelectedData.filter((data) => {
                      return data.reportParam == item;
                    });

                    var newValue = selectedData[0].value.split("-");
                    if (this.dateFormatType == "dd/MM/yyyy") {
                      var newFDate = newValue[0].split("/");
                      newFDate =
                        newFDate[1] + "/" + newFDate[0] + "/" + newFDate[2];
                      var newTDate = newValue[1].split("/");
                      newTDate =
                        newTDate[1] + "/" + newTDate[0] + "/" + newTDate[2];
                    } else {
                      var newFDate = newValue[0];
                      var newTDate = newValue[1];
                    }
                    fromDate = formatDate(
                      new Date(newFDate),
                      "MM/dd/yyyy",
                      this.translate.getDefaultLang()
                    );

                    toDate = formatDate(
                      new Date(newTDate),
                      "MM/dd/yyyy",
                      this.translate.getDefaultLang()
                    );
                  }
                  newSelectedFilter[item] = fromDate + " - " + toDate;
                } else {
                  var selDate = this.selectedFilter[item].split("-");
                  var tempFormDate = new Date(selDate[0]);
                  var newFormDate = new Date(tempFormDate.setDate(1));
                  var fDate = formatDate(
                    newFormDate,
                    "MM/dd/yyyy",
                    this.translate.getDefaultLang()
                  );
                  var tDate = formatDate(
                    selDate[1],
                    "MM/dd/yyyy",
                    this.translate.getDefaultLang()
                  );
                  newSelectedFilter[item] = fDate + " - " + tDate;
                }

                newSelectedFilter[item + "_date"] = this.selectedFilter[item];
              } else {
                newSelectedFilter[item] = this.selectedFilter[item];
              }
            }
            if (key == keys.length - 1) {
              newSelectedFilter["reportId"] = this.reportId;
              newSelectedFilter["designId"] = this.reportDesignId;
              newSelectedFilter["filterCriteria"] = this.selectedSavedFilter;
              localStorage.removeItem("filterInfo");
              localStorage.setItem(
                "filterInfo",
                JSON.stringify(newSelectedFilter)
              );
              if (this.reportId == 21) {
                this.isLoaded = false;
                this.viewFilters = false;
                this.downloadReport();
              } else {
                this.router.navigate(["./report-results/" + this.reportId], {
                  skipLocationChange: true,
                });
              }
            }
          });
        } else {
          newSelectedFilter["reportId"] = this.reportId;
          newSelectedFilter["designId"] = this.reportDesignId;
          newSelectedFilter["filterCriteria"] = this.selectedSavedFilter;
          localStorage.removeItem("filterInfo");
          localStorage.setItem("filterInfo", JSON.stringify(newSelectedFilter));
          if (this.reportId == 21) {
            this.downloadReport();
          } else {
            this.router.navigate(["./report-results/" + this.reportId], {
              skipLocationChange: true,
            });
          }
        }
      }
    }
  }

  changeGroupList() {
    var group1 = "";
    var group2 = "";
    var group3 = "";
    if (this.selectedFilter["CHKSUMGROUP1"]) {
      group1 = this.selectedFilter["CHKSUMGROUP1"];
    }
    if (this.selectedFilter["CHKSUMGROUP2"]) {
      group2 = this.selectedFilter["CHKSUMGROUP2"];
    }
    if (this.selectedFilter["CHKSUMGROUP3"]) {
      group3 = this.selectedFilter["CHKSUMGROUP3"];
    }
    var params = {
      group1: group1,
      group2: group2,
      group3: group3,
    };

    this.reportService.getGroupByList(params).subscribe((res) => {
      if (res["status"] == true) {
        var group1Data = [];
        var group2Data = [];
        var group3Data = [];
        res["group1"].map((data) => {
          var keys = Object.keys(data);
          this.translate.get(data[keys[1]]).subscribe((label) => {
            group1Data.push({ label: label, value: data[keys[0]] });
          });
        });

        res["group2"].map((data) => {
          var keys = Object.keys(data);
          this.translate.get(data[keys[1]]).subscribe((label) => {
            group2Data.push({ label: label, value: data[keys[0]] });
          });
        });

        res["group3"].map((data) => {
          var keys = Object.keys(data);
          this.translate.get(data[keys[1]]).subscribe((label) => {
            group3Data.push({ label: label, value: data[keys[0]] });
          });
        });
        this.filterGroupBy.map((item) => {
          if (item.Name == "LBLGROUP1") {
            item.Data = group1Data;
          } else if (item.Name == "LBLGROUP2") {
            item.Data = group2Data;
          } else {
            item.Data = group3Data;
          }
        });
      }
    });
  }

  downloadReport() {
    var params = this.reportService.FilterInfo;
    this.dumpParam = params;
    this.reportService.getReportData(params).subscribe((res) => {
      if (res["status"]) {
        if (res["DataStatus"].length > 0) {
          this.categoryData = res["Data"];
          this.obsData = res["DataStatus"];
          this.translate
            .get([
              "LBLCHECKLISTNO",
              "LBLSETUP",
              "LBLOBSDATE",
              "LBLSITENAME",
              "LBLOBSERVERNAME",
              "LBLAREANAME",
              "LBLSUBAREAFULLNAME",
              "LBLSHIFTNAME",
              "LBLLENGTH",
              "LBLPEOPLECONTACTED",
              "LBLPEOPLEOBSERVED",
              "LBLSAFECOMMENTS",
              "LBLUNSAFECOMMENTS",
              "LBLSTATUS",
              "LBLENTEREDBY",
              "LBLENTEREDDATE",
              "LBLUSERNAME",
              "LBLSAFE",
              "LBLUNSAFE",
            ])
            .subscribe((resLabel) => {
              this.subAreaOption = this.auth.UserInfo["subAreaOptionValue"];
              if (this.subAreaOption == 0) {
                this.cols = [
                  { field: "CARDID", header: resLabel["LBLCHECKLISTNO"] },
                  {
                    field: "CHECKLISTSETUPNAME",
                    header: resLabel["LBLSETUP"],
                  },
                  { field: "OBSERVATIONDATE", header: resLabel["LBLOBSDATE"] },
                  { field: "SITENAME", header: resLabel["LBLSITENAME"] },
                  {
                    field: "OBSERVERNAME",
                    header: resLabel["LBLOBSERVERNAME"],
                  },
                  { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                  { field: "SHIFTNAME", header: resLabel["LBLSHIFTNAME"] },
                  { field: "LENGTH", header: resLabel["LBLLENGTH"] },
                  {
                    field: "PEOPLECONTACTED",
                    header: resLabel["LBLPEOPLECONTACTED"],
                  },
                  {
                    field: "PEOPLEOBSERVED",
                    header: resLabel["LBLPEOPLEOBSERVED"],
                  },
                  {
                    field: "SAFECOMMENTS",
                    header: resLabel["LBLSAFECOMMENTS"],
                  },
                  {
                    field: "UNSAFECOMMENTS",
                    header: resLabel["LBLUNSAFECOMMENTS"],
                  },
                  { field: "STATUS", header: resLabel["LBLSTATUS"] },
                  { field: "ENTEREDBY", header: resLabel["LBLENTEREDBY"] },
                  { field: "ENTEREDDATE", header: resLabel["LBLENTEREDDATE"] },
                  { field: "ENTEREDBYID", header: resLabel["LBLUSERNAME"] },
                ];
              } else {
                this.cols = [
                  { field: "CARDID", header: resLabel["LBLCHECKLISTNO"] },
                  {
                    field: "CHECKLISTSETUPNAME",
                    header: resLabel["LBLSETUP"],
                  },
                  { field: "OBSERVATIONDATE", header: resLabel["LBLOBSDATE"] },
                  { field: "SITENAME", header: resLabel["LBLSITENAME"] },
                  {
                    field: "OBSERVERNAME",
                    header: resLabel["LBLOBSERVERNAME"],
                  },
                  { field: "AREANAME", header: resLabel["LBLAREANAME"] },
                  {
                    field: "SUBAREANAME",
                    header: resLabel["LBLSUBAREAFULLNAME"],
                  },
                  { field: "SHIFTNAME", header: resLabel["LBLSHIFTNAME"] },
                  { field: "LENGTH", header: resLabel["LBLLENGTH"] },
                  {
                    field: "PEOPLECONTACTED",
                    header: resLabel["LBLPEOPLECONTACTED"],
                  },
                  {
                    field: "PEOPLEOBSERVED",
                    header: resLabel["LBLPEOPLEOBSERVED"],
                  },
                  {
                    field: "SAFECOMMENTS",
                    header: resLabel["LBLSAFECOMMENTS"],
                  },
                  {
                    field: "UNSAFECOMMENTS",
                    header: resLabel["LBLUNSAFECOMMENTS"],
                  },
                  { field: "STATUS", header: resLabel["LBLSTATUS"] },
                  { field: "ENTEREDBY", header: resLabel["LBLENTEREDBY"] },
                  { field: "ENTEREDDATE", header: resLabel["LBLENTEREDDATE"] },
                  { field: "ENTEREDBYID", header: resLabel["LBLUSERNAME"] },
                ];
              }

              for (var i = 0; i <= this.categoryData.length - 1; i++) {
                if (i == 0) {
                  this.cols.push({
                    field: "###LBLSAFE",
                    header: "###LBLSAFE",
                  });
                  this.cols.push({
                    field: "###LBLUNSAFE",
                    header: "###LBLUNSAFE",
                  });
                  if (
                    params["INCLUDECOMMENTS"] &&
                    params["INCLUDECOMMENTS"] == "1"
                  ) {
                    this.cols.push({
                      field: "###LBLSAFECOMMENTS",
                      header: "###LBLSAFECOMMENTS",
                    });
                    this.cols.push({
                      field: "###LBLUNSAFECOMMENTS",
                      header: "###LBLUNSAFECOMMENTS",
                    });
                  }
                } else {
                  this.cols.push({
                    field: "###LBLSAFE" + i,
                    header: "###LBLSAFE" + i,
                  });
                  this.cols.push({
                    field: "###LBLUNSAFE" + i,
                    header: "###LBLUNSAFE" + i,
                  });
                  if (
                    params["INCLUDECOMMENTS"] &&
                    params["INCLUDECOMMENTS"] == "1"
                  ) {
                    this.cols.push({
                      field: "###LBLSAFECOMMENTS" + i,
                      header: "###LBLSAFECOMMENTS" + i,
                    });
                    this.cols.push({
                      field: "###LBLUNSAFECOMMENTS" + i,
                      header: "###LBLUNSAFECOMMENTS" + i,
                    });
                  }
                }
              }
            });
          this.updateRowGroupMetaData();
          this.manageExportData();
        } else {
          this.translate.get("LBLRPTNORECFND").subscribe((message) => {
            this.messageService.add({
              severity: "error",
              detail: message,
            });
          });
          this.isLoaded = true;
          this.viewFilters = true;
        }
      } else {
        this.messageService.add({
          severity: "error",
          detail: res["message"],
        });
        this.isLoaded = true;
        this.viewFilters = true;
      }
    });
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.categoryData) {
      for (let i = 0; i < this.categoryData.length; i++) {
        let tableData = this.categoryData[i];
        let maincategory = tableData.MAINCATEGORYNAME;
        if (i == 0) {
          this.rowGroupMetadata[maincategory] = { index: 0, size: 1 };
        } else {
          let previousRowData = this.categoryData[i - 1];
          let previousRowGroup = previousRowData.MAINCATEGORYNAME;
          if (maincategory === previousRowGroup) {
            this.rowGroupMetadata[maincategory].size++;
          } else {
            this.rowGroupMetadata[maincategory] = { index: i, size: 1 };
          }
        }
      }
    }
  }

  manageExportData(): void {
    this.tableRows = [];
    this.pdfRows = [];
    this.columns = [];
    if (this.obsData.length > 0 && this.cols.length > 0) {
      this.obsData.map((data, key) => {
        var dataJson = {};
        var tempRows = [];
        this.cols.map((item) => {
          dataJson[item["header"]] = data[item["field"]];
          // dataJson[this.cols[0].header] = data[this.cols[0].field];
          // dataJson[this.cols[1].header] = data[this.cols[1].field];
          tempRows.push(data[item["field"]]);
          if (this.obsData.length - 1 == key) {
            this.columns.push(item.header);
          }
        });
        this.tableRows.push(dataJson);
        this.pdfRows.push(tempRows);
      });
      this.exportData();
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ["data"],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "csv",
      type: "array",
    });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const EXCEL_EXTENSION = ".csv";
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    fs.saveAs(data, fileName + "_" + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportData(): void {
    //Excel Title, Header, Data
    const header = this.columns;
    const data = this.tableRows;
    const excelFileName = "DATADUMPREPORT";
    var hslNumbers = localStorage
      .getItem("CustomColor")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customColor = this.reportService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );

    var hslNumbers = localStorage
      .getItem("CustomFont")
      .match(/\d+/g)
      .map((n) => parseInt(n));
    const customFontColor = this.reportService.convertHslToHex(
      hslNumbers[0],
      hslNumbers[1],
      hslNumbers[2]
    );
    const EXCEL_TYPE =
      "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8";

    const EXCEL_EXTENSION = ".xlsx";

    //Create workbook and worksheet
    let workbook = new ExcelJS.Workbook();
    // Report scope sheet
    let firstWorksheet = workbook.addWorksheet("Report Scope");
    firstWorksheet.addRow([]);
    this.translate.get("LBLDETAILS").subscribe((title) => {
      let firstHeaderRow = firstWorksheet.addRow([title]);
    });

    firstWorksheet.getCell("A2").font = {
      name: "Arial Unicode MS",
      family: 4,
      size: 18,
      bold: true,
      color: { argb: "00000000" },
    };
    firstWorksheet.getCell("A2").alignment = {
      vertical: "middle",
      horizontal: "left",
      indent: 1,
    };
    firstWorksheet.getColumn("A").width = 100;
    firstWorksheet.getRow(2).height = 30;

    this.translate
      .get(["LBLOBSDATE", "LBLCREATEDDATE"])
      .subscribe((resLabel) => {
        if (this.dumpParam["OBSDATE"]) {
          firstWorksheet.addRow([]);
          firstWorksheet.addRow([]);
          firstWorksheet.addRow([resLabel["LBLOBSDATE"]]).eachCell((cell) => {
            cell.font = {
              name: "Arial Unicode MS",
              family: 4,
              size: 14,
              bold: true,
              color: { argb: "00000000" },
            };
            cell.alignment = {
              vertical: "middle",
              horizontal: "left",
              indent: 2,
            };
          });
          firstWorksheet.lastRow.height = 25;
          var bothDate = this.dumpParam["OBSDATE"].split("-");
          var fDate = formatDate(
            bothDate[0],
            this.auth.UserInfo["dateFormat"],
            this.translate.getDefaultLang()
          );
          var tDate = formatDate(
            bothDate[1],
            this.auth.UserInfo["dateFormat"],
            this.translate.getDefaultLang()
          );
          firstWorksheet
            .addRow([
              fDate +
                " - " +
                tDate +
                " (" +
                this.auth.UserInfo["dateFormat"].toUpperCase() +
                ")",
            ])
            .eachCell((cell) => {
              cell.font = {
                name: "Arial Unicode MS",
                family: 4,
                size: 12,
                color: { argb: "00000000" },
              };
              cell.alignment = {
                vertical: "middle",
                horizontal: "left",
                indent: 3,
              };
            });
        }

        firstWorksheet.lastRow.height = 25;
      });

    // Report View sheet
    let worksheet = workbook.addWorksheet("Sheet1");

    var beforeMainCatCol = 17;
    var beforeSubCatCol = 17;
    var mainCatColCount = 17;
    var subCatColCount = 17;
    var startMainMergeCell = "R";
    var startSubMergeCell = "R";
    var endMainMergeCell;
    var endSubMergeCell;
    var maincategory = Object.keys(this.rowGroupMetadata);
    var addMoreCol = 2;
    if (
      this.dumpParam["INCLUDECOMMENTS"] &&
      this.dumpParam["INCLUDECOMMENTS"] == "1"
    ) {
      addMoreCol = 4;
    }

    maincategory.map((mainCat) => {
      var subCatgory = this.categoryData.filter((catData) => {
        return catData.MAINCATEGORYNAME == mainCat;
      });
      mainCatColCount = mainCatColCount + subCatgory.length * addMoreCol;
      endMainMergeCell = excelColumnName.intToExcelCol(mainCatColCount);
      worksheet.mergeCells(startMainMergeCell + "1:" + endMainMergeCell + "1");
      worksheet.getRow(1).getCell(beforeMainCatCol + 1).value = mainCat;
      worksheet.getRow(1).height = 40;
      var mainCol = excelColumnName.intToExcelCol(beforeMainCatCol + 1);
      worksheet.getCell(mainCol + "1").fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "FF9900" },
      };
      worksheet.getCell(mainCol + "1").alignment = {
        vertical: "middle",
        horizontal: "center",
      };
      worksheet.getCell(mainCol + "1").border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
      beforeMainCatCol = mainCatColCount;
      startMainMergeCell = excelColumnName.intToExcelCol(mainCatColCount + 1);
      subCatgory.map((subCat) => {
        subCatColCount = subCatColCount + addMoreCol;
        endSubMergeCell = excelColumnName.intToExcelCol(subCatColCount);
        worksheet.mergeCells(startSubMergeCell + "2:" + endSubMergeCell + "2");
        worksheet.getRow(2).getCell(beforeSubCatCol + 1).value =
          subCat["SUBCATEGORYNAME"];
        worksheet.getRow(2).height = 30;
        var subCol = excelColumnName.intToExcelCol(beforeSubCatCol + 1);
        worksheet.getCell(subCol + "2").fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "CCCC00" },
        };
        worksheet.getCell(subCol + "2").alignment = {
          vertical: "middle",
          horizontal: "center",
        };
        worksheet.getCell(subCol + "2").border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
        beforeSubCatCol = subCatColCount;
        startSubMergeCell = excelColumnName.intToExcelCol(subCatColCount + 1);
      });
    });
    //Add Header Row
    let headerRow = worksheet.addRow(header);
    headerRow.height = 20;
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      this.translate
        .get(["LBLSAFE", "LBLUNSAFE", "LBLSAFECOMMENTS", "LBLUNSAFECOMMENTS"])
        .subscribe((resLabel) => {
          if (number > 17) {
            if (number % 2 != 0) {
              if (cell.value.search("LBLUNSAFECOMMENTS") != -1) {
                cell.value = resLabel["LBLUNSAFECOMMENTS"];
              } else {
                cell.value = resLabel["LBLUNSAFE"];
              }
            } else {
              if (cell.value.search("LBLSAFECOMMENTS") != -1) {
                cell.value = resLabel["LBLSAFECOMMENTS"];
              } else {
                cell.value = resLabel["LBLSAFE"];
              }
            }
          }
        });
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: customColor },
      };
      cell.font = {
        color: { argb: customFontColor },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
      cell.alignment = { vertical: "middle", horizontal: "left" };
      // if (number == 2) {
      //   cell.alignment = { vertical: "middle", horizontal: "center" };
      // } else {
      //   cell.alignment = { vertical: "middle", horizontal: "left" };
      // }
    });
    // Add Data and Conditional Formatting
    let sumOfData = 0;
    data.forEach((element) => {
      let eachRow = [];
      this.columns.map((headers, key) => {
        if (key == 1) {
          sumOfData = sumOfData + element[headers];
        }
        eachRow.push(element[headers]);
      });
      if (element.isDeleted === "Y") {
        let deletedRow = worksheet.addRow(eachRow);
        deletedRow.eachCell((cell, number) => {
          cell.font = {
            name: "Calibri",
            family: 4,
            size: 11,
            bold: false,
            strike: true,
          };
          cell.alignment = {
            vertical: "middle",
            horizontal: "left",
          };
          cell.numFmt = this.auth.changeCellValueType(cell.value);
        });
      } else {
        worksheet.addRow(eachRow).eachCell((cell, number) => {
          cell.alignment = {
            vertical: "middle",
            horizontal: "left",
          };
          cell.numFmt = this.auth.changeCellValueType(cell.value);
        });
      }
    });
    // let finalRow = worksheet.addRow(["Grand Total", sumOfData]);
    // finalRow.getCell(2).alignment = {
    //   vertical: "middle",
    //   horizontal: "center",
    // };
    // finalRow.eachCell((cell) => {
    //   cell.font = {
    //     size: 12,
    //     bold: true,
    //   };
    // });

    header.map((item, key) => {
      if (key == 3) {
        worksheet.getColumn(key + 1).width = 30;
      } else if (key > 11) {
        worksheet.getColumn(key + 1).width = 25;
      } else {
        worksheet.getColumn(key + 1).width = 20;
      }
    });

    worksheet.addRow([]);
    this.isLoaded = true;
    this.viewFilters = true;
    //Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: EXCEL_TYPE });
      fs.saveAs(
        blob,
        excelFileName + "_" + new Date().getTime() + EXCEL_EXTENSION
      );
    });
  }

  onSelectAllFilterList(i) {
    const selected = this.filterList[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.filterList[i].REPORTPARAM] = selected;
  }

  onClearAllFilterList(i) {
    this.selectedFilter[this.filterList[i].REPORTPARAM] = [];
  }
  onSelectAllFilterOptional(i) {
    const selected = this.filterOptional[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.filterOptional[i].REPORTPARAM] = selected;
  }

  onClearAllFilterOptional(i) {
    this.selectedFilter[this.filterOptional[i].REPORTPARAM] = [];
  }

  onSelectAllFilterChartType(i) {
    const selected = this.filterChartType[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.filterChartType[i].REPORTPARAM] = selected;
  }
  onClearAllFilterChartType(i) {
    this.selectedFilter[this.filterChartType[i].REPORTPARAM] = [];
  }

  onSelectAllFilterCategory(i) {
    const selected = this.filterCategory[i]["Data"].map((item) => item.value);
    this.selectedFilter[this.filterCategory[i].REPORTPARAM] = selected;
  }
  onClearAllFilterCategory(i) {
    this.selectedFilter[this.filterCategory[i].REPORTPARAM] = [];
  }

  clearAllFilters() {
    this.selectedFilter = [];
    this.selectedDateFilters = false;
  }

  changeFilter(event) {
    if (event.target.value) {
      this.saveFilterDisabled = false;
    } else {
      this.saveFilterDisabled = true;
    }
  }
}
