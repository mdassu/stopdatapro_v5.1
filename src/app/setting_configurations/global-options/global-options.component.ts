import { map } from "rxjs/operators";
import { Component, ViewContainerRef, OnInit, ViewChild } from "@angular/core";

import { ColorPickerService, Cmyk, Hsla } from "ngx-color-picker";
import { NgForm } from "@angular/forms";
import { BreadcrumbService } from "../../breadcrumb.service";
import { TreeNode, SelectItem, LazyLoadEvent } from "primeng/primeng";
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
} from "@angular/forms";
import { Message } from "primeng/primeng";
import { TooltipModule } from "primeng/tooltip";

import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService, MessageService } from "primeng/api";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";

import { SettingsService } from "../../_services/settings.service";
import { FileUpload } from "primeng/fileupload";
import { UtilService } from "src/app/_services/util.service";
import { TranslateService } from "@ngx-translate/core";
import { UploadImageService } from "../../_services/upload-image.service";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { EnvService } from "src/env.service";
import { GlobalDataService } from "src/app/_services/global-data.service";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";
@Component({
  selector: "app-global-options",
  templateUrl: "./global-options.component.html",
  styleUrls: ["./global-options.component.css"],
  providers: [ConfirmationService, MessageService, CustomValidatorsService],
})
export class GlobalOptionsComponent implements OnInit {
  // Color Picker
  public selectedColor: string = "customColor";
  public customColor: string = "#009688";

  public selectedColorFont: string = "customFont";
  public customFont: string = "#ff0000";

  isLoading: boolean = true;

  //General Setting
  AddGlobalGeneral: FormGroup;
  submitted: boolean;
  autoLogInterval: any;
  defaultDateFormat: any = [
    { dateType: " DD/MM/YYYY", value: 103 },
    { dateType: "MM/DD/YYYY", value: 101 },
    { dateType: " YYYY/MM/DD", value: 111 },
  ];
  enableLogin: any;
  loginAttempt: SelectItem[];
  // customColor: string = '#d5d5d5';
  // customFont: string = '#c7572e';
  newCustomColor: any;
  newCustomFont: any;
  pwdLength: any;
  pwdComplexity: SelectItem[];
  pwdExpPeriod: any;
  uploadImage: any[];
  usernameLength: any;
  loginAttemps: boolean = true;
  numericShow: boolean = true;
  alphaShow: boolean = true;
  specShow: boolean = true;
  generalData: any;
  fileToUpload: any = "";

  //Application Setting
  ApplicationConfiguration: FormGroup;
  applicationSubmitted: boolean;
  languages: any;
  groupType: any;
  EmailType: any;
  timeZones: any;
  defaultLanguage: SelectItem[];
  mandatoryGroup: any = [];
  defaultTimeZone: SelectItem[];
  mandatoryGroups: any;

  //CheckList Configuration Setting
  CheckListConfigurationForm: FormGroup;
  submittedCheckList: boolean;
  AreaName: SelectItem[];
  msgs: Message[];
  roleObservation: boolean = true;
  roles: any;
  definedfields: any;
  mdefinedfields: any;

  enableArr: SelectItem[];
  uploadedFiles: any[] = [];
  ByDefaultChecklist: any;
  filedName: any;
  selectedValue: string;

  index = 0;
  lastIndex = 0;
  filestring: any;
  imageData: any;
  formData: any;
  imageUrl: any;
  imageName: any;
  uploadedImage: any = "";
  appName: any;
  defaultLogoStatus: any;
  generalLoading: boolean = false;
  applicationLoading: boolean = false;
  checklistLoading: boolean = false;
  companyLogo: any;
  // configurationOBSChecklist: any;

  @ViewChild(FileUpload)
  fileUpload: FileUpload;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private fb: FormBuilder,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private cookieService: CookieService,
    private settingservice: SettingsService,
    private route: Router,
    public vcRef: ViewContainerRef,
    private cpService: ColorPickerService,
    private util: UtilService,
    private translate: TranslateService,
    private auth: AuthenticationService,
    private env: EnvService,
    private uploadImageService: UploadImageService,
    private globalData: GlobalDataService,
    private customValidatorsService: CustomValidatorsService
  ) {
    this.breadcrumbService.setItems([
      {
        label: "LBLSETTINGSMGMT",
        url: "./assets/help/settings-and-config-general.md",
      },
      { label: "LBLGLOBALOPTIONS" },
    ]);
  }

  ngOnInit() {
    var data = {
      usernameLength: "",
      enableLogin: 0,
      loginAttempt: "1",
      pwdLength: "",
      pwdComplexity: "",
      pwdExpPeriod: "",
      autoLogInterval: "",
      defaultDateFormat: "",
      uploadImage: "",
      customColor: "",
      customFont: "",
      pwdMinnumChar: "",
      pwdMinalphaChar: "",
      pwdMinSpecChar: "",
    };
    this.createFormBuilder(data);
    this.getGeneralData();

    var appFormData = {
      languageList: "",
      cmbdefaultLang: "",
      groupTypeList: "",
      groupTypeId: "",
      sheduleTypeList: "",
      timeZoneList: "",
      cmbDefaultTimeZone: "",
    };
    this.applicationFormBuilder(appFormData);
    this.getApplicationData();

    // Checklist Configuration
    var checkFormData = {
      enableComments: 1,
      maxObserved: "",
      checklistOlderLimit: "",
      expandCategory: "",
      obsListingxMonth: "",
      obsApprovePc: "",
      obsApproveMobile: "",
      roleList: "",
      userDefinedList: "",
      customField: "",
    };

    this.ckecklistFormBuilder(checkFormData);
    this.getChaklistData();

    // loginAttenp

    this.enableLogin = [];
    this.enableArr = [];
    this.pwdComplexity = [];
    this.translate
      .get([
        "LBLYES",
        "LBLNO",
        "LBLNONE",
        "LBLALPHA",
        "LBLNUMERIC",
        "LBLALPHASPECIAL",
      ])
      .subscribe((res: Object) => {
        this.enableLogin = [];
        this.enableLogin.push({ label: res["LBLYES"], id: 1 });
        this.enableLogin.push({ label: res["LBLNO"], id: 0 });
        this.enableArr = [];
        this.enableArr.push({ label: res["LBLYES"], value: 1 });
        this.enableArr.push({ label: res["LBLNO"], value: 0 });
        this.pwdComplexity = [];
        this.pwdComplexity.push({ label: res["LBLNONE"], value: "0" });
        this.pwdComplexity.push({ label: res["LBLNUMERIC"], value: "1" });
        this.pwdComplexity.push({ label: res["LBLALPHA"], value: "2" });
        this.pwdComplexity.push({
          label: res["LBLALPHASPECIAL"],
          value: "3",
        });
      });

    this.loginAttempt = [];
    this.loginAttempt.push({ label: "1", value: "1" });
    this.loginAttempt.push({ label: "2", value: "2" });
    this.loginAttempt.push({ label: "3", value: "3" });
    this.loginAttempt.push({ label: "4", value: "4" });
    this.loginAttempt.push({ label: "5", value: "5" });
    this.appName = this.env.appName;
    // this.configurationOBSChecklist = parseInt(
    //   this.env.configurationOBSChecklist
    // );
  }

  //Get General Configuration
  getGeneralData() {
    this.settingservice.getGeneralData().subscribe((res) => {
      if (res["status"] == true) {
        var generalData = res["GeneralData"];
        if (parseInt(generalData.EnableLoginAttempts) == 1) {
          this.loginAttemps = false;
        }
        this.imageUrl =
          this.auth.UserInfo["uploadFileUrl"] + this.auth.UserInfo["companyId"];
        this.uploadedImage = generalData.CompanyLogo;
        this.companyLogo = generalData.CompanyLogo;
        this.defaultLogoStatus = parseInt(generalData.DefaultCompanyLogo);
        let imgCompanylogo = document.getElementById("imgCompanylogo") as HTMLImageElement;
        if (generalData.CompanyLogo == "") {
          this.uploadedImage = "logo_dupont.png";
          this.imageUrl = location.href + "/assets/images/";
          imgCompanylogo.src = location.href + "/assets/images/logo_dupont.png";
        } else {
          imgCompanylogo.src = this.imageUrl + "/" + this.uploadedImage;
        }

        this.customColor = generalData.CustomColor;
        this.customFont = generalData.CustomFont;

        var data = {
          usernameLength: generalData.UserNameMinLength,
          enableLogin: generalData.EnableLoginAttempts,
          loginAttempt: generalData.LoginAttempts,
          pwdLength: generalData.PasswordMinLength,
          pwdExpPeriod: generalData.PasswordExpirationPeriod,
          autoLogInterval: generalData.AutoLogout,
          defaultDateFormat: generalData.DefaultDateFormat,
          uploadImage: "",
          customColor: generalData.CustomColor,
          customFont: generalData.CustomFont,
          pwdComplexity: generalData.PasswordComplexity,
          pwdMinnumChar: generalData.PasswordMinNumericalChar,
          pwdMinalphaChar: generalData.PasswordMinAlphaChar,
          pwdMinSpecChar: generalData.PasswordMinSpecialChar,
        };
        this.createFormBuilder(data);

        if (generalData.PasswordComplexity == 2) {
          this.numericShow = false;
          this.alphaShow = false;
          this.specShow = true;

          this.AddGlobalGeneral.controls["pwdMinnumChar"].setValidators([
            Validators.required,
            Validators.maxLength(2),
            Validators.pattern("[0-9]*"),
            Validators.max(24),
            Validators.min(1),
          ]);
          this.AddGlobalGeneral.controls["pwdMinalphaChar"].setValidators([
            Validators.required,
            Validators.maxLength(2),
            Validators.pattern("[0-9]*"),
            Validators.max(24),
            Validators.min(1),
          ]);

          this.AddGlobalGeneral.get("pwdMinSpecChar").clearValidators();
        } else if (generalData.PasswordComplexity == 3) {
          this.numericShow = false;
          this.alphaShow = false;
          this.specShow = false;
          this.AddGlobalGeneral.controls["pwdMinnumChar"].setValidators([
            Validators.required,
            Validators.maxLength(2),
            Validators.pattern("[0-9]*"),
            Validators.max(24),
            Validators.min(1),
          ]);
          this.AddGlobalGeneral.controls["pwdMinalphaChar"].setValidators([
            Validators.required,
            Validators.maxLength(2),
            Validators.pattern("[0-9]*"),
            Validators.max(24),
            Validators.min(1),
          ]);
          this.AddGlobalGeneral.controls["pwdMinSpecChar"].setValidators([
            Validators.required,
            Validators.maxLength(2),
            Validators.pattern("[0-9]*"),
            Validators.max(24),
            Validators.min(1),
          ]);
        } else {
          this.numericShow = true;
          this.alphaShow = true;
          this.specShow = true;
          this.AddGlobalGeneral.get("pwdMinnumChar").clearValidators();
          this.AddGlobalGeneral.get("pwdMinalphaChar").clearValidators();
          this.AddGlobalGeneral.get("pwdMinSpecChar").clearValidators();
        }
        this.AddGlobalGeneral.get("pwdMinnumChar").updateValueAndValidity();
        this.AddGlobalGeneral.get("pwdMinalphaChar").updateValueAndValidity();
        this.AddGlobalGeneral.get("pwdMinSpecChar").updateValueAndValidity();
      }
      this.isLoading = false;
    });
  }
  fileChangeEvent(event) {
    this.fileToUpload = this.fileUpload.files;
    this.fileToUpload = this.fileUpload.files;
    var reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsBinaryString(this.fileToUpload[0]);
    this.AddGlobalGeneral.get("uploadImage").markAsDirty();
  }

  fileRemoveEvent(event) {
    this.filestring = "";
    this.imageName = "";
    this.imageData = {};
    this.formData.delete("hdndata");
  }
  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.filestring = btoa(binaryString);
    this.imageName = this.fileToUpload[0]["name"];
    this.imageData = {
      action: "binarydata",
      filepath: this.auth.UserInfo["companyId"] + "/",
      companyid: this.auth.UserInfo["companyId"],
      filename: this.fileToUpload[0]["name"],
      binaryimgdata: this.filestring,
    };
    this.formData = new FormData();
    this.formData.append("hdndata", JSON.stringify(this.imageData));
  }

  get uform() {
    return this.AddGlobalGeneral.controls;
  }
  submitGeneral(value: string) {
    this.submitted = true;
    if (this.AddGlobalGeneral.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.generalLoading = true;
      var passLenght = this.AddGlobalGeneral.value["pwdLength"];
      if (this.AddGlobalGeneral.value["pwdComplexity"] == 2) {
        var totPass =
          parseInt(this.AddGlobalGeneral.value["pwdMinnumChar"]) +
          parseInt(this.AddGlobalGeneral.value["pwdMinalphaChar"]);
      } else if (this.AddGlobalGeneral.value["pwdComplexity"] == 3) {
        var totPass =
          parseInt(this.AddGlobalGeneral.value["pwdMinnumChar"]) +
          parseInt(this.AddGlobalGeneral.value["pwdMinalphaChar"]) +
          parseInt(this.AddGlobalGeneral.value["pwdMinSpecChar"]);
      }

      if (passLenght < totPass) {
        this.translate.get("ALTPWDREQUIRED").subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
        this.generalLoading = false;
      } else {
        if (this.newCustomColor == undefined) {
          this.AddGlobalGeneral.value["customColor"] = this.customColor;
        } else {
          this.AddGlobalGeneral.value["customColor"] = this.newCustomColor;
        }
        if (this.newCustomFont == undefined) {
          this.AddGlobalGeneral.value["customFont"] = this.customFont;
        } else {
          this.AddGlobalGeneral.value["customFont"] = this.newCustomFont;
        }

        this.AddGlobalGeneral.value["uploadImage"] = this.imageName;
        localStorage.setItem("imageName", this.imageName);
        localStorage.setItem(
          "AutoLogoutInterval",
          this.AddGlobalGeneral.controls["autoLogInterval"].value
        );

        if (this.imageData != undefined) {
          this.uploadImageService
            .uploadImage(this.formData)
            .subscribe((res) => {
              this.uploadImageService.changeImage(this.imageName);
            });
        } else if (this.defaultLogoStatus == 1) {
          this.AddGlobalGeneral.value["uploadImage"] = "";
        }
        this.settingservice
          .updateGeneral(this.AddGlobalGeneral.value)
          .subscribe((resGeneral) => {
            if (resGeneral["status"] == true) {
              if (
                this.AddGlobalGeneral.value["customColor"].indexOf("#") != -1
              ) {
                this.util.themeChange(
                  this.util.hexToHSL(this.AddGlobalGeneral.value["customColor"])
                );
              } else {
                this.util.themeChange(
                  this.AddGlobalGeneral.value["customColor"]
                );
              }

              if (
                this.AddGlobalGeneral.value["customFont"].indexOf("#") != -1
              ) {
                this.util.fontChange(
                  this.util.hexToHSL(this.AddGlobalGeneral.value["customFont"])
                );
              } else {
                this.util.fontChange(this.AddGlobalGeneral.value["customFont"]);
              }

              // this.util.themeChange(this.AddGlobalGeneral.value["customColor"]);
              // this.util.fontChange(this.AddGlobalGeneral.value["customFont"]);
              localStorage.setItem(
                "CustomColor",
                this.AddGlobalGeneral.value["customColor"]
              );
              localStorage.setItem(
                "CustomFont",
                this.AddGlobalGeneral.value["customFont"]
              );
              localStorage.setItem(
                "customFont",
                this.AddGlobalGeneral.value["customFont"]
              );
              this.translate
                .get(resGeneral["message"])
                .subscribe((res: string) => {
                  this.messageService.add({
                    severity: "success",
                    detail: res,
                  });
                });
              this.uploadedImage = "";
              this.generalLoading = false;
            } else {
              this.translate
                .get(resGeneral["message"])
                .subscribe((res: string) => {
                  this.messageService.add({
                    severity: "error",
                    detail: res,
                  });
                });
              this.uploadedImage = "";
              this.generalLoading = false;
            }
            this.getGeneralData();
          });
      }
    }
  }

  //Get Application Configuration
  getApplicationData() {
    this.settingservice.getApplicationData().subscribe((appRes) => {
      if (appRes["status"] == true) {
        var applicationData = appRes["GeneralData"];

        // this.languages = appRes["language"];
        this.languages = [];
        const lang = appRes["language"];
        lang.map((data) => {
          this.languages.push({
            label: data["LANGUAGENAME"],
            value: data["LANGUAGEID"],
          });
        });

        // this.groupType = appRes["entity"];
        let groupType = appRes["entity"];
        this.groupType = [];
        groupType.map((data) => {
          this.groupType.push({
            label: data["ENTITYVALUE"],
            value: data["ENTITYID"],
          });
        });

        // this.EmailType = appRes["scheduletype"];
        let EmailType = appRes["scheduletype"];
        this.EmailType = [];
        EmailType.map((data) => {
          this.EmailType.push({
            label: data["SCHEDULETYPENAME"],
            value: data["SCHEDULETYPEID"],
          });
        });

        this.mandatoryGroups = appRes["mandatoryGroupType"];
        const tempArr = appRes["timezone"];
        tempArr.map((items) => {
          items["TIMEZONENAME"] = this.translate.get(items["TIMEZONENAME"])[
            "value"
          ];
        });
        this.timeZones = [];
        tempArr.map((data) => {
          this.timeZones.push({
            label: data["TIMEZONENAME"],
            value: data["TIMEZONEID"],
          });
        });

        var mandatoryIds = "";
        var mandatoryId;
        this.mandatoryGroups.map(function (m) {
          if (m["MANDATORY"] == 1) {
            mandatoryIds += m["GROUPTYPEID"] + ",";
          }
        });
        if (mandatoryIds != "") {
          var mandatoryIds = mandatoryIds.slice(0, -1);
          mandatoryId = mandatoryIds.split(",").map(function (mandatoryIds) {
            return Number(mandatoryIds);
          });
        } else {
          mandatoryId = [];
        }

        var assignLanguageId;
        if (appRes["assignLanguage"] != "") {
          var langData = appRes["assignLanguage"];
          assignLanguageId = langData.split(",").map(function (langData) {
            return Number(langData);
          });
        } else {
          assignLanguageId = [];
        }

        var assignGroupIds;
        if (appRes["assignEntity"] != "") {
          var groupData = appRes["assignEntity"];
          assignGroupIds = groupData.split(",").map(function (groupData) {
            return Number(groupData);
          });
        } else {
          assignGroupIds = [];
        }

        var assignEmailType;
        if (appRes["assignScheduletype"] != "") {
          var emailData = appRes["assignScheduletype"];
          assignEmailType = emailData.split(",").map(function (groupData) {
            return Number(groupData);
          });
        } else {
          assignEmailType = [];
        }

        var assignTimezoneIds;
        if (appRes["assignTimezone"] != "") {
          var timeData = appRes["assignTimezone"];
          assignTimezoneIds = timeData.split(",").map(function (timeData) {
            return Number(timeData);
          });
        } else {
          assignTimezoneIds = [];
        }

        var appFormData = {
          languageList: assignLanguageId,
          cmbdefaultLang: applicationData.LANGUAGEID,
          groupTypeList: assignGroupIds,
          groupTypeId: mandatoryId,
          sheduleTypeList: assignEmailType,
          timeZoneList: assignTimezoneIds,
          cmbDefaultTimeZone: applicationData.TIMEZONE,
        };
        this.applicationFormBuilder(appFormData);
        this.getLanguage();
        this.getGroup();
        this.getTimeZone();
      }
    });
  }

  getLanguage() {
    var langId = this.ApplicationConfiguration.value["languageList"];
    var fn = this;
    this.defaultLanguage = [];
    if (langId.length > 0) {
      langId.forEach(function (id) {
        var lang;
        lang = fn.languages.filter(function (lang) {
          return lang["value"] == id;
        });
        fn.defaultLanguage.push(lang[0]);
      });
    } else {
      fn.defaultLanguage = [];
      this.ApplicationConfiguration.controls["cmbdefaultLang"].setValue(null);
    }
  }

  getGroup() {
    var groupId = this.ApplicationConfiguration.value["groupTypeList"];
    var fn = this;
    this.mandatoryGroup = [];
    if (groupId.length > 0) {
      groupId.forEach(function (id) {
        var group;
        group = fn.groupType.filter(function (group) {
          return group["value"] == id;
        });
        fn.mandatoryGroup.push(group[0]);
      });
      if (this.ApplicationConfiguration.value["groupTypeId"]) {
        this.mandatoryGroup = this.auth.rearrangeSelects(
          this.mandatoryGroup,
          this.ApplicationConfiguration.value["groupTypeId"]
        );
      }
    } else {
      fn.mandatoryGroup = [];
      this.ApplicationConfiguration.controls["groupTypeId"].setValue(null);
    }
  }

  getTimeZone() {
    var timezoneId = this.ApplicationConfiguration.value["timeZoneList"];
    var fn = this;
    this.defaultTimeZone = [];
    if (timezoneId.length > 0) {
      timezoneId.forEach(function (id) {
        var timezone;
        timezone = fn.timeZones.filter(function (timezone) {
          return timezone["value"] == id;
        });
        fn.defaultTimeZone.push(timezone[0]);
      });
    } else {
      fn.defaultTimeZone = [];
      this.ApplicationConfiguration.controls["cmbDefaultTimeZone"].setValue(
        null
      );
    }
  }

  get aform() {
    return this.ApplicationConfiguration.controls;
  }
  submitApplication(value: string) {
    this.applicationSubmitted = true;
    if (this.ApplicationConfiguration.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.applicationLoading = true;
      if (
        this.ApplicationConfiguration.value["groupTypeId"] != "" &&
        this.ApplicationConfiguration.value["groupTypeId"] != null
      ) {
        this.ApplicationConfiguration.value[
          "groupTypeId"
        ] = this.ApplicationConfiguration.value["groupTypeId"].join();
      } else {
        this.ApplicationConfiguration.value["groupTypeId"] = "";
      }

      if (
        this.ApplicationConfiguration.value["groupTypeList"] != "" &&
        this.ApplicationConfiguration.value["groupTypeList"] != null
      ) {
        this.ApplicationConfiguration.value[
          "groupTypeList"
        ] = this.ApplicationConfiguration.value["groupTypeList"].join();
      } else {
        this.ApplicationConfiguration.value["groupTypeList"] = "";
      }

      if (
        this.ApplicationConfiguration.value["languageList"] != "" &&
        this.ApplicationConfiguration.value["languageList"] != null
      ) {
        this.ApplicationConfiguration.value[
          "languageList"
        ] = this.ApplicationConfiguration.value["languageList"].join();
      } else {
        this.ApplicationConfiguration.value["languageList"] = "";
      }

      if (
        this.ApplicationConfiguration.value["sheduleTypeList"] != "" &&
        this.ApplicationConfiguration.value["sheduleTypeList"] != null
      ) {
        this.ApplicationConfiguration.value[
          "sheduleTypeList"
        ] = this.ApplicationConfiguration.value["sheduleTypeList"].join();
      } else {
        this.ApplicationConfiguration.value["sheduleTypeList"] = "";
      }

      if (
        this.ApplicationConfiguration.value["timeZoneList"] != "" &&
        this.ApplicationConfiguration.value["timeZoneList"] != null
      ) {
        this.ApplicationConfiguration.value[
          "timeZoneList"
        ] = this.ApplicationConfiguration.value["timeZoneList"].join();
      } else {
        this.ApplicationConfiguration.value["timeZoneList"] = "";
      }

      this.settingservice
        .updateApplication(this.ApplicationConfiguration.value)
        .subscribe((resGeneral) => {
          if (resGeneral["status"] == true) {
            this.getApplicationData();
            this.translate
              .get(resGeneral["message"])
              .subscribe((res: string) => {
                this.messageService.add({
                  severity: "success",
                  detail: res,
                });
              });
            this.applicationLoading = false;
          } else {
            this.getApplicationData();
            this.translate
              .get(resGeneral["message"])
              .subscribe((res: string) => {
                this.messageService.add({
                  severity: "error",
                  detail: res,
                });
              });
            this.applicationLoading = false;
          }
          this.getApplicationData();
        });
    }
  }

  //Get CheckList Configuration
  getChaklistData() {
    this.settingservice.getCheckListData().subscribe((checkRes) => {
      if (checkRes["status"] == true) {
        var checkData = checkRes["GeneralData"];
        if (parseInt(checkData.OBSAPPROVALMOBILE) == 1) {
          this.roleObservation = false;
        }
        if (parseInt(checkData.OBSAPPROVALPC) == 1) {
          this.roleObservation = false;
        }
        // this.roles = checkRes["role"];
        this.roles = [];
        let roles = checkRes["role"];
        roles.map((data) => {
          this.roles.push({
            label: data["ROLENAME"],
            value: data["ROLEID"],
          });
        });

        // this.definedfields = checkRes["entity"];
        this.definedfields = [];
        let definedfields = checkRes["entity"];
        definedfields.map((data) => {
          this.definedfields.push({
            label: data["ENTITYVALUE"],
            value: data["ENTITYID"],
          });
        });

        // this.mdefinedfields = checkRes["mandUserFeilds"];
        this.mdefinedfields = [];
        let mdefinedfields = checkRes["mandUserFeilds"];
        mdefinedfields.map((data) => {
          this.mdefinedfields.push({
            label: data["ENTITYVALUE"],
            value: data["ENTITYID"],
          });
        });

        var assignRole;
        if (checkRes["assignRole"] != "") {
          var roleData = checkRes["assignRole"];
          assignRole = roleData.split(",").map(function (roleData) {
            return Number(roleData);
          });
        } else {
          assignRole = [];
        }

        var assignEntity;
        if (checkRes["assignEntity"] != "") {
          var fieldData = checkRes["assignEntity"];
          assignEntity = fieldData.split(",").map(function (fieldData) {
            return Number(fieldData);
          });
        } else {
          assignEntity = [];
        }

        var mandatoryFieldIds = "";
        var mandatoryFieldId;
        mdefinedfields.map(function (m) {
          if (m["MANDATORY"] == 1) {
            mandatoryFieldIds += m["ENTITYID"] + ",";
          }
        });
        if (mandatoryFieldIds != "") {
          var mandatoryFieldIds = mandatoryFieldIds.slice(0, -1);
          mandatoryFieldId = mandatoryFieldIds
            .split(",")
            .map(function (mandatoryFieldIds) {
              return Number(mandatoryFieldIds);
            });
        } else {
          mandatoryFieldId = [];
        }

        var checkFormData = {
          enableComments: checkData.EnableChecklistComments,
          maxObserved: checkData.MaxPeopleObservedLimit,
          checklistOlderLimit: checkData.PreviousDaysLimitOnObservationDate,
          expandCategory: checkData.EXPANDCATEGORY,
          obsListingxMonth: checkData.OBSLISTINGXMONTHS,
          obsApprovePc: checkData.OBSAPPROVALPC,
          obsApproveMobile: checkData.OBSAPPROVALMOBILE,
          roleList: assignRole,
          userDefinedList: assignEntity,
          customField: mandatoryFieldId,
        };
        this.ckecklistFormBuilder(checkFormData);
        this.getUserField();
      }
    });
  }

  getUserField() {
    var userFieldId = this.CheckListConfigurationForm.value["userDefinedList"];
    var fn = this;
    this.mdefinedfields = [];
    if (userFieldId.length > 0) {
      userFieldId.forEach(function (id) {
        var userField;
        userField = fn.definedfields.filter(function (userField) {
          return userField["value"] == id;
        });
        fn.mdefinedfields.push(userField[0]);
      });
    } else {
      fn.mdefinedfields = [];
    }
  }

  get cform() {
    return this.CheckListConfigurationForm.controls;
  }
  onSubmitCheckList(value: string) {
    this.submittedCheckList = true;
    if (this.CheckListConfigurationForm.invalid) {
      this.customValidatorsService.scrollToError();
    } else {
      this.checklistLoading = true;
      var roleList = this.CheckListConfigurationForm.value["roleList"];
      if (
        this.CheckListConfigurationForm.value["obsApproveMobile"] == 1 ||
        this.CheckListConfigurationForm.value["obsApprovePc"] == 1
      ) {
        if (roleList != "") {
          var roleValid = true;
        } else {
          var roleValid = false;
        }
      } else {
        var roleValid = true;
      }
      if (roleValid == false) {
        this.translate.get("ALTASSIGNROLE").subscribe((res: string) => {
          this.messageService.add({
            severity: "error",
            detail: res,
          });
        });
      } else {
        if (
          this.CheckListConfigurationForm.value["customField"] != "" &&
          this.CheckListConfigurationForm.value["customField"] != null
        ) {
          this.CheckListConfigurationForm.value[
            "customField"
          ] = this.CheckListConfigurationForm.value["customField"].join();
        } else {
          this.CheckListConfigurationForm.value["customField"] = "";
        }

        if (
          this.CheckListConfigurationForm.value["roleList"] != "" &&
          this.CheckListConfigurationForm.value["roleList"] != null
        ) {
          this.CheckListConfigurationForm.value[
            "roleList"
          ] = this.CheckListConfigurationForm.value["roleList"].join();
        } else {
          this.CheckListConfigurationForm.value["roleList"] = "";
        }

        if (
          this.CheckListConfigurationForm.value["userDefinedList"] != "" &&
          this.CheckListConfigurationForm.value["userDefinedList"] != null
        ) {
          this.CheckListConfigurationForm.value[
            "userDefinedList"
          ] = this.CheckListConfigurationForm.value["userDefinedList"].join();
        } else {
          this.CheckListConfigurationForm.value["userDefinedList"] = "";
        }

        this.settingservice
          .updateCheckList(this.CheckListConfigurationForm.value)
          .subscribe((resGeneral) => {
            if (resGeneral["status"] == true) {
              // const currUser = JSON.parse(localStorage["currentUser"]);
              const currUser = this.auth.UserInfo;
              currUser[
                "editObservation"
              ] = this.CheckListConfigurationForm.value["maxObserved"];
              localStorage.setItem(
                "currentUser-" + currUser["userId"],
                JSON.stringify(currUser)
              );
              var oldData = this.globalData.GlobalData;
              oldData[
                "PREVIOUSDAYLIMITONOBSDATE"
              ] = this.CheckListConfigurationForm.value["checklistOlderLimit"];
              localStorage.removeItem("globalData");
              localStorage.setItem("globalData", JSON.stringify(oldData));
              this.getChaklistData();
              this.translate
                .get(resGeneral["message"])
                .subscribe((res: string) => {
                  this.messageService.add({
                    severity: "success",
                    detail: res,
                  });
                });
              this.checklistLoading = false;
            } else {
              this.checklistLoading = false;
              this.getChaklistData();
              this.translate
                .get(resGeneral["message"])
                .subscribe((res: string) => {
                  this.messageService.add({
                    severity: "error",
                    detail: res,
                  });
                });
            }
            this.submittedCheckList = false;
            // this.getChaklistData();
          });
      }
    }
  }

  changeLogin(logVal) {
    if (logVal == 1) {
      this.loginAttemps = false;
    } else {
      this.loginAttemps = true;
    }
  }

  createFormBuilder(data) {
    this.AddGlobalGeneral = this.fb.group({
      autoLogInterval: new FormControl(
        data.autoLogInterval,
        Validators.compose([
          Validators.required,
          Validators.maxLength(2),
          Validators.pattern("[0-9]*"),
          Validators.min(1),
          this.noWhitespaceValidator,
        ])
      ),
      defaultDateFormat: new FormControl(
        parseInt(data.defaultDateFormat),
        Validators.required
      ),
      uploadImage: new FormControl(data.uploadImage),
      customFont: new FormControl(data.customFont),
      customColor: new FormControl(data.customColor),
      enableLogin: new FormControl(
        parseInt(data.enableLogin),
        Validators.required
      ),
      loginAttempt: new FormControl(data.loginAttempt, Validators.required),
      usernameLength: new FormControl(
        data.usernameLength,
        Validators.compose([
          Validators.required,
          Validators.maxLength(2),
          Validators.pattern("[0-9]*"),
          Validators.max(24),
          Validators.min(1),
          this.noWhitespaceValidator,
        ])
      ),
      pwdLength: new FormControl(
        data.pwdLength,
        Validators.compose([
          Validators.required,
          Validators.maxLength(2),
          Validators.pattern("[0-9]*"),
          Validators.max(24),
          Validators.min(1),
          this.noWhitespaceValidator,
        ])
      ),
      pwdComplexity: new FormControl(data.pwdComplexity),
      pwdExpPeriod: new FormControl(
        data.pwdExpPeriod,
        Validators.compose([
          Validators.required,
          Validators.maxLength(2),
          Validators.pattern("[0-9]*"),
          this.noWhitespaceValidator,
        ])
      ),
      pwdMinnumChar: new FormControl(
        data.pwdMinnumChar,
        Validators.compose([
          Validators.required,
          Validators.maxLength(2),
          Validators.pattern("[0-9]*"),
          Validators.max(24),
          Validators.min(1),
          this.noWhitespaceValidator,
        ])
      ),
      pwdMinalphaChar: new FormControl(
        data.pwdMinalphaChar,
        Validators.compose([
          Validators.required,
          Validators.maxLength(2),
          Validators.pattern("[0-9]*"),
          Validators.max(24),
          Validators.min(1),
          this.noWhitespaceValidator,
        ])
      ),
      pwdMinSpecChar: new FormControl(
        data.pwdMinSpecChar,
        Validators.compose([
          Validators.required,
          Validators.maxLength(2),
          Validators.pattern("[0-9]*"),
          Validators.max(24),
          Validators.min(1),
          this.noWhitespaceValidator,
        ])
      ),
    });
  }

  applicationFormBuilder(appFormData) {
    this.ApplicationConfiguration = this.fb.group({
      languageList: new FormControl(
        appFormData.languageList,
        Validators.required
      ),
      cmbdefaultLang: new FormControl(
        appFormData.cmbdefaultLang,
        Validators.required
      ),
      groupTypeList: new FormControl(appFormData.groupTypeList),
      groupTypeId: new FormControl(appFormData.groupTypeId),
      sheduleTypeList: new FormControl(appFormData.sheduleTypeList),
      timeZoneList: new FormControl(
        appFormData.timeZoneList,
        Validators.required
      ),
      cmbDefaultTimeZone: new FormControl(
        parseInt(appFormData.cmbDefaultTimeZone),
        Validators.required
      ),
    });
  }

  ckecklistFormBuilder(checkFormData) {
    this.CheckListConfigurationForm = this.fb.group({
      enableComments: new FormControl(
        parseInt(checkFormData.enableComments),
        Validators.required
      ),
      maxObserved: new FormControl(
        checkFormData.maxObserved,
        Validators.compose([
          Validators.maxLength(3),
          Validators.pattern("[0-9]*"),
        ])
      ),
      checklistOlderLimit: new FormControl(
        checkFormData.checklistOlderLimit,
        Validators.compose([
          Validators.maxLength(3),
          Validators.pattern("[0-9]*"),
        ])
      ),
      expandCategory: new FormControl(
        parseInt(checkFormData.expandCategory),
        Validators.required
      ),
      obsListingxMonth: new FormControl(
        checkFormData.obsListingxMonth,
        Validators.compose([
          Validators.required,
          Validators.maxLength(2),
          Validators.pattern("[0-9]*"),
          this.noWhitespaceValidator,
        ])
      ),
      obsApprovePc: new FormControl(checkFormData.obsApprovePc),
      obsApproveMobile: new FormControl(checkFormData.obsApproveMobile),
      roleList: new FormControl(checkFormData.roleList),
      userDefinedList: new FormControl(checkFormData.userDefinedList),
      customField: new FormControl(checkFormData.customField),
    });
    if (parseInt(checkFormData.obsApprovePc) == 1) {
      this.CheckListConfigurationForm.controls["roleList"].setValidators([
        Validators.required,
      ]);
    }

    // if (this.configurationOBSChecklist == 0) {
    //   this.CheckListConfigurationForm.removeControl("obsListingxMonth");
    // }
  }

  rolesObservation(logVal) {
    var obsPc = this.CheckListConfigurationForm.value["obsApprovePc"];
    var obsMobile = this.CheckListConfigurationForm.value["obsApproveMobile"];
    if (obsPc == 1 || obsMobile == 1) {
      this.roleObservation = false;
      this.CheckListConfigurationForm.controls["roleList"].setValidators([
        Validators.required,
      ]);
    } else {
      this.roleObservation = true;
      this.CheckListConfigurationForm.controls["roleList"].clearValidators();
      this.CheckListConfigurationForm.controls[
        "roleList"
      ].updateValueAndValidity();
    }
  }

  complexityType(complexType) {
    if (complexType == 2) {
      this.numericShow = false;
      this.alphaShow = false;
      this.specShow = true;

      this.AddGlobalGeneral.controls["pwdMinnumChar"].setValidators([
        Validators.required,
        Validators.maxLength(2),
        Validators.pattern("[0-9]*"),
        Validators.max(24),
        Validators.min(1),
      ]);
      this.AddGlobalGeneral.controls["pwdMinalphaChar"].setValidators([
        Validators.required,
        Validators.maxLength(2),
        Validators.pattern("[0-9]*"),
        Validators.max(24),
        Validators.min(1),
      ]);

      this.AddGlobalGeneral.get("pwdMinSpecChar").clearValidators();
    } else if (complexType == 3) {
      this.numericShow = false;
      this.alphaShow = false;
      this.specShow = false;

      this.AddGlobalGeneral.controls["pwdMinnumChar"].setValidators([
        Validators.required,
        Validators.maxLength(2),
        Validators.pattern("[0-9]*"),
        Validators.max(24),
        Validators.min(1),
      ]);
      this.AddGlobalGeneral.controls["pwdMinalphaChar"].setValidators([
        Validators.required,
        Validators.maxLength(2),
        Validators.pattern("[0-9]*"),
        Validators.max(24),
        Validators.min(1),
      ]);
      this.AddGlobalGeneral.controls["pwdMinSpecChar"].setValidators([
        Validators.required,
        Validators.maxLength(2),
        Validators.pattern("[0-9]*"),
        Validators.max(24),
        Validators.min(1),
      ]);
    } else {
      this.numericShow = true;
      this.alphaShow = true;
      this.specShow = true;
      this.AddGlobalGeneral.get("pwdMinnumChar").clearValidators();
      this.AddGlobalGeneral.get("pwdMinalphaChar").clearValidators();
      this.AddGlobalGeneral.get("pwdMinSpecChar").clearValidators();
    }
    this.AddGlobalGeneral.get("pwdMinnumChar").updateValueAndValidity();
    this.AddGlobalGeneral.get("pwdMinalphaChar").updateValueAndValidity();
    this.AddGlobalGeneral.get("pwdMinSpecChar").updateValueAndValidity();
  }

  // ColorPicker Functionalty
  public onEventLog(event: string, data: any): void {
    this.newCustomColor = data;
  }
  public onEventLog1(event: string, data: any): void {
    this.newCustomFont = data;
  }
  onCustomFontColorChange(data: any) {
    this.AddGlobalGeneral.get("customFont").markAsDirty();
  }
  onCustomColorChange(data: any) {
    this.AddGlobalGeneral.get("customColor").markAsDirty();
  }
  globalReset() {
    this.getGeneralData();
  }

  applicationReset() {
    this.getApplicationData();
  }
  checklistReset() {
    this.getChaklistData();
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  onResetBrandColor() {
    this.customColor = this.util.defaultBrandColor;
    this.newCustomColor = this.util.defaultBrandColor;
    this.newCustomFont = this.util.defaultFontColor;
    this.customFont = this.util.defaultFontColor;
    this.util.themeChange(this.newCustomColor);
    this.util.fontChange(this.newCustomFont);
    this.AddGlobalGeneral.get("customFont").markAsDirty();
    this.AddGlobalGeneral.get("customColor").markAsDirty();
  }
  onLanguageSelectAll() {
    const selected = this.languages.map((item) => item.LANGUAGEID);
    this.ApplicationConfiguration.get("languageList").patchValue(selected);
    this.ApplicationConfiguration.get("languageList").markAsDirty();
  }
  onLanguageClearAll() {
    this.ApplicationConfiguration.get("languageList").patchValue([]);
    this.ApplicationConfiguration.get("languageList").markAsDirty();
  }
  onGroupTypeSelectAll() {
    const selected = this.groupType.map((item) => item.ENTITYID);
    this.ApplicationConfiguration.get("groupTypeList").patchValue(selected);
    this.ApplicationConfiguration.get("groupTypeList").markAsDirty();
  }
  onGroupTypeClearAll() {
    this.ApplicationConfiguration.get("groupTypeList").patchValue([]);
    this.ApplicationConfiguration.get("groupTypeList").markAsDirty();
  }
  onEmailTypeSelectAll() {
    const selected = this.EmailType.map((item) => item.SCHEDULETYPEID);
    this.ApplicationConfiguration.get("sheduleTypeList").patchValue(selected);
    this.ApplicationConfiguration.get("sheduleTypeList").markAsDirty();
  }
  onEmailTypeClearAll() {
    this.ApplicationConfiguration.get("sheduleTypeList").patchValue([]);
    this.ApplicationConfiguration.get("sheduleTypeList").markAsDirty();
  }
  onTimeZonesSelectAll() {
    const selected = this.timeZones.map((item) => item.TIMEZONEID);
    this.ApplicationConfiguration.get("timeZoneList").patchValue(selected);
    this.ApplicationConfiguration.get("timeZoneList").markAsDirty();
  }
  onTimeZonesClearAll() {
    this.ApplicationConfiguration.get("timeZoneList").patchValue([]);
    this.ApplicationConfiguration.get("timeZoneList").markAsDirty();
  }
  onMandatoryGroupSelectAll() {
    const selected = this.mandatoryGroup.map((item) => item.ENTITYID);
    this.ApplicationConfiguration.get("groupTypeId").patchValue(selected);
    this.ApplicationConfiguration.get("groupTypeId").markAsDirty();
  }
  onMandatoryGroupClearAll() {
    this.ApplicationConfiguration.get("groupTypeId").patchValue([]);
    this.ApplicationConfiguration.get("groupTypeId").markAsDirty();
  }
  onRolesSelectAll() {
    const selected = this.roles.map((item) => item.ROLEID);
    this.CheckListConfigurationForm.get("roleList").patchValue(selected);
    this.CheckListConfigurationForm.get("roleList").markAsDirty();
  }
  onRolesClearAll() {
    this.CheckListConfigurationForm.get("roleList").patchValue([]);
    this.CheckListConfigurationForm.get("roleList").markAsDirty();
  }
  onDefinedfieldsSelectAll() {
    const selected = this.definedfields.map((item) => item.ENTITYID);
    this.CheckListConfigurationForm.get("userDefinedList").patchValue(selected);
    this.CheckListConfigurationForm.get("userDefinedList").markAsDirty();
  }
  onDefinedfieldsClearAll() {
    this.CheckListConfigurationForm.get("userDefinedList").patchValue([]);
    this.CheckListConfigurationForm.get("userDefinedList").markAsDirty();
  }
  onMdefinedfieldsSelectAll() {
    const selected = this.mdefinedfields.map((item) => item.ENTITYID);
    this.CheckListConfigurationForm.get("customField").patchValue(selected);
    this.CheckListConfigurationForm.get("customField").markAsDirty();
  }
  onMdefinedfieldsClearAll() {
    this.CheckListConfigurationForm.get("customField").patchValue([]);
    this.CheckListConfigurationForm.get("customField").markAsDirty();
  }

  handleChange(e) {
    if (
      (this.AddGlobalGeneral.dirty && this.lastIndex === 0) ||
      (this.ApplicationConfiguration.dirty && this.lastIndex === 1) ||
      (this.CheckListConfigurationForm.dirty && this.lastIndex === 2)
    ) {
      this.translate.get("LBLCONFIRMYESNO").subscribe((text: string) => {
        this.confirmationService.confirm({
          message: text,

          accept: () => {
            if (this.lastIndex === 0) {
              this.getGeneralData();
            } else if (this.lastIndex === 1) {
              this.getApplicationData();
            } else {
              this.getChaklistData();
            }
            this.lastIndex = e.index;
            this.setHelpFile(e.index);
          },
          reject: () => {
            this.index = this.lastIndex;
            this.setHelpFile(this.lastIndex);
          },
        });
      });
    } else {
      this.lastIndex = e.index;
      this.setHelpFile(e.index);
    }
  }

  setHelpFile(index) {
    switch (index) {
      case 0:
        this.breadcrumbService.setItems([
          {
            label: "LBLSETTINGSMGMT",
            url: "./assets/help/settings-and-config-general.md",
          },
          { label: "LBLGLOBALOPTIONS" },
        ]);
        break;
      case 1:
        this.breadcrumbService.setItems([
          {
            label: "LBLSETTINGSMGMT",
            url: "./assets/help/settings-and-config-app-config.md",
          },
          { label: "LBLGLOBALOPTIONS" },
        ]);
        break;
      case 2:
        this.breadcrumbService.setItems([
          {
            label: "LBLSETTINGSMGMT",
            url: "./assets/help/settings-and-config-app-config.md",
          },
          { label: "LBLGLOBALOPTIONS" },
        ]);
        break;
    }
  }

  removeImage() {
    this.generalLoading = true;
    this.settingservice.removeImage().subscribe((res) => {
      this.translate.get(["ALTIMGEREMOVEDSUCCESS", "ALTIMGNOTREVMOVEDSCCESS"]).subscribe(label => {
        if (res["status"] == true) {
          this.generalLoading = false;
          this.imageName = "logo_dupont.png";
          this.uploadImageService.changeImage(this.imageName);
          this.messageService.add({
            severity: "success",
            detail: label["ALTIMGEREMOVEDSUCCESS"],
          });
          this.getGeneralData();
        } else {
          this.generalLoading = false;
          this.messageService.add({
            severity: "error",
            detail: label['ALTIMGNOTREVMOVEDSCCESS'],
          });
        }
      })
    });
  }
}
