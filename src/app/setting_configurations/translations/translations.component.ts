import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy,
} from "@angular/core";
import { BreadcrumbService } from "src/app/breadcrumb.service";
import { ChecklistService } from "src/app/_services/checklist.service";
import { SelectItem, MessageService, ConfirmationService } from "primeng/api";
import { Subscription } from "rxjs";
import { UserGroupService } from "src/app/_services/user-groups.service";
import { CookieService } from "ngx-cookie-service";
import { SettingsService } from "src/app/_services/settings.service";
import { UserDefinedFieldService } from "src/app/_services/user-defined-field.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "src/app/_services/authentication.service";
import { Router } from "@angular/router";
import { CustomValidatorsService } from "src/app/_services/custom-validators.service";

@Component({
  selector: "app-translations",
  templateUrl: "./translations.component.html",
  styleUrls: ["./translations.component.css"],
})
export class TranslationsComponent implements OnInit, OnDestroy {
  cols: Array<object> = [];
  isLoaded: boolean;
  tableValues = [];
  textsCount: any;
  selectedData: any;
  inputToggle = true;
  labelType: any;
  language: any;
  customDropdown: Array<SelectItem>;
  languages: Array<SelectItem>;
  exportData = {};
  dropDownLabel: string;
  input: string;
  subscription: Subscription;
  languageId: number;
  groupTypeId: number;
  mainCategoryId: number;
  bindValue: string;
  bindLabel: string;
  tableRowDataName: string;
  tableRowDataValue: string;
  initialData: any;
  dataKey: any;
  selectionIndexes: Array<number> = [];
  onSubmitTouched: boolean = true;

  @Input() screenType: string;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private checklistService: ChecklistService,
    private userGroupService: UserGroupService,
    private userDefinedFieldService: UserDefinedFieldService,
    private cookieService: CookieService,
    private settingService: SettingsService,
    private messageService: MessageService,
    private translate: TranslateService,
    private confirmationService: ConfirmationService,
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit() {
    if (!this.screenType) {
      this.breadcrumbService.setItems([
        {
          label: "LBLSETTINGSMGMT",
          url: "./assets/help/settings-translations.md",
        },
        { label: "LBLTRANSLATION" },
      ]);
    }
    this.selectedData = [];
    this.tableRowDataName = "ENTITYNAME";
    this.tableRowDataValue = "ENTITYVALUE";

    this.languageId = this.authenticationService.UserInfo["languageId"];
    this.groupTypeId = +this.cookieService.get("grouptypeid");
    this.mainCategoryId = +this.cookieService.get("mainCategoryId");
    const obj = {};
    if (this.screenType == "categories") {
      this.dataKey = "ENTITYID";
      this.dropDownLabel = 'LBLMAINCATEGORY';
      this.bindLabel = "MAINCATEGORYNAME";
      this.bindValue = "MAINCATEGORYID";
      this.subscription = this.checklistService
        .getCategoryTranslations({
          languageId: this.languageId,
          mainCategoryId: this.mainCategoryId,
        })
        .subscribe((res) => {
          this.language = this.languageId;
          this.labelType = this.mainCategoryId;
          this.languages = res["Language"];
          this.customDropdown = res["Category"];
          this.tableValues = res["Translation"];
          this.isLoaded = true;
          this.initialData = JSON.stringify(this.tableValues);
        });
    } else if (this.screenType == "userGroups") {
      this.dataKey = "ENTITYID";
      this.dropDownLabel = 'LBLGROUPTYPE';
      this.bindLabel = "label";
      this.bindValue = "value";
      this.subscription = this.userGroupService
        .getUserTranslationData({
          languageId: this.languageId,
          groupTypeId: this.groupTypeId,
        })
        .subscribe((res) => {
          this.language = this.languageId;
          this.labelType = this.groupTypeId;
          this.languages = res["language"];
          this.customDropdown = res["groupType"];
          this.tableValues = res["data"];
          this.isLoaded = true;
          this.initialData = JSON.stringify(this.tableValues);
        });
    } else if (this.screenType == "user-defined-fields") {
      this.dataKey = "ENTITYID";
      this.dropDownLabel = 'LBLFIELDNAME';
      this.bindLabel = "ENTITYVALUE";
      this.bindValue = "ENTITYID";
      this.userDefinedFieldService
        .getTranslationsData({ languageId: this.languageId })
        .subscribe((res) => {
          this.labelType = res["field"][0]["ENTITYID"];
          this.language = this.languageId;
          this.customDropdown = res["field"];
          this.languages = res["language"];
          this.tableValues = res["Data"];
          this.isLoaded = true;
          this.initialData = JSON.stringify(this.tableValues);
        });
    } else {
      this.dataKey = "TRANSLATIONID";
      this.dropDownLabel = "LBLTYPE";
      this.bindLabel = "label";
      this.bindValue = "value";
      this.subscription = this.settingService
        .getConfigurationTranslations({
          languageId: this.languageId,
          labelType: 1,
        })
        .subscribe((res) => {
          this.language = this.languageId;
          this.labelType = 1;
          this.languages = res["language"];
          var labelArray;
          this.translate.get(["LBLCUSTOM", "LBLGROUPS"]).subscribe(resLabel => {
            labelArray = [
              { label: resLabel['LBLCUSTOM'], value: 1 },
              { label: resLabel['LBLGROUPS'], value: 2 },
            ];
          });
          this.customDropdown = labelArray;
          this.tableValues = res["data"];
          this.isLoaded = true;
          this.initialData = JSON.stringify(this.tableValues);
          if (this.labelType === 1) {
            this.tableRowDataName = "ORIGINALVALUE";
            this.tableRowDataValue = "LABELVALUE";
          } else if (this.labelType === 2) {
            this.tableRowDataName = "ENTITYNAME";
            this.tableRowDataValue = "ENTITYVALUE";
          }
          this.translate
            .get(["LBLORIGIANL", "LBLTRANSLATED"])
            .subscribe((res: Object) => {
              this.cols = [
                { field: this.tableRowDataName, header: res["LBLORIGIANL"] },
                { field: this.tableRowDataValue, header: res["LBLTRANSLATED"] },
              ];
            });
        });
    }
    this.translate
      .get(["LBLORIGIANL", "LBLTRANSLATED"])
      .subscribe((res: Object) => {
        this.cols = [
          { field: this.tableRowDataName, header: res["LBLORIGIANL"] },
          { field: this.tableRowDataValue, header: res["LBLTRANSLATED"] },
        ];
      });
    this.textsCount = this.cols.length;
  }

  loadTableData(obj) {
    this.onSubmitTouched = true;
    this.tableValues = [];
    if (this.screenType == "categories") {
      this.subscription = this.checklistService
        .getCategoryTranslations(obj)
        .subscribe((res) => {
          this.tableValues = res["Translation"];
          this.isLoaded = true;
          this.initialData = JSON.stringify(this.tableValues);
        });
    } else if (this.screenType == "userGroups") {
      this.subscription = this.userGroupService
        .getUserTranslationData(obj)
        .subscribe((res) => {
          this.tableValues = res["data"];
          this.isLoaded = true;
          this.initialData = JSON.stringify(this.tableValues);
        });
    } else if (this.screenType == "user-defined-fields") {
      this.subscription = this.userDefinedFieldService
        .getTranslationsData(obj)
        .subscribe((res) => {
          this.tableValues = res["Data"];
          this.isLoaded = true;
          this.initialData = JSON.stringify(this.tableValues);
        });
    } else {
      if (this.labelType === 1) {
        this.dataKey = "TRANSLATIONID";
        this.tableRowDataName = "ORIGINALVALUE";
        this.tableRowDataValue = "LABELVALUE";
      } else if (this.labelType === 2) {
        this.dataKey = "ENTITYID";
        this.tableRowDataName = "ENTITYNAME";
        this.tableRowDataValue = "ENTITYVALUE";
      }
      this.subscription = this.settingService
        .getConfigurationTranslations(obj)
        .subscribe((res) => {
          if (res["status"] == true) {
            this.tableValues = res["data"];
            this.isLoaded = true;
            this.initialData = JSON.stringify(this.tableValues);
          }
        });
    }
  }

  onSelect(rowIndex) {
    this.selectionIndexes.includes(rowIndex)
      ? this.selectionIndexes.splice(this.selectionIndexes.indexOf(rowIndex), 1)
      : this.selectionIndexes.push(rowIndex);
    if (!this.selectionIndexes.includes(rowIndex)) {
      this.tableValues[rowIndex] = JSON.parse(this.initialData)[rowIndex];
    }
    if (this.selectedData.length > 0) {
      this.onSubmitTouched = false;
    } else {
      this.onSubmitTouched = true;
    }
  }

  isEnable(index) {
    return this.selectionIndexes.includes(index) ? false : true;
  }

  ddChange(toggleString) {
    this.isLoaded = false;
    const obj = {};
    obj["languageId"] = this.language || this.languageId;
    if (this.screenType == "categories") {
      obj["mainCategoryId"] = this.labelType;
    } else if (this.screenType == "userGroups") {
      obj["groupTypeId"] = this.labelType;
    } else if (this.screenType == "user-defined-fields") {
      obj["customField"] = this.labelType;
    } else {
      obj["labelType"] = this.labelType;
    }
    this.loadTableData(obj);
    this.selectionIndexes.length = 0;
    this.selectedData = [];
  }

  onSubmit() {
    // this.tableValues = [];
    this.isLoaded = false;
    let pipeInclude = false;
    this.onSubmitTouched = true;
    if (this.selectedData.length > 0) {
      if (this.screenType == "categories") {
        let isEmptyValue: boolean;
        const tempArr = this.selectedData.map((x) => {
          if (!isEmptyValue) {
            isEmptyValue =
              x["ENTITYVALUE"] === "" ||
              x["ENTITYVALUE"].length > 256 ||
              x["ENTITYVALUE"].startsWith(" ");
          }
          return { eid: x["ENTITYID"], eval: x["ENTITYVALUE"] };
        });
        this.exportData["entityIds"] = tempArr;
        this.exportData["languageId"] = this.language;
        this.exportData["mainCategoryId"] = this.labelType;
        if (isEmptyValue) {
          this.translate
            .get("ALTTRANSLATIONVALIDATION")
            .subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
        } else {
          this.exportData["entityIds"].forEach((x) => {
            if (x.eval.includes("|")) {
              pipeInclude = true;
            }
          });
          if (!pipeInclude) {
            this.subscription = this.checklistService
              .updateCategoryTranslations(this.exportData)
              .subscribe((res) => {
                this.selectedData = [];
                this.loadTableData({
                  languageId: this.exportData["languageId"],
                  mainCategoryId: this.exportData["mainCategoryId"],
                });
                this.inputToggle = true;
                if (res["status"] == true) {
                  this.translate
                    .get(res["message"])
                    .subscribe((res: string) => {
                      this.messageService.add({
                        severity: "success",
                        detail: res,
                      });
                    });
                  this.selectionIndexes.length = 0;
                  this.selectedData = [];
                } else {
                  this.translate
                    .get(res["message"])
                    .subscribe((res: string) => {
                      this.messageService.add({
                        severity: "error",
                        detail: res,
                      });
                    });
                  this.selectionIndexes.length = 0;
                  this.selectedData = [];
                }
              });
          } else {
            this.selectedData = [];
            this.loadTableData({
              languageId: this.exportData["languageId"],
              mainCategoryId: this.exportData["mainCategoryId"],
            });
            this.selectionIndexes.length = 0;
            this.selectedData = [];
            this.translate
              .get("ALTVALIDATETRANSTEXT")
              .subscribe((res: string) => {
                this.messageService.add({
                  severity: "error",
                  detail: res,
                });
              });
          }
        }
        this.onSubmitTouched = false;
      } else if (this.screenType == "userGroups") {
        let isEmptyValue: boolean;
        const tempArr = this.selectedData.map((x) => {
          if (!isEmptyValue) {
            isEmptyValue =
              x["ENTITYVALUE"] === "" ||
              x["ENTITYVALUE"].length > 256 ||
              x["ENTITYVALUE"].startsWith(" ");
          }
          return { eid: x["ENTITYID"], eval: x["ENTITYVALUE"] };
        });
        this.exportData["fieldId"] = tempArr;
        this.exportData["languageId"] = this.language;
        this.exportData["groupTypeId"] = this.labelType;
        if (isEmptyValue) {
          this.translate
            .get("ALTTRANSLATIONVALIDATION")
            .subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
        } else {
          this.exportData["fieldId"].forEach((x) => {
            if (x.eval.includes("|")) {
              pipeInclude = true;
            }
          });
          if (!pipeInclude) {
            this.subscription = this.userGroupService
              .updateUserGroupTranslations(this.exportData)
              .subscribe((res) => {
                this.selectedData = [];
                this.loadTableData({
                  languageId: this.exportData["languageId"],
                  groupTypeId: this.exportData["groupTypeId"],
                });
                this.inputToggle = true;
                if (res["status"] == true) {
                  this.translate
                    .get(res["message"])
                    .subscribe((res: string) => {
                      this.messageService.add({
                        severity: "success",
                        detail: res,
                      });
                    });
                  this.selectionIndexes.length = 0;
                  this.selectedData = [];
                } else {
                  this.translate
                    .get(res["message"])
                    .subscribe((res: string) => {
                      this.messageService.add({
                        severity: "error",
                        detail: res,
                      });
                    });
                }
              });
          } else {
            this.selectedData = [];
            this.loadTableData({
              languageId: this.exportData["languageId"],
              groupTypeId: this.exportData["groupTypeId"],
            });
            this.selectionIndexes.length = 0;
            this.selectedData = [];
            this.translate
              .get("ALTVALIDATETRANSTEXT")
              .subscribe((res: string) => {
                this.messageService.add({
                  severity: "error",
                  detail: res,
                });
              });
          }
        }
        this.onSubmitTouched = false;
      } else if (this.screenType == "user-defined-fields") {
        let isEmptyValue: boolean;
        const tempArr = this.selectedData.map((x) => {
          if (!isEmptyValue) {
            isEmptyValue =
              x["ENTITYVALUE"] === "" ||
              x["ENTITYVALUE"].length > 256 ||
              x["ENTITYVALUE"].startsWith(" ");
          }
          return { eid: x["ENTITYID"], eval: x["ENTITYVALUE"], etype: x["ENTITYTYPE"] };
        });
        this.exportData["parentId"] = this.labelType;
        this.exportData["languageId"] = this.language;
        this.exportData["fieldId"] = tempArr;
        if (isEmptyValue) {
          this.translate
            .get("ALTTRANSLATIONVALIDATION")
            .subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
        } else {
          this.exportData["fieldId"].forEach((x) => {
            if (x.eval.includes("|")) {
              pipeInclude = true;
            }
          });
          if (!pipeInclude) {
            this.subscription = this.userDefinedFieldService
              .updateTranslations(this.exportData)
              .subscribe((res) => {
                this.loadTableData({
                  customField: this.exportData["parentId"],
                  languageId: this.exportData["languageId"],
                });
                this.inputToggle = true;
                if (res["status"] == true) {
                  this.translate
                    .get(res["message"])
                    .subscribe((res: string) => {
                      this.messageService.add({
                        severity: "success",
                        detail: res,
                      });
                    });
                  this.selectionIndexes.length = 0;
                  this.selectedData = [];
                } else {
                  this.translate
                    .get(res["message"])
                    .subscribe((res: string) => {
                      this.messageService.add({
                        severity: "error",
                        detail: res,
                      });
                    });
                }
              });
          } else {
            this.loadTableData({
              customField: this.exportData["parentId"],
              languageId: this.exportData["languageId"],
            });
            this.selectionIndexes.length = 0;
            this.selectedData = [];
            this.translate
              .get("ALTVALIDATETRANSTEXT")
              .subscribe((res: string) => {
                this.messageService.add({
                  severity: "error",
                  detail: res,
                });
              });
          }
        }
        this.onSubmitTouched = false;
      } else {
        let isEmptyValue: boolean;
        let tempArr;
        if (this.labelType === 1) {
          tempArr = this.selectedData.map((x) => {
            if (!isEmptyValue) {
              isEmptyValue =
                x["LABELVALUE"] === "" ||
                x["LABELVALUE"].length > 256 ||
                x["LABELVALUE"].startsWith(" ");
            }
            return { eid: x["TRANSLATIONID"], eval: x["LABELVALUE"] };
          });
        } else if (this.labelType === 2) {
          tempArr = this.selectedData.map((x) => {
            return { eid: x["ENTITYID"], eval: x["ENTITYVALUE"] };
          });
        }

        this.exportData["transId"] = tempArr;
        this.exportData["languageId"] = this.language;
        this.exportData["labelType"] = this.labelType;
        if (isEmptyValue) {
          this.isLoaded = true;
          this.onSubmitTouched = false;
          this.translate
            .get("ALTTRANSLATIONVALIDATION")
            .subscribe((res: string) => {
              this.messageService.add({
                severity: "error",
                detail: res,
              });
            });
        } else {
          this.exportData["transId"].forEach((x) => {
            if (x.eval.includes("|")) {
              pipeInclude = true;
            }
          });
          if (!pipeInclude) {
            this.subscription = this.settingService
              .updateConfigurationTranslations(this.exportData)
              .subscribe((res) => {
                this.selectedData = [];
                this.loadTableData({
                  languageId: this.exportData["languageId"],
                  labelType: this.exportData["labelType"],
                });
                this.inputToggle = true;
                if (res["status"] == true) {
                  this.subscription = this.translate
                    .get(["ALTTRANSLATIONUPDATE", "LBLCANCEL", "LBLLOGOUT"])
                    .subscribe((res: Object) => {
                      this.confirmationService.confirm({
                        message: res["ALTTRANSLATIONUPDATE"],
                        acceptLabel: res["LBLLOGOUT"],
                        rejectLabel: res["LBLCANCEL"],
                        accept: () => {
                          setTimeout(() => {
                            this.authenticationService.logout();
                            this.router.navigate(["/login"], {
                              skipLocationChange: true,
                            });
                          }, 500);
                        },
                      });
                    });
                  this.selectionIndexes.length = 0;
                  this.selectedData = [];
                } else {
                  this.translate
                    .get(res["message"])
                    .subscribe((res: string) => {
                      this.messageService.add({
                        severity: "error",
                        detail: res,
                      });
                    });
                }
              });
          } else {
            this.selectedData = [];
            this.loadTableData({
              languageId: this.exportData["languageId"],
              labelType: this.exportData["labelType"],
            });
            this.selectionIndexes.length = 0;
            this.selectedData = [];
            this.translate
              .get("ALTVALIDATETRANSTEXT")
              .subscribe((res: string) => {
                this.messageService.add({
                  severity: "error",
                  detail: res,
                });
              });
          }
        }
      }
    } else {
      this.onSubmitTouched = false;
      this.translate.get("ALTSELECTEDIT").subscribe((res: string) => {
        this.messageService.add({
          severity: "error",
          detail: res,
        });
      });
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onReset() {
    this.selectionIndexes.length = 0;
    this.selectedData = [];
    this.tableValues = JSON.parse(this.initialData);
    if (this.selectedData.length > 0) {
      this.onSubmitTouched = false;
    } else {
      this.onSubmitTouched = true;
    }
  }

  onSort() {
    this.selectionIndexes.length = 0;
    this.selectedData = [];
  }
}
