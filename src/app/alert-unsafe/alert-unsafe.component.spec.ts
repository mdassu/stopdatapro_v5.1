import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertUnsafeComponent } from './alert-unsafe.component';

describe('AlertUnsafeComponent', () => {
  let component: AlertUnsafeComponent;
  let fixture: ComponentFixture<AlertUnsafeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertUnsafeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertUnsafeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
