import { Component, OnInit } from "@angular/core";
import { AlertUnsafeService } from "../_services/alert-unsafe.service";
import { SlideUpDownAnimations } from "../_animations/slide-up-down.animations";
import { MessageService, ConfirmationService } from "primeng/api";
import { TranslateService } from "@ngx-translate/core";
import { BreadcrumbService } from "../breadcrumb.service";
import { EnvService } from "src/env.service";
import { ObservationChecklistService } from "../_services/observation-checklist.service";
import { AuthenticationService } from "../_services/authentication.service";
import { GlobalDataService } from "../_services/global-data.service";

@Component({
  selector: "app-alert-unsafe",
  templateUrl: "./alert-unsafe.component.html",
  styleUrls: ["./alert-unsafe.component.css"],
  animations: [SlideUpDownAnimations],
})
export class AlertUnsafeComponent implements OnInit {
  animationState = "out";
  filterData = [];
  selectedItem = [];
  insertDataSet: {};
  alertValue = 0;
  checkboxValue: boolean;
  alertChanged: boolean;
  appName: any;
  constructor(
    private alertUnsafeService: AlertUnsafeService,
    private messageService: MessageService,
    private translate: TranslateService,
    private obcService: ObservationChecklistService,
    private breadcrumbService: BreadcrumbService,
    private confirmationService: ConfirmationService,
    private auth: AuthenticationService,
    private env: EnvService,
    private globalData: GlobalDataService
  ) {
    this.breadcrumbService.setItems([
      { label: "LBLUNSAFEALERT", url: "./assets/help/alert-unsafe.md" },
      { label: "LBLDASHBOARD", routerLink: ["/dashboard"] },
    ]);
  }

  ngOnInit() {
    this.onLoadData();
    this.appName = this.env.appName;
    this.toggleFilterShow();
  }

  onLoadData() {
    let dataSet = [];
    this.alertUnsafeService.getAlertUnsafeData({}).subscribe((res) => {
      this.alertValue = res["unsafeAlert"];
      this.checkboxValue = res["unsafeAlert"] == 0 ? false : true;
      var filterSetting = res["filterSetting"];
      if (filterSetting) {
        if (filterSetting["SITEID"]) {
          this.selectedItem["SITEID"] = filterSetting["SITEID"]
            .split(",")
            .map(Number);
        }
        if (filterSetting["AREAID"]) {
          this.selectedItem["AREAID"] = filterSetting["AREAID"]
            .split(",")
            .map(Number);
        }
        if (filterSetting["SUBAREAID"]) {
          this.selectedItem["SUBAREAID"] = filterSetting["SUBAREAID"]
            .split(",")
            .map(Number);
        }
        if (filterSetting["SHIFTID"]) {
          this.selectedItem["SHIFTID"] = filterSetting["SHIFTID"]
            .split(",")
            .map(Number);
        }
      }
      res["filter"].map((item) => {
        if (item["Data"] != "[]") {
          const value = Object.keys(JSON.parse(item["Data"])[0])[0];
          const label = Object.keys(JSON.parse(item["Data"])[0])[1];

          var data = JSON.parse(item["Data"]);
          var keys = Object.keys(data[0]);
          var newData = [];
          data.map((val) => {
            newData.push({
              label: val[label],
              value: val[value],
            });
          });
          dataSet.push({
            Name: item["Name"],
            Data: newData,
            value: value,
            label: label,
          });
        }
        if (this.selectedItem["SITEID"]) {
          this.auth.rearrangeSelect(dataSet, this.selectedItem["SITEID"]);
        }
        if (this.selectedItem["AREAID"]) {
          this.auth.rearrangeSelect(dataSet, this.selectedItem["AREAID"]);
        }
        if (this.selectedItem["SUBAREAID"]) {
          this.auth.rearrangeSelect(dataSet, this.selectedItem["SUBAREAID"]);
        }
        if (this.selectedItem["SHIFTID"]) {
          this.auth.rearrangeSelect(dataSet, this.selectedItem["SHIFTID"]);
        }
      });
    });
    setTimeout(() => {
      this.filterData = dataSet;
    }, 2000);
    this.alertChanged = false;
    if (this.selectedItem["SITEID"]) {
      this.auth.rearrangeSelect(this.filterData, this.selectedItem["SITEID"]);
    }
    if (this.selectedItem["AREAID"]) {
      this.auth.rearrangeSelect(this.filterData, this.selectedItem["AREAID"]);
    }
    if (this.selectedItem["SUBAREAID"]) {
      this.auth.rearrangeSelect(
        this.filterData,
        this.selectedItem["SUBAREAID"]
      );
    }
    if (this.selectedItem["SHIFTID"]) {
      this.auth.rearrangeSelect(this.filterData, this.selectedItem["SHIFTID"]);
    }
  }

  toggleFilterShow() {
    this.animationState = this.animationState === "out" ? "in" : "out";
  }

  onSubmit() {
    if (!this.alertChanged && this.alertValue == 0) {
      this.translate.get("ALTCHECKUNSAFE").subscribe((message) => {
        this.messageService.add({
          severity: "error",
          detail: message,
        });
      });
    } else {
      if (this.alertValue == 0) {
        this.translate.get("ALTUNSAFEDELETE").subscribe((res) => {
          this.confirmationService.confirm({
            message: res,
            accept: () => {
              this.saveData();
            },
            reject: () => {
              this.onLoadData();
            },
          });
        });
      } else {
        this.saveData();
      }
    }
  }

  saveData() {
    if (this.insertDataSet == undefined) {
      this.filterChange("LBLBSITES");
    }
    this.insertDataSet["alertValue"] = this.alertValue;
    this.alertUnsafeService
      .insertAlertUnsafeData(this.insertDataSet)
      .subscribe((res) => {
        this.translate.get(res["message"]).subscribe((message) => {
          this.messageService.add({
            severity: "success",
            detail: message,
          });
          this.onLoadData();
        });
      });
  }

  filterChange(name) {
    var enableSubarea = parseInt(this.globalData.GlobalData["ENABLESUBAREA"]);

    let siteIds = this.selectedItem["SITEID"]
      ? this.selectedItem["SITEID"].join()
      : "";
    let areaIds = this.selectedItem["AREAID"]
      ? this.selectedItem["AREAID"].join()
      : "";
    let subAreaIds = this.selectedItem["SUBAREAID"]
      ? this.selectedItem["SUBAREAID"].join()
      : "";
    let shiftIds = this.selectedItem["SHIFTID"]
      ? this.selectedItem["SHIFTID"].join()
      : "";
    this.insertDataSet = {
      siteIds: siteIds,
      areaIds: areaIds,
      subAreaIds: subAreaIds,
      shiftIds: shiftIds,
    };
    var params = {
      siteId: this.selectedItem["SITEID"].join(),
    };

    if (name == "LBLBSITES") {
      this.obcService.getAreaFilter(params).subscribe((res) => {
        if (res["status"] == true) {
          var newArea = [];
          if (res["area"].length > 0) {
            res["area"].map((val) => {
              newArea.push({
                label: val["AREANAME"],
                value: val["AREAID"],
              });
            });
          }
          var newShift = [];
          if (res["shift"].length > 0) {
            res["shift"].map((val) => {
              newShift.push({
                label: val["SHIFTNAME"],
                value: val["SHIFTID"],
              });
            });
          }
          if (enableSubarea == 1) {
            this.filterData[3]["Data"] = newShift;
          } else {
            this.filterData[2]["Data"] = newShift;
          }
          this.filterData[1]["Data"] = newArea;

          this.selectedItem["AREAID"] = "";
          this.selectedItem["SUBAREAID"] = "";
          this.selectedItem["SHIFTID"] = "";
        } else {
          if (enableSubarea == 1) {
            this.filterData[3]["Data"] = [];
          } else {
            this.filterData[2]["Data"] = [newShift];
          }
          this.filterData[1]["Data"] = [];
          // this.filterData[3]["Data"] = [];
          this.selectedItem["AREAID"] = "";
          this.selectedItem["SUBAREAID"] = "";
          this.selectedItem["SHIFTID"] = "";
        }
      });
    }
    var params1 = {
      areaId: areaIds,
    };
    if (name == "LBLAREAS") {
      this.obcService.getSubAreaFilter(params1).subscribe((res) => {
        if (res["status"] == true) {
          var newSubArea = [];
          if (res["subArea"].length > 0) {
            res["subArea"].map((val) => {
              newSubArea.push({
                label: val["SUBAREANAME"],
                value: val["SUBAREAID"],
              });
            });
          }
          this.filterData[2]["Data"] = newSubArea;
          this.selectedItem["SUBAREAID"] = "";
        } else {
          this.filterData[2]["Data"] = [];
          this.selectedItem["SUBAREAID"] = "";
        }
      });
    }
    if (siteIds) {
      this.auth.rearrangeSelect(this.filterData, this.selectedItem["SITEID"]);
    }
    if (areaIds) {
      this.auth.rearrangeSelect(this.filterData, this.selectedItem["AREAID"]);
    }
    if (subAreaIds) {
      this.auth.rearrangeSelect(
        this.filterData,
        this.selectedItem["SUBAREAID"]
      );
    }
    if (shiftIds) {
      this.auth.rearrangeSelect(this.filterData, this.selectedItem["SHIFTID"]);
    }
  }

  onSelectAll(index) {
    const selectedData = this.filterData[index]["Data"].map(
      (res) => res[this.filterData[index]["value"]]
    );
    this.selectedItem[this.filterData[index]["value"]] = selectedData;
    // this.filterChange();
  }

  onClearAll(index) {
    this.selectedItem[this.filterData[index]["value"]] = [];
  }

  alertChange() {
    this.alertChanged = true;
    this.alertValue = this.checkboxValue ? 1 : 0;
  }

  onReset() {
    this.selectedItem = [];
  }
}
