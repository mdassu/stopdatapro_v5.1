import { Component, OnDestroy } from "@angular/core";
import { MenuItem } from "primeng/primeng";
import { Subscription } from "rxjs";
import { BreadcrumbService } from "./breadcrumb.service";

@Component({
  selector: "app-breadcrumb",
  templateUrl: "./app.breadcrumb.component.html",
  styleUrls: ["./app.breadcrumb.component.css"]
})
export class AppBreadcrumbComponent implements OnDestroy {
  subscription: Subscription;
  items: MenuItem[];
  showHelpDialog: boolean = false;
  helpFileUrl: string;
  showIcon: boolean = true;

  constructor(public breadcrumbService: BreadcrumbService) {
    this.subscription = breadcrumbService.itemsHandler.subscribe(response => {
      this.items = response;
      if (
        response[0]["label"] == "LBLDASHBOARD" ||
        response[1]["label"] == "LBLANALYTICS"
      ) {
        this.showIcon = false;
      } else {
        this.showIcon = true;
      }
      this.helpFileUrl = response[0]["url"];
    });
  }

  showHelp() {
    this.showHelpDialog = true;
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
