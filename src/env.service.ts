export class EnvService {
  // The values that are defined here are the default values that can
  // be overridden by env.js

  // API url
  public apiUrl = "";

  // build version
  public version = "";

  // analytics dashboard url
  public analyticDashboardUrl = "";

  // analytics dashboard url
  public cookieUrl = "";

  // Enable/Disable "By default, the Observation Checklist page will display checklists for the Previous" in Global Option -> Checklist Configuration
  public configurationOBSChecklist = "";

  // Application Name
  public appName = "";

  // Whether or not to enable debug mode
  public enableDebug = true;

  constructor() {}
}
