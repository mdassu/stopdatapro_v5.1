(function (window) {
  window.__env = window.__env || {};

  // API url
  window.__env.apiUrl = 'http://15.206.53.162:8086/api';
  //window.__env.apiUrl = 'https://services-azuatdp50.stopdatapro.com/api';
  // Build version
  window.__env.version = '0.1.22';

  // analytics dashboard url
  window.__env.analyticDashboardUrl = 'https://analytics-azuat-dims.consultdss.com';

  // analytics dashboard url
  window.__env.cookieUrl = 'https://analytics-azuat-dims.consultdss.com';

  // Enable/Disable "By default, the Observation Checklist page will display checklists for the Previous" in Global Option -> Checklist Configuration
  // window.__env.configurationOBSChecklist = '0';

  // Application Name
  window.__env.appName = 'DataPro';

  // Whether or not to enable debug mode
  // Setting this to false will disable console output
  window.__env.enableDebug = false;
}(this));