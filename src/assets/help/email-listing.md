## Email Configuration

System administrator may use email services to remind the users about the task that they have to perform or keep the supervisors updated about every action performed. The emails will be delivered to the users in the following ways.

**Application Inbox**

A User Name is associated with every user registered in the application. When a user gets qualified to receive an email, it will be delivered to the application Inbox based on the user name. The application Inbox is accessible within Others.

**Email Inbox**

If a user has registered his/her email address, a copy of the email will be sent to the specified email address as well.

![Image of markdown](assets/help/screenshots/Email-Listing.PNG)

Default templates are available for the following notifications:

- Corrective Action Overdue

- Corrective Action Reminder

- Corrective Action Assigned

- Corrective Action Update

**Note**: By default, Corrective Action Overdue, Corrective Action Assigned and Corrective Action Update templates will be active, except for the Corrective Action Reminder template which will be inactive.
