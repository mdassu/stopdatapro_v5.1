## Index Report


This report displays the percentage of safe and unsafe conditions observed at each site.

System administrator may filter the data by Sites, Areas, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Main Categories, Observation Date and Include Checklist Without Group Assignments.


![Image of markdown](assets/help/screenshots/index-report-filter.png)


**Note**: Specifying the Observation Date range is mandatory to generate this report.  

This is an example of the report.


![Image of markdown](assets/help/screenshots/index-report-report-1.png)