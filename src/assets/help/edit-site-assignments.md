## Assign Areas, Shifts, Users and Groups to a Site

Every site will contain different areas that need to be observed. The **Assignments** tab allows system administrator to assign the already created areas, shifts, users and groups to a site.

![Image of markdown](assets/help/screenshots/addSiteAssignments.PNG)

The steps to assign areas to a site are:

1. From the **Menu** Page, navigate to **Site Management**.

2. Select **Sites** from the list of menu options.

3. The Site-List dialog box displays.

4. Select the site name.

5. The **Site – General Information** dialog box displays.

6. Click the **Assignments** tab.

7. The Site Name displays.

8. Select the **Assignment Type** from the dropdown menu. The available options include Areas, Shifts, Users and Groups.

There are two list boxes that display: **Available** and **Assigned**. Available list box contains all the active items that may be assigned to the site. Assigned list box contains the items that are already assigned to the site.

- To add an available item, click .

- To add all of the options available, click .

- To remove an item from the Assigned list, choose the individual item and click .

- To remove all the items at once, click .

9. Click to save the information entered.
