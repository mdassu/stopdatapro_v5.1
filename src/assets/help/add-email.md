## Setting up a Notification

There are a few details that the system administrator needs to provide while creating a notification.

- A unique name to identify the notification

- Recipients address

- Message that has to be communicated

## General

System administrator may specify a name and the email type in the **General** section.

![Image of markdown](assets/help/screenshots/Add-Email-General.PNG)

**Email Option Name** - Enter a unique name that is used to identify the email notification.

**Email Type** - Select the appropriate type from the Email Type drop down menu.

- Corrective Actions Outstanding - This email type may be used to remind the responsible person and the owner when the corrective action is not performed before the response date and the card status is open.

- Corrective Actions Remainders - This email type may be used to remind the responsible person and the owner about the date on or before which corrective actions need to be performed.

- Corrective Actions Assigned - This email type may be used to notify the responsible person when a new corrective action card is created.

- Corrective Actions Updated - This email type may be used to notify the responsible person and the owner of the corrective action whenever a new corrective action is performed and the data is updated in the corrective action card.

- Observation Remainder Weekly - This email type may be used to remind the users/observers that have not completed the weekly target. This email will only be sent for users with weekly targets.

- Observation Remainder Monthly - This email type may be used to remind the users/observers that have not completed the monthly target. This email will only be sent for users with monthly targets.

**Mail Details** - Select the notification name from which the email details should be copied.

## Email Details

This section may be used to specify the users to whom an email should be sent and also to enter the message that has to be communicated to the user. Default content is available for each email type with appropriate keywords to provide the recipient with the required information. You can also customize the email content by clicking the Edit link.

![Image of markdown](assets/help/screenshots/Add-Email-Email-Details.PNG)

Reply To - Enter the email address that would automatically populate in the **To** field when the user attempts to reply to the email sent from this notification.

Cc - Enter the email ID to which a carbon copy of this email has to be sent.

Subject – This is a text field. Enter the subject here.

Body –This is a text field. Click the Edit link to enter the message.

- The message text can be formatted by using the HTML Editor located above the compose box.

- Use the keywords from the Keywords drop down menu to import the user information and training information of users while sending emails.

- The default keywords available for each notification type vary based on the information that need to be sent to the users.

![Image of markdown](assets/help/screenshots/Add-Email-Email-Filter-Criteria.PNG)

System administrator may use the Email Filter Criteria to restrict the user groups or sites for which emails should be sent. When no criterion has been selected, users irrespective of the site will qualify to receive emails. When a site is chosen, only users who belong to that site/group will qualify to receive emails.

## Email Scheduling

System administrator may use the scheduling section to specify the interval in which the emails should be sent.
