## Group Listing Page

There are several options that assist system administrators in managing groups. The Group List page contains functionalities that help the system administrator to add, edit, delete and change the status of the group.
To view group list, from the Home Page, navigate to User Management. And click Groups from the menu options. The Groups List page opens.

**Group Type** – Groups may be filtered based on Group Type. This option helps super administrators to filter the groups based on the group type. The available options include **All**, **Region**, **Group**, **Division**, **Department** and **Title**.

- Region- This option displays only the groups created under the group type **Region**.

- Group- This option displays only the groups created under the group type **Group**.

- Division - This option displays only the groups created under the group type **Division**.

- Department - This option displays only the groups created under the group type **Department**.

- Title- This option displays only the groups created under the group type **Title**.

![Image of markdown](assets/help/screenshots/Group-listing.PNG)

- To add a new group, select the **Add** button.

- To edit an existing group, click on the preferred group name.

- To delete a group, select the group and click the **Delete** button.

- To activate or inactivate a group, select the group and click the **Change Status** button.
