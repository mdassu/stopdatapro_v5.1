## Assign Sites to Checklist

System administrator may associate the designed checklist with appropriates sites by accessing the Sites tab.

The steps to assign sites to a checklist are:

1. From the **Menu** Page, navigate to Checklist Configuration.

2. Select **Checklist Setup** from the list of menu options.

3. The **Checklist Setup Listing** page displays.

4. Select the checklist to assign sites.

5. Click the **Sites** tab.

6. The available list box displays the sites that are not associated with the checklists yet.

![Image of markdown](assets/help/screenshots/Edit-Checklistsetup-Sites.PNG)

7. Move the sites from the Available list box to Assigned list box.

8. Click the Save button.

**Note:** The **Sites** tab will be enabled when No is selected from the All Sites Setup dropdown menu in the Checklist Setup - General Information Page.

![Image of markdown](assets/help/screenshots/Add-checklistsetup.PNG)
