## Trend Line Report
 

This report may be used to measure the trend in safety performance over a period of time. The visual representation of this report enables you to analyze the safety behavior using a line chart or column chart that is plotted using the safe and unsafe observations made. You can choose any one of the different trend line types while running this report: Month-to-Month, 3-Month Moving average or Year-to-Year.


System administrator may filter the data by Sites, Areas, Observers, Shifts, Checklist Setup, Main Categories, Observation Date and Chart Type.


![Image of markdown](assets/help/screenshots/trend-line-report-filter.png)


This is an example of the Month-to-Month trend line report. This is a line chart and displays a maximum of 18 data points.


![Image of markdown](assets/help/screenshots/trend-line-report-report-1.png)


**Month-to-Month trend line report**


This is an example of the 3-Month Moving Average. This is also a line chart and displays a maximum of 18 data points. The average is calculated for every 3 months and displayed as a point in the chart.


![Image of markdown](assets/help/screenshots/trend-line-report-report-2.png)


**3-Month Moving Average trend line report**


This is an example of the Year to Year trend line report. This is a column chart and plots data for a maximum 4 years.


![Image of markdown](assets/help/screenshots/trend-line-report-report-3.png)