## Scheduled Reports Listing Page


To access the Schedule Reports listing page, navigate to Reports Management, from the Home Page. And click Schedule Reports from the list of menu options. The Scheduling Listing page opens. This page contains functionalities that help the system administrator to email reports.


- To **add** a new item, select the Add button.

- To edit an existing item, click on the preferred schedule name.

- To delete an item , select the user and click the **Delete** button.

- To activate or inactivate an item, select the schedule name and click the **Change Status** button.