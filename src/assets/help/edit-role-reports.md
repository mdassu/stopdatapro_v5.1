## Assign Reports to Roles

The **Reports** tab allows system administrator to assign reports to roles. The users who are assigned to this role may have access only to these reports.

![Image of markdown](assets/help/screenshots/Reports-roles.PNG)

The steps to assign reports to roles are:

1. Log into the application and choose **User Management** - > **Roles** from the Home page.

2. The **Role Listing** page displays.

3. Click on the role name for which reports have to be assigned.

4. Click the **Reports** tab.

The **Available** List displays the reports that are not assigned to the selected role.

5. To assign reports, select them from Available list box and move to the Assigned list box.

6. The assigned users will be listed in the **Assigned** list box.

7. Click the **Save** button to save the assignments.

## Remove Reports to Roles

To remove reports from a role, select them from the **Assigned** list box and move them to the **Available** list box.
