## Inbox

Users may access the emails that they received by accessing the **Application Inbox** or **Email Inbox**.

![Image of markdown](assets/help/screenshots/email-inbox-1.png)

![Image of markdown](assets/help/screenshots/email-inbox-2.png)


**Accessing Application Inbox**

The steps to access the Inbox are:

1. Log into the application.

2. Choose **Others - > Inbox** from the Home page.
or

3. Click the **Home** icon and choose **Inbox** from the menu items that displays on the left side.

4. The email listing page displays.

5. User may filter the emails by From **Email Address** or **Received Date**.


![Image of markdown](assets/help/screenshots/email-inbox.png)


The email list page consists of four tabs. The emails are categorized and displayed under different tabs to help the users find the email easily.

- **Unread** - Lists all emails that are not read by the users yet.

- **Emails** - Lists all emails that are used to keep the user updated about an event and emails that are sent as a remainder.

- **Scheduled Reports** - Lists all reports that are scheduled to the users.

- **All Emails** - List all remainders emails and the reports emailed to the users.

To delete an email from the list, select the checkbox beside the entry and click the **Delete Mail List** button.