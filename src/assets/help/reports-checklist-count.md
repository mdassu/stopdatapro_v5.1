## Checklist Count


This report displays the observers and the total number of observation checklists assigned to each observer.
 

System administrator may filter the data by Sites, Area, Observers, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Observation Date, Entered Date, Observer Status, Hide Observer With Zero Checklist, Include Historical Data, Include Checklist Without Group Assignments and Display Incomplete Observations Only.

 
Note: To display only the incomplete observation checklist count, use the filter criterion 'Display Incomplete Observations Only'.


Select the filter criteria and click the **Run Report** button.


![Image of markdown](assets/help/screenshots/reports-checklist-count-filter.png)


**Note**: Specifying the date range is mandatory to generate this report.


This is an example of a report.

- System administrator may generate a summary report or a detail report by selecting the appropriate option from the **Report Type** dropdown menu.

- System administrator may also view the records by a specific site by selecting the preferred one from the **Site** dropdown menu.


![Image of markdown](assets/help/screenshots/reports-checklist-count-report-1.png)


Click the observer name to view the number of checklists completed by this observer for each site.


![Image of markdown](assets/help/screenshots/reports-checklist-count-report-2.png)