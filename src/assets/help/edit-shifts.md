## Edit a Shift

To edit a shift:

1. Log into the application and choose **Site Management** -> **Shifts** from the Home page.

2. Click on the shift name to edit the information.

3. The **General** information page displays.

4. Modify the required details.

5. Click the **Save** button.

Note: To save the entered information and add a new area, click the Save and Add New button.

## Assign Sites to Shifts

After creating shifts, system administrator may associate them appropriate sites.

1. From the **Menu** Page, navigate to **Site Management**.

2. Select **Shifts** from the list of menu options.

The **Shift-Listing** page displays.

3. Click on the preferred item to open it.

4. The **General** information page displays.

5. Click the **Assignments** tab.

![Image of markdown](assets/help/screenshots/Shifts-Assignments.PNG)

6. The Available list box displays the sites that are not associated with the shift yet. The Assigned list box displays the sites that are already associated with the Shifts.

- To add an available item, click .

- To add all of the sites available, click .

- To remove an item from the Assigned list, choose the individual item and click .

- To remove all the items at once, click .

7. Click the **Save** button.
