## Edit a Group

System administrator may edit the general information of the group and the user assignments.

To edit a group:

1. Log into the application and choose **User Management** -> **Groups** from the Home page.

2. Select the appropriate **Group Type** from the drop down menu.

3. Click on the group name to edit the information.

4. Click the **Save** button.

**Note:** To save the entered information and add a new group, click the **Save and Add New** button.
