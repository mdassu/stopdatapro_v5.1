## Edit Groups on Checklists


The Edit Observation User Groups option allows system administrators to modify the groups assigned to the observer on specific observation checklists for a specific period of time.
 

To edit the group information:

1. Log into the application and choose **Data Entry - > Edit Groups on Checklists** from the Home page.

2. The user Listing page displays.

3. Select the user to change the group information.

4. The **Observation User Group Info** dialog box displays.

![Image of markdown](assets/help/screenshots/edit-groups-on-checklists-edit.PNG)


**Note**: If any group has been assigned to the user already, it will be populated in the group fields.
 

- Select the appropriate groups from the dropdown menu.

- Select the date range for which the group should be assigned to the users while they make the observations.
 

5. Click the Save button.