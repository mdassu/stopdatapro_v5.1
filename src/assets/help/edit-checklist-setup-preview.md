## Checklist Preview

The **Preview** tab enables administrators to see how the created checklist will display to the users.

![Image of markdown](assets/help/screenshots/Edit-Checklistsetup-Preview.PNG)

1. From the **Menu** Page, navigate to **Checklist Setup**.

2. Select **Checklist Setup** from the list of menu options.
   The **Checklist - Setup** dialog box displays.

3. Double click the checklist name to open.
   The **General Information** page displays.

4. Click the **Preview** Tab.
