## Add a Site

The steps to add a site are:

1. Log in to the application and choose **Site Management** - > **Sites** from the Home page.
   The **Site - Listing** page displays.
2. Click the **Add** button.
3. The **Add Site** dialog box displays.

![Image of markdown](assets/help/screenshots/addEditSite.PNG)

- **Site Name** - The Site Name is a descriptive title that may have up to 100 characters. The Site Name is a text field that is used to display the name of the facility in the application.

- **Address** - This is a text field. Enter the address of the facility.

- **City** - This is a text field. Enter the name of the city in which the facility is located.

- **State** - This is a text field. Enter the name of the state in which the facility is located.

- **Country** - Select the country from the drop down menu.

- **Zip Code** - This is numerical field. Enter the zip code of the city.

- **Comments** - Enter any comments specific to the facility.

- **Employees** - Enter the number of employees present in the facility.

- **Status** - Select the status from the dropdown menu. By default, Active displays. Specify if it is active or inactive. Inactivating will hide this from users' access.

- **Time Zone** - Select the appropriate time zone for the site. The time zone selected here will be in the **User – General Info** page while adding or editing users. When multiple sites are assigned to a user, the time zone pertaining to the default site will be used for the user.

**Note:** Emails will be triggered based on the time zone of the default site.

Note: Click to proceed without saving the entered information.

4. Click **Save** to add the site.

To save the entered information and add a new site, click the **Save and Add New** button.
