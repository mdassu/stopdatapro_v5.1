## Translate Group Name

The group names may be translated to other languages by accessing the **Translation** button accessible at the **Group List** page. When the language of the application is changed, group name displays accordingly.

The steps to translate the group names are:

1. From the **Menu** Page, navigate to **User Management**.

2. Select **Groups** from the list of menu options.
   The Groups Listing dialog box displays.

3. Click the **Translation** button.

4. The Group Translation page displays.

![Image of markdown](assets/help/screenshots/Group-translations.PNG)

5. Select the Group Type from the dropdown menu.

6. Select the language to which the label has to be translated from the **Languages** dropdown menu.

7. The groups corresponding to the selected group type displays.

8. Select the checkbox beside the group name.

9. Enter the appropriate translation in the text box available against it.

_Note:_ Click to proceed without saving the entered information.

10. Click to save the information entered.
