## User Defined Module Name Translation

The userdefined names may be translated to other languages by accessing the **Translation** button accessible at the **Userdefined List page**. When the language of the application is changed, userdefined name displays accordingly.

The steps to translate the user defined names are:

1. From the **Menu** Page, navigate to **Site Management**.

2. Select **User Defined Fields** from the list of menu options.
   The User Defined Fields Listing dialog box displays.

3. Click the **Translation** button.

4. Select the Field Name from the dropdown menu.

![Image of markdown](assets/help/screenshots/Field-Translations.PNG)

5. The User Defined Field Name and the associated User Defined Field values display.

6. Select the Language from the dropdown menu to which the label has to be translated.

7. Select the checkbox beside the Field name.

8. Enter the appropriate translation in the text box available against it.

_Note:_ Click to proceed without saving the entered information.

9.Click to save the information entered.

10.Click the Reset button to reset the information entered.
