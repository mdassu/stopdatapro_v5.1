## Target Vs Activity Report - Monthly

 
This report displays the target set for each observer and the number of observations completed by the observer for each month of a calendar year.
 

System administrator may filter the data by Sites, Areas, Observers, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Observation Date, Observer Status, Hide Observer With Zero Checklist, Include Checklist Without Group Assignments and Target Status. Specifying the observation date range is mandatory to generate this report and this report may be processed for a maximum of 12 months period only.  


![Image of markdown](assets/help/screenshots/target-activity-monthly-filter.png)


**Note**: When a specific target status is selected, only the records pertaining to the selection will be generated. The other fields will be blank.
 

This is an example of the report. By default, this report gives the count each observer has completed and the percentage of completion.


![Image of markdown](assets/help/screenshots/target-activity-monthly-report-1.png)


System administrator may view the data by choosing the options from the Report Type dropdown menu. The available options include: Monthly Target and Summary Report. By default, the report will be processed for monthly target.

- **Red** indicates that the observer has not met the target.

- **Yellow** indicates that the observer has just met the target specified.

- **Green** indicates that the observed has completed more than what he/she is expected to complete.

- **Not Applicable** displays when records are filtered based on target status for a specific period and no records are available.

 
To view the summary of the report, select **Summary Report** from Report Type dropdown menu. In the Summary report, Target displays the total for the date range selected.


![Image of markdown](assets/help/screenshots/target-activity-monthly-report-2.png)