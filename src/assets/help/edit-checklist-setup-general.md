## Design a Checklist

A checklist is a compiled form that contains the safety observations which could be performed on a site. System administrator may choose if the checklist should contain **Main categories and Sub categories**. There are also options to choose if the checklist should be assigned to specific sites or all sites.

- Firstly decide on the sites for which you are creating the checklist

- Secondly choose the categories that are appropriate for the site

- Finally choose the sub categories

**Note**: Prior to setting up a checklist, the system administrator should create the **Main Categories** and **Sub Categories**.

The steps to edit a checklist are:

1. From the **Home** Page, navigate to **Checklist Configuration**.

2. The **Checklist Configuration** menu expands and lists the sub menus on the left side.

3. Select **Checklist Setup** from the list of menu options.

4. The **Checklist Setup** listing page displays.

5. Click on the checklist name to edit it.

6. Make the required changes.

7. Click the **Save** button.
