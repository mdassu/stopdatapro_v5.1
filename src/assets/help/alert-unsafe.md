## Alert Subscription for Unsafe Observation

Users may subscribe to be informed about the unsafe acts recorded in the checklist everyday by selecting the Alert Unsafe option.

![Image of markdown](assets/help/screenshots/AlertUnsafe-icon.PNG)

When the user click the **Alert Unsafe** link, a dialog box displays with options to choose the sites, areas and shifts for which they want to view the Observation checklist.

![Image of markdown](assets/help/screenshots/AlertUnsafe.PNG)

**Note:**

1. By default, users will receive emails for observations made to the site they belong to.

2. By default, this option will be available for Super Admin and Site Admin. However, this option may be turned on for other user roles as well.

![Image of markdown](assets/help/screenshots/alert-unsafe-emails.png)
