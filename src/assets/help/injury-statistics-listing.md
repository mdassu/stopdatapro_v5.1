## Injury Statistics Listing Page

The Injury Statistics Listing page contains functionalities that help the administrators to add, edit, delete and activate/inactivate the entries regarding injury statistic.


**Access Injury Statistics List**

The steps to record injury statistics are:

1. Log in to the application and choose **Data Entry - > Injury Statistics** from the Home page. The **Injury Statistics Listing** page displays.


**Add Injury Statistics**

The steps to record injury statistics are:

1. Log in to the application and choose **Data Entry - > Injury Statistics** from the Home page. The **Injury Statistics Listing** page displays.

2. Click the **Add** button.

3. The **Injury Statistics** page opens.


![Image of markdown](assets/help/screenshots/injury-statistics-listing.jpg)

- **Year** – Select the year for which entries should be made.

- **Hours Worked** – Enter the total number of working hours.

- **Total Injuries** – Enter the total number of injuries recorded in the site.

- **Site** – To assign sites, select the sites from the **Available** list box and move them to the Assigned list box.

- **Status** – Specify if it is active or not.

4. Click the **Save** button.



**Edit Injury Statistics**

To edit the Injury Statistics:


- Log in to the application and choose **Data Entry - > Injury Statistics** from the Home page. The **Injury Statistics Listing** page displays.

- Click on the item to open it.

- Modify the details.

- Click the **Save** button.


**Delete Injury Statistics**

To delete the Injury Statistics:

1. Log in to the application and choose **Data Entry - > Injury Statistics** from the Home page. The **Injury Statistics Listing** page displays.

2. Select the item by selecting the checkbox beside it. 

3. Click the **Delete** button.


**Inactivate/Activate Injury Statistics**

To inactivate Injury Statistics:

1. Log in to the application and choose Data Entry - > Injury Statistics from the Home page.  
The Injury Statistics Listing page displays.

2. Select the item by selecting the checkbox beside it. 

3. Click the Change Status button.