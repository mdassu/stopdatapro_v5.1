## Specify Targets for a Site

System administrator may determine the number of observations that should be completed every month using the **Targets** tab. The target will be assigned to all users who belong to that site.

![Image of markdown](assets/help/screenshots/siteTargets.PNG)

The steps to specify targets to a site are:

1. From the **Menu** Page, navigate to **Site Management**.

2. Select **Sites** from the list of menu options. The **Site** list dialog box displays.

3. Select the site name.

4. Click the **Targets** tab.

5. If the target is the same for all the months, enter the numerical value in the first field provided. It will be automatically copied to all the month fields.
   Enter the target for a month in the **Target** column.
   Enter the number of observations that should be performed every week in the **Observation / Week** column.
   Note: Values may also be entered one by one for each month.

6. Select the **Set this target for all observers assigned to the site** checkbox to apply the targets to all the observers assigned to this site.

7. Click the **Save** button to proceed with the action.

**Note:** Select **Weekly** from the dropdown menu to select weekly targets for users. Enter the value in the textbox provided. The value will be applied for all the weeks.

![Image of markdown](assets/help/screenshots/siteTargetWeekly.PNG)
