## Approval


The observation data entered offline or from a mobile app may be approved and synced with the application by accessing the **Approval** link. Only users with appropriate permission can approve the observation data.


**Accessing Approval Link**

The steps to access the approval link are:

1. Log into the application.

2. Choose **Others - > Approval** from the Home page.
or

3. Click the **Home** icon and choose **Approval** from the menu items that displays on the left side.

The **Approval** listing page displays.
 
4. Administrator may filter records based on Sites, Areas, Shifts, Observers, Observation Data and Status.


![Image of markdown](assets/help/screenshots/approval-listing.png)


5. Click on an item to view or edit the observation data.


- To approve a single entry, select the checkbox beside the entry and click the Approve button.

- To approve all the entries, click the Approve All button.

- To delete an entry, select the checkbox beside the entry and click the Delete button.