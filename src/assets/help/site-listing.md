## Email Users Assigned to a Site

System administrator may send emails to users belonging to a single site or multiple sites. This feature is useful when they have to communicate a common message to all users belonging to a site. When the system administrator creates an email notification, all the users assigned to the selected sites will qualify to receive that email.

The steps to send an email to users are:

1. Log in to the application and choose **Site Management** - > **Sites** from the **Home** page.

2. The **Site - Listing** page displays.

![Image of markdown](assets/help/screenshots/Site-listing.PNG)

3. Select the checkbox beside the site name. System administrator may select multiple sites as well.

4. Click the **Send Email** button available at the bottom of the page. Enter the information in the pop up that displays.

- **Selected Site(s)** - The selected sites displays here for reference.

- **Email Option Name** - Enter a name to identify the email notification.

- **Subject** - Enter a brief description about the message communicated in the email.

- **Body** - Enter the message that should be communicated to the users.

![Image of markdown](assets/help/screenshots/Site-listing-email.PNG)

5. Click the **Send** button.

Email will be sent to all users who are assigned to the selected sites.
