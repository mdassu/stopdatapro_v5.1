## Translate Category Name

The category names may be translated to other languages by accessing the **Translation** button accessible at the **Category List** page. When the language of the application is changed, category name displays accordingly.

The steps to translate the main category names are:

1. From the **Menu** Page, navigate to **Checklist Configuration**.

2. Select **Categories** from the list of menu options.
   The Categories Listing dialog box displays.

3. Click the **Translation** button.

4. Select the Main Category from the dropdown menu.

![Image of markdown](assets/help/screenshots/Category-Translation.PNG)

5. The main category and the associated sub categories display.

6. Select the Language from the dropdown menu to which the label has to be translated.

7. Select the checkbox beside the category name.

8. Enter the appropriate translation in the text box available against it.

_Note:_ Click to proceed without saving the entered information.

9. Click to save the information entered.
