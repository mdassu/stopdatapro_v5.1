## Sub Area Module - General Tab

The steps to add a Sub Area are:

1. From the Home Page, navigate to **Site Management**

2. Select **Sub Areas** from the list of menu options.
   The **Sub Areas Fields Listing** dialog box displays.

3. Click the Sub Area Name from the listing.
   The **Sub Area - General** Tab displays.

Enter the Sub Area name and Click the **Save** button to create and update the Sub Area.

![Image of markdown](assets/help/screenshots/SubAreaAdd.PNG)
