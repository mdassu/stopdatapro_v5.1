## Sub Area Listing Page

To access the sub area list page, navigate to Site Management, from the Home Page. And click Sub Areas from the list of menu options. The Sub Area List page opens. This page contains functionalities that help the system administrator to add, edit, delete and change the status of the sub area.

![Image of markdown](assets/help/screenshots/sub-Area-Listing.PNG)

- To add a new sub area, select the **Add** button.

- To edit an existing sub area information, click on the preferred sub area name.

- To delete an sub area, select the sub area name and click the **Delete** button.

- To activate or inactivate an sub area, select the sub area and click the **Change Status** button.
