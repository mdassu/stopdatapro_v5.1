## Delete Sub Category Items

System administrator may delete the sub categories that are no longer in use.

![Image of markdown](assets/help/screenshots/Edit-Category-Delete.PNG)

The steps to delete sub categories are:

1. From the Menu Page, navigate to Checklist Configuration.

2. Select Categories from the list of menu options.
   The Category-List dialog box displays.

3. Select the main category that contains the sub categories to be deleted.

4. Click the Delete tab.

5. Select the checkbox beside the Sub Category name.

6. Click the Delete button.

The sub category will be removed from all the checklists and the associated data will also be removed.
