## Assign Categories to Checklist

An observation checklist is made of categories and sub categories. **Categories** are already created using the Categories application link. Here we create a checklist and associate them with appropriate sites.

A checklist may contain as many categories as required. There is no restriction on the number of categories that may be associated with a checklist.

- Firstly decide on the sites for which you are creating the checklist

- Secondly choose the categories that are appropriate for the site

- Finally choose the sub categories

Note: Checklists may be created only for specific sites.

## Assign Main Categories to a Checklist

The steps to access Main Categories tab are:

1. From the **Menu** Page, navigate to **Checklist Configuration**.

2. Select **Checklist Setup** from the list of menu options.
   The **Checklist Setup Listing** page displays.

3. Select the checklist to assign main categories.

4. Click the **Main Categories** tab.

![Image of markdown](assets/help/screenshots/Edit-Checklistsetup-Main-Categories.PNG)

There are two list boxes that display: Available and Assigned.

5. Choose the main categories that need to be assigned to the checklist.

6. Click the Save button.

## Assign Sub Categories to a Checklist

Once the main categories are assigned to the checklist. The corresponding sub categories will be auto assigned.

The steps to access Main Categories tab are:

1. From the **Menu** Page, navigate to **Checklist Configuration**.

2. Select **Checklist Setup** from the list of menu options.
   The **Checklist Setup Listing** dialog box displays.

3. Select the checklist to assign sub categories.

4. Click the **Sub Categories** tab.

![Image of markdown](assets/help/screenshots/Edit-Checklistsetup-Sub-Categories.PNG)

There are two list boxes that display: Available and Assigned.

5. Choose the sub categories that need to be assigned to the checklist.

6. Click the Save button.
