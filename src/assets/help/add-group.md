## Add a Group

Users with appropriate permissions can create groups. Prior to creating groups decide and make a plan of how the user base should be divided and the matrix has to be created based on the organizational structure.

To add a group:

1. Log into the application and choose **User Management** -> **Groups** from the Home page.

2. Select the appropriate **Group Type** from the drop down menu.

3. Click on the **Add** button available at the bottom of the page.

![Image of markdown](assets/help/screenshots/group-add.PNG)

4. In the General page, enter the following details:

- **Group Type** - The Group Type selected in the Group List page displays for reference.

- **Group Name** - Enter a unique group name that helps in identifying the group.

- **Status** - Select the status from the dropdown menu. This option is the trigger that indicates whether or not to display the group. Once displayed, users may select this option. If this option is not active, the group will not be available for selection.

  5. Click the **Save** button.

**Note:** To save the entered information and add a new group, click the **Save and Add New** button.
