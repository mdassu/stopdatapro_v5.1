## Site Observation Stats


This report displays the average number of observations made per hour specific to a site, area and shift.


System administrator may filter the data by Sites, Areas, Observers, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Observation Date, Observer Status and Include Checklist Without Group Assignments.


![Image of markdown](assets/help/screenshots/site-observation-stats-filter.png)


**Note**: Specifying the observation date range is mandatory to generate this report.


This is an example of a report.

- System administrator may view data specific to safe and unsafe counts by selecting the appropriate option from the Report Type dropdown menu.

- System administrator may also filter data based on a specific site or year by accessing the Site and Year dropdown menu.


![Image of markdown](assets/help/screenshots/site-observation-stats-report-1.png)