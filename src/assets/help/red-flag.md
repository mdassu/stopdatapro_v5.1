## Red Flags Report
 

This report may be used as an alerting system which helps system administrators to identify the sites and areas that requires immediate attention.


System administrator may filter the data by Sites, Areas, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Main Categories, Observation Date, Include Checklist Without Group Assignments.


![Image of markdown](assets/help/screenshots/red-flag-filter.png)


**Note**: Specifying the Observation Date range is mandatory to generate this report.  


This is an example of the report.


![Image of markdown](assets/help/screenshots/red-flag-report-1.png)


- When the value is greater or lesser than the limit, a flag displays.

- When it is safe, a green flag displays.

- When it is unsafe, a red flag displays.


Click on the red flag name to view the details about the area and categories.


![Image of markdown](assets/help/screenshots/red-flag-report-3.png)