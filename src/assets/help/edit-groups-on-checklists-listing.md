## User Listing Page


The user listing page displays only the user levels: Observers and User and observers. To access this page, navigate to **Data Entry** and select **Edit Groups on Checklists**.

![Image of markdown](assets/help/screenshots/edit-groups-on-checklists-listing.PNG)