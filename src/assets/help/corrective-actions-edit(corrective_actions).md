## Corrective Action


For each observation, any number of corrective actions may be added. The Corrective Actions tab contains a list page and displays the Responsible Person Name, Email ID, Response Required Date, Priority and Card Status. There are also options to add, edit or delete the corrective action recommendations.


**Recommend Corrective Actions**

The information that should be specified in the Corrective Action tab are:


![Image of markdown](assets/help/screenshots/corrective-actions-edit_corrective_actions.jpg)


- **Main => Sub Categories Assignments** - System administrator may select the category assignment for which corrective actions - need to be specified.

- **Corrective Action Planned** - Enter the action that should be taken to fix the issue.

- **Action Owner** - Select the name of the person who is in charge to rectify the issue. This user oversees the Responsible - person. When the Responsible person had not taken any steps to rectify the issue until the Response Required Date, the action owner will be notified about the pending tasks. 

- **Priority** - Classify the injury potential of the hazard by selecting the appropriate option. The available options include - **High, Medium and Low**.

- **Responsible Person** - Select the name of the employee who is responsible to take the correction action. This user will have permissions to edit/update any corrective actions that are assigned to them. This user will be notified via emails about the corrective actions until the Response required Date.
 
- **Others** - Enter the name of the user who is not enrolled in the STOP database.
 
- **Email ID(s)** - Enter the email address of the users populated in the Others field.
 
- **Response Date Required** - Select the date on or before which the correction actions need to be taken.
 
- **Correction Action Performed** - Enter the corrective action that was already performed to rectify the unsafe behavior.
 
- **Responded Date** - Select the date on which the corrective action was performed.
 
- **Card Status** – Select the card status from the dropdown menu. It may be either **Open, Completed** or **Alternate Action**.
 
    - **Open** - This signifies that the recommended corrective action is yet to be completed.
    
    - **Completed** - This signifies that the recommended corrective action is already completed.
    
    - **Alternative Action** - This signifies that an alternate action is recommended.
