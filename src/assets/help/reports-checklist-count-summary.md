## Checklist Count Summary
 

This report displays the total number of observation checklists that may be grouped by site, area, shift or group.


System administrator may filter the data by Sites, Area, Observers, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Observation Date, Entered Date, Observer Status, Include Historical Data, Include Checklist without Group Assignments and Group By [Group 1, Group 2 and Group 3].


![Image of markdown](assets/help/screenshots/reports-checklist-count-summary-filter.png)


Selecting the **Group By** filter is mandatory. This report can be grouped by three levels: Group 1, Group 2, and Group 3. Grouping can be done based on site, area, shift and groups.


**Example 1:**

This report is grouped using all the three **Group By** links. [**Group1 - Site ; Group 2 - Area ; Group 3 - Shift**]


![Image of markdown](assets/help/screenshots/reports-checklist-count-summary-report-1.png)


The records are grouped firstly by site name, secondly by area name and finally by shifts. Clicking the Shift link opens the detailed report displaying the observer and their corresponding checklist count details.


![Image of markdown](assets/help/screenshots/reports-checklist-count-summary-report-2.png)


**Example 2:**

This report is grouped using the **Group By** link: **Group1 - Site**.


![Image of markdown](assets/help/screenshots/reports-checklist-count-summary-report-3.png)


The records are grouped by site name. Clicking the Site link opens the detailed report.