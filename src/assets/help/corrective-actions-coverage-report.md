## Unsafe Acts – Corrective Actions Coverage Report

 
This report have an ability to view the number of unsafe acts covered through the corrective actions and number of unsafe acts for which corrective actions are not planned.
 

System administrator may filter the data by Sites, Areas, Observers, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Main Categories, Observation Date, Action Due Date, Responded Date, Priority, Unsafe Acts - Corrective Action Status


![Image of markdown](assets/help/screenshots/corrective-actions-coverage-filter.png)


This is an example of the processed report.


![Image of markdown](assets/help/screenshots/corrective-actions-coverage-report-1.png)