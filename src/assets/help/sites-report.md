## Sites Report


This report lists the general details of sites, and the total number of users assigned to each site. This report also displays a detail account of all the users assigned to each site.


System administrator may filter the data by Sites, Region, Group, Division, Department, Title and User Level.


![Image of markdown](assets/help/screenshots/sites-report-filter.png)


This is an example of the report.

System administrator may also filter the data specific to Observers, Users, Areas and Shifts.


![Image of markdown](assets/help/screenshots/sites-report-report-1.png)