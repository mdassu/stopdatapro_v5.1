## Setting Permissions for a Role

Role represents the permissions and access needed to do a task. Depending on the tasks a user has to perform, the settings may be changed.

The permission page displays the Main modules with a check box beside them. The respective sub modules will be available under them.

- Click the + icon to view the application links available with each main module.

- Select or deselect the check box beside the main module to show or hide the application links present within the main module.

- To show or hide individual application link with main module, expand the main module, and select or deselect the appropriate check boxes.

The available main modules are User Management, Site management, Checklist Configuration, Data Entry, Settings &Configuration, Report Management, Email Configuration and Others.

**Note:** By default, Alert Unsafe option is available for Super Admin and Site Admin. However, this option may be turned on for other user roles as well. This option is accessible under profile.

![Image of markdown](assets/help/screenshots/edit-role-permission.PNG)
