## Observations Report By Category
 

This report displays the total number of safe and unsafe conditions observed for each month of a calendar year by category. By default, this report displays records of all categories.
 

System administrator may filter the data by Sites, Areas, Observers, Shifts, Region, Region, Group, Division, Department, Title, Checklist Setup, Main Categories, Observation Date, Observer Status and Include Checklist Without Group Assignments.


**Note**:

- Specifying the Observation Date range is mandatory to generate this report.  

- Report may be generated for a maximum of 12 months period only.


![Image of markdown](assets/help/screenshots/monthly-safe-unsafe-filter.png)


This is an example of the report.

- To view only the safe records select **Safe Main Categories** from the **Report Type** dropdown menu.

- To view the records pertaining to a specific site, select the respective site from the **Site Name** dropdown menu.

- System administrator may also filter the data by main categories.


![Image of markdown](assets/help/screenshots/monthly-safe-unsafe-report-1.png)


System administrator may click the category link or the bar from the chart to view the safe counts and other general information about the observation checklist specific to that category.


![Image of markdown](assets/help/screenshots/monthly-safe-unsafe-report-2.png)