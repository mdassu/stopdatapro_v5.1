## Category Comments


This report displays the comments that the observer had entered in the checklist. It displays the site name, observer name, observation date, category details and the corresponding comments.

System administrator may filter the data by Sites, Areas, Observers, Shifts, Region, Group, Division, Department, Title, Checklist Setup and Main Categories.  


![Image of markdown](assets/help/screenshots/category-comments-filter.png)


**Note**: Specifying the Observation Date range is mandatory to generate this report.  


This is an example of the report.


![Image of markdown](assets/help/screenshots/category-comments-report-1.png)