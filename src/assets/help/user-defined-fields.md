## User Defined Module Listing Page

To access the user defined list page, navigate to **Site Management** from the **Home** page. Select **User Defined Fields** from the list of menu options. The User Defined Fields List page displays. This page contains functionalities that help the system administrator to translate the User Defined Field values and create,assign User Defined Field values to sites.

To translate User Defined Fields , select the Translate button.

![Image of markdown](assets/help/screenshots/Userdefinedfields-listing.PNG)
