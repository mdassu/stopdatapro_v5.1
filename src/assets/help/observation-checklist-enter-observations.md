## Enter Observation Data into a Checklist


Once the system administrator has entered the general information, they may click the **Observations** tab to enter the physical data into the checklist. The checklist selected in the General Info tab displays here.


**How to enter Observation Data**

- System administrator may enter the number of safe and unsafe observations the observer has observed in the text box available on either sides of the checklist items.

- System administrator may also click the item to enter any comment specific to it.

The category and sub category items that contain comments will be indicated by displaying a comment icon  beside it.


![Image of markdown](assets/help/screenshots/observation-checklist-enter-observations.jpg)


System administrator may click the Add Corrective Actions button to corrective actions from the Observation page.