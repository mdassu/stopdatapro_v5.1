## Edit Red Flags

System administrator may edit the general information of the group and the user assignments.

To edit a red flag information:

1. Log into the application and choose **Data Entry -> Red Flags** from the Home page.

2. The Red Flags - Listing page displays.

3. Click on the item name to open it.

4. Modify the required information.

5. Click the Save button.