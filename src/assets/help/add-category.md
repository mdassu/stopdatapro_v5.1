## Add a Main Category

Main Category is an entry in the checklist that helps to categorize the unsafe conditions that need be observed in a site. This may include the use of equipment, disposal of materials or complying with safety guidelines.

For example, consider that an employee employed in a welding company must use eye glasses to protect his eyes from sparks. Here, the main category will be Personal Protective Equipment and the Sub Category will be Eye Protection.

The steps to add a Main Category are:

1. From the Home Page, navigate to Checklist Configuration.

2. Select Categories from the list of menu options.
   The Categories Listing dialog box displays.

3. Click the Add button.
   The Category- General dialog box displays.

![Image of markdown](assets/help/screenshots/Edit-Category-general.PNG)

- Category Name - Enter the category name. For example: PPE

- Status - Specify if the category is active or not. Select the status from the dropdown menu. Administrators can hide this category from users' access without deleting it by deselecting the checkbox.

4. Click the Save button.
