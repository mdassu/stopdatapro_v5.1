## Examine General Information of a Corrective Action Card

There are a few general information such as the site details and observation date which the system administrator should specify prior to recommending a corrective action.


![Image of markdown](assets/help/screenshots/corrective-actions-edit_corrective_actions.jpg)

- **Site** – Select the site where the unsafe condition was observed.

- **Observation Date** – Select the data on which the observation was made.

- **Observer** – Select the observer who made the observations for the selected site.

- **Area** – Select the area from the dropdown menu.

- **Shift** - Select the area from the dropdown menu.

- **Status** - By default, **Active** is selected.

- **Safe Comments** - Displays the safe comments entered in the corrective action card. This is a read-only field.

- **Unsafe Comments** - Displays the unsafe comments entered in the corrective action card. This is a read-only field.

- **Custom Fields** - Custom Fields is an user defined field, this can be enabled or disabled based on the global options settings.

**Note**: Safe / Unsafe comments will be shown for only observation linked correction actions and not for direct corrective actions.