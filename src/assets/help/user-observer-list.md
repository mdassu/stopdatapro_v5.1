## Users / Observers Report


This report displays the users details such as their user name, last name, first name, email ID, job type, user level and group information.


System administrator may filter the data by Sites, Region, Group, Division, Department, Title, User Level, Observer Status and Roles.


![Image of markdown](assets/help/screenshots/user-observer-list-filter.png)


This is an example of the report.


![Image of markdown](assets/help/screenshots/user-observer-list-report-1.png)