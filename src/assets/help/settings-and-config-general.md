## General Settings


The General Settings of Global Options enable super administrators to customize the log in settings, the company logo and the date format.


**General Preferences**


![Image of markdown](assets/help/screenshots/settings-general-preferences.png)


- **Auto-Logout Interval** – Enter the maximum amount of time a user can remain idle until they are logged out of the application. 

- **Default Date Format** – The default format is MM/DD/YYYY. Super administrator may modify the format of the date by selecting the appropriate option from the drop down menu. The date format gets applied across the application.

- **Company Logo** – Super administrator may upload the company logo using this field. This is the logo that will appear throughout the application at the top left corner. The maximum file size of the image that may be uploaded is 10 MB. To add a logo, click the **Upload** link seen beside the box. The upload window opens. Click **Browse** and locate the image. Once the image is selected, click **Upload**.
**Note**: Saving the Company Logo box as blank, would set the DSS logo by default.

- **Company Logo Preview** – The preview of the uploaded image displays here.

- **Theme**  – Customize the display color of the application by choosing the preferred option from this drop down menu.



**Login Settings**

**Login Settings** - This feature may be used to determine the login settings of the application.



![Image of markdown](assets/help/screenshots/settings-login-settings.png)



- **Enable Login Attempts** – Selecting ‘**Yes**’ will enable the **Login Attempts** field.

- **Login Attempts** – The number of times a user can make consecutive unsuccessful attempts to log into the application may be specified here. If a user fails to enter the correct password within the specified number of times, the account will be locked immediately.

- **User Name Minimum Length** – Enter the minimum length of the username that should be allowed in the system. This value will be validated when the username is entered while adding or editing a user account. If the username is shorter than the value specified here, the system administrator will be prompted about the required length of the user name.

- **Password Minimum Length** – Enter the minimum length of the password that should be allowed in the system. This value will be validated when the password is entered while adding or editing a user account. If the password is shorter than the value specified here, the system administrator will be prompted about the required length of the password.

- **Password Complexity** – This determines if the password should contain plain text or a combination of numerical characters, alphabetical characters and special characters to reinforce password security. The available options include None, numeric, alphanumeric and alphanumeric with special character.

    - **Password Minimum # of Numerical Characters** – Set the minimum number of numerical characters that should be present in the password.

    - **Password Minimum # of Alphabetical Characters** – Set the minimum number of alphabetical characters that should be present in the password.

    - **Password Minimum # of Special Characters** –  Set the minimum number of special characters that should be present in the password.

- **Password Expiration Period** – System administrators may choose this field to set the password expiration date i.e. the number of days for which the password is valid. If the days specified is **60**, then the password of the user expires every 60 days.

 