## Edit a Site

To edit a site:

1. Log into the application and choose **Site Management** -> **Sites** from the Home page.
   The **Site - Listing** page displays.

2. Click on the site name to edit the information.

3. The **Site - General** dialog box displays.

4. Modify the required details.

5. Click the **Save** button.

To save the entered information and add a new site, click the **Save and Add New** button.
