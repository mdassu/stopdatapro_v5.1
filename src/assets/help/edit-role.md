## Edit a Role

System administrator may modify the user roles according to the organizational hierarchy requirements. While editing the role, system administrator may modify the following:

- General information of a role

- Permissions of a role

- User assignments

- Report assignments to a role

To edit a role:

1. Log into the application and choose User Management -> Roles from the Home page.

2. The Role - Listing dialog box displays.

3. Click the role name to edit the information.

4. Modify the information.

5. Click the **Save** button.
