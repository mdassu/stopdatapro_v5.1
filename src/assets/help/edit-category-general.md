## Edit a Main Category

Main Category is an entry in the checklist that helps to categorize the unsafe conditions that need be observed in a site. This may include the use of equipment, disposal of materials or complying with safety guidelines.

For example, consider that an employee employed in a welding company must use eye glasses to protect his eyes from sparks. Here, the main category will be Personal Protective Equipment and the Sub Category will be Eye Protection.

System administrator may edit the general information of the main category and the sub category assignments.

The steps to edit the category information are:

1. From the **Home** page, navigate to **Checklist Configuration**.

2. Select **Categories** from the list of menu options.

3. Select the checkbox present beside the main category name and click on it.

4. The General tab displays.

![Image of markdown](assets/help/screenshots/Edit-Category-general.PNG)

5. Modify the details.

6. Click the **Save** button.

7. System administrator may also access the other tabs to make the required changes.
