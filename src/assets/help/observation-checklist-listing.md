## Observation Checklist


Users with appropriate permission can enter the data in checklist. The basic elements that should be specified while filling up a checklist are Observer details, Categories, Site, Shift and Area. Checklist should be created for a specific area in a site and for a specific shift. System administrator should make sure that all these information had been furnished in the database prior to creating a checklist.


![Image of markdown](assets/help/screenshots/observation-checklist-1.PNG)

![Image of markdown](assets/help/screenshots/observation-checklist-2.PNG)

![Image of markdown](assets/help/screenshots/observation-checklist-3.PNG)

![Image of markdown](assets/help/screenshots/observation-checklist-4.PNG)



**Delete an Observation Checklist**

The steps to delete an observation checklist are:

1. From the **Home** Page, navigate to **Data Entry**.

2. The **Data Entry** menu expands and lists the sub menus on the left side.

3. Select **Observation Checklist** from the list of menu options.

4. The **Observation Checklist** listing page displays.

5. Select the checkbox beside the item to be deleted.

6. Click the **Delete** button.
