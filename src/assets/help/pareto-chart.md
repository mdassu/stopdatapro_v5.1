## Pareto Chart Report
 

A Pareto chart contains a bar graph and a line graph. This report displays the unsafe count against the categories. The lengths of the bars are proportionate to the number of unsafe conditions observed, and are arranged in such a way that the categories with more unsafe counts display on the left and the least on the right. System administrator may analyze the categories that needs more attention.


System administrator may filter the data by Sites, Areas, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Main Categories, Observation Date, Include Checklist Without Group Assignments.


![Image of markdown](assets/help/screenshots/pareto-chart-filter.png)


**Note**: Specifying the observation date range is mandatory to generate this report.  


This is an example of the report.


![Image of markdown](assets/help/screenshots/pareto-chart-report-1.png)


Click on the category name to view details specific to sub categories.


![Image of markdown](assets/help/screenshots/pareto-chart-report-2.png)


System administrator may click on the sub category name to view the data specific to the sub category selected.


![Image of markdown](assets/help/screenshots/pareto-chart-report-3.png)