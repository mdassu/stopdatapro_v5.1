## Reports


![Image of markdown](assets/help/screenshots/report-management-icon.png)


Reports help system administrator to analyze the observation data and get real insights for better decision making. System administrator may export reports to any of the following formats: Excel xlsx, Excel, PDF, Unformatted Excel, Unformatted Excel xlsx and Unformatted CSV. Reports are classified under two types: Trend Analysis and Volume Analysis.


![Image of markdown](assets/help/screenshots/reports-management-listing-tables.png)