## Translate Labels


Administrators may customize text entries in English and multiple other languages as well.


The steps to access **Translation Labels** are:

1. Log in to the application and choose **Settings and Configuration - > Translations** from the Home page.

2. The **Translation** page displays.


![Image of markdown](assets/help/screenshots/settings-translations.png)


3. **Languages** - By default, English is selected. System administrator may select the language in which the text needs to be translated. The options available include Chinese, Danish, Dutch, English, French, Portuguese and Spanish.

4. **Label Type** - Select the label type from the drop down menu. The available options include **Custom** and Group.

5. Select the text that needs translation by selecting the checkbox beside it.

6. Use the text box that displays beside to enter the appropriate translation.

7. Click the **Save** button.