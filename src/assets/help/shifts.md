## Shift Listing Page

The Shift List page contains functionalities that help the administrators to add, edit, delete and activate/inactivate the shifts.

To view the shift list, navigate to Site Management from the Home Page. And click Shifts from the menu options. The Shift List page opens.

![Image of markdown](assets/help/screenshots/shifts-listing.PNG)

- To add a new site, click the **Add** button.

- To edit an existing site, simply click on the site name.

- To delete a site, select the site and click the **Delete** button.

- To activate or inactivate a site, select the site and click the **Change Status** button.
