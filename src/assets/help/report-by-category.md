## Checklist Report (By Category)


This is a bar chart report and displays the safe and unsafe counts by category. System administrator may filter the data by Sites, Areas, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Main Categories, Observation Date, Include Checklist Without Group Assignments.


![Image of markdown](assets/help/screenshots/report-by-category-filter.png)


**Note**: Specifying the Observation Date range is mandatory to generate this report.  
 

This is an example of the report.


To view records by a specific category, select it from the **Report Type** drop down menu. The available types include Safe Main Categories, Unsafe Main Categories and Unsafe Sub Categories. System administrator may also view records specific to a site. The main category name and the sub category name displays beside the bar.


![Image of markdown](assets/help/screenshots/report-by-category-report-1.png)


Click on the category name or the bar to view the detailed report.

The detailed report displays the main category comments, the sub category comments and the safe count.


![Image of markdown](assets/help/screenshots/report-by-category-report-2.png)


**Note**: To view the unsafe count in the detailed report, select *Unsafe Sub* Categories from the *Report Type* dropdown menu.