## Red Flags Listing Page

To access the red flags listing page, navigate to Data Entry, from the Home Page. And click Red Flags from the list of menu options. The Red Flags Listing page opens. This page contains functionalities that help the system administrator to add, edit, delete or change the status of the red flags.

- To add a new user, select the **Add** button.

- To edit an existing user account, click on the preferred user name.

- To delete a user account, select the user and click the **Delete** button.

- To activate or inactivate a user account, select the user and click the **Change Status** button.

To delete a red flag:

1. Log into the application and choose Data Entry -> Red Flags from the Home page.

2. The Red Flags - Listing page displays.

3. Select the checkbox beside the item name and click the Delete button.