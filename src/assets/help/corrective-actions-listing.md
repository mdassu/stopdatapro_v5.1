## Corrective Action Listing

For each observation, any number of corrective actions may be added.

**Access Correction Action Tab**

1. Log into the application and choose **Data Entry -> Corrective Actions**.

2. The Corrective Action Listing page displays.

3. Select the appropriate **Corrective Action Type** from the dropdown menu.

4. Click on the preferred item.

5. Click the **Corrective Action** tab will be shown.


![Image of markdown](assets/help/screenshots/corrective-actions-listing.jpg)

The Corrective action listing page contains options to add, edit or delete corrective action recommendations.

- Click the **Add** button to recommend a corrective action.

- Click on an item to edit it.

- Select an item and click the **Delete** button to delete it.