## Assign Users to Groups

After creating groups, system administrator may assign them to the users.

![Image of markdown](assets/help/screenshots/group-add-assignments.PNG)

The steps to assign groups to users are:

1. Log into the application and choose **User Management** -> **Groups** from the Home page.

2. The **Group Listing** page displays.

3. Click the group name to which users have to be assigned.

4. The **General** information page displays.

5. Click the **Assignments** tab.

The **Available** List displays the users who are not assigned to the selected role.
To assign users, select them from Available list box and use the navigation button to move them to the Assigned list box.

6. The assigned users will be listed in the **Assigned** list box.

7. Click the Save button to **save** the assignments.
