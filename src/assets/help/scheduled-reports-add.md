## Email a Report
 

System administrators may specify the schedule details in the **Scheduling** page. This page is divided into five sections: General, Report Details, Email Details, Schedule Details and Active.


**General**


In the General section, system administrator can specify the name and file format in which the report should be generated.

- Schedule Name - Enter the a unique name to identify the report schedule.

- Export File Type - Select the format in which the report has to be processed and e-mailed. The file types may be PDF, Excel and xlsx.

- Export Format - Select if the excel should be formatted or unformatted.


![Image of markdown](assets/help/screenshots/scheduled-reports-general.png)


**Report Details**

In the Report Details section, system administrator can select the report that should be sent to the recipients.

- Report Name - Select the report that should be emailed to the users.

- Report Display - Select the report type from this dropdown menu.


![Image of markdown](assets/help/screenshots/scheduled-reports-report-details.png)


**Email Details**

In the Email Details section, system administrators may specify the recipients and enter the content in this section.


![Image of markdown](assets/help/screenshots/scheduled-reports-email-details.png)


- To - Select the recipients by using the **Click** link. A dialog box opens displaying the users registered in the application.

- Cc - Enter the email address of users who should receive a carbon copy of the report.

- Subject – Enter the subject of the message here.

- Body – Compose the message that has to be communicated to the recipients. System administrators may change the format, font and color of the message text using the HTML Editor. They may also use the keywords from the Keywords dropdown menu to display the first name and last name of the users.


**Report Filter Criteria**

System administrator may filter the data that should be displayed in the report by using the filter criteria links available inside the box. The **Report Filter Criteria** changes based on the report selected in the **Report Details** section.


![Image of markdown](assets/help/screenshots/scheduled-reports-report-filter.png)


**Email Scheduling**


System administrator may choose the time and date on which the report should be emailed to the users. Choose from the available options whether the emails have to be sent on a weekly basis or monthly basis.

- Send this Email - Select from the dropdown menu, if the email has to be sent once in a week or once in a month.

- Email Start Date - Select the date from which emails should be sent to users.

- Email End Date - Select the date until which emails should be sent to users.

- Time Zone - Select the Time Zone based on which email should be sent to users.

- Time - Select the time at which email should be sent.


![Image of markdown](assets/help/screenshots/scheduled-reports-email-scheduling.png)


**Weekly-Example**


**Weekly** – Select this option to send emails on a weekly basis. Only one email will be sent per week.


![Image of markdown](assets/help/screenshots/scheduled-reports-email-scheduling-weekly.png)


Select the day of week on which an email should be sent during the scheduled period.

Example:

Consider that the Email Start Date is set as January1st and Email End Date as January 31st. And in the weekly setup, Monday is chosen.


![Image of markdown](assets/help/screenshots/scheduled-reports-email-scheduling-weekly-example1.png)


For the above setting, emails will be sent every Monday of January i.e., on the 8th, 15th, 22th and 29th.


**Monthly-Example**


**Monthly** – Select this option to send emails on a monthly basis. Only one email will be sent per month.


![Image of markdown](assets/help/screenshots/scheduled-reports-email-scheduling-monthly.png)


**First, Second, Third, Fourth** and **Last** refer to the week of the month. Choose the week on which the emails need to be sent by selecting the appropriate radio button.

Select the days from the **Day of Week** box. System administrator can choose the day on which emails should be sent.


**Example 1**


Consider that the Email Start Date is set as January1st and Email End Date as March 31st. And in the monthly setup, Second Monday is chosen.


![Image of markdown](assets/help/screenshots/scheduled-reports-email-scheduling-monthly-example2.png)