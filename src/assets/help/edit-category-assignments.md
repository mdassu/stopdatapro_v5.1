## Assign Sub Categories to a Main Category

System administrator may add sub categories specific to a Main Category by accessing the **Assignments** tab present within **Categories**. This page contains options that enable administrators to enter details about the sub category.

**Access Assignments Tab**

The steps to access Assignments tab are:

1. From the Menu Page, navigate to Checklist Configuration.

2. Select Categories from the list of menu options.
   The Category-List dialog box displays.
3. Select the main category to which sub categories need to be added.

4. click the Assignments tab.

![Image of markdown](assets/help/screenshots/Edit-Category-Assignments.PNG)

## Add Sub Categories

To add sub categories:

1. Enter the sub category name in the text box provided.

![Image of markdown](assets/help/screenshots/edit-category-assignments-add-subcategory.PNG)

2. Click the **Add** button.

3. The item displays in the Available Sub-Categories List box.

4. Add as many sub categories as required.

## Assign Sub Categories to the Main Category

The sub categories added will be **available** in the Available list box. System administrator may choose the items that should be associated with the Main Category and assign them using the buttons present between the two list boxes.

- To add an available item to the selected Main Category, choose the item and click .

- To add all of the available items, click .

- To remove an item from the Assigned list, choose the individual item and click .

- To remove all the items at once, click .

Click the **Save** button and the sub categories will be part of the main category.
