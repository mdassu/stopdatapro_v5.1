## Target Vs Activity Report - Weekly


This report displays the target set for each observer and the number of observations completed by the observer on a weekly basis. This report may be used to view how the user is completing his weekly targets.
 

System administrator may filter the data by Sites, Areas, Observers, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Observation Date, Observer Status, Hide Observer With Zero Checklist, Include Checklist Without Group Assignments, Target Status and Start of Week.


![Image of markdown](assets/help/screenshots/target-activity-weekly-filter.png)



This report can be processed for a maximum of 12 weeks.
 

**Note**: The filter criterion “Start of Week” may be used to select a day that should be considered as the first day of the week to process reports. By default, the week starts on Sunday and weekly reports will be drawn from Sunday through Saturday.

 
This is an example of the processed report. Select **Summary** from the **Report Type** dropdown menu to view the summary report.


![Image of markdown](assets/help/screenshots/target-activity-weekly-report-1.png)