## Add Red Flags

To add a red flag:

1. Log into the application and choose **Data Entry -> Red Flags** from the Home page.

2. The Red Flags - Listing page displays.

3. Click the Add button available at the bottom of the page.

4. In the General page, enter the following details:

- **Red Flag Name** - Enter the Red Flag Name that helps in identifying the area and category.

- **Site** - Select the site from the dropdown menu.

- **Area** - The areas associated with the site displays in the dropdown menu. Select the area.

- **Observer** - Select the observer who collected observation data for the selected area.

- **Checklist Setup** - Select the checklist appropriate to the selected site and area.

- **Main Category** - Select the main category for which the red flag has to be created.

- **Sub Category** - Select the sub category for which the red flag has to be created.

- **Red Flag Name** - Provide the red flag name to easily identify the injury potential.

- **Metric** - Choose from the options available in the dropdown menu. The options are Safe and Unsafe.

- **Trigger** - Select the trigger from the dropdown menu. The available options include Greater #, Lesser #, Greater % and Lesser - %.

- **Value** - Enter the numeric value here. This is the threshold value above or below which the safety condition changes.

- **Status** - Select the status from drop down menu.

_Note: The Metric, Trigger and Value are dependent fields and the three components together define if the activity is safe or unsafe._


For example, consider the settings in the image given below.
![Image of markdown](assets/help/screenshots/red-flag-add.jpg)

_**Main Category** - Tools and Equipment - Actions_

_**Sub Category** - Used Correctly_

_**Metrics** - Unsafe_

_**Trigger** - Greater_

_**Value** - 5_

_When the unsafe count in the checklist exceeds 5 for the sub category "Used Correctly", a red flag displays in the report to alert the supervisor._