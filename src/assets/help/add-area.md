## Add an Area

Every facility will have its own divisions or departments. Adding areas to the DataProTM database allows administrators to make safety observations constrained to specific areas. This allows administrators to quickly spot the areas that need attention.

The steps to add an area are:

1. Log into the application and choose **Site Management** -> **Areas** from the Home page.

The **Area-List** dialog box displays.

2. Click the **Add** button.

3. The **Add Area - General Information** dialog box displays.

- **Area Name** - This is a text field. Enter the name that helps users/administrators to identify the area present in the facility.

- **Status** - Specify if the status is active or not i.e., the area is in use any longer or not.

4. Click the **Save** button.

**Note:** To save the entered information and add a new area, click the **Save and Add New button**.
