## Examine User General Information

The basic elements that are required to access the **STOP DataPro** application are User ID, Password, and Role. **User ID** and **Password** are required to log into the application. And **Role** determines the functionalities that should be available to a user. There are a few mandatory fields on the **General** tab which the system administrator has to provide to manage user accounts efficiently.

#### Profile

![Image of markdown](assets/help/screenshots/User-Add-profile.png)

- **User Name** - (Required) 24 character limit. Make this ID easy for users to remember. Enter the user ID.

- **First Name** - (Required) 128 character limit. Enter the first name of the user.

- **Last Name** - (Required) 128 character limit. Enter the last name of the user.

- **Email ID** - 24 character limit. Enter the Email address of the user.

- **Job Title** - Enter the designation of the user.

- **Type** - Select user type from the dropdown menu. The options available include **User, Observer and User and Observer**.

- **Roles** - Select user role from the dropdown menu. The fields enabled in this dialog box vary based on the user role selected.

- **Offline Access** - Grant permission to access observation checklist when there is no internet.

- **Approve Offline / Scanned Observations** - Grant permission to approve observations made while the user was offline.

#### Authentication

![Image of markdown](assets/help/screenshots/User-add-edit-Authentication.PNG)

- **Password** - (Required). Specify the password of the User. This is required to log into the application. The default setting is that the password must contain at least one alphabetic, one numeric and one special character. The permissible minimum length of the password is 8. These specifications may be changed using the **Global Options** feature.

- **Confirm Password**- (Required) Type the password again.

#### Site Assignments

![Image of markdown](assets/help/screenshots/User-add-Site-assignments.PNG)

There are two purposes to assign a user to sites.

- Observation Checklists cannot be assigned to a user directly. Observation checklists may be accessed by a user only if the user is assigned to a site. Each site will have its observation checklist designed depending on the type of job performed. Users assigned to sites will have access to the observation checklists associated with the respective sites.

- To group the users according to the site they belong to.

**Default Site** - Select the default site from the dropdown menu. This is the site that displays as soon as the user logs in.

#### Groups

Group is a medium that helps to organize the vast user base based on their region, location or department. To assign groups to users, select the appropriate group from the **Group Type** drop down menu. A red star beside the dropdown menu indicates that the group assignment is mandatory.

![Image of markdown](assets/help/screenshots/User-Add-Group-assignment.PNG)

#### User Preferences

![Image of markdown](assets/help/screenshots/User-add-User-preference.PNG)

- **Languages** - System administrator may select the language in which the application should be launched to this user.

- **Date Format** - Select the format in which the date should display to the user.

- **Time Zone** - Select the time zone the user belongs to. Emails will be scheduled to the user based on the time zone set here.

- **Status** - Specify if the status of the user is active or not.

**Note:** To save the entered information and add a new user, click the **Save and Add New** button.
