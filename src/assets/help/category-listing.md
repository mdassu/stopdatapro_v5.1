## Main Category Listing Page

To access the main category list page, navigate to **Checklist Configuration** from the **Home** page. Select **Categories** from the list of menu options. The Categories List page displays. This page contains functionalities that help the system administrator to add, edit, delete and change the status of the Main Category.

To add a new main category, select the Add button.

- To edit an existing main category, simply click on the category name.

- To delete a category, select the category and click the Delete button.

- To activate or inactivate a category, select the category and click the Change Status button.
