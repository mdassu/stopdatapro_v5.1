## Role Listing Page

To view the role list, navigate to User Management from the Home Page. Click Roles from the menu options. The Role Listing page opens.

![Image of markdown](assets/help/screenshots/Role-listing.PNG)

There are several options that assist system administrators in managing roles. The Role List page contains functionalities that help the system administrator to add, edit, delete or inactivate/activate a group.

.To add a custom role, select the Add button.

.To edit an existing role, simply click on the preferred role.

.To delete a role, select the checkbox beside the role name and click the Delete button.

.To activate or inactivate a role, select the role and click the Change Status button.

Activating/Inactivating a role

## To inactivate a role:

1.Log into the application and choose User Management -> Roles from the Home page.

2.The Role - Listing page opens.

3.Select the checkbox beside the role name.

4.Click the Change Status button.
