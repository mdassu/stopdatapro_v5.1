## User Defined Module - Assignments Tab

The steps to assign a User Defined Field Value to Sites.:

1.If you set **Yes** and Click the Save button the selected User Defined field value will be assigned for All Sites.

![Image of markdown](assets/help/screenshots/Userdefinedfields-assignments.PNG)

2 . If you set **No** and Click the Save button the selected User Defined field value will be assigned for the selected Sites only.

![Image of markdown](assets/help/screenshots/edit-user-defined-fileds-assignment-2.PNG)
