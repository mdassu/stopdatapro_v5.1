## Corrective Action Report
 

This is a pie chart report and the system administrator may access this report to view the corrective action status. The corrective action status may be Open Corrective Action - Coming Due, Open Corrective Action - Past Due, Closed Corrective Action - On-Time and Closed Corrective Action - Late.


System administrator may filter the data by Sites, Areas, Observers, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Main Categories, Observation Date, Response Date Required, Responded Date, Priority and Corrective Action Card Status.


![Image of markdown](assets/help/screenshots/corrective-action-report-filter.png)


**Note**: Specifying the date range is mandatory to generate this report.  

This is an example of the report.

System administrator may view the data specific to a site by selecting the appropriate one from the **Site** drop down menu.


![Image of markdown](assets/help/screenshots/corrective-action-report-report-1.png)


The processed report displays the corrective action count for the following Corrective Action Card Status:

- **Open Corrective Action** - Coming Due - This signifies that the corrective action process is in progress and should be completed on or before the specified response due date.

- **Open Corrective Action** - Past Due - This signifies that the corrective action process is in progress and the specified response due date has already passed.

- **Closed Corrective Action** - On-Time - This signifies that the corrective action process has been completed prior to the specified response due date.

- **Closed Corrective Action** - Late - This signifies that the corrective action process has been completed past the specified response due date.

System administrator may click the Corrective Action Status links to view the sub reports.

This is an example for **Open Corrective Action - Coming Due** report.
