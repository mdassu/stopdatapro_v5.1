## User Defined Module - General Tab

The steps to add a User Defined Field value are:

1. From the **Home** Page, navigate to **Site Management**

2. Select **User Defined Fields** from the list of menu options.
   The **User Defined Fields Listing** dialog box displays.

3. Click the User Defined Field Name from the listing.
   The **User Defined- General** Tab displays.

![Image of markdown](assets/help/screenshots/Userdefinedfields-Add.PNG)

4. Enter the Field Value name and Click the **Add** button ,now the confirm popup will be shown asking to confirm the creating Field value to be assign to All sites (or) not, If you click confirm the User Defined Field value will be created and will be assigned to All Sites, If you click cancel the User Defined Field value will be created and will not be assign to any of the sites.

The steps to delete a User Defined Field Value Name.:

1. Select the checkbox in the User Defined Field value listing tab.

2. Click the **Delete** button , After reading the appropiate confirm message if you click confirm the selected Field values will be deleted.

![Image of markdown](assets/help/screenshots/edit-user-defined-fields-delete.PNG)
