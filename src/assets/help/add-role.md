## Add a Role

System administrator can add custom roles by modifying the permissions and reports that may be accessed by the user.

To add a custom role:

1. Log into the application and choose User Management -> Roles from the Home page.
   The Role - Listing page displays.

2. Click the **Add** button available at the bottom of the page.

In the General page, enter the following:

- Role Name - Enter a name to uniquely identify the role.

- Description - Enter a concise description of the role.

- Status - By default, the status is Active. The role will be accessible to the users only if it is active.

![Image of markdown](assets/help/screenshots/Add-role.PNG)

3. Click the **Save** button.
