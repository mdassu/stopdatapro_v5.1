## Edit an Observation Checklist


The steps to edit an observation checklist are:

1. From the **Home** Page, navigate to **Data Entry**.

2. The **Data Entry** menu expands and lists the sub menus on the left side.

3. Select **Observation Checklist** from the list of menu options.

4. The **Observation Checklist** listing page displays.

5. Click on the checklist name.

6. Make the required changes.

7. Click the **Save** button.