## User Listing Page

To access the user list page, navigate to User Management, from the Home Page. And click Users from the list of menu options.

The Users List page opens. This page contains functionalities that help the system administrator to add, edit, delete and change

the status of the user. You can filter the user list based on site, user level, roles and groups.

![Image of markdown](assets/help/screenshots/user-listing.png)

- To add a new user, select the Add button.

- To edit an existing user account, click on the preferred user name.

- To delete a user account, select the user and click the Delete button.

- To activate or inactivate a user account, select the user and click the Change Status button.
