## Specify Targets for Users

System administrator may determine the number of checklist an Observer/User or Observer should complete each month using the **Targets** tab. This tab will be enabled only for users whose user role is either **Observer** or **User and Observer**.

![Image of markdown](assets/help/screenshots/User-add-targets.PNG)

If a user is assigned to more than one site, different targets may be set for different sites.

1. Log into the application and choose **User Management** - > **Users** from the Home page.
   The **User** list dialog box displays.

2. Click on the user name to assign targets.
   The **General** information page opens.

3. Click the **Targets** tab.

4. Select the Site from the site name dropdown menu. This dropdown lists the sites that are assigned to the user.

5. If the target is the same for all the months, use the **Populate for all months** field. Enter the numerical value. It will be automatically copied to all the month fields.
   Note: Values may also be entered one by one for every month.

6. Select the **Apply the same to all sites assigned to this user with the above target information** checkbox to apply the same targets for other sites as well.

7. Click the **Save** button to proceed with the action.

**Note:** Select **Weekly** from the dropdown menu to select weekly targets for users. Enter the value in the textbox provided. The value will be applied for all the weeks.

![Image of markdown](assets/help/screenshots/User-targets-Weekly.PNG)
