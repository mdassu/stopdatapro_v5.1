## Forgot Password

This email will be sent to those users who have forgotten their password and have submitted a request by clicking the **Forgot Password** link present in the login page.

Note: System administrator can only edit this notification and cannot add any new notification of this type.

## Email Details

This section may be used to specify the users to whom a copy of the email should be sent and to enter the message that has to be communicated to the user. The subject and the content of the email may be customized to other native languages as well. By default, the content will be in English.

![Image of markdown](assets/help/screenshots/Add-Email-Email-Details.PNG)

Reply To - Enter the email address that would automatically populate in the **To** field when the user attempts to reply to the email sent from this notification.

Cc - Enter the email ID to which a carbon copy of this email has to be sent.

Subject – This is a text field. Enter the subject here.

Body –This is a text field. Compose the message that needs to be communicated to the users.

- The message text can be formatted by using the HTML Editor located above the compose box.

- Use the keywords from the Keywords drop down menu to import the user information and training information of users while sending emails.

- The default keywords available for each notification type vary based on the information that need to be sent to the users.

System administrator may provide equivalent translations in other languages using the **Multilingual Content** tab.

When the translation for the email content is available in the language the user has selected then the user will receive the email in that language only.

![Image of markdown](assets/help/screenshots/multilingual-content.PNG)

To add translations in other languages:

- Select the **Multilingual Content** tab.

- Select the language from the **Languages** dropdown menu.

- Enter the subject and the content of the email.

- Click the **Save** button.

## Email Filter Criteria

System administrator may configure this notification for a specific site or group by selecting the appropriate option from the Filter Criteria box.

When a site is selected, only users belonging to that sit will receive notifications.

![Image of markdown](assets/help/screenshots/Add-Email-Email-Filter-Criteria.PNG)
