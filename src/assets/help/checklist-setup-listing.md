## View Checklist Setup List Page

To access the checklist setup list page, navigate to Checklist Configuration, from the Home Page. And click Checklist Setup from the list of menu options. The Checklist Setup Listing page opens. This page contains functionalities that help the system administrator to design, edit or delete the checklist.

![Image of markdown](assets/help/screenshots/Checklist-setup-listing.PNG)

Note: Three new checklists: **SFS/SFEO Default checklist, STOP for Healthcare – Default** and **STOP for PSM - Default** have been added replacing the default checklist. This is applicable for new customers only.

- To design a new checklist, click the **Add** button.

* To edit an existing checklist information, click on the preferred checklist name.

- To delete a checklist, select the checklist and click the **Delete** button.

* To activate or inactivate a checklist, select the checklist and click the **Change Status** button.
