## Manage Profile Settings

 
Users can change their profile information according to their preference at any time.

 
**Change General Information**

 
Users may change their first name, last name, password and email address while updating their profile information.

 
To change the general information:

- Click the  ![Image of markdown](assets/help/screenshots/user-profile-icon.png)  icon available on the right side of the title bar.

- In the Profile information page, update your name, password and email address.


![Image of markdown](assets/help/screenshots/user-profile-general.png)


- **User Name** - Enter the name that should be used to log into the system.

- **Password** - Enter the password that may be used from the next time.

- **Confirm Password** - Re-enter the password.

- **Last Name** - Enter the last name that should display across the application.

- **Last Name** - Enter the first name.

- **Email Id** - Email address to which emails should be sent.


**Change User preferences**


- **Date format** - Specify the preferred date format in which dates should display.

- **Languages** - Select the language in which the application should display.

- **Default Site** - Select the default site. User may use this when multiple sites are assigned to them.

- **Time zone** - Select the time zone the user belongs to. Email gets triggered based on the time zone selected here.

- **Quick Links** - These are links that display on the left side of the screen. Users may access this link to quickly access the application links.


![Image of markdown](assets/help/screenshots/user-profile-user-preferences.png)