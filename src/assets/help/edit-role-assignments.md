## Assign Users to Role

After creating roles, system administrator may assign them to the users.

![Image of markdown](assets/help/screenshots/Role-assignments.PNG)

The steps to assign roles to users are:

1. Log into the application and choose **User Management** - > **Roles** from the Home page.

2. The **Role Listing** page displays.

3. Click on the role name for which users have to be assigned.

4. Click the **Assignments** tab.

The **Available** List displays the users who are not assigned to the selected role.

To assign users, select them from Available list box and use the navigation button to move them to the Assigned list box.

5. The assigned users will be listed in the **Assigned** list box.

6. Click the **Save** button to save the assignments.
