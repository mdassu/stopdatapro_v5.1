## Add a Shift

The steps to add a shift are:

1. Log in to the application and choose Site Management - > Shifts from the Home page.

2. The **Shift - Listing** page displays.

3. Click the **Add** button.

4. The General information page displays.

![Image of markdown](assets/help/screenshots/Shift-Add.PNG)

- **Shift Name** - Enter the name that is used to uniquely identify the shift.

- **Status** – Select the status from the dropdown menu. By default, Active displays. Inactivating will hide this from the users’ access.

5. Click **Save** to add the shift.

**Note:** To save the entered information and add a new area, click the **Save and Add New** button.
