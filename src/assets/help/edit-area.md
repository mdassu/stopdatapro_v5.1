## Edit an Area

To edit the area information:

1.Log into the application and choose Site Management -> Areas from the Home page.

2.Click on the area name to edit the information.

3.The Area - General dialog box displays.

4.Modify the required details.

5.Click the Save button.

Note: To save the entered information and add a new area, click the Save and Add New button.

## Assign Sites to the Areas

Once the area has been created, it may be associated with appropriate sites.

1. Log in to the application and choose **Site Management** - > **Areas** from the Home page.
   The **Area-Listing** page displays.

2. Click on the area name from the **Area - Listing** page.
3. Click the **Assignments** tab.

![Image of markdown](assets/help/screenshots/areasAssignments.PNG)

4. The Available list box displays the site that are not associated with the area yet. The Assigned list box displays the sites that are already associated with the Area.

- To add an available item, click .

- To add all of the sites available, click .

- To remove an item from the Assigned list, choose the individual item and click .

- To remove all the items at once, click .

5. Click the **Save** button.
