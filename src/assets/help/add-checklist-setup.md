## Design a Checklist

A checklist is a compiled form that contains the safety observations which could be performed on a site. System administrator may choose if the checklist should contain **Main categories and Sub categories**. There are also options to choose if the checklist should be assigned to specific sites or all sites.

- Firstly decide on the sites for which you are creating the checklist

- Secondly choose the categories that are appropriate for the site

- Finally choose the sub categories

**Note**: Prior to setting up a checklist, the system administrator should create the **Main Categories** and **Sub Categories**.

The steps to add a checklist are:

1. From the **Home** Page, navigate to **Checklist Configuration**.

2. The **Checklist Configuration** menu expands and lists the sub menus on the left side.

3. Select **Checklist Setup** from the list of menu options.

4. The **Checklist Setup** listing page displays.

5. Click the **Add** button.
   The **Checklist - General** information page displays.

![Image of markdown](assets/help/screenshots/Add-checklistsetup.PNG)

- **Checklist Setup name** - Enter the name that helps in identifying the checklist.

- **All Safe Check** - Select **Yes** to enable **All Safe** checkbox in the checklist. When there are no unsafe observations, the user may just select this checkbox.

- **All Sites Setup** - Select **Yes** if the checklist has to be assigned to all sites available in the application. When **No** is selected, the **Sites** tab will be enabled and the system administrator may chose the sites for which this checklist should be assigned.

- **Copy From** - This dropdown displays the already created checklist. Select the **checklist** from which the categories should be copied to the current checklist. This eliminates the need of the administrator to create a checklist from scratch. All the main categories associated with the selected checklist will be copied to the new one. System administrator may edit the category assignments by accessing the tabs available within **Checklist Setup**.

- **Status** - Select the status from the dropdown menu. By default, Active displays.

**Note**: If the logged in user is a site administrator then this dropdown menu displays only the sites that are assigned to the user.

Click the **Save** button.
