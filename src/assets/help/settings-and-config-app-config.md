## Application Configuration


System administrator can configure some of the basic settings by using the **Application Configuration** tab. They can customize:

- The languages that should be accessible to the users

- The group types that should be enabled

- The email notification types and

- The time zones


**Languages** - The languages selected here will display in the **Languages** dropdown across the application.


![Image of markdown](assets/help/screenshots/settings-languages.png)


**Group Types** - Groups Types are used to categorize the users based on the location or the functional role. By default, there are 5 Group Types: Region, Group, Division, Department and Title. System administrator may enable or disable them according to the organizational structure. The group types selected here display in the Users - *General Information page*.


![Image of markdown](assets/help/screenshots/settings-group-types.png)


**Email Types** - Emails may be sent to users to remind or notify a specific task to the users. System administrator may configure the email types that should be available in the application by selecting the items from here.


![Image of markdown](assets/help/screenshots/settings-email-types.png)


**Time Zone** - System administrator may enable only the time zones that are appropriate for their site. Emails will be generated based on the time zones.


![Image of markdown](assets/help/screenshots/settings-time-zones.png)


**Approval Required for Observation Checklists Entered Via PC / Mobile App** - A new option has been added in the Application Configuration page to set if approval is required from the administrator to accept the checklist data entered by the users from the PC. BY default, this option is set as No. When No is set for both PC and Mobile App, the Role - Assignment box will be disabled, as approval is not required.


![Image of markdown](assets/help/screenshots/settings-approval-required.png)


When Approval Required is set as Yes for both PC & Mobile, the roles that require approval will be the same for mobile users and PC users.


**Note:**


- Super admin does not require approval to add checklist entries and can view all the entries.

- For other roles, Observation checklists will be sent to the approval queue based on the options and roles selected in the Application Configuration page. If the setting is such that the user role requires approval, then the logged in user can view the entry only until it is approved.

- If approval is not required for observations entered through PC or Mobile, then the user who made the entry can access it from the observation listing page as well as in the reports.