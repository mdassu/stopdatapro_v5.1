## Checklist Report (By Metrics)
 

This report displays the safe count and unsafe counts recorded for each site.


System administrator may filter data by Sites, Area, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Observation Date and Include Checklist Without Group Assignments.


![Image of markdown](assets/help/screenshots/report-by-metrics-filter.png)


**Note**: Specifying the Observation Date range is mandatory to generate this report.  


This is an example of the report.


![Image of markdown](assets/help/screenshots/report-by-metrics-report-1.png)