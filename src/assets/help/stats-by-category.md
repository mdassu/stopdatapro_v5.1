## Stats By Category Report


This report displays the checklist safe and unsafe value in tabular and checklist format.
 

System administrator may filter the data by Sites, Areas, Observers, Shifts, Region, Group, Division, Department, Title, Checklist Setup, Observation Date, Observer Status, Hide Observer With Zero Checklist, Include Checklist Without Group Assignments, Target Status and Start of Week.


![Image of markdown](assets/help/screenshots/stats-by-category-filter.png)


This is an example of the processed report. Select Tabular from the Report Type dropdown menu to view the Tabular format report.


![Image of markdown](assets/help/screenshots/stats-by-category-report-1.png)


This is an example of the processed report. Select Checklist from the Report Type dropdown menu to view the Checklist format report.


![Image of markdown](assets/help/screenshots/stats-by-category-report-2.png)