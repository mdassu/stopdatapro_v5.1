## Area Listing Page

To access the area list page, navigate to Site Management, from the Home Page. And click Areas from the list of menu options. The Area List page opens. This page contains functionalities that help the system administrator to add, edit, delete and change the status of the area.

![Image of markdown](assets/help/screenshots/Areas-listing.PNG)

- To add a new area, select the **Add** button.

- To edit an existing area information, click on the preferred user name.

- To delete an area, select the area name and click the **Delete** button.

- To activate or inactivate an area, select the area and click the **Change Status** button.
